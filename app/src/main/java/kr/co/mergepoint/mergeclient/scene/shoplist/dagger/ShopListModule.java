package kr.co.mergepoint.mergeclient.scene.shoplist.dagger;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocation;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Filter;
import kr.co.mergepoint.mergeclient.scene.shoplist.ShopListActivity;
import kr.co.mergepoint.mergeclient.scene.shoplist.model.ShopListModel;
import kr.co.mergepoint.mergeclient.scene.shoplist.presenter.ShopListPresenter;
import kr.co.mergepoint.mergeclient.scene.shoplist.presenter.callback.ShopListCallback;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.ShopListView;

import static kr.co.mergepoint.mergeclient.application.common.Properties.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UPDATE_INTERVAL_IN_MILLISECONDS;

/**
 * Created by jgson on 2017. 6. 5..
 */

@Module
public class ShopListModule {

    private ShopListActivity activity;

    public ShopListModule(ShopListActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ShopListScope
    FusedLocationProviderClient provideGoogleApiClientBuilder() {
        return LocationServices.getFusedLocationProviderClient(activity);
    }

    @Provides
    @ShopListScope
    LocationRequest provideLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        return locationRequest;
    }

    @Provides
    @ShopListScope
    UserLocation provideUserLocation(FusedLocationProviderClient providerClient, LocationRequest locationRequest) {
        return new UserLocation(providerClient, activity, locationRequest, activity);
    }

    @Provides
    @ShopListScope
    ShopListModel provideModel() { return new ShopListModel(); }

    @Provides
    @ShopListScope
    ShopListView provideView() { return new ShopListView(activity, R.layout.shop_list); }

    @Provides
    @ShopListScope
    ShopListPresenter providePresenter(ShopListView view, ShopListModel model, UserLocation userLocation) {
        return new ShopListPresenter(view, model, new ShopListCallback(view, model), userLocation);
    }
}
