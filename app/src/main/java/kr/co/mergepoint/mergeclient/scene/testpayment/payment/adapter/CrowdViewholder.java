package kr.co.mergepoint.mergeclient.scene.testpayment.payment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.co.mergepoint.mergeclient.R;

/**
 * User: Marty
 * Date: 2018-07-18
 * Time: 오후 2:33
 * Description:
 */
public class CrowdViewholder extends RecyclerView.ViewHolder {
    @BindView(R.id.crowd_list_delete)
    ImageView crowdListDelete;
    @BindView(R.id.crowd_name)
    TextView crowdName;
    @BindView(R.id.crowd_list_prise)
    TextView crowdListPrise;

    public CrowdViewholder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);
    }

    public CrowdViewholder(Context mCon, ViewGroup parent) {
        super(LayoutInflater.from(mCon).inflate(R.layout.crowd_listitem, parent, false));


    }
}
