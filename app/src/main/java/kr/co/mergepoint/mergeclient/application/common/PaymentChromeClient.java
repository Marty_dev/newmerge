package kr.co.mergepoint.mergeclient.application.common;

import android.webkit.JsResult;
import android.webkit.WebView;

import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.scene.payment.PaymentActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * Created by 1017sjg on 2017. 7. 24..
 */

public class PaymentChromeClient extends CustomChromeClient {

    private BaseActivity activity;

    public PaymentChromeClient(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
        new MergeDialog.Builder(activity)
                .setContent(message)
                .setCancelBtn(false)
                .setConfirmClick(result::confirm).build().show();
        MDEBUG.debug("JS Alert is Show");

        return true;
    }

    @Override
    public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
        new MergeDialog.Builder(activity)
                .setContent(message)
                .setConfirmClick(result::confirm)
                .setCancelClick(result::cancel).build().show();
        MDEBUG.debug("JS Confirm is Show");

        return true;
    }
}
