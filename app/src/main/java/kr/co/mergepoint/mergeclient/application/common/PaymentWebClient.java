package kr.co.mergepoint.mergeclient.application.common;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.Browser;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.scene.payment.PaymentActivity;

/**
 * Created by 1017sjg on 2017. 7. 24..
 */

public class PaymentWebClient extends CustomWebClient {

    private BaseActivity activity;
    private WebView webView;

    public PaymentWebClient(WebView webview, BaseActivity activity) {
        this.activity = activity;
        this.webView = webview;
    }

    private void showMergeAlert(MergeDialog.OnDialogClick positive) {
        new MergeDialog.Builder(activity).setContent("확인을 누르시면 구글플레이로 이동합니다.").setConfirmClick(positive).build().show();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return urlLoad(view, url);
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        String url = request.getUrl().toString();
        return urlLoad(view, url);
    }

    private boolean urlLoad(final WebView view, String url) {
        Log.d("================url::", url);
        if ((url.startsWith("http://") || url.startsWith("https://")) && url.endsWith(".apk")) {
            downloadFile(url);
            return super.shouldOverrideUrlLoading(view, url);
        } else if ((url.startsWith("http://") || url.startsWith("https://")) && (url.contains("market.android.com") || url.contains("m.ahnlab.com/kr/site/download"))) {
            Uri uri = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            try {
                activity.startActivity(intent);
                return true;
            } catch (ActivityNotFoundException e) {
                return false;
            }
        } else if (url.startsWith("http://") || url.startsWith("https://")) {
            view.loadUrl(url);
            return true;
        } else if (url != null
                && (url.contains("vguard") || url.contains("droidxantivirus") || url.contains("smhyundaiansimclick://")
                || url.contains("smshinhanansimclick://") || url.contains("smshinhancardusim://") || url.contains("smartwall://") || url.contains("appfree://")
                || url.contains("v3mobile") || url.endsWith(".apk") || url.contains("market://") || url.contains("ansimclick")
                || url.contains("market://details?id=com.shcard.smartpay") || url.contains("shinhan-sr-ansimclick://"))) {
            return callApp(url);
        } else if (url.startsWith("smartxpay-transfer://")) {
            boolean isatallFlag = isPackageInstalled(activity.getApplicationContext(), "kr.co.uplus.ecredit");
            if (isatallFlag) {
                boolean override = false;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, activity.getPackageName());

                try {
                    activity.startActivity(intent);
                    override = true;
                } catch (ActivityNotFoundException ignored) {}

                return override;
            } else {
                showMergeAlert(() -> {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(("market://details?id=kr.co.uplus.ecredit")));
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.putExtra(Browser.EXTRA_APPLICATION_ID, activity.getPackageName());
                    activity.startActivity(intent);
                    activity.overridePendingTransition(0, 0);
                });
                return true;
            }
        } else if (url.startsWith("ispmobile://")) {
            boolean isatallFlag = isPackageInstalled(activity.getApplicationContext(), "kvp.jjy.MispAndroid320");
            if (isatallFlag) {
                boolean override = false;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, activity.getPackageName());

                try {
                    activity.startActivity(intent);
                    override = true;
                } catch (ActivityNotFoundException ignored) {}
                return override;
            } else {
                showMergeAlert(() -> view.loadUrl("http://mobile.vpay.co.kr/jsp/MISP/andown.jsp"));
                return true;
            }
        } else if (url.startsWith("paypin://")) {
            boolean isatallFlag = isPackageInstalled(activity.getApplicationContext(), "com.skp.android.paypin");
            if (isatallFlag) {
                boolean override = false;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, activity.getPackageName());

                try {
                    activity.startActivity(intent);
                    override = true;
                } catch (ActivityNotFoundException ex) {
                }
                return override;
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(("market://details?id=com.skp.android.paypin&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS5za3AuYW5kcm9pZC5wYXlwaW4iXQ..")));
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, activity.getPackageName());
                activity.startActivity(intent);
                activity.overridePendingTransition(0, 0);
                return true;
            }
        } else if (url.startsWith("lguthepay://")) {
            boolean isatallFlag = isPackageInstalled(activity.getApplicationContext(), "com.lguplus.paynow");
            if (isatallFlag) {
                boolean override = false;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, activity.getPackageName());

                try {
                    activity.startActivity(intent);
                    override = true;
                } catch (ActivityNotFoundException ignored) {}
                return override;
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(("market://details?id=com.lguplus.paynow")));
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, activity.getPackageName());
                activity.startActivity(intent);
                activity.overridePendingTransition(0, 0);
                return true;
            }
        } else {
            return callApp(url);
        }
    }

    // 외부 앱 호출
    private boolean callApp(String url) {
        Intent intent = null;
        // 인텐트 정합성 체크 : 2014 .01추가
        try {
            intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
            Log.e("intent getScheme     >", intent.getScheme());
            Log.e("intent getDataString >", intent.getDataString());
        } catch (URISyntaxException ex) {
            Log.e("Browser", "Bad URI " + url + ":" + ex.getMessage());
            return false;
        }
        try {
            boolean retval = true;
            //chrome 버젼 방식 : 2014.01 추가
            if (url.startsWith("intent")) { // chrome 버젼 방식
                // 앱설치 체크를 합니다.

                // 국민 앱카드가 설치 되지 않은 경우
                // 국민카드(+앱카드)로 변경 하여 호출 하고
                // 국민카드(+앱카드)도 없는 경우에 국민카드(+앱카드)마켓으로 이동
                if (activity.getPackageManager().resolveActivity(intent, 0) == null && "com.kbcard.cxh.appcard".equals(intent.getPackage())) {
                    intent.setPackage("com.kbcard.kbkookmincard");	// 국민카드(+앱카드)로 변경

                    if (activity.getPackageManager().resolveActivity(intent, 0) == null ) {
                        String packagename = intent.getPackage();
                        if (packagename != null) {
                            Uri uri = Uri.parse("market://search?q=pname:" + packagename);
                            intent = new Intent(Intent.ACTION_VIEW, uri);
                            activity.startActivity(intent);
                            retval = true;
                        }
                    } else {
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.setComponent(null);
                        try {
                            if (activity.startActivityIfNeeded(intent, -1)) {
                                retval = true;
                            }
                        } catch (ActivityNotFoundException ex) {
                            retval = false;
                        }
                    }
                } else if (activity.getPackageManager().resolveActivity(intent, 0) == null ) {	// 앱 미설치 시 마켓 이동
                    String packagename = intent.getPackage();
                    if (packagename != null) {
                        Uri uri = Uri.parse("market://search?q=pname:" + packagename);
                        intent = new Intent(Intent.ACTION_VIEW, uri);
                        activity.startActivity(intent);
                        retval = true;
                    }
                } else {
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setComponent(null);
                    try {
                        if (activity.startActivityIfNeeded(intent, -1)) {
                            retval = true;
                        }
                    } catch (ActivityNotFoundException ex) {
                        retval = false;
                    }
                }
            } else { // 구 방식
                Uri uri = Uri.parse(url);
                intent = new Intent(Intent.ACTION_VIEW, uri);
                activity.startActivity(intent);
                retval = true;
            }
            return retval;
        } catch (ActivityNotFoundException e) {
            Log.e("error ===>", e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

//    private boolean urlLoading(final WebView view, String url) {
//        if (url.startsWith("http://") || url.startsWith("https://")) {
//            view.loadUrl(url);
//            return true;
//        }
//
//        if ((url.startsWith("http://") || url.startsWith("https://"))
//                && (url.contains("market.android.com") || url.contains("m.ahnlab.com/kr/site/download"))) {
//            Uri uri = Uri.parse(url);
//            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//            try {
//                activity.startActivity(intent);
//            } catch (ActivityNotFoundException e) {
//                return false;
//            }
//        } else if (url.contains("vguard") || url.contains("droidxantivirus") || url.contains("smhyundaiansimclick://")
//                        || url.contains("smshinhanansimclick://") || url.contains("smshinhancardusim://") || url.contains("smartwall://")
//                        || url.contains("appfree://") || url.contains("v3mobile") || url.endsWith(".apk") || url.contains("market://")
//                        || url.contains("ansimclick") || url.contains("market://details?id=com.shcard.smartpay") || url.contains("shinhan-sr-ansimclick://")) {
//            Intent intent;
//
//            try {
//                intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
//                Log.e("intent getScheme >", intent.getScheme());
//                Log.e("intent getDataString >", intent.getDataString());
//            } catch (URISyntaxException e) {
//                Log.e("Browser", "Bad URI " + url + ":" + e.getMessage());
//                return false;
//            }
//
//            try {
//                boolean retval = true;
//
//                if (url.startsWith("intent")) {
//                    if (activity.getPackageManager().resolveActivity(intent, 0) == null) {
//                        String packageName = intent.getPackage();
//                        if (packageName != null) {
//                            Uri uri = Uri.parse("market://search?q=pname:" + packageName);
//                            intent = new Intent(Intent.ACTION_VIEW, uri);
//                            activity.startActivity(intent);
//                            retval = true;
//                        }
//                    } else {
//                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
//                        intent.setComponent(null);
//                        try {
//                            if (activity.startActivityIfNeeded(intent, -1)) {
//                                retval = true;
//                            }
//                        } catch (ActivityNotFoundException e) {
//                            retval = false;
//                        }
//                    }
//                } else {
//                    Uri uri = Uri.parse(url);
//                    intent = new Intent(Intent.ACTION_VIEW, uri);
//                    activity.startActivity(intent);
//                    retval = true;
//                }
//                return retval;
//            } catch (ActivityNotFoundException e) {
//                Log.e("error ===>", e.getMessage());
//                e.printStackTrace();
//                return false;
//            }
//        }
//
//        if (url.startsWith("smartxpay-transfer://")) {
//            boolean isatallFlag = isPackageInstalled(activity.getApplicationContext(), "kr.co.uplus.ecredit");
//            if (isatallFlag) {
//                boolean override = false;
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                intent.addCategory(Intent.CATEGORY_BROWSABLE);
//                intent.putExtra(Browser.EXTRA_APPLICATION_ID, activity.getPackageName());
//
//                try {
//                    activity.startActivity(intent);
//                    override = true;
//                } catch (ActivityNotFoundException e) {
//                    e.printStackTrace();
//                }
//
//                return override;
//            } else {
//                showAlert(new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        view.loadUrl("http://mobile.vpay.co.kr/jsp/MISP/andown.jsp");
//                    }
//                }, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.cancel();
//                    }
//                });
//                return true;
//            }
//        }
//
//        return false;
//    }

    // DownloadFileTask생성 및 실행
    private void downloadFile(String mUrl) {
        new DownloadFileTask().execute(mUrl);
    }

    // AsyncTask<Params,Progress,Result>
    private class DownloadFileTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            URL myFileUrl = null;
            try {
                myFileUrl = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream is = conn.getInputStream();

                // 다운 받는 파일의 경로는 sdcard/ 에 저장되며 sdcard에 접근하려면 uses-permission에
                // android.permission.WRITE_EXTERNAL_STORAGE을 추가해야만 가능.
                String mPath = "sdcard/v3mobile.apk";
                FileOutputStream fos;
                File f = new File(mPath);
                if (f.createNewFile()) {
                    fos = new FileOutputStream(mPath);
                    int read;
                    while ((read = is.read()) != -1) {
                        fos.write(read);
                    }
                    fos.close();
                }

                return "v3mobile.apk";
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }

        @Override
        protected void onPostExecute(String filename) {
            if (!"".equals(filename)) {
                Toast.makeText(activity.getApplicationContext(), "download complete", Toast.LENGTH_SHORT).show();

                // 안드로이드 패키지 매니저를 사용한 어플리케이션 설치.
                File apkFile = new File(Environment.getExternalStorageDirectory() + "/" + filename);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
                activity.startActivity(intent);
            }
        }
    }

    private boolean isPackageInstalled(Context context, String packageName) {
        try {
            context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_ACTIVITIES); // TODO:android.content.pm.PackageManager$NameNotFoundException: kvp.jjy.MispAndroid320
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
    }
}
