package kr.co.mergepoint.mergeclient.scene.payment.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.PaymentChromeClient;
import kr.co.mergepoint.mergeclient.application.common.PaymentWebClient;
import kr.co.mergepoint.mergeclient.application.common.SingleWebFragment;
import kr.co.mergepoint.mergeclient.application.common.script.WebReturnScriptInterface;
import kr.co.mergepoint.mergeclient.scene.payment.PaymentActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static kr.co.mergepoint.mergeclient.application.common.Properties.WEB_URL;

/**
 * Created by 1017sjg on 2017. 7. 21..
 */

public class CardPaymentWebFragment extends SingleWebFragment<WebReturnScriptInterface> {

    public BaseActivity activity;

    public static CardPaymentWebFragment newInstance(String url, BaseActivity activity) {
        Bundle args = new Bundle();
        args.putString(WEB_URL, url);
        CardPaymentWebFragment fragment = new CardPaymentWebFragment();
        fragment.activity = activity;
        fragment.setArguments(args);


        return fragment;
    }


    @Override
    protected WebChromeClient setWebChromeClient(ContentLoadingProgressBar loadProgressBar) {
        PaymentChromeClient chromeClient = new PaymentChromeClient(activity);
        chromeClient.setContentLoadingProgressBar(loadProgressBar);
        return chromeClient;
    }

    @Override
    protected WebViewClient setWebViewClient() {
        return new PaymentWebClient(getWebView(), activity);
    }

    @Override
    protected WebReturnScriptInterface setJavascript() {
        return new WebReturnScriptInterface(activity);
    }

    @Override
    protected String setPageTitle() {
        return getString(R.string.payment_title);
    }

    class AndroidBridge{
        @JavascriptInterface
        public void failure(String msg){
            MDEBUG.debug("Failure is Call" + msg);

        }
        @JavascriptInterface
        public void success(){
            MDEBUG.debug("Success is Call");
            // TODO: 2018-07-17  Success Activity !  
        }

        @JavascriptInterface
        public void getHtml(String msg){
            MDEBUG.debug("HTML MSG : " + msg);
        }
    }
}
