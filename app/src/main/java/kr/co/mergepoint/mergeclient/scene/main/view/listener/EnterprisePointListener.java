package kr.co.mergepoint.mergeclient.scene.main.view.listener;

public interface EnterprisePointListener {
    void changeEnterprisePointHistory(int type, int period);
}
