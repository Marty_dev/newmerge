package kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import kr.co.mergepoint.mergeclient.databinding.ShopMenuChildBinding;
import kr.co.mergepoint.mergeclient.scene.data.menu.ShopChildInfo;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class ChildMenuHolder extends ChildViewHolder {

    private ShopMenuChildBinding shopMenuChildBinding;

    public ChildMenuHolder(ShopMenuChildBinding shopMenuChildBinding) {
        super(shopMenuChildBinding.getRoot());
        this.shopMenuChildBinding = shopMenuChildBinding;
    }

    public void onBind(ShopChildInfo childInfo) {
        shopMenuChildBinding.setShopChildInfo(childInfo);
    }

    public ShopMenuChildBinding getShopMenuChildBinding() {
        return shopMenuChildBinding;
    }
}
