package kr.co.mergepoint.mergeclient.application.common.location;

import android.annotation.SuppressLint;
import android.content.IntentSender;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.support.design.widget.Snackbar.LENGTH_SHORT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.REQUEST_CHECK_SETTINGS;

/**
 * Created by jgson on 2018. 1. 2..
 */

public class UserLocation {

    private UserLocationListener userLocationListener;
    private BaseActivity activity;
    private FusedLocationProviderClient locationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;

    public UserLocation(FusedLocationProviderClient locationProviderClient, UserLocationListener userLocationListener, LocationRequest locationRequest, BaseActivity activity) {
        this.locationProviderClient = locationProviderClient;
        this.userLocationListener = userLocationListener;
        this.activity = activity;
        this.locationRequest = locationRequest;
        this.locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                userLocationListener.startLoadList(locationResult.getLastLocation());
                stopLocationUpdates();
            }
        };
    }

    public void startLocationUpdates() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            /*마시멜로우 이상 권한체크*/
            boolean fineResult = ContextCompat.checkSelfPermission(activity, ACCESS_FINE_LOCATION) != PERMISSION_GRANTED;
            boolean coarseResult = ContextCompat.checkSelfPermission(activity, ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED;
            if (fineResult && coarseResult) {
                if (activity.shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION) || activity.shouldShowRequestPermissionRationale(ACCESS_COARSE_LOCATION)) {
                    /*권한을 취소한적이 있을 때*/
                    Snackbar.make(activity.findViewById(android.R.id.content), R.string.request_permission, LENGTH_SHORT).show();
                    userLocationListener.startLoadList(null);
                } else {
                    /*권한 취소를 한적이 없는 최초의 권한 동의*/
                    new Handler().postDelayed(() -> new MergeDialog.Builder(activity)
                            .setContent(R.string.location_permission_text)
                            .setConfirmClick(() -> userLocationListener.requestPermissions())
                            .setCancelClick(() -> userLocationListener.startLoadList(null)).build().show(), 400);
                }
            } else {
                /*권한 동의했을 때*/
                requestLocationUpdates();
            }
        } else {
            /*마시멜로우 아랫버전으로 권한체크 불필요*/
            requestLocationUpdates();
        }
    }

    public void stopLocationUpdates() {
        locationProviderClient.removeLocationUpdates(locationCallback);
    }

    @SuppressLint("MissingPermission")
    private void requestLocationUpdates() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(this.locationRequest);
        SettingsClient client = LocationServices.getSettingsClient(activity);
        client.checkLocationSettings(builder.build())
                .addOnSuccessListener(activity, locationSettingsResponse -> locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper()))
                .addOnFailureListener(activity, e -> {
                    int statusCode = ((ApiException) e).getStatusCode();
                    switch (statusCode) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            /*위치 설정에 만족하지 못했을때, 위치설정 업그레이드가 필요함!*/
                            Log.i("UserLocation", "Location settings are not satisfied. Attempting to upgrade location settings ");
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the
                                // result in onActivityResult().
                                ResolvableApiException rae = (ResolvableApiException) e;
                                rae.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException sie) {
                                Log.i("UserLocation", "PendingIntent unable to execute request.");
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            /*위치 설정이 적합하지않음, 설정을 고쳐야함!*/
                            String errorMessage = "Location settings are inadequate, and cannot be fixed here. Fix in Settings.";
                            Log.e("UserLocation", errorMessage);
                            Snackbar.make(activity.findViewById(android.R.id.content), errorMessage, LENGTH_SHORT).show();
                    }
                });
    }
}
