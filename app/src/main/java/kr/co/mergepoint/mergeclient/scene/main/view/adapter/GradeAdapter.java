package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.databinding.SingleNoGradientImageBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.GradeRes;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

/**
 * Created by 1017sjg on 2017. 6. 19..
 */

public class GradeAdapter extends PagerAdapter {

    private ArrayList<Integer> gradeImg;
    private String[] grades;
    private GradeRes Res;

    public GradeAdapter(Context context, String[] grades,GradeRes res) {
        this.gradeImg = new ArrayList<>();
        this.grades = grades;
        this.Res = res;
        TypedArray drawables = context.getResources().obtainTypedArray(R.array.grade_description_img);
        for (int i = 0; i < grades.length; i++)
            gradeImg.add(drawables.getResourceId(i, NO_VALUE));
        drawables.recycle();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        SingleNoGradientImageBinding imageBinding = DataBindingUtil.inflate(LayoutInflater.from(container.getContext()), R.layout.single_no_gradient_image, container, false);
        imageBinding.setDrawable(gradeImg.get(position));
        imageBinding.setGrade(Res.gradeList.get(position));
        if (Res.gradeList.get(position).terms.isEmpty()) {
            imageBinding.terms.setVisibility(View.INVISIBLE);
            imageBinding.textTerms.setVisibility(View.INVISIBLE);
        }else{
            imageBinding.terms.setVisibility(View.VISIBLE);
            imageBinding.textTerms.setVisibility(View.VISIBLE);
            imageBinding.terms.setText(Res.gradeList.get(position).terms);
        }
        if (Res.gradeList.get(position).benefitComment.isEmpty()) {
            imageBinding.benefit.setVisibility(View.INVISIBLE);
            imageBinding.textBenefit.setVisibility(View.INVISIBLE);
        }else{
            imageBinding.benefit.setVisibility(View.VISIBLE);
            imageBinding.textBenefit.setVisibility(View.VISIBLE);
            imageBinding.benefit.setText(Res.gradeList.get(position).benefitComment);
        }
        container.addView(imageBinding.getRoot());
        return imageBinding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return gradeImg.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return grades[position];
    }
}
