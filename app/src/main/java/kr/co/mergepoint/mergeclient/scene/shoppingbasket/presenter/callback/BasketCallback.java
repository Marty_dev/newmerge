package kr.co.mergepoint.mergeclient.scene.shoppingbasket.presenter.callback;

import android.view.View;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseCallback;
import kr.co.mergepoint.mergeclient.application.common.CustomCountView;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.basket.Basket;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.model.BasketModel;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.view.BasketView;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.view.adapter.BasketListAdapter;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class BasketCallback extends BaseCallback<BasketView, BasketModel> implements BasketCallbackInterface {

    public BasketCallback(BasketView baseView, BasketModel baseModel) {
        super(baseView, baseModel);
    }

    @Override
    public MergeCallback<ResponseObject<Integer>> getDeleteMenuCallback(final View view) {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Integer> basketResult = response.body();
            if (response.isSuccessful() && basketResult != null && !basketResult.isFailed()) {
                // oid remove // TODO: 2018-07-19
                baseView.deleteMenu(view, basketResult.getObject());
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<Basket> getShoppingBasketCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            final Basket basket = response.body();
            if (response.isSuccessful() && basket != null)
                baseView.binding.setBasketAdapter(new BasketListAdapter(basket, baseView.activity));
        });
    }

    @Override
    public MergeCallback<ResponseObject<Integer>> getModifyShoppingBasketMenuCallback(final CustomCountView view, final int menuCount) {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Integer> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                if (responseObject.isFailed()) {
                    baseView.showAlert(responseObject.getMessage());
                    baseView.binding.getBasketAdapter().setMenuCount(view, menuCount);
                }
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }
}
