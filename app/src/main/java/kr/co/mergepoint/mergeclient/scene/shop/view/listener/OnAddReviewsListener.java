package kr.co.mergepoint.mergeclient.scene.shop.view.listener;

/**
 * Created by jgson on 2018. 1. 4..
 */

public interface OnAddReviewsListener {
    void addReviews(int page);
}
