package kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment;

import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.HistoryApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.DateModel;
import kr.co.mergepoint.mergeclient.application.common.FragmentLifeCycle;
import kr.co.mergepoint.mergeclient.databinding.UsingMenuBinding;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.scene.newcoupon.BarcodeGen;
import kr.co.mergepoint.mergeclient.scene.newcoupon.view.BarcodeResult;
import kr.co.mergepoint.mergeclient.scene.newcoupon.view.Barcode_detailActivity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter.UsingMenuAdapter;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.APPROVE_COMPLETE_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FCM_RECEIVER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USING_MENU_TAG;

/**
 * Created by 1017sjg on 2017. 10. 23..
 */

public class UsingFragmentv2 extends BaseFragment implements View.OnTouchListener {
    int rootlayout;
    private UsagePayment usagePayment;
    private UsingMenuBinding menuBinding;
    private boolean isApprove;
    BaseActivity activity;
    private HistoryApi historyApi;
    public Vibrator vibrator;
    BottomSheetBehavior bottom;
    MergeApplication mApp = MergeApplication.getMergeApplication();
    public boolean isApprove() {
        return isApprove;
    }

    public void setApprove(boolean approve) {
        isApprove = approve;
    }

    public int getOid() {
        return usagePayment != null ? usagePayment.oid : NO_VALUE;
    }

    public Map<String, Object> getApproveInfo() {
        //String pin = menuBinding.pin1.getText().toString() + menuBinding.pin2.getText().toString() + menuBinding.pin3.getText().toString() + menuBinding.pin4.getText().toString();
        Map<String, Object> info = new HashMap<>();
        info.put("oid", usagePayment.oid);
        info.put("checkPin",false);
        info.put("pin", "");

        return info;
    }

    public void resetPinCodeView() {
        menuBinding.pin1.getText().clear();
        menuBinding.pin2.getText().clear();
        menuBinding.pin3.getText().clear();
        menuBinding.pin4.getText().clear();
        menuBinding.pin1.requestFocus();
    }

    public static UsingFragmentv2 newInstance(BaseActivity lifeCycle, UsagePayment usagePayment,int rootlayout) {
        Bundle args = new Bundle();
        UsingFragmentv2 fragment = new UsingFragmentv2();
        fragment.lifeCycle = (FragmentLifeCycle)lifeCycle;
        fragment.usagePayment = usagePayment;
        fragment.setArguments(args);
        fragment.rootlayout = rootlayout;
        fragment.activity = lifeCycle;

        return fragment;
    }

    public static UsingFragmentv2 newInstance(UsagePayment usagePayment, BaseActivity activity,int rootlayout ) {
        Bundle args = new Bundle();
        UsingFragmentv2 fragment = new UsingFragmentv2();
        fragment.usagePayment = usagePayment;
        fragment.setArguments(args);
        fragment.activity = activity;
        fragment.rootlayout = rootlayout;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getActivity().registerReceiver(receiver, new IntentFilter(FCM_RECEIVER));

        menuBinding = DataBindingUtil.inflate(inflater, R.layout.using_menu, container, false);
        menuBinding.setTitle(getString(R.string.use_approve_title));
        menuBinding.setOnClick(this);
        menuBinding.setOnTouch(this);
        boolean isFran = usagePayment.approveType == 3;
        menuBinding.setUseApprove(usagePayment);
        menuBinding.setGuideline(isFran ? getString(R.string.use_guide_barcode) : getString(R.string.use_guide));
        menuBinding.setUseAdapter(new UsingMenuAdapter(usagePayment.usedMenuList));
        menuBinding.barcodeGuideline.setVisibility(isFran ? View.VISIBLE : View.GONE);
        menuBinding.useGuideline.setVisibility(isFran ? View.GONE :View.VISIBLE);
        menuBinding.enterprisePayNotice.setVisibility(isFran ? View.GONE :View.VISIBLE);
        menuBinding.enterpriseConfirmBtn.setVisibility(isFran? View.GONE :View.VISIBLE);
        menuBinding.usageBottomsheet.setVisibility(isFran? View.VISIBLE : View.GONE);
        timerHandler = new TimerHandler();


        if (isFran) {
            if (usagePayment.expireDateTime ==null)
                startTime = System.currentTimeMillis();
            else
                startTime = (new DateModel().getCalendarAllDate(usagePayment.expireDateTime)).getTime();

            menuBinding.barcodeStatetv.setText("바코드 열기");
            menuBinding.barcodeStatetv.setTextColor(Color.parseColor("#ffffff"));

            MDEBUG.debug("Fran Card Image  : " + usagePayment.cardImage);
            Picasso.with(activity).load(MergeApplication.getMergeApplication().getImageUrlFiltered(usagePayment.cardImage)).into(menuBinding.barcodeCardimg);
            menuBinding.barcodeLeftpricetv.setText(activity.MoneyForm(usagePayment.orderPrice) + "원");
            menuBinding.barcodeShopname.setText(usagePayment.shopName);
            menuBinding.barcodeCodetv.setText(usagePayment.barCode == null ? "A1234567890B" : usagePayment.barCode);
            menuBinding.barcodeImage.setImageBitmap(activity.BarcodeGenerate(usagePayment.barCode == null ? "A1234567890B" : usagePayment.barCode));
        }
        ///menuBinding.usageBottomSheet.setVisibility(usagePayment.approveType == 3 ? View.VISIBLE : View.GONE);
       // menuBinding.pinLayout.setVisibility(usagePayment.shopType > 1 ? View.INVISIBLE : View.VISIBLE);
      //  menuBinding.approveBtn.setVisibility(usagePayment.shopType > 1 ? View.INVISIBLE : View.VISIBLE);
      //  menuBinding.cancelBtn.setVisibility(usagePayment.shopType > 1 ? View.INVISIBLE : View.VISIBLE);
      //  menuBinding.enterpriseConfirmLl.setVisibility(usagePayment.shopType > 1 ? View.VISIBLE : View.GONE);
        menuBinding.enterprisePayNotice.setText(Html.fromHtml(getResources().getString(R.string.order_approval_warning_enterprize)));


        bottom = BottomSheetBehavior.from(menuBinding.usageBottomsheet);

        bottom.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    menuBinding.barcodeStatetv.setText("바코드 열기");
                    menuBinding.barcodeStatetv.setTextColor(Color.parseColor("#ffffff"));

                    menuBinding.barcodeStatetv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.bx_menu_collapse,0);
                }
                else if (newState == BottomSheetBehavior.STATE_EXPANDED){
                    menuBinding.barcodeStatetv.setText("바코드 닫기");
                    menuBinding.barcodeStatetv.setTextColor(Color.parseColor("#a5a5a5"));

                    menuBinding.barcodeStatetv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.bx_menu_expand,0);
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                float offSet = 1.0f - slideOffset;
                menuBinding.barcodeOpenimg.setAlpha(offSet);

            }
        });
        historyApi = MergeApplication.getMergeAppComponent().getRetrofit().create(HistoryApi.class);
        vibrator = (Vibrator)activity.getSystemService(Context.VIBRATOR_SERVICE);
        return menuBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        vibrator.vibrate(new long[]{1800,50},0);

        mApp.isBarcodeforPay = true;


        if (timerThread == null){
            timerThread = new TimerThread(timerHandler);
            timerThread.start();
        } else  if (!timerThread.istimerrun ){
            timerThread.isTerminate = true;
            timerThread = new TimerThread(timerHandler);
            timerThread.start();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        vibrator.cancel();
        timerThread.istimerrun = false;
        timerThread.isTerminate = true;
        mApp.isBarcodeforPay = false;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
    }
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view.getId() == menuBinding.pin1.getId() || view.getId() == menuBinding.pin2.getId() || view.getId() == menuBinding.pin3.getId() || view.getId() == menuBinding.pin4.getId())
            menuBinding.paymentScroll.post(() -> ObjectAnimator.ofInt(menuBinding.paymentScroll, "scrollY", menuBinding.paymentScroll.getBottom()).setDuration(300).start());
        view.performClick();
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.enterprise_confirm_btn:
            case R.id.approve_btn:
                MergeApplication.getMergeApplication().showLoading(rootActivity);
                approveMenu(approveMenu2(), getApproveInfo());
                break;
            /*뒤로가기 및 취소를 눌렀을 때 승인대기중인 메뉴 취소하기*/
            case R.id.cancel_btn:
            case R.id.back:
                activity.getSupportFragmentManager().beginTransaction().remove(this).commit();
                activity.getSupportFragmentManager().popBackStack();
                break;
            case R.id.barcode_bigbarcode:
            case R.id.barcode_codetv:
            case R.id.barcode_image:
                Intent inte = new Intent(activity,Barcode_detailActivity.class);
                inte.putExtra("barcode",usagePayment.barCode == null ? "A123456789B" : usagePayment.barCode);
                inte.putExtra("title",usagePayment.shopName);
                inte.putExtra("viewtype",2);
                inte.putExtra("price",usagePayment.orderPrice);

                startActivity(inte);
                break;
            case R.id.barcode_timer:
                MergeApplication.getMergeApplication().showLoading(rootActivity);
                timerThread.istimerrun = false;
                timerThread.isTerminate = true;
                historyApi.refreshBarcode(usagePayment.oid).enqueue(refrshBarcode());
                // TODO: 2018-10-08   바코드 리셋
        }
    }
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getStringExtra("action") == null){
                return;
            }
            onNewIntent(intent);
        }
    };

    public void onNewIntent(Intent data){

        String action = data.getStringExtra("action");
        if (action != null) {
            if (action.equals("22")) {

                Intent inte = new Intent(activity,BarcodeResult.class);
                startActivity(inte);
            } else if (action.equals("24")) {
                new MergeDialog.Builder(activity).setConfirmString("이용내역 가기")
                        .setContent("금액이 일치하지 않습니다.\n다시 한번 금액을 확인해 주세요.")
                        .setConfirmClick(() -> {
                            Intent inte = new Intent(activity,UsageActivity.class);
                            startActivity(inte);
                            activity.finish();


                        }).build().show();
            }
        }
    }


    private Callback<ResponseObject<UsagePayment>> refrshBarcode(){
        return new Callback<ResponseObject<UsagePayment>>() {
            @Override
            public void onResponse(Call<ResponseObject<UsagePayment>> call, Response<ResponseObject<UsagePayment>> response) {
                MergeApplication.getMergeApplication().hideLoading(rootActivity);

                if (response.isSuccessful()){
                    if (!response.body().isFailed()){
                        startTime = (new DateModel().getCalendarAllDate(response.body().getObject().expireDateTime)).getTime();
                        menuBinding.barcodeImage.setImageBitmap(activity.BarcodeGenerate(response.body().getObject().barCode));
                        menuBinding.barcodeCodetv.setText(response.body().getObject().barCode);
                        usagePayment = response.body().getObject();

                        if (!timerThread.istimerrun){
                            timerThread.isTerminate = true;
                            timerThread = new TimerThread(timerHandler);
                            menuBinding.barcodeTimer.setText("유효시간 " + getString(R.string.timer, 10, 00));
                            timerThread.start();

                        }
                    }else{
                        showAlert(response.body().getMessage());
                    }
                }else{
                    showAlert(R.string.retry);
                }
            }

            @Override
            public void onFailure(Call<ResponseObject<UsagePayment>> call, Throwable t) {
                MergeApplication.getMergeApplication().hideLoading(rootActivity);

            }
        };
    }
    public Callback<ResponseObject<Integer>> approveMenu2()
    {
        return new Callback<ResponseObject<Integer>>() {
            @Override
            public void onResponse(Call<ResponseObject<Integer>> call, Response<ResponseObject<Integer>> response) {
                MergeApplication.getMergeApplication().hideLoading(rootActivity);
                if (!response.body().isFailed())
                {
                    vibrator.cancel();
                    vibrator.vibrate(1000);
                    ResultFragmentv2 fragment = ResultFragmentv2.newInstance(getString(R.string.use_approve_complete_title), getString(R.string.use_approve_complete_content));
                    activity.getSupportFragmentManager().beginTransaction()
                            .replace(rootlayout, fragment, APPROVE_COMPLETE_TAG)
                            .addToBackStack(APPROVE_COMPLETE_TAG).commit();
                }
            }

            @Override
            public void onFailure(Call<ResponseObject<Integer>> call, Throwable t) {
                MergeApplication.getMergeApplication().hideLoading(rootActivity);

            }
        };
    }


    public void approveMenu(Callback<ResponseObject<Integer>> callback, Map<String, Object> info) {
        int oid = (int) info.get("oid");
        String pin = (String) info.get("pin");
        boolean checkPin = (boolean) info.get("checkPin");
        MDEBUG.debug("oid is  : " + oid);

        historyApi.approveMenu(oid,checkPin).enqueue(callback);

    }


    /*  public MergeCallback<ResponseObject<Integer>> approveMenu2() {
        return new MergeCallback<>(baseView, new MergeCallback.MergeCallWithFailure<ResponseObject<Integer>>() {
            @Override
            public void onResponse(Call<ResponseObject<Integer>> call, Response<ResponseObject<Integer>> response) {
                ResponseObject<Integer> responseObject = response.body();
                baseView.hideLoading();
                if (response.isSuccessful() && responseObject != null) {
                    if (!responseObject.isFailed()) {
                        baseView.openApproveResultFragment();
                    } else {
                        MDEBUG.debug("getmessage : " + responseObject.getMessage());
                        baseView.setApproveFlag(false);
                        baseView.showAlert(responseObject.getMessage());
                        if (Objects.equals(responseObject.getCode(), "USE010"))
                            baseView.resetPinCodeView();
                    }
                } else {
                    baseView.setApproveFlag(false);
                    baseView.showAlert(R.string.retry);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseObject<Integer>> call, @NonNull Throwable t) {
                baseView.hideLoading();

                baseView.setApproveFlag(false);
                baseView.showAlert(R.string.retry);
            }
        });
    }
*/

    public TimerHandler timerHandler;
    public TimerThread timerThread;
    public long startTime;
    class TimerThread extends Thread{
        public boolean  isTerminate = false;
        private TimerHandler  handler;
        public boolean istimerrun = true;

        public TimerThread(TimerHandler handler) {
            this.handler = handler;
            istimerrun = true;
            isTerminate = false;
        }

        @Override
        public void run() {
            super.run();

            while (istimerrun) {
                long millis = (startTime - 1000) - System.currentTimeMillis();
                int seconds = (int) (millis / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                Message msg = new Message();
                msg.arg1 = minutes;
                msg.arg2 = seconds;
                msg.obj = istimerrun;

                if (minutes <= 0 && seconds <= 0) {
                    istimerrun = false;
                } else {
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        showAlert("타이머 세팅 실패");
                        istimerrun = false;
                        break;
                    }
                }

                if (isTerminate)
                    return;
                handler.sendMessage(msg);
            }
        }
    }
    class TimerHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int minutes = msg.arg1;
            int seconds = msg.arg2;
            if ((boolean) msg.obj)
                menuBinding.barcodeTimer.setText("유효시간 " + getString(R.string.timer, minutes, seconds));

        }
    }
}

