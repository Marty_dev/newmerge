package kr.co.mergepoint.mergeclient.application.common.mvvm.dagger;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.application.common.mvvm.AttachedActivity;
import kr.co.mergepoint.mergeclient.application.common.mvvm.AttachedViewModelActivity;
import kr.co.mergepoint.mergeclient.application.common.mvvm.ViewModelActivity;

/**
 * Created by jgson on 2017. 5. 31..
 */

@Module
public class ActivityModule {

    private ViewModelActivity activity;

    public ActivityModule(ViewModelActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    AttachedActivity provideAttachedActivity() {
        return new AttachedViewModelActivity(activity);
    }
}
