package kr.co.mergepoint.mergeclient.scene.cardmanagement;

import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;

import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.script.WebReturnScriptListener;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.dagger.CardModule;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.dagger.DaggerCardComponent;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.presenter.CardManagementPresenter;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public class CardManagementActivity extends BaseActivity implements WebReturnScriptListener{

    @Inject CardManagementPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerCardComponent.builder().cardModule(new CardModule(this)).build().inject(this);
        presenter.onCreate();
    }

    @Override
    protected void onActivityClick(View view) {
        presenter.onClick(view);
    }

    @Override
    public void success() {
        presenter.easyPayCardRegistResult(true, "");
    }

    @Override
    public void failure(String error) {
        presenter.easyPayCardRegistResult(false, error);
    }
}
