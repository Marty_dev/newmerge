package kr.co.mergepoint.mergeclient.scene.data.shoplist;

import android.databinding.ObservableArrayList;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 6. 13..
 */

public class ShopList {
    public ObservableArrayList<Shop> shops = new ObservableArrayList<>();

    public ShopList() {}

    public void addAll(ArrayList<Shop> shopArrayList) {
        shops.addAll(shopArrayList);
    }

    private void add(Shop shop) {
        shops.add(shop);
    }

    public void removeAll() {
        shops.clear();
    }

    public void remove(int index) {
        shops.remove(index);
    }

    public Shop get(int i) {
        return shops.get(i);
    }
}
