package kr.co.mergepoint.mergeclient.scene.data.main;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 8. 18..
 */

public class Point {
    @SerializedName("oid")
    public int oid;

    @SerializedName("shopName")
    public String shopName;

    @SerializedName("memberRef")
    public int memberRef;

    @SerializedName("paymentRef")
    public int paymentRef;

    @SerializedName("shopRef")
    public int shopRef;

    @SerializedName("type")
    public int type;

    @SerializedName("totalPoints")
    public int totalPoints;

    @SerializedName("creDateTime")
    public ArrayList<Integer> creDateTime;
}
