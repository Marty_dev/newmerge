package kr.co.mergepoint.mergeclient.scene.shop.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;
import kr.co.mergepoint.mergeclient.scene.shoplist.ShopListActivity;

/**
 * Created by jgson on 2017. 6. 5..
 */

@ShopScope
@Component(modules = ShopModule.class)
public interface ShopComponent {
    void inject(ShopActivity activity);
}
