package kr.co.mergepoint.mergeclient.application.common.mvvm.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.application.common.mvvm.ViewModel;

/**
 * Created by jgson on 2017. 5. 31..
 */

@ActivityScope
@Component(modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(ViewModel viewModel);
}
