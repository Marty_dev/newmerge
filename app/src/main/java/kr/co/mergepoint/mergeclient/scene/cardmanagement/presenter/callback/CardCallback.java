package kr.co.mergepoint.mergeclient.scene.cardmanagement.presenter.callback;

import kr.co.mergepoint.mergeclient.application.common.BaseCallback;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.model.CardManagementModel;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.view.CardManagementView;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class CardCallback extends BaseCallback<CardManagementView, CardManagementModel> implements CardCallbackInterface {

    public CardCallback(CardManagementView baseView, CardManagementModel baseModel) {
        super(baseView, baseModel);
    }
}
