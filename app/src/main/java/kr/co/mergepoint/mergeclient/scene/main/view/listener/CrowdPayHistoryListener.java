package kr.co.mergepoint.mergeclient.scene.main.view.listener;

import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPayItem;

public interface CrowdPayHistoryListener {
    void changeCrowdPayHistory(int type, int period);
    void crowdPayHistoryDetail(CrowdPayItem crowdPayItem);
}
