package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.SpinnerAdapter;
import kr.co.mergepoint.mergeclient.databinding.EnterprisePointHistoryBinding;
import kr.co.mergepoint.mergeclient.scene.data.history.CompanyPointHistory;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.EnterprisePointAdapter;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.EnterprisePointListener;

import static kr.co.mergepoint.mergeclient.application.common.Properties.CENTER;

/**
 * Created by jgson on 2017. 7. 18..
 */

public class EnterprisePointFragment extends BaseFragment implements AdapterView.OnItemSelectedListener, TabLayout.OnTabSelectedListener {

    public int type = 1;
    public int period = 1;

    private EnterprisePointListener listener;
    private EnterprisePointHistoryBinding pointBinding;
    private ArrayList<CompanyPointHistory> companyPointHistories;
    private String userEnterprisePoint;
    private EnterprisePointAdapter adapter;

    public static EnterprisePointFragment newInstance(EnterprisePointListener listener, ArrayList<CompanyPointHistory> companyPointHistories, String userEnterprisePoint) {
        Bundle args = new Bundle();
        EnterprisePointFragment fragment = new EnterprisePointFragment();
        fragment.listener = listener;
        fragment.companyPointHistories = companyPointHistories;
        fragment.userEnterprisePoint = userEnterprisePoint;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        pointBinding = DataBindingUtil.inflate(inflater, R.layout.enterprise_point_history, container, false);
        pointBinding.setTitle("기업포인트 내역");
        pointBinding.setOnClick(this);
        pointBinding.setTabListener(this);
        pointBinding.setSpinnerItemListener(EnterprisePointFragment.this);
        pointBinding.setSpinnerAdapter(new SpinnerAdapter(getResources().getStringArray(R.array.term_spinner), CENTER));
        pointBinding.setTabArray(new String[]{"전체 포인트", "지급 포인트", "사용/회수 포인트"});
        pointBinding.userEnterprisePoint.setText(userEnterprisePoint != null ? userEnterprisePoint : "0P");

        adapter = new EnterprisePointAdapter(companyPointHistories);
        pointBinding.setEnterprisePointAdapter(adapter);

        return pointBinding.getRoot();
    }

    // 포인트 기간 설정에 대한 리스
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        period = 0 == position ? 1 : (3 == position ? 3 : 6);
        listener.changeEnterprisePointHistory(type, period);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}

    // 포인트 상단의 탭 리스너
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        type = tab.getPosition() + 1;
        listener.changeEnterprisePointHistory(type, period);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}
    @Override
    public void onTabReselected(TabLayout.Tab tab) {}

    public void changePointHistory(ArrayList<CompanyPointHistory> companyPointHistories) {
        this.companyPointHistories = companyPointHistories;
        if (pointBinding != null){
            adapter.setParams(companyPointHistories);
            pointBinding.setEnterprisePointAdapter(adapter);
        }
    }
}
