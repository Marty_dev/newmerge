package kr.co.mergepoint.mergeclient.scene.shoplist.presenter.callback;

import android.util.Log;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.application.common.BaseCallback;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.shoplist.model.ShopListModel;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.ShopListView;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class ShopListCallback extends BaseCallback<ShopListView, ShopListModel> implements ShopListCallbackInterface {

    public ShopListCallback(ShopListView baseView, ShopListModel baseModel) {
        super(baseView, baseModel);
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<Shop>>> searchingMapCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<Shop>> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed() && responseObject.getObject() != null && responseObject.getObject().size() > 0) {
                baseView.addPOIItems(responseObject.getObject());
                // TODO: 2018-09-05  Map Listener 
            } else {
                Log.d("ShopListCallback", "failed");
            }
        });
    }

    public MergeCallback<ResponseObject<ArrayList<Shop>>> searchingMapCallbackinSearch() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<Shop>> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed() && responseObject.getObject() != null && responseObject.getObject().size() > 0) {
                baseView.addPOIItemsPostSearch(responseObject.getObject());
                MDEBUG.debug("Map Response Size  : " + response.body().getObject().size());
                // TODO: 2018-09-05  Map Listener
            } else {
                Log.d("ShopListCallback", "failed");
            }
        });
    }
}
