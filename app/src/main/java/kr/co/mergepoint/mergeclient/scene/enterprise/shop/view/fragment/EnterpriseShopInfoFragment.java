package kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.EnterpriseShopDetailInfoBinding;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.shop.view.ShopMapView;

/**
 * Created by 1017sjg on 2017. 6. 23..
 */

public class EnterpriseShopInfoFragment extends BaseFragment {

    private ShopDetail detail;
    private EnterpriseShopDetailInfoBinding shopDetailInfoBinding;

    public void setDetail(ShopDetail detail) {
        this.detail = detail;
        addMapView(detail);
    }

    public static EnterpriseShopInfoFragment newInstance() {
        Bundle args = new Bundle();
        EnterpriseShopInfoFragment fragment = new EnterpriseShopInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        shopDetailInfoBinding = DataBindingUtil.inflate(inflater, R.layout.enterprise_shop_detail_info, container, false);
        if (detail != null)
            addMapView(detail);
        return shopDetailInfoBinding.getRoot();
    }

    private void addMapView(ShopDetail detail) {
        ShopMapView mapView = new ShopMapView(getRootActivity());
        mapView.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        MapPOIItem customMarker = new MapPOIItem();
        MapPoint mapPoint = MapPoint.mapPointWithGeoCoord(detail.latitude, detail.longitude);
        customMarker.setItemName(detail.shopName);
        customMarker.setMapPoint(mapPoint);
        customMarker.setMarkerType(MapPOIItem.MarkerType.CustomImage);
        customMarker.setCustomImageResourceId(R.drawable.obj_position);
        customMarker.setCustomImageAutoscale(false);
        customMarker.setCustomImageAnchor(0.5f, 1.0f);
        mapView.addPOIItem(customMarker);
        mapView.setMapCenterPoint(mapPoint, false);
        shopDetailInfoBinding.setShopDetail(detail);
        shopDetailInfoBinding.setOnClick(getRootActivity());
        shopDetailInfoBinding.shopMapLayout.addView(mapView, 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (shopDetailInfoBinding.shopMapLayout.getChildCount() == 1 && detail != null)
            addMapView(detail);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (shopDetailInfoBinding != null)
            shopDetailInfoBinding.shopMapLayout.removeViewAt(0);
    }
}
