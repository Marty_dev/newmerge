package kr.co.mergepoint.mergeclient.scene.shop.view;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextSwitcher;

import com.facebook.CallbackManager;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.scene.data.basket.AddBasket;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.PreExperience;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.GoogleSearchInfo;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.MergeReview;
import kr.co.mergepoint.mergeclient.scene.shop.view.fragment.ShopMenuFragment;

/**
 * Created by jgson on 2017. 12. 8..
 */

public interface ShopViewInterface {

    /*페이지 열기*/
    void openPayment();

    void openShopShared(ShopDetail shopDetail);

    void openMergesPickDetail(Bundle bundle);

    void openPreExperience(ArrayList<PreExperience> preExperiences, String shopName);

    void openMenuDetail(ShopMenuDetail detail);

    void openPaymentResult();

    void openShoppingBasket();

    void openMapIntent(double latitude, double longitude);

    void openReviewWeb(Bundle bundle);

    /*GETTER or SETTER*/
    AddBasket getSelectOptions() ;

    ShopMenuFragment getShopMenuFragment();

    CallbackManager getShareFacebookCallback();

    boolean startShoppingBasket();

    void setReviewAdapter(MergeReview mergeReview);

    /*ANIMATION*/
    void animBasket();

    @Properties.LikeState
    int animateLike();

    void createAnimSet(final ImageView heart, final TextSwitcher heartNum);

    ObjectAnimator createAnim(View view, String property, float start, float end, int duration, TimeInterpolator interpolator);

    void setupLikeTextSwitcher();

    /*ETC*/
    void callShopPhone();

    void onBackPressed();
}
