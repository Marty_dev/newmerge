package kr.co.mergepoint.mergeclient.scene.shoppingbasket.view.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.application.common.CustomCountView;
import kr.co.mergepoint.mergeclient.databinding.ShoppingBasketMenuItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.basket.BasketInShop;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.BasketActivity;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

/**
 * Created by 1017sjg on 2017. 7. 3..
 */

public class ShopInMenuAdapter extends BasicListAdapter<BasicListHolder<ShoppingBasketMenuItemBinding, ShopMenuDetail>, ShopMenuDetail> {

    private BasketActivity activity;
    private String shopName;
    private BasketListAdapter basketListAdapter;

    ShopInMenuAdapter(BasketListAdapter basketListAdapter, BasketInShop basketInShop, BasketActivity activity) {
        super(basketInShop.menu);
        this.activity = activity;
        this.basketListAdapter = basketListAdapter;
        this.shopName = basketInShop.shopName;
    }

    @Override
    public BasicListHolder<ShoppingBasketMenuItemBinding, ShopMenuDetail> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_basket_menu_item, parent, false);
        ShoppingBasketMenuItemBinding binding = DataBindingUtil.bind(view);

        return new BasicListHolder<ShoppingBasketMenuItemBinding, ShopMenuDetail>(binding) {
            @Override
            public void setDataBindingWithData(ShopMenuDetail data) {
                getDataBinding().setMenuCount(data.count);
                getDataBinding().setMenuDetail(data);
                getDataBinding().setShopName(shopName);
                getDataBinding().setOnClick(activity);
                getDataBinding().setCountChangeListener(activity);
                getDataBinding().setTouchListener(activity);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<ShoppingBasketMenuItemBinding, ShopMenuDetail> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }

    boolean deleteItem(View view) {
        int arraysize = getObservableArrayList().size();
        ShopMenuDetail menuDetail = getShopMenuDetail(view);
        if (menuDetail != null) {
            int position = getObservableArrayList().indexOf(menuDetail);
            if (position != NO_VALUE) {
                getObservableArrayList().remove(position);
                notifyItemRemoved(position);

            }
        }

        //return getObservableArrayList().size() == (arraysize - 1);
        return getObservableArrayList().size() == 0;
    }
    boolean getCheckable(View view) {
        int arraysize = getObservableArrayList().size();
        ShopMenuDetail menuDetail = getShopMenuDetail(view);
        if (menuDetail != null) {
            int position = getObservableArrayList().indexOf(menuDetail);
            if (position != NO_VALUE) {
                return menuDetail.isCheck;

            }
        }

        //return getObservableArrayList().size() == (arraysize - 1);
        return false;
    }


    // View로 ShopMenuDetail를 서치
    ShopMenuDetail getShopMenuDetail(View view) {
        ShoppingBasketMenuItemBinding itemBinding = getItemBinding(view);
        return itemBinding != null ? itemBinding.getMenuDetail() : null;
    }

    CustomCountView getCountView(View view) {
        ShoppingBasketMenuItemBinding itemBinding = getItemBinding(view);
        return itemBinding != null ? itemBinding.countLayout.countView : null;
    }

    void setMenuCount(View view, int count) {
        ShoppingBasketMenuItemBinding itemBinding = getItemBinding(view);
        if (itemBinding != null && itemBinding.getMenuDetail() != null) {
            if ( itemBinding.getMenuDetail().isCheck) {
                basketListAdapter.substractPrice(itemBinding.getMenuDetail().payPrice * itemBinding.getMenuDetail().getCount());
                basketListAdapter.addPrice(itemBinding.getMenuDetail().payPrice * count);
            }
            itemBinding.getMenuDetail().setCount(count);
        }
    }

    private ShoppingBasketMenuItemBinding getItemBinding(View view) {
        ViewDataBinding viewDataBinding = getViewDataBinding(view);
        return viewDataBinding instanceof ShoppingBasketMenuItemBinding ? (ShoppingBasketMenuItemBinding) viewDataBinding : null;
    }
}
