package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.MyCouponListFooterBinding;
import kr.co.mergepoint.mergeclient.databinding.MyCouponListHeaderBinding;
import kr.co.mergepoint.mergeclient.databinding.MyTicketListItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.Ticket;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_FOOTER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_HEADER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_ITEM;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class TicketAdapter extends BasicListAdapter<BasicListHolder, Ticket> {

    private Resources resources;
    private int totalTicketCount;

    public TicketAdapter(ArrayList<Ticket> arrayList) {
        super(arrayList);
        resources = MergeApplication.getMergeAppComponent().getUtility().getResuorces();
        for (Ticket ticket :arrayList) {
            totalTicketCount+=ticket.remainTicketCount;
        }
    }

    @Override
    public BasicListHolder setCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == LIST_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_ticket_list_item, parent, false);
            MyTicketListItemBinding ticketListItemBinding = DataBindingUtil.bind(view);

            return new BasicListHolder<MyTicketListItemBinding, Ticket>(ticketListItemBinding) {
                @Override
                public void setDataBindingWithData(Ticket data) {
                    getDataBinding().setTicket(data);
                    MDEBUG.debug("ticket data day" + data.getCompareDay());


                }
            };
        } else if (viewType == LIST_TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_coupon_list_header, parent, false);
            MyCouponListHeaderBinding mycouponListHeaderBinding = DataBindingUtil.bind(view);

            return new BasicListHolder<MyCouponListHeaderBinding, Integer>(mycouponListHeaderBinding) {
                @Override
                public void setDataBindingWithData(Integer data) {
                    getDataBinding().setCouponString(fromHtml(resources.getString(R.string.available_tickets, data)));
                }
            };
        } else if (viewType == LIST_TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_coupon_list_footer, parent, false);
            MyCouponListFooterBinding mycouponListFooterBinding = DataBindingUtil.bind(view);

            return new BasicListHolder<MyCouponListFooterBinding, Map<String, String>>(mycouponListFooterBinding) {
                @Override
                public void setDataBindingWithData(Map<String, String> data) {}
            };
        }

        return null;
    }

    private Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
            return Html.fromHtml(source);
        return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setBindViewHolder(BasicListHolder holder, int position) {
        if (position == 0) {
            holder.bind(totalTicketCount);
        } else if (position > 0 && position < getItemCount() - 1) {
            holder.bind(getObservableArrayList().get(position - 1));
        } else if (position == getItemCount() - 1) {
            Map<String, String> map = new HashMap<>();
            map.put("title", resources.getString(R.string.ticket_footer_title));
            map.put("content", resources.getString(R.string.ticket_footer_content));
            holder.bind(map);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return LIST_TYPE_HEADER;
        } else if (getItemCount() - 1 == position) {
            return LIST_TYPE_FOOTER;
        } else {
            return LIST_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + 2;
    }
}
