package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.FavoriteCrowdUserItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class FavoriteCrowedUserAdapter extends BasicListAdapter<BasicListHolder<FavoriteCrowdUserItemBinding, CrowdMember>, CrowdMember> {

    private View.OnClickListener onClickListener;

    public FavoriteCrowedUserAdapter(ArrayList<CrowdMember> arrayList, View.OnClickListener onClickListener) {
        super(arrayList);
        this.onClickListener = onClickListener;
    }

    @Override
    public BasicListHolder<FavoriteCrowdUserItemBinding, CrowdMember> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorite_crowd_user_item, parent, false);
        FavoriteCrowdUserItemBinding crowdUserBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<FavoriteCrowdUserItemBinding, CrowdMember>(crowdUserBinding) {
            @Override
            public void setDataBindingWithData(CrowdMember data) {
                getDataBinding().setUser(data);
                getDataBinding().setOnClick(onClickListener);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<FavoriteCrowdUserItemBinding, CrowdMember> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
