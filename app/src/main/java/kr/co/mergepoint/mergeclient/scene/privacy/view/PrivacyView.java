package kr.co.mergepoint.mergeclient.scene.privacy.view;

import android.app.DatePickerDialog;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.Calendar;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.application.common.CustomDatePickerDialog;
import kr.co.mergepoint.mergeclient.application.common.script.PostScriptInterface;
import kr.co.mergepoint.mergeclient.databinding.PrivacyBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.privacy.view.fragment.SearchEnterpriseFragment;
import kr.co.mergepoint.mergeclient.scene.privacy.PrivacyActivity;
import kr.co.mergepoint.mergeclient.scene.privacy.presenter.PrivacyPresenter;
import kr.co.mergepoint.mergeclient.scene.privacy.view.fragment.ModifyPrivacyFragment;
import kr.co.mergepoint.mergeclient.scene.privacy.view.fragment.SimplePrivacyFragment;
import kr.co.mergepoint.mergeclient.scene.register.view.fragment.PostcodeView;

import static kr.co.mergepoint.mergeclient.application.common.Properties.MODIFY_PRIVACY_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.POSTCODE_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.REGISTER_POST_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SEARCH_ENTERPRISE_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SIMPLE_PRIVACY_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UP_DOWN;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class PrivacyView extends BaseView<PrivacyActivity, PrivacyPresenter, PrivacyBinding> {

    public PrivacyView(PrivacyActivity activity, int layout) {
        super(activity, layout);
    }

    @Override
    protected Toolbar setToolbar() {
        return binding.toolbarBinding.basicToolbar;
    }

    public String getUserPassword() {
        return binding.inputPassword.getText().toString().trim();
    }

    public boolean emptyUserPw() {
        boolean isPw = binding.inputPassword.getText().toString().trim().length() > 0;
        if (isPw) {
            showLoading();
        } else {
            showAlert(R.string.input_pw);
        }
        return isPw;
    }

    public void openSearchEnterprise() {
        openFragment(RIGHT_LEFT, R.id.confirm_password_layout, SearchEnterpriseFragment.newInstance(activity), SEARCH_ENTERPRISE_FRAGMENT_TAG);
    }

    public void openSimplePrivacy(Member member) {
        openFragment(RIGHT_LEFT, R.id.confirm_password_layout, SimplePrivacyFragment.newInstance(member), SIMPLE_PRIVACY_TAG);
    }

    public void shopEditPrivacy(Member member) {
        openFragment(RIGHT_LEFT, R.id.confirm_password_layout, ModifyPrivacyFragment.newInstance(member), MODIFY_PRIVACY_TAG);
    }

    public void showSearchPost() {
        openAddFragment(UP_DOWN, R.id.confirm_password_layout, PostcodeView.newInstance(new PostScriptInterface(activity), POSTCODE_URL), REGISTER_POST_TAG);
    }

    public void showCalendar(ArrayList<Integer> birth, DatePickerDialog.OnDateSetListener setListener) {
        Calendar calendar = Calendar.getInstance();
        int year = birth == null ? calendar.get(Calendar.YEAR) : birth.get(0);
        int month = birth == null ? calendar.get(Calendar.MONTH) : birth.get(1) - 1;
        int day = birth == null ? calendar.get(Calendar.DAY_OF_MONTH) : birth.get(2);
        CustomDatePickerDialog datePickerDialog = new CustomDatePickerDialog(activity, R.style.RegisterBirthSpinner, setListener, year, month, day);
        datePickerDialog.show();
    }

    public void askModifyInfo(MergeDialog.OnDialogClick onClickListener, Member member) {
        ModifyPrivacyFragment modifyPrivacyFragment = (ModifyPrivacyFragment) findFragmentByTag(MODIFY_PRIVACY_TAG);
        modifyPrivacyFragment.setModifyMember();
        if (modifyPrivacyFragment.isCheckPw()) {
            if (member.getName().trim().isEmpty() && member.getName().length() == 0) {
                new MergeDialog.Builder(activity).setContent(R.string.modify_necessary_name).setCancelBtn(false).build().show();
            } else if (member.getGender() == NO_VALUE) {
                new MergeDialog.Builder(activity).setContent(R.string.modify_necessary_gender).setCancelBtn(false).build().show();
            } else {
                new MergeDialog.Builder(activity).setContent(R.string.modify_ask_me_again).setConfirmClick(onClickListener).build().show();
            }
        } else {
            showAlert(getString(R.string.confirm_pw));
        }
    }

    public void closePostcode() {
        removeFragment(findFragmentByTag(REGISTER_POST_TAG));
    }
}
