package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.Map;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.ResetPwBinding;

import static kr.co.mergepoint.mergeclient.application.common.Properties.OID;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USER_PATH;

/**
 * Created by 1017sjg on 2017. 10. 18..
 */

public class ResetPwFragment extends BaseFragment {

    private ResetPwBinding resetPwBinding;
    private int oid;
    private String userPath;

    public static ResetPwFragment newInstance(Bundle bundle) {
        ResetPwFragment fragment = new ResetPwFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        oid = getArguments().getInt(OID);
        userPath = getArguments().getString(USER_PATH);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        resetPwBinding = DataBindingUtil.inflate(inflater, R.layout.reset_pw, container, false);
        resetPwBinding.setTitle(getString(R.string.reset_pw_title));
        resetPwBinding.setOnClick(this);
        return resetPwBinding.getRoot();
    }

    public String getPasswordText() {
        return resetPwBinding != null ? resetPwBinding.userPw.getText().toString().trim() : "";
    }

    public String getPasswordConfirmText() {
        return resetPwBinding != null ? resetPwBinding.userPwConfirm.getText().toString().trim() : "";
    }

    public Map<String, Object> getPassword() {
        Map<String, Object> resetPwMap = new HashMap<>();
        resetPwMap.put("oid", oid);
        resetPwMap.put("userPath", userPath);
        resetPwMap.put("password", resetPwBinding.userPwConfirm.getText().toString().trim());
        return resetPwMap;
    }
}
