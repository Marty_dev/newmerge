package kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter;

import android.view.View;

import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.FilterItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.SelectItem;

/**
 * Created by 1017sjg on 2017. 9. 7..
 */

public class FilterHolder extends BasicListHolder<FilterItemBinding, SelectItem> {

    private View.OnClickListener onClickListener;
    private OnItemSelectedListener onItemSelectedListener;

    public interface OnItemSelectedListener {
        void onItemSelected(SelectItem item);
    }

    public FilterHolder(FilterItemBinding databinding, OnItemSelectedListener selectedListener) {
        super(databinding);
        this.onItemSelectedListener = selectedListener;
        this.onClickListener = view -> {
            getData().setSelected(!getData().isSelected());
            onItemSelectedListener.onItemSelected(getData());
        };
    }

    @Override
    public void setDataBindingWithData(SelectItem data) {
        setData(data);
        getDataBinding().setTitle(data.getName());
        getDataBinding().setOnClick(onClickListener);
    }
}
