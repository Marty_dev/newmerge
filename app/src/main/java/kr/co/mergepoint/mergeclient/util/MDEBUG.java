package kr.co.mergepoint.mergeclient.util;

import android.util.Log;

import kr.co.mergepoint.mergeclient.BuildConfig;

public class MDEBUG {
    public static void debug (String msg){
        if (
  BuildConfig.DEBUG
//true
        //false
                )
            Log.d("<Debug>>>>",buildLogMsg(msg));
    }

    public static String buildLogMsg(String message) {

        StackTraceElement ste = Thread.currentThread().getStackTrace()[4];

        StringBuilder sb = new StringBuilder();

        sb.append("[");
        sb.append(ste.getFileName().replace(".java", ""));
        sb.append("::");
        sb.append(ste.getMethodName());
        sb.append("]");
        sb.append(message);

        return sb.toString();

    }
}
