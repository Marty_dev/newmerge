package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.PointListItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.Point;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class PointAdapter extends BasicListAdapter<BasicListHolder<PointListItemBinding, Point>, Point> {

    public PointAdapter(ArrayList<Point> arrayList) {
        super(arrayList);
    }
    public PointAdapter(){}

    public void setParams(ArrayList<Point> arrayList){
        if (arrayList == null)
            return;
        getObservableArrayList().clear();
        getObservableArrayList().addAll(arrayList);

    }
    @Override
    public BasicListHolder<PointListItemBinding, Point> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.point_list_item, parent, false);
        PointListItemBinding pointListItemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<PointListItemBinding, Point>(pointListItemBinding) {
            @Override
            public void setDataBindingWithData(Point data) {
                getDataBinding().setPoint(data);
                getDataBinding().setType(data.type != 1);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<PointListItemBinding, Point> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
