package kr.co.mergepoint.mergeclient.scene.shop.view.listener;

import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionMenu;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionSelectMenu;

/**
 * Created by 1017sjg on 2017. 6. 26..
 */

public interface OnOptionCheckListener {
    void onMultiCheck(boolean checked, ShopOptionSelectMenu selectMenu, int dispayCount, int optionKind);
    void onSingleRadio(ShopOptionMenu optionMenu, boolean checked, ShopOptionSelectMenu preSelectMenu, ShopOptionSelectMenu touchSelectMenu, int optionKind,int count);
    void onOptionCount(boolean isPlus, ShopOptionSelectMenu selectMenu, int optionKind);
}
