package kr.co.mergepoint.mergeclient.application.common;

import android.databinding.ObservableArrayList;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

/**
 * Created by 1017sjg on 2017. 6. 30..
 */

public abstract class BasicListAdapter<T extends BasicListHolder, S> extends RecyclerView.Adapter<T> {

    private ObservableArrayList<S> observableArrayList;
    private RecyclerView recyclerView;

    public void setArrayList(ObservableArrayList<S> list){
        observableArrayList.clear();
        observableArrayList.addAll(list);

    }

    public BasicListAdapter() {
        observableArrayList = new ObservableArrayList<>();
    }

    public BasicListAdapter(ArrayList<S> arrayList) {
        observableArrayList = new ObservableArrayList<>();
        if (arrayList != null && arrayList.size() > 0)
            observableArrayList.addAll(arrayList);
    }

    protected void changeListData(ArrayList<S> arrayList) {
        observableArrayList.clear();
        if (arrayList != null)
            observableArrayList.addAll(arrayList);

    }

    public void setListData(ArrayList<S> arrayList) {
        changeListData(arrayList);

        notifyDataSetChanged();
        //notifyItemRangeChanged(0, arrayList.size() - 1);
    }

    public void setFirstInserted(ArrayList<S> arrayList) {
        int removeSize = getObservableArrayList().size();
        changeListData(arrayList);
        notifyItemRangeRemoved(0, removeSize);
        if (arrayList != null)
            notifyItemRangeInserted(0, arrayList.size());
    }

    public void addData(ArrayList<S> arrayList) {
        int currentIndex = observableArrayList.size();
        observableArrayList.addAll(arrayList);
        notifyItemRangeInserted(currentIndex, observableArrayList.size() - 1);
    }

    public void clear() {
        int size = getObservableArrayList().size();
        getObservableArrayList().clear();
        notifyItemRangeRemoved(0, size);
    }

    protected void addListData(ArrayList<S> arrayList) {
        observableArrayList.addAll(arrayList);
    }

    protected RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public T onCreateViewHolder(ViewGroup parent, int viewType) {
        return setCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(T holder, int position) {
        setBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return observableArrayList.size();
    }

    public ObservableArrayList<S> getObservableArrayList() {
        return observableArrayList;
    }

    public int getPosition(View v) {
        RecyclerView.ViewHolder holder = getViewHolder(v);
        return holder != null ? holder.getAdapterPosition() : NO_VALUE;
    }

    public ViewDataBinding getViewDataBinding(View v) {
        BasicListHolder viewHolder = (BasicListHolder) getViewHolder(v);
        return viewHolder != null ? viewHolder.getDataBinding() : null;
    }

    public RecyclerView.ViewHolder getViewHolder(View v) {
        return recyclerView.findContainingViewHolder(v);
    }

    public abstract T setCreateViewHolder(ViewGroup parent, int viewType);

    public abstract void setBindViewHolder(T holder, int position);
}
