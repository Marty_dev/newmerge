package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.net.CookieHandler;

import kr.co.mergepoint.mergeclient.BuildConfig;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.CustomCookieManager;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.databinding.SettingsBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;
import okhttp3.ResponseBody;

import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_ROLE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PUSH;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SETTING_PREFS;
import static kr.co.mergepoint.mergeclient.application.common.Properties.VIBRATION;

/**
 * Created by 1017sjg on 2017. 8. 9..
 */

public class SettingsFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {

    private SettingsBinding settingsBinding;

    public static SettingsFragment newInstance() {
        Bundle args = new Bundle();
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        settingsBinding = DataBindingUtil.inflate(inflater, R.layout.settings, container, false);
        settingsBinding.setTitle(getString(R.string.settings));
        settingsBinding.setOnClick(this);
        settingsBinding.setSwitchListener(this);
        settingsBinding.setIsLogin(MergeApplication.getInitialInfo().isSignin());
        settingsBinding.currentversion.setText("현재 버전 : " + BuildConfig.VERSION_NAME);

        SharedPreferences sharedPreferences = rootActivity.getSharedPreferences(SETTING_PREFS, Context.MODE_PRIVATE);
        settingsBinding.pushSwitch.setChecked(sharedPreferences.getBoolean(PUSH, true));
        settingsBinding.vibrationSwitch.setChecked(sharedPreferences.getBoolean(VIBRATION, true));

        Member member = MergeApplication.getMember();
        settingsBinding.approvalManagementLayout.setVisibility(member != null && member.getRoles().contains(ENTERPRISE_ROLE) ? View.VISIBLE : View.GONE);
        return settingsBinding.getRoot();
    }

    public void setUserLogoutState() {
        settingsBinding.setIsLogin(false);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        SharedPreferences sharedPreferences = rootActivity.getSharedPreferences(SETTING_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        switch (compoundButton.getId()) {
            case R.id.push_switch:
                editor.putBoolean(PUSH, isChecked);
                break;
            case R.id.vibration_switch:
                editor.putBoolean(VIBRATION, isChecked);
        }
        editor.apply();
    }



}
