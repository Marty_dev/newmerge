package kr.co.mergepoint.mergeclient.scene.payment.dagger;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.payment.PaymentActivity;
import kr.co.mergepoint.mergeclient.scene.payment.model.PaymentModel;
import kr.co.mergepoint.mergeclient.scene.payment.presenter.PaymentPresenter;
import kr.co.mergepoint.mergeclient.scene.payment.presenter.callback.PaymentCallback;
import kr.co.mergepoint.mergeclient.scene.payment.view.PaymentView;

/**
 * Created by jgson on 2017. 6. 5..
 */

@Module
public class PaymentModule {

    private PaymentActivity activity;

    public PaymentModule(PaymentActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PaymentScope
    PaymentModel provideModel() {
        return new PaymentModel();
    }

    @Provides
    @PaymentScope
    PaymentView provideView() {
        return new PaymentView(activity, R.layout.payment);
    }

    @Provides
    @PaymentScope
    PaymentPresenter providePresenter(PaymentView view, PaymentModel model) {
        return new PaymentPresenter(view, model, new PaymentCallback(view, model));
    }
}
