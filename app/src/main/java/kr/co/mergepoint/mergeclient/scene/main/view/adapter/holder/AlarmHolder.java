package kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * User: Marty
 * Date: 2018-08-01
 * Time: 오후 5:37
 * Description:
 */
public class AlarmHolder extends RecyclerView.ViewHolder{
    public TextView title,date;
    public RelativeLayout content;
    public AlarmHolder(View itemView) {
        super(itemView);
    }
}
