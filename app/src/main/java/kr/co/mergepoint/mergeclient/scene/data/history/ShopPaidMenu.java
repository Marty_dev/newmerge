package kr.co.mergepoint.mergeclient.scene.data.history;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionMenu;

/**
 * Created by 1017sjg on 2017. 7. 31..
 */

public class ShopPaidMenu implements Parcelable {
    @SerializedName("paymentRef")
    public int paymentRef;

    @SerializedName("menuNum")
    public int menuNum;

    @SerializedName("menuOrderNum")
    public int menuOrderNum;

    @SerializedName("menuUnusedNum")
    public int menuUnusedNum;

    @SerializedName("payDateTime")
    public ArrayList<Integer> payDateTime;

    @SerializedName("payMenu")
    public ArrayList<ShopMenuDetail> payMenu;

    private ShopPaidMenu(Parcel in) {
        paymentRef = in.readInt();
        menuNum = in.readInt();
        menuOrderNum = in.readInt();
        menuUnusedNum = in.readInt();
        payDateTime = new ArrayList<>();
        in.readList(payDateTime, Integer.class.getClassLoader());
        payMenu = new ArrayList<>();
        in.readTypedList(payMenu, ShopMenuDetail.CREATOR);
    }

    public static final Parcelable.Creator<ShopPaidMenu> CREATOR = new Parcelable.Creator<ShopPaidMenu>() {
        @Override
        public ShopPaidMenu createFromParcel(Parcel in) {
            return new ShopPaidMenu(in);
        }

        @Override
        public ShopPaidMenu[] newArray(int size) {
            return new ShopPaidMenu[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(paymentRef);
        dest.writeInt(menuNum);
        dest.writeInt(menuOrderNum);
        dest.writeInt(menuUnusedNum);
        dest.writeList(payDateTime);
        dest.writeTypedList(payMenu);
    }
}
