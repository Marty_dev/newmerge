package kr.co.mergepoint.mergeclient.application.common;

import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

/**
 * Created by 1017sjg on 2017. 7. 7..
 */

public class CustomChromeClient extends WebChromeClient {

    private ContentLoadingProgressBar contentLoadingProgressBar;

    public void setContentLoadingProgressBar(ContentLoadingProgressBar contentLoadingProgressBar) {
        this.contentLoadingProgressBar = contentLoadingProgressBar;
    }

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        contentLoadingProgressBar.setProgress(newProgress);
        if (newProgress >= 100) {
            contentLoadingProgressBar.setVisibility(View.GONE);
        } else if (contentLoadingProgressBar.getVisibility() != View.VISIBLE){
            contentLoadingProgressBar.setVisibility(View.VISIBLE);
        }
    }
}
