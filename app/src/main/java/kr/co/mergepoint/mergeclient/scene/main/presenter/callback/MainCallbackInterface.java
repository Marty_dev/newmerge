package kr.co.mergepoint.mergeclient.scene.main.presenter.callback;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.CompanyPointHistory;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.main.CombineImagesDetail;
import kr.co.mergepoint.mergeclient.scene.data.main.EventDetail;
import kr.co.mergepoint.mergeclient.scene.data.main.Notice;
import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPayItem;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.data.payment.Ticket;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import okhttp3.ResponseBody;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public interface MainCallbackInterface {

    MergeCallback<ResponseBody> getTokenCallback();

    MergeCallback<ResponseObject<Long>> refreshUserPoints();

    MergeCallback<ResponseObject<JsonObject>> shopVisitCallback();

    MergeCallback<ResponseBody> getLogoutCallback();

    MergeCallback<ResponseObject<Object>> getWithdrawalCallback();

    MergeCallback<ResponseObject<Object>> getAllianceCallback();

    MergeCallback<ResponseObject<Integer>> getQuestionCallback();

    MergeCallback<ResponseObject<Integer>> getCouponCallback();

    MergeCallback<ResponseObject<Member>> getMemberInfoCallback();

    MergeCallback<ResponseObject<Member>> resetPasswordCallback();

    MergeCallback<ResponseObject<ArrayList<Notice>>> getNoticeListCallback();

    MergeCallback<ResponseObject<ArrayList<CombineImagesDetail>>> getMergesPickCallback();

    MergeCallback<ResponseObject<EventDetail>> getEventDetailCallback();

    MergeCallback<ResponseObject<ArrayList<Shop>>> getFavoriteListCallback();

    MergeCallback<ResponseObject<Boolean>> changeCrowdApprovalType(int type, int oid, String userName);

    MergeCallback<ResponseObject<ArrayList<CrowdMember>>> getCrowdMemberList();

    MergeCallback<ResponseObject<ArrayList<CrowdMember>>> getDelegationMemberList();

    MergeCallback<ResponseObject<Long>> changeFavoriteCrowdMember();

    MergeCallback<ResponseObject<ArrayList<Ticket>>> getTicketList();

    MergeCallback<ResponseObject<ArrayList<CompanyPointHistory>>> getCompanyPointHistory();

    MergeCallback<ResponseObject<ArrayList<CrowdPayItem>>> getCrowdPaymentHistory();

    MergeCallback<ResponseObject<ArrayList<CrowdPay>>> getCrowdApprovalRequestList();

    MergeCallback<ResponseObject<ArrayList<CrowdPay>>> sendCheckApproval();

    MergeCallback<ResponseObject<ShopPayment>> getCrowdPayHistoryDetail();

    MergeCallback<ResponseObject<ArrayList<Coupon>>> getMyCouponList();
}
