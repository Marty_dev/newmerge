package kr.co.mergepoint.mergeclient.scene.data.menu;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import kr.co.mergepoint.mergeclient.BR;

/**
 * Created by 1017sjg on 2017. 6. 23..
 */

public class ShopChildInfo extends BaseObservable implements Parcelable {

    @SerializedName("oid")
    public int oid;

    @SerializedName("shopRef")
    public int shopRef;

    @SerializedName("categoryRef")
    public int categoryRef;

    @SerializedName("name")
    public String name;

    @SerializedName("soldout")
    public boolean soldout;

    @SerializedName("soldoutPeriod")
    public int soldoutPeriod;

    @SerializedName("chooseType")
    public boolean chooseType;

    @SerializedName("price")
    public int price;

    @SerializedName("image")
    public String image;

    @SerializedName("comment")
    public String comment;

    @SerializedName("mainMenuCheck")
    public boolean mainMenuCheck;

    @SerializedName("unable")
    public boolean unable;

    public ShopChildInfo(Parcel in) {
        oid = in.readInt();
        shopRef = in.readInt();
        categoryRef = in.readInt();
        name = in.readString();
        soldout = in.readInt() == 1;
        soldoutPeriod = in.readInt();
        chooseType = in.readInt() == 1;
        price = in.readInt();
        image = in.readString();
        comment = in.readString();
        unable = in.readInt() == 1;
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
        notifyPropertyChanged(BR.price);
    }

    public ShopChildInfo() {}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(oid);
        dest.writeInt(shopRef);
        dest.writeInt(categoryRef);
        dest.writeString(name);
        dest.writeInt(soldout ? 1 : 0);
        dest.writeInt(soldoutPeriod);
        dest.writeInt(chooseType ? 1 : 0);
        dest.writeInt(price);
        dest.writeString(image);
        dest.writeString(comment);
        dest.writeInt(unable ? 1 : 0);
    }

    public static final Creator<ShopChildInfo> CREATOR = new Creator<ShopChildInfo>() {
        @Override
        public ShopChildInfo createFromParcel(Parcel in) {
            return new ShopChildInfo(in);
        }

        @Override
        public ShopChildInfo[] newArray(int size) {
            return new ShopChildInfo[size];
        }
    };
}
