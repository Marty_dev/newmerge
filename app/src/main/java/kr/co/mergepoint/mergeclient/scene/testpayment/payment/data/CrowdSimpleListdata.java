package kr.co.mergepoint.mergeclient.scene.testpayment.payment.data;

import com.google.gson.annotations.SerializedName;

/**
 * User: Marty
 * Date: 2018-07-18
 * Time: 오후 2:37
 * Description:
 */
public class CrowdSimpleListdata {

    @SerializedName("oid")
    public int oid;

    @SerializedName("memberRef")
    public int memberRef;

    @SerializedName("memberName")
    public String memberName;

    @SerializedName("allocatePoint")
    public int allocatePoint;

    @SerializedName("approveState")
    public int approveState ;

}
