package kr.co.mergepoint.mergeclient.scene.testpayment.payment.http;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.application.common.MergeCallbackV2;
import kr.co.mergepoint.mergeclient.scene.data.basket.AddBasket;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.CrowdSettingActivity;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.Payment_v2_Activity;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.CrowdInfo;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.PaymentRequest;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ticketbody;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.view.SearchCrowdPayUserFragmentV2;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.SEARCH_CROWD_PAY_FRAGMENTV2_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.requiredLoginActivities;

/**
 * User: Marty
 * Date: 2018-07-18
 * Time: 오후 6:56
 * Description:
 */
public class Payment_v2_Callbacks {

    public Api apis;

    Context mCon;
    BaseActivity activity;
    Member member;
    public Payment_v2_Callbacks(BaseActivity activity) {
        this.activity = activity;
        mCon = activity.getBaseContext();
        this.apis = MergeApplication.getMergeAppComponent().getRetrofit().create(Api.class);
        member = MergeApplication.getMember();
    }



    public void PaymentRequestByMenu(AddBasket addBasket){
        MergeApplication.getMergeApplication().showLoading(activity);
        MDEBUG.debug("Proccessing 8");

        apis.requestPayment(addBasket).enqueue(PaymentDataCallback());
        MDEBUG.debug("Proccessing 9");

    }

    public void PaymentRequestByBasket(ArrayList<Integer> oids){
        MergeApplication.getMergeApplication().showLoading(activity);
        apis.requestPayment(TextUtils.join(",",oids)).enqueue(PaymentDataCallback());
    }
    public void PaymentRequestByPrice(int shopref,int price){
        MergeApplication.getMergeApplication().showLoading(activity);
        apis.requestPayment("0",shopref,price).enqueue(PaymentDataCallback());
    }

    public void RequestPointAdapt(int oid, long point,int couponoid){
        MergeApplication.getMergeApplication().showLoading(activity);
        apis.requestPointAdapt(oid,point,couponoid).enqueue(PaymentDataCallback());
    }
    public void RequestEnterAdapt(ticketbody ticket){
        MergeApplication.getMergeApplication().showLoading(activity);
        apis.requestEnterAdapt(ticket).enqueue(PaymentDataCallback());
    }
    public void RequestCrowdList(int type ,String search ,int oid){
        MergeApplication.getMergeApplication().showLoading(activity);
        apis.getCrowedMemberList(type,search,oid).enqueue(getCrowedMemberList());
    }

    public void RequestAddCrowds(int Shopref,String oids){
        MergeApplication.getMergeApplication().showLoading(activity);
        apis.requestAddCrowd(Shopref,oids).enqueue(getCrowdListAdd());
    }

    public void RequestReadPayment(long Shopoid){
        MergeApplication.getMergeApplication().showLoading(activity);
        apis.requestReadPayment(Shopoid).enqueue(PaymentDataCallback());
    }

    public Callback<ResponseObject<CrowdInfo>> getCrowdListAdd(){
        return new Callback<ResponseObject<CrowdInfo>>() {
            @Override
            public void onResponse(Call<ResponseObject<CrowdInfo>> call, Response<ResponseObject<CrowdInfo>> response) {
                MDEBUG.debug("Res : \n" + response.toString());
                if(response.body().getObject().attendees == null || response.body().getObject().attendees.size() == 0){
                    new MergeDialog.Builder(activity).setContent("함께 결제할 인원을 선택해 주세요").setCancelBtn(false).build().show();
                    MergeApplication.getMergeApplication().hideLoading(activity);
                    return;
                }
                MergeApplication.getMergeApplication().hideLoading(activity);
                if (activity instanceof Payment_v2_Activity)
                    ((Payment_v2_Activity)activity).goCrowdSettingActivity();
                else if (activity instanceof CrowdSettingActivity) {
                    ((CrowdSettingActivity) activity).init();
                }

            }

            @Override
            public void onFailure(Call<ResponseObject<CrowdInfo>> call, Throwable t) {
                MDEBUG.debug("Failed :" + t.toString());
                MergeApplication.getMergeApplication().hideLoading(activity);
                new MergeDialog.Builder(activity).setContent(R.string.retry).setCancelBtn(false).build().show();

            }
        };
    }

    public void testapi(){
        MergeApplication.getMergeApplication().showLoading(activity);
        apis.requestcrowdApprove(217).enqueue(new Callback<ResponseObject<String>>() {
            @Override
            public void onResponse(Call<ResponseObject<String>> call, Response<ResponseObject<String>> response) {
                MDEBUG.debug("call response" +response.body().getObject());
                MDEBUG.debug("SUCCESS");
                MergeApplication.getMergeApplication().hideLoading(activity);

            }

            @Override
            public void onFailure(Call<ResponseObject<String>> call, Throwable t) {
                MDEBUG.debug("TEST FAILLLED!@@");
                MDEBUG.debug("TEST FAILLLED!@@ ::" + t.toString());
                MergeApplication.getMergeApplication().hideLoading(activity);

            }
        });
    }

    public void Requestfavorit(int oid){
        MergeApplication.getMergeApplication().showLoading(activity);
        apis.requestfavorite(oid).enqueue(new MergeCallbackV2<ResponseObject<Long>>(activity, new MergeCallbackV2.MergeCall<ResponseObject<Long>>() {
            @Override
            public void onResponse(Call<ResponseObject<Long>> call, Response<ResponseObject<Long>> response) {

            }
        }));
    }

    public Callback<ResponseObject<ArrayList<CrowdMember>>> getCrowedMemberList() {
        return new Callback<ResponseObject<ArrayList<CrowdMember>>>() {
            @Override
            public void onResponse(Call<ResponseObject<ArrayList<CrowdMember>>> call, Response<ResponseObject<ArrayList<CrowdMember>>> response) {
                ResponseObject<ArrayList<CrowdMember>> responseObject = response.body();
                if (responseObject.isFailed()){
                    new MergeDialog.Builder(activity).setContent(responseObject.getMessage()).setCancelBtn(false).build().show();
                    MergeApplication.getMergeApplication().hideLoading(activity);
                    return;
                }
                SearchCrowdPayUserFragmentV2 fragment = (SearchCrowdPayUserFragmentV2) ((BaseFragment) activity.getSupportFragmentManager().findFragmentByTag(SEARCH_CROWD_PAY_FRAGMENTV2_TAG));
                if (fragment != null && responseObject != null) {

                    fragment.setSearchCrowedPayUserAdapter(responseObject.getObject());
                }
                MergeApplication.getMergeApplication().hideLoading(activity);

            }

            @Override
            public void onFailure(Call<ResponseObject<ArrayList<CrowdMember>>> call, Throwable t) {
                MergeApplication.getMergeApplication().hideLoading(activity);
                new MergeDialog.Builder(activity).setContent(R.string.retry).setCancelBtn(false).build().show();


            }
        };
    }


    public Callback<ResponseObject<PaymentRequest>> PaymentDataCallback(){
        final Payment_v2_Activity activity = (Payment_v2_Activity)this.activity;
        return new Callback<ResponseObject<PaymentRequest>>() {
            @Override
            public void onResponse(Call<ResponseObject<PaymentRequest>> call, Response<ResponseObject<PaymentRequest>> response) {
                MDEBUG.debug("Proccessing 7");

                if (response.isSuccessful()) {
                    ResponseObject<PaymentRequest> res = response.body();

                    if (res.getObject() != null) {
                        PaymentRequest obj = res.getObject();
                        MDEBUG.debug("Proccessing 6");

                        if (obj.shopPayment.size() != 1 || !obj.shopPayment.get(0).usableCompanyPay) {
                            activity.crowdpayBtn.setVisibility(View.GONE);
                            activity.crowdList.setVisibility(View.GONE);
                        } else {
                            activity.crowdpayBtn.setVisibility(View.VISIBLE);
                            activity.crowdList.setVisibility(View.VISIBLE);
                        }

                        activity.payTotalprise.setText(MoneyForm(obj.orderPrice) + "원");
                        activity.payTotalpoint.setText(MoneyForm(obj.couponPrice) + "원");
                        activity.payTotalcount.setText(obj.menuNum + "개");
                        activity.payPointprise.setText(MoneyForm(obj.pointPrice) + "원");
                        activity.payPlannedpoint.setText(MoneyForm(obj.plannedPoints) + "원");
                        activity.payCardprise.setText(MoneyForm(obj.payPrice) + "원");
                        activity.payEnterprise.setText(MoneyForm(obj.companyPoints) + "원");

                        activity.isEnterMember = (MergeApplication.getMember().getRoles().contains("02") ||
                                MergeApplication.getMember().getRoles().contains("03"));
                        MDEBUG.debug("Proccessing 5");

                        activity.initList(obj, activity.isEnterMember);


                        //  기업 회원  --  기업 제휴 매장  -- 기업포인트만 , 함께결제
                        //  기업 회원  --  일반 매장       -- 개인 기업 포인트 , 함께결제
                        //  개인 회원  --  일반 매장       -- 개인포인트
                        //  장바구니   --  다중 점포       -- 분야 위와 동일하나 함께결제 불가
                    }
                    if (res.isFailed()) {
                        // TODO: 2018-07-20 오류 출력
                        new MergeDialog.Builder(activity).setContent(res.getMessage()).setCancelBtn(false).build().show();
                    }
                }else
                    new MergeDialog.Builder(activity).setContent(R.string.retry).setCancelBtn(false).build().show();

                MDEBUG.debug("the end");

                MergeApplication.getMergeApplication().hideLoading(activity);

            }

            @Override
            public void onFailure(Call<ResponseObject<PaymentRequest>> call, Throwable t) {
                MDEBUG.debug("FaileD");
                MergeApplication.getMergeApplication().hideLoading(activity);
                Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.retry), Snackbar.LENGTH_SHORT).show();
                MDEBUG.debug("ERORORO : " + t.toString());

            }
        };
    }

    public String MoneyForm(int money){
        String formdata = String.format("%,d",money);
        return formdata;
    }
}
