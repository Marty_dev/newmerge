package kr.co.mergepoint.mergeclient.scene.data.history;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;

/**
 * Created by 1017sjg on 2017. 7. 29..
 */

public class ShopHistoryPayment implements Parcelable {

    @SerializedName("shopRef")
    public int shopRef;

    @SerializedName("shopName")
    public String shopName;

    @SerializedName("menuNum")
    public int menuNum;

    @SerializedName("useState")
    public int useState;

    @SerializedName("menuOrderNum")
    public int menuOrderNum;

    @SerializedName("consummerRef")
    public int consummerRef;

    @SerializedName("menuUnusedNum")
    public int menuUnusedNum;

    @SerializedName("expDate")
    public ArrayList<Integer> expDate;

    @SerializedName("payDateTime")
    public ArrayList<Integer> payDateTime;

    @SerializedName("payMenu")
    public ArrayList<ShopMenuDetail> payMenu;

    @SerializedName("paidMenu")
    public ArrayList<ShopPaidMenu> paidMenu;



    private ShopHistoryPayment(Parcel in) {
        shopRef = in.readInt();
        shopName = in.readString();
        menuNum = in.readInt();
        useState = in.readInt();
        menuOrderNum = in.readInt();
        menuUnusedNum = in.readInt();
        expDate = new ArrayList<>();
        in.readList(expDate, Integer.class.getClassLoader());
        payDateTime = new ArrayList<>();
        in.readList(payDateTime, Integer.class.getClassLoader());
        payMenu = new ArrayList<>();
        in.readTypedList(payMenu, ShopMenuDetail.CREATOR);
        paidMenu = new ArrayList<>();
        in.readTypedList(paidMenu, ShopPaidMenu.CREATOR);
        in.readInt();
    }

    public static final Parcelable.Creator<ShopHistoryPayment> CREATOR = new Parcelable.Creator<ShopHistoryPayment>() {
        @Override
        public ShopHistoryPayment createFromParcel(Parcel in) {
            return new ShopHistoryPayment(in);
        }

        @Override
        public ShopHistoryPayment[] newArray(int size) {
            return new ShopHistoryPayment[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(shopRef);
        dest.writeString(shopName);
        dest.writeInt(menuNum);
        dest.writeInt(useState);
        dest.writeInt(menuOrderNum);
        dest.writeInt(menuUnusedNum);
        dest.writeList(expDate);
        dest.writeList(payDateTime);
        dest.writeTypedList(payMenu);
        dest.writeTypedList(paidMenu);
        dest.writeInt(consummerRef);
    }
}

