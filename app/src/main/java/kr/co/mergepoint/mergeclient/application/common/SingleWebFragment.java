package kr.co.mergepoint.mergeclient.application.common;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.databinding.SingleWebviewBinding;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static kr.co.mergepoint.mergeclient.application.common.Properties.APP_USER_AGENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WEB_JAVASCRIPT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WEB_URL;

/**
 * Created by 1017sjg on 2017. 6. 16..
 */

public abstract class SingleWebFragment<T> extends BaseFragment implements View.OnClickListener{

    private WebView webView;

    public WebView getWebView() {
        return webView;
    }

    @SuppressLint({"SetJavaScriptEnabled", "JavascriptInterface"})
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SingleWebviewBinding singleWebviewBinding = DataBindingUtil.inflate(inflater, R.layout.single_webview, container, false);
        singleWebviewBinding.setTitle(setPageTitle());
        singleWebviewBinding.setClick(this);

        webView = singleWebviewBinding.webview;

        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            WebView.setWebContentsDebuggingEnabled(true);

        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setSupportMultipleWindows(true);

        // 설정값
        if (setJavascript() != null)
            webView.addJavascriptInterface(setJavascript(), WEB_JAVASCRIPT);
        webView.setWebViewClient(setWebViewClient());
        webView.setWebChromeClient(setWebChromeClient(singleWebviewBinding.toolbarDatabinding.loadProgressBar));
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUserAgentString(singleWebviewBinding.webview.getSettings().getUserAgentString() + "/" + APP_USER_AGENT);

        // 웹뷰의 쿠키저장소를 사용할 수 있도록 허용
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            CookieManager.getInstance().setAcceptThirdPartyCookies(singleWebviewBinding.webview, true);
        CookieManager.getInstance().setAcceptCookie(true);

        webView.loadUrl(getArguments().getString(WEB_URL));


        return singleWebviewBinding.getRoot();
    }

    protected abstract WebChromeClient setWebChromeClient(ContentLoadingProgressBar loadProgressBar);
    protected abstract WebViewClient setWebViewClient();
    protected abstract String setPageTitle();
    protected abstract T setJavascript();



    @Override
    public void onClick(View v) {
        MDEBUG.debug("pre onClick Logic");
        if (webView.canGoBack()) {
            MDEBUG.debug("goback");
            webView.goBack();
        } else {
            MDEBUG.debug("Fragment deleted");
            getFragmentManager().popBackStack();
        }
    }
}
