package kr.co.mergepoint.mergeclient.scene.newcoupon.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponV1;
import kr.co.mergepoint.mergeclient.scene.newcoupon.Coupondialog_onClick;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * User: Marty
 * Date: 2018-09-12
 * Time: 오전 10:48
 * Description:
 */
public class MergeNewDialog extends Dialog {
    @BindView(R.id.dialog_top_title)
    TextView dialogTopTitle;
    @BindView(R.id.coupon_notice_date1_text)
    TextView couponNoticeDate1Text;
    @BindView(R.id.coupon_notice_date1_tv1)
    TextView couponNoticeDate1Tv1;
    @BindView(R.id.coupon_notice_date1_tv2)
    TextView couponNoticeDate1Tv2;
    @BindView(R.id.coupon_notice_date2_text)
    TextView couponNoticeDate2Text;
    @BindView(R.id.coupon_notice_date2_tv1)
    TextView couponNoticeDate2Tv1;
    @BindView(R.id.coupon_notice_date2_tv2)
    TextView couponNoticeDate2Tv2;
    @BindView(R.id.coupon_notice_howtouse_text)
    TextView couponNoticeHowtouseText;
    @BindView(R.id.coupon_notice_howtouse_tv)
    TextView couponNoticeHowtouseTv;
    @BindView(R.id.dialog_detail_rl)
    RelativeLayout dialogDetailRl;
    @BindView(R.id.dialog_content_rl)
    RelativeLayout dialogContentRl;
    @BindView(R.id.basebottomline)
    TextView basebottomline;
    @BindView(R.id.basecenterline)
    TextView basecenterline;
    @BindView(R.id.dialog_cancel)
    TextView dialogCancel;
    @BindView(R.id.dialog_confirm)
    TextView dialogConfirm;


    /*
     * 1 = MergeCoupon
     * 2 = 프랜차이즈
     * 3 = 연장
     * 4 = 삭제
     * */
    int typecode;

    String title, userule, date, howto;
    String content, phonenum;
    String b_date1, r_date1, b_date2, r_date2;
    int couponoid;
    CouponV1 data;
    @BindView(R.id.dialog_content)
    TextView dialogContent;
    Context mCon;
    @BindView(R.id.dialog_two_btnrl)
    RelativeLayout dialogTwoBtnrl;
    @BindView(R.id.dialog_single_btn)
    TextView dialogSingleBtn;

    Coupondialog_onClick onConfirm;

    public MergeNewDialog(@NonNull Context context) {
        super(context);
        mCon = context;

    }

    public MergeNewDialog(Context context, String title, String userule, String date, String howto) {
        super(context);
        mCon = context;
        typecode = 1;
        this.title = title;
        this.userule = userule;
        this.date = date;
        this.howto = howto;
    }

    public MergeNewDialog(Context context, String content, String phonenum) {
        super(context);
        typecode = 3;
        this.phonenum = phonenum;
        mCon = context;
        this.howto = howto;
    }

    public MergeNewDialog(Context context, String content) {
        super(context);
        typecode = 4;
        mCon = context;
        this.content = content;
    }

    public MergeNewDialog(Context context, CouponV1 data, int type) {
        super(context);
        mCon = context;
        typecode = type;
        this.data = data;
    }

    public MergeNewDialog setOnConfirm(Coupondialog_onClick onclick){
        this.onConfirm = onclick;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coupon_dialogs);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        View.OnClickListener cancel = (v)->this.dismiss();
        dialogSingleBtn.setOnClickListener(cancel);
        dialogCancel.setOnClickListener(cancel);
        if (typecode == 1 || typecode == 2) {
            dialogContent.setVisibility(View.GONE);
            dialogContentRl.setBackground(mCon.getResources().getDrawable(R.drawable.popup_box_mid_white));
        } else {
            dialogContentRl.setVisibility(View.GONE);
            dialogContentRl.setBackground(mCon.getResources().getDrawable(R.drawable.popup_top_w));
        }
        if (typecode == 1) {
            dialogTopTitle.setText(data.getCouponName());
            couponNoticeDate1Text.setText("사용조건");
            couponNoticeDate1Tv1.setText(data.getUseCondition());
            couponNoticeDate2Text.setText("유효기간");
            couponNoticeDate2Tv1.setText(data.getExpValidity1());
            couponNoticeDate2Tv2.setText(data.getExpValidity());
            couponNoticeHowtouseTv.setText(data.getUseMethod());
            dialogSingleBtn.setText("닫기");
            dialogSingleBtn.setVisibility(View.VISIBLE);
            dialogTwoBtnrl.setVisibility(View.GONE);
            dialogTwoBtnrl.setVisibility(View.GONE);

        }else if(typecode == 2){
            couponNoticeDate1Tv1.setText(data.getExpValidity1());
            couponNoticeDate1Tv2.setText(data.getExpValidity());
            couponNoticeDate2Tv1.setText(data.getBarCodeValidity());
            couponNoticeDate2Tv2.setText(data.getBarCodeValidityInfo());
            couponNoticeHowtouseTv.setText(data.getUseMethod());
            dialogTwoBtnrl.setVisibility(View.VISIBLE);
            dialogSingleBtn.setVisibility(View.GONE);
            dialogConfirm.setText("바코드발급");
            dialogConfirm.setTextColor(Color.parseColor("#fe5e6d"));
            dialogConfirm.setOnClickListener((v)->{
                MDEBUG.debug("Call?? Null Check " + onConfirm == null ? "null" : "Not null");
                if (onConfirm != null)
                    dismiss();
                    onConfirm.onConfirm(v,data);
            });

        }
    }
}
