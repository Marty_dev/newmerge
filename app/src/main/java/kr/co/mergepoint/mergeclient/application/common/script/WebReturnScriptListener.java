package kr.co.mergepoint.mergeclient.application.common.script;

/**
 * Created by 1017sjg on 2017. 7. 21..
 */

public interface WebReturnScriptListener {
    void success();
    void failure(String error);
}
