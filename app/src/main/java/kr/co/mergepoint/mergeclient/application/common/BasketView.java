package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.BasketApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MAP;

/**
 * Created by jgson on 2018. 2. 5..
 */

public class BasketView extends ConstraintLayout {

    private boolean isBasket;
    private AppCompatTextView basketNumberTextView;

    public BasketView(Context context) {
        super(context);
        setup();
    }

    public BasketView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public BasketView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup();
    }

    private void setup() {
        DisplayMetrics displayMetrics = MergeApplication.getUtility().getResuorces().getDisplayMetrics();
        int basketNumSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 19, displayMetrics);
        int basketNumEndMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5.7f, displayMetrics);
        int basketImgSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 78, displayMetrics);

        ConstraintSet set = new ConstraintSet();
        set.clone(this);

        ImageView imageView = new ImageView(getContext());
        imageView.setId(View.generateViewId());
        imageView.setImageResource(R.drawable.shopping_basket);
        addView(imageView);

        set.connect(imageView.getId(), ConstraintSet.START, getId(), ConstraintSet.START);
        set.connect(imageView.getId(), ConstraintSet.TOP, getId(), ConstraintSet.TOP);
        set.connect(imageView.getId(), ConstraintSet.END, getId(), ConstraintSet.END);
        set.connect(imageView.getId(), ConstraintSet.BOTTOM, getId(), ConstraintSet.BOTTOM);
        set.constrainWidth(imageView.getId(), basketImgSize);
        set.constrainHeight(imageView.getId(), basketImgSize);

        basketNumberTextView = new AppCompatTextView(getContext());
        basketNumberTextView.setId(View.generateViewId());
        basketNumberTextView.setGravity(Gravity.CENTER);
        basketNumberTextView.setPadding(0,-10,0,0);
        basketNumberTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15.5f);
        basketNumberTextView.setTypeface(ResourcesCompat.getFont(getContext(), R.font.spoqa_han_sans_bold));
        basketNumberTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.red_03));
        basketNumberTextView.setBackgroundResource(R.drawable.basket_number);
        addView(basketNumberTextView);

        set.connect(basketNumberTextView.getId(), ConstraintSet.TOP, getId(), ConstraintSet.TOP);
        set.connect(basketNumberTextView.getId(), ConstraintSet.END, getId(), ConstraintSet.END, basketNumEndMargin);
        set.constrainWidth(basketNumberTextView.getId(), basketNumSize);
        set.constrainHeight(basketNumberTextView.getId(), basketNumSize);

        set.applyTo(this);
    }

    public void getCartCount(BaseView baseView) {
        getCartCount(baseView, MAP, false);
    }
    public void getCartCount(BaseActivity baseView) {
        getCartCount(baseView, MAP, false);
    }

    public void getCartCount(BaseView baseView, int mapButton, boolean isDescription) {
        BasketApi basketApi = MergeApplication.getMergeAppComponent().getRetrofit().create(BasketApi.class);
        basketApi.getShoppingBasketCount().enqueue(new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Integer> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                int basketNumber = responseObject.getObject();
                MDEBUG.debug("basket info size : " + basketNumber);
                MDEBUG.debug("basket info msg: " + responseObject.getMessage());
                setBasketState(basketNumber, mapButton, !responseObject.isFailed() && basketNumber != 0, isDescription);
            } else {
                setBasketState(0, mapButton, false, isDescription);
            }
        }));
    }
    public void getCartCount(BaseActivity baseView, int mapButton, boolean isDescription) {
        BasketApi basketApi = MergeApplication.getMergeAppComponent().getRetrofit().create(BasketApi.class);
        basketApi.getShoppingBasketCount().enqueue(new Callback<ResponseObject<Integer>>() {
            @Override
            public void onResponse(Call<ResponseObject<Integer>> call, Response<ResponseObject<Integer>> response) {
                ResponseObject<Integer> responseObject = response.body();
                if (response.isSuccessful() && responseObject != null) {
                    int basketNumber = responseObject.getObject();
                    MDEBUG.debug("basket info size : " + basketNumber);
                    MDEBUG.debug("basket info msg: " + responseObject.getMessage());
                    setBasketState(basketNumber, mapButton, !responseObject.isFailed() && basketNumber != 0, isDescription);
                } else {
                    setBasketState(0, mapButton, false, isDescription);
                }
            }

            @Override
            public void onFailure(Call<ResponseObject<Integer>> call, Throwable t) {
                Snackbar.make(findViewById(android.R.id.content), baseView.getString(R.string.retry), Snackbar.LENGTH_SHORT).show();
            }
        });
    }
    private void setBasketState(int basketNumber, int mapButton, boolean isShow, boolean isDescription) {
        this.isBasket = isShow;
        animBasket(basketNumber, mapButton, isDescription);
    }

    private void animBasket(int basketNumber, int mapButton, boolean isDescription) {
        if (isBasket && mapButton != 3) {
            /* 장바구니 있을 때 */
            int descriptionHeight = 0;
            /* 맵 열린 상태 */
            if (mapButton == LIST && isDescription) {
                 /* 점포 상세보기가 열린 상태 */
                descriptionHeight = -((int) MergeApplication.getUtility().getResuorces().getDimension(R.dimen.map_description_view_height));
            }

            animate().setDuration(500).translationY(descriptionHeight).withLayer().start();
            setVisibility(View.VISIBLE);
            setBasketNumber(basketNumber);
        } else {
            /* 장바구니 없을 때 */
            int basketBottom = (int) MergeApplication.getUtility().getResuorces().getDimension(R.dimen.shop_list_basket_margin);
            int basketIconHeight = (int) MergeApplication.getUtility().getResuorces().getDimension(R.dimen.shop_list_basket_icon_size);
            animate().setDuration(500).translationY(basketBottom + basketIconHeight).withLayer().start();
            setVisibility(View.GONE);
        }
    }

    private void setBasketNumber(int basketNumber) {
        if (basketNumber > 0) {
            String basketNumStr = basketNumber > 9 ? "9+" : String.valueOf(basketNumber);
            basketNumberTextView.setText(basketNumStr);
        }
    }
}
