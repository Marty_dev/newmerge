package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.shop.view.listener.OnRadioChange;
import kr.co.mergepoint.mergeclient.scene.shop.view.listener.RadioObserver;

/**
 * Created by 1017sjg on 2017. 7. 3..
 */

public class CustomCountAddButton extends AppCompatButton implements RadioObserver {

    public int selectOptionOid;
    public OnRadioChange onRadioChange;

    public CustomCountAddButton(Context context) {
        super(context);
    }

    public CustomCountAddButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomCountAddButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setOnRadioChange(OnRadioChange onRadioChange) {
        this.onRadioChange = onRadioChange;
        if (onRadioChange != null)
            onRadioChange.add(this);
    }

    @Override
    public void onChangeRadio(int selectMenuOid) {
        setBackgroundResource(this.selectOptionOid == selectMenuOid ? R.drawable.btn_quantity_plus : R.drawable.btn_quantity_black_add);
    }
}
