package kr.co.mergepoint.mergeclient.scene.shoppingbasket.presenter.callback;

import android.view.View;

import kr.co.mergepoint.mergeclient.application.common.CustomCountView;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.basket.Basket;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public interface BasketCallbackInterface {

    MergeCallback<ResponseObject<Integer>> getModifyShoppingBasketMenuCallback(final CustomCountView view, final int menuCount);

    MergeCallback<ResponseObject<Integer>> getDeleteMenuCallback(final View view);

    MergeCallback<Basket> getShoppingBasketCallback();
}
