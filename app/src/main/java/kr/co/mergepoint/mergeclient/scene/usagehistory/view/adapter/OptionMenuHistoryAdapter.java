package kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.UsageHistoryOptionListItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionMenu;

/**
 * Created by 1017sjg on 2017. 7. 29..
 */

public class OptionMenuHistoryAdapter extends BasicListAdapter<BasicListHolder<UsageHistoryOptionListItemBinding, ShopOptionMenu>, ShopOptionMenu> {

    public OptionMenuHistoryAdapter(ArrayList<ShopOptionMenu> arrayList) {
        super(arrayList);
    }

    @Override
    public BasicListHolder<UsageHistoryOptionListItemBinding, ShopOptionMenu> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.usage_history_option_list_item, parent, false);
        UsageHistoryOptionListItemBinding optionListItemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<UsageHistoryOptionListItemBinding, ShopOptionMenu>(optionListItemBinding) {
            @Override
            public void setDataBindingWithData(ShopOptionMenu data) {
                getDataBinding().setOption(data);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<UsageHistoryOptionListItemBinding, ShopOptionMenu> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
