package kr.co.mergepoint.mergeclient.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatDialog;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.databinding.MainEnterpriseNoticeBinding;
import kr.co.mergepoint.mergeclient.scene.data.menu.EnterpriseNotice;

import static kr.co.mergepoint.mergeclient.application.common.Properties.SETTING_PREFS;

/**
 * Created by jgson on 2018. 4. 2..
 */

public class MergeEnterpriseNotice {

    private AppCompatDialog currentAlert;

    private final BaseActivity activity;
    private EnterpriseNotice deptNotice;
    private EnterpriseNotice comNotice;

    public static class Builder {
        private final BaseActivity activity;
        private EnterpriseNotice deptNotice;
        private EnterpriseNotice comNotice;

        public Builder (BaseActivity activity) {
            this.activity = activity;
        }

        public Builder setDeptNotice(EnterpriseNotice deptNotice) {
            this.deptNotice = deptNotice;
            return this;
        }

        public Builder setComNotice(EnterpriseNotice comNotice) {
            this.comNotice = comNotice;
            return this;
        }

        public MergeEnterpriseNotice build() {
            return new MergeEnterpriseNotice(this);
        }
    }

    private MergeEnterpriseNotice(Builder builder) {
        this.activity = builder.activity;
        this.deptNotice = builder.deptNotice;
        this.comNotice = builder.comNotice;
    }

    private void setEnterpriseText(MainEnterpriseNoticeBinding alert, int tabPosition) {
        String title = tabPosition == 0 ? deptNotice.title : comNotice.title;
        ArrayList<Integer> dateArray = tabPosition == 0 || tabPosition == -1 ? deptNotice.staDateTime : comNotice.staDateTime;
        String date = String.format(Locale.getDefault(), "등록날짜 %d-%02d-%02d-%02d:%02d", dateArray.get(0), dateArray.get(1), dateArray.get(2), dateArray.get(3), dateArray.get(4));
        String message = tabPosition == 0 ? deptNotice.bodyText : comNotice.bodyText;
        alert.enterpriseNoticeTitle.setText(title);
        alert.enterpriseNoticeDate.setText(date);
        alert.enterpriseNoticeMessage.setText(message);
    }

    public void show() {
        if (activity != null && !activity.isDestroyed() && !(deptNotice == null && comNotice == null)) {
            SharedPreferences sharedPreferences = activity.getSharedPreferences(SETTING_PREFS, Context.MODE_PRIVATE);
            long dateLong = sharedPreferences.getLong("ENTERPRISE_NOTICE", 0);

            Calendar currentCalendar = Calendar.getInstance();
            currentCalendar.setTimeInMillis(System.currentTimeMillis());
            currentCalendar.set(Calendar.HOUR, 0);
            currentCalendar.set(Calendar.MINUTE, 0);
            currentCalendar.set(Calendar.SECOND, 0);
            currentCalendar.set(Calendar.MILLISECOND, 0);

            Calendar saveCalendar = Calendar.getInstance();
            saveCalendar.setTimeInMillis(dateLong);
            saveCalendar.set(Calendar.HOUR, 0);
            saveCalendar.set(Calendar.MINUTE, 0);
            saveCalendar.set(Calendar.SECOND, 0);
            saveCalendar.set(Calendar.MILLISECOND, 0);

            if (saveCalendar.compareTo(currentCalendar) == -1) {
                MainEnterpriseNoticeBinding alert = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.main_enterprise_notice, null, false);
                if (deptNotice != null && comNotice != null) {
                    setEnterpriseText(alert, 0);
                    alert.deptNoticeButton.setChecked(true);
                } else {
                    setEnterpriseText(alert, deptNotice != null ? 0 : 1);
                    if (deptNotice != null) {
                        alert.deptNoticeButton.setChecked(true);
                        alert.deptNoticeButton.setBackground(ContextCompat.getDrawable(activity,R.drawable.round_top_white));
                        alert.comNoticeButton.setVisibility(View.GONE);
                    } else {
                        alert.comNoticeButton.setChecked(true);
                        alert.comNoticeButton.setBackground(ContextCompat.getDrawable(activity,R.drawable.round_top_white));
                        alert.deptNoticeButton.setVisibility(View.GONE);
                    }
                }
                alert.notWatchTodayCheck.setTypeface(ResourcesCompat.getFont(activity, R.font.spoqa_han_sans_regular));
                alert.deptNoticeButton.setTypeface(ResourcesCompat.getFont(activity, R.font.spoqa_han_sans_regular));
                alert.comNoticeButton.setTypeface(ResourcesCompat.getFont(activity, R.font.spoqa_han_sans_regular));
                alert.enterpriseNoticeGroup.setOnCheckedChangeListener((radioGroup, checkedId) -> setEnterpriseText(alert, checkedId == R.id.dept_notice_button ? 0 : 1));
                alert.closeEnterpriseNotice.setOnClickListener(view -> {
                    boolean isNotWatchTodayCheck = alert.notWatchTodayCheck.isChecked();
                    if (isNotWatchTodayCheck) {
                        sharedPreferences.edit().putLong("ENTERPRISE_NOTICE", new Date(System.currentTimeMillis()).getTime()).apply();
                    } else {
                        sharedPreferences.edit().remove("ENTERPRISE_NOTICE").apply();
                    }
                    currentAlert.dismiss();
                });
                currentAlert = new AppCompatDialog(activity, R.style.AlertDialog);
                currentAlert.setContentView(alert.getRoot());
                currentAlert.setOnKeyListener((dialogInterface, keyCode, keyEvent) -> {
                    if (KeyEvent.KEYCODE_BACK == keyCode) {
                        currentAlert.dismiss();
                        return true;
                    }
                    return false;
                });
                Window window = currentAlert.getWindow();
                if (window != null)
                    window.getAttributes().windowAnimations = R.style.MergeDialogAnim;
                currentAlert.show();
            }
        }
    }
}
