package kr.co.mergepoint.mergeclient.scene.shop.presenter;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BasePresenter;
import kr.co.mergepoint.mergeclient.application.common.BookMark;
import kr.co.mergepoint.mergeclient.application.common.CustomViewPager;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.shop.model.ShopModel;
import kr.co.mergepoint.mergeclient.scene.shop.presenter.callback.ShopCallback;
import kr.co.mergepoint.mergeclient.scene.shop.view.ShopView;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.ShopPagerAdapter;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder.ChildMenuHolder;
import kr.co.mergepoint.mergeclient.scene.shop.view.listener.OnAddReviewsListener;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.view.PricePaymentActivity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static android.app.Activity.RESULT_OK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NONMEMBER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NOTBUY;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_PRICE_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PLAY;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RESET;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WEB_URL;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class ShopPresenter extends BasePresenter<ShopView, ShopModel, ShopCallback> implements ViewPager.OnPageChangeListener, CustomViewPager.LikeListener, BookMark.OnCheckFavorite, OnAddReviewsListener {

    private Runnable payEndResultRunnable;

    public ShopPresenter(ShopView baseView, ShopModel baseModel, ShopCallback callback) {
        super(baseView, baseModel, callback);
    }

    @Override
    protected void setBindingProperty() {
        baseView.binding.setOnClick(this);
        baseView.binding.setLikeListener(this);
        baseView.binding.setFavoriteListener(this);
        baseView.binding.setDetailShopImagePagerListener(this);
        baseView.binding.shopDetailTab.setupWithViewPager(baseView.binding.shopDetailPager);
        baseView.binding.setPagerAdapter(new ShopPagerAdapter(baseView.activity));
        baseView.binding.setDetailPagerListener(new TabLayout.TabLayoutOnPageChangeListener(baseView.binding.shopDetailTab));
    }

    public void onCreate(Bundle savedInstanceState) {
        baseView.showLoading();
        if (baseView.activity.getIntent().getExtras() != null) {
            Object shopObj = baseView.activity.getIntent().getExtras().get(SHOP_INFO);
            if (shopObj instanceof Shop) {
                baseModel.getShopDetailInfo(((Shop) shopObj).oid, baseCallback.shopDetailCallback());
            } else if (shopObj instanceof Integer) {
                baseModel.getShopDetailInfo((int) shopObj, baseCallback.shopDetailCallback());
            }
        }

        baseView.setupLikeTextSwitcher();
    }

    public void onResume() {

    }

    public void onPostResume() {
        if (payEndResultRunnable != null)
            new Handler().postDelayed(payEndResultRunnable, 500);
        baseView.animBasket();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        ShopDetail shopDetail = baseView.binding.getShopDetail();
        switch (v.getId()) {
            case R.id.shoppingbasket:
                if (baseView.startShoppingBasket()) {
                    baseView.showLoading();
                    baseModel.addShoppingBasket(baseCallback.addBasketCallback(), baseView.getSelectOptions());
                }
                break;
            case R.id.payment:
                baseView.openPayment();
                break;
            case R.id.usage_history:
            case R.id.payment_result_history:
                baseView.openActivity(UsageActivity.class);
                break;
            case R.id.payment_result_main:
                baseView.openClearTaskActivity(MainActivity.class);
                break;
            case R.id.shop_call:
                baseView.callShopPhone();
                break;
            case R.id.shop_share:
                baseView.openShopShared(shopDetail);
                break;
            case R.id.shop_map:
            case R.id.shop_map_front:
                //baseView.openMapIntent(shopDetail.latitude, shopDetail.longitude);
                baseView.openMapIntent(shopDetail.latitude, shopDetail.longitude,shopDetail.address01);
                break;
            case R.id.shop_list_cart:
                baseView.openShoppingBasket();
                break;
            case R.id.banner:
                baseView.showLoading();
                baseModel.getPreExperience(baseCallback.getPreExperienceCallback(), shopDetail.oid);
                break;
            case R.id.shop_merges_pick:
                Object object = v.getTag();
                if (object != null && (object instanceof Integer)) {
                    int shopMergesPickRef = (int) v.getTag();
                    baseView.showLoading();
                    baseModel.requestMergesPickDetail(baseCallback.getMergesPickCallback(), shopMergesPickRef);
                }
                break;
            case R.id.shop_list_item_layout:
                if (v.getTag() instanceof String) {
                    Bundle bundle = new Bundle();
                    bundle.putString(WEB_URL, (String) v.getTag());
                    baseView.openReviewWeb(bundle);
                }
                break;
            case R.id.shop_pricecustom:
                baseView.openActivityForResult(PricePaymentActivity.class,PAY_PRICE_REQUEST);
                break;

        }
    }

    /*메뉴의 child 메뉴 클릭했을 때*/
    public void onChildTouch(ChildViewHolder childViewHolder) {
        if (childViewHolder != null && childViewHolder instanceof ChildMenuHolder) {
            ChildMenuHolder childMenuHolder = (ChildMenuHolder) childViewHolder;
            int childOid = childMenuHolder.getShopMenuChildBinding().getShopChildInfo().oid;
            baseModel.getMenuDetailInfo(childOid, baseCallback.addMenuDetail());
        } else {
            baseView.showAlert(R.string.retry);
        }
    }

    /*매장 상단 이미지 페이지 변경 리스너*/
    @Override
    public void onPageSelected(int position) {
        baseView.binding.shopDetailPictureLayout.shopPictureIndicator.selectDot(position);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onLikeTapLongEnd(int count) {
        baseModel.sendLikeCount(baseCallback.getLikeCountCallback(), baseView.binding.getShopDetail().getOid(), count);
    }

    @Override
    public boolean onLikeTap(int count, boolean isLongTouch) {
        switch (baseView.animateLike()) {
            case PLAY:
                if (!isLongTouch)
                    baseModel.sendLikeCount(baseCallback.getLikeCountCallback(), baseView.binding.getShopDetail().getOid(), 1);
                return isLongTouch;
            case RESET:
                if (isLongTouch)
                    baseModel.sendLikeCount(baseCallback.getLikeCountCallback(), baseView.binding.getShopDetail().getOid(), count);
                new MergeDialog.Builder(baseView.activity)
                        .setContent(R.string.shop_like_text)
                        .setConfirmString(R.string.shop_like_reset).setCancelBtn(false)
                        .setConfirmClick(() -> {
                            baseModel.sendLikeCount(baseCallback.getLikeCountCallback(), baseView.binding.getShopDetail().getOid(), baseView.binding.getShopDetail().maxLikeCount + 1);
                            baseView.binding.getShopDetail().setGivenLikeCount(0);
                        }).build().show();
                return false;
            case NONMEMBER:
                baseView.showAlert(R.string.please_login);
                return false;
            case NOTBUY:
                new MergeDialog.Builder(baseView.activity)
                        .setContent(R.string.shop_like_not_buy)
                        .setCancelBtn(false).build().show();
                return false;
        }
        return false;
    }

    @Override
    public void checkFavorite(View view, boolean isLogin) {
        if (isLogin) {
            baseModel.checkFavoriteCall(baseCallback.getCheckFavoriteCall(), baseView.binding.getShopDetail().getOid());
        } else {
            baseView.showAlert(R.string.please_login);
        }
    }

    public void onStop() {
        payEndResultRunnable = null;
    }

    int paymentRef;
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MDEBUG.debug("Activity Result" + requestCode +"  :  "+ resultCode);
        if (resultCode == RESULT_OK) {
            if (requestCode == PAY_RESULT_REQUEST) {
                paymentRef = data.getIntExtra("paymentRef",-1);
                payEndResultRunnable = () -> baseView.openPaymentResult(paymentRef,data.getBooleanExtra("isMulti",false));
            }else if (requestCode == PAY_PRICE_REQUEST){
                int price = data.getIntExtra("price", -1);
                int shopRef = baseView.binding.getShopDetail().oid;

                baseView.openPaymentByPrice(price,shopRef);
            }
        }
        /*페이스북 콜백 매니져*/
        if (baseView.getShareFacebookCallback() != null)
            baseView.getShareFacebookCallback().onActivityResult(requestCode, resultCode, data);
    }

    public void onBackPressed() {
        baseView.onBackPressed();
    }

    @Override
    public void addReviews(int page) {
        if (baseView.binding != null) {
            ShopDetail detail = baseView.binding.getShopDetail();
            if (detail != null) {
//                baseModel.requestGoogleReview(baseCallback.getGoogleReviewCallback(), page, baseView.getString(R.string.google_search_query, detail.shopName, MergeApplication.getStreetName(detail.streetCodeRef)));
                baseModel.requestMergeReview(baseCallback.getMergeReview(), detail.oid, page);
            }
        }
    }
}
