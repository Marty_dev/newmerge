package kr.co.mergepoint.mergeclient.scene.privacy.model;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.api.EnterpriseApi;
import kr.co.mergepoint.mergeclient.api.UserApi;
import kr.co.mergepoint.mergeclient.application.common.BaseModel;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.main.Company;
import retrofit2.Callback;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class PrivacyModel extends BaseModel {

    private Member member;
    private UserApi userApi;
    private EnterpriseApi enterpriseApi;

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public PrivacyModel() {
        userApi = retrofit.create(UserApi.class);
        enterpriseApi = retrofit.create(EnterpriseApi.class);
    }

    public void getUserPrivacy(Callback<ResponseObject<Member>> callback, String password) {
        userApi.getUserPrivacy(password).enqueue(callback);
    }

    public void sendModifyInfo(Callback<ResponseObject<Member>> callback) {
        userApi.userModifyInfo(member).enqueue(callback);
    }

    public void searchEnterprise(Callback<ResponseObject<ArrayList<Company>>> callback, String searchText) {
        enterpriseApi.getSearchEnterprise(searchText).enqueue(callback);
    }

    public void registerEnterprise(Callback<ResponseObject<Boolean>> callback, int companyRef) {
        enterpriseApi.registerEnterprise(companyRef).enqueue(callback);
    }
}
