package kr.co.mergepoint.mergeclient.scene.search.presenter;

import android.Manifest;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasePresenter;
import kr.co.mergepoint.mergeclient.application.common.LoadRecyclerView;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocation;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocationListener;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.ShopInfo;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.EnterpriseShopActivity;
import kr.co.mergepoint.mergeclient.scene.search.model.SearchModel;
import kr.co.mergepoint.mergeclient.scene.search.presenter.callback.SearchCallback;
import kr.co.mergepoint.mergeclient.scene.search.view.SearchView;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.ShopListAdapter;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;

import static android.app.Activity.RESULT_OK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ADD;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CORPORATE_ALLIANCE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CORPORATE_CAFETERIA;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FRANCHISE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.GENERAL_PARTNERSHIP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INITIAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MAIN_SEARCH;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PERMISSIONS_REQUEST_LOCATION;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_FAVORITE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_OID;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class SearchPresenter extends BasePresenter<SearchView, SearchModel, SearchCallback> implements TextView.OnEditorActionListener, LoadRecyclerView.LoadRecyclerViewListener, UserLocationListener {

    private UserLocation userLocation;

    public SearchPresenter(SearchView baseView, SearchModel baseModel, SearchCallback callback, UserLocation userLocation) {
        super(baseView, baseModel, callback);
        this.userLocation = userLocation;
    }

    @Override
    protected void setBindingProperty() {
        baseView.binding.setTitle(baseView.getString(R.string.search_shop_title));
        baseView.binding.setOnClick(this);
        baseView.binding.setListListener(this);
        baseView.binding.setEditorListener(this);
        baseView.binding.setSearchWatcher(getTextWatcher());
        baseView.binding.setListAdapter(new ShopListAdapter());
    }

    public void onCreate() {
        Intent intent = baseView.activity.getIntent();
        if (intent != null)
            baseView.setSearchText(intent.getStringExtra(MAIN_SEARCH));
        userLocation.startLocationUpdates();
    }

    /*위치 권한*/
    @Override
    protected void grantedLocation() {
        userLocation.startLocationUpdates();
    }

    @Override
    protected void deniedLocation() {
        baseView.binding.searchList.loadShopList(INITIAL, baseModel.filter.getListFilter());
    }

    /*위치 권한 요청 및 위치에 따른 리스트 업데이트*/
    @Override
    public void requestPermissions() {
        baseView.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_LOCATION);
    }

    @Override
    public void startLoadList(Location location) {
        MDEBUG.debug("Loading Start");
        baseView.showLoading();
        baseModel.filter.setLocation(location);
        baseModel.filter.setSearchString(baseView.getSearchText());
        baseView.binding.searchList.loadShopList(INITIAL, baseModel.filter.getMainSearch());
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.search_text_delete:
                baseView.searchTextClear();
                break;
        }
    }

    /* 검색 클릭 리스너 */
    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            baseView.searchClearFocused();
            baseView.hideKeyboard();

            if (textView.length() > 0) {
                userLocation.startLocationUpdates();
            } else {
                baseView.showAlert(R.string.input_search);
            }
            return true;
        }

        return false;
    }

    /* 리스트 터치 */
    @Override
    public void onTouchItem(RecyclerView recyclerView, View view, int position) {
        ShopListAdapter adapter = baseView.binding.searchList.getShopListAdapter();
        Shop shop = adapter.getObservableArrayList().get(position);
        if (shop.type == GENERAL_PARTNERSHIP || shop.type == FRANCHISE) {
            baseView.openActivityForResultWithParcelable(ShopActivity.class, SHOP_INFO, shop, SHOP_INFO_REQUEST);
        } else if (shop.type == CORPORATE_ALLIANCE || shop.type == CORPORATE_CAFETERIA) {
            baseView.openActivityForResultWithParcelable(EnterpriseShopActivity.class, SHOP_INFO, shop, SHOP_INFO_REQUEST);
        }
    }

    /* 리스트 로드 */
    @Override
    public void onLoadItem(LoadRecyclerView loadRecyclerView, int currentPage) {
        baseView.binding.searchList.loadShopList(ADD, baseModel.filter.getLoadFilter(currentPage));
    }

    @Override
    public void onInitSuccess(ShopInfo shopInfo) {
        ShopListAdapter adapter = baseView.binding.searchList.getShopListAdapter();
        adapter.updateShopList(shopInfo.getShopList());

        if (baseView.binding.searchList.getListLoadScrollListener() != null)
            baseView.binding.searchList.getListLoadScrollListener().resetScroll();

        baseView.binding.searchList.setTotalPage(shopInfo.getTotalPage());
        baseView.hideLoading();
        baseView.emptyResults(shopInfo.getListSize() == 0);
    }

    @Override
    public void onAddSuccess(final ShopInfo shopInfo) {
        final ShopListAdapter adapter = baseView.binding.searchList.getShopListAdapter();
        baseView.activity.runOnUiThread(() -> {
            adapter.addShopList(shopInfo.getShopList());
            baseView.binding.searchList.setTotalPage(shopInfo.getTotalPage());
            baseView.hideLoading();
        });
    }

    @Override
    public void onResponseFailure(@NonNull Call<ShopInfo> call) {
        baseView.showAlert(R.string.retry);
        baseView.binding.notSearch.setVisibility(View.VISIBLE);
    }

    /* 검색 텍스트 리스너 */
    private TextWatcher getTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (baseView.isSearchFocused())
                    baseView.visibleTextClear(s.length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        };
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SHOP_INFO_REQUEST) {
                boolean isFavorite = data.getBooleanExtra(SHOP_FAVORITE, false);
                int oid = data.getIntExtra(SHOP_OID, NO_VALUE);
                ShopListAdapter shopListAdapter = baseView.getShopListAdapter();
                shopListAdapter.singleShopFavoriteUpdate(oid, isFavorite);
            }
        }
    }
}
