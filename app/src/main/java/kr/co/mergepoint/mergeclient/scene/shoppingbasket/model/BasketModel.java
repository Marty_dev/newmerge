package kr.co.mergepoint.mergeclient.scene.shoppingbasket.model;

import kr.co.mergepoint.mergeclient.api.BasketApi;
import kr.co.mergepoint.mergeclient.application.common.BaseModel;
import kr.co.mergepoint.mergeclient.scene.data.basket.Basket;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import retrofit2.Callback;

/**
 * Created by 1017sjg on 2017. 6. 28..
 */

public class BasketModel extends BaseModel{

    private BasketApi basketApi;

    public BasketModel() {
        basketApi = retrofit.create(BasketApi.class);
    }

    public void getUserShoppingBasket(Callback<Basket> callback) {
        basketApi.getUserShoppingBasket().enqueue(callback);
    }

    public void modifyShoppingBasketMenu(Callback<ResponseObject<Integer>> callback, int menuOid, int count) {
        basketApi.modifyShoppingBasketMenu(menuOid, count).enqueue(callback);
    }

    public void deleteMenu(Callback<ResponseObject<Integer>> callback, int menuOid) {
        basketApi.deleteShoppingBasket(menuOid).enqueue(callback);
    }
}
