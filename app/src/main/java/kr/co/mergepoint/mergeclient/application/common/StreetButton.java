package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;

import kr.co.mergepoint.mergeclient.R;

/**
 * Created by 1017sjg on 2017. 9. 14..
 */

public class StreetButton extends AppCompatButton implements View.OnClickListener {

    private OnStreetListener onStreetListener;
    public interface OnStreetListener {
        void onMainStreetTouch(String streetName);
    }

    public StreetButton(Context context) {
        super(context);
    }

    public StreetButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StreetButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public StreetButton(Context context, OnStreetListener onStreetListener, String text) {
        super(context);
        init(onStreetListener, text);
    }

    private void init(OnStreetListener onStreetListener, String text) {
        setOnClickListener(this);
        setId(View.generateViewId());
        setText(text);
        setBackground(ContextCompat.getDrawable(getContext(), R.drawable.main_street_selector));
        setTextColor(ContextCompat.getColorStateList(getContext(), R.color.main_street_text_selector));
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
        setGravity(Gravity.CENTER);
        setMinWidth(0);
        setMinHeight(0);
        setMinimumWidth(0);
        setMinimumHeight(0);
        setPadding(0,0,0,0);

        this.onStreetListener = onStreetListener;
    }

    @Override
    public void onClick(View view) {
        if (onStreetListener != null)
            onStreetListener.onMainStreetTouch(getText().toString());
    }
}
