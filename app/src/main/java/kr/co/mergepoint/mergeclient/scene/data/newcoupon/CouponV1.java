package kr.co.mergepoint.mergeclient.scene.data.newcoupon;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import kr.co.mergepoint.mergeclient.api.NewCouponApi;

/**
 * User: Marty
 * Date: 2018-09-17
 * Time: 오후 2:13
 * Description:
 */
public class CouponV1 {
    @SerializedName("consummerName")
    private String consummerName;
    @Expose
    @SerializedName("couponImage")
    private String couponImage;
    @Expose
    @SerializedName("useMethod")
    private String useMethod;
    @Expose
    @SerializedName("barCodeValidityInfo")
    private String barCodeValidityInfo;
    @Expose
    @SerializedName("barCodeValidity")
    private String barCodeValidity;
    @Expose
    @SerializedName("expValidity")
    private String expValidity;
    @Expose
    @SerializedName("expValidity1")
    private String expValidity1;
    @Expose
    @SerializedName("useCondition")
    private String useCondition;
    @Expose
    @SerializedName("couponState")
    private int couponState;
    @Expose
    @SerializedName("expDate")
    private ArrayList<Integer> expDate;
    @Expose
    @SerializedName("issueExpDate")
    private ArrayList<Integer> issueExpDate;
    @Expose
    @SerializedName("couponMinPrice")
    private int couponMinPrice;
    @Expose
    @SerializedName("couponDescription")
    private String couponDescription;

    @Expose
    @SerializedName("smallDescription")
    private String smallDescription;
    @Expose
    @SerializedName("couponName")
    private String couponName;
    @Expose
    @SerializedName("couponCategory")
    private int couponCategory;
    @Expose
    @SerializedName("shopRef")
    private int shopRef;
    @Expose
    @SerializedName("couponRef")
    private int couponRef;
    @Expose
    @SerializedName("oid")
    private int oid;
    @Expose
    @SerializedName("priceBigtext")
    private int priceBigtext;
    @Expose
    @SerializedName("priceSmalltext")
    private int priceSmalltext;

    public String getSmallDescription() {
        return smallDescription;
    }

    public void setSmallDescription(String smallDescription) {
        this.smallDescription = smallDescription;
    }


    public String getConsummerName() {
        return consummerName;
    }

    public void setConsummerName(String consummerName) {
        this.consummerName = consummerName;
    }

    public String getCouponImage() {
        return couponImage;
    }

    public void setCouponImage(String couponImage) {
        this.couponImage = couponImage;
    }

    public ArrayList<Integer> getIssueExpDate() {
        return issueExpDate;
    }

    public void setIssueExpDate(ArrayList<Integer> issueExpDate) {
        this.issueExpDate = issueExpDate;
    }

    public String getUseMethod() {
        return useMethod;
    }

    public void setUseMethod(String useMethod) {
        this.useMethod = useMethod;
    }

    public String getBarCodeValidityInfo() {
        return barCodeValidityInfo;
    }

    public void setBarCodeValidityInfo(String barCodeValidityInfo) {
        this.barCodeValidityInfo = barCodeValidityInfo;
    }

    public String getBarCodeValidity() {
        return barCodeValidity;
    }

    public void setBarCodeValidity(String barCodeValidity) {
        this.barCodeValidity = barCodeValidity;
    }

    public String getExpValidity() {
        return expValidity;
    }

    public void setExpValidity(String expValidity) {
        this.expValidity = expValidity;
    }

    public String getExpValidity1() {
        return expValidity1;
    }

    public void setExpValidity1(String expValidity1) {
        this.expValidity1 = expValidity1;
    }

    public String getUseCondition() {
        return useCondition;
    }

    public void setUseCondition(String useCondition) {
        this.useCondition = useCondition;
    }

    public int getCouponState() {
        return couponState;
    }

    public void setCouponState(int couponState) {
        this.couponState = couponState;
    }

    public ArrayList<Integer> getExpDate() {
        return expDate;
    }

    public void setExpDate(ArrayList<Integer> expDate) {
        this.expDate = expDate;
    }

    public int getCouponMinPrice() {
        return couponMinPrice;
    }

    public void setCouponMinPrice(int couponMinPrice) {
        this.couponMinPrice = couponMinPrice;
    }

    public String getCouponDescription() {
        return couponDescription;
    }

    public void setCouponDescription(String couponDescription) {
        this.couponDescription = couponDescription;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public int getCouponCategory() {
        return couponCategory;
    }

    public void setCouponCategory(int couponCategory) {
        this.couponCategory = couponCategory;
    }

    public int getShopRef() {
        return shopRef;
    }

    public void setShopRef(int shopRef) {
        this.shopRef = shopRef;
    }

    public int getCouponRef() {
        return couponRef;
    }

    public void setCouponRef(int couponRef) {
        this.couponRef = couponRef;
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }



}
