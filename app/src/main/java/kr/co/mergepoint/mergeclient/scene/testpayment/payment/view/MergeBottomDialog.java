package kr.co.mergepoint.mergeclient.scene.testpayment.payment.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.couponListener;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopCoupon;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopTicket;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.ticketListener;
import kr.co.mergepoint.mergeclient.util.MDEBUG;


/**
 * User: Marty
 * Date: 2018-07-18
 * Time: 오후 5:24
 * Description:
 */
public class MergeBottomDialog extends BottomSheetDialogFragment {
    ImageView ticketTitleClose;
    TextView ticketTitleTv;
    RelativeLayout ticketTitleRl;
    ImageView ticketCheckimg;
    TextView ticketNoneChoice;
    RelativeLayout ticketNoneChoicerl;
    ListView ticketlist;
    Unbinder unbinder;

    boolean isTicket;
    public ticketListener ticketlistener;
    public couponListener couponlistener;
    BaseActivity activity;

    ShopCoupon coupon;
    ShopTicket ticket;
    String title;

    TicketAdpater adapter;
    ArrayList<ShopCoupon> coupon_items;
    ArrayList<ShopTicket> ticket_items;

    int selectedoid;
    Viewholder selectedView;



    public void setCoupon(List<ShopCoupon> coupon, couponListener couponlistener) {
        this.coupon_items = (ArrayList<ShopCoupon>) coupon;
        this.couponlistener = couponlistener;
        isTicket = false;
        title = "쿠폰 선택";
    }

    public void setTicket(List<ShopTicket> ticket,ticketListener ticketlistener) {
        this.ticket_items = (ArrayList<ShopTicket>)ticket;
        this.ticketlistener = ticketlistener;
        isTicket = true;
        title = "식권 선택";
    }


    public static MergeBottomDialog getInstance(BaseActivity activity, int oid, boolean isTicket) {

        MergeBottomDialog dialog = new MergeBottomDialog();
        dialog.activity = activity;
        dialog.selectedoid = oid;
        dialog.isTicket = isTicket;
        return dialog;
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_bottomdialog, null);
        unbinder = ButterKnife.bind(this, view);

        ticketTitleTv = view.findViewById(R.id.ticket_title_tv);
        ticketNoneChoicerl = view.findViewById(R.id.ticket_none_choicerl);
        ticketCheckimg = (ImageView)view.findViewById(R.id.ticket_checkimg);
        ticketTitleClose = view.findViewById(R.id.ticket_title_close);
        ticketlist = (ListView)view.findViewById(R.id.ticketlist);
        ticketTitleTv.setText(title);
        if (isTicket)
            adapter = new TicketAdpater(ticket_items,true,selectedoid);
        else
            adapter = new TicketAdpater( coupon_items,selectedoid);

        adapter.selectview = selectedView;
        ticketTitleClose.setOnClickListener((View v)->{
            dismiss();
        });
        if (selectedoid != 0){
            ticketCheckimg.setImageResource(R.drawable.check_round);
        }

        ticketNoneChoicerl.setOnClickListener((view1 ->{
            ticketCheckimg.setImageResource(R.drawable.red_round);
            adapter.selectview = null;
            adapter.oid = 0;
            adapter.notifyDataSetChanged();
            dismiss();
          //  new Handler().postDelayed(()-> dismiss() ,1000);

            if (isTicket)
                ticketlistener.onTicketChoose(null);
            else
                couponlistener.onCouponChoose(null);

        }));

        ticketlist.setAdapter(adapter);

        /*
        ticketlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Viewholder views = (Viewholder)view.getTag();

                MDEBUG.debug("Seleceted View is Set?>??? : " + (adapter.selectview == null ? "null" : "none null") );
                MDEBUG.debug("oid is Set?>??? : " + selectedoid);

                if (adapter.selectview != null && selectedoid != 0) {
                    MDEBUG.debug("HERE????????????");
                    adapter.selectview.checkimg.setImageResource(R.drawable.cart);
                }
                views.checkimg.setImageResource(R.drawable.red_round);
                adapter.selectview = views;


                if (selectedoid != 0){
                    ticketCheckimg.setImageResource(R.drawable.check_round);
                }

                if (isTicket) {
                    ticket = (ShopTicket) adapterView.getItemAtPosition(i);
                    selectedoid = ticket.oid;

                   // ticketlistener.onTicketChoose(ticket);
                }
                else {
                    coupon = (ShopCoupon) adapterView.getItemAtPosition(i);
                    selectedoid = coupon.oid;
                  //  couponlistener.onCouponChoose(coupon);
                }

              //  new Handler().postDelayed(()-> dismiss() ,1000);

            }
        });
        */

        return view;
    }




    @Override
    public void onStart() {
        super.onStart();

        try {
            Field mBehaviorField = getDialog().getClass().getDeclaredField("mBehavior");
            mBehaviorField.setAccessible(true);
            final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(getDialog());
            behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (newState == BottomSheetBehavior.STATE_SETTLING || newState == BottomSheetBehavior.STATE_DRAGGING) {
                        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                }
            });
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void dismiss() {
        super.dismiss();

    }

    class TicketAdpater extends BaseAdapter{
        ArrayList<ShopCoupon> shopCoupons;
        ArrayList<ShopTicket> shopTickets;
        int oid;
        boolean isTicket;
        ShopCoupon cbj = null;
        ShopTicket tbj = null;
        public Viewholder selectview;

        public TicketAdpater(ArrayList<ShopCoupon> shopCoupons,int oid) {
            this.shopCoupons = shopCoupons;
            this.isTicket = false;
            this.oid = oid;
        }

        public TicketAdpater(ArrayList<ShopTicket> shopTickets,boolean isTicket,int oid) {
            this.shopTickets = shopTickets;
            this.isTicket =isTicket;
            this.oid = oid;
        }

        @Override
        public int getCount() {
            MDEBUG.debug("isTicket " + isTicket);
            return isTicket ? shopTickets.size() : shopCoupons.size();
        }

        @Override
        public Object getItem(int i) {
            return isTicket ? shopTickets.get(i) : shopCoupons.get(i);
        }

        @Override
        public long getItemId(int i) { return 0; }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            Viewholder viewholder;

            int currentoid = (isTicket ? shopTickets.get(i).oid : shopCoupons.get(i).oid);
            if (view == null){
                view = ((LayoutInflater)viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.ticket_list_item,null);
                viewholder = new Viewholder(view);
                view.setTag(viewholder);
            }else{
                viewholder = (Viewholder)view.getTag();
            }


            if (isTicket) {
                tbj = shopTickets.get(i);
                viewholder.ticketcount.setVisibility(View.VISIBLE);
                viewholder.ticketcount.setText("(남은 식권 수 " + tbj.remainTicketCount + "장)");
                viewholder.tickettitle.setText(tbj.ticketName);
                viewholder.ticketdescription.setText(tbj.ticketDescription);
            } else {
                cbj = shopCoupons.get(i);
                viewholder.tickettitle.setText(cbj.couponName);
                viewholder.ticketdescription.setText(cbj.couponDescription);
                viewholder.ticketcount.setVisibility(View.GONE);
            }
            if (oid == (isTicket ? shopTickets.get(i).oid : shopCoupons.get(i).oid)){

                viewholder.checkimg.setImageResource(R.drawable.red_round);
                selectview = viewholder;
            }else
                viewholder.checkimg.setImageResource(R.drawable.check_round);

            final ShopTicket selectticket = tbj;
            final ShopCoupon selectcoupon = cbj;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (oid == currentoid){
                        return;
                    }
                    oid = currentoid;
                    if (selectview != null)
                        selectview.checkimg.setImageResource(R.drawable.check_round);
                    viewholder.checkimg.setImageResource(R.drawable.red_round);
                    selectview = viewholder;
                    ticketCheckimg.setImageResource(R.drawable.check_round);
                    notifyDataSetChanged();
                    if (isTicket){
                        ticketlistener.onTicketChoose(selectticket);
                    }else {
                        couponlistener.onCouponChoose(selectcoupon);
                    }
                    dismiss();
                }
            });





            return view;
        }


    }
}

class Viewholder{
    public Viewholder(View view) {
        this.checkimg = (ImageView)view.findViewById(R.id.ticket_checkimg);
        this.tickettitle = (TextView)view.findViewById(R.id.ticket_list_title);
        this.ticketcount = (TextView)view.findViewById(R.id.ticket_list_count);
        this.ticketdescription = (TextView)view.findViewById(R.id.ticket_list_description);
    }

    public ImageView checkimg;
    public TextView tickettitle;
    public TextView ticketcount;
    public TextView ticketdescription;
}