package kr.co.mergepoint.mergeclient.application.module;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Field;
import java.net.CookieHandler;
import java.net.CookiePolicy;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.application.common.CustomCookieManager;
import kr.co.mergepoint.mergeclient.application.common.PersistentCookieStore;
import okhttp3.Cache;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static kr.co.mergepoint.mergeclient.application.common.Properties.BASE_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CONNECT_TIMEOUT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.READ_TIMEOUT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WRITE_TIMEOUT;

/**
 * Created by jgson on 2017. 6. 2..
 */

@Module
public class NetworkModule {

    private Context context;

    public NetworkModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Cache provideHttpCache() {
        return new Cache(context.getCacheDir(), 10 * 1024 * 1024);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkhttpClient(Cache cache, PersistentCookieStore cookieStore) {
        CustomCookieManager cookieManager = new CustomCookieManager(cookieStore, CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(cookieManager);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .cookieJar(new JavaNetCookieJar(cookieManager))
                .addInterceptor(interceptor)
                .cache(cache)
                .build();

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            try {
                SSLContext ssl;
                ssl = SSLContext.getInstance("SSL");
                ssl.init(null, new TrustManager[]{new X509TrustManager() {
                    @SuppressLint("TrustAllX509TrustManager")
                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {}
                    @SuppressLint("TrustAllX509TrustManager")
                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {}
                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                }}, new SecureRandom());

                String workerClassName = "okhttp3.OkHttpClient";
                try {
                    Class workerClass = Class.forName(workerClassName);
                    Field hostnameVerifier = workerClass.getDeclaredField("hostnameVerifier");
                    hostnameVerifier.setAccessible(true);
                    hostnameVerifier.set(okHttpClient, (HostnameVerifier) (s, sslSession) -> true);
                    Field sslSocketFactory = workerClass.getDeclaredField("sslSocketFactory");
                    sslSocketFactory.setAccessible(true);
                    sslSocketFactory.set(okHttpClient, ssl.getSocketFactory());
                } catch (Exception e) { e.printStackTrace(); }
            } catch (Exception e) {e.printStackTrace();}
        }

        return okHttpClient;
    }

    @Provides
    @Singleton
    PersistentCookieStore provideCookieStore() {
        return new PersistentCookieStore(context);
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .build();
    }
}
