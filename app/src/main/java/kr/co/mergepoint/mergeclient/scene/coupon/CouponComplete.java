package kr.co.mergepoint.mergeclient.scene.coupon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.PointFragment;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder.Menu;

import static kr.co.mergepoint.mergeclient.application.common.Properties.POINT_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UP_DOWN;

public class CouponComplete extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onActivityClick(View view) {

    }

    TextView newPoint ,totalPoint;
    Button toMenu , toList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_complete);
        ((TextView)(findViewById(R.id.ec_toolbar)).findViewById(R.id.toolbar_textbar)).setText("포인트 등록완료");
        ((findViewById(R.id.ec_toolbar)).findViewById(R.id.back)).setOnClickListener((view)->{
            Intent inte = new Intent(this, CouponApply.class);
            startActivity(inte);finish();
        });
        newPoint = (TextView)findViewById(R.id.coupon_point_tv);
        totalPoint = (TextView)findViewById(R.id.coupon_total_tv);
        toMenu = (Button)findViewById(R.id.coupon_tomymenu_btn);
        toList = (Button)findViewById(R.id.coupon_topointlist_btn);

        toMenu.setOnClickListener(this);
        toList.setOnClickListener(this);
        String newpoint ="신규 등록 포인트 : " +  String.format("%,d",getIntent().getIntExtra("addPoints",0)) + "원";
        String totalpoint = "전체 포인트 : " +  String.format("%,d",getIntent().getIntExtra("newPoints",0)) + "원" +
                "";

        newPoint.setText(newpoint);
        totalPoint.setText(totalpoint);



    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.coupon_tomymenu_btn){
            finish();
        }else if (view.getId() == R.id.coupon_topointlist_btn){
            this.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(getEnterAnim(RIGHT_LEFT), getExitAnim(RIGHT_LEFT), getPopEnterAnim(RIGHT_LEFT), getPopExitAnim(RIGHT_LEFT))
                    .add(R.id.coupon_content,PointFragment.newInstance(), POINT_FRAGMENT_TAG)
                    .addToBackStack(POINT_FRAGMENT_TAG).commit();
        } else if (view.getId() == R.id.back){
            finish();
        }
    }


    protected int getEnterAnim(@Properties.FragmentAnim int anim) {
        return anim == UP_DOWN ? R.anim.slide_from_down : R.anim.slide_from_right;
    }

    protected int getExitAnim(@Properties.FragmentAnim int anim) {
        return anim == UP_DOWN ? 0 : R.anim.slide_to_left;
    }

    protected int getPopEnterAnim(@Properties.FragmentAnim int anim) {
        return anim == UP_DOWN ? 0 : R.anim.slide_pop_from_right;
    }

    protected int getPopExitAnim(@Properties.FragmentAnim int anim) {
        return anim == UP_DOWN ? R.anim.slide_to_down : R.anim.slide_pop_to_right;
    }
}
