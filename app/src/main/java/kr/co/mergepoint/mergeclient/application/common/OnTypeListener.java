package kr.co.mergepoint.mergeclient.application.common;

/**
 * Created by 1017sjg on 2017. 9. 14..
 */

public interface OnTypeListener {
    void onTypeClick(@Properties.TouchType int type, String data);
}
