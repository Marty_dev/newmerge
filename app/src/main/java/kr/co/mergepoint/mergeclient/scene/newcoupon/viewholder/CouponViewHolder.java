package kr.co.mergepoint.mergeclient.scene.newcoupon.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import kr.co.mergepoint.mergeclient.R;

/**
 * User: Marty
 * Date: 2018-09-17
 * Time: 오전 10:19
 * Description:
 */
public class CouponViewHolder extends RecyclerView.ViewHolder {
    public TextView couponShopName;
    public TextView couponPriceTv;
    public TextView couponPriceBase;
    public TextView couponDateTv;
    public LinearLayout couponDetailLl;
    public RelativeLayout couponTopRl;
    public TextView couponBottomBase;
    public TextView couponBarcodeBtn;
    public TextView couponDeleteBtn;
    public RelativeLayout couponBottomTwobtn;
    public RelativeLayout couponBottomRl;
    public ImageView couponLogo;
    public ImageView couponRefund;
    public TextView coupondetailbtn;
    public RelativeLayout another;

    public CouponViewHolder(View itemView) {
        super(itemView);
        couponShopName = (TextView)itemView.findViewById(R.id.coupon_shop_name);
        couponPriceTv = (TextView)itemView.findViewById(R.id.coupon_price_tv);
        couponPriceBase = (TextView)itemView.findViewById(R.id.coupon_price_base);
        couponDateTv = (TextView)itemView.findViewById(R.id.coupon_date_tv);
        couponDetailLl = (LinearLayout)itemView.findViewById(R.id.coupon_detail_ll);
        couponTopRl = (RelativeLayout)itemView.findViewById(R.id.coupon_top_rl);
        couponBottomBase = (TextView)itemView.findViewById(R.id.coupon_bottom_base);
        couponBarcodeBtn = (TextView)itemView.findViewById(R.id.coupon_barcode_btn);
        couponDeleteBtn = (TextView)itemView.findViewById(R.id.coupon_delete_btn);
        couponBottomTwobtn = (RelativeLayout)itemView.findViewById(R.id.coupon_bottom_twobtn);
        couponBottomRl = (RelativeLayout)itemView.findViewById(R.id.coupon_bottom_rl);
        couponLogo = (ImageView)itemView.findViewById(R.id.coupon_logo_img);
        couponRefund = (ImageView)itemView.findViewById(R.id.coupon_none_img);
        coupondetailbtn =(TextView)itemView.findViewById(R.id.coupon_bottom_detailbtn);
        another = (RelativeLayout)itemView.findViewById(R.id.coupon_another_rl);

    }
}
