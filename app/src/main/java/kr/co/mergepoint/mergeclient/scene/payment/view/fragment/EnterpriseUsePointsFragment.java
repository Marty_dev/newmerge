package kr.co.mergepoint.mergeclient.scene.payment.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.EnterpriseUsePointsBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.payment.view.adapter.CrowdPayListAdapter;

/**
 * Created by 1017sjg on 2017. 8. 8..
 */

public class EnterpriseUsePointsFragment extends BaseFragment {

    private EnterpriseUsePointsBinding usePointsBinding;
    private ShopPayment shopPayment;
    private boolean single;
    private ArrayList<CrowdPay> crowdPays;
    private CrowdPay myCrowdPay;
    private int remainPayPrice;

    public int getRemainPayPrice() {
        return usePointsBinding.getRemainPayPrice();
    }

    public int getPayShopRef() {
        return shopPayment.oid;
    }

    public static EnterpriseUsePointsFragment newInstance(ShopPayment shopPayment, ArrayList<CrowdPay> crowdPays, boolean single) {
        Bundle args = new Bundle();
        EnterpriseUsePointsFragment fragment = new EnterpriseUsePointsFragment();
        fragment.shopPayment = shopPayment;
        fragment.remainPayPrice = shopPayment.payPrice;
        fragment.crowdPays = crowdPays;
        fragment.single = single;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        usePointsBinding = DataBindingUtil.inflate(inflater, R.layout.enterprise_use_points, container, false);
        usePointsBinding.setTitle("기업포인트 사용");
        usePointsBinding.setOnClick(this);
        usePointsBinding.setPayShopOid(shopPayment.oid);
        usePointsBinding.setIsCrowedPay(single);
        usePointsBinding.setRemainPayPrice(remainPayPrice);

        setEnterpriseUserAdapter(crowdPays);
        usePointsBinding.setMyCrowdPay(myCrowdPay);
        setPointPrice();
        return usePointsBinding.getRoot();
    }

    public void setCrowdPayList(ArrayList<CrowdPay> crowdPays) {
        this.crowdPays = crowdPays;
    }

    private void setPointPrice() {
        int companyPoint = 0;
        int returnPoint = 0;
        for (CrowdPay crowdPay :crowdPays) {
            companyPoint+=crowdPay.allocatePoint;
            returnPoint+=crowdPay.returnPoints;
        }
        usePointsBinding.setCompanyPoint(companyPoint + myCrowdPay.allocatePoint);
        usePointsBinding.setReturnPoint(returnPoint + myCrowdPay.returnPoints);
        usePointsBinding.setTotalPoint(usePointsBinding.getCompanyPoint() - usePointsBinding.getReturnPoint());
        usePointsBinding.setRemainPayPrice(remainPayPrice - usePointsBinding.getTotalPoint());
    }

    private void setCrowdPays(ArrayList<CrowdPay> crowdPays) {
        Member member = MergeApplication.getMember();
        if (member != null) {
            int memberOid = member.getOid();
            for (CrowdPay crowdPay : crowdPays) {
                if (memberOid == crowdPay.memberRef) {
                    myCrowdPay = crowdPay;
                    crowdPays.remove(crowdPay);
                }
            }
        }
        usePointsBinding.enterprisePointApply.setText(crowdPays != null && crowdPays.size() > 0 ? "승인요청" : "적용하기");
    }

    public void setEnterpriseUserAdapter(ArrayList<CrowdPay> arrayList) {
        setCrowdPays(arrayList);
        crowdPays = arrayList;
        usePointsBinding.setCurrentSelectUserCount(crowdPays.size());
        usePointsBinding.setEnterpriseUserAdapter(new CrowdPayListAdapter(crowdPays, this));
        setPointPrice();
    }
}
