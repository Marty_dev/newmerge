package kr.co.mergepoint.mergeclient.scene.data.shop.review;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jgson on 2018. 1. 3..
 */

public class Thumbnail {
    @SerializedName("src")
    public String src;
}
