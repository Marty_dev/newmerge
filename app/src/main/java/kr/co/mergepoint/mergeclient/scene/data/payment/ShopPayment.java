package kr.co.mergepoint.mergeclient.scene.data.payment;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import kr.co.mergepoint.mergeclient.BR;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class ShopPayment extends BaseObservable {
    @SerializedName("oid")
    public int oid;

    @SerializedName("shopType")
    public int shopType;

    @SerializedName("memberRef")
    public int memberRef;

    @SerializedName("shopRef")
    public int shopRef;

    @SerializedName("shopName")
    public String shopName;

    @SerializedName("orderPrice")
    public int orderPrice;

    @SerializedName("pointPrice")
    public int pointPrice;

    @SerializedName("couponRef")
    public int couponRef;

    @SerializedName("couponPrice")
    public int couponPrice;

    @SerializedName("payPrice")
    public int payPrice;

    @SerializedName("settlementState")
    public int settlementState;

    @SerializedName("useState")
    public int useState;

    @SerializedName("companyPoints")
    public int companyPoints;

    @SerializedName("returnPoints")
    public int returnPoints;

    @SerializedName("shopCoupon")
    public ArrayList<Coupon> shopCoupon;

    @SerializedName("menu")
    public ArrayList<ShopMenuDetail> menu;

    @SerializedName("crowdPay")
    public ArrayList<CrowdPay> crowdPay;

    @SerializedName("attendeeCount")
    public int attendeeCount;

    @SerializedName("usePoint")
    public int usePoint;

    @SerializedName("consummerRef")
    public int consummerRef;

    public List<PaymentMenuGroup> menuGroups;

    @Bindable
    public int getCompanyPoints() {
        return companyPoints;
    }

    public void setCompanyPoints(int companyPoints) {
        this.companyPoints = companyPoints;
        notifyPropertyChanged(BR.companyPoints);
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof ShopPayment)) {
            return false;
        }

        ShopPayment shopPayment = (ShopPayment) o;
        return shopPayment.oid == oid;
    }
}
