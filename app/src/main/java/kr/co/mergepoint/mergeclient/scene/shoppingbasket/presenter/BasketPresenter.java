package kr.co.mergepoint.mergeclient.scene.shoppingbasket.presenter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasePresenter;
import kr.co.mergepoint.mergeclient.application.common.CustomCountView;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.model.BasketModel;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.presenter.callback.BasketCallback;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.view.BasketView;

import static android.app.Activity.RESULT_OK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_REQUEST;

/**
 * Created by 1017sjg on 2017. 6. 28..
 */

public class BasketPresenter extends BasePresenter<BasketView, BasketModel, BasketCallback> {

    public BasketPresenter(BasketView baseView, BasketModel baseModel, BasketCallback callback) {
        super(baseView, baseModel, callback);
    }

    @Override
    protected void setBindingProperty() {
        baseView.binding.setTitle(baseView.getString(R.string.basket_title));
        baseView.binding.setOnClick(this);
    }

    public void onCreate(Bundle savedInstanceState) {
        baseView.showLoading();
        baseModel.getUserShoppingBasket(baseCallback.getShoppingBasketCallback());
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.basket_payment:
                ArrayList<Integer> selectOid = baseView.binding.getBasketAdapter().getSelectMenuOid();
                if (selectOid.size() > 0) {
                    baseView.showPaymentView(baseView.binding.getBasketAdapter().getSelectMenuOid());
                } else {
                    baseView.showAlert(R.id.check_menu);
                }
                break;
            case R.id.shop_basekt_detail:
                baseView.openMenuDetail(view);
                break;
            case R.id.menu_delete:
                baseModel.deleteMenu(baseCallback.getDeleteMenuCallback(view), (int) view.getTag());
                break;
        }
    }

    public void changeCount(final CustomCountView view, boolean isPlus) {
        final int menuCount = isPlus ? view.currentCount() + 1 : view.currentCount() - 1;
        baseModel.modifyShoppingBasketMenu(baseCallback.getModifyShoppingBasketMenuCallback(view, menuCount), view.optionSelectOid, menuCount);
        baseView.binding.getBasketAdapter().setMenuCount(view, menuCount);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == PAY_RESULT_REQUEST)
            baseView.setResultFinish(data);
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        baseView.setMenuCheck(compoundButton, isChecked);
    }
}
