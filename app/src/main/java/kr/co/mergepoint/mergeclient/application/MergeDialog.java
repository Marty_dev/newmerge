package kr.co.mergepoint.mergeclient.application;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDialog;
import android.text.Spanned;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.databinding.AlertBinding;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class MergeDialog implements View.OnClickListener {
    private final BaseActivity activity;
    private final String content;

    private final String confirmString;
    private final String cancelString;
    private final boolean isConfirmBtn;
    private final boolean isCancelBtn;
    private final Spanned htmlText;

    private OnDialogClick onConfirmClick;
    private OnDialogClick onCancelClick;

    private AppCompatDialog currentAlert;

    public interface OnDialogClick {
        void onDialogClick();
    }

    public static class Builder {
        private final BaseActivity activity;

        private String content;
        private String confirmString;
        private String cancelString;

        private boolean isConfirmBtn;
        private boolean isCancelBtn;
        private Spanned htmlText;

        private OnDialogClick onConfirmClick;
        private OnDialogClick onCancelClick;

        public Builder (BaseActivity activity) {
            this.activity = activity;
            this.isConfirmBtn = true;
            this.isCancelBtn = true;
        }

        public Builder (Context activity) {
            this.activity = (BaseActivity) activity;
            this.isConfirmBtn = true;
            this.isCancelBtn = true;
        }

        public Builder setContent(String content) {
            this.content = content;
            return this;
        }

        public Builder setContent(int content) {
            this.content = activity.getResources().getString(content);
            return this;
        }

        public Builder setConfirmString(String confirmString) {
            this.confirmString = confirmString;
            return this;
        }

        public Builder setConfirmString(int confirmString) {
            this.confirmString = activity.getResources().getString(confirmString);
            return this;
        }

        public Builder setCancelString(String cancelString) {
            this.cancelString = cancelString;
            return this;
        }

        public Builder setCancelString(int cancelString) {
            this.cancelString = activity.getResources().getString(cancelString);
            return this;
        }

        public Builder setHtmlText(Spanned htmlText) {
            this.htmlText = htmlText;
            return this;
        }

        public Builder setConfirmBtn(boolean isConfirmBtn) {
            this.isConfirmBtn = isConfirmBtn;
            return this;
        }

        public Builder setCancelBtn(boolean isCancelBtn) {
            this.isCancelBtn = isCancelBtn;
            return this;
        }

        public Builder setConfirmClick(OnDialogClick onConfirmClick) {
            this.onConfirmClick = onConfirmClick;
            return this;
        }

        public Builder setCancelClick(OnDialogClick onCancelClick) {
            this.onCancelClick = onCancelClick;
            return this;
        }

        public MergeDialog build() {
            return new MergeDialog(this);
        }
    }

    private MergeDialog(Builder builder) {
        this.activity = builder.activity;
        this.content = builder.content;
        this.confirmString = builder.confirmString == null ? "확인" : builder.confirmString;
        this.cancelString = builder.cancelString == null ? "취소" : builder.cancelString;
        this.isConfirmBtn = builder.isConfirmBtn;
        this.isCancelBtn = builder.isCancelBtn;
        this.htmlText = builder.htmlText;
        this.onConfirmClick = builder.onConfirmClick;
        this.onCancelClick = builder.onCancelClick;
    }

    public void show() {
        if (activity != null && !activity.isDestroyed()) {
            AlertBinding alert = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.alert, null, false);
            if (content != null && !content.isEmpty()) {
                alert.contentText.setText(content);
            } else {
                alert.contentText.setText(htmlText);
            }
            alert.setOnConfirmClick(MergeDialog.this);
            alert.setOnCancelClick(MergeDialog.this);
            alert.setConfirmString(confirmString);
            alert.setCancelString(cancelString);
            alert.setIsConfirmBtn(isConfirmBtn);
            alert.setIsCancelBtn(isCancelBtn);
            if (!isCancelBtn) {
                alert.confirm.setBackground(ContextCompat.getDrawable(activity, R.drawable.payment_bottom));
            }

            if (!isConfirmBtn) {
                alert.cancel.setBackground(ContextCompat.getDrawable(activity, R.drawable.payment_bottom));
            }

            currentAlert = new AppCompatDialog(activity, R.style.AlertDialog);
            currentAlert.setContentView(alert.getRoot());
            currentAlert.setOnKeyListener((dialogInterface, keyCode, keyEvent) -> {
                if (KeyEvent.KEYCODE_BACK == keyCode) {
                    currentAlert.dismiss();
                    if (onCancelClick != null)
                        onCancelClick.onDialogClick();
                    return true;
                }
                return false;
            });
            Window window = currentAlert.getWindow();
            if (window != null)
                window.getAttributes().windowAnimations = R.style.MergeDialogAnim;
            currentAlert.show();
        }
    }

    @Override
    public void onClick(View view) {
        if (!activity.isDestroyed() && currentAlert != null)
            currentAlert.dismiss();
        switch (view.getId()) {
            case R.id.confirm:
                if (onConfirmClick != null)
                    onConfirmClick.onDialogClick();
                break;
            case R.id.cancel:
                if (onCancelClick != null)
                    onCancelClick.onDialogClick();
                break;
            default:
                break;
        }
    }
}
