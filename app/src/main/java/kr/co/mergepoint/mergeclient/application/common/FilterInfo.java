package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.View;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NORMAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SELECT;

/**
 * Created by jgson on 2017. 7. 8..
 */

public class FilterInfo extends AppCompatButton implements View.OnClickListener{

    private Drawable selectDrawable;
    private Drawable nonSelectDrawable;
    private int select;

    @Properties.StateButton
    public int getSelect() {
        return select;
    }

    public void setSelect(@Properties.StateButton int select) {
        this.select = select;
    }

    public void setImageDrawable(Drawable select, Drawable nonSelect) {
        this.selectDrawable = select;
        this.nonSelectDrawable = nonSelect;
    }

    public FilterInfo(Context context) {
        super(context);
        init();
    }

    public FilterInfo(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FilterInfo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setSelect(NORMAL);
        setOnClickListener(this);
        setBackground(getSelect() == SELECT ? selectDrawable : nonSelectDrawable);
    }

    @Override
    public void onClick(View v) {
        invalidate();
        setSelect(getSelect() == SELECT ? NORMAL : SELECT);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        setBackground(getSelect() == SELECT ? selectDrawable : nonSelectDrawable);
    }
}
