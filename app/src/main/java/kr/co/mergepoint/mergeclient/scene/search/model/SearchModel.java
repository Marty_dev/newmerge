package kr.co.mergepoint.mergeclient.scene.search.model;

import kr.co.mergepoint.mergeclient.application.common.BaseModel;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Filter;

import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class SearchModel extends BaseModel {

    public Filter filter;

    public SearchModel() {
        filter = new Filter(LIST);
    }
}
