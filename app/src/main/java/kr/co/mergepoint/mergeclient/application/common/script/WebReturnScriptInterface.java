package kr.co.mergepoint.mergeclient.application.common.script;

import android.webkit.JavascriptInterface;

/**
 * Created by 1017sjg on 2017. 7. 21..
 */

public class WebReturnScriptInterface {

    private WebReturnScriptListener listener;

    public WebReturnScriptInterface(WebReturnScriptListener listener) {
        this.listener = listener;
    }

    @JavascriptInterface
    public void success() {
        listener.success();
    }

    @JavascriptInterface
    public void failure(String error) {
        listener.failure(error);
    }
}
