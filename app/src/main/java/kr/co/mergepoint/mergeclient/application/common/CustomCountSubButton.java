package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.View;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.shop.view.listener.OnRadioChange;
import kr.co.mergepoint.mergeclient.scene.shop.view.listener.RadioObserver;

/**
 * Created by 1017sjg on 2017. 7. 3..
 */

public class CustomCountSubButton extends AppCompatButton implements RadioObserver {

    public int selectOptionOid;
    public OnRadioChange onRadioChange;

    public CustomCountSubButton(Context context) {
        super(context);
    }

    public CustomCountSubButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomCountSubButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setOnRadioChange(OnRadioChange onRadioChange) {
        this.onRadioChange = onRadioChange;
        if (onRadioChange != null)
            onRadioChange.add(this);
    }

    @Override
    public void onChangeRadio(int selectMenuOid) {
        setBackgroundResource(this.selectOptionOid == selectMenuOid ? R.drawable.btn_quantity_minus : R.drawable.btn_quantity_black_sub);
    }
}
