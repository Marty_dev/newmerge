package kr.co.mergepoint.mergeclient.scene.data.history;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;

/**
 * Created by 1017sjg on 2017. 7. 25..
 */

public class UsagePayment {

    @SerializedName("consummerRef")
    public int consummerRef;
    @SerializedName("companyPoints")
    public int companyPoints;
    /* PayConfirm */
    @SerializedName("plannedPoints")
    public int plannedPoints;

    @SerializedName("shopPayment")
    public ArrayList<ShopPayment> shopPayment;

    /* ShopHistoryPayment*/
    @SerializedName("menuUnusedNum")
    public int menuUnusedNum;

    @SerializedName("payMenu")
    public ArrayList<ShopMenuDetail> payMenu;

    @SerializedName("paidMenu")
    public ArrayList<ShopPaidMenu> paidMenu;

    /* UsedHistory */
    @SerializedName("menuCount")
    public int menuCount;

    @SerializedName("accumulatedPoint")
    public int accumulatedPoint;

    @SerializedName("state")
    public int state;

    @SerializedName("usedMenuList")
    public ArrayList<UsedMenu> usedMenuList;

    @SerializedName("useDateTime")
    public ArrayList<Integer> useDateTime;
    @SerializedName("approveDateTime")
    public ArrayList<Integer> approveDateTime;


    @SerializedName("barCode")
    public String barCode;
    @SerializedName("approveType")
    public int approveType;
    @SerializedName("issueDateTime")
    public ArrayList<Integer> issueDateTime;

    @SerializedName("cardImage")
    public String cardImage;
    @SerializedName("expireDateTime")
    public ArrayList<Integer> expireDateTime;



    /* 공통 */
    @SerializedName("oid")
    public int oid;

    @SerializedName("shopType")
    public int shopType;

    @SerializedName("shopRef")
    public int shopRef;

    @SerializedName("shopName")
    public String shopName;

    @SerializedName("orderPrice")
    public int orderPrice;

    @SerializedName("pointPrice")
    public int pointPrice;

    @SerializedName("couponPrice")
    public int couponPrice;

    @SerializedName("payPrice")
    public int payPrice;

    @SerializedName("menuNum")
    public int menuNum;

    @SerializedName("menuOrderNum")
    public int menuOrderNum;

    @SerializedName("useState")
    public int useState;

    @SerializedName("expDate")
    public ArrayList<Integer> expDate;
    @SerializedName("payDateTime")
    public ArrayList<Integer> payDateTime;

//    private int stateNum;
//    private String store;
//    private String menu;
//    private ArrayList<Integer> date;
//    private ArrayList<Integer> payTime;

    public int getStateNum() {
        return useState;
    }

    public String getStore() {
        return shopName != null ? shopName : getShopArrayName(shopPayment);
    }

    public String getMenu() {
        return payMenu == null && shopPayment == null ? getUsedMeneName(usedMenuList) : getMeneName(payMenu != null ? payMenu : getMenuArray(shopPayment));
    }

    public ArrayList<Integer> getDate() {
        return expDate;
    }

    public ArrayList<Integer> getPayTime() {
        return payDateTime == null && payMenu == null ? approveDateTime : (payDateTime != null ? payDateTime : payMenu.get(0).getPayDateTime());
    }

    private String getShopArrayName(ArrayList<ShopPayment> shopPayment) {
        return shopPayment.size() > 1 ? String.format(Locale.getDefault(), "%s 외 %d건", shopPayment.get(0).shopName, shopPayment.size()) : shopPayment.get(0).shopName;
    }

    private ArrayList<ShopMenuDetail> getMenuArray(ArrayList<ShopPayment> shopPayments) {
        ArrayList<ShopMenuDetail> menuDetails = new ArrayList<>();
        for (ShopPayment payment : shopPayments)
            menuDetails.addAll(payment.menu);

        return menuDetails;
    }

    private String getMeneName(ArrayList<ShopMenuDetail> shopPayments) {
        ArrayList<String> menuName = new ArrayList<>();
        for (ShopMenuDetail shopMenu : shopPayments)
            menuName.add(shopMenu.name);

        return TextUtils.join(", ", menuName);
    }

    private String getUsedMeneName(ArrayList<UsedMenu> usedMenus) {
        ArrayList<String> menuName = new ArrayList<>();
        for (UsedMenu usedMenu : usedMenus)
            menuName.add(usedMenu.payMenu.name);

        return TextUtils.join(", ", menuName);
    }
}
