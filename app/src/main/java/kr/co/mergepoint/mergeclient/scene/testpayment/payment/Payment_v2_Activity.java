package kr.co.mergepoint.mergeclient.scene.testpayment.payment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.BaseCheckButton;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.BaseRadioButton;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.application.common.SimpleWebFragment;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.CardPaymentWebFragment;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.SearchCrowdPayUserListener;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.adapter.CrowdListAdapter;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.adapter.ShopListAdapter;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.CrowdSimpleListdata;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.PaymentRequest;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ticketbody;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.http.Payment_v2_Callbacks;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.view.SearchCrowdPayUserFragmentV2;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static kr.co.mergepoint.mergeclient.application.common.Properties.BASE_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CARD_PAYMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INFO_PROVIDE_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INFO_PROVIDE_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAYMENT_RESULT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAYMENT_RESULT_ARRAY;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SEARCH_CROWD_PAY_FRAGMENTV2_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UP_DOWN;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WEB_URL;

public class Payment_v2_Activity extends BaseActivity implements SearchCrowdPayUserListener {

    @BindView(R.id.back_arrow)
    public ImageView backArrow;
    @BindView(R.id.toolbar_title)
    public TextView toolbarTitle;
    @BindView(R.id.payment_toolbar)
    public RelativeLayout paymentToolbar;
    @BindView(R.id.payment_section1)
    public TextView paymentSection1;
    @BindView(R.id.crowdpay_btn)
    public TextView crowdpayBtn;
    @BindView(R.id.payment_section2)
    public TextView paymentSection2;
    @BindView(R.id.pay_totalprise)
    public TextView payTotalprise;
    @BindView(R.id.pay_totalcount)
    public TextView payTotalcount;
    @BindView(R.id.pay_totalpoint)
    public TextView payTotalpoint;
    @BindView(R.id.pay_enterprise)
    public TextView payEnterprise;
    @BindView(R.id.pay_pointprise)
    public TextView payPointprise;
    @BindView(R.id.pay_plannedpoint)
    public TextView payPlannedpoint;
    @BindView(R.id.pay_cardprise)
    public TextView payCardprise;
    @BindView(R.id.pay_layout)
    public LinearLayout payLayout;
    @BindView(R.id.select_payment_text)
    public TextView selectPaymentText;
    @BindView(R.id.card_pay)
    public BaseRadioButton cardPay;
    @BindView(R.id.select_notice_text)
    public TextView selectNoticeText;
    @BindView(R.id.payment_notice_text)
    public TextView paymentNoticeText;
    @BindView(R.id.info_provide)
    public TextView infoProvide;

    @BindView(R.id.payment_shop_list)
    public RecyclerView paymentShopList;
    @BindView(R.id.crowd_list)
    public RecyclerView crowdList;
    @BindView(R.id.payment_ticket_sheet)
    public RelativeLayout paymentTicketSheet;

    public boolean isEnterMember;

    ShopListAdapter shoplist;
    BottomSheetBehavior ticketsheet;
    @BindView(R.id.payment_confimrbtn)
    TextView paymentConfimrbtn;
    @BindView(R.id.pay_enterprise_text)
    TextView payEnterpriseText;

    private Context mCon = this;
    public CrowdListAdapter crowdlist;
    public ArrayList<ShopPayment> shops;
    PaymentRequest paymentInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_v2_);
        ButterKnife.bind(this);
        ticketsheet = BottomSheetBehavior.from(paymentTicketSheet);
        ticketsheet.setHideable(true);
        callbacks = new Payment_v2_Callbacks(this);

        isEnterMember = (MergeApplication.getMember().getRoles().contains("02") || MergeApplication.getMember().getRoles().contains("03"));


        payEnterprise.setVisibility(isEnterMember ? View.VISIBLE : View.GONE);
        payEnterpriseText.setVisibility(isEnterMember ? View.VISIBLE : View.GONE);
        crowdpayBtn.setOnClickListener(view1 -> {

            if (crowdpayBtn.getText().toString().equals("함께 결제하기")) {
                this.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.payment_content, SearchCrowdPayUserFragmentV2.newInstance(this, this), SEARCH_CROWD_PAY_FRAGMENTV2_TAG)
                        .addToBackStack(SEARCH_CROWD_PAY_FRAGMENTV2_TAG).commit();
            }else if (crowdpayBtn.getText().toString().equals("수정하기")){
                goCrowdSettingActivity();
            }
        });
        backArrow.setOnClickListener(view1 -> {
            finish();
        });

        Datainit();

        SpannableString spannableString = new SpannableString(getResources().getString(R.string.payment_provide_info));
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                onViewClicked(view);
            }
        }, 5, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        infoProvide.setText(spannableString);
        infoProvide.setMovementMethod(LinkMovementMethod.getInstance());
        paymentConfimrbtn.setOnClickListener(view1 -> {

                boolean isCheck = true;
                for (ShopPayment pay : paymentInfo.shopPayment) {
                    if (pay.shopType == 2 || pay.shopType == 3) {
                        if (pay.payPrice == 0) {
                            isCheck &= true;
                        } else {
                            isCheck &= false;
                        }
                    } else
                        isCheck &= true;
                }
                if (!isCheck) {
                    new MergeDialog.Builder(this).setContent(R.string.check_payment_enterpoint).setCancelBtn(false).build().show();
                } else {
                    // TODO: 2018-07-23  CardPayment
                    this.getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(getEnterAnim(UP_DOWN), getExitAnim(UP_DOWN), getPopEnterAnim(UP_DOWN), getPopExitAnim(UP_DOWN))
                            .replace(R.id.payment_content, CardPaymentWebFragment.newInstance(BASE_URL + "payment/cardPay/v1/" + paymentInfo.oid, this), CARD_PAYMENT_TAG)
                            .addToBackStack(CARD_PAYMENT_TAG).commit();
                }

        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        MDEBUG.debug("is Resume call??");

        if (paymentInfo != null && isEnterMember && paymentInfo.shopPayment.size() == 1)
            callbacks.RequestReadPayment(paymentInfo.shopPayment.get(0).paymentRef);

    }


    public void onNewIntent(Intent intent) {
        CardPaymentWebFragment paymentWebFragment = (CardPaymentWebFragment) findFragmentByTag(CARD_PAYMENT_TAG);
        WebView paymentWeb = paymentWebFragment.getWebView();
        Log.e("===============", "onNewIntent!!");
        if (intent != null) {
            if (Intent.ACTION_VIEW.equals(intent.getAction())) {
                Uri uri = intent.getData();
                if (uri != null) {

                    Log.e("================uri", uri.toString());
                    if (String.valueOf(uri).startsWith("ISP 커스텀스키마를 넣어주세요")) { // ISP 커스텀스키마를 넣어주세요
                        String result = uri.getQueryParameter("result");
                        if ("success".equals(result)) {
                            paymentWeb.loadUrl("javascript:doPostProcess();");
                        } else if ("cancel".equals(result)) {
                            paymentWeb.loadUrl("javascript:doCancelProcess();");
                        } else {
                            paymentWeb.loadUrl("javascript:doNoteProcess();");
                        }
                    } else if (String.valueOf(uri).startsWith("계좌이체 커스텀스키마를 넣어주세요")) { // 계좌이체 커스텀스키마를 넣어주세요
                        /*계좌이체는 WebView가 아무일을 하지 않아도 됨*/
                    } else if (String.valueOf(uri).startsWith("paypin 커스텀스키마를 넣어주세요")) { // paypin 커스텀스키마를 넣어주세요
                        paymentWeb.loadUrl("javascript:doPostProcess();");
                    } else if (String.valueOf(uri).startsWith("paynow 커스텀스키마를 넣어주세요")) { // paynow 커스텀스키마를 넣어주세요
                        /*paynow는 WebView가 아무일을 하지 않아도 됨*/
                    }
                }
            }
        }
    }

    @Override
    public void updateCrowedMemberTable(int type, String searchString) {
        callbacks.RequestCrowdList(type, searchString, shops.get(0).oid);
    }


    void Datainit() {

        if (getIntent().getParcelableExtra(PAYMENT_RESULT) != null) {
            callbacks.PaymentRequestByMenu(getIntent().getParcelableExtra(PAYMENT_RESULT));
        } else if (getIntent().getIntegerArrayListExtra(PAYMENT_RESULT_ARRAY) != null) {
            callbacks.PaymentRequestByBasket(getIntent().getIntegerArrayListExtra(PAYMENT_RESULT_ARRAY));
        }else if(getIntent().getIntExtra("price", -1) != -1){
            callbacks.PaymentRequestByPrice(getIntent().getIntExtra("shopRef",-1),getIntent().getIntExtra("price",1));
        }
    }

    public void goCrowdSettingActivity() {
        Intent inte = new Intent(this, CrowdSettingActivity.class);
        inte.putExtra("Shopoid", shops.get(0).oid);
        startActivity(inte);
        this.getSupportFragmentManager().beginTransaction()
                .remove((BaseFragment) getSupportFragmentManager().findFragmentByTag(SEARCH_CROWD_PAY_FRAGMENTV2_TAG))
                .commit();
    }



    public void initList(PaymentRequest obj, boolean isEnterMember) {
        paymentInfo = obj;
        payTotalprise.setText(MoneyForm(obj.orderPrice) + "원");
        payTotalpoint.setText(MoneyForm(obj.couponPrice) + "원");
        payTotalcount.setText(obj.menuNum + "개");
        payPointprise.setText(MoneyForm(obj.pointPrice) + "원");
        payPlannedpoint.setText(MoneyForm(obj.plannedPoints) + "원");
        payCardprise.setText(MoneyForm(obj.payPrice)+ "원");
        payEnterprise.setText(MoneyForm(obj.companyPoints) + "원");
        this.shops = (ArrayList<ShopPayment>) paymentInfo.shopPayment;
        MDEBUG.debug("Proccessing 3");

        if (shoplist == null)
            shoplist = new ShopListAdapter(R.layout.payment_shop_item_v2, this, shops);
        shoplist.arrayList = shops;
        shoplist.isEnterMember = isEnterMember;
        MDEBUG.debug("Proccessing 4");
        if (paymentShopList.getAdapter() == null)
            paymentShopList.setAdapter(shoplist);
        else
            shoplist.notifyDataSetChanged();

        initcrowdList(shops);

    }

    public void initcrowdList(ArrayList<ShopPayment> array) {

        if (crowdlist == null)
            crowdlist = new CrowdListAdapter(R.layout.crowd_listitem, this);

        if (isEnterMember && shops.size() == 1) {
            crowdlist.arrayList = (ArrayList<CrowdSimpleListdata>) array.get(0).crowdPay;
            crowdList.setAdapter(crowdlist);

            if (array.get(0).crowdPay.size() != 0) {
                crowdpayBtn.setText("수정하기");
                crowdpayBtn.setBackground(getResources().getDrawable(R.drawable.gray_rect2));
            } else {
                crowdpayBtn.setText("함께 결제하기");
                crowdpayBtn.setBackground(getResources().getDrawable(R.drawable.white_box_1));

            }
        }
    }

    public void setPointAdapt(int oid, long point, int coupon) {
        callbacks.RequestPointAdapt(oid, point, coupon);
    }

    public void setEnterAdapt(int ticketcount, int ticketoid, long companyPoint, int crowdoid) {
        ticketbody tic = new ticketbody();
        tic.crowdPayRef = crowdoid;
        tic.companyPoint = companyPoint;
        tic.ticketCount = ticketcount;
        tic.memberRef = MergeApplication.getMember().getOid();
        tic.ticketRef = ticketoid;
        callbacks.RequestEnterAdapt(tic);
    }


    public String MoneyForm(String money) {
        return String.format("%,d", money);
    }
    public String MoneyForm(int money) {
        return String.format("%,d", money);
    }
    @Override
    protected void onActivityClick(View view) {
        MDEBUG.debug("Click?");

    }

    public void onViewClicked(View view) {
        Bundle bundle = new Bundle();
        bundle.putString(WEB_URL, INFO_PROVIDE_URL + paymentInfo.oid);
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.payment_content, SimpleWebFragment.newInstance(bundle, getString(R.string.personal_information_provided)), INFO_PROVIDE_TAG)
                .addToBackStack(INFO_PROVIDE_TAG).commit();
    }


    protected int getEnterAnim(@Properties.FragmentAnim int anim) {
        return anim == UP_DOWN ? R.anim.slide_from_down : R.anim.slide_from_right;
    }

    protected int getExitAnim(@Properties.FragmentAnim int anim) {
        return anim == UP_DOWN ? 0 : R.anim.slide_to_left;
    }

    protected int getPopEnterAnim(@Properties.FragmentAnim int anim) {
        return anim == UP_DOWN ? 0 : R.anim.slide_pop_from_right;
    }

    protected int getPopExitAnim(@Properties.FragmentAnim int anim) {
        return anim == UP_DOWN ? R.anim.slide_to_down : R.anim.slide_pop_to_right;
    }

    @Override
    public void success() {
        super.success();
        MDEBUG.debug("Success");
        paymentResult(true, "");
    }

    @Override
    public void failure(String error) {
        super.failure(error);
        MDEBUG.debug("Failed  : " + error);
        paymentResult(false, error);

    }

    public void paymentResult(boolean isSuccess, String error) {
        if (isSuccess) {
            Intent intent = new Intent();
            MDEBUG.debug("payment.oid " + paymentInfo.oid);
            intent.putExtra("paymentRef",paymentInfo.oid);
            intent.putExtra("isMulti",paymentInfo.shopPayment.size() != 1);
            MDEBUG.debug("isMulti : " + (paymentInfo.shopPayment.size() != 1));
            this.setResult(RESULT_OK, intent);
            this.finish();
        } else {
            new MergeDialog.Builder(this)
                    .setContent(error)
                    .setConfirmClick(() -> removeFragment(findFragmentByTag(CARD_PAYMENT_TAG))).setCancelBtn(false).build().show();
        }
    }

    protected void removeFragment(Fragment fragment) {
        if (fragment != null) {
            this.getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            this.getSupportFragmentManager().popBackStack();
        }
    }

    public BaseFragment findFragmentByTag(String tag) {
        return (BaseFragment) this.getSupportFragmentManager().findFragmentByTag(tag);
    }
}
