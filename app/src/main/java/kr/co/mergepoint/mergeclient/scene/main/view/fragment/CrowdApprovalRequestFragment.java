package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.CrowdApprovalRequestBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.payment.Approval;
import kr.co.mergepoint.mergeclient.scene.data.payment.ApprovalState;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.Ticket;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.CrowdApprovalRequestAdapter;

/**
 * Created by 1017sjg on 2017. 8. 7..
 */

public class CrowdApprovalRequestFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {

    private CrowdApprovalRequestBinding requestBinding;
    private ArrayList<CrowdPay> myCrowdPay;
    private ArrayList<CrowdPay> crowdPays;
    private ArrayList<Integer> rejectOids;
    private String requesterName;
    private ArrayList<Integer> reqDateTime;

    private long startTime = 0;
    private Handler timerHandler = new Handler();
    private Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            long millis = (startTime + 300000) - System.currentTimeMillis();
            int seconds = (int) (millis / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;

            if (isAdded()) {
                requestBinding.timer.setText(getString(R.string.request_timer, minutes, seconds));
                if (minutes <= 0 && seconds <= 0) {
                    timerHandler.removeCallbacks(timerRunnable);
                } else {
                    timerHandler.postDelayed(this, 500);
                }
            }
        }
    };

    public Approval getApproval() {
        ArrayList<CrowdPay> arrayList = new ArrayList<>();
        arrayList.addAll(myCrowdPay);
        arrayList.addAll(crowdPays);
        ArrayList<ApprovalState> approvalStates = new ArrayList<>();
        for (CrowdPay crowdPay :arrayList) {
            approvalStates.add(new ApprovalState(crowdPay.oid, !rejectOids.contains(crowdPay.oid)));
        }
        return new Approval(approvalStates);
    }

    public Approval getApprovalReject() {
        ArrayList<CrowdPay> arrayList = new ArrayList<>();
        arrayList.addAll(myCrowdPay);
        arrayList.addAll(crowdPays);
        ArrayList<ApprovalState> approvalStates = new ArrayList<>();
        for (CrowdPay crowdPay :arrayList) {
            approvalStates.add(new ApprovalState(crowdPay.oid, false));
        }
        return new Approval(approvalStates);
    }

    public static CrowdApprovalRequestFragment newInstance(ArrayList<CrowdPay> crowdPays) {
        Bundle args = new Bundle();
        CrowdApprovalRequestFragment fragment = new CrowdApprovalRequestFragment();
        fragment.crowdPays = crowdPays;
        fragment.myCrowdPay = new ArrayList<>();
        fragment.rejectOids = new ArrayList<>();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Member member = MergeApplication.getMember();
        if (member != null) {
            int memberOid = member.getOid();
            if (crowdPays.size() > 0) {
                requesterName = crowdPays.get(0).requesterName;
                reqDateTime = crowdPays.get(0).reqDateTime;
            }
            for (CrowdPay crowdPay : crowdPays) {
                if (memberOid == crowdPay.memberRef)
                    myCrowdPay.add(crowdPay);
            }
            if (myCrowdPay != null)
                crowdPays.removeAll(myCrowdPay);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        requestBinding = DataBindingUtil.inflate(inflater, R.layout.crowd_approval_request, container, false);
        requestBinding.setTitle("함께결제 승인하기");
        requestBinding.setOnClick(this);

        requestBinding.setRequesterName(requesterName);

        if (myCrowdPay.size() > 0) {
            Map<String, Object> myTicketInfo = getMyTicketInfo();
            requestBinding.point.setText(getString(R.string.point, (int) myTicketInfo.get("TOTAL_POINT_PRICE")));
            requestBinding.ticket.setText(getString(R.string.point, (int) myTicketInfo.get("TOTAL_TICKET_PRICE")));

            String ticketName = (String) myTicketInfo.get("TICKET_NAME");
            String ticketPriceName = (String) myTicketInfo.get("TICKET_PRICE_NAME");
            requestBinding.ticketNameText.setVisibility(!ticketName.isEmpty() ? View.VISIBLE : View.GONE);
            requestBinding.ticketPriceText.setVisibility(!ticketPriceName.isEmpty() ? View.VISIBLE : View.GONE);
            requestBinding.ticketNameText.setText(ticketName);
            requestBinding.ticketPriceText.setText(ticketPriceName);
        }

        requestBinding.myPointLayout.setVisibility(myCrowdPay.size() > 0 ? View.VISIBLE : View.GONE);
        requestBinding.delegationApprovalLayout.setVisibility(crowdPays.size() > 0 ? View.VISIBLE : View.GONE);
        requestBinding.setCrowedListAdapter(new CrowdApprovalRequestAdapter(crowdPays, this));
        startTimer(reqDateTime);
        return requestBinding.getRoot();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean check) {
        int oid = (int) compoundButton.getTag();
        if (check) {
            rejectOids.add(oid);
        } else {
            int index = rejectOids.indexOf(oid);
            if (index >= 0)
                rejectOids.remove(index);
        }
    }

    private Map<String, Object> getMyTicketInfo() {
        Map<String, Object> ticketInfo = new HashMap<>();
        ticketInfo.put("TOTAL_TICKET_PRICE", 0);
        ticketInfo.put("TOTAL_POINT_PRICE", 0);
        ticketInfo.put("TICKET_NAME", "");
        ticketInfo.put("TICKET_PRICE_NAME", "");
        for (CrowdPay crowdPay :myCrowdPay) {
            int pointPrice = (int) ticketInfo.get("TOTAL_POINT_PRICE");
            ticketInfo.put("TOTAL_POINT_PRICE", pointPrice + crowdPay.usePoints);
            if (crowdPay.comPayPoints != null) {
                ArrayList<String> ticketNameArray = new ArrayList<>();
                ArrayList<String> ticketPriceArray = new ArrayList<>();
                for (Ticket ticket : crowdPay.comPayPoints) {
                    int price = (int) ticketInfo.get("TOTAL_TICKET_PRICE");
                    ticketInfo.put("TOTAL_TICKET_PRICE", price + (ticket.ticketUnitPrice * ticket.useCount));
                    ticketNameArray.add(ticket.ticketName + " X" + String.valueOf(ticket.useCount));
                    ticketPriceArray.add(getString(R.string.point, ticket.ticketUnitPrice));
                }
                ticketInfo.put("TICKET_NAME", TextUtils.join("\n", ticketNameArray));
                ticketInfo.put("TICKET_PRICE_NAME", TextUtils.join("\n", ticketPriceArray));
            }
        }
        return ticketInfo;
    }

    private void startTimer(ArrayList<Integer> dateArray) {
        if (dateArray != null && dateArray.size() >= 6) {
            String dateStr = String.format(Locale.getDefault(), "%d-%02d-%02d %02d:%02d:%02d", dateArray.get(0), dateArray.get(1), dateArray.get(2), dateArray.get(3), dateArray.get(4), dateArray.get(5));
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            try {
                Date date = format.parse(dateStr);
                startTime = date.getTime();
            } catch (ParseException ignored) {}
        } else {
            startTime = System.currentTimeMillis();
        }
        timerHandler.postDelayed(timerRunnable, 0);
        Log.d("startTimer", "" + startTime);
    }
}
