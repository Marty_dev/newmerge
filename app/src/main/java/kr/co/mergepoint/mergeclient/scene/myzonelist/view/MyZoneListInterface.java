package kr.co.mergepoint.mergeclient.scene.myzonelist.view;

import java.util.Map;

import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.ShopListAdapter;

/**
 * Created by jgson on 2018. 1. 8..
 */

public interface MyZoneListInterface {

    /* 장바구니 열기 */
    void openShoppingBasket();

    void openPaymentResult();

    void openShopDetail(Shop shop);

    void animCart();

    void searchCategoryShop(Map<String, Object> filter);


    /* GETTER or SETTER */

    String getStreetName();

    ShopListAdapter getShopListAdapter();

    String[] getStreetStringArray();
}
