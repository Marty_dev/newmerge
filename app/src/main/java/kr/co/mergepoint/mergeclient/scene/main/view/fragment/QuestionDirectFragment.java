package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.SpinnerAdapter;
import kr.co.mergepoint.mergeclient.databinding.QuestionDirectBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.Contactus;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;

import static kr.co.mergepoint.mergeclient.application.common.Properties.LEFT;

/**
 * Created by 1017sjg on 2017. 8. 14..
 */

public class QuestionDirectFragment extends BaseFragment {

    private QuestionDirectBinding questionDirectBinding;
    private Uri galleryPath;

    public static QuestionDirectFragment newInstance() {
        Bundle args = new Bundle();
        QuestionDirectFragment fragment = new QuestionDirectFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        questionDirectBinding = DataBindingUtil.inflate(inflater, R.layout.question_direct, container, false);
        questionDirectBinding.setTitle(getString(R.string.contact_us));
        questionDirectBinding.setOnClick(this);

        InitialInfo initialInfo = MergeApplication.getInitialInfo();
        if (initialInfo != null && initialInfo.getMember() != null) {
            Member member =initialInfo.getMember();
            questionDirectBinding.setEmail(member.getId());
            questionDirectBinding.setPhone(member.getPhone());
        }
        questionDirectBinding.classificationSpinner.setAdapter(new SpinnerAdapter(getResources().getStringArray(R.array.question_direct_classification), LEFT));

        return questionDirectBinding.getRoot();
    }

    public void setAttachFileName(Uri uri) {
        galleryPath = getRealPathFromURI(uri);
        questionDirectBinding.fileName.setText(galleryPath.getLastPathSegment());
    }

    public Uri getRealPathFromURI(Uri uri){
        String result;
        Cursor cursor = rootActivity.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return Uri.parse(result);
    }

    public Contactus getQuestionData() {
        String email = questionDirectBinding.emailText.getText().toString();
        String phone = questionDirectBinding.phoneText.getText().toString();
        String title = questionDirectBinding.titleText.getText().toString();
        String content = questionDirectBinding.content.getText().toString();
        int classification = questionDirectBinding.classificationSpinner.getSelectedItemPosition();

        if (!questionDirectBinding.agreeCheck.isChecked()) {
            showAlert(R.string.check_personal_info);
            return null;
        } else if (email.trim().isEmpty()) {
            showAlert(R.string.enter_email);
            return null;
        } else if (phone.trim().isEmpty()) {
            showAlert(R.string.enter_phone);
            return null;
        } else if (classification == 0) {
            showAlert(R.string.enter_classification);
            return null;
        } else if (title.trim().isEmpty()) {
            showAlert(R.string.enter_title);
            return null;
        } else if (content.trim().isEmpty()) {
            showAlert(R.string.enter_text);
            return null;
        }

        return new Contactus(email, phone, classification, title, content, galleryPath != null ? galleryPath.getPath() : null);
    }

}
