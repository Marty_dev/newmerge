package kr.co.mergepoint.mergeclient.scene.franchise.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.franchise.OnLocationDialogClick;

/**
 * User: Marty
 * Date: 2018-10-04
 * Time: 오후 5:13
 * Description:
 */
public class LocationDialog extends Dialog {
    @BindView(R.id.dialog_top_title)
    TextView dialogTopTitle;
    @BindView(R.id.dialog_research_location)
    RelativeLayout dialogResearchLocation;
    @BindView(R.id.dialog_map_location)
    RelativeLayout dialogMapLocation;
    @BindView(R.id.dialog_content_rl)
    LinearLayout dialogContentRl;
    @BindView(R.id.basebottomline)
    TextView basebottomline;
    @BindView(R.id.dialog_single_btn)
    TextView dialogSingleBtn;

    OnLocationDialogClick onClickListener;
    public LocationDialog(@NonNull Context context) {
        super(context);
    }

    public LocationDialog(@NonNull Context context, OnLocationDialogClick onClickListener) {
        super(context);
        this.onClickListener = onClickListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_dialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ButterKnife.bind(this);
    }

    @OnClick({R.id.dialog_research_location, R.id.dialog_map_location, R.id.dialog_single_btn})
    public void onViewClicked(View view) {
        int pos = 0;

        switch (view.getId()) {
            case R.id.dialog_research_location: // 2
                pos++;
            case R.id.dialog_map_location: // 1
                pos++;
                onClickListener.onClick(pos);
            case R.id.dialog_single_btn:
                dismiss();
        }
    }
}
