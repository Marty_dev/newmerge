package kr.co.mergepoint.mergeclient.scene.privacy.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.scene.privacy.PrivacyActivity;

/**
 * Created by jgson on 2017. 6. 5..
 */

@PrivacyScope
@Component(modules = PrivacyModule.class)
public interface PrivacyComponent {
    void inject(PrivacyActivity activity);
}
