package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.DateModel;
import kr.co.mergepoint.mergeclient.application.common.MergeBaseAdapter;
import kr.co.mergepoint.mergeclient.scene.data.main.Alarmdata;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder.AlarmHolder;

/**
 * User: Marty
 * Date: 2018-08-01
 * Time: 오후 5:37
 * Description:
 */
public class AlarmAdapter extends MergeBaseAdapter<Alarmdata,AlarmHolder> {


    public AlarmAdapter() {
    }

    public AlarmAdapter(int layout, BaseActivity mCon) {
        super(layout, mCon);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= inflater.inflate(mLayout,parent,false);
        AlarmHolder alarmHolder = new AlarmHolder(v);
        return alarmHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AlarmHolder alarmholder = (AlarmHolder)holder;
        Alarmdata data = (Alarmdata) arrayList.get(position);

        alarmholder.title = holder.itemView.findViewById(R.id.alarm_title);
        alarmholder.date = holder.itemView.findViewById(R.id.alarm_date);
        alarmholder.content = holder.itemView.findViewById(R.id.alarm_content);

        alarmholder.content.setOnClickListener(mCon);
        alarmholder.content.setTag(data.oid);

        alarmholder.title.setText(data.title);
        DateModel dateModel = new DateModel();
        alarmholder.date.setText(dateModel.getStringDateWithSeconds((ArrayList<Integer>) data.sendDateTime));
    }
}
