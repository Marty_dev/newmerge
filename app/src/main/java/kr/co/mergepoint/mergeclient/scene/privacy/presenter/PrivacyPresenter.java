package kr.co.mergepoint.mergeclient.scene.privacy.presenter;

import android.app.DatePickerDialog;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasePresenter;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.SearchEnterpriseListener;
import kr.co.mergepoint.mergeclient.scene.privacy.model.PrivacyModel;
import kr.co.mergepoint.mergeclient.scene.privacy.presenter.callback.PrivacyCallback;
import kr.co.mergepoint.mergeclient.scene.privacy.view.PrivacyView;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class PrivacyPresenter extends BasePresenter<PrivacyView, PrivacyModel, PrivacyCallback> implements TextView.OnEditorActionListener, DatePickerDialog.OnDateSetListener, SearchEnterpriseListener {

    public PrivacyPresenter(PrivacyView baseView, PrivacyModel baseModel, PrivacyCallback callback) {
        super(baseView, baseModel, callback);
    }

    @Override
    protected void setBindingProperty() {
        baseView.binding.setTitle(baseView.getString(R.string.privacy_title));
        baseView.binding.setOnClick(this);
        baseView.binding.setOnDoneListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.enter_privacy:
                /* 간단 개인정보 보기 */
                if (baseView.emptyUserPw())
                    baseModel.getUserPrivacy(baseCallback.getUserPrivacy(), baseView.getUserPassword());
                break;
            case R.id.info_edit:
                /* 개인정보 수정 */
                baseView.shopEditPrivacy(baseModel.getMember());
                break;
            case R.id.postcode_btn:
                /* 우편번호 찾기 */
                baseView.showSearchPost();
                break;
            case R.id.user_birth:
                /* 생년월일을 위한 달려 보기 */
                baseView.showCalendar(baseModel.getMember().getBirthday(), this);
                break;
            case R.id.send_server:
                /* 변경된 개인정보을 실행하기 */
                baseView.askModifyInfo(() -> {
                    baseView.showLoading();
                    baseModel.sendModifyInfo(baseCallback.returnModifyInfo());
                }, baseModel.getMember());
                break;
            case R.id.search_enterprise:
                baseView.openSearchEnterprise();
                break;
            case R.id.next_button:
                baseView.openClearTaskActivity(MainActivity.class);
                break;
        }
    }

    /* PW 입력 후 가상 키보드에서 완료 이벤트 리스너 */
    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            baseView.hideKeyboard();
            if (baseView.emptyUserPw())
                baseModel.getUserPrivacy(baseCallback.getUserPrivacy(), baseView.getUserPassword());
            return true;
        }
        return false;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        baseModel.getMember().setBirthday(new ArrayList<>(Arrays.asList(year, month + 1, day)));
    }

    public void setAddress(final String add01, final String add02, final String add03) {
        baseModel.getMember().setPostcode(add01);
        baseModel.getMember().setAddress01(add02);
        baseModel.getMember().setAddress02(add03);
        baseView.closePostcode();
    }

    @Override
    public void searchEnterprise(String searchText) {
        baseView.showLoading();
        baseModel.searchEnterprise(baseCallback.getSearchEnterpriseListCallback(), searchText);
    }

    @Override
    public void registerEnterprise(int companyRef) {
        baseView.showLoading();
        baseModel.registerEnterprise(baseCallback.registerEnterpriseListCallback(), companyRef);
    }
}
