package kr.co.mergepoint.mergeclient.application.common;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by 1017sjg on 2017. 7. 18..
 */

public abstract class ListLoadScrollListener extends RecyclerView.OnScrollListener {

    private boolean loading;
    private int previousTotal;
    private int currentPage;
    private LinearLayoutManager mLinearLayoutManager;

    protected ListLoadScrollListener() {
        this.mLinearLayoutManager = setLinearLayoutManager();
        resetScroll();
    }

    public void resetScroll() {
        this.loading = true;
        this.previousTotal = 0;
        this.currentPage = 0;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = mLinearLayoutManager.getItemCount();
        int firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
        int visibleThreshold = 5;

        if (loading && (totalItemCount > previousTotal)) {
            previousTotal = totalItemCount;
            loading = false;
        }

        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            currentPage++;
            onLoadMore(currentPage);
            loading = true;
        }
    }

    public abstract void onLoadMore(int currentPage);
    public void onFailedLoadList (){
        currentPage--;
    }
    public abstract LinearLayoutManager setLinearLayoutManager();
}
