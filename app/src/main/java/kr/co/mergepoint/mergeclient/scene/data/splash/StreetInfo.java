package kr.co.mergepoint.mergeclient.scene.data.splash;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 1017sjg on 2017. 6. 14..
 */

public class StreetInfo implements Parcelable{
    @SerializedName("streetCode")
    private String code;

    @SerializedName("name")
    private String name;

    @SerializedName("longitude")
    private Double longitude;

    @SerializedName("latitude")
    private Double latitude;

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public StreetInfo(String code) {
        this.code = code;
    }

    private StreetInfo(Parcel in) {
        code = in.readString();
        name = in.readString();
        longitude = in.readDouble();
        latitude = in.readDouble();
    }

    public static final Creator<StreetInfo> CREATOR = new Creator<StreetInfo>() {
        @Override
        public StreetInfo createFromParcel(Parcel in) {
            return new StreetInfo(in);
        }

        @Override
        public StreetInfo[] newArray(int size) {
            return new StreetInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(name);
        dest.writeDouble(longitude);
        dest.writeDouble(latitude);
    }
}