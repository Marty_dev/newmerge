package kr.co.mergepoint.mergeclient.scene.franchise.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BookMark;
import kr.co.mergepoint.mergeclient.application.common.TagLayout;

/**
 * User: Marty
 * Date: 2018-10-02
 * Time: 오전 10:17
 * Description:
 */
public class NewShop_ViewHolder extends RecyclerView.ViewHolder {
    public ImageView backImg;
    public TextView FranName,FranTime,Heartnum,Marknum,Ordernum,Distance;
    public TagLayout Taglayout;
    public BookMark bookMark;

    public NewShop_ViewHolder(View itemView) {
        super(itemView);
        backImg = (ImageView)itemView.findViewById(R.id.fran_back_img);
        FranName = (TextView)itemView.findViewById(R.id.fran_shop_name);
        FranTime = (TextView)itemView.findViewById(R.id.fran_shop_time);
        Heartnum = (TextView)itemView.findViewById(R.id.fran_heartnum);
        Marknum = (TextView)itemView.findViewById(R.id.fran_bookmarknum);
        Ordernum = (TextView)itemView.findViewById(R.id.fran_ordernum);
        Distance = (TextView)itemView.findViewById(R.id.fran_distance);
        Taglayout = (TagLayout)itemView.findViewById(R.id.fran_tag_category);
        bookMark= (BookMark)itemView.findViewById(R.id.fran_bookmark);


    }

}
