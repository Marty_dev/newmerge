package kr.co.mergepoint.mergeclient.application.common.mvvm;

import java.net.URISyntaxException;

import kr.co.mergepoint.mergeclient.application.common.BaseActivity;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public interface AttachedActivity {
    void startActivity(Class<? extends BaseActivity> activityClass);
    void openFragment(int parentLayout, ViewModelFragment fragment, String tag);
    void openUrl(String url) throws URISyntaxException;
    ViewModelActivity getActivity();
}
