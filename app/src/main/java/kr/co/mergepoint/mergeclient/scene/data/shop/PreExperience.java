package kr.co.mergepoint.mergeclient.scene.data.shop;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jgson on 2017. 12. 7..
 */

public class PreExperience implements Parcelable {
    @SerializedName("image")
    public String image;

    @SerializedName("thumbnail")
    public String thumbnail;

    private PreExperience(Parcel in) {
        image = in.readString();
        thumbnail = in.readString();
    }

    public static final Parcelable.Creator<PreExperience> CREATOR = new Parcelable.Creator<PreExperience>() {
        @Override
        public PreExperience createFromParcel(Parcel in) {
            return new PreExperience(in);
        }

        @Override
        public PreExperience[] newArray(int size) {
            return new PreExperience[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image);
        dest.writeString(thumbnail);
    }
}
