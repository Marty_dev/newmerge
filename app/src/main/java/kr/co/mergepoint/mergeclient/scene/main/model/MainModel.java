package kr.co.mergepoint.mergeclient.scene.main.model;

import android.content.Intent;
import android.location.Location;

import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.EnterpriseApi;
import kr.co.mergepoint.mergeclient.api.EventApi;
import kr.co.mergepoint.mergeclient.api.HistoryApi;
import kr.co.mergepoint.mergeclient.api.MergesPickApi;
import kr.co.mergepoint.mergeclient.api.NoticeApi;
import kr.co.mergepoint.mergeclient.api.QuestionApi;
import kr.co.mergepoint.mergeclient.api.ShopApi;
import kr.co.mergepoint.mergeclient.api.UserApi;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseModel;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.CompanyPointHistory;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.main.CombineImagesDetail;
import kr.co.mergepoint.mergeclient.scene.data.main.Contactus;
import kr.co.mergepoint.mergeclient.scene.data.main.EventDetail;
import kr.co.mergepoint.mergeclient.scene.data.main.GradeRes;
import kr.co.mergepoint.mergeclient.scene.data.main.MemberGrade;
import kr.co.mergepoint.mergeclient.scene.data.main.Notice;
import kr.co.mergepoint.mergeclient.scene.data.main.userPointinfo;
import kr.co.mergepoint.mergeclient.scene.data.payment.Approval;
import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPayItem;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.data.payment.Ticket;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by jgson on 2017. 5. 31..
 */

public class MainModel extends BaseModel{

    private UserApi userApi;
    private ShopApi shopApi;
    private QuestionApi questionApi;
    private NoticeApi noticeApi;
    private MergesPickApi mergesPickApi;
    private EventApi eventApi;
    private EnterpriseApi enterpriseApi;

    public MainModel() {
        userApi = retrofit.create(UserApi.class);
        shopApi = retrofit.create(ShopApi.class);
        questionApi = retrofit.create(QuestionApi.class);
        noticeApi = retrofit.create(NoticeApi.class);
        mergesPickApi = retrofit.create(MergesPickApi.class);
        eventApi = retrofit.create(EventApi.class);
        enterpriseApi = retrofit.create(EnterpriseApi.class);
    }

    public Map<String, Object> callApp(MainActivity activity) {
        Intent intent = activity.getIntent();
        if (intent != null) {
            Map<String, Object> callInfo = new HashMap<>();
            String type = intent.getStringExtra("type");
            String oidStr = intent.getStringExtra("oid");
            String userPath = intent.getStringExtra("userPath");
            String title = intent.getStringExtra("title");
            String value = intent.getStringExtra("value");
            String action = intent.getStringExtra("action");
            int actionNum = 0;
            try{
                actionNum = Integer.parseInt(action);
            }catch (Exception e){}
            if (value != null && (actionNum >= 19 && actionNum <= 24)) {
                callInfo.put("title", title);
                callInfo.put("value", value);
                callInfo.put("action", action);
                activity.setIntent(null);
                return callInfo;
            }
            MDEBUG.debug("FCM Title is : " + title);
            if (title != null && title.contains("함께 결제 승인을 해 주세요.")) {
                callInfo.put("CROWD_PAY_APPROVAL", true);
                activity.setIntent(null);
                return callInfo;
            } else {
                if (type != null) {
                    try {
                        if (oidStr != null)
                            callInfo.put("oid", Integer.parseInt(oidStr));  /*NumberFormatException*/
                        if (userPath != null)
                            callInfo.put("userPath", userPath);
                        callInfo.put("type", type);     /*IllegalArgumentException*/
                    } catch (NumberFormatException e) {
                        new MergeDialog.Builder(activity).setContent(R.string.wrong_url_text).setCancelBtn(false).build().show();
                    } catch (IllegalArgumentException e) {
                        new MergeDialog.Builder(activity).setContent(R.string.wrong_Argument_text).setCancelBtn(false).build().show();
                    }

                    activity.setIntent(null);
                    return callInfo.containsKey("type") ? callInfo : null;
                }
            }
        }

        activity.setIntent(null);
        return null;
    }

    public void requestMemberKey(Callback<ResponseBody> callback, String token) {
        userApi.sendMemberKey(token).enqueue(callback);
    }

    public void getVisitShopInfo(Callback<ResponseObject<JsonObject>> callback, int shopOid) {
        shopApi.getShopVisitInfo(shopOid).enqueue(callback);
    }

    public void logout(Callback<ResponseBody> callback) {
        userApi.logout().enqueue(callback);
    }

    public void withdrawal(Callback<ResponseObject<Object>> callback, boolean isRefund) {
        userApi.userWithdrawal(isRefund).enqueue(callback);
    }

    public void refreshUserPoints(Callback<ResponseObject<Long>> callback) {
        userApi.getUserPoints().enqueue(callback);
    }

    public void alliance(Callback<ResponseObject<Object>> callback, Contactus contactus) {
        questionApi.requestAlliance(contactus.email, contactus.phone, contactus.name, contactus.title, contactus.body).enqueue(callback);
    }

    public void requestNoticeList(Callback<ResponseObject<ArrayList<Notice>>> callback) {
        noticeApi.requestNoticeList().enqueue(callback);
    }

    public void requestMergesPickDetail(Callback<ResponseObject<ArrayList<CombineImagesDetail>>> callback, int oid) {
        mergesPickApi.requestMergesPickDetail(oid).enqueue(callback);
    }

    public void requestEventDetail(Callback<ResponseObject<EventDetail>> callback, int oid) {
        eventApi.requestEventDetail(oid).enqueue(callback);
    }

    public void getCompanyPointHistory(Callback<ResponseObject<ArrayList<CompanyPointHistory>>> callback, int type, int period) {
        enterpriseApi.getCompanyPointHistory(type, period).enqueue(callback);
    }

    public void question(Callback<ResponseObject<Integer>> callback, Contactus contactus) {

        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), contactus.email);
        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), contactus.phone);
        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(contactus.type));
        RequestBody title = RequestBody.create(MediaType.parse("text/plain"), contactus.title);
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), contactus.body);

        if (contactus.attachFile != null) {
            File file = new File(contactus.attachFile);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part attachFile = MultipartBody.Part.createFormData("attachFile", file.getName(), reqFile);
            questionApi.requestQuestion(email, phone, type, title, body, attachFile).enqueue(callback);
        } else {
            questionApi.requestQuestion(email, phone, type, title, body).enqueue(callback);
        }
    }

    public void downCoupon(Callback<ResponseObject<Integer>> callback, String data) {
        userApi.downCoupon(Integer.parseInt(data)).enqueue(callback);
    }

    public void registMember(Callback<ResponseObject<Member>> callback, String userPath) {
        userApi.getMemberInfo(userPath).enqueue(callback);
    }

    public void changeCrowedApprovalType(Callback<ResponseObject<Boolean>> callback, int type, int approver) {
        userApi.changeCrowedApprovalType(type, approver).enqueue(callback);
    }

    public void changeCrowedApprovalType(Callback<ResponseObject<Boolean>> callback, int type) {
        userApi.changeCrowedApprovalType(type).enqueue(callback);
    }

    public void resetPassword(Callback<ResponseObject<Member>> callback, Map<String, Object> resetInfo) {
        int oid = (int) resetInfo.get("oid");
        String userPath = (String) resetInfo.get("userPath");
        String password = (String) resetInfo.get("password");
        userApi.resetPassword(oid, userPath, password).enqueue(callback);
    }

    public void requestFavoriteList(Callback<ResponseObject<ArrayList<Shop>>> shopInfoCallback, Location location) {
        Map<String, Object> position = new HashMap<>();
        if (location != null) {
            position.put("latitude", location.getLatitude());
            position.put("longitude", location.getLongitude());
        }
        shopApi.getFavoriteList(position).enqueue(shopInfoCallback);
    }

    public void getCrowedMemberList(Callback<ResponseObject<ArrayList<CrowdMember>>> callback, int type, String keyword) {
        enterpriseApi.getCrowedMemberList(type, keyword).enqueue(callback);
    }

    public void changeFavoriteCrowedMember(Callback<ResponseObject<Long>> callback, int memberOid) {
        enterpriseApi.changeCrowdPayFavorite(memberOid).enqueue(callback);
    }

    public void getTicketList(Callback<ResponseObject<ArrayList<Ticket>>> callback) {
        enterpriseApi.getTicketList().enqueue(callback);
    }

    public void getCrowdPaymentHistory(Callback<ResponseObject<ArrayList<CrowdPayItem>>> callback, int type, int period) {
        enterpriseApi.getCrowdPaymentHistory(type, period).enqueue(callback);
    }

    public void getCrowdApprovalRequestList(Callback<ResponseObject<ArrayList<CrowdPay>>> callback) {
        enterpriseApi.getCrowdApprovalRequestList().enqueue(callback);
    }

    public void sendCheckApproval(Callback<ResponseObject<ArrayList<CrowdPay>>> callback, Approval approval) {
        enterpriseApi.sendCheckApproval(approval).enqueue(callback);
    }

    public void getCrowdPayHistoryDetail(Callback<ResponseObject<ShopPayment>> callback, int crowdPayRef) {
        enterpriseApi.requestCrowdPayHistoryDetail(crowdPayRef).enqueue(callback);
    }

    public void getMyCouponList(Callback<ResponseObject<ArrayList<Coupon>>> callback) {
        userApi.couponList().enqueue(callback);
    }

    public void readInfo(Callback<userPointinfo> callback){
        userApi.readMemberInfo().enqueue(callback);
    }
    public void readMemberGrade(Callback<GradeRes> callback){
        userApi.readmembergrade().enqueue(callback);
    }
}
