package kr.co.mergepoint.mergeclient.scene.testpayment.payment.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * User: Marty
 * Date: 2018-07-22
 * Time: 오후 8:58
 * Description:
 */
public class CrowdInfo {

    @SerializedName("payShopRef")
    public long payShopRef;

    @SerializedName("remainPayPrice")
    public long remainPayPrice ;

    @SerializedName("attendees")
    public ArrayList<Attendee> attendees ;



}
