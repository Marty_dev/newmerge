package kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter;

import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.application.common.BookMark;
import kr.co.mergepoint.mergeclient.application.common.CustomRecyclerView;
import kr.co.mergepoint.mergeclient.databinding.ShopListEnterpriseItemBinding;
import kr.co.mergepoint.mergeclient.databinding.ShopListItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;

import static kr.co.mergepoint.mergeclient.application.common.Properties.CORPORATE_ALLIANCE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CORPORATE_CAFETERIA;
import static kr.co.mergepoint.mergeclient.application.common.Properties.GENERAL_PARTNERSHIP;

/**
 * Created by 1017sjg on 2017. 6. 13..
 */

public class ShopListAdapter extends BasicListAdapter<BasicListHolder, Shop> {

    private BookMark.OnCheckFavorite onCheckFavorite;
    private int[] gradeDrawable = new int[] { R.drawable.shop_list_welcome, R.drawable.shop_list_bronz, R.drawable.shop_list_silver, R.drawable.shop_list_gold, R.drawable.shop_list_black};

    public ShopListAdapter() {
        super();
    }

    public ShopListAdapter(ArrayList<Shop> arrayList) {
        super(arrayList);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (recyclerView instanceof CustomRecyclerView) {
            CustomRecyclerView customRecyclerView = (CustomRecyclerView) recyclerView;
            onCheckFavorite = customRecyclerView.getOnCheckFavorite();
        }
    }

    @Override
    public BasicListHolder setCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == CORPORATE_ALLIANCE || viewType == CORPORATE_CAFETERIA) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_list_enterprise_item, parent, false);
            ShopListEnterpriseItemBinding binding = DataBindingUtil.bind(view);

            return new BasicListHolder<ShopListEnterpriseItemBinding, Shop>(binding) {
                @Override
                public void setDataBindingWithData(Shop data) {
                    getDataBinding().setShop(data);
                    getDataBinding().setFavoriteListener(onCheckFavorite);
                    getDataBinding().setEmptyImg(R.drawable.img_store_list_background);
                }
            };
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_list_item, parent, false);
            ShopListItemBinding binding = DataBindingUtil.bind(view);

            return new BasicListHolder<ShopListItemBinding, Shop>(binding) {
                @Override
                public void setDataBindingWithData(Shop data) {
                    getDataBinding().setShop(data);
                    Member member = MergeApplication.getMember();
                    if (member != null) {
                        getDataBinding().pointAccrualRate.setText(String.format(Locale.getDefault(), "%d%%적립", member.getRate() / 10));
                        getDataBinding().pointAccrualRate.setCompoundDrawablesWithIntrinsicBounds(gradeDrawable[member.getGrade() - 1], 0, 0, 0);
                    }
                    getDataBinding().pointAccrualRate.setVisibility(member != null ? View.VISIBLE : View.GONE);
                    getDataBinding().setFavoriteListener(onCheckFavorite);
                    getDataBinding().setEmptyImg(R.drawable.img_store_list_background);
                }
            };
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setBindViewHolder(BasicListHolder holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return getObservableArrayList().get(position).type;
    }

    public void singleShopFavoriteUpdate(int oid, boolean isFavorite) {
        int index = getObservableArrayList().indexOf(new Shop(oid, isFavorite));
        if (index >= 0) {
            Shop selectShop = getObservableArrayList().get(index);
            selectShop.favorite = isFavorite;
            notifyItemChanged(index);
        }
    }

    public void updateShopList(final ArrayList<Shop> arrayList) {
        final Handler handler = new Handler();
        new Thread(() -> {
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new ShopListDiffCallback(getObservableArrayList(), arrayList)); // TODO: 스트릿 바꾸면서 오류남!!
            handler.post(() -> dispatchUpdate(diffResult, arrayList));
        }).start();
    }

    private void dispatchUpdate(DiffUtil.DiffResult diffResult, ArrayList<Shop> arrayList) {
        changeListData(arrayList);
        diffResult.dispatchUpdatesTo(this);
    }

    public void addShopList(final ArrayList<Shop> arrayList) {
        final int preSize = getObservableArrayList().size() - 1;
        final Handler handler = new Handler();
        new Thread(() -> handler.post(() -> {
            addListData(arrayList);
            notifyItemRangeInserted(preSize, getObservableArrayList().size() - 1);
        })).start();
    }
}
