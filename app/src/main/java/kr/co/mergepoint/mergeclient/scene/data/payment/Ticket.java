package kr.co.mergepoint.mergeclient.scene.data.payment;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class Ticket implements Parcelable {

    public static String defaultstr = "제한없음";
    @SerializedName("oid")
    public int oid;

    @SerializedName("companyRef")
    public int companyRef;

    @SerializedName("memberRef")
    public int memberRef;

    @SerializedName("senderRef")
    public int senderRef;

    @SerializedName("reason")
    public String reason;

    @SerializedName("ticketRef")
    public int ticketRef;

    @SerializedName("ticketCategory")
    public int ticketCategory;

    @SerializedName("ticketName")
    public String ticketName;

    @SerializedName("ticketCount")
    public int ticketCount;

    @SerializedName("useCount")
    public int useCount;

    @SerializedName("ticketUnitPrice")
    public int ticketUnitPrice;

    @SerializedName("remainTicketCount")
    public int remainTicketCount;

    @SerializedName("returnPoint")
    public int returnPoint;

    /**/
    @SerializedName("usableDate")
    public ArrayList<Integer> usableDate;

    @SerializedName("usableEndTime")
    public ArrayList<Integer> usableEndTime;

    @SerializedName("usableStaTime")
    public ArrayList<Integer> usableStaTime;

    @SerializedName("scheduledRecoveryTime")
    public String scheduledRecoveryTime;

    @SerializedName("expireDateTime")
    public ArrayList<Integer> expireDateTime;

    public int day;
    public int hours;
    public int minute;

    public String getAvailableTime(){
        if (usableStaTime == null|| usableEndTime == null){
            return null;
        }
        StringBuilder returndata = new StringBuilder();
        returndata.append(String.format("%02d",usableStaTime.get(0)));
        returndata.append(":");
        returndata.append(String.format("%02d",usableStaTime.get(1)));
        returndata.append(" ~ ");
        returndata.append(String.format("%02d",usableEndTime.get(0)));
        returndata.append(":");
        returndata.append(String.format("%02d",usableEndTime.get(1)));
        return returndata.toString();

    }
    public String getAvailableDate(){
        if (usableDate == null)
            return null;

        String[] days = {"월","화","수","목","금","토","일"};
        StringBuilder returndata = new StringBuilder();
        for (Integer i : usableDate){
               returndata.append(days[i-1] + " ");
        }
        return  returndata.toString();
    }


    public void getCompareCalendar() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-ddHH:mm");
        Date enddate = new Date();

        try {
            enddate = dateFormat.parse((""+expireDateTime.get(0)) + ("-"+expireDateTime.get(1) ) + ("-"+expireDateTime.get(2)) + (" "+expireDateTime.get(3) + ":") + (expireDateTime.get(4) + ""));

        }catch (Exception e){

        }


        long sttime = Calendar.getInstance().getTimeInMillis();
        long endtime = enddate.getTime();

        long difftime = endtime - sttime;

        long day = difftime / (1000 * 60 * 60 * 24);
        difftime %= (1000 * 60 * 60 * 24);
        long hour = difftime / (1000 * 60 * 60);
        difftime %= (1000 * 60 * 60);
        long min = difftime / (1000 * 60);

        this.day = (int)day;
        this.hours = (int)hour;
        this.minute = (int)min;


    }

    public int getCompareDay() {
        getCompareCalendar();
        return this.day;
    }

    public int getCompareHour() {
        getCompareCalendar();

        return this.hours;
    }

    public int getCompareMin() {
        getCompareCalendar();

        return this.minute;
    }

    public Ticket(String ticketName, ArrayList<Integer> usableDate,ArrayList<Integer> usableStaTime,ArrayList<Integer> usableEndTime, String scheduledRecoveryTime, int ticketUnitPrice, int ticketCount) {
        this.ticketName = ticketName;
        this.usableDate = usableDate;
        this.usableStaTime = usableStaTime;
        this.usableEndTime = usableEndTime;
        this.scheduledRecoveryTime = scheduledRecoveryTime;
        this.ticketUnitPrice = ticketUnitPrice;
        this.ticketCount = ticketCount;
    }

    private Ticket(Parcel in) {
        oid = in.readInt();
        companyRef = in.readInt();
        memberRef = in.readInt();
        senderRef = in.readInt();
        reason = in.readString();
        ticketRef = in.readInt();
        ticketCategory = in.readInt();
        ticketName = in.readString();
        useCount = in.readInt();
        ticketCount = in.readInt();
        ticketUnitPrice = in.readInt();
        remainTicketCount = in.readInt();
        returnPoint = in.readInt();

        in.readList(usableDate, Integer.class.getClassLoader());
        in.readList(usableStaTime, Integer.class.getClassLoader());
        in.readList(usableEndTime, Integer.class.getClassLoader());


        scheduledRecoveryTime = in.readString();
        expireDateTime = new ArrayList<>();
        in.readList(expireDateTime, Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<Ticket> CREATOR = new Parcelable.Creator<Ticket>() {
        @Override
        public Ticket createFromParcel(Parcel in) {
            return new Ticket(in);
        }

        @Override
        public Ticket[] newArray(int size) {
            return new Ticket[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(oid);
        dest.writeInt(companyRef);
        dest.writeInt(memberRef);
        dest.writeInt(senderRef);
        dest.writeString(reason);
        dest.writeInt(ticketRef);
        dest.writeInt(ticketCategory);
        dest.writeString(ticketName);
        dest.writeInt(useCount);
        dest.writeInt(ticketCount);
        dest.writeInt(ticketUnitPrice);
        dest.writeInt(remainTicketCount);
        dest.writeInt(returnPoint);

        dest.writeList(usableDate);
        dest.writeList(usableStaTime);
        dest.writeList(usableEndTime);
        dest.writeString(scheduledRecoveryTime);
        dest.writeList(expireDateTime);
    }
}
