package kr.co.mergepoint.mergeclient.scene.payment.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.SearchCrowdUserItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.SearchCrowdPayUserAdapterListener;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class SearchCrowdPayUserAdapter extends BasicListAdapter<BasicListHolder<SearchCrowdUserItemBinding, CrowdMember>, CrowdMember> implements CompoundButton.OnCheckedChangeListener {

    private ArrayList<Integer> oidList;
    private View.OnClickListener onClickListener;
    private SearchCrowdPayUserAdapterListener adapterListener;

    public ArrayList<Integer> getOidList() {
        return oidList;
    }

    public SearchCrowdPayUserAdapter(View.OnClickListener onClickListener, SearchCrowdPayUserAdapterListener adapterListener, ArrayList<CrowdMember> arrayList) {
        super(arrayList);
        this.onClickListener = onClickListener;
        this.adapterListener = adapterListener;
        oidList = new ArrayList<>();
    }

    @Override
    public BasicListHolder<SearchCrowdUserItemBinding, CrowdMember> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_crowd_user_item, parent, false);
        SearchCrowdUserItemBinding crowdUserBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<SearchCrowdUserItemBinding, CrowdMember>(crowdUserBinding) {
            @Override
            public void setDataBindingWithData(CrowdMember data) {
                getDataBinding().setUser(data);
                getDataBinding().checkButton.setChecked(oidList.contains(data.oid));
                getDataBinding().setOnClick(onClickListener);
                getDataBinding().setOnCheckChangeListener(SearchCrowdPayUserAdapter.this);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<SearchCrowdUserItemBinding, CrowdMember> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean check) {
        int oid = (int) compoundButton.getTag();
        if (check) {
            oidList.add(oid);
        } else {
            int index = oidList.indexOf(oid);
            if (index >= 0)
                oidList.remove(index);
        }
        adapterListener.checkUser(oidList.size());
    }
}
