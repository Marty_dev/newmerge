package kr.co.mergepoint.mergeclient.scene.data.main;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * User: Marty
 * Date: 2018-08-17
 * Time: 오전 9:14
 * Description:
 */
public class GradeRes {
    @SerializedName("oid")
    public int oid;
    @SerializedName("nextPrice")
    public int nextPrice;
    @SerializedName("grade")
    public int grade;
    @SerializedName("gradeList")
    public ArrayList<MemberGrade> gradeList;

}
