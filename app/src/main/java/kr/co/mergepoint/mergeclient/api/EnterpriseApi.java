package kr.co.mergepoint.mergeclient.api;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.CompanyPointHistory;
import kr.co.mergepoint.mergeclient.scene.data.login.MemberPoint;
import kr.co.mergepoint.mergeclient.scene.data.main.Company;
import kr.co.mergepoint.mergeclient.scene.data.payment.AdaptPoint;
import kr.co.mergepoint.mergeclient.scene.data.payment.Approval;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPayItem;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.data.payment.Ticket;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by 1017sjg on 2017. 6. 28..
 */

public interface EnterpriseApi {
    @GET("company/find")
    Call<ResponseObject<ArrayList<Company>>> getSearchEnterprise(@Query("company") String company);

    @POST("company/regist")
    @FormUrlEncoded
    Call<ResponseObject<Boolean>> registerEnterprise(@Field("companyRef") int companyRef);

    @GET("crowdPay/member/list")
    Call<ResponseObject<ArrayList<CrowdMember>>> getCrowedMemberList(@Query("type") int type, @Query("keyword") String keyword);

    @GET("crowdPay/companyPoint/list/{payShopOid}")
    Call<ResponseObject<ArrayList<CrowdPay>>> requestCrowedPay(@Path("payShopOid") int payShopOid);

    @GET("crowdPay/companyPoint/define/{crowdPayRef}")
    Call<ResponseObject<MemberPoint>> requestSettingCrowedPayPoint(@Path("crowdPayRef") int crowdPayRef);

    @POST("crowdPay/member/add/{payShopRef}")
    @FormUrlEncoded
    Call<ResponseObject<ArrayList<CrowdPay>>> requestSelectCrowedMember(@Path("payShopRef") int payShopRef, @Field("oids") String oids);

    @POST("crowdPay/favorite/change/{oid}")
    Call<ResponseObject<Long>> changeCrowdPayFavorite(@Path("oid") int memberOid);

    @POST("crowdPay/companyPoint/adapt")
    Call<ResponseObject<ArrayList<CrowdPay>>> requestSelectPoint(@Body AdaptPoint adaptPoint);

    @POST("crowdPay/companyPoint/approveRequest/{payShopRef}")
    Call<ResponseObject<ArrayList<CrowdPay>>> requestCrowdApproval(@Path("payShopRef") int payShopRef);

    @GET("comPoint/ticket/list")
    Call<ResponseObject<ArrayList<Ticket>>> getTicketList();

    @GET("crowdPay/request/list")
    Call<ResponseObject<ArrayList<CrowdPay>>> getCrowdApprovalRequestList();

    @GET("comPoint/list")
    Call<ResponseObject<ArrayList<CompanyPointHistory>>> getCompanyPointHistory(@Query("type") int type, @Query("period") int period);

    @GET("crowdPay/payment/list")
    Call<ResponseObject<ArrayList<CrowdPayItem>>> getCrowdPaymentHistory(@Query("type") int type, @Query("period") int period);

    @POST("crowdPay/request/approve")
    Call<ResponseObject<ArrayList<CrowdPay>>> sendCheckApproval(@Body Approval approval);

    @POST("crowdPay/companyPoint/retry/{crowdPayRef}")
    Call<ResponseObject<ArrayList<CrowdPay>>> retryCrowdApproval(@Path("crowdPayRef") int crowdPayRef);

    @POST("crowdPay/companyPoint/delete/{crowdPayRef}")
    Call<ResponseObject<ArrayList<CrowdPay>>> deleteCrowdApproval(@Path("crowdPayRef") int crowdPayRef);

    @GET("crowdPay/payment/detail/{crowdPayRef}")
    Call<ResponseObject<ShopPayment>> requestCrowdPayHistoryDetail(@Path("crowdPayRef") int crowdPayRef);

}
