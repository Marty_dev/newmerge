package kr.co.mergepoint.mergeclient.scene.shoplist.presenter.callback;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public interface ShopListCallbackInterface {

    MergeCallback<ResponseObject<ArrayList<Shop>>> searchingMapCallback();
}
