package kr.co.mergepoint.mergeclient.scene.shoplist.view;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;

import kr.co.mergepoint.mergeclient.R;

/**
 * Created by jgson on 2018. 1. 8..
 */

public class FilterDrawerLayout extends DrawerLayout {
    public FilterDrawerLayout(Context context) {
        super(context);
    }

    public FilterDrawerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FilterDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        View view = findViewById(R.id.shop_conv);

        if (view != null && view instanceof HorizontalScrollView) {
            HorizontalScrollView scrollView = (HorizontalScrollView) view;

            int[] point = new int[2];
            int width = scrollView.getWidth();
            int height = scrollView.getHeight();
            scrollView.getLocationOnScreen(point);

            int minX = point[0];
            int maxX = minX + width;
            int minY = point[1];
            int maxY = minY + height;

            if ((minX <= ev.getX() && maxX >= ev.getX()) && (maxY >= ev.getY() && minY <= ev.getY()))
                return false;
        }
        return super.onInterceptTouchEvent(ev);
    }
}
