package kr.co.mergepoint.mergeclient.scene.shop.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import net.daum.mf.map.api.MapView;

/**
 * Created by jgson on 2018. 1. 5..
 */

public class ShopMapView extends MapView {

    public ShopMapView(Activity activity) {
        super(activity);
    }

    public ShopMapView(Context context) {
        super(context);
    }

    public ShopMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ShopMapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                this.getParent().requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_UP:
                this.getParent().requestDisallowInterceptTouchEvent(false);
                break;
        }
        super.onTouchEvent(event);
        return true;
    }
}
