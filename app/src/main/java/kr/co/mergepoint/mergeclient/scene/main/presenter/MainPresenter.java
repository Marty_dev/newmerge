package kr.co.mergepoint.mergeclient.scene.main.presenter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.util.ArrayList;
import java.util.Map;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BasePresenter;
import kr.co.mergepoint.mergeclient.application.common.OnTypeListener;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.application.common.RecyclerItemListener;
import kr.co.mergepoint.mergeclient.application.common.StreetButton;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocation;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocationListener;
import kr.co.mergepoint.mergeclient.scene.QRcodeActivity;
import kr.co.mergepoint.mergeclient.scene.coupon.CouponApply;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.main.Contactus;
import kr.co.mergepoint.mergeclient.scene.data.main.MergesPick;
import kr.co.mergepoint.mergeclient.scene.data.main.Notice;
import kr.co.mergepoint.mergeclient.scene.data.main.userPointinfo;
import kr.co.mergepoint.mergeclient.scene.data.payment.Approval;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPayItem;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;
import kr.co.mergepoint.mergeclient.scene.franchise.view.FranchiseListActivity;
import kr.co.mergepoint.mergeclient.scene.franchise.view.FranchiseShopListActivity;
import kr.co.mergepoint.mergeclient.scene.login.LoginActivity;
import kr.co.mergepoint.mergeclient.scene.main.model.MainModel;
import kr.co.mergepoint.mergeclient.scene.main.presenter.callback.MainCallback;
import kr.co.mergepoint.mergeclient.scene.main.view.MainView;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.MainBannerAdapter;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.MainNewsAdapter;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.MainStreetAdapter;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.MergesPickAdapter;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder.ChildEventHolder;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.ContactusFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.CrowdApprovalRequestFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.DelegationUserFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.FavoriteFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.NoticeFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.QuestionDirectFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.ThemeFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.WithdrawalFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.CrowdPayHistoryListener;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.DelegationUserListener;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.EnterprisePointListener;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.SettingsCrowedApprovalListener;
import kr.co.mergepoint.mergeclient.scene.myzonelist.MyZoneListActivity;
import kr.co.mergepoint.mergeclient.scene.newcoupon.view.newMyCouponActivity;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.SearchCrowdPayUserListener;
import kr.co.mergepoint.mergeclient.scene.privacy.PrivacyActivity;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;
import kr.co.mergepoint.mergeclient.scene.shoplist.ShopListActivity;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.ShopListAdapter;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ALARM_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ALARM_LIST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ATTACH_PIC_RESULT_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.COMPANY_INFO_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.COMPANY_INFO_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.COMPANY_INFO_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CONTACT_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CONTACT_US_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CROWD_APPROVAL_REQUEST_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.DELEGATION_APPROVAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.DELEGATION_USER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.DELEGATION_USER_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ELECT_FINANCIAL_TERMS_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ELECT_TERMS_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ELECT_TERMS_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_NOTICE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_NOTICE_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.EVENT_DETAIL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.EVENT_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.EVENT_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.EVENT_LIST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.EXTERNAL_AD_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FAVORITE_CROWED_USER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FAVORITE_CROWED_USER_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FAVORITE_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FAVORITE_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.GALLERY_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.GRADE_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.GRADE_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INITIAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INTENT_EVENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INTENT_FORGOT_PW;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INTENT_REGISTER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INTENT_SHOP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LICENSE_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MERGES_PICK_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MERGES_PICK_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NOTICE_DETAIL_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NOTICE_DETAIL_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.OID;
import static kr.co.mergepoint.mergeclient.application.common.Properties.OPEN_SOURCE_LICENSE_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.OPEN_SOURCE_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PERMISSIONS_REQUEST_LOCATION;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PERMISSIONS_REQUEST_LOCATION_FAVORITE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PERMISSIONS_REQUEST_LOCATION_THEME;
import static kr.co.mergepoint.mergeclient.application.common.Properties.POINT_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.POINT_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PRIVACY_POLICY_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PRIVACY_POLICY_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PRIVACY_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.QR_CODE_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.QUESTIONS_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.QUESTIONS_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.QUESTION_DIRECT_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.QUESTION_DIRECT_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RESET_PW_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RESET_PW_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SERVICE_CENTER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SETTINGS_APPROVAL_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SETTINGS_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SETTINGS_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SETTING_APPROVAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_FAVORITE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_OID;
import static kr.co.mergepoint.mergeclient.application.common.Properties.STREET_NAME;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TERMS_USE_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TERMS_USE_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.THEME_LIST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.THEME_LIST_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.THEME_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TOUCH_COUPON;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TOUCH_EVENT_LIST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TOUCH_EX_AD;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TOUCH_IN_AD;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TOUCH_SHARE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TOUCH_SHOP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USER_GUIDE_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USER_PATH;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WEB_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WITHDRAWAL_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WITHDRAWAL_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WITHDRAWAL_POLICY_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WITHDRAWAL_POLICY_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WITHDRAWAL_POLICY_URL;

/**
 * Created by jgson on 2017. 5. 31..
 */

public class MainPresenter extends BasePresenter<MainView, MainModel, MainCallback>
        implements ViewPager.OnPageChangeListener, TextView.OnEditorActionListener, OnTypeListener,
        StreetButton.OnStreetListener, NoticeFragment.OnNotice, RecyclerItemListener.RecyclerTouchListener,
        UserLocationListener, SettingsCrowedApprovalListener, SearchCrowdPayUserListener, EnterprisePointListener, CrowdPayHistoryListener, DelegationUserListener {

    private UserLocation userLocation;

    public MainPresenter(MainView view, MainModel model, MainCallback callback, UserLocation userLocation) {
        super(view, model, callback);
        this.userLocation = userLocation;
    }

    @Override
    protected void setBindingProperty() {
        baseView.binding.setOnClick(this);
        baseView.binding.setInitInfo(MergeApplication.getInitialInfo());
        baseView.binding.setBottomBannerPagerListener(this);
        baseView.binding.setSearchEnterListener(this);
        baseView.binding.setMainNewsListener(this);
        baseView.binding.mainStreetListview.setLayoutManager(new GridLayoutManager(baseView.activity,3));
        baseView.binding.mainStreetListview.setAdapter(new MainStreetAdapter(MergeApplication.getInitialInfo().getStreetName(),this,baseView.activity));
        InitialInfo initialInfo = MergeApplication.getInitialInfo();
        if (initialInfo != null) {
            baseView.binding.setNewsAdapter(new MainNewsAdapter(initialInfo.getNotice()));
            baseView.binding.setBottomBannerPager(new MainBannerAdapter(initialInfo.getBanner(), R.drawable.obj_main_empty_banner, this));
            baseView.binding.setMainMergesPickPager(new MergesPickAdapter(initialInfo.getMergespick(), R.drawable.img_empty_theme, this));
         //   baseView.binding.mainBottomBannerIndicator.createDotPanel(initialInfo.getBanner().size() == 0 ? 1 : initialInfo.getBanner().size());
        }



    }

    public void onCreate() {
        InitialInfo initialInfo = MergeApplication.getInitialInfo();
        if (initialInfo != null) {
            baseView.setStreet(initialInfo.getStreetName(), this);
            Member member = initialInfo.getMember();
            if (member != null) {
                Crashlytics.setUserIdentifier(String.valueOf(member.getOid()));
                Crashlytics.setUserName(member.getName());
                Crashlytics.setUserEmail(member.getId());
                String token = FirebaseInstanceId.getInstance().getToken();
                if (member.isNeedRegist() || (token != null && !token.equals(member.getFcmKey())))
                    baseModel.requestMemberKey(baseCallback.getTokenCallback(), token);
            }
        }
        baseView.showTutorialView();
        baseView.showEnterpriseState();
        baseView.showEnterpriseNotice();
    }

    public void onResume() {
        baseView.removeAllFocus();
        baseView.startAutoBannerSlide();
        baseModel.refreshUserPoints(baseCallback.refreshUserPoints());
        if (MergeApplication.getInitialInfo().isSignin())
            baseView.binding.mainWelcomMember.setText(Html.fromHtml("<b>"+MergeApplication.getInitialInfo().getMember().getName() + "</b>님<br/>오늘은 어디로 가실건가요?"));
        baseView.binding.mainWelcomMember.setVisibility(MergeApplication.getInitialInfo().isSignin() ? View.VISIBLE : View.GONE);
        baseView.binding.mainWelcomNonmemberRl.setVisibility(MergeApplication.getInitialInfo().isSignin() ? View.GONE : View.VISIBLE);
        if (MergeApplication.getInitialInfo().isSignin()) {
            baseModel.readInfo(new Callback<userPointinfo>() {
                @Override
                public void onResponse(Call<userPointinfo> call, Response<userPointinfo> response) {
                    MDEBUG.debug("Success Info " + response.body());
                    Member member = MergeApplication.getInitialInfo().getMember();
                    userPointinfo info = response.body();
                    member.isCompanyMember = info.companyMember;
                    int Points = info.personalPoints;
                    if (response.body().companyMember) {
                        member.EnterPoint = info.companyPoints;
                        member.personalPoint = info.personalPoints;
                        member.alarmCount = info.alarmCount;

                        Points += info.companyPoints;
                    }
                    baseView.binding.setPoint(Points);

                }
                @Override
                public void onFailure(Call<userPointinfo> call, Throwable t) {
                    MDEBUG.debug("Failed Info " + t.toString());
                }
            });
        }


    }

    public void onPostResume() {
        Map<String, Object> callInfo = baseModel.callApp(baseView.activity);

        try {
            String intentType = (String) callInfo.get("type");
            String userPath = callInfo.containsKey("userPath") ? (String) callInfo.get("userPath") : "";
            int oid = callInfo.containsKey("oid") ? (int) callInfo.get("oid") : NO_VALUE;
            boolean crowd = callInfo.containsKey("CROWD_PAY_APPROVAL") && (boolean) callInfo.get("CROWD_PAY_APPROVAL");

            MDEBUG.debug("crowd : " + crowd);
            if (callInfo.containsKey("value") && callInfo.containsKey("action")){
                String title="",value ="",action ="";
                title = (String)callInfo.get("title");
                value = (String)callInfo.get("value");
                action = (String)callInfo.get("action");

                MDEBUG.debug("title : " + title  + " , value : " + value + ", action : " + action );

                switch (action){
                    case "22":
                    case "23":
                    case "24":
                        baseView.openActivity(UsageActivity.class);
                        return;
                    case "19":
                    case "20":
                    case "21":
                        baseView.openActivity(newMyCouponActivity.class);
                        return;
                }

            }
            if (crowd) {
                baseView.showLoading();
                baseModel.getCrowdApprovalRequestList(baseCallback.getCrowdApprovalRequestList());
            } else {
                switch (intentType) {
                    case INTENT_SHOP:
                        baseModel.getVisitShopInfo(baseCallback.shopVisitCallback(), oid);
                        break;
                    case INTENT_EVENT:
                        baseModel.requestEventDetail(baseCallback.getEventDetailCallback(), oid);
                        break;
                    case INTENT_FORGOT_PW:
                        Bundle bundle = new Bundle();
                        bundle.putInt(OID, oid);
                        bundle.putString(USER_PATH, userPath);
                        baseView.openPage(RESET_PW_FRAGMENT, bundle, RESET_PW_TAG);
                        break;
                    case INTENT_REGISTER:
                        baseModel.registMember(baseCallback.getMemberInfoCallback(), userPath);
                        break;
                }
            }
        } catch (Exception ignored) {}
    }

    public void onPause() {
        baseView.stopAutoBannerSlide();
    }

    @TargetApi(Build.VERSION_CODES.M)
    @SuppressLint("MissingPermission")
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == QR_CODE_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // 승낙
                baseView.openActivityForResult(QRcodeActivity.class, QR_CODE_REQUEST);
            } else {
                // 거절
                if (baseView.activity.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA))
                    baseView.showAlert(R.string.qr_request_permission);
            }
        } else if (requestCode == GALLERY_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                baseView.openChooseGallery();
            } else {
                if (baseView.activity.shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE))
                    baseView.showAlert(R.string.gallery_request_permission);
            }
        } else if (requestCode == PERMISSIONS_REQUEST_LOCATION_THEME) {
            ThemeFragment themeFragment = (ThemeFragment) baseView.findFragmentByTag(THEME_LIST_TAG);
            if (themeFragment != null)
                themeFragment.getUserLocation().startLocationUpdates();
        } else if (requestCode == PERMISSIONS_REQUEST_LOCATION_FAVORITE) {
            FavoriteFragment favoriteFragment = (FavoriteFragment) baseView.findFragmentByTag(FAVORITE_FRAGMENT_TAG);
            if (favoriteFragment != null)
                favoriteFragment.getUserLocation().startLocationUpdates();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == QR_CODE_REQUEST && data != null) {
                String oid = data.getStringExtra("oid");
                if (oid != null) {
                    try {
                        baseModel.getVisitShopInfo(baseCallback.shopVisitCallback(), Integer.parseInt(oid));
                    } catch (NumberFormatException e) {
                        new MergeDialog.Builder(baseView.activity).setContent(R.string.wrong_url_text).setCancelBtn(false).build().show();
                    }
                } else {
                    new MergeDialog.Builder(baseView.activity).setContent(R.string.wrong_url_text).setCancelBtn(false).build().show();
                }
            } else if (requestCode == ATTACH_PIC_RESULT_REQUEST) {
                QuestionDirectFragment fragment = (QuestionDirectFragment) baseView.findFragmentByTag(QUESTION_DIRECT_FRAGMENT_TAG);
                fragment.setAttachFileName(data.getData());
            } else if (requestCode == FAVORITE_REQUEST || requestCode == THEME_REQUEST) {
                boolean isFavorite = data.getBooleanExtra(SHOP_FAVORITE, false);
                int oid = data.getIntExtra(SHOP_OID, NO_VALUE);
                baseView.setListWithFavorite(requestCode, oid, isFavorite);
            }
        }

        /*페이스북 콜백 매니져*/
        if (baseView.getShareFacebookCallback() != null)
            baseView.getShareFacebookCallback().onActivityResult(requestCode, resultCode, data);
    }

    /*위치 권한*/
    @Override
    protected void grantedLocation() {
        userLocation.startLocationUpdates();
    }

    @Override
    protected void deniedLocation() {
        startLoadList(null);
    }

    /*위치 권한 요청 및 위치에 따른 리스트 업데이트*/
    @Override
    public void requestPermissions() {
        baseView.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_LOCATION);
    }

    @Override
    public void startLoadList(Location location) {
        ThemeFragment themeFragment = (ThemeFragment) baseView.findFragmentByTag(THEME_LIST_TAG);
        if (themeFragment != null) {
            themeFragment.getFilter().setLocation(location);
            themeFragment.getShopList().loadShopList(INITIAL, themeFragment.getFilter().getThemeSearch());
        } else {
            baseModel.requestFavoriteList(baseCallback.getFavoriteListCallback(), location);
        }
    }

    /*메인 화면에서의 버튼 클릭 이벤트*/
    @Override
    public void onClick(final View v) {
        super.onClick(v);
        switch (v.getId()) {
            /* 메인 */
            /* 메뉴 열기/닫기 */
            case R.id.open_menu:
                baseView.openMenuAnim();
                break;
            case R.id.close_menu:
                baseView.activity.getSupportFragmentManager().popBackStack();
                break;

            /* 메인 검색 */
            case R.id.main_search:
                baseView.openMainSearch();
                break;
            /* 제휴 문의*/
            /*case R.id.alliance:
                baseView.openPage(CONTACT_US_FRAGMENT, CONTACT_FRAGMENT_TAG);
                break;*/
            /* 사용자 포인트 내역 */
            case R.id.point_btn:
            case R.id.user_name:
            case R.id.user_point_num:
                baseView.openPage(POINT_FRAGMENT, POINT_FRAGMENT_TAG);
                break;
            /* 머지스픽 리스트 */
            case R.id.main_merges_pick_total:
                baseView.openPage(MERGES_PICK_FRAGMENT, MERGES_PICK_TAG);
                break;
            /* 공지사항 리스트 */
            case R.id.main_news_total:
                baseView.showLoading();
                baseModel.requestNoticeList(baseCallback.getNoticeListCallback());
                break;
            /* 메인 배너 */
            case R.id.banner:
                String data = baseView.getBannerActionData();
                if (data != null && data.length() > 0) {
                    switch (baseView.getBannerActionType()) {
                        case EVENT_DETAIL: /* 이벤트상세 페이지*/
                            baseModel.requestEventDetail(baseCallback.getEventDetailCallback(), Integer.parseInt(data));
                            break;
                        case EVENT_LIST: /* 이벤트 리스트 */
                            baseView.openPage(EVENT_FRAGMENT, EVENT_FRAGMENT_TAG);
                            break;
                        case THEME_LIST: /* 테마 카테고리에 해당하는 리스트 */
                            baseView.openThemeFragment(userLocation, data);
                            break;
                        default:
                            break;
                    }
                }
                break;
            /* 머지스픽 상세보기 */
            case R.id.main_merges_pick_img:
                int mergesPickPosition = baseView.binding.mergesPickPager.getCurrentItem();
                MergesPick selectMergesPick = baseView.binding.getMainMergesPickPager().getMergesPicks().get(mergesPickPosition);
                baseModel.requestMergesPickDetail(baseCallback.getMergesPickCallback(), selectMergesPick.oid);
                break;
            /* 머지스픽 리스트에서 상세보기로 가기 */
            case R.id.merges_pick_list_img:
                baseModel.requestMergesPickDetail(baseCallback.getMergesPickCallback(), (int) v.getTag());
                break;
            /* 제휴문의 보내기 */
            case R.id.send_alliance:
                ContactusFragment contactusFragment = (ContactusFragment) baseView.findFragmentByTag(CONTACT_FRAGMENT_TAG);
                Contactus contactus = contactusFragment.getAllianceData();
                if (contactus != null)
                    baseModel.alliance(baseCallback.getAllianceCallback(), contactus);
                break;
            /* 이용약관 */
            case R.id.terms_of_use:
                baseView.openVerticalPage(TERMS_USE_FRAGMENT, createWebBundle(USER_GUIDE_URL), TERMS_USE_TAG);
                break;
            /* 개인정보 처리방침 */
            case R.id.privacy_policy:
                baseView.openVerticalPage(PRIVACY_POLICY_FRAGMENT, createWebBundle(PRIVACY_URL), PRIVACY_POLICY_TAG);
                break;
            /* 사업자 정보 */
         /*   case R.id.company_info:
                baseView.openVerticalPage(COMPANY_INFO_FRAGMENT, createWebBundle(COMPANY_INFO_URL), COMPANY_INFO_TAG);
                break;
            /* 전자금융거래 이용약관 */
           /* case R.id.electronic_finance:
                baseView.openVerticalPage(ELECT_FINANCIAL_TERMS_FRAGMENT, createWebBundle(ELECT_TERMS_URL), ELECT_TERMS_TAG);
                break;
            /*비밀번호 변경*/
            case R.id.reset_pw_send_mail:
                if (baseView.getEqualPw()) {
                    Map<String, Object> info = baseView.getResetPwInfo();
                    if (info.size() > 0) {
                        baseView.showLoading();
                        baseModel.resetPassword(baseCallback.resetPasswordCallback(), info);
                    }
                }
                break;
            /* 메뉴 */
            /* 서비스 센터 전화걸기 */
            case R.id.main_service_call:
                baseView.activity.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "02-333-2262")));
                break;
            case R.id.service_center:
                baseView.callServiceCenter();
                break;
            /* 메뉴에서 비회원 로그인 */
            case R.id.main_nonmember_login:
            case R.id.menu_login_icon:
                baseView.openActivity(LoginActivity.class);
                break;
            /* 나의 정보 */
            case R.id.user_info_layout:
                baseView.openActivity(PrivacyActivity.class);
                break;
            /* 구  QR 코드 읽기 현 이용내역 리스트   2018 10 10 */
            case R.id.qr_code:
                //baseView.requestCameraPermission();
                //break;
            /* 이용내역 */
            case R.id.list_layout:
                baseView.openActivity(UsageActivity.class);
                break;
            /* 이벤트 리스트 */
            case R.id.event_layout:
                baseView.openPage(EVENT_FRAGMENT, EVENT_FRAGMENT_TAG);
                break;
            /* 관심매장 리스트 */
            case R.id.favorites_store_layout:
                baseView.openFavoriteFragment(userLocation);
                break;
            /* 사용자 쿠폰 리스트 */
            case R.id.coupon_layout:
                baseView.openActivity(newMyCouponActivity.class);

//                baseView.showLoading();
//                baseModel.getMyCouponList(baseCallback.getMyCouponList());
                break;
            /* 자주 묻는 질문 */
            case R.id.frequently_asked_questions:
                baseView.openPage(QUESTIONS_FRAGMENT, QUESTIONS_FRAGMENT_TAG);
                break;
            /* 문의하기 */
            case R.id.question_direct:
                baseView.openPage(QUESTION_DIRECT_FRAGMENT, QUESTION_DIRECT_FRAGMENT_TAG);
                break;
            /* 공지사항 리스트 */
            case R.id.notice_layout:
                baseModel.requestNoticeList(baseCallback.getNoticeListCallback());
                break;
            /* 등급 정보 */
            case R.id.user_grade_image:
                baseView.openPage(GRADE_FRAGMENT, GRADE_FRAGMENT_TAG);
                break;
                // 쿠폰 입력
            /* 설정 */
            case R.id.setting:
                baseView.openPage(SETTINGS_FRAGMENT, SETTINGS_FRAGMENT_TAG);
                break;
            /* 문의하기 사진 첨부 */
            case R.id.add_file:
                baseView.requestGalleryPermission();
                break;
            /* 문의 보내기 */
            case R.id.send_question:
                QuestionDirectFragment fragment = (QuestionDirectFragment) baseView.findFragmentByTag(QUESTION_DIRECT_FRAGMENT_TAG);
                Contactus question = fragment.getQuestionData();
                if (question != null)
                    baseModel.question(baseCallback.getQuestionCallback(), question);
                break;
            /* 설정 */
            /* 로그아웃 */
            case R.id.logout:
                if ((Boolean) v.getTag()) {
                    baseModel.logout(baseCallback.getLogoutCallback());
                } else {
                    baseView.openActivity(LoginActivity.class);
                }
                break;
            /* 회원 탈퇴 */
            case R.id.withdrawal_btn:
                WithdrawalFragment withdrawalFragment = (WithdrawalFragment)baseView.findFragmentByTag(WITHDRAWAL_FRAGMENT_TAG);
                boolean ischecked = (withdrawalFragment != null) && withdrawalFragment.isRefund();
                MDEBUG.debug("withdraw is checked"  + ischecked);
                if (ischecked) {
                    new MergeDialog.Builder(baseView.activity).setContent(R.string.withdrawal_confirm_text)
                            .setConfirmClick(() -> baseModel.withdrawal(baseCallback.getWithdrawalCallback(), baseView.getWithdrawalRefund())).build().show();
                }else{

                }
                break;
            /* 회원 탈퇴/환불 정책*/
            case R.id.withdrawal_tv_02:
                baseView.openVerticalPage(WITHDRAWAL_POLICY_FRAGMENT, createWebBundle(WITHDRAWAL_POLICY_URL), WITHDRAWAL_POLICY_TAG);
                break;
            /* 회원 탈퇴 페이지 열기*/
            case R.id.withdrawal:
                baseView.openPage(WITHDRAWAL_FRAGMENT, WITHDRAWAL_FRAGMENT_TAG);
                break;
            /* 라이센스 */
            case R.id.license:
                baseView.openVerticalPage(LICENSE_FRAGMENT, createWebBundle(OPEN_SOURCE_LICENSE_URL), OPEN_SOURCE_TAG);
                break;
            case R.id.tutorial_end:
                baseView.closeTutorial();
                break;
            /* 메인 제휴 기업 식당 */
            case R.id.approval_management:
                baseView.openPage(SETTING_APPROVAL, SETTINGS_APPROVAL_FRAGMENT_TAG);
                break;
            case R.id.delegation_user_name:
                baseView.openPage(DELEGATION_USER, DELEGATION_USER_FRAGMENT_TAG);
                break;
            case R.id.alliance_favorite_layout:
                baseView.openPage(FAVORITE_CROWED_USER, FAVORITE_CROWED_USER_FRAGMENT_TAG);
                break;
            case R.id.ticket_layout:
                baseView.showLoading();
                baseModel.getTicketList(baseCallback.getTicketList());
                break;
            case R.id.enterprise_point_layout:
                changeEnterprisePointHistory(1,1);
                break;
            case R.id.crowd_pay_list_layout:
                changeCrowdPayHistory(1,1);
                break;
            case R.id.main_myzone_btn:
            case R.id.corporate_alliance_button:
                baseView.openActivityWithString(MyZoneListActivity.class, STREET_NAME, "My Zone");
                break;
            case R.id.main_franchise_button:
            case R.id.franchise_button:
                baseView.openActivity(FranchiseListActivity.class);
                break;
            case R.id.crowd_favorite_button:
                baseView.showLoading();
                baseModel.changeFavoriteCrowedMember(baseCallback.changeFavoriteCrowdMember(), (int) v.getTag());
                break;
            case R.id.crowd_pay_approval_request_cancel:
            case R.id.crowd_pay_approval_request_ok:
                CrowdApprovalRequestFragment requestFragment = (CrowdApprovalRequestFragment) baseView.findFragmentByTag(CROWD_APPROVAL_REQUEST_FRAGMENT_TAG);
                Approval approval = R.id.crowd_pay_approval_request_ok == v.getId() ? requestFragment.getApproval() : requestFragment.getApprovalReject();
                baseView.showLoading();
                baseModel.sendCheckApproval(baseCallback.sendCheckApproval(), approval);
                break;
            case R.id.crowd_pay_approval_delegation_apply:
                baseView.showLoading();
                DelegationUserFragment userFragment = (DelegationUserFragment) baseView.findFragmentByTag(DELEGATION_USER_FRAGMENT_TAG);
                CrowdMember member = userFragment.getSelectCrowdMember();
                if (member != null)
                    baseModel.changeCrowedApprovalType(baseCallback.changeCrowdApprovalType(DELEGATION_APPROVAL, member.oid, member.name), DELEGATION_APPROVAL, member.oid);
                break;
            case R.id.enterprise_notice_layout:
                baseView.openPage(ENTERPRISE_NOTICE, ENTERPRISE_NOTICE_FRAGMENT_TAG);
                break;
            case R.id.pointadd_layout:
                Intent inte = new Intent(baseView.activity, CouponApply.class);
                baseView.activity.startActivity(inte);
                break;
            case R.id.alarm:
                baseView.openPage(ALARM_LIST, ALARM_FRAGMENT_TAG);

                break;
            case R.id.alarm_content:
                baseView.showLoading();
                baseModel.getCrowdApprovalRequestList(baseCallback.getCrowdApprovalRequestList());
                break;
            case R.id.textbtn :
                break;

                // Street 변경후
            case R.id.main_street_item_rl:
                onMainStreetTouch((String)v.getTag());
                break;

        }
    }

    private Bundle createWebBundle(String url) {
        Bundle bundle = new Bundle();
        bundle.putString(WEB_URL, url);
        return bundle;
    }

    /* 메인 검색의 키보드 검색 아이콘 리스너 */
    @Override
    public boolean onEditorAction(TextView textView, int keyCode, KeyEvent keyEvent) {
        if (keyCode == EditorInfo.IME_ACTION_SEARCH) {
            baseView.openMainSearch();
            return true;
        }
        return false;
    }

    /* 점포 즐겨찾기 터치 리스너 */
    public void onListTouchItem(RecyclerView recyclerView, View view, int position) {
        ShopListAdapter shopListAdapter = (ShopListAdapter) recyclerView.getAdapter();
        baseView.openShopDetail(shopListAdapter.getObservableArrayList().get(position));
//        baseView.openActivityForResultWithParcelable(ShopActivity.class, SHOP_INFO, shopListAdapter.getObservableArrayList().get(position), FAVORITE_REQUEST);
    }

    /* 이벤트 상세 페이지 열기 */
    public void onTouchEvent(ChildViewHolder childViewHolder) {
        ChildEventHolder childEventHolder = (ChildEventHolder) childViewHolder;
        baseModel.requestEventDetail(baseCallback.getEventDetailCallback(), childEventHolder.getEventInfo().oid);
    }

    /* 하단 배너 페이지 인디케이터 */
    @Override
    public void onPageSelected(int position) {
    //    baseView.binding.mainBottomBannerIndicator.selectDot(position);
    }
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
    @Override
    public void onPageScrollStateChanged(int state) {}

    /* 액션 타입 버튼 리스너 */
    @Override
    public void onTypeClick(@Properties.TouchType int type, String data) {
        switch (type) {
            case TOUCH_SHOP:
                baseView.openActivityWithInt(ShopActivity.class, SHOP_INFO, Integer.parseInt(data));
                break;
            case TOUCH_SHARE:
                baseView.openEventShared();
                break;
            case TOUCH_COUPON:
                /* 쿠폰 다운로드 */
                baseModel.downCoupon(baseCallback.getCouponCallback(), data);
                break;
            case TOUCH_IN_AD:
                /* 아직 정해진 내용이 없음 */
                break;
            case TOUCH_EX_AD:
                baseView.openVerticalPage(EXTERNAL_AD_FRAGMENT, createWebBundle(data), OPEN_SOURCE_TAG);
                break;
            case TOUCH_EVENT_LIST:
                baseView.openPage(EVENT_FRAGMENT, EVENT_FRAGMENT_TAG);
                break;
        }
    }

    /* 메인 스트릿 터치 리스너 */
    @Override
    public void onMainStreetTouch(String streetName) {
        baseView.openActivityWithString(ShopListActivity.class, STREET_NAME, streetName);
    }

    /* 공지사항 리스트 터치 리스너 */
    @Override
    public void onNoticeClick(Notice notice) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(NOTICE_DETAIL_TAG, notice);
        baseView.openPage(NOTICE_DETAIL_FRAGMENT, bundle, NOTICE_DETAIL_TAG);
    }

    /* 공지사항 상세 페이지 열기 */
    @Override
    public void onClickItem(View v, int position) {
        InitialInfo initialInfo = MergeApplication.getInitialInfo();
        if (initialInfo != null) {
            ArrayList<Notice> notices = initialInfo.getNotice();
            try {
                onNoticeClick(notices.get(position));
            } catch (IndexOutOfBoundsException ignore) {}
        }
    }

    /* 설정의 함께결제 자동승인 타입 지정하기 */
    @Override
    public void onClickApprovalType(int type) {
        baseModel.changeCrowedApprovalType(baseCallback.changeCrowdApprovalType(type, 0, null), type);
    }

    @Override
    public void onClickApprovalType(int type, int approvalDelegationUserOid, String approvalDelegationUserName) {
        baseModel.changeCrowedApprovalType(baseCallback.changeCrowdApprovalType(type, approvalDelegationUserOid, approvalDelegationUserName), type, approvalDelegationUserOid);
    }

    @Override
    public void updateCrowedMemberTable(int type, String searchString) {
        baseView.showLoading();
        baseModel.getCrowedMemberList(baseCallback.getCrowdMemberList(), type, searchString.isEmpty() ? null : searchString);
    }

    @Override
    public void changeEnterprisePointHistory(int type, int period) {
        baseView.showLoading();
        baseModel.getCompanyPointHistory(baseCallback.getCompanyPointHistory(), type, period);
    }

    @Override
    public void changeCrowdPayHistory(int type, int period) {
        baseView.showLoading();
        baseModel.getCrowdPaymentHistory(baseCallback.getCrowdPaymentHistory(), type, period);
    }

    @Override
    public void crowdPayHistoryDetail(CrowdPayItem crowdPayItem) {
        baseView.showLoading();
        baseModel.getCrowdPayHistoryDetail(baseCallback.getCrowdPayHistoryDetail(), crowdPayItem.crowdPayRef);
    }



    @Override
    public void updateDelegationTable(int type, String searchString) {
        baseView.showLoading();
        baseModel.getCrowedMemberList(baseCallback.getDelegationMemberList(), type, searchString.isEmpty() ? null : searchString);
    }
}
