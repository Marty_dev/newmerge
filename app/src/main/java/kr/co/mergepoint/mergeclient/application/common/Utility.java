package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 1017sjg on 2017. 6. 10..
 */

public class Utility {

    private Context context;

    public Utility(Context context) {
        this.context = context;
    }

    public Resources getResuorces() {
        return context.getResources();
    }

    public Drawable getDrawable(int res) {
        return ContextCompat.getDrawable(context, res);
    }

    public int getColor(int res) {
        return ContextCompat.getColor(context, res);
    }

    public boolean checkEmailFormat(String emailStr) {
        String regex = "^[_a-zA-Z0-9-.]+@[.a-zA-Z0-9-]+\\.[a-zA-Z]+$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(emailStr);
        return matcher.matches();
    }

    public boolean checkPhoneNumberFormat(String phone) {
        String regex = "^\\d{3}\\d{3,4}\\d{4}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    public int getDeviceWidth() {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public String getApiKeyFromManifest(String apiName) {
        String apiKey = null;

        try {
            String packageName = context.getPackageName();
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_META_DATA);
            Bundle bundle = info.metaData;
            if(bundle != null)
                apiKey = bundle.getString(apiName);
        } catch (Exception ignored) {}

        return apiKey;
    }
}
