package kr.co.mergepoint.mergeclient.scene.myzonelist;

import android.annotation.TargetApi;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import javax.inject.Inject;

import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocationListener;
import kr.co.mergepoint.mergeclient.scene.myzonelist.dagger.DaggerMyZoneListComponent;
import kr.co.mergepoint.mergeclient.scene.myzonelist.dagger.MyZoneListModule;
import kr.co.mergepoint.mergeclient.scene.myzonelist.presenter.MyZoneListPresenter;

/**
 * Created by 1017sjg on 2017. 6. 13..
 */

public class MyZoneListActivity extends BaseActivity implements UserLocationListener {

    @Inject MyZoneListPresenter presenter;

    @Override
    protected void onPostResume() {
        super.onPostResume();
        presenter.onPostResume();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerMyZoneListComponent.builder().myZoneListModule(new MyZoneListModule(this)).build().inject(this);
        presenter.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    protected void onActivityClick(View view) {
        presenter.onClick(view);
    }

    @Override
    public void requestPermissions() {
        presenter.requestPermissions();
    }

    @Override
    public void startLoadList(Location location) {
        presenter.startLoadList(location);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}