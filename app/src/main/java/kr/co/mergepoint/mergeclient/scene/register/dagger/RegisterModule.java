package kr.co.mergepoint.mergeclient.scene.register.dagger;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.SmsMessageReceiver;
import kr.co.mergepoint.mergeclient.scene.register.RegisterActivity;
import kr.co.mergepoint.mergeclient.scene.register.model.RegisterModel;
import kr.co.mergepoint.mergeclient.scene.register.presenter.RegisterPresenter;
import kr.co.mergepoint.mergeclient.scene.register.presenter.callback.RegisterCallback;
import kr.co.mergepoint.mergeclient.scene.register.view.RegisterView;

/**
 * Created by jgson on 2017. 6. 5..
 */

@Module
public class RegisterModule {

    private RegisterActivity activity;

    public RegisterModule(RegisterActivity activity) {
        this.activity = activity;
    }

    @Provides
    @RegisterScope
    SmsMessageReceiver provideReceiver() {
        return new SmsMessageReceiver(activity);
    }

    @Provides
    @RegisterScope
    RegisterModel provideModel() { return new RegisterModel(); }

    @Provides
    @RegisterScope
    RegisterView provideView() { return new RegisterView(activity, R.layout.register); }

    @Provides
    @RegisterScope
    RegisterPresenter providePresenter(RegisterModel model, RegisterView view, SmsMessageReceiver receiver) { return new RegisterPresenter(view, model, new RegisterCallback(view, model), receiver); }
}
