package kr.co.mergepoint.mergeclient.scene.register.view;

import android.app.DatePickerDialog;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import io.fabric.sdk.android.services.network.HttpRequest;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.application.common.CustomDatePickerDialog;
import kr.co.mergepoint.mergeclient.application.common.SmsMessageReceiver;
import kr.co.mergepoint.mergeclient.application.common.script.PostScriptInterface;
import kr.co.mergepoint.mergeclient.application.common.script.PostScriptListener;
import kr.co.mergepoint.mergeclient.databinding.AlertMarketingBinding;
import kr.co.mergepoint.mergeclient.databinding.RegisterBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.register.RegisterActivity;
import kr.co.mergepoint.mergeclient.scene.register.presenter.RegisterPresenter;
import kr.co.mergepoint.mergeclient.scene.register.view.fragment.PostcodeView;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.internal.huc.OkHttpURLConnection;

import static kr.co.mergepoint.mergeclient.application.common.Properties.ACTIVE_USER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MEMBER_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.POSTCODE_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.REGISTER_POST_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UP_DOWN;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WITHDRAWAL_USER;

/**
 * Created by jgson on 2017. 6. 5..
 */

public class RegisterView extends BaseView<RegisterActivity, RegisterPresenter, RegisterBinding> {

    private AppCompatDialog currentAlert;
    private Member memberInfo;

    private EditText id;
    private EditText pw;
    private EditText pwConfirm;
    private EditText phone;
    private EditText phoneSubscribedNumber;
    private EditText name;
    private RadioGroup gender;
    private TextView birth;
    private EditText postCode;
    private EditText address;
    private EditText detailedAddress;
    private EditText email;
    private CheckBox agreeCheck;

    private boolean checkId;        // 아이디 중복 확인
    private boolean checkPw;        // 비밀번호 일치
    private boolean checkPhone;     // 핸드폰 인증

    private boolean isMarketing;     // 마켓팅 제공 동의
    private boolean isSms;          // 문자 수신 동의
    private boolean isMail;         // 메일 수신 동의

    public RegisterView(RegisterActivity activity, int layout) {
        super(activity, layout);

        id = binding.registerId;
        pw = binding.registerPw;
        pwConfirm = binding.registerPwConfirm;
        phone = binding.registerPhone;
        phoneSubscribedNumber = binding.registerNumberSubscribed;

        name = binding.registerName;
/*
        gender = binding.genderRadioGroup;
        birth = binding.userBirth;
        postCode = binding.postCode;
        address = binding.registerAddress;
        detailedAddress = binding.registerDetailedAddress;
        email = binding.additionRegisterEmail;
        */
        agreeCheck = binding.agreeCheck;
        isMarketing = true;
        isSms = true;
        isMail = true;
    }

    @Override
    protected Toolbar setToolbar() {
        return binding.toolbarBinding.basicToolbar;
    }

    public Map<String, Object> getMemberInfo() {
        memberInfo.setId(id.getText().toString());
        memberInfo.setEmailConfirm(checkId);
        memberInfo.setPassword(pw.getText().toString());
        memberInfo.setPhone(phone.getText().toString());
        memberInfo.setPhoneConfirm(checkPhone);

        memberInfo.setName(name.getText().toString());
           /*
        if (gender.getCheckedRadioButtonId() != -1)
            memberInfo.setGender(gender.getCheckedRadioButtonId() == R.id.male ? 1 : 2);

        if (!postCode.getText().toString().trim().isEmpty())
            memberInfo.setPostcode(postCode.getText().toString());
        if (!address.getText().toString().trim().isEmpty())
            memberInfo.setAddress01(address.getText().toString());
        if (!detailedAddress.getText().toString().trim().isEmpty())
            memberInfo.setAddress02(detailedAddress.getText().toString());
        if (!email.getText().toString().trim().isEmpty())
            memberInfo.setEmail(email.getText().toString());
        */
        memberInfo.setMarketingAgree(isMarketing);
        memberInfo.setSmsAgree(isSms);
        memberInfo.setEmailAgree(isMail);
        memberInfo.setPerInfoAgree(agreeCheck.isChecked());

        Map <String,Object> maps = memberInfo.getMemberMap();

        for( String keys : maps.keySet() ){


            MDEBUG.debug("params  : key = " + keys);
            MDEBUG.debug("params  : value = " + maps.get(keys));
        }

        MDEBUG.debug("ADID init :" + MergeApplication.getMergeApplication().ADID_KEY);
        maps.put("av",MergeApplication.getMergeApplication().ADID_KEY);

        return maps;
    }

    public void setMemberInfo() {
        Member member = activity.getIntent().getParcelableExtra(MEMBER_INFO);
        memberInfo = member != null ? member : new Member();
        checkId = memberInfo.isSocial();

        /* 이미 일반 회원으로 가입해서 멤버 테이블을 생성할 필요가 없을 때 create = false
        *  그렇기 때문에 소셜 로그인 시 create가 false일 때 비밀번호와 핸드폰 인증이 불필요! */
        if (!memberInfo.isCreate()) {
            checkPw = true;
            checkPhone = true;
        }

        binding.setMember(memberInfo);
    }

    public void showSearchPost(PostScriptListener listener) {
        openFragment(UP_DOWN, R.id.register_root, PostcodeView.newInstance(new PostScriptInterface(listener), POSTCODE_URL), REGISTER_POST_TAG);
    }

    public void setAddress(final String add01, final String add02, final String add03) {
        binding.getMember().setPostcode(add01);
        binding.getMember().setAddress01(add02);
        binding.getMember().setAddress02(add03);
        removeFragment(activity.getSupportFragmentManager().findFragmentByTag(REGISTER_POST_TAG));
    }

    public void setCertificationNum(String certificationNum) {
        showKeyboard();
        phoneSubscribedNumber.requestFocus();
        phoneSubscribedNumber.setText(certificationNum);
    }

    public void setupSmsReceiver(SmsMessageReceiver receiver) {
        activity.registerReceiver(receiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
    }

    public void removeSmsReceiver(SmsMessageReceiver receiver) {
        activity.unregisterReceiver(receiver);
    }

    public void successPhone(boolean isSuccess) {
        checkPhone = isSuccess;
        binding.numberSubscribedImg.setImageResource(isSuccess ? R.drawable.icon_registe_yes : R.drawable.icon_registe_no);
    }

    public void successId(boolean isSuccess) {
        checkId = isSuccess;
        binding.idSubscribed.setImageResource(isSuccess ? R.drawable.icon_registe_yes : R.drawable.icon_registe_no);
    }

    public void successPw() {
        checkPw = pw.getText().toString().equals(pwConfirm.getText().toString());
        binding.pwSubscribed.setImageResource(checkPw ? R.drawable.icon_registe_yes : R.drawable.icon_registe_no);
    }

    public boolean checkField() {
        if (id.getText().toString().trim().isEmpty()) {
            Snackbar.make(getContentView(), R.string.input_id, Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (!checkId) {
            Snackbar.make(getContentView(), R.string.check_id, Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (memberInfo.isCreate() && pw.getText().toString().trim().isEmpty()) {
            Snackbar.make(getContentView(), R.string.input_pw, Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (!checkPw) {
            Snackbar.make(getContentView(), R.string.check_pw, Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (memberInfo.isCreate() && phone.getText().toString().trim().isEmpty()) {
            Snackbar.make(getContentView(), R.string.input_phone, Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (!checkPhone) {
            Snackbar.make(getContentView(), R.string.check_phone, Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (name.getText().toString().trim().isEmpty()) {
            Snackbar.make(getContentView(), R.string.input_name, Snackbar.LENGTH_SHORT).show();
            return false;
        }
        /*else if (gender.getCheckedRadioButtonId() == NO_VALUE) {
            Snackbar.make(getContentView(), R.string.input_gender, Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (birth.getText().toString().trim().isEmpty()) {
            Snackbar.make(getContentView(), R.string.input_birth, Snackbar.LENGTH_SHORT).show();
            return false;
        } */
        else if (!agreeCheck.isChecked()) {
            Snackbar.make(getContentView(), R.string.check_accept_terms, Snackbar.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public boolean validPhoneNumber() {
        hideKeyboard();
        String phoneNum = phone.getText().toString().trim();
        if (phoneNum.isEmpty()) {
            Snackbar.make(getContentView(), R.string.input_phone, Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (!MergeApplication.getUtility().checkPhoneNumberFormat(phoneNum)) {
            Snackbar.make(getContentView(), R.string.valid_phone, Snackbar.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public String getID() {
        return id.getText().toString();
    }

    public String getPhoneNum() {
        return phone.getText().toString();
    }

    public String getPhoneSubscribedNumber() {
        return phoneSubscribedNumber.getText().toString();
    }

    public void setMarketing(boolean marketing) {
        isMarketing = marketing;
    }

    public void setSms(boolean sms) {
        isSms = sms;
    }

    public void setMail(boolean mail) {
        isMail = mail;
    }

    public void showCalendar(DatePickerDialog.OnDateSetListener setListener) {
        Calendar calendar = Calendar.getInstance();
        boolean emptyBirth = birth.getText().toString().isEmpty() && birth.getText().toString().length() == 0;
        ArrayList<String> birthArray = new ArrayList<>(Arrays.asList(birth.getText().toString().split("-")));
        int year = emptyBirth ? calendar.get(Calendar.YEAR) - 20 : Integer.parseInt(birthArray.get(0));
        int month = emptyBirth ? calendar.get(Calendar.MONTH) : Integer.parseInt(birthArray.get(1)) - 1;
        int day = emptyBirth ? calendar.get(Calendar.DAY_OF_MONTH) : Integer.parseInt(birthArray.get(2));
        CustomDatePickerDialog datePickerDialog = new CustomDatePickerDialog(activity, R.style.RegisterBirthSpinner, setListener, year, month, day);
        datePickerDialog.show();
    }

    public void setBirth(int year, int month, int day) {
        birth.setText(activity.getString(R.string.date, year, month + 1, day));
        memberInfo.setBirthday(new ArrayList<>(Arrays.asList(year, month + 1, day)));
    }

    public void openMain(Member member) {
        MergeApplication.getInitialInfo().setMember(member);
        MergeApplication.getInitialInfo().setSignin(true);
        openClearTaskActivity(MainActivity.class);
    }

    public void showMarketingAgreeAlert(View.OnClickListener onClickListener, CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        AlertMarketingBinding alertMarketingBinding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.alert_marketing, null, false);
        alertMarketingBinding.setOnClick(onClickListener);
        alertMarketingBinding.setOnCheck(onCheckedChangeListener);
        alertMarketingBinding.setIsMarketing(isMarketing);
        alertMarketingBinding.setIsSMS(isSms);
        alertMarketingBinding.setIsMail(isMail);
        currentAlert = new AppCompatDialog(activity, R.style.AlertDialog);
        currentAlert.setContentView(alertMarketingBinding.getRoot());
        currentAlert.show();
    }

    public void closeMarketingAlert() {
        if (currentAlert != null)
            currentAlert.dismiss();
    }

    public void userStateCheck(Member member, MergeDialog.OnDialogClick onDialogClick, MergeDialog.OnDialogClick cancelClick) {
        if (member.getState() == ACTIVE_USER) {
            new GoogleADIDTask().execute();

            /*활성화 유저*/
            openMain(member);

        } else if (member.getState() == WITHDRAWAL_USER) {
            /*탈퇴한 유저*/
            new MergeDialog.Builder(activity).setContent(R.string.withdrawal_user).setConfirmString("복구하기").setConfirmClick(onDialogClick).setCancelClick(cancelClick).build().show();
        } else {
            /*휴면 유저 - 정책이 만들어진 후 처리*/
        }
    }
    class GoogleADIDTask extends AsyncTask<Void,Void,String> {
        String ADID = "";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... strings) {
            try {
                ADID = AdvertisingIdClient.getAdvertisingIdInfo(activity.getApplicationContext()).getId();
                MDEBUG.debug("ADID : " + ADID);
            }catch (Exception e){
                MDEBUG.debug("error"  + e.toString());
            }

            requestWebServer("key", ADID, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    MDEBUG.debug("res"+response.message() + "/" +response.body().string());
                }
            });
            return ADID;
        }

        private OkHttpClient client = new OkHttpClient();
        /** 웹 서버로 요청을 한다. */
        public void requestWebServer(String parameter, String parameter2, Callback callback) {
            HttpUrl.Builder urlBuilder = HttpUrl.parse("http://api.i-screen.kr/api/ads_complete_run").newBuilder();
            urlBuilder.addQueryParameter("ai", parameter);
            urlBuilder.addQueryParameter("av", parameter2);
            String url = urlBuilder.build().toString();

            Request request = new Request.Builder()
                    .url(url)
                    .build();
            client.newCall(request).enqueue(callback);
        }

    }
}

