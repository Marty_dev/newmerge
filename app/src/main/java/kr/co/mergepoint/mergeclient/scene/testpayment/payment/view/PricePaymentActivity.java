package kr.co.mergepoint.mergeclient.scene.testpayment.payment.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

public class PricePaymentActivity extends BaseActivity implements TextWatcher,View.OnFocusChangeListener,TextView.OnEditorActionListener{

    @Override
    protected void onActivityClick(View view) {

    }

    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.price_toolbar)
    RelativeLayout priceToolbar;
    @BindView(R.id.price_edt)
    EditText priceEdt;
    @BindView(R.id.price_textdel)
    ImageButton priceTextdel;
    @BindView(R.id.price_rl)
    RelativeLayout priceRl;
    @BindView(R.id.price_p_man)
    TextView pricePMan;
    @BindView(R.id.price_p_ochun)
    TextView pricePOchun;
    @BindView(R.id.price_p_chun)
    TextView pricePChun;
    @BindView(R.id.price_confirm)
    TextView priceConfirm;
    @BindView(R.id.price_baseline)
    TextView priceBaseline;

    private int mMoney = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_payment);


        ButterKnife.bind(this);
        priceEdt.setOnFocusChangeListener(this);
        priceEdt.addTextChangedListener(this);
        priceEdt.setImeOptions(EditorInfo.IME_ACTION_DONE);
        priceEdt.setOnEditorActionListener(this);
    }
    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if ( i == EditorInfo.IME_ACTION_DONE){
            try{
                mMoney = Integer.parseInt(textView.getText().toString());
            }catch (Exception e){
                MDEBUG.debug("e" + e.toString());
            }
            textView.setText(MoneyForm(mMoney));
        }
        return false;
    }
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        MDEBUG.debug("edit before");
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        String msg = charSequence.toString();
        if (msg.isEmpty()) {
            priceBaseline.setBackground(getResources().getDrawable(R.drawable.edtbottom_gray));
            priceTextdel.setVisibility(View.GONE);
        } else {
            priceBaseline.setBackground(getResources().getDrawable(R.drawable.edtbottom_red));
            priceTextdel.setVisibility(View.VISIBLE);


        }
        MDEBUG.debug("edit ing");

    }

    @Override
    public void onFocusChange(View view, boolean b) {
        MDEBUG.debug("Focus is" + (b? " on" : " off"));
        if (mMoney > 0) {
            if (b) {
                priceTextdel.setVisibility(View.GONE);
                ((EditText) view).setText(MoneyForm(mMoney));
            } else {
                priceTextdel.setVisibility(View.VISIBLE);
                ((EditText) view).setText(MoneyForm(mMoney));
            }
        }else{
            ((EditText) view).setText(null);

        }

    }

    @Override
    public void afterTextChanged(Editable editable) {
        MDEBUG.debug("edit after");

    }


    @OnClick({R.id.back_arrow, R.id.price_textdel, R.id.price_p_man, R.id.price_p_ochun, R.id.price_p_chun, R.id.price_confirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.price_textdel:
                mMoney = 0;
                priceEdt.setText(null);
                break;
            case R.id.price_p_man:
                mMoney += 5000;
            case R.id.price_p_ochun:
                mMoney += 4000;
            case R.id.price_p_chun:
                mMoney += 1000;
                priceEdt.setText(MoneyForm(mMoney));
                break;
            case R.id.back_arrow:
                finish();
                break;
            case R.id.price_confirm:
                if (getMoneyfromEdit() <= 0 || mMoney <= 0 ){
                    Snackbar.make(findViewById(android.R.id.content),"금액을 입력해주세요",Snackbar.LENGTH_SHORT).show();
                    return;
                }


                Intent inte = new Intent();
                inte.putExtra("price",mMoney);
                this.setResult(RESULT_OK, inte);
                this.finish();
                break;
        }
    }

    int getMoneyfromEdit(){
        String text = priceEdt.getText().toString();
        text = text.replace("," , "");
        int money;
        try{
            money = Integer.parseInt(text);
            mMoney = money;

        }catch (Exception e){
            money = 0;
        }
        return money;
    }



}
