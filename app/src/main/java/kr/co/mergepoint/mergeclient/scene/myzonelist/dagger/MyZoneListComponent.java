package kr.co.mergepoint.mergeclient.scene.myzonelist.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.scene.myzonelist.MyZoneListActivity;

/**
 * Created by jgson on 2017. 6. 5..
 */

@MyZoneListScope
@Component(modules = MyZoneListModule.class)
public interface MyZoneListComponent {
    void inject(MyZoneListActivity activity);
}
