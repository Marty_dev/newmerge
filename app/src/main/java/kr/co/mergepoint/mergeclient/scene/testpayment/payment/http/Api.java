package kr.co.mergepoint.mergeclient.scene.testpayment.payment.http;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.basket.AddBasket;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.scene.data.history.UseMenuList;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.CrowdInfo;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.CrowdTicketbody;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.PaymentRequest;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.PreUseList;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.PreUseRequest;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ticketbody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * User: Marty
 * Date: 2018-07-18
 * Time: 오후 6:53
 * Description:
 */
public interface Api {

    /*POST*/
    @POST("payment/request/v1")
    Call<ResponseObject<PaymentRequest>> requestPayment(@Body AddBasket basket);

    @POST("payment/request/v1")
    @FormUrlEncoded
    Call<ResponseObject<PaymentRequest>> requestPayment(@Field("oid") String oid);

    @POST("payment/request/v1")
    @FormUrlEncoded
    Call<ResponseObject<PaymentRequest>> requestPayment(@Field("oid") String oid,@Field("shopRef")int shopRef,@Field("payPrice") int price);

    @POST("crowdPay/companyPoint/adapt/v1")
    Call<ResponseObject<PaymentRequest>> requestEnterAdapt(@Body ticketbody ticket );

    @POST("crowdPay/companyPoint/approveRequest/v1/{oid}")
    @FormUrlEncoded
    Call<ResponseObject<String>> requestcrowdApprove(@Field("oid") int oid);

    @POST("/payment/modify/shop/v1/{oid}")
    @FormUrlEncoded
    Call<ResponseObject<PaymentRequest>> requestPointAdapt(@Path("oid") long oid,@Field("point")long point , @Field("coupon")int coupon);

    @GET("crowdPay/member/list")
    Call<ResponseObject<ArrayList<CrowdMember>>> getCrowedMemberList(@Query("type") int type, @Query("keyword") String keyword,@Query("oid")int shopoid);

    @POST("/crowdPay/favorite/change/{oid}")
    Call<ResponseObject<Long>> requestfavorite(@Path("oid") int oid);

    @POST("/crowdPay/member/add/v1/{payShopRef}")
    @FormUrlEncoded
    Call<ResponseObject<CrowdInfo>> requestAddCrowd(@Path("payShopRef") long oid, @Field("oids")String point);

    @POST("/crowdPay/member/delete/v1/{crowdPayRef}")
    Call<ResponseObject<CrowdInfo>> requestdeleteCrowdMember(@Path("crowdPayRef") long crowdPayRef);
    @POST("/crowdPay/member/adapt/v1")
    Call<ResponseObject<CrowdInfo>> requestCrowdPoints(@Body CrowdTicketbody body);

    @POST("/crowdPay/companyPoint/approveRequest/v1/{oid}")
    Call<ResponseObject<ArrayList<CrowdPay>>> requestCrowdApprove(@Path("oid") long payShopoid);
    @POST("/payment/read/v1/{oid}")
    Call<ResponseObject<PaymentRequest>> requestReadPayment(@Path("oid") long payShopoid);

    @POST("/crowdPay/companyPoint/retry/v1/{crowdPayRef}")
    Call<ResponseObject<ArrayList<CrowdPay>>> requestReApproval(@Path("crowdPayRef") long crowdPayRef);

    @POST("/crowdPay/payment/delete/v1/{crowdPayRef}")
    Call<ResponseObject<PaymentRequest>> requestdeleteCrowdMemberv2(@Path("crowdPayRef") long crowdPayRef);

    @POST("/payment/useRequest/v1/{paymentRef}")
    Call<ResponseObject<ArrayList<PreUseRequest>>> requestpaymentRefv2(@Path("paymentRef") long paymentRef);

    @POST("/payment/useRequest/{type}")
    Call<ResponseObject<UsagePayment>> requestUseMenus(@Path("type") long type , @Body PreUseList preUseRequest);




}



