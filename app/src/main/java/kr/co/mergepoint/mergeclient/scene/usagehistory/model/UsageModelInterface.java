package kr.co.mergepoint.mergeclient.scene.usagehistory.model;

import java.util.ArrayList;
import java.util.Map;

import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.scene.data.history.UseMenu;
import kr.co.mergepoint.mergeclient.scene.data.main.UsageFilter;
import retrofit2.Callback;

/**
 * Created by jgson on 2017. 12. 13..
 */

public interface UsageModelInterface {
    void getHistory(Callback<ResponseObject<ArrayList<UsagePayment>>> callback, UsageFilter filter);

    void getUsedHistory(Callback<ResponseObject<ArrayList<UsagePayment>>> callback, UsageFilter filter);

    void payCancel(Callback<ResponseObject<Integer>> callback, int oid);

    void pointRefund(Callback<ResponseObject<Integer>> callback, int oid);

    void sendUsageMenu(Callback<ResponseObject<UsagePayment>> callback, ArrayList<UseMenu> useMenus);

    void approveMenu(Callback<ResponseObject<Integer>> callback, Map<String, Object> info);

    void rejectMenu(Callback<ResponseObject<Integer>> callback, int rejectOid);
}
