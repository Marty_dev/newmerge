package kr.co.mergepoint.mergeclient.scene.testpayment.payment.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Marty
 * Date: 2018-07-22
 * Time: 오후 9:06
 * Description:
 */
public class Attendee {

    @SerializedName("crowdPayRef")
    public long crowdPayRef;
    @SerializedName("memberRef")
    public long memberRef;
    @SerializedName("memberName")
    public String memberName;
    @SerializedName("companyPoints")
    public long companyPoints;
    @SerializedName("usePoints")
    public long usePoints;
    @SerializedName("ticketRef")
    public long ticketRef;
    @SerializedName("ticketName")
    public String ticketName;
    @SerializedName("ticketUseCount")
    public int ticketUseCount;
    @SerializedName("ticketDescription")
    public String ticketDescription;
    @SerializedName("tickets")
    public ArrayList<ShopTicket> tickets;
}
