package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.util.AttributeSet;

import static kr.co.mergepoint.mergeclient.application.common.Properties.COLLAPSE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.EXPAND;
import static kr.co.mergepoint.mergeclient.application.common.Properties.IDLE;

/**
 * Created by jgson on 2017. 7. 13..
 */

public class CustomAppBarLayout extends AppBarLayout implements AppBarLayout.OnOffsetChangedListener {

    private int state;

    @Properties.AppBarState
    public int getState() {
        return state;
    }

    public void setState(@Properties.AppBarState int state) {
        this.state = state;
    }

    public CustomAppBarLayout(Context context) {
        super(context);
        init();
    }

    public CustomAppBarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        addOnOffsetChangedListener(this);
        setState(EXPAND);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset == 0) {
            setState(EXPAND);
        } else if (Math.abs(verticalOffset)-appBarLayout.getTotalScrollRange() == 0) {
            setState(COLLAPSE);
        } else {
            setState(IDLE);
        }
    }
}
