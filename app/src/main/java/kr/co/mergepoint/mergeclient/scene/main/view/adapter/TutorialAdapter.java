package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import kr.co.mergepoint.mergeclient.R;

/**
 * Created by jgson on 2018. 2. 7..
 */

public class TutorialAdapter extends PagerAdapter {

    private Integer[] tutorialImages;

    public TutorialAdapter() {
        tutorialImages = new Integer[] { R.drawable.tutorial_1, R.drawable.tutorial_2, R.drawable.tutorial_3, R.drawable.tutorial_4 };
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(container.getContext());
        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        imageView.setImageResource(tutorialImages[position]);
        int color = position == 3 ? R.color.gray_31 : R.color.gray_30;
        imageView.setBackgroundColor(ContextCompat.getColor(container.getContext(), color));
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return tutorialImages.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
