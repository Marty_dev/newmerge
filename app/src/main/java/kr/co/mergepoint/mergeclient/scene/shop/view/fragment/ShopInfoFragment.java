package kr.co.mergepoint.mergeclient.scene.shop.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.WideBannerAdapter;
import kr.co.mergepoint.mergeclient.databinding.ShopDetailInfoBinding;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.shop.view.ShopMapView;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.ShopConvenienceInfoAdapter;

/**
 * Created by 1017sjg on 2017. 6. 23..
 */

public class ShopInfoFragment extends BaseFragment implements ViewPager.OnPageChangeListener {

    private ShopDetail detail;
    private ShopDetailInfoBinding shopDetailInfoBinding;

    public int getPreExperienceIndex() {
        return shopDetailInfoBinding.shopPreExperiencePager.getCurrentItem();
    }

    public static ShopInfoFragment newInstance() {
        Bundle args = new Bundle();
        ShopInfoFragment fragment = new ShopInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        shopDetailInfoBinding = DataBindingUtil.inflate(inflater, R.layout.shop_detail_info, container, false);
        if (detail != null)
            updateInfo();
        return shopDetailInfoBinding.getRoot();
    }

    public void setShopDetail(ShopDetail detail) {
        this.detail = detail;
        updateInfo();
    }

    private void updateInfo() {
        shopDetailInfoBinding.setShopDetail(detail);
        shopDetailInfoBinding.setOnClick(getRootActivity());
        shopDetailInfoBinding.setPreExperienceAdapter(new WideBannerAdapter(R.layout.banner_item, detail.preExImage, R.drawable.img_pre_ex, getRootActivity()));
        shopDetailInfoBinding.setInfoAdapter(new ShopConvenienceInfoAdapter(detail.additional));
        shopDetailInfoBinding.setPreExperienceListener(this);

        if (detail.preExImage != null) {
            String indicatorString = getResources().getString(R.string.pre_experience_indicator);
            shopDetailInfoBinding.preExperienceTextIndicator.setText(String.format(indicatorString, 1, detail.preExImage.size()));
        }
        addMapView();
    }

    private void addMapView() {
        ShopMapView mapView = new ShopMapView(getRootActivity());
        mapView.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        MapPOIItem customMarker = new MapPOIItem();
        MapPoint mapPoint = MapPoint.mapPointWithGeoCoord(detail.latitude, detail.longitude);
        customMarker.setItemName(detail.shopName);
        customMarker.setMapPoint(mapPoint);
        customMarker.setMarkerType(MapPOIItem.MarkerType.CustomImage);
        customMarker.setCustomImageResourceId(R.drawable.obj_position);
        customMarker.setCustomImageAutoscale(false);
        customMarker.setCustomImageAnchor(0.5f, 1.0f);
        mapView.addPOIItem(customMarker);
        mapView.setMapCenterPoint(mapPoint, false);
        shopDetailInfoBinding.shopMapLayout.addView(mapView,0);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (shopDetailInfoBinding.shopMapLayout.getChildCount() == 1 && detail != null)
            addMapView();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (shopDetailInfoBinding != null)
            shopDetailInfoBinding.shopMapLayout.removeViewAt(0);
    }

    @Override
    public void onPageSelected(int position) {
        String indicatorString = getResources().getString(R.string.pre_experience_indicator);
        shopDetailInfoBinding.preExperienceTextIndicator.postDelayed(() -> shopDetailInfoBinding.preExperienceTextIndicator.setText(String.format(indicatorString, position + 1, detail.preExImage.size())), 200);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageScrollStateChanged(int state) {}
}
