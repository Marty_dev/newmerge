package kr.co.mergepoint.mergeclient.scene.data.payment;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.BR;
import kr.co.mergepoint.mergeclient.scene.data.cardregiste.CardInfo;

    /**
     * Created by 1017sjg on 2017. 7. 5..
     */

    public class PayConfirm extends BaseObservable {

        @SerializedName("oid")
        public int oid;

        @SerializedName("memberRef")
        public int memberRef;

        @SerializedName("menuNum")
        public int menuNum;

        @SerializedName("menuOrderNum")
        public int menuOrderNum;

        @SerializedName("orderNum")
        public String orderNum;

        @SerializedName("pointRate")
        public int pointRate;

        @SerializedName("availablePoints")
        public int availablePoints;

        @SerializedName("companyPoints")
        public int companyPoints;

        @SerializedName("returnPoint")
        public int returnPoint;

        @SerializedName("orderPrice")
        public int orderPrice;

        @SerializedName("pointPrice")
        private int pointPrice;

        @SerializedName("mergeCouponRef")
        public int mergeCouponRef;

        @SerializedName("couponPrice")
        private int couponPrice;

        @SerializedName("payPrice")
        private int payPrice;

        @SerializedName("plannedPoints")
        private int plannedPoints;

        @SerializedName("approvalNum")
        public String approvalNum;

        @SerializedName("cardCode")
        public String cardCode;

        @SerializedName("settlementState")
        public int settlementState;

        @SerializedName("useState")
        public int useState;

        @SerializedName("payDateTime")
        public ArrayList<Integer> payDateTime;

        @SerializedName("expDate")
        public ArrayList<Integer> expDate;

        @SerializedName("cartCoupon")
        public ArrayList<Coupon> cartCoupon;

        @SerializedName("mergeCoupon")
        public ArrayList<Coupon> mergeCoupon;

        @SerializedName("shopPayment")
        public ArrayList<ShopPayment> shopPayment;

        @SerializedName("billCard")
        public ArrayList<CardInfo> billCard;

    @Bindable
    public int getPointPrice() {
        return pointPrice;
    }

    public void setPointPrice(int pointPrice) {
        this.pointPrice = pointPrice;
        notifyPropertyChanged(BR.pointPrice);
    }

    @Bindable
    public int getCompanyPoints() {
        return companyPoints;
    }

    public void setCompanyPoints(int companyPoints) {
        this.companyPoints = companyPoints;
        notifyPropertyChanged(BR.companyPoints);
    }

    @Bindable
    public int getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(int couponPrice) {
        this.couponPrice = couponPrice;
        notifyPropertyChanged(BR.couponPrice);
    }

    @Bindable
    public int getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(int payPrice) {
        this.payPrice = payPrice;
        notifyPropertyChanged(BR.payPrice);
    }

    @Bindable
    public int getPlannedPoints() {
        return plannedPoints;
    }

    public void setPlannedPoints(int plannedPoints) {
        this.plannedPoints = plannedPoints;
        notifyPropertyChanged(BR.plannedPoints);
    }

    public boolean isEnterPriseShop(){
        boolean isEnterShop = false;
        for (ShopPayment pays : this.shopPayment){
            if(pays.shopType != 1)
                isEnterShop = true;
        }
        return isEnterShop;
    }
}
