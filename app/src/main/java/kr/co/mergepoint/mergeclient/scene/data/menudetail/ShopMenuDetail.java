package kr.co.mergepoint.mergeclient.scene.data.menudetail;

import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.android.databinding.library.baseAdapters.BR;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.menu.ShopChildInfo;

/**
 * Created by 1017sjg on 2017. 6. 24..
 */

public class ShopMenuDetail extends ShopChildInfo {

    @SerializedName(value = "menuCount", alternate = {"paymentCount"})
    public int count;

    @SerializedName("serveType")
    public int serveType;

    @SerializedName("usedCount")
    public int usedCount;

    @SerializedName("settlementState")
    public int settlementState;

    @SerializedName("useState")
    public int useState;

    @SerializedName("salePrice")
    public int salePrice;

    @SerializedName("payPrice")
    public int payPrice;

    @SerializedName("serveDate")
    public ArrayList<Integer> serveDate;

    @SerializedName("payDateTime")
    public ArrayList<Integer> payDateTime;

    @SerializedName("expDate")
    public ArrayList<Integer> expDate;

    @SerializedName(value = "optionMenu", alternate = {"optionMenus"})
    public ArrayList<ShopOptionMenu> optionMenu;

    public boolean isCheck;
    private int displayCount;

    public ShopMenuDetail() {
        optionMenu = new ArrayList<>();
        isCheck = true;
        displayCount = 1;
    }

    @Bindable
    public int getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(int payPrice) {
        this.payPrice = payPrice;
        notifyPropertyChanged(BR.payPrice);
    }

    @Bindable
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
        notifyPropertyChanged(BR.count);
    }

    public ArrayList<Integer> getPayDateTime() {
        return payDateTime;
    }

    public ArrayList<Integer> getExpDate() {
        return expDate;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public int getDisplayCount() {
        return count - usedCount;
    }

    public void setDisplayCount(int displayCount) {
        this.displayCount = displayCount;
    }

    private ShopMenuDetail(Parcel in) {
        count = in.readInt();
        serveType = in.readInt();
        usedCount = in.readInt();
        settlementState = in.readInt();
        useState = in.readInt();
        salePrice = in.readInt();
        payPrice = in.readInt();
        payDateTime = new ArrayList<>();
        in.readList(payDateTime, Integer.class.getClassLoader());
        expDate = new ArrayList<>();
        in.readList(expDate, Integer.class.getClassLoader());
        optionMenu = new ArrayList<>();
        in.readTypedList(optionMenu, ShopOptionMenu.CREATOR);
        isCheck = in.readInt() == 1;
        displayCount = in.readInt();
    }

    public static final Parcelable.Creator<ShopMenuDetail> CREATOR = new Parcelable.Creator<ShopMenuDetail>() {
        @Override
        public ShopMenuDetail createFromParcel(Parcel in) {
            return new ShopMenuDetail(in);
        }

        @Override
        public ShopMenuDetail[] newArray(int size) {
            return new ShopMenuDetail[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(count);
        dest.writeInt(serveType);
        dest.writeInt(usedCount);
        dest.writeInt(settlementState);
        dest.writeInt(useState);
        dest.writeInt(salePrice);
        dest.writeInt(payPrice);
        dest.writeList(expDate);
        dest.writeList(payDateTime);
        dest.writeTypedList(optionMenu);
        dest.writeInt(isCheck ? 1 : 0);
        dest.writeInt(displayCount);
    }
}
