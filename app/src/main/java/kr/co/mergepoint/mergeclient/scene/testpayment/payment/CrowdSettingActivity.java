package kr.co.mergepoint.mergeclient.scene.testpayment.payment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.CrowdApprovalListener;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.SearchCrowdPayUserListener;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.adapter.CrowdSettingListAdapter;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.Attendee;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.CrowdInfo;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.http.Api;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.http.Payment_v2_Callbacks;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.view.CrowdApprovalFragmentV2;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.view.SearchCrowdPayUserFragmentV2;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.CROWED_APPROVAL_FRAGMENTV2_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SEARCH_CROWD_PAY_FRAGMENTV2_TAG;

public class CrowdSettingActivity extends BaseActivity implements CrowdApprovalListener,SearchCrowdPayUserListener{

    public Api apis;
    public CrowdInfo crowdInfo;
    public int shopref;
    CrowdSettingActivity activity = this;
    Context mCon = this.getBaseContext();
    @BindView(R.id.back_arrow)
    public ImageView backArrow;
    @BindView(R.id.crowd_point_text)
    public TextView crowdPointText;
    @BindView(R.id.crowd_list)
    public RecyclerView crowdList;
    @BindView(R.id.crowd_point_tv)
    public TextView crowdPointTv;


    CrowdSettingListAdapter adapter;
    @BindView(R.id.crowd_confirmbtn)
    TextView crowdConfirmbtn;
    @BindView(R.id.add_crowd_member)
    TextView addCrowdMember;

    public int oid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crowd_setting);
        ButterKnife.bind(this);
        oid = getIntent().getIntExtra("Shopoid", -1);

        addCrowdMember.setOnClickListener(view->{
            this.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.crowd_content, SearchCrowdPayUserFragmentV2.newInstance(this, this), SEARCH_CROWD_PAY_FRAGMENTV2_TAG)
                    .addToBackStack(SEARCH_CROWD_PAY_FRAGMENTV2_TAG).commit();
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        init();

    }
    @Override
    public void updateCrowedMemberTable(int type, String searchString) {
        callbacks.RequestCrowdList(type, searchString, oid);
    }

    public void init() {

        apis = MergeApplication.getMergeAppComponent().getRetrofit().create(Api.class);
        if (oid == -1) {
            new MergeDialog.Builder(this).setContent(R.string.retry).setCancelBtn(false).build().show();
            return;
        } else {

            int shopoid = getIntent().getIntExtra("Shopoid", -1);
            adapter = new CrowdSettingListAdapter(R.layout.crowd_point_item, this, shopoid);
            crowdList.setAdapter(adapter);
            apis.requestAddCrowd(shopoid, null).enqueue(new Callback<ResponseObject<CrowdInfo>>() {
                @Override
                public void onResponse(Call<ResponseObject<CrowdInfo>> call, Response<ResponseObject<CrowdInfo>> response) {
                    MDEBUG.debug("Res : \n" + response.toString());
                    if (response.body().isFailed()) {
                        new MergeDialog.Builder(activity).setContent(response.body().getMessage()).setCancelBtn(false).build().show();
                        return;
                    }
                    if (response.body().getObject().attendees == null || response.body().getObject().attendees.size() == 0) {
                        finish();
                        return;
                    }
                    MergeApplication.getMergeApplication().hideLoading(activity);
                    crowdInfo = response.body().getObject();
                    crowdPointTv.setText(String.format("%,d", crowdInfo.remainPayPrice) + "원");
                    initList(crowdInfo.attendees);
                }
                @Override
                public void onFailure(Call<ResponseObject<CrowdInfo>> call, Throwable t) {
                    MDEBUG.debug("Failed :" + t.toString());
                    MergeApplication.getMergeApplication().hideLoading(activity);
                    new MergeDialog.Builder(activity).setContent(R.string.retry).setCancelBtn(false).build().show();
                }
            });


        }
    }


    public void initList(ArrayList<Attendee> attendees) {

        if (attendees == null || attendees.size() == 0) {
            finish();
            return;
        }
        adapter.arrayList = attendees;
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityClick(View view) {

    }

    @OnClick({R.id.back_arrow, R.id.crowd_confirmbtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_arrow:
                finish();
                break;
            case R.id.crowd_confirmbtn:
                MergeApplication.getMergeApplication().showLoading(this);

                // 금액설정안하면?
                apis.requestCrowdApprove(crowdInfo.payShopRef).enqueue(new Callback<ResponseObject<ArrayList<CrowdPay>>>() {
                    @Override
                    public void onResponse(Call<ResponseObject<ArrayList<CrowdPay>>> call, Response<ResponseObject<ArrayList<CrowdPay>>> response) {
                        MergeApplication.getMergeApplication().hideLoading(activity);
                        MDEBUG.debug("RES:: SUCCESS");


                        if (response.body().isFailed()) {

                        } else {
                            activity.getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.crowd_content, CrowdApprovalFragmentV2.newInstance(activity, response.body().getObject()), CROWED_APPROVAL_FRAGMENTV2_TAG)
                                    .addToBackStack(CROWED_APPROVAL_FRAGMENTV2_TAG).commit();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseObject<ArrayList<CrowdPay>>> call, Throwable t) {
                        MergeApplication.getMergeApplication().hideLoading(activity);
                        MDEBUG.debug("RES:: failed + " + t.toString());
                    }
                });
                break;
        }
    }

    @Override
    public void endApproval() {
        MDEBUG.debug("When Call??");
        finish();
    }
}
