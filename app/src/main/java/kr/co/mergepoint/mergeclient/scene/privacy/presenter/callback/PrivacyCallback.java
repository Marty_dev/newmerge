package kr.co.mergepoint.mergeclient.scene.privacy.presenter.callback;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseCallback;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.application.common.ResultPageFragment;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.main.Company;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.privacy.view.adapter.SearchEnterpriseAdapter;
import kr.co.mergepoint.mergeclient.scene.privacy.model.PrivacyModel;
import kr.co.mergepoint.mergeclient.scene.privacy.view.PrivacyView;
import kr.co.mergepoint.mergeclient.scene.privacy.view.fragment.SearchEnterpriseFragment;

import static kr.co.mergepoint.mergeclient.application.common.Properties.RESULT_PAGE_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SEARCH_ENTERPRISE_FRAGMENT_TAG;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class PrivacyCallback extends BaseCallback<PrivacyView, PrivacyModel> implements PrivacyCallbackInterface {

    public PrivacyCallback(PrivacyView baseView, PrivacyModel baseModel) {
        super(baseView, baseModel);
    }

    @Override
    public MergeCallback<ResponseObject<Member>> getUserPrivacy() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Member> memberResponseObject = response.body();
            if (response.isSuccessful() && memberResponseObject != null) {
                if (memberResponseObject.isFailed()) {
                    baseView.showAlert(memberResponseObject.getMessage());
                } else {
                    Member member = memberResponseObject.getObject();
                    if (member != null) {
                        baseView.openSimplePrivacy(member);
                        baseModel.setMember(member);
                    }
                }
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Member>> returnModifyInfo() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Member> memberResponseObject = response.body();
            if (response.isSuccessful() && memberResponseObject != null) {
                if (memberResponseObject.isFailed()) {
                    baseView.showAlert(memberResponseObject.getMessage());
                } else {
                    Member member = memberResponseObject.getObject();
                    if (member != null) {
                        MergeApplication.getInitialInfo().setMember(member);
                        new MergeDialog.Builder(baseView.activity)
                                .setContent(R.string.information_changed_successfully)
                                .setCancelBtn(false).setConfirmString(R.string.move_main)
                                .setConfirmClick(() -> baseView.openClearTaskActivity(MainActivity.class)).build().show();
                    } else {
                        baseView.showAlert(R.string.retry);
                    }
                }
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<Company>>> getSearchEnterpriseListCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<Company>> responseObject = response.body();
            SearchEnterpriseFragment enterpriseFragment = (SearchEnterpriseFragment) baseView.findFragmentByTag(SEARCH_ENTERPRISE_FRAGMENT_TAG);
            if (enterpriseFragment != null && responseObject != null) {
                enterpriseFragment.setAdapter(new SearchEnterpriseAdapter(responseObject.getObject()));
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Boolean>> registerEnterpriseListCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Boolean> responseObject = response.body();
            if (responseObject != null) {
                if (responseObject.getObject()) {
                    baseView.openFragment(RIGHT_LEFT,
                            R.id.confirm_password_layout,
                            ResultPageFragment.newInstance(
                                    true,
                                    "기업회원 가입하기",
                                    "승인요청 완료!",
                                    "해당 기업에 승인요청을 완료했습니다.\n" + "기업 관리자가 확인하기 전에는 승인 대기 상태이며,\n" + "나의 정보에서 승인여부를 확인하실 수 있습니다.",
                                    "확인"),
                            RESULT_PAGE_FRAGMENT_TAG);
                } else {
                    new MergeDialog.Builder(baseView.activity).setContent(responseObject.getMessage()).setCancelBtn(false).build().show();
                }
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }
}
