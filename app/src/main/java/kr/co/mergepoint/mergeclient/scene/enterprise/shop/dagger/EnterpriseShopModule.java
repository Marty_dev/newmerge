package kr.co.mergepoint.mergeclient.scene.enterprise.shop.dagger;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.EnterpriseShopActivity;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.model.EnterpriseShopModel;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.presenter.EnterpriseShopPresenter;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.presenter.callback.EnterpriseShopCallback;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.EnterpriseShopView;

/**
 * Created by jgson on 2017. 6. 5..
 */

@Module
public class EnterpriseShopModule {

    private EnterpriseShopActivity activity;

    public EnterpriseShopModule(EnterpriseShopActivity activity) {
        this.activity = activity;
    }

    @Provides
    @EnterpriseShopScope
    EnterpriseShopModel provideModel() { return new EnterpriseShopModel(); }

    @Provides
    @EnterpriseShopScope
    EnterpriseShopView provideView() { return new EnterpriseShopView(activity, R.layout.enterprise_shop_detail); }

    @Provides
    @EnterpriseShopScope
    EnterpriseShopPresenter providePresenter(EnterpriseShopView view, EnterpriseShopModel model) {
        return new EnterpriseShopPresenter(view, model, new EnterpriseShopCallback(view, model));
    }
}
