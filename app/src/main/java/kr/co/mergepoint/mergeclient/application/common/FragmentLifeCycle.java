package kr.co.mergepoint.mergeclient.application.common;

/**
 * Created by 1017sjg on 2017. 10. 25..
 */

public interface FragmentLifeCycle {
    void onFragmentDestroy();
}
