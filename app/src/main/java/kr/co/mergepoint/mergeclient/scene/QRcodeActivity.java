package kr.co.mergepoint.mergeclient.scene;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.util.ArrayList;
import java.util.List;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.databinding.QrCameraBinding;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by 1017sjg on 2017. 7. 24..
 */

public class QRcodeActivity extends BaseActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView scannerView;
    private List<BarcodeFormat> formats;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        QrCameraBinding cameraBinding = DataBindingUtil.setContentView(this, R.layout.qr_camera);
        cameraBinding.toolbarBinding.setTitle(getString(R.string.qr_code));
        cameraBinding.toolbarBinding.setOnClick(this);

        scannerView = cameraBinding.scanner;
        formats = new ArrayList<>();
        formats.add(BarcodeFormat.QR_CODE);
    }

    @Override
    public void handleResult(Result result) {
        Intent resultIntent = new Intent();
        Uri uri = Uri.parse(result.getText());
        for (String queryName: uri.getQueryParameterNames())
            resultIntent.putExtra(queryName, uri.getQueryParameter(queryName));
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @Override
    protected void onActivityClick(View view) {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.setAutoFocus(true);
        scannerView.setFormats(formats);
        scannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }
}
