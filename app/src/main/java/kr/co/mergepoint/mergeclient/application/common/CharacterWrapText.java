package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextPaint;
import android.util.AttributeSet;

/**
 * Created by 1017sjg on 2017. 7. 12..
 */

public class CharacterWrapText extends AppCompatTextView {

    public CharacterWrapText(Context context) {
        super(context);
    }

    public CharacterWrapText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CharacterWrapText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(shrinkWithWordUnit(text.toString()), type);
    }

    public String shrinkWithWordUnit(String s) {
        if (!getText().toString().isEmpty()) {
            Paint mStaticMasurePaint = new TextPaint();

            mStaticMasurePaint.setTextSize(getTextSize());
            mStaticMasurePaint.setTypeface(getTypeface());

//        String[] tokens = s.replace("\n", "").split(" ");
            String[] tokens = s.split(" ");

            StringBuilder newStr = new StringBuilder();

            int currLineWidth = 0;

            for (String token : tokens) {

                if (currLineWidth + mStaticMasurePaint.measureText(" " + token) > getWidth()) {

                    newStr.append("\n").append(token);

                    currLineWidth = (int) mStaticMasurePaint.measureText(token);

                } else {

                    if (newStr.toString().equals("")) {

                        newStr = new StringBuilder(token);

                        currLineWidth = (int) mStaticMasurePaint.measureText(token);

                    } else {

                        newStr.append(" ").append(token);

                        currLineWidth += mStaticMasurePaint.measureText(" " + token);

                    }

                }

            }

            return newStr.toString();
        }

        return s;
    }
}
