package kr.co.mergepoint.mergeclient.scene.testpayment.payment;

import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopCoupon;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopPayment;

/**
 * User: Marty
 * Date: 2018-07-19
 * Time: 오후 4:47
 * Description:
 */
public interface couponListener {
    void onCouponChoose(ShopCoupon coupon);
}
