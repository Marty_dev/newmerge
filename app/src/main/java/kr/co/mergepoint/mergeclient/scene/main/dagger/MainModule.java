package kr.co.mergepoint.mergeclient.scene.main.dagger;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocation;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.main.model.MainModel;
import kr.co.mergepoint.mergeclient.scene.main.presenter.MainPresenter;
import kr.co.mergepoint.mergeclient.scene.main.presenter.callback.MainCallback;
import kr.co.mergepoint.mergeclient.scene.main.view.MainView;

import static kr.co.mergepoint.mergeclient.application.common.Properties.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UPDATE_INTERVAL_IN_MILLISECONDS;

/**
 * Created by jgson on 2017. 5. 31..
 */

@Module
public class MainModule {

    private MainActivity activity;

    public MainModule(MainActivity activity) {
        this.activity = activity;
    }

    @Provides
    @MainScope
    FusedLocationProviderClient provideGoogleApiClientBuilder() {
        return LocationServices.getFusedLocationProviderClient(activity);
    }

    @Provides
    @MainScope
    LocationRequest provideLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        return locationRequest;
    }

    @Provides
    @MainScope
    UserLocation provideUserLocation(FusedLocationProviderClient providerClient, LocationRequest locationRequest) {
        return new UserLocation(providerClient, activity, locationRequest, activity);
    }

    @Provides
    @MainScope
    MainModel provideModel() { return new MainModel(); }

    @Provides
    @MainScope
    MainView provideView() {
        return new MainView(activity, R.layout.activity_main);
    }

    @Provides
    @MainScope
    MainPresenter providePresenter(MainView view, MainModel model, UserLocation userLocation) {
        return new MainPresenter(view, model, new MainCallback(view, model), userLocation);
    }
}
