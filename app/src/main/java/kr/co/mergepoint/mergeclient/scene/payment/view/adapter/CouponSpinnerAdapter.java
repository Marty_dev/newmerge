package kr.co.mergepoint.mergeclient.scene.payment.view.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import kr.co.mergepoint.mergeclient.BR;
import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;

/**
 * Created by 1017sjg on 2017. 9. 4..
 */

public class CouponSpinnerAdapter extends BaseAdapter {

    public ArrayList<Coupon> coupons;

    private int normalLayout;
    private int dropdownLayout;

    public CouponSpinnerAdapter(int normalLayout, int dropdownLayout) {
        this.coupons = new ArrayList<>();
        this.normalLayout = normalLayout;
        this.dropdownLayout = dropdownLayout;
    }

    public void changeCoupons(ArrayList<Coupon> shopCoupon, ArrayList<Coupon> mergeCoupon) {
        coupons.clear();
        coupons.addAll(shopCoupon);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            coupons.addAll(mergeCoupon.stream().filter((Coupon coupon) -> !coupon.used).collect(Collectors.toList()));
        } else {
            ArrayList<Coupon> couponList = new ArrayList<>();
            for (Coupon coupon : mergeCoupon) {
                if (!coupon.used)
                    couponList.add(coupon);
            }
            coupons.addAll(couponList);
        }
        notifyDataSetChanged();
    }

    public void changeCoupons(ArrayList<Coupon> basketCoupon) {
        coupons.clear();
        coupons.addAll(basketCoupon);

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return coupons.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return position == 0 ? new Coupon(0) : coupons.get(position - 1);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewDataBinding binding;
        if (convertView == null) {
            View view = LayoutInflater.from(parent.getContext()).inflate(normalLayout, parent, false);
            binding = DataBindingUtil.bind(view);
            convertView = binding.getRoot();
            convertView.setTag(binding);
        } else {
            binding = (ViewDataBinding) convertView.getTag();
        }

        binding.setVariable(BR.spinnerTitle, position == 0 ? "쿠폰을 선택하세요." : ((Coupon) getItem(position)).couponName);
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewDataBinding binding;
        if (convertView == null) {
            View view = LayoutInflater.from(parent.getContext()).inflate(dropdownLayout, parent, false);
            binding = DataBindingUtil.bind(view);
            convertView = binding.getRoot();
            convertView.setTag(binding);
        } else {
            binding = (ViewDataBinding) convertView.getTag();
        }

        binding.setVariable(BR.spinnerTitle, position == 0 ? "쿠폰을 선택하세요." : ((Coupon) getItem(position)).couponName);
        return convertView;
    }
}
