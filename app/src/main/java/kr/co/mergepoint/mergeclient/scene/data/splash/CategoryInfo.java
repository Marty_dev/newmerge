package kr.co.mergepoint.mergeclient.scene.data.splash;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * Created by 1017sjg on 2017. 6. 14..
 */

public class CategoryInfo implements Parcelable{
    @SerializedName("categoryCode")
    public String code;

    @SerializedName("name")
    public String name;

    public CategoryInfo(String code) {
        this.code = code;
    }

    private CategoryInfo(Parcel in) {
        code = in.readString();
        name = in.readString();
    }

    public static final Creator<CategoryInfo> CREATOR = new Creator<CategoryInfo>() {
        @Override
        public CategoryInfo createFromParcel(Parcel in) {
            return new CategoryInfo(in);
        }

        @Override
        public CategoryInfo[] newArray(int size) {
            return new CategoryInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof CategoryInfo)) return false;

        CategoryInfo categoryInfo = (CategoryInfo) obj;
        return Objects.equals(code, categoryInfo.code);
    }
}
