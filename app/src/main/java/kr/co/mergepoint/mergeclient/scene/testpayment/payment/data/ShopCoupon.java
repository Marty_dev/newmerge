package kr.co.mergepoint.mergeclient.scene.testpayment.payment.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * User: Marty
 * Date: 2018-07-19
 * Time: 오전 10:45
 * Description:
 */
public class ShopCoupon {

    @Expose
    @SerializedName("couponMinPrice")
    public long couponMinPrice;
    @Expose
    @SerializedName("couponDescription")
    public String couponDescription;
    @Expose
    @SerializedName("couponName")
    public String couponName;
    @Expose
    @SerializedName("couponNum")
    public String couponNum;
    @Expose
    @SerializedName("oid")
    public int oid;
}
