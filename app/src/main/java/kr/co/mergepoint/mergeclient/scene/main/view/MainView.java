package kr.co.mergepoint.mergeclient.scene.main.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Button;

import com.facebook.CallbackManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.application.MergeEnterpriseNotice;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.application.common.ShareFragment;
import kr.co.mergepoint.mergeclient.application.common.SimpleWebFragment;
import kr.co.mergepoint.mergeclient.application.common.StreetButton;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocation;
import kr.co.mergepoint.mergeclient.databinding.ActivityMainBinding;
import kr.co.mergepoint.mergeclient.scene.QRcodeActivity;
import kr.co.mergepoint.mergeclient.scene.data.history.CompanyPointHistory;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.main.Banner;
import kr.co.mergepoint.mergeclient.scene.data.main.EventInfo;
import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPayItem;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.data.payment.Ticket;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.EnterpriseShopActivity;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.main.presenter.MainPresenter;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.AlarmFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.CombineImagesDetailFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.ContactusFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.CrowdApprovalRequestFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.CrowdApprovalRequestResultFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.CrowdPayHistoryDetailFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.CrowdPayHistoryFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.DelegationUserFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.EnterpriseNoticeFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.EnterprisePointFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.EventFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.FavoriteCrowedUserFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.FavoriteFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.GradeFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.MenuFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.MergesPickListFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.MyCouponFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.MyTicketFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.NoticeDetailFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.NoticeFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.PointFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.QuestionDirectFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.QuestionsFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.ResetPwFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.SettingsCrowedApprovalFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.SettingsFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.ThemeFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.TutorialFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.WithdrawalFragment;
import kr.co.mergepoint.mergeclient.scene.search.SearchActivity;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;

import static android.content.ContentValues.TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ALARM_LIST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.APPROVAL_DELEGATION_NAME;
import static kr.co.mergepoint.mergeclient.application.common.Properties.APPROVAL_DELEGATION_OID;
import static kr.co.mergepoint.mergeclient.application.common.Properties.APPROVAL_TYPE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ATTACH_PIC_RESULT_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.BANNER_DELAY;
import static kr.co.mergepoint.mergeclient.application.common.Properties.BANNER_PERIOD;
import static kr.co.mergepoint.mergeclient.application.common.Properties.COMPANY_INFO_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CONTACT_US_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CORPORATE_ALLIANCE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CORPORATE_CAFETERIA;
import static kr.co.mergepoint.mergeclient.application.common.Properties.COUPON_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CROWD_APPROVAL_REQUEST_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CROWD_APPROVAL_REQUEST_RESULT_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CROWD_PAY_HISTORY_DETAIL_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CROWED_PAY_HISTORY_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.DELEGATION_USER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ELECT_FINANCIAL_TERMS_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_NOTICE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_POINT_HISTORY_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_ROLE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.EVENT_DETAIL_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.EVENT_DETAIL_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.EVENT_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.EXTERNAL_AD_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FAVORITE_CROWED_USER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FAVORITE_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FAVORITE_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.GENERAL_PARTNERSHIP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.GRADE_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INTENT_EVENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LICENSE_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MAIN_SEARCH;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MENU_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MENU_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MERGES_PICK_DETAIL_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MERGES_PICK_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NOTICE_DETAIL_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NOTICE_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.POINT_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PRIVACY_POLICY_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.QR_CODE_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.QUESTIONS_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.QUESTION_DIRECT_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RESET_PW_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RESET_PW_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SERVICE_CENTER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SETTINGS_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SETTINGS_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SETTING_APPROVAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SETTING_PREFS;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHARE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TERMS_USE_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.THEME_LIST_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TICKET_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TUTORIAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TUTORIAL_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TUTORIAL_PREFS;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UP_DOWN;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WITHDRAWAL_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WITHDRAWAL_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WITHDRAWAL_POLICY_FRAGMENT;

/**
 * Created by jgson on 2017. 5. 31..
 */

public class MainView extends BaseView<MainActivity, MainPresenter, ActivityMainBinding> implements MainViewInterface {

    private Timer bannerTimer;
    public MainView(MainActivity activity, int layout) {
        super(activity, layout);
    }

    @Override
    protected Toolbar setToolbar() {
        return binding.toolbarBinding.mainToolbar;
    }

    @Override
    public void openMenuAnim() {
        openMenu(R.id.main_layout, getPageFragment(MENU_FRAGMENT, null), MENU_FRAGMENT_TAG);
    }

    @Override
    public void openPage(@Properties.MainFragment int page, String tag) {
        openPage(page, new Bundle(), tag);
    }

    @Override
    public void openPage(@Properties.MainFragment int page, Bundle bundle, String tag) {
        openFragment(RIGHT_LEFT, R.id.main_layout, getPageFragment(page, bundle), tag);
    }

    public void openShopDetail(Shop shop) {
        if (shop.type == GENERAL_PARTNERSHIP) {
            openActivityForResultWithParcelable(ShopActivity.class, SHOP_INFO, shop, SHOP_INFO_REQUEST);
        } else if (shop.type == CORPORATE_ALLIANCE || shop.type == CORPORATE_CAFETERIA) {
            openActivityForResultWithParcelable(EnterpriseShopActivity.class, SHOP_INFO, shop, SHOP_INFO_REQUEST);
        }
    }
    @Override
    public void openVerticalPage(@Properties.MainFragment int page, Bundle bundle, String tag) {
        openAddFragment(UP_DOWN, R.id.main_layout, getPageFragment(page, bundle), tag);
    }

    @Override
    public void openMainSearch() {
        String search = binding.mainStreetWrapper.mainSearchEdit.getText().toString();
        if (search.trim().length() > 0 && !search.isEmpty()) {
            binding.mainStreetWrapper.mainSearchEdit.getText().clear();
            binding.mainStreetWrapper.mainSearchEdit.clearFocus();
            openActivityWithString(SearchActivity.class, MAIN_SEARCH, search);
        } else {
            showAlert(R.string.input_main_search);
        }
    }

    @Override
    public void openThemeFragment(UserLocation userLocation, String data) {
        openFragment(RIGHT_LEFT, R.id.main_layout, ThemeFragment.newInstance(userLocation, data), THEME_LIST_TAG);
    }

    @Override
    public void openFavoriteFragment(UserLocation userLocation) {
        showLoading();
        openFragment(RIGHT_LEFT, R.id.main_layout, FavoriteFragment.newInstance(activity, userLocation), FAVORITE_FRAGMENT_TAG);
    }

    @Override
    public void openEventShared() {
        CombineImagesDetailFragment eventDetailFragment = (CombineImagesDetailFragment) findFragmentByTag(EVENT_DETAIL_FRAGMENT_TAG);
        EventInfo info = eventDetailFragment.getEventInfo();
        if (info != null) {
            openShareFragment(R.id.main_layout, ShareFragment.newInstance(info.title, info.title, info.listImage, info.oid, INTENT_EVENT), SHARE);
        } else {
            showAlert(R.string.empty_event_info);
        }
    }

    @Override
    public void openChooseGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        activity.startActivityForResult(Intent.createChooser(intent, getString(R.string.attach_photo)), ATTACH_PIC_RESULT_REQUEST);
    }

    @Override
    public void openTicketList(ArrayList<Ticket> tickets) {
        openFragment(RIGHT_LEFT, R.id.main_layout, MyTicketFragment.newInstance(tickets), TICKET_FRAGMENT_TAG);
    }

    @Override
    public void openCompanyPointHistory(ArrayList<CompanyPointHistory> companyPointHistories, String userEnterprisePoint) {
        openFragment(RIGHT_LEFT, R.id.main_layout, EnterprisePointFragment.newInstance(activity, companyPointHistories, userEnterprisePoint), ENTERPRISE_POINT_HISTORY_FRAGMENT_TAG);
    }

    @Override
    public void openCrowdPayHistory(ArrayList<CrowdPayItem> crowdPays) {
        openFragment(RIGHT_LEFT, R.id.main_layout, CrowdPayHistoryFragment.newInstance(activity, crowdPays), CROWED_PAY_HISTORY_FRAGMENT_TAG);
    }

    @Override
    public void openCrowdApprovalRequest(ArrayList<CrowdPay> crowdPays) {
        openFragment(RIGHT_LEFT, R.id.main_layout, CrowdApprovalRequestFragment.newInstance(crowdPays), CROWD_APPROVAL_REQUEST_FRAGMENT_TAG);
    }

    @Override
    public void openCrowdApprovalRequestResult(ArrayList<CrowdPay> crowdPays, String message) {
        activity.getSupportFragmentManager().popBackStack();
        activity.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(getEnterAnim(RIGHT_LEFT), getExitAnim(RIGHT_LEFT), getPopEnterAnim(RIGHT_LEFT), getPopExitAnim(RIGHT_LEFT))
                .replace(R.id.main_layout, CrowdApprovalRequestResultFragment.newInstance(crowdPays, message), CROWD_APPROVAL_REQUEST_RESULT_FRAGMENT_TAG)
                .addToBackStack(CROWD_APPROVAL_REQUEST_RESULT_FRAGMENT_TAG).commit();
    }

    @Override
    public void openCrowdPayHistoryDetail(ShopPayment shopPayment) {
        openFragment(RIGHT_LEFT, R.id.main_layout, CrowdPayHistoryDetailFragment.newInstance(shopPayment), CROWD_PAY_HISTORY_DETAIL_FRAGMENT_TAG);
    }

    @Override
    public void openMyCouponList(ArrayList<Coupon> coupons) {
        openFragment(RIGHT_LEFT, R.id.main_layout, MyCouponFragment.newInstance(coupons), COUPON_FRAGMENT_TAG);
    }

    @Override
    public void closeTutorial() {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(TUTORIAL_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(TUTORIAL, true);
        editor.apply();
        activity.onBackPressed();
    }

    @Override
    public void showTutorialView() {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(TUTORIAL_PREFS, Context.MODE_PRIVATE);
        if (!sharedPreferences.getBoolean(TUTORIAL, false)) {
            openFragment(UP_DOWN, R.id.main_layout, TutorialFragment.newInstance(), TUTORIAL_FRAGMENT_TAG);
        }
    }

    @Override
    public void showEnterpriseNotice() {
        Member member = MergeApplication.getMember();
        InitialInfo initialInfo = MergeApplication.getInitialInfo();
        if (initialInfo != null && member != null && member.getRoles().contains(ENTERPRISE_ROLE)) {
            new MergeEnterpriseNotice.Builder(activity).setDeptNotice(initialInfo.getDeptNotice()).setComNotice(initialInfo.getComNotice()).build().show();
        }
    }

    @Override
    public void showEnterpriseState() {
        Member member = MergeApplication.getMember();
        if (member != null) {
            if (member.isSubscribeAlarmed() && member.getRoles().contains(ENTERPRISE_ROLE)) {
                new MergeDialog.Builder(activity).setContent(R.string.intro_enterprise_approval).setCancelBtn(false).build().show();
            } else if (member.isSubscribeAlarmed() && !member.getRoles().contains(ENTERPRISE_ROLE)) {
                new MergeDialog.Builder(activity).setContent(R.string.intro_enterprise_reject).setCancelBtn(false).build().show();
            }
        }
    }

    @Override
    public BaseFragment getPageFragment(@Properties.MainFragment int page, Bundle bundle) {
        switch (page) {
            case EVENT_FRAGMENT:
                return EventFragment.newInstance(activity);
            case POINT_FRAGMENT:
                return PointFragment.newInstance();
            case NOTICE_FRAGMENT:
                return NoticeFragment.newInstance(bundle, activity);
            case NOTICE_DETAIL_FRAGMENT:
                return NoticeDetailFragment.newInstance(bundle);
            case GRADE_FRAGMENT:
                return GradeFragment.newInstance(activity.presenter.baseModel);
            case MENU_FRAGMENT:
                return MenuFragment.newInstance();
            case SETTINGS_FRAGMENT:
                return SettingsFragment.newInstance();
            case WITHDRAWAL_FRAGMENT:
                return WithdrawalFragment.newInstance();
            case WITHDRAWAL_POLICY_FRAGMENT:
                return SimpleWebFragment.newInstance(bundle, getString(R.string.leave_refund_policy));
            case CONTACT_US_FRAGMENT:
                return ContactusFragment.newInstance();
            case TERMS_USE_FRAGMENT:
                return SimpleWebFragment.newInstance(bundle, getString(R.string.terms_of_use));
            case PRIVACY_POLICY_FRAGMENT:
                return SimpleWebFragment.newInstance(bundle, getString(R.string.use_of_personal_information));
            case ELECT_FINANCIAL_TERMS_FRAGMENT:
                return SimpleWebFragment.newInstance(bundle, getString(R.string.electronic_financial_terms));
            case COMPANY_INFO_FRAGMENT:
                return SimpleWebFragment.newInstance(bundle, getString(R.string.company_info));
            case LICENSE_FRAGMENT:
                return SimpleWebFragment.newInstance(bundle, getString(R.string.setting_license));
            case EXTERNAL_AD_FRAGMENT:
                return SimpleWebFragment.newInstance(bundle, getString(R.string.external_ad));
            case QUESTIONS_FRAGMENT:
                return QuestionsFragment.newInstance();
            case QUESTION_DIRECT_FRAGMENT:
                return QuestionDirectFragment.newInstance();
            case MERGES_PICK_FRAGMENT:
                return MergesPickListFragment.newInstance();
            case MERGES_PICK_DETAIL_FRAGMENT:
                return CombineImagesDetailFragment.newInstance(getString(R.string.merges_pick), activity, bundle);
            case EVENT_DETAIL_FRAGMENT:
                return CombineImagesDetailFragment.newInstance(getString(R.string.event), activity, bundle);
            case RESET_PW_FRAGMENT:
                return ResetPwFragment.newInstance(bundle);
            case SETTING_APPROVAL:
                return SettingsCrowedApprovalFragment.newInstance(activity);
            case DELEGATION_USER:
                return DelegationUserFragment.newInstance(activity);
            case FAVORITE_CROWED_USER:
                return FavoriteCrowedUserFragment.newInstance(activity);
            case ENTERPRISE_NOTICE:
                return EnterpriseNoticeFragment.newInstance();
            case ALARM_LIST:
                return AlarmFragment.newInstance(activity);
        }
        return null;
    }

    /* 회원탈퇴 환불여부 */
    @Override
    public boolean getWithdrawalRefund() {
        WithdrawalFragment withdrawalFragment = (WithdrawalFragment) findFragmentByTag(WITHDRAWAL_FRAGMENT_TAG);
//        return (withdrawalFragment != null) && withdrawalFragment.isRefund();
        return false;
    }

    @Override
    public boolean getEqualPw() {
        ResetPwFragment resetPwFragment = (ResetPwFragment) findFragmentByTag(RESET_PW_TAG);
        String pw = resetPwFragment.getPasswordText();
        String pwConfirm = resetPwFragment.getPasswordConfirmText();
        boolean equalPwStr = Objects.equals(pw, pwConfirm);
        if (!equalPwStr)
            new MergeDialog.Builder(activity).setContent(R.string.reset_pw_content).setCancelBtn(false).build().show();
        return equalPwStr;
    }

    @Override
    public int getBannerActionType() {
       /* int position = binding.mainBottomBanner.getCurrentItem();
        if (binding.getBottomBannerPager().getBanners().size() > 0) {
            Banner banner = binding.getBottomBannerPager().getBanners().get(position);
            return banner.actionType;
        }
        */
        return NO_VALUE;
    }

    @Override
    public String getBannerActionData() {
        /*
        int position = binding.mainBottomBanner.getCurrentItem();
        if (binding.getBottomBannerPager().getBanners().size() > 0) {
            Banner banner = binding.getBottomBannerPager().getBanners().get(position);
            return banner.actionData;
        }
        */
        return null;
    }

    @Override
    public Map<String, Object> getResetPwInfo() {
        ResetPwFragment resetPwFragment = (ResetPwFragment) findFragmentByTag(RESET_PW_TAG);
        return (resetPwFragment != null) ? resetPwFragment.getPassword() : null;
    }

    @Override
    public CallbackManager getShareFacebookCallback() {
        ShareFragment fragment = (ShareFragment) findFragmentByTag(SHARE);
        return fragment != null ? fragment.getCallbackManager() : null;
    }

    /* 메인 스트릿 뷰 자동 생성 */
    @Override
    public void setStreet(ArrayList<String> streetArray, StreetButton.OnStreetListener onStreetListener) {
        activity.runOnUiThread(() -> {
            ConstraintLayout layout = binding.mainStreetWrapper.streetLayout;
            ConstraintSet set = new ConstraintSet();
            int height = (int) activity.getResources().getDimension(R.dimen.main_street_btn_height);
            int stRow = (streetArray.size() / 5) * 2;
            int stRowPlus = 0;
            if (streetArray.size() % 5 != 0)
                stRowPlus = streetArray.size() % 5 >= 3 ? 2 : 1 ;
            int row = stRow + stRowPlus;
            int baseID = 0;
            set.clone(layout);
            Button preButton = null;
            int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 9.67f, activity.getResources().getDisplayMetrics());
            int endMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3.3f, activity.getResources().getDisplayMetrics());
            int total = streetArray.size();
            Iterator<String> str = streetArray.iterator();
            for (int i = 0; i < row; i++) {
                ArrayList<StreetButton> buttons = new ArrayList<>();
                int streetRow = i % 2 == 0 ? 2 : 3;
                int temp = total - streetRow;
                int column = temp < 0 ? total % streetRow : streetRow;
                total = temp;
                for (int j = 0; j < column; j++) {
                    String text = str.hasNext() ? str.next() : "";
                    StreetButton button = new StreetButton(activity, onStreetListener, text);
                    layout.addView(button);
                    buttons.add(button);
                    if (j == 0) {
                    /* 제일 왼쪽 기준 */
                        set.connect(button.getId(), ConstraintSet.START, layout.getId(), ConstraintSet.START);
                        if (i == 0) {
                            set.connect(button.getId(), ConstraintSet.TOP, layout.getId(), ConstraintSet.TOP);
                        } else {
                            set.connect(button.getId(), ConstraintSet.TOP, preButton != null ? preButton.getId() : 0, ConstraintSet.BOTTOM, margin);
                        }
                    } else {
                        /* 나머지 */
                        set.connect(baseID, ConstraintSet.END, button.getId(), ConstraintSet.START, endMargin);
                        set.connect(button.getId(), ConstraintSet.START, baseID, ConstraintSet.END, endMargin);
                        set.connect(button.getId(), ConstraintSet.BASELINE, baseID, ConstraintSet.BASELINE);
                    }
                    if (j == column - 1)
                        set.connect(button.getId(), ConstraintSet.END, layout.getId(), ConstraintSet.END);
                    int textWidth = (int) (button.getPaint().measureText(button.getText().toString(), 0, button.getText().toString().length()) + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, activity.getResources().getDisplayMetrics()));
                    set.setHorizontalChainStyle(button.getId(), ConstraintSet.CHAIN_PACKED);
                    set.constrainWidth(button.getId(), textWidth);
                    set.constrainHeight(button.getId(), height);
                    baseID = button.getId();
                    preButton = button;
                }
                /* addToHorizontalChain 만들기 */
                if (buttons.size() > 1) {
                    for (int k = 0; k < buttons.size(); k++) {
                        if (k == 0) {
                            if ( k + 1 >= buttons.size())
                                break;
                            set.addToHorizontalChain(buttons.get(k).getId(), buttons.get(k + 1).getId(), layout.getId());
                        } else if (k == buttons.size() - 1) {
                            set.addToHorizontalChain(buttons.get(k).getId(), layout.getId(), buttons.get(k - 1).getId());
                        } else {
                            set.addToHorizontalChain(buttons.get(k).getId(), buttons.get(k + 1).getId(), buttons.get(k - 1).getId());
                        }
                    }
                }
            }
            set.applyTo(layout);
        });
    }

    @Override
    public void setListWithFavorite(int requestCode, int oid, boolean isFavorite) {
        if (requestCode == FAVORITE_REQUEST) {
            FavoriteFragment favoriteFragment = (FavoriteFragment) findFragmentByTag(FAVORITE_FRAGMENT_TAG);
            favoriteFragment.setFavorite(oid, isFavorite);
        } else {
            ThemeFragment themeFragment = (ThemeFragment) findFragmentByTag(THEME_LIST_TAG);
            themeFragment.setThemeFavorite(oid, isFavorite);
        }
    }

    /* 설정에서 사용자의 로그인 상태를 가져오기 */
    @Override
    public void setUserState() {
        SettingsFragment settingsFragment = (SettingsFragment) findFragmentByTag(SETTINGS_FRAGMENT_TAG);
        if (settingsFragment != null)
            settingsFragment.setUserLogoutState();
    }

    /* QR코드를 위한 권한 동의 */
    @Override
    public void requestCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            /*마시멜로우 이상 권한체크*/
            int camera = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
            if (camera != PackageManager.PERMISSION_GRANTED) {
                if (activity.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    showAlert(R.string.qr_request_permission);
                } else {
                    new MergeDialog.Builder(activity)
                            .setContent(R.string.camera_permission_text)
                            .setConfirmClick(() -> requestPermissions(new String[]{Manifest.permission.CAMERA}, QR_CODE_REQUEST)).build().show();
                }
            } else {
                openActivityForResult(QRcodeActivity.class, QR_CODE_REQUEST);
            }
        } else {
            /*마시멜로우 아랫버전으로 권한체크 불필요*/
            openActivityForResult(QRcodeActivity.class, QR_CODE_REQUEST);
        }
    }

    /* 갤러리를 위한 권한 동의 */
    @Override
    public void requestGalleryPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            /*마시멜로우 이상 권한체크*/
            int gallery = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
            if (gallery != PackageManager.PERMISSION_GRANTED) {
                if (activity.shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showAlert(R.string.gallery_request_permission);
                } else {
                    new MergeDialog.Builder(activity)
                            .setContent(R.string.album_permission_text)
                            .setConfirmClick(() -> requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ATTACH_PIC_RESULT_REQUEST)).build().show();
                }
            } else {
                openChooseGallery();
            }
        } else {
            /*마시멜로우 아랫버전으로 권한체크 불필요*/
            openChooseGallery();
        }
    }

    /* 메뉴의 서비스 센터 전화걸기 */
    @Override
    public void callServiceCenter() {
        activity.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + SERVICE_CENTER)));
    }

    /* 메인 하단 배너 애니메이션 */
    @Override
    public void startAutoBannerSlide() {/*
        bannerTimer = new Timer();
        bannerTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                activity.runOnUiThread(() -> {
                    if (binding.mainBottomBanner != null && binding.mainBottomBanner.getAdapter() != null) {
                        int current = binding.mainBottomBanner.getCurrentItem();
                        int total = binding.mainBottomBanner.getAdapter().getCount();
                        binding.mainBottomBanner.setCurrentItem((current == total - 1) ? 0 : current + 1, true);
                        Log.d(TAG, "run: " + "banner");
                    }
                });
            }
        }, BANNER_DELAY, BANNER_PERIOD);*/
    }

    @Override
    public void stopAutoBannerSlide() {
        /*
        bannerTimer.cancel();
        bannerTimer = null;
        */
    }

    @Override
    public void updateCrowdApprovalType(int type, int oid, String userName) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(SETTING_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(APPROVAL_TYPE, type);
        if (oid != 0) {
            editor.putInt(APPROVAL_DELEGATION_OID, oid);
            editor.putString(APPROVAL_DELEGATION_NAME, userName);
        }
        editor.apply();
    }
}
