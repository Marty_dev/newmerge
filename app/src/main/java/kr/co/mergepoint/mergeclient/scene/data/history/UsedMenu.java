package kr.co.mergepoint.mergeclient.scene.data.history;

import com.google.gson.annotations.SerializedName;

import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;

/**
 * Created by 1017sjg on 2017. 9. 22..
 */

public class UsedMenu {

    @SerializedName("useCount")
    public int useCount;

    @SerializedName("payMenu")
    public ShopMenuDetail payMenu;
}
