package kr.co.mergepoint.mergeclient.scene.payment.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.application.common.CustomCountView;
import kr.co.mergepoint.mergeclient.databinding.EnterpriseSelectPointsItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.UseTicket;

/**
 * Created by 1017sjg on 2017. 7. 6..
 */

public class TicketListAdapter extends BasicListAdapter<BasicListHolder<EnterpriseSelectPointsItemBinding, UseTicket>, UseTicket> {

    private CustomCountView.OnChangeCountListener countListener;

    public TicketListAdapter(ArrayList<UseTicket> arrayList, CustomCountView.OnChangeCountListener countListener) {
        super(arrayList);
        this.countListener = countListener;
    }

    @Override
    public BasicListHolder<EnterpriseSelectPointsItemBinding, UseTicket> setCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.enterprise_select_points_item, parent, false);
        EnterpriseSelectPointsItemBinding listBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<EnterpriseSelectPointsItemBinding, UseTicket>(listBinding) {
            @Override
            public void setDataBindingWithData(UseTicket data) {
                getDataBinding().countView.setText(String.valueOf(data.ticketCount));
                getDataBinding().countView.minCount = 0;
                getDataBinding().countView.menuPrice = data.companyTicket.ticketUnitPrice;
                getDataBinding().setTicket(data.companyTicket);
                getDataBinding().setOnCountListener(countListener);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<EnterpriseSelectPointsItemBinding, UseTicket> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
