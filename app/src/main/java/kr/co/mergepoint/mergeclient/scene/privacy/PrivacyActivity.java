package kr.co.mergepoint.mergeclient.scene.privacy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import javax.inject.Inject;

import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.script.PostScriptListener;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.SearchEnterpriseListener;
import kr.co.mergepoint.mergeclient.scene.privacy.dagger.DaggerPrivacyComponent;
import kr.co.mergepoint.mergeclient.scene.privacy.dagger.PrivacyModule;
import kr.co.mergepoint.mergeclient.scene.privacy.presenter.PrivacyPresenter;

/**
 * Created by jgson on 2017. 6. 1..
 */

public class PrivacyActivity extends BaseActivity implements PostScriptListener, SearchEnterpriseListener {

    @Inject PrivacyPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerPrivacyComponent.builder().privacyModule(new PrivacyModule(this)).build().inject(this);
    }

    @Override
    protected void onActivityClick(View view) {
        presenter.onClick(view);
    }

    @Override
    public void setAddress(String add01, String add02, String add03) {
        presenter.setAddress(add01, add02, add03);
    }

    @Override
    public void searchEnterprise(String searchText) {
        presenter.searchEnterprise(searchText);
    }

    @Override
    public void registerEnterprise(int companyRef) {
        presenter.registerEnterprise(companyRef);
    }
}
