package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by 1017sjg on 2017. 9. 14..
 */

public class TypeButton extends AppCompatButton implements View.OnClickListener {

    private OnTypeListener typeListener;
    private int type;
    private String data;

    @Properties.TouchType
    public int getType() {
        return type;
    }

    public void setType(@Properties.TouchType int type) {
        this.type = type;
    }

    public void setTypeListener(OnTypeListener typeListener) {
        this.typeListener = typeListener;
    }

    public void setTypeWithData(int type, String data) {
        setType(type);
        this.data = data;
    }

    public TypeButton(Context context) {
        super(context);
        init();
    }

    public TypeButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TypeButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (typeListener != null)
            typeListener.onTypeClick(getType(), data);
    }
}
