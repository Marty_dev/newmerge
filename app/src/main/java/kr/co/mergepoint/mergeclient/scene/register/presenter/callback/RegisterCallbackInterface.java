package kr.co.mergepoint.mergeclient.scene.register.presenter.callback;

import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.register.IdConfirm;
import kr.co.mergepoint.mergeclient.scene.data.register.PhoneConfirm;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import okhttp3.ResponseBody;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public interface RegisterCallbackInterface {

    MergeCallback<PhoneConfirm> phoneConfirmCallback();

    MergeCallback<IdConfirm> idConfirmCallback();

    MergeCallback<ResponseObject<String>> sendPhoneAuthNumCallback();

    MergeCallback<ResponseObject<Member>> registerCallback();

    /* 회원 탈퇴 처리한 회원 복구하기 */
    MergeCallback<ResponseObject<Member>> restoreUser();

    /* 로그아웃 */
    MergeCallback<ResponseBody> getLogoutCallback();
}
