package kr.co.mergepoint.mergeclient.scene.main.presenter.callback;

import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.net.CookieHandler;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseCallback;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.CustomCookieManager;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.CompanyPointHistory;
import kr.co.mergepoint.mergeclient.scene.data.history.ShopHistoryPayment;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.main.CombineImagesDetail;
import kr.co.mergepoint.mergeclient.scene.data.main.EventDetail;
import kr.co.mergepoint.mergeclient.scene.data.main.Notice;
import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPayItem;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.data.payment.Ticket;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;
import kr.co.mergepoint.mergeclient.scene.login.LoginActivity;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.main.model.MainModel;
import kr.co.mergepoint.mergeclient.scene.main.view.MainView;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.CrowdPayHistoryFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.DelegationUserFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.EnterprisePointFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.FavoriteCrowedUserFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.FavoriteFragment;
import kr.co.mergepoint.mergeclient.scene.register.RegisterActivity;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import okhttp3.ResponseBody;

import static kr.co.mergepoint.mergeclient.application.common.Properties.COMBINE_EVENT_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.COMBINE_IMG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CROWED_PAY_HISTORY_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.DELEGATION_USER_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_POINT_HISTORY_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.EVENT_DETAIL_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.EVENT_DETAIL_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FAVORITE_CROWED_USER_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FAVORITE_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MEMBER_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MERGES_PICK_DETAIL_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MERGES_PICK_DETAIL_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NOTICE_FRAGMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NOTICE_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAYMENT_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class MainCallback extends BaseCallback<MainView, MainModel> implements MainCallbackInterface{

    public MainCallback(MainView baseView, MainModel baseModel) {
        super(baseView, baseModel);
    }

    @Override
    public MergeCallback<ResponseBody> getTokenCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {});
    }

    @Override
    public MergeCallback<ResponseObject<Long>> refreshUserPoints() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Long> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                InitialInfo initialInfo = MergeApplication.getInitialInfo();
                if (initialInfo != null && initialInfo.getMember() != null)
                    initialInfo.getMember().setPoints(responseObject.getObject().intValue());
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<JsonObject>> shopVisitCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<JsonObject> responseBody = response.body();
            if (response.isSuccessful() && responseBody != null) {
                if (Objects.equals(responseBody.getSource(), "payment")) {
                    ShopHistoryPayment historyPayment = new Gson().fromJson(responseBody.getObject().toString(), ShopHistoryPayment.class);
                    baseView.openActivityWithParcelable(UsageActivity.class, PAYMENT_INFO, historyPayment);
                } else if (Objects.equals(responseBody.getSource(), "shop")) {
                    Shop shop = new Gson().fromJson(responseBody.getObject().toString(), Shop.class);
                    baseView.openActivityWithParcelable(ShopActivity.class, SHOP_INFO, shop);
                }
            }
        });
    }

    @Override
    public MergeCallback<ResponseBody> getLogoutCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            baseView.showAlert(R.string.logout_success);
            CustomCookieManager customCookieManager = (CustomCookieManager) CookieHandler.getDefault();
            customCookieManager.getCookieStore().removeAll();
            InitialInfo initialInfo = MergeApplication.getInitialInfo();
            if (initialInfo != null)
                initialInfo.setLoginState(null, false);
            baseView.setUserState();
        }, R.string.logout_fail);
    }

    @Override
    public MergeCallback<ResponseObject<Object>> getWithdrawalCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Object> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                if (!responseObject.isFailed()) {
                    CustomCookieManager customCookieManager = (CustomCookieManager) CookieHandler.getDefault();
                    customCookieManager.getCookieStore().removeAll();
                    InitialInfo initialInfo = MergeApplication.getInitialInfo();
                    if (initialInfo != null)
                        initialInfo.setLoginState(null, false);
                    baseView.openClearTaskActivity(MainActivity.class);
                }
                baseView.showAlert(responseObject.getMessage());
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Object>> getAllianceCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Object> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                new MergeDialog.Builder(baseView.activity)
                        .setContent(R.string.success_alliance_text)
                        .setConfirmString(R.string.move_main)
                        .setConfirmClick(() -> baseView.openClearTaskActivity(MainActivity.class)).build().show();
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Integer>> getQuestionCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Integer> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                new MergeDialog.Builder(baseView.activity)
                        .setContent(R.string.success_contact_us_text)
                        .setConfirmString(R.string.move_main)
                        .setConfirmClick(() -> baseView.openClearTaskActivity(MainActivity.class)).build().show();
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Integer>> getCouponCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Integer> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                new MergeDialog.Builder(baseView.activity).setContent(R.string.success_issuing_coupon).setCancelBtn(false).build().show();
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Member>> getMemberInfoCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Member> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                Member member = responseObject.getObject();
                member.setSocial(true);
                baseView.openActivityWithParcelable(RegisterActivity.class, MEMBER_INFO, member);
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Member>> resetPasswordCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Member> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed() && responseObject.getObject() != null) {
                Member member = responseObject.getObject();
                new MergeDialog.Builder(baseView.activity).setContent(baseView.activity.getString(R.string.success_reset_pw, member.getName())).setCancelBtn(false).setConfirmClick(() -> baseView.openActivity(LoginActivity.class)).build().show();
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<Notice>>> getNoticeListCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<Notice>> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed() && responseObject.getObject() != null) {
                ArrayList<Notice> notices = responseObject.getObject();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(NOTICE_FRAGMENT_TAG, notices);
                baseView.openPage(NOTICE_FRAGMENT, bundle, NOTICE_FRAGMENT_TAG);
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CombineImagesDetail>>> getMergesPickCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CombineImagesDetail>> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(COMBINE_IMG, responseObject.getObject());
                baseView.openPage(MERGES_PICK_DETAIL_FRAGMENT, bundle, MERGES_PICK_DETAIL_TAG);
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<EventDetail>> getEventDetailCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<EventDetail> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                EventDetail eventDetail = responseObject.getObject();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(COMBINE_IMG, eventDetail.details);
                bundle.putParcelable(COMBINE_EVENT_INFO, eventDetail.getEventInfo());
                baseView.openPage(EVENT_DETAIL_FRAGMENT, bundle, EVENT_DETAIL_FRAGMENT_TAG);
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<Shop>>> getFavoriteListCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<Shop>> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                ArrayList<Shop> shopInfo = responseObject.getObject();
                FavoriteFragment favoriteFragment = (FavoriteFragment) baseView.findFragmentByTag(FAVORITE_FRAGMENT_TAG);
                if (favoriteFragment != null)
                    favoriteFragment.setFavoriteAdapter(shopInfo);
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Boolean>> changeCrowdApprovalType(int type, int oid, String userName) {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Boolean> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                if (!responseObject.getObject()) {
                    new MergeDialog.Builder(baseView.activity).setContent(responseObject.getMessage()).setCancelBtn(false).build().show();
                } else {
                    baseView.updateCrowdApprovalType(type, oid, userName);
                    if (baseView.findFragmentByTag(DELEGATION_USER_FRAGMENT_TAG) != null)
                        baseView.activity.onBackPressed();
                }
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CrowdMember>>> getCrowdMemberList() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CrowdMember>> responseObject = response.body();
            FavoriteCrowedUserFragment fragment = (FavoriteCrowedUserFragment) baseView.findFragmentByTag(FAVORITE_CROWED_USER_FRAGMENT_TAG);
            if (fragment != null && responseObject != null) {
                fragment.setFavoriteCrowedPayUserAdapter(responseObject.getObject());
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CrowdMember>>> getDelegationMemberList() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CrowdMember>> responseObject = response.body();
            DelegationUserFragment fragment = (DelegationUserFragment) baseView.findFragmentByTag(DELEGATION_USER_FRAGMENT_TAG);
            if (fragment != null && responseObject != null) {
                fragment.setDelegationUserAdapter(responseObject.getObject());
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Long>> changeFavoriteCrowdMember() {
        return new MergeCallback<>(baseView, (call, response) -> {});
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<Ticket>>> getTicketList() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<Ticket>> responseObject = response.body();
            if (responseObject != null) {
                for (Ticket ticket : responseObject.getObject()) {
                    MDEBUG.debug("ticket origin  : " + ticket.getCompareDay());

                    if (ticket.expireDateTime != null) {
                        for (Integer i : ticket.expireDateTime) {
                            MDEBUG.debug("ticket origin  : " + i);
                        }
                    }
                    MDEBUG.debug("-------------------");

                }
                baseView.openTicketList(responseObject.getObject());
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CompanyPointHistory>>> getCompanyPointHistory() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CompanyPointHistory>> responseObject = response.body();
            if (responseObject != null) {
                BaseFragment pointFragment = baseView.findFragmentByTag(ENTERPRISE_POINT_HISTORY_FRAGMENT_TAG);
                ArrayList<CompanyPointHistory> tempList = new ArrayList<>();
                for (CompanyPointHistory ref : responseObject.getObject()){
                    MDEBUG.debug("obj info : " + ref.reason);
                    MDEBUG.debug("obj info : " + ref.givenDateTime);
                    MDEBUG.debug("obj info : " + ref.point);
                    MDEBUG.debug("obj info : " + ref.remainTicketPrice);
                    MDEBUG.debug("obj info : " + ref.tickePrice);
                    MDEBUG.debug("obj info : " + ref.type);
                    if (ref.tickePrice == 0  && ref.remainTicketPrice == 0){
                        tempList.add(ref);
                    }else{
                        ref.point = (int)ref.tickePrice;
                        tempList.add(ref);
                    }
                }
                if (pointFragment != null) {
                    EnterprisePointFragment enterprisePointFragment = (EnterprisePointFragment) pointFragment;
                    enterprisePointFragment.changePointHistory(tempList);
                } else {
                    int totalPoint = 0;
                    try {
                        totalPoint = Integer.valueOf(responseObject.getMessage());
                    } catch (NumberFormatException ignored) {}
                    baseView.openCompanyPointHistory(tempList, String.format(Locale.getDefault(), "%,dP", totalPoint));
                }
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CrowdPayItem>>> getCrowdPaymentHistory() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CrowdPayItem>> responseObject = response.body();
            if (responseObject != null) {
                BaseFragment payFragment = baseView.findFragmentByTag(CROWED_PAY_HISTORY_FRAGMENT_TAG);
                if (payFragment != null) {
                    CrowdPayHistoryFragment historyFragment = (CrowdPayHistoryFragment) payFragment;
                    historyFragment.changeCrowdPayHistory(responseObject.getObject());
                } else {
                    baseView.openCrowdPayHistory(responseObject.getObject());
                }
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CrowdPay>>> getCrowdApprovalRequestList() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CrowdPay>> responseObject = response.body();


            if (responseObject.getObject() != null) {
                    ArrayList<CrowdPay> crowdPays = responseObject.getObject();
                    baseView.openCrowdApprovalRequest(crowdPays == null ? new ArrayList<>() : crowdPays);
            }else if (responseObject.getObject() == null && responseObject.isFailed()){
                new MergeDialog.Builder(baseView.activity).setContent("해당 결제가 취소되어\n" +
                        "더 이상 진행 할 수 없습니다.").setCancelBtn(false).setConfirmBtn(true).setConfirmString("확인").build().show();

            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CrowdPay>>> sendCheckApproval() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CrowdPay>> responseObject = response.body();

            if (responseObject != null) {
                baseView.openCrowdApprovalRequestResult(responseObject.getObject(), responseObject.getMessage());
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ShopPayment>> getCrowdPayHistoryDetail() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ShopPayment> responseObject = response.body();
            if (responseObject != null)
                baseView.openCrowdPayHistoryDetail(responseObject.getObject());
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<Coupon>>> getMyCouponList() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<Coupon>> responseObject = response.body();
            if (responseObject != null) {
                baseView.openMyCouponList(responseObject.getObject());
            }
        });
    }
}
