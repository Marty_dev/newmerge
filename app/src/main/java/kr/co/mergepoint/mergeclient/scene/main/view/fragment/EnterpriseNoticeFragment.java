package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Locale;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.EnterpriseNoticeBinding;
import kr.co.mergepoint.mergeclient.scene.data.menu.EnterpriseNotice;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;

/**
 * Created by 1017sjg on 2017. 8. 7..
 */

public class EnterpriseNoticeFragment extends BaseFragment implements TabLayout.OnTabSelectedListener {

    private EnterpriseNoticeBinding noticeBinding;
    private EnterpriseNotice comNotice;
    private EnterpriseNotice deptNotice;

    public static EnterpriseNoticeFragment newInstance() {
        EnterpriseNoticeFragment fragment = new EnterpriseNoticeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InitialInfo initialInfo = MergeApplication.getInitialInfo();
        if (initialInfo != null) {
            comNotice = initialInfo.getComNotice();
            deptNotice = initialInfo.getDeptNotice();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        noticeBinding = DataBindingUtil.inflate(inflater, R.layout.enterprise_notice, container, false);
        noticeBinding.setTitle("기업 공지사항");
        noticeBinding.setOnClick(this);
        noticeBinding.setTabArray(new String[]{"관리자 공지", "회사 공지"});
        noticeBinding.setTabListener(this);
        int position = noticeBinding.enterpriseNoticeTab.getSelectedTabPosition();
        setNoticeInfo(position);
        return noticeBinding.getRoot();
    }

    private void setNoticeInfo(int position) {
        if (position == 0 || position == -1) {
            noticeBinding.emptyEnterpriseNotice.setVisibility(deptNotice != null ? View.GONE : View.VISIBLE);
            noticeBinding.enterpriseNoticeScroll.setVisibility(deptNotice != null ? View.VISIBLE : View.GONE);
            if (deptNotice != null)
                displayNoticeText(deptNotice);
        } else {
            noticeBinding.emptyEnterpriseNotice.setVisibility(comNotice != null ? View.GONE : View.VISIBLE);
            noticeBinding.enterpriseNoticeScroll.setVisibility(comNotice != null ? View.VISIBLE : View.GONE);
            if (comNotice != null)
                displayNoticeText(comNotice);
        }
    }

    private void displayNoticeText(EnterpriseNotice notice) {
        noticeBinding.enterpriseNoticeTitle.setText(notice.title);
        noticeBinding.enterpriseNoticeDate.setText(String.format(Locale.getDefault(), "%d-%02d-%02d-%02d:%02d", notice.staDateTime.get(0), notice.staDateTime.get(1), notice.staDateTime.get(2), notice.staDateTime.get(3), notice.staDateTime.get(4)));
        noticeBinding.enterpriseNoticeMessage.setText(notice.bodyText);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        setNoticeInfo(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}

    @Override
    public void onTabReselected(TabLayout.Tab tab) {}
}
