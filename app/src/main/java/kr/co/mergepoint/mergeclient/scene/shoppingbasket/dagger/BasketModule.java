package kr.co.mergepoint.mergeclient.scene.shoppingbasket.dagger;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.BasketActivity;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.model.BasketModel;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.presenter.BasketPresenter;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.presenter.callback.BasketCallback;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.view.BasketView;

/**
 * Created by jgson on 2017. 6. 5..
 */

@Module
public class BasketModule {

    private BasketActivity activity;

    public BasketModule(BasketActivity activity) {
        this.activity = activity;
    }

    @Provides
    @BasketScope
    BasketModel provideModel() { return new BasketModel(); }

    @Provides
    @BasketScope
    BasketView provideView() { return new BasketView(activity, R.layout.shopping_basket); }

    @Provides
    @BasketScope
    BasketPresenter providePresenter(BasketView view, BasketModel model) {
        return new BasketPresenter(view, model, new BasketCallback(view, model));
    }
}
