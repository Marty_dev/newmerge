package kr.co.mergepoint.mergeclient.application.common;

import kr.co.mergepoint.mergeclient.application.MergeApplication;
import retrofit2.Retrofit;

/**
 * Created by 1017sjg on 2017. 7. 27..
 */

public class BaseModel {

    protected Retrofit retrofit;

    public BaseModel() {
        retrofit = MergeApplication.getMergeAppComponent().getRetrofit();
    }
}
