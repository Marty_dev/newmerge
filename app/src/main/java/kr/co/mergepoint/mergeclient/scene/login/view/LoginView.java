package kr.co.mergepoint.mergeclient.scene.login.view;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.application.common.script.LoginScriptListener;
import kr.co.mergepoint.mergeclient.databinding.LoginBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.login.SocialSignup;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;
import kr.co.mergepoint.mergeclient.scene.login.LoginActivity;
import kr.co.mergepoint.mergeclient.scene.login.presenter.LoginPresenter;
import kr.co.mergepoint.mergeclient.scene.login.view.fragment.ForgotPwFragment;
import kr.co.mergepoint.mergeclient.scene.login.view.fragment.ForgotPwResultFragment;
import kr.co.mergepoint.mergeclient.scene.login.view.fragment.SocialLoginView;
import kr.co.mergepoint.mergeclient.scene.login.view.fragment.SocialSignupEmailFragment;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.register.RegisterActivity;

import static kr.co.mergepoint.mergeclient.application.common.Properties.ACTIVE_USER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FORGOT_PW_RESULT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FORGOT_PW_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INPUT_INSTA_EMAIL_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MEMBER_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SOCIAL_LOGIN_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SOURCE_ACTIVITY;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UP_DOWN;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WITHDRAWAL_USER;

/**
 * Created by jgson on 2017. 5. 31..
 */

public class LoginView extends BaseView<LoginActivity, LoginPresenter, LoginBinding> {

    public LoginView(LoginActivity activity, int layout) {
        super(activity, layout);
    }

    @Override
    protected Toolbar setToolbar() {
        return binding.toolbarBinding.basicToolbar;
    }

    public void openSocialRegister(Member member) {
        removeFragment(findFragmentByTag(SOCIAL_LOGIN_TAG));
        openActivityWithParcelable(RegisterActivity.class, MEMBER_INFO, member);
    }

    public void openForgotPasswordPage(TextView.OnEditorActionListener actionListener) {
        openFragment(RIGHT_LEFT, R.id.main_wrapper, ForgotPwFragment.newInstance(actionListener), FORGOT_PW_TAG);
    }

    public void openForgotPwResultPage() {
        openFragment(RIGHT_LEFT, R.id.main_wrapper, ForgotPwResultFragment.newInstance(), FORGOT_PW_RESULT_TAG);
    }

    public void openSocialLogin(final String url, LoginScriptListener listener) {
        openFragment(UP_DOWN, R.id.main_wrapper, SocialLoginView.newInstance(url, listener), SOCIAL_LOGIN_TAG);
    }

    public void openMain(Member member) {
        InitialInfo initialInfo = MergeApplication.getInitialInfo();
        if (initialInfo != null) {
            initialInfo.setLoginState(member, true);

            Bundle bundle = activity.getIntent().getExtras();
            if (bundle != null) {
                Class<?> tClass = (Class<?>) bundle.get(SOURCE_ACTIVITY);
                openClearTopActivity(tClass);
            } else {
                openClearTaskActivity(MainActivity.class);
            }
        }
    }

    public void forgotPwEnd() {
        removeFragment(findFragmentByTag(FORGOT_PW_RESULT_TAG));
        removeFragment(findFragmentByTag(FORGOT_PW_TAG));
    }

    private Boolean isEmptyUserID() {
        return getUserID().isEmpty() && getUserID().length() > 0;
    }

    private Boolean isEmptyUserPW() {
        return getUserPW().isEmpty() && getUserID().length() > 0;
    }

    public String getUserID() {
        return binding.userId.getText().toString();
    }

    public String getUserPW() {
        return binding.userPw.getText().toString();
    }

    public String getForgotPasswordEmail() {
        ForgotPwFragment forgotPwFragment = (ForgotPwFragment) findFragmentByTag(FORGOT_PW_TAG);
        return forgotPwFragment != null && forgotPwFragment.getForgotPasswordBinding() != null ? forgotPwFragment.getForgotPasswordBinding().pwUserId.getText().toString().trim() : "";
    }

    public SocialSignup getSocialRegisterInfo() {
        SocialSignupEmailFragment signupEmailFragment = (SocialSignupEmailFragment) findFragmentByTag(INPUT_INSTA_EMAIL_TAG);

        if (signupEmailFragment.getInputEmailInstaBinding() != null) {
            String email = signupEmailFragment.getInputEmailInstaBinding().socialUserEmail.getText().toString().trim();
            boolean checkEmailFormat = MergeApplication.getMergeAppComponent().getUtility().checkEmailFormat(email);
            SocialSignup socialSignup = signupEmailFragment.getSocialSignup();
            if (!email.isEmpty() && email.length() > 0 && checkEmailFormat) {
                showLoading();
                socialSignup.email = email;
                return socialSignup;
            } else if (!checkEmailFormat) {
                showAlert(R.string.enter_mail_format);
                return null;
            } else {
                showAlert(R.string.enter_mail_join);
                return null;
            }
        } else {
            return null;
        }
    }

    public boolean login() {
        if (isEmptyUserID()) {
            showAlert(R.string.requried_id);
            return false;
        } else if (isEmptyUserPW()) {
            showAlert(R.string.requried_pw);
            return false;
        }
        showLoading();
        return true;
    }

    public boolean forgotPw() {
        ForgotPwFragment forgotPwFragment = (ForgotPwFragment) findFragmentByTag(FORGOT_PW_TAG);
        return forgotPwFragment != null;
    }

    public boolean requirePwAuthMail() {
        String id = getForgotPasswordEmail();
        boolean checkEmailFormat = MergeApplication.getMergeAppComponent().getUtility().checkEmailFormat(id);
        if (!id.isEmpty() && id.length() > 0 && checkEmailFormat) {
            showLoading();
            return true;
        } else if (!checkEmailFormat) {
            showAlert(R.string.enter_mail_format);
            return false;
        } else {
            showAlert(R.string.enter_mail_used_id);
            return false;
        }
    }

    public void userStateCheck(Member member, MergeDialog.OnDialogClick confirmClick, MergeDialog.OnDialogClick cancelClick) {
        if (member.getState() == ACTIVE_USER) {
            /*활성화 유저*/
            openMain(member);
        } else if (member.getState() == WITHDRAWAL_USER) {
            /*탈퇴한 유저*/
            new MergeDialog.Builder(activity).setContent(R.string.withdrawal_user).setConfirmString(R.string.recovery).setConfirmClick(confirmClick).setCancelClick(cancelClick).build().show();
        } else {
            /*휴면 유저 - 정책이 만들어진 후 처리*/
        }
    }
}
