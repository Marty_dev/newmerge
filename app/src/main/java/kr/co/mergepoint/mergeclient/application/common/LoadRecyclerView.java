package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;
import android.view.View;

import java.util.Map;

import kr.co.mergepoint.mergeclient.api.StreetApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.ShopInfo;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.ADD;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INITIAL;

/**
 * Created by 1017sjg on 2017. 8. 3..
 */

public class LoadRecyclerView extends CustomRecyclerView {

    private LoadRecyclerViewListener listener;
    private ListLoadScrollListener listLoadScrollListener;
    private int totalPage;
    private int currentPage;
    private String searchText;
    private Call preCall;

    public void disableSearch() {
        searchText = null;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setListener(LoadRecyclerViewListener listener) {
        this.listener = listener;
    }

    public ListLoadScrollListener getListLoadScrollListener() {
        return listLoadScrollListener;
    }

    public interface LoadRecyclerViewListener extends CustomRecyclerViewTouch {
        void onLoadItem(LoadRecyclerView loadRecyclerView, int currentPage);
        void onInitSuccess(ShopInfo shopInfo);
        void onAddSuccess(ShopInfo shopInfo);
        void onResponseFailure(@NonNull Call<ShopInfo> call);
    }

    public LoadRecyclerView(Context context) {
        super(context);
    }

    public LoadRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public LoadRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setInit(BasicListAdapter adapter, final LoadRecyclerViewListener listener) {
        super.setInit(adapter);
        setListener(listener);
        listLoadScrollListener = new ListLoadScrollListener() {
            @Override
            public void onLoadMore(int currentPage) {
                if (totalPage >= currentPage)
                    listener.onLoadItem(LoadRecyclerView.this, currentPage);
            }

            @Override
            public LinearLayoutManager setLinearLayoutManager() {
                return (CustomLinearLayoutManager) getLayoutManager();
            }
        };
        addOnScrollListener(listLoadScrollListener);
    }

    @Override
    public void onClickItem(View v, int position) {
        if (listener != null)
            listener.onTouchItem(this, v, position);
    }

    public void loadShopList(@Properties.LoadState int loadState, Map<String, Object> parameter) {
        if (preCall != null)
            preCall.cancel();
        MDEBUG.debug("loadShopList Called");
        parameter.put("version" , "1.3.2");
        Call<ShopInfo> call = MergeApplication.getMergeAppComponent().getRetrofit().create(StreetApi.class).getStreetInfo(parameter);
        call.enqueue(getShopListCallback(loadState));
        preCall = call;
    }

    public Callback<ShopInfo> getShopListCallback(@Properties.LoadState int loadState) {
        return new Callback<ShopInfo>() {
            @Override
            public void onResponse(@NonNull Call<ShopInfo> call, @NonNull Response<ShopInfo> response) {
                ShopInfo shopInfo = response.body();
                if (response.isSuccessful() && shopInfo != null) {
                    if (loadState == INITIAL) {
                        listener.onInitSuccess(shopInfo);
                    } else if (loadState == ADD) {
                        listener.onAddSuccess(shopInfo);
                    }
                } else {
                    listener.onResponseFailure(call);
                }
                if (getContext() instanceof BaseActivity)
                    MergeApplication.getMergeApplication().hideLoading((BaseActivity) getContext());
            }

            @Override
            public void onFailure(@NonNull Call<ShopInfo> call, @NonNull Throwable t) {
                if (!call.isCanceled())
                    listener.onResponseFailure(call);
                if (getContext() instanceof BaseActivity)
                    MergeApplication.getMergeApplication().hideLoading((BaseActivity) getContext());
            }
        };
    }
}
