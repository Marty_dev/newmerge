package kr.co.mergepoint.mergeclient.scene.shop.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.databinding.ShopDetailPreExperienceDetailImgBinding;
import kr.co.mergepoint.mergeclient.scene.data.shop.PreExperience;

/**
 * Created by jgson on 2017. 12. 7..
 */

public class PreExperiencePagerAdapter extends PagerAdapter {

    protected ArrayList<PreExperience> imgUrl;

    public PreExperiencePagerAdapter(ArrayList<PreExperience> imgUrl) {
        this.imgUrl = imgUrl;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ShopDetailPreExperienceDetailImgBinding imgBinding = DataBindingUtil.inflate(LayoutInflater.from(container.getContext()), R.layout.shop_detail_pre_experience_detail_img, container, false);
        imgBinding.setUrl(imgUrl.get(position).image);
        container.addView(imgBinding.getRoot());
        return imgBinding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return imgUrl.size() == 0 ? 1 : imgUrl.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
