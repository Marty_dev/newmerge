package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.script.WebReturnScriptListener;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.newcoupon.BarcodeGen;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.http.Payment_v2_Callbacks;

/**
 * Created by jgson on 2017. 6. 1..
 */

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener ,WebReturnScriptListener {
    public Payment_v2_Callbacks callbacks;
    @Override
    public void finish() {
        super.finish();
        hideKeyboard();
        overridePendingTransitionExit();
    }

    @Override
    public void onBackPressed() {
        if (!(getSupportFragmentManager().getFragments().size() > 0 && getSupportFragmentManager().popBackStackImmediate())) {
            if (BaseActivity.this instanceof MainActivity) {
                new MergeDialog.Builder(this).setContent("종료하시겠어요?")
                        .setConfirmClick(() -> {
                            BaseActivity.this.finish();
                            overridePendingTransitionExit();
                        }).build().show();
            } else {
                super.onBackPressed();
                overridePendingTransitionExit();
            }
        }
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        hideKeyboard();
        overridePendingTransitionEnter();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Thread.setDefaultUncaughtExceptionHandler(new MergeUncaughtException(this));
        overridePendingTransitionEnter();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        callbacks = new Payment_v2_Callbacks(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    @Override
    public void success() {

    }

    @Override
    public void failure(String error) {

    }
    public String MoneyForm(String money) {
        return String.format("%,d", Integer.parseInt(money));
    }
    public String MoneyForm(int money) {
        return String.format("%,d", money);
    }
    public String MoneyForm(long money) {
        return String.format("%,d", money);
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null && imm != null)
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    protected void showKeyboard() {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void onClick(View v) {
        onActivityClick(v);
    }

    protected abstract void onActivityClick(View view);

    public Bitmap BarcodeGenerateRotate(String code){
        BarcodeGen gen = new BarcodeGen();
        float wpx, hpx;
        wpx = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 390,
                getResources().getDisplayMetrics()
        );
        hpx = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 111,
                getResources().getDisplayMetrics()
        );
        try {
            Bitmap bitmap = gen.encodeAsBitmap(code, BarcodeFormat.CODE_128, (int) wpx, (int) hpx);
            Matrix ma = new Matrix();
            ma.postRotate(90f);
            Bitmap rotatebitBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),ma,false);
            return rotatebitBitmap;
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;
    }
    public Bitmap BarcodeGenerate(String code){
        BarcodeGen gen = new BarcodeGen();
        float wpx, hpx;
        wpx = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 220,
                getResources().getDisplayMetrics()
        );
        hpx = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 70,
                getResources().getDisplayMetrics()
        );
        try {
            Bitmap bitmap = gen.encodeAsBitmap(code, BarcodeFormat.CODE_128, (int) wpx, (int) hpx);
            return bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;

    }

    public Bitmap BarcodeGenerate(String code,int w, int h){
        BarcodeGen gen = new BarcodeGen();
        float wpx, hpx;
        wpx = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, w,
                getResources().getDisplayMetrics()
        );
        hpx = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, h,
                getResources().getDisplayMetrics()
        );
        try {
            Bitmap bitmap = gen.encodeAsBitmap(code, BarcodeFormat.CODE_128, (int) wpx, (int) hpx);
            return bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;

    }

    public Bitmap BarcodeGenerateRotate(String code,int w, int h,boolean isRotate){
        BarcodeGen gen = new BarcodeGen();
        float wpx, hpx;
        wpx = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, w,
                getResources().getDisplayMetrics()
        );
        hpx = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, h,
                getResources().getDisplayMetrics()
        );
        try {
            Bitmap bitmap = gen.encodeAsBitmap(code, BarcodeFormat.CODE_128, (int) wpx, (int) hpx);
            Matrix ma = new Matrix();
            ma.postRotate(90f);
            Bitmap rotatebitBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),ma,false);
            return isRotate ? rotatebitBitmap : bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;

    }
}
