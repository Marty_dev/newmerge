package kr.co.mergepoint.mergeclient.scene.shop.view.fragment;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.ShopMenuDetailBinding;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionMenu;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.ShopMenuDetailAdapter;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * Created by 1017sjg on 2017. 6. 24..
 */

@SuppressLint("UseSparseArrays")
public class ShopMenuDetailFragment extends BaseFragment {

    private ShopMenuDetailBinding shopMenuDetailBinding;
    private ShopMenuDetail shopMenuDetail;

    public static ShopMenuDetailFragment newInstance(ShopMenuDetail detail) {
        detail.setCount(1);
        detail.setPayPrice(detail.getPrice());
        Bundle args = new Bundle();
        ShopMenuDetailFragment fragment = new ShopMenuDetailFragment();
        fragment.shopMenuDetail = detail;
        String menuName = detail.getName().replaceAll("(\r\n|\n)", "\n");
        detail.setName(menuName);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        shopMenuDetailBinding = DataBindingUtil.inflate(inflater, R.layout.shop_menu_detail, container, false);
        shopMenuDetailBinding.setOnClick(this);
        shopMenuDetailBinding.setMenuDetail(shopMenuDetail);
        shopMenuDetailBinding.setDetailAdapter(new ShopMenuDetailAdapter(shopMenuDetail, getContext()));
        shopMenuDetailBinding.setEmptyImg(R.drawable.img_no_menu_pic);

        MDEBUG.debug("here + " +  shopMenuDetail.serveType);
        if (shopMenuDetail.image == null) {
            MDEBUG.debug("here");
            shopMenuDetailBinding.menuAppbar.setExpanded(false);
        }
        return shopMenuDetailBinding.getRoot();
    }

    public ArrayList<ShopOptionMenu> getOptionMenuArray() {
        return shopMenuDetailBinding.getDetailAdapter().getObservableArrayList();
    }

    public ShopMenuDetail getShopMenuDetail() {
        return shopMenuDetailBinding.getMenuDetail();
    }
}
