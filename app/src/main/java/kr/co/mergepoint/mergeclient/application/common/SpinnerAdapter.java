package kr.co.mergepoint.mergeclient.application.common;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Arrays;

import kr.co.mergepoint.mergeclient.BR;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;

import static kr.co.mergepoint.mergeclient.application.common.Properties.CENTER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.STREET;

/**
 * Created by 1017sjg on 2017. 7. 19..
 */

public class SpinnerAdapter<T extends ViewDataBinding, S extends ViewDataBinding> extends BaseAdapter {

    protected ArrayList<String> strings;
    private int normalLayout;
    private int dropLayout;

    public SpinnerAdapter(ArrayList<Coupon> coupons, @Properties.Gravity int gravity) {
        this.strings = new ArrayList<>();
        this.strings.add("쿠폰을 선택하세요.");
        for (Coupon coupon: coupons)
            strings.add(coupon.couponName);

        setInitLayout(gravity);
    }

    public SpinnerAdapter(String[] strings, @Properties.Gravity int gravity) {
        this.strings = new ArrayList<>(Arrays.asList(strings));
        setInitLayout(gravity);
    }

    private void setInitLayout(@Properties.Gravity int gravity) {
        switch (gravity) {
            case LEFT:
                normalLayout = R.layout.spinner_normal_left;
                dropLayout = R.layout.spinner_dropdown_left;
                break;
            case CENTER:
                normalLayout = R.layout.spinner_normal_center;
                dropLayout = R.layout.spinner_dropdown_center;
                break;
            case STREET:
                normalLayout = R.layout.spinner_normal_street;
                dropLayout = R.layout.spinner_dropdown_street;
                break;
        }
    }

    @Override
    public int getCount() {
        return strings.size();
    }

    @Override
    public Object getItem(int position) {
        return strings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("unchecked")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        T normalBinding;
        if (convertView == null) {
            View view = LayoutInflater.from(parent.getContext()).inflate(normalLayout, parent, false);
            normalBinding = DataBindingUtil.bind(view);
            convertView = normalBinding.getRoot();
            convertView.setTag(normalBinding);
        } else {
            normalBinding = (T) convertView.getTag();
        }

        normalBinding.setVariable(BR.spinnerTitle, strings.get(position));

        return convertView;
    }

    @SuppressWarnings("unchecked")
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        S dropdownBinding;
        if (convertView == null) {
            View view = LayoutInflater.from(parent.getContext()).inflate(dropLayout, parent, false);
            dropdownBinding = DataBindingUtil.bind(view);
            convertView = dropdownBinding.getRoot();
            convertView.setTag(dropdownBinding);
        } else {
            dropdownBinding = (S) convertView.getTag();
        }

        dropdownBinding.setVariable(BR.spinnerTitle, strings.get(position));
        dropdownBinding.setVariable(BR.parentWidth, parent.getWidth());
        return convertView;
    }
}
