package kr.co.mergepoint.mergeclient.application.common.mvvm;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import kr.co.mergepoint.mergeclient.application.common.mvvm.dagger.ActivityComponent;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public abstract class ViewPagerViewModel extends ViewModel {

    protected abstract PagerAdapter getAdapter();

    public ViewPagerViewModel(@Nullable State savedInstanceState, @NonNull ActivityComponent component) {
        super(savedInstanceState, component);
    }

    public final void setupPagerView(ViewPager viewPager) {
        viewPager.setAdapter(getAdapter());
    }
}
