package kr.co.mergepoint.mergeclient.scene.data.history;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 7. 29..
 */

public class CompanyPointHistory implements Parcelable {

    @SerializedName("type")
    public int type;

    @SerializedName("reason")
    public String reason;

    @SerializedName("point")
    public int point;

    @SerializedName("givenDateTime")
    public ArrayList<Integer> givenDateTime;

    @SerializedName("ticketPrice")
    public long tickePrice;

    @SerializedName("remainTicketPrice")
    public long remainTicketPrice;


    private CompanyPointHistory(Parcel in) {
        type = in.readInt();
        reason = in.readString();
        point = in.readInt();
        givenDateTime = new ArrayList<>();
        in.readList(givenDateTime, Integer.class.getClassLoader());
    }

    public static final Creator<CompanyPointHistory> CREATOR = new Creator<CompanyPointHistory>() {
        @Override
        public CompanyPointHistory createFromParcel(Parcel in) {
            return new CompanyPointHistory(in);
        }

        @Override
        public CompanyPointHistory[] newArray(int size) {
            return new CompanyPointHistory[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(type);
        dest.writeString(reason);
        dest.writeInt(point);
        dest.writeList(givenDateTime);
    }
}

