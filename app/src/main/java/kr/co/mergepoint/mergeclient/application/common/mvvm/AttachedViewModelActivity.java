package kr.co.mergepoint.mergeclient.application.common.mvvm;

import android.content.Intent;

import java.lang.ref.WeakReference;
import java.net.URISyntaxException;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public class AttachedViewModelActivity implements AttachedActivity {

    private final WeakReference<ViewModelActivity> weakActivity;

    public AttachedViewModelActivity(ViewModelActivity weakActivity) {
        this.weakActivity = new WeakReference<>(weakActivity);
    }

    @Override
    public void startActivity(Class<? extends BaseActivity> activityClass) {
        ViewModelActivity activity = weakActivity.get();
        if (activity != null && !activity.isFinishing())
            activity.startActivity(new Intent(activity, activityClass));
    }

    @Override
    public void openUrl(String url) throws URISyntaxException {
        ViewModelActivity activity = weakActivity.get();
        if (activity != null && !activity.isFinishing())
            activity.startActivity(Intent.parseUri(url, 0));
    }

    @Override
    public ViewModelActivity getActivity() {
        ViewModelActivity activity = weakActivity.get();
        return (activity != null && !activity.isFinishing()) ? activity : null;
    }

    @Override
    public void openFragment(int parentLayout, ViewModelFragment fragment, String tag) {
        ViewModelActivity activity = weakActivity.get();

        activity.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_from_right, R.anim.slide_to_left, R.anim.slide_from_left, R.anim.slide_to_right)
                .add(parentLayout, fragment, tag)
                .addToBackStack(null)
                .commit();
    }
}
