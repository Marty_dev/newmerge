package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.databinding.DataBindingUtil;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.application.common.DateModel;
import kr.co.mergepoint.mergeclient.databinding.CrowdPayHistoryListItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPayItem;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class CrowdHistoryAdapter extends BasicListAdapter<BasicListHolder<CrowdPayHistoryListItemBinding, CrowdPayItem>, CrowdPayItem> {

    private int type;

    public CrowdHistoryAdapter(int type, ArrayList<CrowdPayItem> arrayList) {
        super(arrayList);
        this.type = type;
    }

    @Override
    public BasicListHolder<CrowdPayHistoryListItemBinding, CrowdPayItem> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.crowd_pay_history_list_item, parent, false);
        CrowdPayHistoryListItemBinding pointListItemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<CrowdPayHistoryListItemBinding, CrowdPayItem>(pointListItemBinding) {
            @Override
            public void setDataBindingWithData(CrowdPayItem data) {
                getDataBinding().setIsPaymentTab(type == 1);
                getDataBinding().setCrowdItem(data);
                getDataBinding().menuText.setText(TextUtils.join(",", data.menuList));

                String date;
                DateModel dateModel = new DateModel();
                date = (dateModel.getStringDateWithSeconds(type == 1 ? data.payDateTime : data.useDateTime));
                getDataBinding().dateText.setText(date);
                getDataBinding().shopName.setText(data.shopName);
                getDataBinding().crowdCountText.setText(data.attendeeCount + "명 결제");
            }
        };
    }

    public void setParams(int type , ArrayList<CrowdPayItem> arrayList){
        this.type = type;
        if (arrayList == null)
            return;

        this.getObservableArrayList().clear();
        this.getObservableArrayList().addAll(arrayList);
    }
    @Override
    public void setBindViewHolder(BasicListHolder<CrowdPayHistoryListItemBinding, CrowdPayItem> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
