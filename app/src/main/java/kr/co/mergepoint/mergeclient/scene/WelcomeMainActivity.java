package kr.co.mergepoint.mergeclient.scene;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.databinding.WelcomeMainBinding;
import kr.co.mergepoint.mergeclient.scene.login.LoginActivity;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;

/**
 * Created by jgson on 2017. 6. 7..
 */

public class WelcomeMainActivity extends BaseActivity {

    private WelcomeMainBinding binding;

    @Override
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.welcome_main);
        binding.welcomLogin.setOnClickListener(this);
        binding.welcomNonMember.setOnClickListener(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        pageAnim();
    }

    public void pageAnim() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int translationDuration = getResources().getInteger(R.integer.welcome_logo_translation_anim);
        final int alphaDuration = getResources().getInteger(R.integer.welcome_login_alpha_anim);

        binding.logo.animate().translationY(-(height / 2 - height / 4)).setDuration(translationDuration).withLayer().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                binding.welcomBottomLayout.animate().alpha(1f).setDuration(alphaDuration).withLayer();
            }
        });
    }

    @Override
    protected void onActivityClick(View view) {
        switch (view.getId()) {
            case R.id.welcom_login:
                Intent login = new Intent(this, LoginActivity.class);
                startActivity(login);
                break;
            case R.id.welcom_non_member:
                Intent main = new Intent(this, MainActivity.class);
                main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(main);
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        new MergeDialog.Builder(this).setContent(R.string.app_finish_text)
                .setConfirmClick(WelcomeMainActivity.super::onBackPressed).build().show();
    }
}
