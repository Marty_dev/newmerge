package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.CrowdPayHistoryDetailBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.CrowdPayDetailPagerAdapter;

/**
 * Created by jgson on 2017. 7. 18..
 */

public class CrowdPayHistoryDetailFragment extends BaseFragment {

    private ShopPayment shopPayment;
    private CrowdPayDetailPagerAdapter adapter;

    public static CrowdPayHistoryDetailFragment newInstance(ShopPayment shopPayment) {
        Bundle args = new Bundle();
        CrowdPayHistoryDetailFragment fragment = new CrowdPayHistoryDetailFragment();
        fragment.shopPayment = shopPayment;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        adapter  = new CrowdPayDetailPagerAdapter(getContext(), shopPayment);
        CrowdPayHistoryDetailBinding detailBinding = DataBindingUtil.inflate(inflater, R.layout.crowd_pay_history_detail, container, false);
        detailBinding.crowdHistoryDetailTab.setupWithViewPager(detailBinding.pointDetailPager);
        detailBinding.setDetailPagerListener(new TabLayout.TabLayoutOnPageChangeListener(detailBinding.crowdHistoryDetailTab));
        detailBinding.setTitle("함께결제 사용내역");
        detailBinding.setOnClick(this);
        detailBinding.setPagerAdapter(adapter);
        return detailBinding.getRoot();
    }
}
