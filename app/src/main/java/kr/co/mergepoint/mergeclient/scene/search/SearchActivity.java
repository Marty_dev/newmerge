package kr.co.mergepoint.mergeclient.scene.search;

import android.annotation.TargetApi;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import javax.inject.Inject;

import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocationListener;
import kr.co.mergepoint.mergeclient.scene.search.dagger.DaggerSearchComponent;
import kr.co.mergepoint.mergeclient.scene.search.dagger.SearchModule;
import kr.co.mergepoint.mergeclient.scene.search.presenter.SearchPresenter;

/**
 * Created by 1017sjg on 2017. 6. 13..
 */

public class SearchActivity extends BaseActivity implements View.OnClickListener, UserLocationListener {

    @Inject SearchPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerSearchComponent.builder().searchModule(new SearchModule(this)).build().inject(this);
        presenter.onCreate();
    }

    @Override
    protected void onActivityClick(View view) {
        presenter.onClick(view);
    }

    @Override
    public void requestPermissions() {
        presenter.requestPermissions();
    }

    @Override
    public void startLoadList(Location location) {
        presenter.startLoadList(location);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    /* 권한 */
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}