package kr.co.mergepoint.mergeclient.scene.shop.view.listener;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public interface RadioObserver {
    void onChangeRadio(int selectMenuOid);
}
