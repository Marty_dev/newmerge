package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.co.mergepoint.mergeclient.scene.data.main.CombineImagesDetail;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

/**
 * Created by 1017sjg on 2017. 9. 14..
 */

public class CombineImages {

    private Context context;

    public CombineImages(Context context) {
        this.context = context;
    }

    public void addCombineImages(ArrayList<CombineImagesDetail> combineImagesDetails, ConstraintLayout constraintLayout, OnTypeListener typeListener, int width) {
        int preDraweeView = NO_VALUE;

        if (combineImagesDetails != null) {
            ConstraintSet set = new ConstraintSet();
            set.clone(constraintLayout);

            for (CombineImagesDetail detail : combineImagesDetails) {
                SimpleDraweeView simpleDraweeView = new SimpleDraweeView(context);
                simpleDraweeView.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                simpleDraweeView.setId(View.generateViewId());
                addUrl(detail, constraintLayout, simpleDraweeView, typeListener, width);
                constraintLayout.addView(simpleDraweeView);

                if (combineImagesDetails.indexOf(detail) == 0) {
                    set.connect(simpleDraweeView.getId(), ConstraintSet.TOP, constraintLayout.getId(), ConstraintSet.TOP);
                } else {
                    set.connect(simpleDraweeView.getId(), ConstraintSet.TOP, preDraweeView, ConstraintSet.BOTTOM);
                }
                set.connect(simpleDraweeView.getId(), ConstraintSet.START, constraintLayout.getId(), ConstraintSet.START);
                set.connect(simpleDraweeView.getId(), ConstraintSet.END, constraintLayout.getId(), ConstraintSet.END);
                set.constrainHeight(simpleDraweeView.getId(), ConstraintSet.WRAP_CONTENT);
                set.constrainWidth(simpleDraweeView.getId(), ConstraintSet.WRAP_CONTENT);
                preDraweeView = simpleDraweeView.getId();
            }

            set.applyTo(constraintLayout);
        }
    }

    private void addCombineButtons(CombineImagesDetail detail, ConstraintLayout constraintLayout, OnTypeListener typeListener, float ratio) {
        ConstraintSet set = new ConstraintSet();
        set.clone(constraintLayout);

        TypeButton linkBtn = new TypeButton(context);
        linkBtn.setId(View.generateViewId());
        linkBtn.setTypeListener(typeListener);
        linkBtn.setTypeWithData(detail.actionType, detail.actionData);
        linkBtn.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linkBtn.setBackgroundResource(android.R.color.transparent);
        set.centerHorizontally(linkBtn.getId(), constraintLayout.getId());
        set.connect(linkBtn.getId(), ConstraintSet.TOP, constraintLayout.getId(), ConstraintSet.TOP, (int) (detail.buttonLocation * ratio));
        set.constrainWidth(linkBtn.getId(), (int) (detail.buttonWidth * ratio));
        set.constrainHeight(linkBtn.getId(), (int) (detail.buttonHeight * ratio));
        constraintLayout.addView(linkBtn);

        set.applyTo(constraintLayout);
    }

    private void addUrl(final CombineImagesDetail detail, final ConstraintLayout constraintLayout, final SimpleDraweeView simpleDraweeView, final OnTypeListener typeListener, final int width) {
        Matcher matcher = Pattern.compile(".*/(.*)").matcher(detail.bodyImage);

        if (matcher.find()) {
            String imgName = matcher.group(1);
            if (imgName.matches(".*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*")) {
                // 이미지 이름이 한글일 때
                String encodeImageName = null;

                try {
                    encodeImageName = URLEncoder.encode(imgName, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                if (encodeImageName != null)
                    detail.bodyImage = detail.bodyImage.replace(imgName, encodeImageName);
            }
        }

        GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(context.getResources());
        GenericDraweeHierarchy hierarchy = builder.setActualImageScaleType(ScalingUtils.ScaleType.FIT_XY).build();

        ControllerListener<ImageInfo> controllerListener = new BaseControllerListener<ImageInfo>() {
            @Override
            public void onFinalImageSet(String id, @Nullable ImageInfo imageInfo, @Nullable Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                if(imageInfo != null){
                    float wRatio = (float) width / (float) imageInfo.getWidth();
                    simpleDraweeView.getLayoutParams().width = (int) ((float) imageInfo.getWidth() * wRatio);
                    simpleDraweeView.getLayoutParams().height = (int) ((float) imageInfo.getHeight() * wRatio);
                    simpleDraweeView.requestLayout();
                    if (detail.buttonWidth != 0)
                        addCombineButtons(detail, constraintLayout, typeListener, wRatio);
                }
            }
        };

        ImageRequestBuilder imageRequestBuilder = ImageRequestBuilder.newBuilderWithSource(Uri.parse(Properties.BASE_URL + detail.bodyImage));

        DraweeController draweeController = Fresco.newDraweeControllerBuilder()
                .setImageRequest(imageRequestBuilder.build())
                .setControllerListener(controllerListener)
                .setOldController(simpleDraweeView.getController())
                .setAutoPlayAnimations(true)
                .build();

        simpleDraweeView.setHierarchy(hierarchy);
        simpleDraweeView.setController(draweeController);
    }
}
