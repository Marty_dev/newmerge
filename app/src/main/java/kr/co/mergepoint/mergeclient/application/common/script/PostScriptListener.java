package kr.co.mergepoint.mergeclient.application.common.script;

/**
 * Created by 1017sjg on 2017. 6. 16..
 */

public interface PostScriptListener {
    void setAddress(final String add01, final String add02, final String add03);
}
