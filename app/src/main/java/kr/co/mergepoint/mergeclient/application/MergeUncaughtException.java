package kr.co.mergepoint.mergeclient.application;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;

import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.scene.splash.SplashActivity;

import static kr.co.mergepoint.mergeclient.application.common.Properties.UNCAUGHT_EXCEPTION_REQUEST;

/**
 * Created by 1017sjg on 2017. 9. 29..
 */

public class MergeUncaughtException implements Thread.UncaughtExceptionHandler {

    private BaseActivity activity;

    public MergeUncaughtException(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public void uncaughtException(Thread thread, final Throwable throwable) {
        new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                if (activity != null) {
                    MergeDialog.Builder builder = new MergeDialog.Builder(activity);
                    builder.setContent("일시적인 오류입니다.\n앱을 다시 실행해주세요.");
                    builder.setConfirmString("다시 실행");
                    builder.setCancelBtn(false);
                    builder.setConfirmClick(() -> restartApp());
                    builder.build().show();
                }
                Looper.loop();
            }
        }.start();
    }

    private void restartApp() {
        Intent restartIntent = new Intent(activity, SplashActivity.class);
        PendingIntent runner = PendingIntent.getActivity(activity, UNCAUGHT_EXCEPTION_REQUEST, restartIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager am = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        if (am != null)
            am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1000, runner);

        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(10);
    }
}
