package kr.co.mergepoint.mergeclient.scene.data.main;

import android.content.res.Resources;
import android.support.annotation.IntDef;

import java.util.Objects;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.Properties;

import static kr.co.mergepoint.mergeclient.application.common.Properties.ONE_MONTH;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ORDER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAYMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SIX_MONTH;
import static kr.co.mergepoint.mergeclient.application.common.Properties.STORE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.THREE_MONTH;

/**
 * Created by 1017sjg on 2017. 9. 8..
 */

public class UsageFilter {

    private String tabType;
    private int listType;
    private int term;

    @Properties.Terms
    public int getTerm() {
        return term;
    }

    public void setTerm(@Properties.Terms int term) {
        this.term = term;
    }

    @Properties.ListType
    public int getListType() {
        return listType;
    }

    public void setListType(@Properties.ListType int listType) {
        this.listType = listType;
    }

    @Properties.TabType
    public String getTabType() {
        return tabType;
    }

    public void setTabType(@Properties.TabType String tabType) {
        this.tabType = tabType;
    }

    public UsageFilter() {
        setTabType(PAYMENT);
        setTerm(ONE_MONTH);
        setListType(ORDER);
    }

    public void setTermOrType(String tag, int position) {
        Resources resources = MergeApplication.getMergeAppComponent().getUtility().getResuorces();
        String TERM_TAG = resources.getString(R.string.usage_term_spinner_tag);
        String TYPE_TAG = resources.getString(R.string.usage_type_spinner_tag);
        if (tag.equals(TERM_TAG)) {
            setTerm(position == 0 ? ONE_MONTH : (position == 1 ? THREE_MONTH : SIX_MONTH));     // 기간 스피너 리스너
        } else if (tag.equals(TYPE_TAG)) {
            setListType(position == ORDER ? ORDER : STORE);     // 구분 스피너 리스너
        }
    }

    public boolean isOrderList() {
        return getListType() == ORDER;
    }

    public boolean isPaymentTab() {
        return Objects.equals(getTabType(), PAYMENT);
    }

    public boolean isPayOrder() {
        return Objects.equals(getTabType(), PAYMENT) && getListType() == ORDER;
    }
}
