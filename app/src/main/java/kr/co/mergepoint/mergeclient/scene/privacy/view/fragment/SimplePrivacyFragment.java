package kr.co.mergepoint.mergeclient.scene.privacy.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.SimplePrivacyBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;

import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_ROLE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.REGISTER_APPROVAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.REGISTER_WAIT;

/**
 * Created by 1017sjg on 2017. 8. 3..
 */

public class SimplePrivacyFragment extends BaseFragment {

    private Member member;

    public static SimplePrivacyFragment newInstance(Member member) {
        Bundle args = new Bundle();
        SimplePrivacyFragment fragment = new SimplePrivacyFragment();
        fragment.member = member;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SimplePrivacyBinding simplePrivacyBinding = DataBindingUtil.inflate(inflater, R.layout.simple_privacy, container, false);
        simplePrivacyBinding.setTitle(getString(R.string.privacy_title));
        simplePrivacyBinding.setOnClick(this);
        simplePrivacyBinding.setMember(member);

        if (member.getRoles().contains(ENTERPRISE_ROLE)) {
            /*기업 회원*/
            simplePrivacyBinding.enterpriseHeaderTitle.setText("나의 기업 정보");
            simplePrivacyBinding.defaultEnterpriseRegister.setVisibility(View.GONE);
        } else {
            if (member.getCompanyRef() != 0) {
                /*기업 회원 신청 중*/
                simplePrivacyBinding.enterpriseHeaderTitle.setText("우리 기업이 머지포인트 식권을 사용한다면?");
                simplePrivacyBinding.searchEnterprise.setClickable(false);
                simplePrivacyBinding.searchEnterprise.setEnabled(false);
                simplePrivacyBinding.searchEnterprise.setText(fromHtml(getString(R.string.enterprise_wait, member.getCompanyName())));
            } else {
                /*기본 상태*/
                simplePrivacyBinding.enterpriseHeaderTitle.setText("우리 기업이 머지포인트 식권을 사용한다면?");
                simplePrivacyBinding.searchEnterprise.setText(fromHtml(getString(R.string.enterprise_register)));
            }
        }

        return simplePrivacyBinding.getRoot();
    }

    private Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
            return Html.fromHtml(source);
        return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
    }
}
