package kr.co.mergepoint.mergeclient.application.common;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by 1017sjg on 2017. 8. 17..
 */

public class DateModel {

    public DateModel() {}

    public Date getDate(ArrayList<Integer> dateArray) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.set(0,0,0,0,0,0);
        for (int i = 0; i < dateArray.size(); i++) {
            int dateInt = dateArray.get(i);
            switch (i) {
                case 0:
                    calendar.set(Calendar.YEAR, dateInt);
                    break;
                case 1:
                    calendar.set(Calendar.MONTH, dateInt - 1);
                    break;
                case 2:
                    calendar.set(Calendar.DATE, dateInt);
                    break;
                case 3:
                    calendar.set(Calendar.HOUR_OF_DAY, dateInt);
                    break;
                case 4:
                    calendar.set(Calendar.MINUTE, dateInt);
                    break;
                case 5:
                    calendar.set(Calendar.SECOND, dateInt);
                    break;
                default:
                    break;
            }
        }
        date.setTime(calendar.getTime().getTime());
        return date;
    }

    public Date getCalendarDate(ArrayList<Integer> dateArray) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.set(0,0,0,0,0,0);
        for (int i = 0; i < dateArray.size(); i++) {
            int dateInt = dateArray.get(i);
            switch (i) {
                case 0:
                    calendar.set(Calendar.YEAR, dateInt);
                    break;
                case 1:
                    calendar.set(Calendar.MONTH, dateInt - 1);
                    break;
                case 2:
                    calendar.set(Calendar.DATE, dateInt);
                    break;
                default:
                    break;
            }
        }
        date.setTime(calendar.getTime().getTime());
        return date;
    }
    public Date getCalendarAllDate(ArrayList<Integer> dateArray) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.set(0,0,0,0,0,0);
        for (int i = 0; i < dateArray.size(); i++) {
            int dateInt = dateArray.get(i);
            switch (i) {
                case 0:
                    calendar.set(Calendar.YEAR, dateInt);
                    break;
                case 1:
                    calendar.set(Calendar.MONTH, dateInt - 1);
                    break;
                case 2:
                    calendar.set(Calendar.DATE, dateInt);
                    break;
                case 3:
                    calendar.set(Calendar.HOUR_OF_DAY, dateInt);
                    break;
                case 4:
                    calendar.set(Calendar.MINUTE, dateInt);
                    break;
                case 5:
                    calendar.set(Calendar.SECOND, dateInt);
                    break;
                default:
                    break;
            }
        }
        date.setTime(calendar.getTime().getTime());
        return date;
    }

    public Date getCurrentDate() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        date.setTime(calendar.getTime().getTime());

        return date;
    }

//    public boolean getCompareDateOfDays(Date startDate) {
//        Calendar startCalendar = Calendar.getInstance();
//        startCalendar.setTime(startDate);
//        startCalendar.add(Calendar.DAY_OF_MONTH, 7);
//
//        Calendar currentCalendar = Calendar.getInstance();
//        currentCalendar.add(Calendar.DAY_OF_MONTH, 1);
//
//        Date sDate = new Date(startCalendar.getTime().getTime());
//        Date cDate = new Date(currentCalendar.getTime().getTime());
//
//        return !cDate.after(sDate);
//    }

    public boolean getCompareDateOfDays(int period, Date startDate) {
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(startDate);
        startCalendar.set(Calendar.HOUR_OF_DAY, 0);
        startCalendar.set(Calendar.MINUTE, 0);
        startCalendar.set(Calendar.SECOND, 0);
        startCalendar.set(Calendar.MILLISECOND, 0);
        startCalendar.add(Calendar.DAY_OF_MONTH, period);

        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.set(Calendar.HOUR_OF_DAY, 0);
        currentCalendar.set(Calendar.MINUTE, 0);
        currentCalendar.set(Calendar.SECOND, 0);
        currentCalendar.set(Calendar.MILLISECOND, 0);

        Date sDate = new Date(startCalendar.getTime().getTime());
        Date cDate = new Date(currentCalendar.getTime().getTime());

        return cDate.before(sDate);
    }

    @SuppressLint("SimpleDateFormat")
    String getStringDate(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    @SuppressLint("SimpleDateFormat")
    String getStringDateWithSeconds(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }
    @SuppressLint("SimpleDateFormat")
    public String getSimpleDate(Date date) {
        return new SimpleDateFormat("yyyy.MM.dd").format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public String getStringCalendarDate(Date date) {
        return new SimpleDateFormat("yyyy년 MM월 dd일").format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public String getStringDateWithSeconds(ArrayList<Integer> dateArray) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(getDate(dateArray));
    }

    public int getCalculateDay(ArrayList<Integer> dateArray) {
        Date date = getDate(dateArray);
        return getCalculateDay(date);
    }

    public int getCalculateDay(Date date) {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        Calendar target = Calendar.getInstance();
        target.setTime(date);
        target.set(Calendar.HOUR_OF_DAY, 0);
        target.set(Calendar.MINUTE, 0);
        target.set(Calendar.SECOND, 0);
        target.set(Calendar.MILLISECOND, 0);

        long todayTime = today.getTimeInMillis() / (24 * 60 * 60 * 1000);
        long targetTime = target.getTimeInMillis() / (24 * 60 * 60 * 1000);
        long dday = targetTime - todayTime;

        return (int) dday;
    }
}
