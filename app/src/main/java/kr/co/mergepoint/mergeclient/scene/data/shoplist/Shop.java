package kr.co.mergepoint.mergeclient.scene.data.shoplist;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.BR;

/**
 * Created by 1017sjg on 2017. 6. 13..
 */

public class Shop extends BaseObservable implements Parcelable {

    @SerializedName("oid")
    public int oid;

    @SerializedName("type")
    public int type;

    @SerializedName("shopName")
    public String shopName;

    @SerializedName("shopPhone")
    public String shopPhone;

    @SerializedName("categoryCodeRef")
    public ArrayList<String> categoryCodeRef;

    @SerializedName("postcode")
    public int postcode;

    @SerializedName("address01")
    public String address01;

    @SerializedName("address02")
    public String address02;

    @SerializedName("averageAppr")
    public float averageAppr;

    @SerializedName("themeCodeRef")
    public ArrayList<String> themeCodeRef;

    @SerializedName("liked")
    public int liked;

    @SerializedName("displayHour")
    public String displayHour;

    @SerializedName("likeCount")
    public int likeCount;

    @SerializedName("favorite")
    public boolean favorite;

    @SerializedName("favoriteCount")
    public int favoriteCount;

    @SerializedName("backImage")
    public String backImage;

    @SerializedName("latitude")
    public double latitude;

    @SerializedName("longitude")
    public double longitude;

    @SerializedName("distance")
    public int distance;

    @SerializedName("paid")
    public int paid;

    @SerializedName("shopImage")
    public ArrayList<String> shopImage;

    @SerializedName("businessHours")
    public String businessHours;

    public Shop(int oid, boolean favorite) {
        this.oid = oid;
        this.favorite = favorite;
    }

    private Shop(Parcel in) {
        oid = in.readInt();
        shopName = in.readString();
        shopPhone = in.readString();
        in.readStringList(categoryCodeRef == null ? new ArrayList<String>() : categoryCodeRef);
        postcode = in.readInt();
        address01 = in.readString();
        address02 = in.readString();
        averageAppr = in.readFloat();
        in.readStringList(themeCodeRef == null ? new ArrayList<String>() : themeCodeRef);
        liked = in.readInt();
        likeCount = in.readInt();
        favorite = in.readInt() == 1;
        backImage = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        distance = in.readInt();
        paid = in.readInt();
    }

    public static final Creator<Shop> CREATOR = new Creator<Shop>() {
        @Override
        public Shop createFromParcel(Parcel in) {
            return new Shop(in);
        }

        @Override
        public Shop[] newArray(int size) {
            return new Shop[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(oid);
        dest.writeString(shopName);
        dest.writeString(shopPhone);
        dest.writeStringList(categoryCodeRef);
        dest.writeInt(postcode);
        dest.writeString(address01);
        dest.writeString(address02);
        dest.writeFloat(averageAppr);
        dest.writeStringList(themeCodeRef);
        dest.writeInt(liked);
        dest.writeInt(likeCount);
        dest.writeInt(favorite ? 1 : 0);
        dest.writeString(backImage);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeInt(distance);
        dest.writeInt(paid);
    }

    @Bindable
    public int getLiked() {
        return liked;
    }

    public void setLiked(int liked) {
        this.liked = liked;
        notifyPropertyChanged(BR.liked);
    }

    @Bindable
    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
        notifyPropertyChanged(BR.likeCount);
    }

    @Bindable
    public int getFavoriteCount() {
        return favoriteCount;
    }

    public void setFavoriteCount(int favoriteCount) {
        this.favoriteCount = favoriteCount;
        notifyPropertyChanged(BR.favoriteCount);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Shop)) return false;

        Shop shop = (Shop) obj;
        return Objects.equals(oid, shop.oid);
    }
}
