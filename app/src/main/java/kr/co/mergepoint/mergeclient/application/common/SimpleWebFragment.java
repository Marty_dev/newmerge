package kr.co.mergepoint.mergeclient.application.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.webkit.WebChromeClient;
import android.webkit.WebViewClient;

/**
 * Created by 1017sjg on 2017. 6. 16..
 */

public class SimpleWebFragment extends SingleWebFragment {

    private String title;
    private CustomWebClient webClient;
    private CustomChromeClient chromeClient;

    public static SimpleWebFragment newInstance(Bundle bundle, String title) {
        SimpleWebFragment fragment = new SimpleWebFragment();
        fragment.title = title;
        fragment.chromeClient = new CustomChromeClient();
        fragment.webClient = new CustomWebClient();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected WebChromeClient setWebChromeClient(ContentLoadingProgressBar loadProgressBar) {
        chromeClient.setContentLoadingProgressBar(loadProgressBar);
        return chromeClient;
    }

    @Override
    protected WebViewClient setWebViewClient() {
        return webClient;
    }

    @Override
    protected Object setJavascript() {
        return null;
    }

    @Override
    protected String setPageTitle() {
        return title;
    }
}
