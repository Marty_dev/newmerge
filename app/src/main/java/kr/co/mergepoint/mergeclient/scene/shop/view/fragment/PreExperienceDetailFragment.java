package kr.co.mergepoint.mergeclient.scene.shop.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.ShopDetailPreExperienceDetailBinding;
import kr.co.mergepoint.mergeclient.scene.data.shop.PreExperience;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.PreExperiencePagerAdapter;

/**
 * Created by jgson on 2017. 12. 7..
 */

public class PreExperienceDetailFragment extends BaseFragment {

    private String shopName;

    public static PreExperienceDetailFragment newInstance(ArrayList<PreExperience> preExperiences, int select, String shopName) {
        Bundle args = new Bundle();
        args.putParcelableArrayList("PRE_EXPERIENCE", preExperiences);
        args.putInt("SELECT", select);
        PreExperienceDetailFragment fragment = new PreExperienceDetailFragment();
        fragment.shopName = shopName;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postponeEnterTransition();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
        }
        setSharedElementReturnTransition(null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ShopDetailPreExperienceDetailBinding binding = DataBindingUtil.findBinding(view);
        int currentItem = getArguments().getInt("SELECT");
        ArrayList<PreExperience> preExperiences = getArguments().getParcelableArrayList("PRE_EXPERIENCE");
        binding.setImageAdapter(new PreExperiencePagerAdapter(preExperiences));
        binding.preExperiencePager.post(() -> binding.preExperiencePager.setCurrentItem(currentItem, false));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ShopDetailPreExperienceDetailBinding detailBinding = DataBindingUtil.inflate(inflater, R.layout.shop_detail_pre_experience_detail, container, false);
        detailBinding.setOnClick(getRootActivity());
        detailBinding.setShopName(shopName);
        return detailBinding.getRoot();
    }
}
