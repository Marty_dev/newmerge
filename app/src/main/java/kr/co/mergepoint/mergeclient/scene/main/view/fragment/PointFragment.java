package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.UserApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.RecyclerPagerAdapter;
import kr.co.mergepoint.mergeclient.application.common.SpinnerAdapter;
import kr.co.mergepoint.mergeclient.databinding.PointBinding;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.main.UserPoints;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.PointAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.CENTER;

/**
 * Created by jgson on 2017. 7. 18..
 */

public class PointFragment extends BaseFragment implements AdapterView.OnItemSelectedListener, TabLayout.OnTabSelectedListener{

    private int type;
    private int period;

    private PointBinding pointBinding;
    private PointAdapter adapter;

    public static PointFragment newInstance() {
        Bundle args = new Bundle();
        PointFragment fragment = new PointFragment();
        fragment.type = 0;
        fragment.period = 1;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        adapter = new PointAdapter();

        pointBinding = DataBindingUtil.inflate(inflater, R.layout.point, container, false);
        pointBinding.pointTab.setupWithViewPager(pointBinding.pointDetailPager);
        pointBinding.setDetailPagerListener(new TabLayout.TabLayoutOnPageChangeListener(pointBinding.pointTab));
        pointBinding.setTitle(getString(R.string.accumulation_points_title));
        pointBinding.setOnClick(this);
        pointBinding.setTabListener(this);
        pointBinding.setSpinnerItemListener(PointFragment.this);
        pointBinding.setPagerAdapter(new RecyclerPagerAdapter(rootActivity, getResources().getStringArray(R.array.point_tab)));
        pointBinding.setSpinnerAdapter(new SpinnerAdapter(getResources().getStringArray(R.array.term_spinner), CENTER));
        return pointBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        getPointList();
    }

    // 포인트 기간 설정에 대한 리스
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        period = 0 == position ? 1 : (3 == position ? 3 : 6);
        getPointList();
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {}

    // 포인트 상단의 탭 리스너
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        type = tab.getPosition();
        getPointList();
    }
    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}
    @Override
    public void onTabReselected(TabLayout.Tab tab) {}

    private void getPointList() {
        MergeApplication.getMergeApplication().showLoading(rootActivity);
        if (preCall != null)
            preCall.cancel();
        UserApi api = MergeApplication.getMergeAppComponent().getRetrofit().create(UserApi.class);
        Call<ResponseObject<UserPoints>> call = api.pointList(type, period);
        call.enqueue(getPointListCallback(type));
        preCall = call;
    }


    private Callback<ResponseObject<UserPoints>> getPointListCallback(final int position) {
        return new Callback<ResponseObject<UserPoints>>() {
            @Override
            public void onResponse(@NonNull Call<ResponseObject<UserPoints>> call, @NonNull Response<ResponseObject<UserPoints>> response) {
                if (response.isSuccessful()) {
                    ResponseObject<UserPoints> responseObject = response.body();
                    if (responseObject != null) {
                        UserPoints points = responseObject.getObject();
                        if (points != null) {
                            adapter.setParams(points.pointsList);
                            pointBinding.getPagerAdapter().setListAdapter(position, adapter);
                            pointBinding.setPoint(points.totalPoints);
                        }
                    }
                }
                delayHideLoading();
            }

            @Override
            public void onFailure(@NonNull Call<ResponseObject<UserPoints>> call, @NonNull Throwable t) {
                if (!call.isCanceled())
                    showAlert(R.string.retry);
                delayHideLoading();
            }
        };
    }
}
