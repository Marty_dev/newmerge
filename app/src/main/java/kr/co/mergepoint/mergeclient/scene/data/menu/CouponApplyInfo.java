package kr.co.mergepoint.mergeclient.scene.data.menu;

import com.google.gson.annotations.SerializedName;

/**
 * User: Marty
 * Date: 2018-06-05
 * Time: 오후 3:00
 * Description:
 */
public class CouponApplyInfo {

    @SerializedName("codeType")
    private int codeType;
    @SerializedName("couponNum")
    private String couponNum;
    @SerializedName("addPoints")
    private int addPoints;
    @SerializedName("newPoints")
    private int newPoints;


    public int getCodeType() {
        return codeType;
    }

    public void setCodeType(int codeType) {
        this.codeType = codeType;
    }

    public String getCouponNum() {
        return couponNum;
    }

    public void setCouponNum(String couponNum) {
        this.couponNum = couponNum;
    }

    public int getAddPoints() {
        return addPoints;
    }

    public void setAddPoints(int addPoints) {
        this.addPoints = addPoints;
    }

    public int getNewPoints() {
        return newPoints;
    }

    public void setNewPoints(int newPoints) {
        this.newPoints = newPoints;
    }
}
