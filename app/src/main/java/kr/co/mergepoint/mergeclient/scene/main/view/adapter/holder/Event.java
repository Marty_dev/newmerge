package kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.scene.data.main.EventInfo;

/**
 * Created by 1017sjg on 2017. 8. 21..
 */

public class Event extends ExpandableGroup<EventInfo> {

    public boolean isOpen;

    public Event(String title, List<EventInfo> items, boolean isOpen) {
        super(title, items);
        this.isOpen = isOpen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;

        Event event = (Event) o;
        return Objects.equals(getTitle(), event.getTitle());
    }
}
