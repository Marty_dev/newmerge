package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;

import kr.co.mergepoint.mergeclient.R;

/**
 * Created by 1017sjg on 2017. 7. 6..
 */

public class BaseCheckButton extends AppCompatCheckBox {

    public BaseCheckButton(Context context) {
        super(context);
        init(context);
    }

    public BaseCheckButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BaseCheckButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        Typeface spoqa = ResourcesCompat.getFont(context, R.font.spoqa);
        setTypeface(spoqa);
    }
}
