package kr.co.mergepoint.mergeclient.application.common;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.databinding.ResultBinding;

/**
 * Created by 1017sjg on 2017. 8. 10..
 */

public class ResultPageFragment extends BaseFragment {

    private boolean state;
    private String title;
    private String messageTitle;
    private String messageContent;
    private String buttonText;

    public static ResultPageFragment newInstance(boolean state, String title, String messageTitle, String messageContent, String buttonText) {
        Bundle args = new Bundle();
        ResultPageFragment fragment = new ResultPageFragment();
        fragment.state = state;
        fragment.title = title;
        fragment.messageTitle = messageTitle;
        fragment.messageContent = messageContent;
        fragment.buttonText = buttonText;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ResultBinding resultBinding = DataBindingUtil.inflate(inflater, R.layout.result, container, false);
        resultBinding.setTitle(title);
        resultBinding.setOnClick(this);
        resultBinding.setState(state);
        resultBinding.setMessageTitle(messageTitle);
        resultBinding.setMessageContent(messageContent);
        resultBinding.setButtonText(buttonText);
        return resultBinding.getRoot();
    }

}
