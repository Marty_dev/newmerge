package kr.co.mergepoint.mergeclient.scene.privacy.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.RecyclerItemListener;
import kr.co.mergepoint.mergeclient.databinding.SearchEnterpriseBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.Company;
import kr.co.mergepoint.mergeclient.scene.privacy.view.adapter.SearchEnterpriseAdapter;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.SearchEnterpriseListener;

/**
 * Created by 1017sjg on 2017. 8. 9..
 */

public class SearchEnterpriseFragment extends BaseFragment implements RecyclerItemListener.RecyclerTouchListener, TextView.OnEditorActionListener {

    private SearchEnterpriseListener listener;
    private SearchEnterpriseBinding settingsBinding;

    public static SearchEnterpriseFragment newInstance(SearchEnterpriseListener listener) {
        Bundle args = new Bundle();
        SearchEnterpriseFragment fragment = new SearchEnterpriseFragment();
        fragment.listener = listener;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        settingsBinding = DataBindingUtil.inflate(inflater, R.layout.search_enterprise, container, false);
        settingsBinding.setTitle("기업회원 가입하기");
        settingsBinding.setOnClick(this);
        settingsBinding.setTouch(this);
        settingsBinding.setSearchListener(this);
        return settingsBinding.getRoot();
    }

    public void setAdapter(SearchEnterpriseAdapter adapter) {
        settingsBinding.setSearchAdapter(adapter);
    }

    @Override
    public void onClickItem(View v, int position) {
        SearchEnterpriseAdapter adapter = settingsBinding.getSearchAdapter();
        if (adapter != null) {
            Company company = adapter.getObservableArrayList().get(position);
            String alertContent = String.format(getActivity().getString(R.string.alert_enterprise_register), company.companyName, company.ownerName);
            new MergeDialog.Builder(getRootActivity()).setHtmlText(Html.fromHtml(alertContent)).setConfirmClick(() -> listener.registerEnterprise(company.oid)).build().show();
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int keyCode, KeyEvent keyEvent) {
        if (keyCode == EditorInfo.IME_ACTION_SEARCH) {
            listener.searchEnterprise(textView.getText().toString());
            return true;
        }
        return false;
    }
}

