package kr.co.mergepoint.mergeclient.scene.shoplist.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.scene.shoplist.ShopListActivity;

/**
 * Created by jgson on 2017. 6. 5..
 */

@ShopListScope
@Component(modules = ShopListModule.class)
public interface ShopListComponent {
    void inject(ShopListActivity activity);
}
