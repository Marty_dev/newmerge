package kr.co.mergepoint.mergeclient.application.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.application.common.Utility;

/**
 * Created by 1017sjg on 2017. 6. 10..
 */

@Module
public class UtilityModule {
    private Context context;

    public UtilityModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Utility provideUtility() { return new Utility(context); }
}
