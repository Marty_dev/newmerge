package kr.co.mergepoint.mergeclient.scene.shoplist.view.listener;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public interface FilterPublisher {
    void add(FilterObserver observer);
    void delete(FilterObserver observer);
    void notifyObserver();
}
