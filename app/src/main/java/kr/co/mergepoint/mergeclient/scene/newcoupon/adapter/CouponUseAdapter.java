package kr.co.mergepoint.mergeclient.scene.newcoupon.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.DateModel;
import kr.co.mergepoint.mergeclient.application.common.MergeBaseAdapter;
import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponUse;
import kr.co.mergepoint.mergeclient.scene.newcoupon.viewholder.CouponUseViewHolder;

/**
 * User: Marty
 * Date: 2018-09-27
 * Time: 오전 10:40
 * Description:
 */
public class CouponUseAdapter extends MergeBaseAdapter<CouponUse,CouponUseViewHolder> {

    public CouponUseAdapter(BaseActivity mCon) {
        super(R.layout.newcoupon_useitem, mCon);
    }

    public void setList(ArrayList<CouponUse> list){
        if (arrayList == null){
            arrayList = new ArrayList<>();
        }
        arrayList.clear();
        arrayList.addAll(list);
        notifyDataSetChanged();
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CouponUseViewHolder viewHolder;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(mLayout, parent, false);

        viewHolder= new CouponUseViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CouponUse item = arrayList.get(position);
        CouponUseViewHolder vh = (CouponUseViewHolder)holder;
        vh.newCouponprice.setText(mCon.MoneyForm(item.getUsePrice()) +"원");
        vh.useShopname.setText(item.getShopName());
        String date =  new SimpleDateFormat("yyyy.MM.dd").format((new DateModel().getCalendarDate(item.getUseDateTime())));
        vh.useDate.setText(date);
    }
}
