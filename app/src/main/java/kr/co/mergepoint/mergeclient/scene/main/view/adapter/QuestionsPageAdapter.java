package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.databinding.QuestionsPagerBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.Question;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder.Questions;

/**
 * Created by 1017sjg on 2017. 6. 19..
 */

public class QuestionsPageAdapter extends PagerAdapter {

    private ArrayList<QuestionsAdapter> questionsAdapters;

    public QuestionsPageAdapter(Context context) {
        questionsAdapters = new ArrayList<>();
        int[] questionTitle = {R.array.order_and_payment_title, R.array.member_and_services_title, R.array.refund_title, R.array.point_coupon_title};
        int[] questionContent = {R.array.order_and_payment, R.array.member_and_services, R.array.refund, R.array.point_coupon};

        for (int i=0; i<4; i++) {
            String[] titleArray = context.getResources().getStringArray(questionTitle[i]);
            String[] contentArray = context.getResources().getStringArray(questionContent[i]);

            ArrayList<Questions> questionsArrayList = new ArrayList<>();
            for (int j=0; j<titleArray.length; j++) {
                ArrayList<Question> questionArrayList = new ArrayList<>();
                questionArrayList.add(new Question(contentArray[j]));
                Questions questions = new Questions(titleArray[j], questionArrayList);
                questionsArrayList.add(questions);
            }
            questionsAdapters.add(new QuestionsAdapter(questionsArrayList));
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        QuestionsPagerBinding questionsPagerBinding = DataBindingUtil.inflate(LayoutInflater.from(container.getContext()), R.layout.questions_pager, container, false);
        questionsPagerBinding.setExpandableAdapter(questionsAdapters.get(position));
        container.addView(questionsPagerBinding.getRoot());

        return questionsPagerBinding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return questionsAdapters.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
