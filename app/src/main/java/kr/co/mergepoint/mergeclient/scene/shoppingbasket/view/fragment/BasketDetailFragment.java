package kr.co.mergepoint.mergeclient.scene.shoppingbasket.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.ShoppingBasketDetailBinding;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.view.adapter.MenuInOptionAdapter;

/**
 * Created by 1017sjg on 2017. 8. 28..
 */

public class BasketDetailFragment extends BaseFragment {

    private ShopMenuDetail menuDetail;

    public static BasketDetailFragment newInstance(View.OnClickListener onClickListener, ShopMenuDetail menuDetail) {
        Bundle args = new Bundle();
        BasketDetailFragment fragment = new BasketDetailFragment();
        fragment.menuDetail = menuDetail;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ShoppingBasketDetailBinding detailBinding = DataBindingUtil.inflate(inflater, R.layout.shopping_basket_detail, container, false);
        detailBinding.setTitle(getString(R.string.basket_detail_title));
        detailBinding.setOnClick(this);
        detailBinding.setMenuDetail(menuDetail);
        detailBinding.setMenuInOptionAdapter(new MenuInOptionAdapter(menuDetail.optionMenu));
        return detailBinding.getRoot();
    }
}
