package kr.co.mergepoint.mergeclient.scene.usagehistory.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;

/**
 * Created by jgson on 2017. 5. 31..
 */

@UsageScope
@Component(modules = UsageModule.class)
public interface UsageComponent {
    void inject(UsageActivity context);
}
