package kr.co.mergepoint.mergeclient.scene.coupon;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.UserApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.menu.CouponApplyInfo;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouponApply extends BaseActivity {



    @Override
    protected void onActivityClick(View view) {
        // Not use
    }

    private UserApi apis;

    private EditText edt;
    private TextView tv;
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_apply);

        ((TextView)(findViewById(R.id.ec_toolbar)).findViewById(R.id.toolbar_textbar)).setText("프로모션 코드 등록");
        ((findViewById(R.id.ec_toolbar)).findViewById(R.id.back)).setOnClickListener((view)->finish());
        edt = (EditText)findViewById(R.id.user_coupon_edt);
        tv = (TextView)findViewById(R.id.user_coupon_tv);
        btn = (Button)findViewById(R.id.user_coupon_acceptbtn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edt.getText().toString().length() == 0){
                    tv.setText("포인트 코드를 입력해주세요.");
                    return;
                }

                MergeApplication.getMergeApplication().showLoading(CouponApply.this);

                apis = MergeApplication.getMergeAppComponent().getRetrofit().create(UserApi.class);

                apis.applyCoupon(edt.getText().toString().trim()).enqueue(new Callback<ResponseObject<CouponApplyInfo>>() {
                    @Override
                    public void onResponse(Call<ResponseObject<CouponApplyInfo>> call, Response<ResponseObject<CouponApplyInfo>> response) {
                        MergeApplication.getMergeApplication().hideLoading(CouponApply.this);

                        if (response.isSuccessful()){
                            CouponApplyInfo info = response.body().getObject();
                            if (response.body().isFailed()  || info == null){
                                tv.setText(response.body().getMessage());
                            }else{
                                if (info.getCodeType() == 2){
                                    Snackbar.make(findViewById(android.R.id.content), "쿠폰이 추가되었습니다.", Snackbar.LENGTH_SHORT).show();
                                }else if (info.getCodeType() == 1) {
                                    Intent inte = new Intent(CouponApply.this, CouponComplete.class);
                                    inte.putExtra("addPoints", info.getAddPoints());
                                    inte.putExtra("newPoints", info.getNewPoints());
                                    startActivity(inte);
                                }
                                finish();
                            }
                        }else {
                            MDEBUG.debug("Failed");
                            Snackbar.make(findViewById(android.R.id.content), "에러가 발생했습니다 잠시후 다시 시도해주세요.", Snackbar.LENGTH_SHORT).show();
                            tv.setText("에러가 발생했습니다 잠시후 다시 시도해주세요.");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseObject<CouponApplyInfo>> call, Throwable t) {
                        MergeApplication.getMergeApplication().hideLoading(CouponApply.this);
                        MDEBUG.debug("Failed!!");
                        Snackbar.make(findViewById(android.R.id.content), "에러가 발생했습니다 잠시후 다시 시도해주세요.", Snackbar.LENGTH_SHORT).show();
                        tv.setText("에러가 발생했습니다. 잠시후 다시 시도해주세요.");
                    }
                });

            }
        });



    }
}
