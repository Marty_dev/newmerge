package kr.co.mergepoint.mergeclient.scene.shop.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;
import kr.co.mergepoint.mergeclient.scene.shop.view.fragment.ShopInfoFragment;
import kr.co.mergepoint.mergeclient.scene.shop.view.fragment.ShopMenuFragment;
import kr.co.mergepoint.mergeclient.scene.shop.view.fragment.ShopReviewFragment;

/**
 * Created by 1017sjg on 2017. 6. 23..
 */

public class ShopPagerAdapter extends FragmentPagerAdapter {

    private String[] title;
    private Fragment[] fragments = new Fragment[3];

    public ShopPagerAdapter(ShopActivity activity) {
        super(activity.getSupportFragmentManager());

        title = activity.getResources().getStringArray(R.array.shop_detail_tab);
        fragments[0] = ShopInfoFragment.newInstance();
        fragments[1] = ShopMenuFragment.newInstance(activity);
        fragments[2] = ShopReviewFragment.newInstance(activity);
    }

    public ShopInfoFragment getInfoFragment() {
        return (ShopInfoFragment) getItem(0);
    }

    public ShopMenuFragment getMenuFragment() {
        return (ShopMenuFragment) getItem(1);
    }

    public ShopReviewFragment getReviewFragment() {
        return (ShopReviewFragment) getItem(2);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }
}
