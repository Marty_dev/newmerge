package kr.co.mergepoint.mergeclient.scene.cardmanagement.view.fragment;

import android.support.v4.widget.ContentLoadingProgressBar;
import android.webkit.WebChromeClient;
import android.webkit.WebViewClient;

import kr.co.mergepoint.mergeclient.application.common.CustomChromeClient;
import kr.co.mergepoint.mergeclient.application.common.CustomWebClient;
import kr.co.mergepoint.mergeclient.application.common.SingleWebFragment;
import kr.co.mergepoint.mergeclient.application.common.script.WebReturnScriptInterface;
import kr.co.mergepoint.mergeclient.application.common.script.WebReturnScriptListener;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public class UplusRegisteFragment extends SingleWebFragment<WebReturnScriptInterface> {

    private WebReturnScriptListener listener;
    private CustomChromeClient chromeClient;
    private CustomWebClient webClient;

    public static UplusRegisteFragment newInstance(WebReturnScriptListener listener) {
        UplusRegisteFragment uplusRegisteFragment = new UplusRegisteFragment();
        uplusRegisteFragment.listener = listener;
        uplusRegisteFragment.chromeClient = new CustomChromeClient();
        uplusRegisteFragment.webClient = new CustomWebClient();

        return uplusRegisteFragment;
    }

    @Override
    protected WebChromeClient setWebChromeClient(ContentLoadingProgressBar loadProgressBar) {
        chromeClient.setContentLoadingProgressBar(loadProgressBar);
        return chromeClient;
    }

    @Override
    protected WebViewClient setWebViewClient() {
        return webClient;
    }

    @Override
    protected WebReturnScriptInterface setJavascript() {
        return new WebReturnScriptInterface(listener);
    }

    @Override
    protected String setPageTitle() {
        return "간편카드등록";
    }
}
