package kr.co.mergepoint.mergeclient.scene.data.main;

import com.google.gson.annotations.SerializedName;

/**
 * User: Marty
 * Date: 2018-08-01
 * Time: 오전 10:53
 * Description:
 */
public class userPointinfo {

    @SerializedName("companyMember")
    public boolean companyMember;
    @SerializedName("alarmCount")
    public int alarmCount;
    @SerializedName("companyPoints")
    public int companyPoints;
    @SerializedName("personalPoints")
    public int personalPoints;
    @SerializedName("memberRef")
    public int memberRef;
}
