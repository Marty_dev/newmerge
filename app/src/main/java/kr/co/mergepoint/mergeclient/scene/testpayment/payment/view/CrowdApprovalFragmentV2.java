package kr.co.mergepoint.mergeclient.scene.testpayment.payment.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.CrowdApprovalBinding;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPayResult;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPayResultState;
import kr.co.mergepoint.mergeclient.scene.payment.view.adapter.CrowdApprovalAdapter;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.CrowdApprovalListener;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.CrowdSettingActivity;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.CrowdInfo;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.CROWD_RECEIVER;

/**
 * Created by 1017sjg on 2017. 8. 8..
 */

public class CrowdApprovalFragmentV2 extends BaseFragment {

    private SparseIntArray intArray;
    private ArrayList<CrowdPay> crowdPays;
    private CrowdApprovalBinding approvalBinding;
    private BroadcastReceiver receiver;
    private CrowdApprovalListener listener;

    private long startTime = 0;
    private Handler timerHandler = new Handler();
    public CrowdSettingActivity actvity;
    private Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            long millis = (startTime + 300000) - System.currentTimeMillis();
            int seconds = (int) (millis / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;

            if (isAdded()) {
                approvalBinding.approvalTimer.setText(getString(R.string.timer, minutes, seconds));
                if (minutes <= 0 && seconds <= 0) {
                    timerHandler.removeCallbacks(timerRunnable);
                } else {
                    timerHandler.postDelayed(this, 500);
                }
            }
        }
    };

    @Override
    public void onClick(View v) {

        MDEBUG.debug("Not Call?" + v.getId());
        switch (v.getId()){
            case R.id.back:
                this.getRootActivity().getSupportFragmentManager().beginTransaction()
                        .remove(this)
                        .commit();
                break;
            case R.id.approval_reject_delete:
                /* 시간 초과 및 거절 됬을 때 요청 삭제 */
                //baseView.showLoading();
                //baseModel.deleteCrowdApproval(baseCallback.updateCrowdApproval(), (int) view.getTag());
                actvity.apis.requestdeleteCrowdMember((int)v.getTag()).enqueue(getCrowdPayCallback());

                break;
            case R.id.approval_retry:
                /* 시간 초과 됬을 때 재시도 */
                //baseView.showLoading();
                //baseModel.retryCrowdApproval(baseCallback.updateCrowdApproval(), (int) view.getTag());
                actvity.apis.requestReApproval((int)v.getTag()).enqueue(new Callback<ResponseObject<ArrayList<CrowdPay>>>() {
                    @Override
                    public void onResponse(Call<ResponseObject<ArrayList<CrowdPay>>> call, Response<ResponseObject<ArrayList<CrowdPay>>> response) {
                        if (response.body().isFailed())
                            new MergeDialog.Builder(actvity).setContent(response.body().getMessage()).setCancelBtn(false).build().show();
                        else {
                            updateApprovalTable(response.body().getObject());
                        }
                        MergeApplication.getMergeApplication().hideLoading(actvity);
                    }

                    @Override
                    public void onFailure(Call<ResponseObject<ArrayList<CrowdPay>>> call, Throwable t) {
                        new MergeDialog.Builder(actvity).setContent(R.string.retry).setCancelBtn(false).build().show();
                        MergeApplication.getMergeApplication().hideLoading(actvity);
                    }
                });

                break;
        }
    }
    public Callback<ResponseObject<CrowdInfo>> getCrowdPayCallback(){
        return new Callback<ResponseObject<CrowdInfo>>() {
            @Override
            public void onResponse(Call<ResponseObject<CrowdInfo>> call, Response<ResponseObject<CrowdInfo>> response) {
                if (response.body().isFailed())
                    new MergeDialog.Builder(actvity).setContent(response.body().getMessage()).setCancelBtn(false).build().show();
                else{
                    ArrayList<CrowdPay> pays = new ArrayList<>();
                    for (int i = 0 ;i < crowdPays.size(); i++){
                        for (int k = 0 ; k < response.body().getObject().attendees.size(); k++) {
                            if (crowdPays.get(i).oid == response.body().getObject().attendees.get(k).crowdPayRef){
                                pays.add(crowdPays.get(i));
                            }
                        }
                    }
                   updateApprovalTable(pays);
                }
                MergeApplication.getMergeApplication().hideLoading(actvity);

            }
            @Override
            public void onFailure(Call<ResponseObject<CrowdInfo>> call, Throwable t) {
                new MergeDialog.Builder(actvity).setContent(R.string.retry).setCancelBtn(false).build().show();
                MergeApplication.getMergeApplication().hideLoading(actvity);
            }

        };
    }
    public static CrowdApprovalFragmentV2 newInstance(CrowdSettingActivity activity, ArrayList<CrowdPay> crowdPays) {
        Bundle args = new Bundle();
        CrowdApprovalFragmentV2 fragment = new CrowdApprovalFragmentV2();
        fragment.crowdPays = crowdPays;
        fragment.intArray = new SparseIntArray();
        fragment.listener = activity;
        fragment.actvity = activity;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Member member = MergeApplication.getMember();
        if (member != null) {
            int memberOid = member.getOid();
            for (CrowdPay crowdPay :crowdPays) {
                if (crowdPay.memberRef == memberOid)
                    crowdPays.remove(crowdPay);
            }
        }
        startTimer(crowdPays.get(0).reqDateTime);



        }

    @Override
    public void onResume() {
        super.onResume();
        boolean isAutoaprove = true;
        for (CrowdPay pay : crowdPays) {
            isAutoaprove &= (pay.approveState >= 2);
        }
        if (isAutoaprove){
            stopTimer();
            listener.endApproval();
        }
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
               MDEBUG.debug("BroadcastReceiver" + intent.getStringExtra("CROWD_APPROVAL_JSON"));
                String crowdJson = intent.getStringExtra("CROWD_APPROVAL_JSON");
                CrowdPayResult crowdPayResult = new Gson().fromJson(crowdJson, CrowdPayResult.class);

                if (approvalBinding != null) {
                    ArrayList<CrowdPayResultState> states = crowdPayResult.crowdPayResult;
                    boolean isEndRequest = true;
                    boolean isStopTimer = true;
                    for (CrowdPayResultState state:states) {
                        intArray.put(state.crowdPayRef, state.approveState);
                        isEndRequest &= (state.approveState == 2);
                        isStopTimer &= (state.approveState >= 2);
                    }
                    CrowdApprovalAdapter adapter = approvalBinding.getListAdapter();
                    adapter.updateState(intArray);

                    if (isEndRequest) {
                        listener.endApproval();
                    } else if (isStopTimer) {
                        stopTimer();
                    }
                }
            }
        };
        getRootActivity().registerReceiver(receiver, new IntentFilter(CROWD_RECEIVER));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        approvalBinding = DataBindingUtil.inflate(inflater, R.layout.crowd_approval, container, false);
        approvalBinding.setTitle("승인요청");
        approvalBinding.setOnClick(this);
        approvalBinding.setListAdapter(new CrowdApprovalAdapter(crowdPays, this));
        return approvalBinding.getRoot();
    }

    @Override
    public void onPause() {
        super.onPause();
        getRootActivity().unregisterReceiver(receiver);
    }

    private void startTimer(ArrayList<Integer> dateArray) {
        if (dateArray != null && dateArray.size() >= 6) {
            String dateStr = String.format(Locale.getDefault(), "%d-%02d-%02d %02d:%02d:%02d", dateArray.get(0), dateArray.get(1), dateArray.get(2), dateArray.get(3), dateArray.get(4), dateArray.get(5));
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            try {
                Date date = format.parse(dateStr);
                startTime = date.getTime();
            } catch (ParseException ignored) {}
        } else {
            startTime = System.currentTimeMillis();
        }
        timerHandler.postDelayed(timerRunnable, 0);
        Log.d("startTimer", "" + startTime);
    }

    private void stopTimer() {
        timerHandler.removeCallbacks(timerRunnable);
        approvalBinding.approvalTimer.setText("00:00");
    }

    public void updateApprovalTable(ArrayList<CrowdPay> updateCrowdPays) {
        Member member = MergeApplication.getMember();
        if (member != null) {
            crowdPays.clear();
            int memberOid = member.getOid();
            for (CrowdPay crowdPay :updateCrowdPays) {
                if (crowdPay.memberRef != memberOid)
                    crowdPays.add(crowdPay);
            }
        }

        boolean isRequestComplete = true;
        for (CrowdPay crowdPay :crowdPays) {
            isRequestComplete &= (crowdPay.approveState >= 2);

            MDEBUG.debug("Crowd Approval State : " + crowdPay.approveState);
        }
        if (isRequestComplete) {
            stopTimer();
            listener.endApproval();
        }  else {
            ArrayList<Integer> datearr = crowdPays.get(0).reqDateTime;
            int tempdate = datearr.get(5);
            datearr.remove(5);
            datearr.add(5,tempdate == 0 ? 59 : tempdate-1);
            startTimer(datearr);
            MDEBUG.debug("CrowdPay REQ DATE : " + datearr);
            approvalBinding.getListAdapter().setListData(crowdPays);
            SparseIntArray intArray = new SparseIntArray();
            for (CrowdPay crow : crowdPays){
                intArray.put(crow.oid,crow.approveState);
            }
            approvalBinding.getListAdapter().updateState(intArray);
        }
    }
}
