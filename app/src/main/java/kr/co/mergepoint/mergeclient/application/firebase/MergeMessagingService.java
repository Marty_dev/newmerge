package kr.co.mergepoint.mergeclient.application.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponDetail;
import kr.co.mergepoint.mergeclient.scene.newcoupon.view.Barcode_detailActivity;
import kr.co.mergepoint.mergeclient.scene.newcoupon.view.CouponDetailActivity;
import kr.co.mergepoint.mergeclient.scene.splash.SplashActivity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.UsingFragmentv2;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import kr.co.mergepoint.mergeclient.util.Merge_Constant;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.support.v4.app.NotificationCompat.DEFAULT_SOUND;
import static android.support.v4.app.NotificationCompat.DEFAULT_VIBRATE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CROWD_RECEIVER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FCM_RECEIVER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MERGE_NOTIFICATION_CHANNEL_ID;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MERGE_NOTIFICATION_ID;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PUSH;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SETTING_PREFS;
import static kr.co.mergepoint.mergeclient.application.common.Properties.VIBRATION;

/**
 * Created by jgson on 2017. 7. 9..
 */

public class MergeMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String title = "";
        String crowdJson = "";
        String value = "";
        String action = "";
        MDEBUG.debug("noti is init" +  remoteMessage.toString());



        if (remoteMessage.getData().size() > 0) {
            /* 메시지 안에 데이터 페이로드가 있는지 확인 ( 백그라운드 혹은 앱이 실행되지 않았을 때 ) */
            Map<String, String> data = remoteMessage.getData();
            MDEBUG.debug("data payload : " + data.toString());
            title = data.get("title");
            crowdJson = data.get("crowd");

            value = data.get("value");
            action = data.get("clickAction");

            String message = data.get("value");
            MDEBUG.debug("data title : " + title);
            MDEBUG.debug("data crowd : " + crowdJson);
            MDEBUG.debug("data message : " + message);
            Intent intent = new Intent(this, SplashActivity.class);
            intent.putExtra("title", title);
            if (!value.isEmpty() && !action.isEmpty()){
                intent.putExtra("value",value);
                intent.putExtra("action",action);

                if (action.equals("22") || action.equals("24") || action.equals("19")){
                        Intent inte = null;
                        if (MergeApplication.isBarcodeforPay){
                            //inte = new Intent(this,UsageActivity.class);

                            inte = new Intent(FCM_RECEIVER);
                            inte.putExtras(intent.getExtras());
                            sendBroadcast(inte);
                            inte = null;
                        } else if (MergeApplication.isBarcodeforMoney){
                            inte = new Intent(this,CouponDetailActivity.class);
                        } else if (MergeApplication.isBarcodeforUnion){
                            inte = new Intent(this,Barcode_detailActivity.class);

                        }

                        if (inte !=null) {
                            inte.putExtras(intent.getExtras());
                            inte.setFlags(FLAG_ACTIVITY_NEW_TASK);
                            startActivity(inte);
                        }
                }
            }

//            if (MergeApplication._isAppActive){
//                startActivity(intent);
//                return;
//            }
            showNotification(title, message, intent);
        }
        else if (remoteMessage.getNotification() != null) {
            /* 메시지 안에 노티피케이션 페이로드가 있는지 확인 ( 포그라운드 상태 ) */
            MDEBUG.debug("noti payload : " + remoteMessage.getNotification().toString());
            title = remoteMessage.getNotification().getTitle();
            String message = remoteMessage.getNotification().getBody();
            String clickAction = remoteMessage.getNotification().getClickAction();
            MDEBUG.debug("noti title : " + message);
            MDEBUG.debug("noti message : " + message);
            MDEBUG.debug("noti clickAction : " + message);
            Intent intent = new Intent(clickAction);
            showNotification(title, message, intent);
        }

        if (title != null && (title.contains("함께 결제 승인 처리 결과") || title.contains("함께 결제 승인 시간 초과"))) {
            crowedPayMessage(crowdJson);
        }
    }

    private void crowedPayMessage(String crowdJson) {
        Intent intent = new Intent(CROWD_RECEIVER);
        intent.putExtra("CROWD_APPROVAL_JSON", crowdJson);
        sendBroadcast(intent);
    }

    private void showNotification(String title, String message, Intent notificationIntent) {
        MDEBUG.debug("send noti start");
        int i = 0;
        SharedPreferences sharedPreferences = this.getSharedPreferences(SETTING_PREFS, Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean(PUSH, true)) {
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            date.setTime(calendar.getTime().getTime());
            String timeFormat = new SimpleDateFormat("[HH:mm] ", Locale.KOREA).format(date);
            MDEBUG.debug("" + i++);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                NotificationChannel channelMessage = new NotificationChannel(MERGE_NOTIFICATION_CHANNEL_ID, "MergePoint", NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channelMessage);
                MDEBUG.debug("" + i++);

            }

            NotificationCompat.Builder notiBuilder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                MDEBUG.debug("" + i++);

                notiBuilder = new NotificationCompat.Builder(this, MERGE_NOTIFICATION_CHANNEL_ID);
            } else {
                notiBuilder = new NotificationCompat.Builder(this);
                MDEBUG.debug("else" + i++);

            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                MDEBUG.debug("noti icon lollipop");
                notiBuilder.setColor(ContextCompat.getColor(getApplicationContext(), R.color.notification_bg_red));
                notiBuilder.setSmallIcon(R.drawable.icon_notification);
                notiBuilder.setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.icon_merge));
            } else {
                MDEBUG.debug("lollipop down version noti icon setting");
                notiBuilder.setSmallIcon(R.mipmap.ic_launcher);
                notiBuilder.setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher));
            }

            notiBuilder.setContentTitle(timeFormat + title).setContentText(message).setAutoCancel(true).setContentIntent(pendingIntent);

            if (sharedPreferences.getBoolean(VIBRATION, true)) {
                MDEBUG.debug("" + i++);

                notiBuilder.setDefaults(DEFAULT_SOUND | DEFAULT_VIBRATE);
            }


            if (notificationManager != null) {
                MDEBUG.debug("" + i++);

                try {
                    notificationManager.notify(MERGE_NOTIFICATION_ID, notiBuilder.build());
                    sendBroadcast(new Intent(FCM_RECEIVER));
                }catch (Exception e){
                    MDEBUG.debug("noti build Error");
                    MDEBUG.debug(e.toString());
                }finally{
                    MDEBUG.debug("noti build is done");
                }
            }

            
        }
    }
}
