package kr.co.mergepoint.mergeclient.scene.data.main;

/**
 * Created by 1017sjg on 2017. 9. 14..
 */

public class Contactus {
    public String email;
    public String phone;
    public int type;
    public String name;
    public String title;
    public String body;
    public String attachFile;

    public Contactus(String email, String phone, String name, String title, String body) {
        this.email = email;
        this.phone = phone;
        this.name = name;
        this.title = title;
        this.body = body;
    }

    public Contactus(String email, String phone, int type, String title, String body, String attachFile) {
        this.email = email;
        this.phone = phone;
        this.type = type;
        this.title = title;
        this.body = body;
        this.attachFile = attachFile;
    }
}
