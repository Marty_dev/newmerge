package kr.co.mergepoint.mergeclient.scene.payment.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.EnterpriseUsePointsItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;

/**
 * Created by 1017sjg on 2017. 7. 6..
 */

public class CrowdPayListAdapter extends BasicListAdapter<BasicListHolder<EnterpriseUsePointsItemBinding, CrowdPay>, CrowdPay> {

    private View.OnClickListener onClickListener;

    public CrowdPayListAdapter(ArrayList<CrowdPay> arrayList, View.OnClickListener onClickListener) {
        super(arrayList);
        this.onClickListener = onClickListener;
    }

    @Override
    public BasicListHolder<EnterpriseUsePointsItemBinding, CrowdPay> setCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.enterprise_use_points_item, parent, false);
        EnterpriseUsePointsItemBinding listBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<EnterpriseUsePointsItemBinding, CrowdPay>(listBinding) {
            @Override
            public void setDataBindingWithData(CrowdPay data) {
                getDataBinding().setCrowdPay(data);
                getDataBinding().setOnClick(onClickListener);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<EnterpriseUsePointsItemBinding, CrowdPay> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
