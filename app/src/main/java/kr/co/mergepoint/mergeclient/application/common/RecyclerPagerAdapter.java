package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 6. 23..
 */

public class RecyclerPagerAdapter extends PagerAdapter {

    private ArrayList<RecyclerView> recyclers;
    private String[] titles;

    public RecyclerPagerAdapter(Context context, String[] titles) {
        this.recyclers = new ArrayList<>();
        this.titles = titles;
        for (String ignored : titles) recyclers.add(createRecyclerView(context));
    }

    public void setListAdapter(int position, BasicListAdapter listAdapter) {
        RecyclerView recyclerView = recyclers.get(position);
        recyclerView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
    }

    public void setListAdapter(int position, BasicListAdapter listAdapter, RecyclerItemListener.RecyclerTouchListener recyclerTouchListener) {
        RecyclerView recyclerView = recyclers.get(position);
        recyclerView.setAdapter(listAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemListener(recyclerView, recyclerTouchListener));
        listAdapter.notifyDataSetChanged();
    }

    private RecyclerView createRecyclerView(Context context) {
        RecyclerView recyclerView = new RecyclerView(context);
        recyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        recyclerView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        return recyclerView;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        RecyclerView recyclerView = recyclers.get(position);
        collection.addView(recyclerView);
        return recyclerView;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return recyclers.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
