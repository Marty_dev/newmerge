package kr.co.mergepoint.mergeclient.scene.data.main;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 9. 12..
 */

public class MergesPick {

    @SerializedName("oid")
    public int oid;

    @SerializedName("title")
    public String title;

    @SerializedName("description")
    public String description;

    @SerializedName("state")
    public int state;

    @SerializedName("mainImage")
    public String mainImage;

    @SerializedName("listImage")
    public String listImage;

    @SerializedName("creDateTime")
    public ArrayList<Integer> creDateTime;
    @SerializedName("modDateTime")
    public ArrayList<Integer> modDateTime;
}
