package kr.co.mergepoint.mergeclient.scene.data.payment;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class ApprovalState {
    @SerializedName("crowdPayRef")
    public int crowdPayRef;

    @SerializedName("approve")
    public boolean approve;

    public ApprovalState(int crowdPayRef, boolean approve) {
        this.crowdPayRef = crowdPayRef;
        this.approve = approve;
    }
}
