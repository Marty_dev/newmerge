package kr.co.mergepoint.mergeclient.scene.data.shoplist;

/**
 * Created by 1017sjg on 2017. 9. 7..
 */

public class SingleItem {

    private boolean isTheme;
    private String name;
    private String code;

    public SingleItem(boolean isTheme, String name, String code) {
        this.isTheme = isTheme;
        this.name = name;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isTheme() {
        return isTheme;
    }

    public void setTheme(boolean theme) {
        isTheme = theme;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;

        SingleItem itemCompare = (SingleItem) obj;

        return itemCompare.getName().equals(this.getName());
    }
}
