package kr.co.mergepoint.mergeclient.scene.shop;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import javax.inject.Inject;

import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.ExpandableItemListener;
import kr.co.mergepoint.mergeclient.scene.shop.dagger.DaggerShopComponent;
import kr.co.mergepoint.mergeclient.scene.shop.dagger.ShopModule;
import kr.co.mergepoint.mergeclient.scene.shop.presenter.ShopPresenter;
import kr.co.mergepoint.mergeclient.scene.shop.view.listener.OnAddReviewsListener;

/**
 * Created by 1017sjg on 2017. 6. 13..
 */

public class ShopActivity extends BaseActivity implements ExpandableItemListener.ExpandableTouchListener, OnAddReviewsListener {

    @Inject ShopPresenter presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerShopComponent.builder().shopModule(new ShopModule(this)).build().inject(this);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        presenter.onPostResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
        super.onBackPressed();
    }


    @Override
    protected void onActivityClick(View view) {
        presenter.onClick(view);
    }

    @Override
    public void onChildTouch(ChildViewHolder childViewHolder) {
        presenter.onChildTouch(childViewHolder);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void addReviews(int page) {
        presenter.addReviews(page);
    }
}
