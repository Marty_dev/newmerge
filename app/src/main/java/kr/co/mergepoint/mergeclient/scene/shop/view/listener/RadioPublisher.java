package kr.co.mergepoint.mergeclient.scene.shop.view.listener;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public interface RadioPublisher {
    void add(RadioObserver observer);
    void delete(RadioObserver observer);
    void notifyObserver();
}
