package kr.co.mergepoint.mergeclient.scene.main.view.listener;

public interface SettingsCrowedApprovalListener {
    void onClickApprovalType(int type);
    void onClickApprovalType(int type, int approvalDelegationUser, String approvalDelegationUserName);
}
