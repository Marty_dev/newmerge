package kr.co.mergepoint.mergeclient.scene.data.shoplist;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 6. 15..
 */

public class ShopInfo {
    @SerializedName("currentPage")
    private int currentPage;

    @SerializedName("totalPage")
    private int totalPage;

    @SerializedName("totalShop")
    private int totalShop;

    @SerializedName("listSize")
    private int listSize;

    @SerializedName("shopList")
    private ArrayList<Shop> shopList;

    public int getCurrentPage() {
        return currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public int getTotalShop() {
        return totalShop;
    }

    public int getListSize() {
        return listSize;
    }

    public ArrayList<Shop> getShopList() {
        return shopList;
    }
}
