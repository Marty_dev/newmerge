package kr.co.mergepoint.mergeclient.scene.newcoupon.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.NewCouponApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.MergeActivity;
import kr.co.mergepoint.mergeclient.application.common.VerticalTextView;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponDetail;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.ResultFragmentv2;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.APPROVE_COMPLETE_TAG;

public class Barcode_detailActivity extends MergeActivity {
    @BindView(R.id.barcode_vertext)
    VerticalTextView barcodeVertext;
    @BindView(R.id.container)
    RelativeLayout container;
    @BindView(R.id.barcode_rightcontainer)
    RelativeLayout barcodeRightcontainer;
    @BindView(R.id.barcode_bigimg2)
    ImageView barcodeBigimg2;
    @BindView(R.id.barcode_vertext2)
    VerticalTextView barcodeVertext2;
    @BindView(R.id.container_ver)
    RelativeLayout containerVer;

    @Override
    protected void onActivityClick(View view) {

    }

    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.coupon_title)
    TextView couponTitle;
    @BindView(R.id.back)
    RelativeLayout back;
    @BindView(R.id.basic_toolbar)
    RelativeLayout basicToolbar;

    @BindView(R.id.barcode_bigimg)
    ImageView barcodeBigimg;
    @BindView(R.id.baseline1)
    TextView baseline1;
    @BindView(R.id.barcode_price_text)
    TextView barcodePriceText;
    @BindView(R.id.coupon_price_refresh)
    TextView couponPriceRefresh;
    @BindView(R.id.barcode_money)
    TextView barcodeMoney;
    @BindView(R.id.barcode_uselist)
    TextView barcodeUselist;

    //   1 == default  from coupon
    //   2 == custom   from usagelist
    //   3 == custom   from usedlist
    int viewtype;

    String barcode, title;
    int oid;
    int price;
    NewCouponApi API;

    MergeApplication mApp = MergeApplication.getMergeApplication();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_detail);
        ButterKnife.bind(this);
        API = MergeApplication.getMergeAppComponent().getRetrofit().create(NewCouponApi.class);

    }

    @Override
    protected void onResume() {
        super.onResume();
        intentdata();

        mApp.isBarcodeforUnion = true;
    }

    @Override
    protected void onStop() {
        super.onStop();

        mApp.isBarcodeforUnion = false;
    }

    @Override
    protected void onNewIntent(Intent intent){
        super.onNewIntent(intent);
        String action = intent.getStringExtra("action");
        if (action != null) {
            MDEBUG.debug("Action Data " + action);
            if (action.equals("22")) {
               Intent inte = new Intent(this,BarcodeResult.class);
               startActivity(inte);
            } else if (action.equals("24")) {
                new MergeDialog.Builder(BASE).setConfirmString("이용내역 가기")
                        .setContent("금액이 일치하지 않습니다.\n다시 한번 금액을 확인해 주세요.")
                        .setConfirmClick(() -> {
                            Intent inte = new Intent(this, UsageActivity.class);
                            startActivity(inte);
                            finish();
                        }).build().show();
            } else if (action.equals("19")) {
                couponPriceRefresh.callOnClick();
            }
        }
    }

    void initView() {
        couponTitle.setText(title);
        barcodeMoney.setText(MoneyForm(price) + "원");
        barcodeBigimg.setImageBitmap(BarcodeGenerateRotate(barcode));
        barcodeVertext.setText(barcode);

        if (viewtype >= 2) {
            if (viewtype == 2) {
                barcodeUselist.setVisibility(View.GONE);
                couponPriceRefresh.setVisibility(View.GONE);
            } else {
                barcodeBigimg.setVisibility(View.GONE);
                barcodeVertext.setVisibility(View.GONE);
                containerVer.setVisibility(View.VISIBLE);
                barcodeRightcontainer.setVisibility(View.GONE);
                barcodeBigimg2.setImageBitmap(BarcodeGenerateRotate(barcode));
                barcodeVertext2.setText(barcode);

            }
        }

    }

    void intentdata() {
        oid = getIntent().getIntExtra("couponRef", -1);
        price = getIntent().getIntExtra("price", 0);
        barcode = getIntent().getStringExtra("barcode");
        title = oid == -1 ? title : getIntent().getStringExtra("title");
        viewtype = getIntent().getIntExtra("viewtype", -1);
        if (barcode == null){
            return;
        }
        initView();
    }

    @OnClick({R.id.back_arrow, R.id.coupon_price_refresh, R.id.barcode_uselist})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_arrow:
                finish();
                break;
            case R.id.coupon_price_refresh:
                MergeApplication.getMergeApplication().showLoading(BASE);
                API.getBarcodedetail(oid).enqueue(new Callback<ResponseObject<CouponDetail>>() {
                    @Override
                    public void onResponse(Call<ResponseObject<CouponDetail>> call, Response<ResponseObject<CouponDetail>> response) {
                        MergeApplication.getMergeApplication().hideLoading(BASE);

                        if (!response.body().isFailed()) {
                            price = response.body().getObject().getRemainPrice();
                            barcode = response.body().getObject().getBarCode();
                            initView();
                        } else
                            new MergeDialog.Builder(mCon).setCancelBtn(false).setContent(mCon.getResources().getString(R.string.retry)).build().show();
                        MergeApplication.getMergeApplication().hideLoading((BaseActivity) mCon);

                    }

                    @Override
                    public void onFailure(Call<ResponseObject<CouponDetail>> call, Throwable t) {
                        MergeApplication.getMergeApplication().hideLoading(BASE);
                        new MergeDialog.Builder(mCon).setCancelBtn(false).setContent(mCon.getResources().getString(R.string.retry)).build().show();
                        MergeApplication.getMergeApplication().hideLoading((BaseActivity) mCon);

                    }
                });
                break;
            case R.id.barcode_uselist:
                Intent inte = new Intent(this, CouponuseListActivity.class);
                inte.putExtra("oid", oid);
                startActivity(inte);
                break;
        }
    }
}
