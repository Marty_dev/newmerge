package kr.co.mergepoint.mergeclient.scene.data.main;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 9. 18..
 */

public class EventDetail extends EventInfo {

    @SerializedName("details")
    public ArrayList<CombineImagesDetail> details;

    public EventDetail(Parcel in) {
        super(in);
        in.readTypedList(details, CombineImagesDetail.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(details);
    }

    public static final Creator<EventDetail> CREATOR = new Creator<EventDetail>() {
        @Override
        public EventDetail createFromParcel(Parcel in) {
            return new EventDetail(in);
        }

        @Override
        public EventDetail[] newArray(int size) {
            return new EventDetail[size];
        }
    };

    public EventInfo getEventInfo() {
        return new EventInfo(oid, title, listImage, state, creDateTime, modDateTime);
    }
}