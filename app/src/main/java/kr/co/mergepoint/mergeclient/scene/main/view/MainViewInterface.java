package kr.co.mergepoint.mergeclient.scene.main.view;

import android.os.Bundle;

import com.facebook.CallbackManager;

import java.util.ArrayList;
import java.util.Map;

import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.application.common.StreetButton;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocation;
import kr.co.mergepoint.mergeclient.scene.data.history.CompanyPointHistory;
import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPayItem;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.data.payment.Ticket;

/**
 * Created by jgson on 2018. 1. 2..
 */

public interface MainViewInterface {
    /*Fragment or Activity*/
    void openMenuAnim();
    void openPage(@Properties.MainFragment int page, String tag);
    void openPage(@Properties.MainFragment int page, Bundle bundle, String tag);
    void openVerticalPage(@Properties.MainFragment int page, Bundle bundle, String tag);
    void openMainSearch();
    void openThemeFragment(UserLocation userLocation, String data);
    void openFavoriteFragment(UserLocation userLocation);
    void openEventShared();
    void openChooseGallery();
    void openTicketList(ArrayList<Ticket> tickets);
    void openCompanyPointHistory(ArrayList<CompanyPointHistory> companyPointHistories, String userEnterprisePoint);
    void openCrowdPayHistory(ArrayList<CrowdPayItem> crowdPays);
    void openCrowdApprovalRequest(ArrayList<CrowdPay> crowdPays);
    void openCrowdApprovalRequestResult(ArrayList<CrowdPay> crowdPays, String message);
    void openCrowdPayHistoryDetail(ShopPayment shopPayment);
    void closeTutorial();
    void showEnterpriseNotice();

    void showTutorialView();
    void showEnterpriseState();

    /*GETTER or SETTER*/
    BaseFragment getPageFragment(@Properties.MainFragment int page, Bundle bundle);
    boolean getWithdrawalRefund();
    boolean getEqualPw();
    int getBannerActionType();
    String getBannerActionData();
    Map<String, Object> getResetPwInfo();
    CallbackManager getShareFacebookCallback();

    void setStreet(final ArrayList<String> streetArray, final StreetButton.OnStreetListener onStreetListener);
    void setListWithFavorite(int requestCode, int oid, boolean isFavorite);
    void setUserState();

    /*권한 요청*/
    void requestCameraPermission();
    void requestGalleryPermission();

    /*콜센터 전화걸리*/
    void callServiceCenter();

    /*메인 베너 애니메이션*/
    void startAutoBannerSlide();
    void stopAutoBannerSlide();

    void updateCrowdApprovalType(int type, int oid, String userName);

    void openMyCouponList(ArrayList<Coupon> coupons);
}
