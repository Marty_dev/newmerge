package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.RecyclerItemListener;
import kr.co.mergepoint.mergeclient.databinding.NoticeBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.Notice;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.NoticeAdapter;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NOTICE_FRAGMENT_TAG;

/**
 * Created by 1017sjg on 2017. 8. 7..
 */

public class NoticeFragment extends BaseFragment implements RecyclerItemListener.RecyclerTouchListener {

    private NoticeBinding noticeBinding;
    private ArrayList<Notice> notices;
    private OnNotice onNotice;

    public interface OnNotice {
        void onNoticeClick(Notice notice);
    }

    public static NoticeFragment newInstance(Bundle notices, OnNotice onNotice) {
        NoticeFragment fragment = new NoticeFragment();
        fragment.onNotice = onNotice;
        fragment.setArguments(notices);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notices = getArguments().getParcelableArrayList(NOTICE_FRAGMENT_TAG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        noticeBinding = DataBindingUtil.inflate(inflater, R.layout.notice, container, false);
        noticeBinding.setTitle(getString(R.string.notice_title));
        noticeBinding.setOnClick(this);
        noticeBinding.setListAdapter(new NoticeAdapter(notices));
        noticeBinding.setListTouch(NoticeFragment.this);
        return noticeBinding.getRoot();
    }

    @Override
    public void onClickItem(View v, int position) {
        onNotice.onNoticeClick(noticeBinding.getListAdapter().getObservableArrayList().get(position));
    }
}
