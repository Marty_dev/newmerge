package kr.co.mergepoint.mergeclient.scene.data.main;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class Notice implements Parcelable {

    @SerializedName("oid")
    public int oid;

    @SerializedName("title")
    public String title;

    @SerializedName("bodyText")
    public String bodyText;

    @SerializedName("creDateTime")
    public ArrayList<Integer> creDateTime;
    @SerializedName("modDateTime")
    public ArrayList<Integer> modDateTime;

    public Notice() {}

    public Notice(int oid, String title, String bodyText, ArrayList<Integer> creDateTime, ArrayList<Integer> modDateTime) {
        this.oid = oid;
        this.title = title;
        this.bodyText = bodyText;
        this.creDateTime = creDateTime;
        this.modDateTime = modDateTime;
    }

    private Notice(Parcel in) {
        this.oid = in.readInt();
        this.title = in.readString();
        this.bodyText = in.readString();
        in.readList(creDateTime, Integer.class.getClassLoader());
        in.readList(modDateTime, Integer.class.getClassLoader());
    }

    public static final Creator<Notice> CREATOR = new Creator<Notice>() {
        @Override
        public Notice createFromParcel(Parcel in) {
            return new Notice(in);
        }

        @Override
        public Notice[] newArray(int size) {
            return new Notice[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(oid);
        dest.writeString(title);
        dest.writeString(bodyText);
        dest.writeList(creDateTime);
        dest.writeList(modDateTime);
    }
}
