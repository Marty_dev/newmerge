package kr.co.mergepoint.mergeclient.application.common.script;

import android.webkit.JavascriptInterface;

/**
 * Created by 1017sjg on 2017. 6. 16..
 */

public class PostScriptInterface {

    private PostScriptListener listener;

    public PostScriptInterface(PostScriptListener listener) {
        this.listener = listener;
    }

    @JavascriptInterface
    public void setAddress(final String add01, final String add02, final String add03) {
        listener.setAddress(add01, add02, add03);
    }
}
