package kr.co.mergepoint.mergeclient.scene.shoppingbasket.view;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.databinding.ShoppingBasketBinding;
import kr.co.mergepoint.mergeclient.scene.data.basket.Basket;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.payment.PaymentActivity;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.BasketActivity;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.view.adapter.BasketListAdapter;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.presenter.BasketPresenter;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.view.fragment.BasketDetailFragment;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.Payment_v2_Activity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static android.app.Activity.RESULT_OK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.BASKET_DETAIL_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAYMENT_RESULT_ARRAY;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;

/**
 * Created by 1017sjg on 2017. 6. 28..
 */

public class BasketView extends BaseView<BasketActivity, BasketPresenter, ShoppingBasketBinding> {

    public BasketView(BasketActivity activity, int layout) {
        super(activity, layout);
    }

    @Override
    protected Toolbar setToolbar() {
        return binding.shoppingbasketToolbar.basicToolbar;
    }

    public void deleteMenu(View view, int price){
        MDEBUG.debug("price : " + price);
        binding.getBasketAdapter().deleteMenu(view, price);

    }

    public void showPaymentView(ArrayList<Integer> oids) {

        Intent payment = new Intent(activity, Payment_v2_Activity.class);
        payment.putIntegerArrayListExtra(PAYMENT_RESULT_ARRAY, oids);
        activity.startActivityForResult(payment,PAY_RESULT_REQUEST);
/*
        Intent payment = new Intent(activity, PaymentActivity.class);
        payment.putIntegerArrayListExtra(PAYMENT_RESULT_ARRAY, oids);
        activity.startActivityForResult(payment, PAY_RESULT_REQUEST);
*/   }

    public void openMenuDetail(View view) {
        BasketListAdapter listAdapter = binding.getBasketAdapter();
        ShopMenuDetail menuDetail = listAdapter.getShopMenuDetail(view);
        openFragment(RIGHT_LEFT, R.id.basket_layout, BasketDetailFragment.newInstance(activity, menuDetail), BASKET_DETAIL_TAG);
    }

    public ShopMenuDetail getMenuDetail(View view) {
        return binding.getBasketAdapter().getShopMenuDetail(view);
    }

    public void setMenuCheck(View view, boolean isChecked) {
        binding.getBasketAdapter().checkMenu(view, isChecked);
    }

    public void setResultFinish(Intent data) {
        Intent intent = new Intent();
        intent.putExtra("paymentRef",data.getIntExtra("paymentRef",-1));
        intent.putExtra("isMulti",data.getBooleanExtra("isMulti",false));
        activity.setResult(RESULT_OK, intent);
        activity.finish();
    }
}
