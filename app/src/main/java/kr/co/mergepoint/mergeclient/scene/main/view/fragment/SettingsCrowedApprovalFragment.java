package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.SettingsApprovalBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.SettingsCrowedApprovalListener;

import static kr.co.mergepoint.mergeclient.application.common.Properties.APPROVAL_DELEGATION_NAME;
import static kr.co.mergepoint.mergeclient.application.common.Properties.APPROVAL_DELEGATION_OID;
import static kr.co.mergepoint.mergeclient.application.common.Properties.APPROVAL_TYPE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.AUTO_APPROVAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.DEFAULT_APPROVAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.DELEGATION_APPROVAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SETTING_PREFS;

/**
 * Created by 1017sjg on 2017. 8. 9..
 */

public class SettingsCrowedApprovalFragment extends BaseFragment {

    private SettingsApprovalBinding settingsBinding;
    private SettingsCrowedApprovalListener listener;

    private int delegationOid;
    private String delegationName;

    public static SettingsCrowedApprovalFragment newInstance(SettingsCrowedApprovalListener listener) {
        Bundle args = new Bundle();
        SettingsCrowedApprovalFragment fragment = new SettingsCrowedApprovalFragment();
        fragment.listener = listener;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        settingsBinding = DataBindingUtil.inflate(inflater, R.layout.settings_approval, container, false);
        settingsBinding.setTitle(getString(R.string.settings));
        settingsBinding.setOnClick(this);

        SharedPreferences sharedPreferences = getRootActivity().getSharedPreferences(SETTING_PREFS, Context.MODE_PRIVATE);
        int type = sharedPreferences.getInt(APPROVAL_TYPE, DEFAULT_APPROVAL);
        delegationOid = sharedPreferences.getInt(APPROVAL_DELEGATION_OID, 0);
        delegationName = sharedPreferences.getString(APPROVAL_DELEGATION_NAME, "없음");
        changeApprovalRadio(type);
        SpannableString userName = new SpannableString(delegationName);
        userName.setSpan(new UnderlineSpan(), 0, userName.length(), 0);

        getRootActivity().runOnUiThread(() -> settingsBinding.setDelegationUserName(userName));
        return settingsBinding.getRoot();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int id = v.getId();
        if (id == settingsBinding.defaultApprovalRadio.getId() || id == settingsBinding.autoApprovalRadio.getId() || id == settingsBinding.approvalDelegationRadio.getId()) {
            int type = (int) v.getTag();
            changeApprovalRadio(type);

            if (type == DELEGATION_APPROVAL) {
                listener.onClickApprovalType(type, delegationOid, delegationName);
            } else {
                listener.onClickApprovalType(type);
            }
        }
    }

    private void changeApprovalRadio(int type) {
        getRootActivity().runOnUiThread(() -> settingsBinding.setType(type));
    }
}
