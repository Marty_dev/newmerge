package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.databinding.CrowdPayHistoryDetailMemberBinding;
import kr.co.mergepoint.mergeclient.databinding.CrowdPayHistoryDetailMenuBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter.MenuHistoryAdapter;

/**
 * Created by jgson on 2018. 4. 3..
 */

public class CrowdPayDetailPagerAdapter extends PagerAdapter {

    private String[] titles;
    private LayoutInflater layoutInflater;
    private ShopPayment shopPayment;
    private CrowdPay myCrowd;

    public CrowdPayDetailPagerAdapter(Context context, ShopPayment shopPayment) {
        titles = new String[] {"구매 메뉴", "함께결제한 직원"};
        layoutInflater = LayoutInflater.from(context);
        this.shopPayment = shopPayment;

        Member member = MergeApplication.getMember();
        if (member != null) {
            for (CrowdPay crowdPay : shopPayment.crowdPay) {
                if (crowdPay.memberRef == member.getOid()) {
                    myCrowd = crowdPay;
                    shopPayment.crowdPay.remove(crowdPay);
                    return;
                }
            }
        }
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        if (position == 0) {
            CrowdPayHistoryDetailMenuBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.crowd_pay_history_detail_menu, collection, false);
            binding.setShopPayment(shopPayment);
            binding.setDate(shopPayment.menu.get(0).getPayDateTime());
            binding.setAdapter(new MenuHistoryAdapter(shopPayment.menu, null));
            binding.getRoot().setTag(position);
            collection.addView(binding.getRoot());
            return binding.getRoot();
        } else {
            CrowdPayHistoryDetailMemberBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.crowd_pay_history_detail_member, collection, false);
            binding.getRoot().setTag(position);
            binding.setMyCrowd(myCrowd);
            binding.setDate(shopPayment.menu.get(0).getPayDateTime());
            binding.setShopPayment(shopPayment);
            binding.setAdapter(new CrowdHistoryDetailMemberAdapter(shopPayment.crowdPay));
            collection.addView(binding.getRoot());
            return binding.getRoot();
        }
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
