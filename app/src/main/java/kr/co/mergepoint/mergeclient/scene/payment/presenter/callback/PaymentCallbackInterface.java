package kr.co.mergepoint.mergeclient.scene.payment.presenter.callback;

import android.view.View;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.login.MemberPoint;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.PayConfirm;
import kr.co.mergepoint.mergeclient.scene.payment.presenter.PaymentPresenter;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public interface PaymentCallbackInterface {
    MergeCallback<ResponseObject<PayConfirm>> getPayConfirmCallback(PaymentPresenter presenter);
    MergeCallback<ResponseObject<PayConfirm>> getPayConfirmCallback(PaymentPresenter presenter, String message);
    MergeCallback<ResponseObject<PayConfirm>> getPayConfirmCallback(PaymentPresenter presenter, View view, int couponOid, int shopPaymentOid);
    MergeCallback<ResponseObject<PayConfirm>> getPayConfirmCallback(PaymentPresenter presenter, String message, View view, int couponOid, int shopPaymentOid);

    MergeCallback<ResponseObject<ArrayList<CrowdPay>>> requestCrowedPayCallback(int shopRef);
    MergeCallback<ResponseObject<MemberPoint>> requestSettingCrowedPayPoint();

    MergeCallback<ResponseObject<ArrayList<CrowdMember>>> getCrowedMemberList();
    MergeCallback<ResponseObject<ArrayList<CrowdPay>>> requestSelectCrowedMember();
    MergeCallback<ResponseObject<ArrayList<CrowdPay>>> requestSelectPoint();

    MergeCallback<ResponseObject<ArrayList<CrowdPay>>> requestCrowdApproval(PaymentPresenter presenter);

    MergeCallback<ResponseObject<PayConfirm>> readPayment(PaymentPresenter presenter);
    MergeCallback<ResponseObject<Long>> changeFavoriteCrowdMember();

    MergeCallback<ResponseObject<ArrayList<CrowdPay>>> updateCrowdApproval();
}
