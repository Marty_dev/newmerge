package kr.co.mergepoint.mergeclient.scene.testpayment.payment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.cachapa.expandablelayout.ExpandableLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.co.mergepoint.mergeclient.R;

/**
 * User: Marty
 * Date: 2018-07-18
 * Time: 오후 2:13
 * Description:
 */
public class ShopViewholder extends RecyclerView.ViewHolder {

    @BindView(R.id.shop_name_tv)
    TextView shopNameTv;
    @BindView(R.id.shop_prise_tv)
    TextView shopPriseTv;
    @BindView(R.id.shop_name_rl)
    RelativeLayout shopNameRl;
    @BindView(R.id.enter_point_plus)
    ImageView enterPointPlus;
    @BindView(R.id.enter_point_shopname)
    TextView enterPointShopname;
    @BindView(R.id.enter_point_prise)
    TextView enterPointPrise;
    @BindView(R.id.enter_point_rl)
    RelativeLayout enterPointRl;
    @BindView(R.id.enter_mypoint_tv)
    TextView enterMypointTv;
    @BindView(R.id.enter_point_confirm)
    TextView enterPointConfirm;
    @BindView(R.id.enter_usepoint_edt)
    EditText enterUsepointEdt;
    @BindView(R.id.enter_point_use_rl)
    RelativeLayout enterPointUseRl;
    @BindView(R.id.enter_point_allbtn)
    TextView enterPointAllbtn;
    @BindView(R.id.enter_ticket_minus)
    ImageView enterTicketMinus;
    @BindView(R.id.enter_ticket_counttv)
    TextView enterTicketCounttv;
    @BindView(R.id.enter_ticket_plus)
    ImageView enterTicketPlus;
    @BindView(R.id.enter_ticket_countrl)
    RelativeLayout enterTicketCountrl;
    @BindView(R.id.enter_ticket_arrow)
    ImageView enterTicketArrow;
    @BindView(R.id.enter_ticket_rl)
    RelativeLayout enterTicketRl;
    @BindView(R.id.enter_expandl)
    ExpandableLayout enterExpandl;
    @BindView(R.id.normal_point_plus)
    ImageView normalPointPlus;
    @BindView(R.id.normal_point_shopname)
    TextView normalPointShopname;
    @BindView(R.id.normal_point_prise)
    TextView normalPointPrise;
    @BindView(R.id.normal_point_rl)
    RelativeLayout normalPointRl;
    @BindView(R.id.normal_mypoint_tv)
    TextView normalMypointTv;
    @BindView(R.id.normal_point_confirm)
    TextView normalPointConfirm;
    @BindView(R.id.normal_usepoint_edt)
    EditText normalUsepointEdt;
    @BindView(R.id.normal_point_use_rl)
    RelativeLayout normalPointUseRl;
    @BindView(R.id.normal_point_allbtn)
    TextView normalPointAllbtn;
    @BindView(R.id.normal_ticket_arrow)
    ImageView normalTicketArrow;
    @BindView(R.id.normal_ticket_rl)
    RelativeLayout normalTicketRl;
    @BindView(R.id.normal_expandl)
    ExpandableLayout normalExpandl;
    @BindView(R.id.enter_ticket_nametv)
    TextView enterTicketNametv;
    @BindView(R.id.normal_ticket_nametv)
    TextView normalTicketNametv;


    public ShopViewholder(Context mCon, ViewGroup parent, View itemView) {
        super(LayoutInflater.from(mCon).inflate(R.layout.payment_shop_item_v2, parent, false));
    }

    public ShopViewholder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
