package kr.co.mergepoint.mergeclient.api;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.main.MergesPick;
import kr.co.mergepoint.mergeclient.scene.data.main.CombineImagesDetail;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by 1017sjg on 2017. 9. 13..
 */

public interface MergesPickApi {

    @POST("mergesPick/list")
    Call<ResponseObject<ArrayList<MergesPick>>> requestMergesPickList();

    @POST("mergesPick/detail/{oid}")
    Call<ResponseObject<ArrayList<CombineImagesDetail>>> requestMergesPickDetail(@Path("oid") int mergesPickOid);
}
