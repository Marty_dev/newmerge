package kr.co.mergepoint.mergeclient.scene.data.newcoupon;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * User: Marty
 * Date: 2018-09-21
 * Time: 오후 5:48
 * Description:
 */
public class CouponUseInfo {

    @SerializedName("remainPrice")
    private long remainPrice;
    @SerializedName("couponUseList")
    private ArrayList<CouponUse> couponUseList;

    public long getRemainPrice() {
        return remainPrice;
    }

    public void setRemainPrice(long remainPrice) {
        this.remainPrice = remainPrice;
    }

    public ArrayList<CouponUse> getCouponUseList() {
        return couponUseList;
    }

    public void setCouponUseList(ArrayList<CouponUse> couponUseList) {
        this.couponUseList = couponUseList;
    }
}
