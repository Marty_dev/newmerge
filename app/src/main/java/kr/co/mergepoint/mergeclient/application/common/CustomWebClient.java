package kr.co.mergepoint.mergeclient.application.common;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.net.CookieHandler;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import kr.co.mergepoint.mergeclient.application.MergeDialog;

import static kr.co.mergepoint.mergeclient.application.common.Properties.BASE_URL;

/**
 * Created by jgson on 2017. 6. 10..
 */

public class CustomWebClient extends WebViewClient {

    public CustomWebClient() {
        setCookie(BASE_URL);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return super.shouldOverrideUrlLoading(view, url);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        return super.shouldOverrideUrlLoading(view, request);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        setCookie(url);
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        addCookie(url);
        super.onPageFinished(view, url);
    }

    @Override
    public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
        String message = "SSL 인증서 오류입니다.";
        switch (error.getPrimaryError()) {
            case SslError.SSL_UNTRUSTED:
                message = "인증 기관이 신뢰되지 않습니다.";
                break;
            case SslError.SSL_EXPIRED:
                message = "만료된 인증서입니다.";
                break;
            case SslError.SSL_IDMISMATCH:
                message = "인증서 호스트 이름이 일치하지 않습니다.";
                break;
            case SslError.SSL_NOTYETVALID:
                message = "인증서가 아직 유효하지 않습니다.";
                break;
        }
        message += " 계속 하시겠습니까?";

        new MergeDialog.Builder(view.getContext()).setContent(message)
                .setConfirmClick(handler::proceed)
                .setCancelClick(handler::cancel).build().show();
    }

    private void addCookie(String url) {
        // 웹뷰의 쿠키저장소에서 쿠키값 받아오기
        String cookie = CookieManager.getInstance().getCookie(url);
        URI uri;

        try {
            uri = new URI(url);

//            URI hostUri = new URI(uri.getHost());
            if (cookie != null) {
                String[] cookies = cookie.split("\\s");
                for (String cookieString :cookies) {
                    // 웹뷰에서 받아온 쿠키값을 토대로 HttpCookie 값 생성
                    int index = cookieString.indexOf("=");
                    if (index > 0) {
                        String cookieID = cookieString.substring(0, index);
                        String cookieValue = cookieString.substring(index + 1).replaceAll("\"", "").replaceAll(";", "");

                        if (!cookieValue.isEmpty()) {
                            HttpCookie httpCookie = new HttpCookie(cookieID, cookieValue);
                            httpCookie.setVersion(0);
                            httpCookie.setDomain(uri.getHost());

                            // 앱의 커스텀 쿠키저장소를 불러와서 추가하기
                            CustomCookieManager customCookieManager = (CustomCookieManager) CookieHandler.getDefault();
                            customCookieManager.getCookieStore().add(uri, httpCookie);  // TODO: hostUri->uri로 변경
                        }
                        Log.d("JSESSIONID_WEBWIEW_ADD", cookieID + "=" + cookieValue);
                    }
                }
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void setCookie(String url) {
        // 앱의 커스텀 쿠키저장소를 불러오기
        CustomCookieManager customCookieManager = (CustomCookieManager) CookieHandler.getDefault();
        CookieStore cookieStore = customCookieManager.getCookieStore();

        // url을 기준으로 URI 생성
        URI uri;

        try {
            uri = new URI(url);

            // 앱의 커스텀 쿠키를 웹뷰에 설정하기
            List<HttpCookie> cookies = cookieStore.get(uri);
            for (HttpCookie cookie : cookies) {
                String setCookie = cookie.toString();
                if (uri.getHost() != null) {
                    String currentCookie = CookieManager.getInstance().getCookie(uri.getHost());
                    if (currentCookie == null || !setCookie.equals(currentCookie))
                        CookieManager.getInstance().setCookie(uri.getHost(), setCookie);
                    Log.d("JSESSIONID_WEBWIEW_SET", setCookie);
                }
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
