package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.View;

import kr.co.mergepoint.mergeclient.R;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NORMAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SELECT;

/**
 * Created by jgson on 2017. 7. 8..
 */

public class FilterButton extends AppCompatButton implements View.OnClickListener {

    private int select;

    @Properties.StateButton
    public int getSelect() {
        return select;
    }

    public void setSelect(@Properties.StateButton int select) {
        this.select = select;
    }

    public FilterButton(Context context) {
        super(context);
        init();
    }

    public FilterButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FilterButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOnClickListener(this);
        setSelect(NORMAL);
        setSelectBackground(select);
    }

    private void setSelectBackground(@Properties.StateButton int isSelect) {
        int drawable = isSelect == SELECT ? R.drawable.rounded_rect_red : android.R.color.transparent;
        int textColor = isSelect == SELECT ? R.color.point_red : R.color.gray_07;

        setBackground(ContextCompat.getDrawable(getContext(), drawable));
        setTextColor(ContextCompat.getColor(getContext(), textColor));
    }

    @Override
    public void onClick(View v) {
        invalidate();
        setSelect(getSelect() == SELECT ? NORMAL : SELECT);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        setSelectBackground(select);
    }
}
