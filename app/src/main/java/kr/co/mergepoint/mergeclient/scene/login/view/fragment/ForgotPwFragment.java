package kr.co.mergepoint.mergeclient.scene.login.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.ForgotPasswordBinding;

/**
 * Created by jgson on 2017. 7. 19..
 */

public class ForgotPwFragment extends BaseFragment {

    private ForgotPasswordBinding forgotPasswordBinding;
    private TextView.OnEditorActionListener actionListener;

    public ForgotPasswordBinding getForgotPasswordBinding() {
        return forgotPasswordBinding;
    }

    public static ForgotPwFragment newInstance(TextView.OnEditorActionListener actionListener) {
        Bundle args = new Bundle();
        ForgotPwFragment fragment = new ForgotPwFragment();
        fragment.actionListener = actionListener;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        forgotPasswordBinding = DataBindingUtil.inflate(inflater, R.layout.forgot_password, container, false);
        forgotPasswordBinding.setTitle(getString(R.string.forgot_pw_title));
        forgotPasswordBinding.setOnClick(this);
        forgotPasswordBinding.setOnForgotListener(actionListener);
        return forgotPasswordBinding.getRoot();
    }
}
