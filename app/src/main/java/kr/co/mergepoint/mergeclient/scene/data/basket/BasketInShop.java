package kr.co.mergepoint.mergeclient.scene.data.basket;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;

/**
 * Created by 1017sjg on 2017. 6. 28..
 */

public class BasketInShop {

    @SerializedName("shopRef")
    public int shopRef;

    @SerializedName("shopName")
    public String shopName;

    @SerializedName("payPrice")
    public int payPrice;

    @SerializedName("menu")
    public ArrayList<ShopMenuDetail> menu;
}
