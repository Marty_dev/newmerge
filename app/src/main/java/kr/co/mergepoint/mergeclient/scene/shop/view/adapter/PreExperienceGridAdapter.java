package kr.co.mergepoint.mergeclient.scene.shop.view.adapter;

import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.support.v4.view.ViewCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.SingleUrlImageBinding;
import kr.co.mergepoint.mergeclient.scene.data.shop.PreExperience;

/**
 * Created by jgson on 2017. 12. 7..
 */

public class PreExperienceGridAdapter extends BasicListAdapter<BasicListHolder<SingleUrlImageBinding, PreExperience>, PreExperience> {

    private LinearLayout.LayoutParams params;

    public PreExperienceGridAdapter(ArrayList<PreExperience> arrayList) {
        super(arrayList);
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, Resources.getSystem().getDisplayMetrics());
        int size = (Resources.getSystem().getDisplayMetrics().widthPixels - px) / 3 ;
        params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.height = size;
        params.width = size;
    }

    @Override
    public BasicListHolder<SingleUrlImageBinding, PreExperience> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_url_image, parent, false);
        SingleUrlImageBinding imageBinding = DataBindingUtil.bind(view);
        return new BasicListHolder<SingleUrlImageBinding, PreExperience>(imageBinding) {
            @Override
            public void setDataBindingWithData(PreExperience data) {
                getDataBinding().setUrl(data.thumbnail);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<SingleUrlImageBinding, PreExperience> holder, int position) {
        ViewCompat.setTransitionName(holder.getDataBinding().singleImg, getObservableArrayList().get(position).image);
        holder.getDataBinding().singleImg.setLayoutParams(params);
        holder.bind(getObservableArrayList().get(position));
    }
}
