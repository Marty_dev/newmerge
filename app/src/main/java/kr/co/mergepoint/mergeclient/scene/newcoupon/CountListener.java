package kr.co.mergepoint.mergeclient.scene.newcoupon;

/**
 * User: Marty
 * Date: 2018-10-16
 * Time: 오후 4:06
 * Description:
 */
public interface CountListener {
    public void refreshCount(int count);
}
