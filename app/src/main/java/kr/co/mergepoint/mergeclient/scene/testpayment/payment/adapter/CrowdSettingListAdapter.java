package kr.co.mergepoint.mergeclient.scene.testpayment.payment.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.MergeBaseAdapter;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.CrowdSettingActivity;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.Attendee;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.CrowdInfo;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.CrowdTicketbody;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopTicket;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.view.MergeBottomDialog;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * User: Marty
 * Date: 2018-07-22
 * Time: 오후 8:11
 * Description:
 */
public class CrowdSettingListAdapter extends MergeBaseAdapter<Attendee,CrowdSettingViewholder> {


    long payShopRef;
    CrowdSettingListAdapter adapter = this;
    CrowdSettingActivity activity ;
    public CrowdSettingListAdapter(int layout, BaseActivity mCon, long payShopRef) {
        super(layout, mCon);
        this.payShopRef = payShopRef;
        activity = (CrowdSettingActivity)mCon;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(mLayout, parent, false);
//        R.layout.crowd_point_item

        CrowdSettingViewholder vh = new CrowdSettingViewholder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Attendee at = arrayList.get(position);
        CrowdSettingViewholder vh = (CrowdSettingViewholder)holder;



        activity.crowdPointTv.setText(String.format("%,d",activity.crowdInfo.remainPayPrice )+ "원");
        if (at.companyPoints == 0 && at.tickets.size() != 0 && at.usePoints == 0){
            vh.crowdMemberPointrl.setVisibility(View.GONE);
        }else if (at.companyPoints != 0 && at.tickets.size() == 0 && (at.ticketName == null || at.ticketName.isEmpty())) {
            vh.crowdMemberTicketrl.setVisibility(View.GONE);
        }

        vh.crowdMembername.setText(at.memberName);
        vh.crowdMemberHavepoint.setText(String.format("%,d",at.companyPoints) + "P");
        vh.crowdMemberPointedt.setText((at.usePoints == 0 ? null : ((String.format("%,d",at.usePoints) + "P"))));
        vh.crowdMemberTickettv.setText(at.ticketName == null ? "식권 선택" : at.ticketName);
        vh.crowdTicketCounttv.setText(at.ticketUseCount + "");
        vh.crowdMemberTicketdiscription.setText(at.ticketDescription == null ? ""  : at.ticketDescription);

        vh.crowdDelete.setOnClickListener(view1-> {
            activity.apis.requestdeleteCrowdMember(at.crowdPayRef).enqueue(getCrowdPayCallback());

        });
        vh.crowdMemberPointedt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE){
                    int price = 0;
                    try{
                        price = Integer.parseInt(textView.getText().toString().replace("P","").replace(",",""));
                    }catch (Exception e){
                        price = 0;
                    }
                    setEnterPoints(at.memberRef,price,at.ticketRef,at.ticketUseCount,at.crowdPayRef);
                    return true;
                }
                return false;
            }
        });
        vh.crowdMemberPointconfirm.setOnClickListener(view2->{
            int price = 0;
            try{
                price = Integer.parseInt(vh.crowdMemberPointedt.getText().toString().replace("P","").replace(",",""));
            }catch (Exception e){
                price = 0;
            }
            setEnterPoints(at.memberRef,price,at.ticketRef,at.ticketUseCount,at.crowdPayRef);

        });

        vh.crowdMemberTicketsp.setOnClickListener(view1->{
            MergeBottomDialog bottomDialog = MergeBottomDialog.getInstance(mCon,(int)at.ticketRef,true);
            bottomDialog.setTicket(at.tickets,(ticket -> {
                if (ticket != null) {
                    MDEBUG.debug("ticke oid"  + ticket.oid);
                    this.setEnterPoints(at.memberRef,at.usePoints,ticket.oid,1,at.crowdPayRef);
                }else{
                    this.setEnterPoints(at.memberRef,at.usePoints,0,0,at.crowdPayRef);

                }
            }));
            bottomDialog.show(mCon.getSupportFragmentManager(),"TicketBottomDialog");
        });
        vh.crowdTicketPlus.setOnClickListener(view1->{
            MDEBUG.debug("???");
            if (at.ticketRef== 0) return;
            int ticketlength = 1;

            for (ShopTicket ticket: at.tickets) {
                if (ticket.oid == at.ticketRef)
                    ticketlength = ticket.remainTicketCount;
            }
            if (Integer.parseInt(vh.crowdTicketCounttv.getText().toString())
                    == ticketlength)
                return;
            // TODO: 2018-07-20 ticket count ++
            setEnterPoints(at.memberRef,at.usePoints,at.ticketRef,at.ticketUseCount+1,at.crowdPayRef);
        });
        vh.crowdTicketMinus.setOnClickListener(view1->{
            MDEBUG.debug("???");
            if (at.ticketRef == 0) return;
            if (Integer.parseInt(vh.crowdTicketCounttv.getText().toString())
                    <= 1)
                return;
            // TODO: 2018-07-20 ticket count --
            setEnterPoints(at.memberRef,at.usePoints,at.ticketRef,at.ticketUseCount-1,at.crowdPayRef);

        });

    }


    public void setEnterPoints(long oid,long points,long ticketRef, long ticketCount ,long crowdPayRef){
        CrowdTicketbody body = new CrowdTicketbody(payShopRef,crowdPayRef,oid,points,ticketRef,ticketCount);
        MergeApplication.getMergeApplication().showLoading(activity);

        activity.apis.requestCrowdPoints(body).enqueue(getCrowdPayCallback());
    }

    public Callback<ResponseObject<CrowdInfo>> getCrowdPayCallback(){
        return new Callback<ResponseObject<CrowdInfo>>() {
            @Override
            public void onResponse(Call<ResponseObject<CrowdInfo>> call, Response<ResponseObject<CrowdInfo>> response) {
                if (response.body().isFailed())
                    new MergeDialog.Builder(activity).setContent(response.body().getMessage()).setCancelBtn(false).build().show();
                else{
                    ((CrowdSettingActivity) mCon).crowdInfo = response.body().getObject();
                    ((CrowdSettingActivity) mCon).initList(response.body().getObject().attendees);
                }
                MergeApplication.getMergeApplication().hideLoading(activity);

            }
            @Override
            public void onFailure(Call<ResponseObject<CrowdInfo>> call, Throwable t) {
                new MergeDialog.Builder(activity).setContent(R.string.retry).setCancelBtn(false).build().show();
                MergeApplication.getMergeApplication().hideLoading(activity);
            }

        };
    }
}
