package kr.co.mergepoint.mergeclient.scene.main.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;

/**
 * Created by jgson on 2017. 5. 31..
 */

@MainScope
@Component(modules = MainModule.class)
public interface MainComponent {
    void inject(MainActivity activity);
}
