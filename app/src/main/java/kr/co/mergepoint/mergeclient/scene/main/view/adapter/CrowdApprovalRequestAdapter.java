package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.databinding.DataBindingUtil;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.CrowdApprovalRequestItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.Ticket;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class CrowdApprovalRequestAdapter extends BasicListAdapter<BasicListHolder<CrowdApprovalRequestItemBinding, CrowdPay>, CrowdPay> {

    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener;

    public CrowdApprovalRequestAdapter(ArrayList<CrowdPay> arrayList, CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        super(arrayList);
        this.onCheckedChangeListener = onCheckedChangeListener;
    }

    @Override
    public BasicListHolder<CrowdApprovalRequestItemBinding, CrowdPay> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.crowd_approval_request_item, parent, false);
        CrowdApprovalRequestItemBinding itemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<CrowdApprovalRequestItemBinding, CrowdPay>(itemBinding) {
            @Override
            public void setDataBindingWithData(CrowdPay data) {
                getDataBinding().setCrowdPay(data);
                getDataBinding().setTicketTotalPrice(String.format(Locale.getDefault(),"%,dP", data.allocatePoint - data.usePoints));
                getDataBinding().setTicketName(getTicketNameText(data.comPayPoints));
                getDataBinding().setTicketPrice(getTicketPriceText(data.comPayPoints));
                getDataBinding().setCheckListener(onCheckedChangeListener);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<CrowdApprovalRequestItemBinding, CrowdPay> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }

    private String getTicketNameText(ArrayList<Ticket> tickets) {
        ArrayList<String> ticketArray = new ArrayList<>();
        for (Ticket ticket : tickets) {
            ticketArray.add(ticket.ticketName + " X" + String.valueOf(ticket.useCount));
        }
        return TextUtils.join("\n", ticketArray);
    }

    private String getTicketPriceText(ArrayList<Ticket> tickets) {
        ArrayList<String> ticketArray = new ArrayList<>();
        for (Ticket ticket : tickets) {
            ticketArray.add(String.format(Locale.getDefault(),"%,dP", ticket.ticketUnitPrice));
        }
        return TextUtils.join("\n", ticketArray);
    }
}
