package kr.co.mergepoint.mergeclient.scene.payment.presenter;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BasePresenter;
import kr.co.mergepoint.mergeclient.databinding.PaymentShopListBinding;
import kr.co.mergepoint.mergeclient.scene.data.basket.AddBasket;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;
import kr.co.mergepoint.mergeclient.scene.data.payment.PayConfirm;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.payment.model.PaymentModel;
import kr.co.mergepoint.mergeclient.scene.payment.presenter.callback.PaymentCallback;
import kr.co.mergepoint.mergeclient.scene.payment.view.PaymentView;
import kr.co.mergepoint.mergeclient.scene.payment.view.adapter.CouponSpinnerAdapter;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.CardPaymentWebFragment;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.EnterpriseSelectPointsFragment;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.EnterpriseUsePointsFragment;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.SearchCrowdPayUserFragment;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.CrowdApprovalListener;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.SearchCrowdPayUserListener;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static android.content.ContentValues.TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CARD_PAYMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_ROLE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_SELECT_POINTS_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_USE_POINTS_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAYMENT_RESULT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAYMENT_RESULT_ARRAY;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SEARCH_CROWD_PAY_FRAGMENT_TAG;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class PaymentPresenter extends BasePresenter<PaymentView, PaymentModel, PaymentCallback>
        implements AdapterView.OnItemSelectedListener, View.OnTouchListener, TextView.OnEditorActionListener, SearchCrowdPayUserListener, CrowdApprovalListener {

    public boolean  _isBillingOkByEnterPrizePoint;
    public String _isBillingOkByEnterPrizePointMsg ;
    public int _shoptype;
    public int _payPrice;

    public PaymentPresenter(PaymentView baseView, PaymentModel baseModel, PaymentCallback callback) {
        super(baseView, baseModel, callback);
    }

    public void onResume(){
//        baseModel.readPayment(baseCallback.readPayment(this), baseView.getPayConfirm().oid);
    }

    @Override
    protected void setBindingProperty() {
        baseView.binding.setTitle(baseView.getString(R.string.payment_title));
        baseView.binding.setOnClick(this);


        Member member = MergeApplication.getMember();
        baseView.binding.setIsEnterpriseUser(member != null && member.getRoles().indexOf(ENTERPRISE_ROLE) >= 0);
        MDEBUG.debug("is Enter Prise User  : " +(member != null && member.getRoles().indexOf(ENTERPRISE_ROLE) >= 0));
    }


    private AddBasket addBasket;
    private ArrayList<Integer> oids;
    public void onCreate() {
        Intent intent= baseView.activity.getIntent();

        addBasket = intent.getParcelableExtra(PAYMENT_RESULT);
        oids = intent.getIntegerArrayListExtra(PAYMENT_RESULT_ARRAY);

        refreshData();


        baseView.showLoading();
    }

    public void refreshData(){
        MDEBUG.debug("pn Create");
        if (addBasket != null) {
            MDEBUG.debug("addBasket");
            baseModel.requestPayment(baseCallback.getPayConfirmCallback(this), addBasket);
        } else if (oids != null) {
            MDEBUG.debug("oid");
            baseModel.requestPayment(baseCallback.getPayConfirmCallback(this), oids);
        }
    }

    public void paymentSuccess() {
        Log.d(TAG, "paymentSuccess: 성공");
        baseView.paymentResult(true, "");
    }

    public void paymentFailure(String error) {
        Log.d(TAG, "paymentFailure: " + error);
        baseView.paymentResult(false, error);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) throws NullPointerException {
        CardPaymentWebFragment paymentWebFragment = (CardPaymentWebFragment) baseView.findFragmentByTag(CARD_PAYMENT_TAG);
        if ((keyCode == KeyEvent.KEYCODE_BACK) && paymentWebFragment.getWebView().canGoBack()) {
            paymentWebFragment.getWebView().goBack();
            return true;
        }
        return false;
    }

    public void onNewIntent(Intent intent) {
        CardPaymentWebFragment paymentWebFragment = (CardPaymentWebFragment) baseView.findFragmentByTag(CARD_PAYMENT_TAG);
        WebView paymentWeb = paymentWebFragment.getWebView();
        Log.e("===============", "onNewIntent!!");
        if (intent != null) {
            if (Intent.ACTION_VIEW.equals(intent.getAction())) {
                Uri uri = intent.getData();
                if (uri != null) {

                    Log.e("================uri", uri.toString());
                    if (String.valueOf(uri).startsWith("ISP 커스텀스키마를 넣어주세요")) { // ISP 커스텀스키마를 넣어주세요
                        String result = uri.getQueryParameter("result");
                        if ("success".equals(result)) {
                            paymentWeb.loadUrl("javascript:doPostProcess();");
                        } else if ("cancel".equals(result)) {
                            paymentWeb.loadUrl("javascript:doCancelProcess();");
                        } else {
                            paymentWeb.loadUrl("javascript:doNoteProcess();");
                        }
                    } else if (String.valueOf(uri).startsWith("계좌이체 커스텀스키마를 넣어주세요")) { // 계좌이체 커스텀스키마를 넣어주세요
                        /*계좌이체는 WebView가 아무일을 하지 않아도 됨*/
                    } else if (String.valueOf(uri).startsWith("paypin 커스텀스키마를 넣어주세요")) { // paypin 커스텀스키마를 넣어주세요
                        paymentWeb.loadUrl("javascript:doPostProcess();");
                    } else if (String.valueOf(uri).startsWith("paynow 커스텀스키마를 넣어주세요")) { // paynow 커스텀스키마를 넣어주세요
                        /*paynow는 WebView가 아무일을 하지 않아도 됨*/
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.payment_start:
                if (baseView.isConfirmUserBilling()) {
                    MDEBUG.debug("shop type : " + _shoptype);
                    if (baseView.getPayConfirm().orderPrice == 0||_isBillingOkByEnterPrizePoint
                            || (_shoptype != 2 && _shoptype != 3) )
                        baseView.openCardPaymentPage();
                    else{
                        new MergeDialog.Builder(baseView.activity).setContent(R.string.check_payment_enterpoint).setCancelBtn(false).build().show();
                    }
                } else {
                    new MergeDialog.Builder(baseView.activity).setContent(R.string.check_payment_checkbox).setCancelBtn(false).build().show();
                }
                break;
            case R.id.info_provide:
                baseView.openInfoProvideWeb();
                break;
            case R.id.point_apply:
                sendPoint(view);
                break;
            case R.id.all_points_apply:
                applyAllPoint(view);
                break;
            case R.id.enterprise_select_button:
                baseView.showLoading();
                int payShopOid = (int) view.getTag();
                baseModel.requestCrowedPay(baseCallback.requestCrowedPayCallback(payShopOid), payShopOid);
                break;
            case R.id.select_user_point:
                baseView.showLoading();
                baseModel.requestSettingCrowedPayPoint(baseCallback.requestSettingCrowedPayPoint(), (int) view.getTag());
                break;
            case R.id.search_crowd_pay_user:
                baseView.openSearchCrowdMember();
                break;
            case R.id.enterprise_point_apply:
                baseView.showLoading();
                baseModel.requestCrowdApproval(baseCallback.requestCrowdApproval(this), (int) view.getTag());
//                refreshData();
                break;
            case R.id.add_crowd_pay_user:
                SearchCrowdPayUserFragment userFragment = (SearchCrowdPayUserFragment) baseView.findFragmentByTag(SEARCH_CROWD_PAY_FRAGMENT_TAG);
                EnterpriseUsePointsFragment pointsFragment = (EnterpriseUsePointsFragment) baseView.findFragmentByTag(ENTERPRISE_USE_POINTS_FRAGMENT_TAG);
                int shopRef = pointsFragment.getPayShopRef();
                String oids = userFragment.getSelectUserOid();
                baseModel.requestSelectCrowedMember(baseCallback.requestSelectCrowedMember(), shopRef, oids);
                baseView.showLoading();
                baseView.activity.onBackPressed();
                break;
            case R.id.enterprise_point_approval:
                MDEBUG.debug("this?");
                EnterpriseSelectPointsFragment selectPointsFragment = (EnterpriseSelectPointsFragment) baseView.findFragmentByTag(ENTERPRISE_SELECT_POINTS_FRAGMENT_TAG);
                if (selectPointsFragment != null) {
                    baseView.showLoading();
                    baseModel.requestSelectPoint(baseCallback.requestSelectPoint(), selectPointsFragment.getSelectPoint());
                }
                break;
            case R.id.crowd_favorite_button:
                baseView.showLoading();
                baseModel.changeFavoriteCrowedMember(baseCallback.changeFavoriteCrowdMember(), (int) view.getTag());
                break;
            case R.id.approval_delete:
            case R.id.approval_reject_delete:
                /* 시간 초과 및 거절 됬을 때 요청 삭제 */
                baseView.showLoading();
                baseModel.deleteCrowdApproval(baseCallback.updateCrowdApproval(), (int) view.getTag());
                break;
            case R.id.approval_retry:
                /* 시간 초과 됬을 때 재시도 */
                baseView.showLoading();
                baseModel.retryCrowdApproval(baseCallback.updateCrowdApproval(), (int) view.getTag());
                break;
            case R.id.delete_user:
                /* 기업포인트 사용화면에서 사용자를 삭제할 때 */
                baseView.showLoading();
                baseModel.deleteCrowdApproval(baseCallback.requestSelectCrowedMember(), (int) view.getTag());
                break;
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            sendPoint(textView);
            return true;
        }
        return false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            view.setFocusable(true);
            switch (view.getId()) {
                case R.id.basket_select_coupon:
                    baseView.setBasketSelectCoupon(baseModel.getBasketCoupons());
                    break;
                case R.id.shop_select_coupon:
                    baseView.setShopSelectCoupon(view, baseModel.getMergeCoupons());
                    break;
                default:
                    break;
            }
        }

        view.performClick();
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.isFocusable()) {
            CouponSpinnerAdapter couponSpinnerAdapter = (CouponSpinnerAdapter) parent.getAdapter();
            PaymentShopListBinding listBinding = (PaymentShopListBinding) baseView.binding.getPaymentShopListAdapter().getViewDataBinding(view);
            if (couponSpinnerAdapter != null) {
                Coupon coupon = (Coupon) couponSpinnerAdapter.getItem(position);
                int couponOid = coupon.oid;
                switch (parent.getId()) {
                    case R.id.basket_select_coupon:
                        int payConfirmOid = baseView.binding.getPayConfirm().oid;
                        baseView.showLoading();
                        baseModel.requestPaymentCoupon(baseCallback.getPayConfirmCallback(this, parent, couponOid, payConfirmOid), payConfirmOid, couponOid);
                        break;
                    case R.id.shop_select_coupon:
                        if (listBinding != null) {
                            int shopPaymentOid = listBinding.getShopPayment().oid;
                            baseView.showLoading();
                            baseModel.requestPaymentShopCoupon(baseCallback.getPayConfirmCallback(this, parent, couponOid, shopPaymentOid), shopPaymentOid, couponOid);
                        }
                        break;
                    default:
                        break;
                }
            }
            parent.setFocusable(false);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}

    private void applyAllPoint(View view) {
        int availablePoint = baseView.getPayConfirm().availablePoints;
        if (availablePoint >= 3000) {

//            baseModel.requestPaymentShopPoint(baseCallback.getPayConfirmCallback(this, message), (int) view.getTag(), point);
            PayConfirm shopPayment = baseView.getPayConfirm();




            EditText editText = baseView.getPayShopPointView(view);
            int payPrice = shopPayment.getPayPrice();
            editText.setText(String.valueOf(availablePoint <= payPrice ? availablePoint : payPrice));
            sendPoint(editText);
        } else {
            new MergeDialog.Builder(baseView.activity).setContent(R.string.available_minimum_points).setCancelBtn(false).build().show();
        }
    }

    private void sendPoint(View view) {
        EditText editText = baseView.getPayShopPointView(view);
        if (editText != null) {
            String pointStr = editText.getText().toString().trim();
            if (!pointStr.isEmpty()) {
                baseView.showLoading();
                /* 콤마 포함된 문자열 포인트뷰에서 숫자 포인트로 변경 */
                int point = Integer.parseInt(pointStr.replaceAll(",", ""));

                /* PayConfirm에서 가용 포인트를 확인해서 포인트 상태 업데이트 */
                baseModel.checkPointState(point, baseView.getPayConfirm().availablePoints);

                /* 포인트 상태에 따른 메시지 가져오고 상태에 따라 EditText 초기화 */
                int messageInt = baseModel.checkPointMessage(editText);
                String message = messageInt > 0 ? baseView.getString(messageInt) : "";

                editText.clearFocus();
                baseView.hideKeyboard();

                baseModel.requestPaymentShopPoint(baseCallback.getPayConfirmCallback(this, message), (int) view.getTag(), point);
                baseModel.usedPoints.put(editText, point);
            }
        }
    }

    @Override
    public void updateCrowedMemberTable(int type, String searchString) {
        baseView.showLoading();
        baseModel.getCrowedMemberList(baseCallback.getCrowedMemberList(), type, searchString.isEmpty() ? null : searchString);
    }

    @Override
    public void endApproval() {
        MDEBUG.debug("end Approval");

        baseView.backToPayment();
        baseView.showLoading();
        baseModel.readPayment(baseCallback.readPayment(this), baseView.getPayConfirm().oid);
    }



}
