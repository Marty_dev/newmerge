package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;

/**
 * Created by 1017sjg on 2017. 6. 13..
 */

public class BaseFragment extends Fragment implements View.OnClickListener{

    protected Call preCall;
    protected BaseActivity rootActivity;
    protected FragmentLifeCycle lifeCycle;

    public BaseActivity getRootActivity() {
        return rootActivity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MDEBUG.debug("Fragment is onCreate");
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity)
            rootActivity = (BaseActivity) context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (lifeCycle != null)
            lifeCycle.onFragmentDestroy();
    }

    @Override
    public void onClick(View v) {
        if (rootActivity != null)
            rootActivity.onClick(v);
    }

    public void showAlert(String message) {
        if (getActivity() != null)
            Snackbar.make(getActivity().findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT).show();
    }

    public void showAlert(int message) {
        if (getActivity() != null)
            Snackbar.make(getActivity().findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT).show();
    }

    protected void delayHideLoading() {
        new Handler().postDelayed(() -> MergeApplication.getMergeApplication().hideLoading(rootActivity), 300);
    }
}
