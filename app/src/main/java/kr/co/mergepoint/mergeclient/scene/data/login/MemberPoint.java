package kr.co.mergepoint.mergeclient.scene.data.login;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.payment.UseTicket;

/**
 * Created by 1017sjg on 2017. 6. 16..
 */

public class MemberPoint extends BaseObservable implements Parcelable {

    @SerializedName("memberRef")
    public int memberRef;

    @SerializedName("companyPoints")
    public int companyPoints;

    @SerializedName("crowdPayRef")
    public int crowdPayRef;

    @SerializedName("payPoints")
    public int payPoints;

    @SerializedName("useTickets")
    public ArrayList<UseTicket> useTickets;

    private MemberPoint(Parcel in) {
        memberRef = in.readInt();
        companyPoints = in.readInt();
        crowdPayRef = in.readInt();
        payPoints = in.readInt();

        in.readTypedList(useTickets, UseTicket.CREATOR);
    }

    public static final Creator<MemberPoint> CREATOR = new Creator<MemberPoint>() {
        @Override
        public MemberPoint createFromParcel(Parcel in) {
            return new MemberPoint(in);
        }

        @Override
        public MemberPoint[] newArray(int size) {
            return new MemberPoint[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(memberRef);
        dest.writeInt(companyPoints);
        dest.writeInt(crowdPayRef);
        dest.writeInt(payPoints);
        dest.writeTypedList(useTickets);
    }
}
