package kr.co.mergepoint.mergeclient.application.common.script;

/**
 * Created by jgson on 2017. 6. 5..
 */

public interface LoginScriptListener {
    void signup(String memberString);
    void signin(boolean result, String memberString);
    void inputEmail(String instaId, String facebookId, String social);
}
