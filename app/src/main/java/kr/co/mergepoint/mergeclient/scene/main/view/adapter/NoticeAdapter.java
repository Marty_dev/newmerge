package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.application.common.DateModel;
import kr.co.mergepoint.mergeclient.databinding.NoticeListItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.Notice;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class NoticeAdapter extends BasicListAdapter<BasicListHolder<NoticeListItemBinding, Notice>, Notice> {

    private DateModel dateModel;

    public NoticeAdapter(ArrayList<Notice> notices) {
        super(notices);
        dateModel = new DateModel();
    }

    @Override
    public BasicListHolder<NoticeListItemBinding, Notice> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notice_list_item, parent, false);
        NoticeListItemBinding listItemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<NoticeListItemBinding, Notice>(listItemBinding) {
            @Override
            public void setDataBindingWithData(Notice data) {
                getDataBinding().setNotice(data);
                getDataBinding().setIsNew(dateModel.getCompareDateOfDays(3, dateModel.getDate(data.creDateTime)));
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<NoticeListItemBinding, Notice> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
