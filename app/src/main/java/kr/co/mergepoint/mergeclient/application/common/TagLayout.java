package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.graphics.Canvas;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.scene.data.splash.CategoryInfo;

import static android.support.constraint.ConstraintSet.BASELINE;
import static android.support.constraint.ConstraintSet.BOTTOM;
import static android.support.constraint.ConstraintSet.CHAIN_PACKED;
import static android.support.constraint.ConstraintSet.END;
import static android.support.constraint.ConstraintSet.PARENT_ID;
import static android.support.constraint.ConstraintSet.START;
import static android.support.constraint.ConstraintSet.TOP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CENTER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

/**
 * Created by 1017sjg on 2017. 7. 12..
 */

public class TagLayout extends ConstraintLayout {

    private Context context;

    public TagLayout(Context context) {
        super(context);
        init(context);
    }

    public TagLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TagLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
    }

    private int getTextWidth(TextView textView) {
        String text = textView.getText().toString();
        int length = text.length();
        float addWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, context.getResources().getDisplayMetrics());
        return (int) (textView.getPaint().measureText(text, 0, length) + addWidth);
    }

    private TextView createTextView(boolean isBlack, String text) {
        int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8.7f, context.getResources().getDisplayMetrics());
        TextView textView = new TextView(context);
        textView.setTypeface(ResourcesCompat.getFont(context, R.font.spoqa));
        textView.setId(View.generateViewId());
        textView.setTextColor(ContextCompat.getColor(context, isBlack ? R.color.gray_07 : R.color.white_02));
        textView.setText(text);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
        textView.setGravity(Gravity.CENTER);
        textView.setPadding(padding,0,0,0);
        textView.setBackgroundResource(isBlack ? R.drawable.icon_thema_tag_g : R.drawable.icon_thema_white_tag);
        return textView;
    }

    /* 태그 자동 생성 */
    public void setTagView(@Properties.Gravity int gravity, boolean isBlack, ArrayList<String> category, ArrayList<String> theme) {
        removeAllViews();
        ArrayList<CategoryInfo> categoryInfo = MergeApplication.getInitialInfo().getCategory();
        if (category != null && categoryInfo != null) {
            ArrayList<String> categoryName = new ArrayList<>();
            for (CategoryInfo info : categoryInfo) {
                if (category.contains(info.code))
                    categoryName.add(info.name);
            }
            ConstraintLayout layout = this;
            ConstraintSet set = new ConstraintSet();
            set.clone(layout);
            int baseID = NO_VALUE;
            int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, context.getResources().getDisplayMetrics());
            for (int i = 0; i < categoryName.size(); i++) {
                TextView textView = createTextView(isBlack, categoryName.get(i));
                layout.addView(textView);

                if (i == 0) {
                    set.connect(textView.getId(), START, PARENT_ID, START);
                    set.connect(textView.getId(), TOP, PARENT_ID, TOP);
                    set.connect(textView.getId(), BOTTOM, PARENT_ID, BOTTOM);
                    if (category.size() == 1 && gravity == CENTER)
                        set.connect(textView.getId(), END, PARENT_ID, END);
                } else {
                    set.connect(textView.getId(), BASELINE, baseID, BASELINE);
                    set.connect(textView.getId(), START, baseID, END, margin);
                    if (gravity == CENTER)
                        set.connect(baseID, END, textView.getId(), START);
                    if (i == category.size() - 1 && gravity == CENTER)
                        set.connect(textView.getId(), END, PARENT_ID, END);
                }

                if (gravity == CENTER)
                    set.setHorizontalChainStyle(textView.getId(), CHAIN_PACKED);
                int width = getTextWidth(textView);
                set.constrainWidth(textView.getId(), width);
                set.constrainHeight(textView.getId(), (int) context.getResources().getDimension(R.dimen.tag_height));
                baseID = textView.getId();
            }
            set.applyTo(layout);
        }
    }
    /* 태그 자동 생성 */
    public void setTagView(boolean isBlack, ArrayList<String> category, ArrayList<String> theme) {
        removeAllViews();
        ArrayList<CategoryInfo> categoryInfo = MergeApplication.getInitialInfo().getCategory();
        if (category != null && categoryInfo != null) {
            ArrayList<String> categoryName = new ArrayList<>();
            for (CategoryInfo info : categoryInfo) {
                if (category.contains(info.code))
                    categoryName.add(info.name);
            }
            ConstraintLayout layout = this;
            ConstraintSet set = new ConstraintSet();
            set.clone(layout);
            int baseID = NO_VALUE;
            int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, context.getResources().getDisplayMetrics());
            for (int i = 0; i < categoryName.size(); i++) {
                TextView textView = createTextView(isBlack, categoryName.get(i));
                layout.addView(textView);

                if (i == 0) {
                    set.connect(textView.getId(), START, 1, START);
                    set.connect(textView.getId(), TOP, 1, TOP);
                    set.connect(textView.getId(), BOTTOM, 1, BOTTOM);
                    set.connect(textView.getId(), END, PARENT_ID, END);
                } else {
                    set.connect(textView.getId(), BASELINE, baseID, BASELINE);
                    set.connect(textView.getId(), START, baseID, END, margin);
                    set.connect(baseID, END, textView.getId(), START);
                    set.connect(textView.getId(), END, PARENT_ID, END);
                }

                set.setHorizontalChainStyle(textView.getId(), CHAIN_PACKED);
                int width = getTextWidth(textView);
                set.constrainWidth(textView.getId(), width);
                set.constrainHeight(textView.getId(), (int) context.getResources().getDimension(R.dimen.tag_height));
                baseID = textView.getId();
            }
            set.applyTo(layout);
        }
    }
}
