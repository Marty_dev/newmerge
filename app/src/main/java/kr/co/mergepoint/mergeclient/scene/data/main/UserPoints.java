package kr.co.mergepoint.mergeclient.scene.data.main;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 9. 13..
 */

public class UserPoints {

    @SerializedName("totalPoints")
    public int totalPoints;

    @SerializedName("pointsList")
    public ArrayList<Point> pointsList;
}
