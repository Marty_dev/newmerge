package kr.co.mergepoint.mergeclient.scene.franchise.adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.ShopApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.BookMark;
import kr.co.mergepoint.mergeclient.application.common.MergeBaseAdapter;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.ShopInfo;
import kr.co.mergepoint.mergeclient.scene.franchise.adapter.viewholder.NewShop_ViewHolder;
import kr.co.mergepoint.mergeclient.scene.franchise.data.FranchisesShop;
import kr.co.mergepoint.mergeclient.scene.login.LoginActivity;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.CUSTOM;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO;

/**
 * User: Marty
 * Date: 2018-10-02
 * Time: 오전 10:17
 * Description:
 */
public class FranchiseShopAdapter extends MergeBaseAdapter<Shop,NewShop_ViewHolder>{
    public FranchiseShopAdapter(BaseActivity mCon) {
        super(R.layout.newshop_list_item, mCon);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        NewShop_ViewHolder viewHolder;
        View v = inflater.inflate(mLayout,parent,false);
        viewHolder = new NewShop_ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NewShop_ViewHolder v  = (NewShop_ViewHolder)holder;
        Shop item = arrayList.get(position);

        if (item.backImage != null)
            Picasso.with(mCon).load(MergeApplication.getMergeApplication().getImageUrlFiltered(item.backImage)).into(v.backImg);
        else
            v.backImg.setImageResource(R.drawable.img_store_list_background);
        v.FranName.setText(item.shopName);
        v.FranTime.setText(item.displayHour);
        v.Marknum.setText(""+item.favoriteCount);
        v.Heartnum.setText(""+item.likeCount);
        String distance = item.distance >= 1000 ? mCon.getString(R.string.km,((float) item.distance/1000)): (item.distance == 0 ? mCon.getString(R.string.zero_m) : mCon.getString(R.string.m,((float) item.distance)));
        v.Distance.setText(distance);
        v.bookMark.setFavoriteStatus(item.favorite);

        ArrayList<String> emptyItems = new ArrayList<>();
        v.Taglayout.setTagView(CUSTOM,false,item.categoryCodeRef == null ? emptyItems : item.categoryCodeRef,item.themeCodeRef == null ? emptyItems : item.themeCodeRef);

        v.Ordernum.setVisibility(item.paid == 0 ? View.GONE:View.VISIBLE);
        v.Ordernum.setText(item.paid + "건");
        v.bookMark.setCheckFavorite(new BookMark.OnCheckFavorite() {
            @Override
            public void checkFavorite(View view, boolean isLogin) {
                if (isLogin){
                    checkFavoriteCall(item.oid,position);
                }else{
                    new MergeDialog.Builder(mCon).setContent("북마크 기능은\n로그인후 이용가능합니다.")
                            .setCancelBtn(true).setCancelString("닫기")
                            .setConfirmBtn(true).setConfirmString("로그인")
                            .setConfirmClick(new MergeDialog.OnDialogClick(){
                                @Override
                                public void onDialogClick() {
                                    mCon.startActivity(new Intent(mCon, LoginActivity.class));
                                }
                            }).build().show();
                }
            }
        });
        v.backImg.setOnClickListener((view)->{
            Intent inte = new Intent(mCon,ShopActivity.class);
            inte.putExtra(SHOP_INFO,item.oid);
            mCon.startActivity(inte);
        });
    }

    private void checkFavoriteCall(int shopOid, int position) {
        ShopApi shopApi = MergeApplication.getMergeAppComponent().getRetrofit().create(ShopApi.class);
        Call<ResponseObject<Integer>> call = shopApi.checkFavorite(shopOid);
        call.enqueue(getCheckFavoriteCall(position));
    }

    private Callback<ResponseObject<Integer>> getCheckFavoriteCall(final int position) {
        return new Callback<ResponseObject<Integer>>() {
            @Override
            public void onResponse(@NonNull Call<ResponseObject<Integer>> call, @NonNull Response<ResponseObject<Integer>> response) {
                ResponseObject<Integer> responseObject = response.body();
                if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                    int count = responseObject.getObject();
                    Shop shop = arrayList.get(position);
                    if (shop != null) {
                        shop.favorite = !shop.favorite;
                        shop.setFavoriteCount(count);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseObject<Integer>> call, @NonNull Throwable t) {}
        };
    }

}
