package kr.co.mergepoint.mergeclient.scene.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import javax.inject.Inject;

import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.script.LoginScriptListener;
import kr.co.mergepoint.mergeclient.scene.login.dagger.DaggerLoginComponent;
import kr.co.mergepoint.mergeclient.scene.login.dagger.LoginModule;
import kr.co.mergepoint.mergeclient.scene.login.presenter.LoginPresenter;

/**
 * Created by jgson on 2017. 5. 31..
 */

public class LoginActivity extends BaseActivity implements LoginScriptListener {

    @Inject LoginPresenter loginPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerLoginComponent.builder().loginModule(new LoginModule(this)).build().inject(this);
        loginPresenter.onCreate();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginPresenter.onDestroy();
    }

    @Override
    protected void onActivityClick(View view) {
        loginPresenter.onClick(view);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginPresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void signup(String memberString) {
        loginPresenter.signup(memberString);
    }

    @Override
    public void signin(boolean result, String memberString) {
        loginPresenter.signin(result, memberString);
    }

    @Override
    public void inputEmail(String instaId, String facebookId, String social) {
        loginPresenter.inputEmail(instaId, facebookId, social);
    }
}
