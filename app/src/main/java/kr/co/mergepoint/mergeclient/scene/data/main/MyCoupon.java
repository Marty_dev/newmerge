package kr.co.mergepoint.mergeclient.scene.data.main;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class MyCoupon {
    @SerializedName("couponList")
    public ArrayList<Coupon> couponList;
}
