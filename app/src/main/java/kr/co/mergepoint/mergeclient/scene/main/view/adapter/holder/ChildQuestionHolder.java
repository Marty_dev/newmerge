package kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import kr.co.mergepoint.mergeclient.databinding.QuestionsChildBinding;
import kr.co.mergepoint.mergeclient.databinding.ShopMenuChildBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.Question;
import kr.co.mergepoint.mergeclient.scene.data.menu.ShopChildInfo;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class ChildQuestionHolder extends ChildViewHolder {

    private QuestionsChildBinding questionsChildBinding;

    public ChildQuestionHolder(QuestionsChildBinding questionsChildBinding) {
        super(questionsChildBinding.getRoot());
        this.questionsChildBinding = questionsChildBinding;
    }

    public void onBind(Question question) {
        questionsChildBinding.setContent(question.getContent());
    }
}
