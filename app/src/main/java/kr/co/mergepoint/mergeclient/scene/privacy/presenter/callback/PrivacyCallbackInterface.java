package kr.co.mergepoint.mergeclient.scene.privacy.presenter.callback;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.main.Company;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public interface PrivacyCallbackInterface {
    MergeCallback<ResponseObject<Member>> getUserPrivacy();

    MergeCallback<ResponseObject<Member>> returnModifyInfo();

    MergeCallback<ResponseObject<ArrayList<Company>>> getSearchEnterpriseListCallback();

    MergeCallback<ResponseObject<Boolean>> registerEnterpriseListCallback();
}
