package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.SearchDelegationUserBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.DelegationUserAdapter;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.DelegationUserListener;
import kr.co.mergepoint.mergeclient.scene.payment.view.adapter.SearchCrowdPayUserAdapter;

import static kr.co.mergepoint.mergeclient.application.common.Properties.APPROVAL_DELEGATION_NAME;
import static kr.co.mergepoint.mergeclient.application.common.Properties.APPROVAL_DELEGATION_OID;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SETTING_PREFS;

/**
 * Created by 1017sjg on 2017. 8. 8..
 */

public class DelegationUserFragment extends BaseFragment implements TabLayout.OnTabSelectedListener {

    private SearchDelegationUserBinding delegationUserBinding;
    private DelegationUserListener listener;
    private String delegationName;
    private int delegationOid;
    public CrowdMember getSelectCrowdMember() {
        return delegationUserBinding != null ? delegationUserBinding.getUserAdapter().getSelectCrowdMember() : null;
    }

    public static DelegationUserFragment newInstance(DelegationUserListener listener) {
        Bundle args = new Bundle();
        DelegationUserFragment fragment = new DelegationUserFragment();
        fragment.listener = listener;
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        delegationUserBinding = DataBindingUtil.inflate(inflater, R.layout.search_delegation_user, container, false);
        delegationUserBinding.setTitle("함께결제 승인위임");
        delegationUserBinding.setOnClick(this);
        delegationUserBinding.setTabListener(this);
        delegationUserBinding.setTabArray(new String[]{"전체", "나의 부서", "즐겨찾기"});
        delegationUserBinding.currentDelegationUserName.setText(delegationName);
        return delegationUserBinding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listener.updateDelegationTable(1, "");

        SharedPreferences sharedPreferences = getRootActivity().getSharedPreferences(SETTING_PREFS, Context.MODE_PRIVATE);
        delegationName = sharedPreferences.getString(APPROVAL_DELEGATION_NAME, "없음");
        delegationOid = sharedPreferences.getInt(APPROVAL_DELEGATION_OID,-1);
    }

    public void setDelegationUserAdapter(ArrayList<CrowdMember> arrayList) {
        delegationUserBinding.setUserAdapter(new DelegationUserAdapter(arrayList, this,delegationOid));
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition() + 1;
        delegationUserBinding.enterpriseSearchLayout.setVisibility(position == 3 ? View.GONE : View.VISIBLE);
        listener.updateDelegationTable(tab.getPosition() + 1, delegationUserBinding.searchText.getText().toString());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}

    @Override
    public void onTabReselected(TabLayout.Tab tab) {}
}
