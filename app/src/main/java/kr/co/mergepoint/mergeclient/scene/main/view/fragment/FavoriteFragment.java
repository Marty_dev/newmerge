package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.CustomRecyclerView;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocation;
import kr.co.mergepoint.mergeclient.databinding.FavoriteListBinding;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.ShopListAdapter;

/**
 * Created by 1017sjg on 2017. 8. 7..
 */

public class FavoriteFragment extends BaseFragment {

    private UserLocation userLocation;
    private FavoriteListBinding listBinding;
    private CustomRecyclerView.CustomRecyclerViewTouch recyclerViewTouch;

    public static FavoriteFragment newInstance(MainActivity mainActivity, UserLocation userLocation) {
        FavoriteFragment fragment = new FavoriteFragment();
        fragment.recyclerViewTouch = mainActivity;
        fragment.userLocation = userLocation;
        return fragment;
    }

    public UserLocation getUserLocation() {
        return userLocation;
    }

    public void setFavoriteAdapter(ArrayList<Shop> shopInfo) {
        listBinding.setListAdapter(new ShopListAdapter(shopInfo));
        listBinding.setListTouch(recyclerViewTouch);
        listBinding.getListAdapter().notifyDataSetChanged();
        listBinding.bookmarkEmpty.setVisibility(shopInfo != null && shopInfo.size() > 0 ? View.GONE : View.VISIBLE);
    }

    public void setFavorite(int oid, boolean favorite) {
        if (listBinding != null)
            listBinding.getListAdapter().singleShopFavoriteUpdate(oid, favorite);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        listBinding = DataBindingUtil.inflate(inflater, R.layout.favorite_list, container, false);
        listBinding.setTitle(getString(R.string.favorite_title));
        listBinding.setOnClick(this);
        return listBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        userLocation.startLocationUpdates();
    }
}
