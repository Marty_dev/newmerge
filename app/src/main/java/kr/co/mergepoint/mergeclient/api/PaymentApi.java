package kr.co.mergepoint.mergeclient.api;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.basket.AddBasket;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.data.payment.PayConfirm;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public interface PaymentApi {
    /*POST*/
    @POST("payment/request")
    Call<ResponseObject<PayConfirm>> requestPayment(@Body AddBasket basket);

    @POST("payment/request")
    @FormUrlEncoded
    Call<ResponseObject<PayConfirm>> requestPayment(@Field("oid") String oid);

    @GET("payment/read/{oid}")
    Call<ResponseObject<PayConfirm>> readPayment(@Path("oid") int shopPayOid);

    @POST("payment/modify/payment/{oid}")
    @FormUrlEncoded
    Call<ResponseObject<PayConfirm>> requestPaymentCoupon(@Path("oid") int oid, @Field("coupon") int coupon);

    @POST("payment/modify/shop/{oid}")
    @FormUrlEncoded
    Call<ResponseObject<PayConfirm>> requestPaymentShopCoupon(@Path("oid") int oid, @Field("coupon") int coupon);

    @POST("payment/modify/shop/{oid}")
    @FormUrlEncoded
    Call<ResponseObject<PayConfirm>> requestPaymentShopPoints(@Path("oid") int oid, @Field("point") int point);

    @POST("payment/cancel/{oid}")
    Call<ResponseObject<Integer>> requestPayCancel(@Path("oid") int oid);

    @POST("payment/toPoint/{oid}")
    Call<ResponseObject<Integer>> requestPointRefund(@Path("oid") int oid);

    /*GET*/
    @GET("payment/billPay/{oid}")
    Call<ResponseBody> requestEasyPayment(@Path("oid") int oid);
}
