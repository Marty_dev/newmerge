package kr.co.mergepoint.mergeclient.application.common;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.BR;

/**
 * Created by 1017sjg on 2017. 6. 19..
 */

public class WideBannerAdapter extends PagerAdapter {

    protected ArrayList<String> imgUrl;
    private View.OnClickListener onClickListener;
    private int emptyImg;
    private int layout;

    public WideBannerAdapter(int layout, ArrayList<String> imgUrl, int emptyImg, View.OnClickListener onClickListener) {
        this.imgUrl = imgUrl == null ? new ArrayList<>() : imgUrl;
        this.onClickListener = onClickListener;
        this.emptyImg = emptyImg;
        this.layout = layout;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(container.getContext()), layout, container, false);
        viewDataBinding.setVariable(BR.imgUrl, imgUrl.size() == 0 ? null : imgUrl.get(position));
        viewDataBinding.setVariable(BR.onClick, onClickListener);
        viewDataBinding.setVariable(BR.emptyImg, emptyImg);

        container.addView(viewDataBinding.getRoot());
        return viewDataBinding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return imgUrl.size() == 0 ? 1 : imgUrl.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
