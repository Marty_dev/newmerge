package kr.co.mergepoint.mergeclient.scene.payment.view.adapter.holder;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import kr.co.mergepoint.mergeclient.databinding.PaymentMenuChildBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.PaymentMenuChild;

/**
 * Created by 1017sjg on 2017. 9. 4..
 */

public class PaymentChildMenuHolder extends ChildViewHolder {

    private PaymentMenuChildBinding menuChildBinding;

    public PaymentChildMenuHolder(PaymentMenuChildBinding menuChildBinding) {
        super(menuChildBinding.getRoot());
        this.menuChildBinding = menuChildBinding;
    }

    public void onBind(PaymentMenuChild childInfo) {
        menuChildBinding.setChild(childInfo);
    }
}
