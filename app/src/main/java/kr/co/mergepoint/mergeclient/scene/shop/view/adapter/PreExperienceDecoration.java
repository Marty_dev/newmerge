package kr.co.mergepoint.mergeclient.scene.shop.view.adapter;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by 1017sjg on 2017. 7. 13..
 */

public class PreExperienceDecoration extends RecyclerView.ItemDecoration {

    private int widthSpacing;
    private int heightSpacing;

    public PreExperienceDecoration(int widthSpacing, int heightSpacing) {
        this.widthSpacing = widthSpacing;
        this.heightSpacing = heightSpacing;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        GridLayoutManager layoutManager = (GridLayoutManager) parent.getLayoutManager();
        int position = parent.getChildLayoutPosition(view);
        int spanCount = layoutManager.getSpanCount();
        int column = position % spanCount;
        getGridItemOffsets(outRect, position, column, spanCount);
    }

    private void getGridItemOffsets(Rect outRect, int position, int column, int spanCount) {
        outRect.left = widthSpacing * (spanCount - column) / spanCount;
        outRect.right = widthSpacing * (column + 1) / spanCount;
        if (position < spanCount) {
            outRect.top = heightSpacing;
        }
        outRect.bottom = heightSpacing;
    }
}
