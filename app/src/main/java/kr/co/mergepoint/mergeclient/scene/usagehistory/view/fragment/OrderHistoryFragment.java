package kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.HistoryApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.DateModel;
import kr.co.mergepoint.mergeclient.databinding.UsageHistoryOrderBinding;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter.OrderHistoryAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_ROLE;

/**
 * Created by 1017sjg on 2017. 7. 28..
 */

public class OrderHistoryFragment extends BaseFragment {

    private int orderOid;
    private DateModel dateModel;
    public UsageHistoryOrderBinding orderBinding;

    public static OrderHistoryFragment newInstance(int orderOid) {
        Bundle args = new Bundle();
        OrderHistoryFragment fragment = new OrderHistoryFragment();
        fragment.orderOid = orderOid;
        fragment.dateModel = new DateModel();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        orderBinding = DataBindingUtil.inflate(inflater, R.layout.usage_history_order, container, false);
        orderBinding.setTitle(getString(R.string.pay_history_title));
        orderBinding.setOnClick(this);
        Member member = MergeApplication.getMember();
        boolean isEnterpriseUser = member != null && member.getRoles().indexOf(ENTERPRISE_ROLE) >= 0;
        orderBinding.setIsEnterpriseUser(isEnterpriseUser);
        return orderBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        MergeApplication.getMergeApplication().showLoading(rootActivity);
        getOrderDetailHistory();
    }

    private boolean isPayCancel(ArrayList<Integer> date) {
        return date != null && dateModel.getCompareDateOfDays(7, dateModel.getDate(date));
    }

    public void getOrderDetailHistory() {
        HistoryApi api = MergeApplication.getMergeAppComponent().getRetrofit().create(HistoryApi.class);
        Call<ResponseObject<UsagePayment>> call =api.requestOrderDetail(orderOid);
        call.enqueue(getOrderDetailCallback());
    }

    private Callback<ResponseObject<UsagePayment>> getOrderDetailCallback() {
        return new Callback<ResponseObject<UsagePayment>>() {
            @Override
            public void onResponse(@Nullable Call<ResponseObject<UsagePayment>> call, @Nullable Response<ResponseObject<UsagePayment>> response) {
                if (response != null && response.isSuccessful()) {
                    ResponseObject<UsagePayment> orderHistory = response.body();
                    if (orderHistory != null) {
                        if (!orderHistory.isFailed()) {
                            UsagePayment usagePayment = orderHistory.getObject();
                            orderBinding.setPayment(usagePayment);
                            orderBinding.setIsPayCancel(usagePayment.useState == 1 ? isPayCancel(usagePayment.getPayTime()) : (usagePayment.useState != 2));
                            orderBinding.setAdapter(new OrderHistoryAdapter(usagePayment, getRootActivity()));
                        } else {
                            showAlert(orderHistory.getMessage());
                        }
                    } else {
                        showAlert(R.string.retry);
                    }
                }
                delayHideLoading();
            }

            @Override
            public void onFailure(@Nullable Call<ResponseObject<UsagePayment>> call, @Nullable Throwable t) {
                showAlert(R.string.retry);
                delayHideLoading();
            }
        };
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

    }
}
