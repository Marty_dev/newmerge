package kr.co.mergepoint.mergeclient.scene.data.main;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 9. 12..
 */

public class CombineImagesDetail implements Parcelable{

    @SerializedName("oid")
    public int oid;

    @SerializedName("bodyImage")
    public String bodyImage;

    @SerializedName("buttonHeight")
    public float buttonHeight;

    @SerializedName("buttonWidth")
    public float buttonWidth;

    @SerializedName("buttonLocation")
    public float buttonLocation;

    @SerializedName("actionType")
    public int actionType;

    @SerializedName("actionData")
    public String actionData;

    @SerializedName("creDateTime")
    public ArrayList<Integer> creDateTime;
    @SerializedName("modDateTime")
    public ArrayList<Integer> modDateTime;

    public CombineImagesDetail(Parcel in) {
        oid = in.readInt();
        bodyImage = in.readString();
        buttonHeight = in.readFloat();
        buttonWidth = in.readFloat();
        buttonLocation = in.readFloat();
        actionType = in.readInt();
        actionData = in.readString();
        in.readList(creDateTime, Integer.class.getClassLoader());
        in.readList(modDateTime, Integer.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(oid);
        dest.writeString(bodyImage);
        dest.writeFloat(buttonHeight);
        dest.writeFloat(buttonWidth);
        dest.writeFloat(buttonLocation);
        dest.writeInt(actionType);
        dest.writeString(actionData);
        dest.writeList(creDateTime);
        dest.writeList(modDateTime);
    }

    public static final Parcelable.Creator<CombineImagesDetail> CREATOR = new Parcelable.Creator<CombineImagesDetail>() {
        @Override
        public CombineImagesDetail createFromParcel(Parcel in) {
            return new CombineImagesDetail(in);
        }

        @Override
        public CombineImagesDetail[] newArray(int size) {
            return new CombineImagesDetail[size];
        }
    };
}
