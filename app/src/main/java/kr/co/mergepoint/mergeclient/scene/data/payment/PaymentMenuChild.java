package kr.co.mergepoint.mergeclient.scene.data.payment;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 1017sjg on 2017. 7. 10..
 */

public class PaymentMenuChild implements Parcelable {
    public int price;
    public int count;

    public PaymentMenuChild(int price, int count) {
        this.price = price;
        this.count = count;
    }

    public static final Parcelable.Creator<PaymentMenuChild> CREATOR = new Parcelable.Creator<PaymentMenuChild>() {
        @Override
        public PaymentMenuChild createFromParcel(Parcel in) {
            return new PaymentMenuChild(in);
        }

        @Override
        public PaymentMenuChild[] newArray(int size) {
            return new PaymentMenuChild[size];
        }
    };

    private PaymentMenuChild(Parcel in) {
        price = in.readInt();
        count = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeInt(price);
        dest.writeInt(count);
    }
}
