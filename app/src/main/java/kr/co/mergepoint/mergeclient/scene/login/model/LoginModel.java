package kr.co.mergepoint.mergeclient.scene.login.model;

import android.content.Intent;
import android.content.res.Resources;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.BuildConfig;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.UserApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.BaseModel;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.login.SocialSignup;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import okhttp3.ResponseBody;
import retrofit2.Callback;

import static kr.co.mergepoint.mergeclient.application.common.Properties.FACEBOOK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.GOOGLE_PROVIDER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.GOOGLE_SIGN_IN;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INSTA;
import static kr.co.mergepoint.mergeclient.application.common.Properties.KAKAO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NAVER;

/**
 * Created by jgson on 2017. 5. 31..
 */

public class LoginModel extends BaseModel {

    private UserApi userApi;
    private GoogleApiClient googleApiClient;

    public LoginModel() {
        userApi = retrofit.create(UserApi.class);
    }

    public void startGoogleLogin(BaseActivity activity, GoogleSignInOptions options, GoogleApiClient.OnConnectionFailedListener listener) {
        googleApiClient = new GoogleApiClient.Builder(activity)
                .enableAutoManage(activity, listener)
                .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                .build();

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        activity.startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
    }

    public void stopGoogleApiClient(BaseActivity activity) {
        if (googleApiClient != null && googleApiClient.isConnected()) {
            googleApiClient.stopAutoManage(activity);
            googleApiClient.disconnect();
        }
    }

    /* 구글 로그인에 필요한 옵션 */
    public GoogleSignInOptions getGoogleSignInOptions() {
        Resources resources = MergeApplication.getMergeAppComponent().getUtility().getResuorces();
        MDEBUG.debug("Build type " + BuildConfig.BUILD_TYPE);
        int clientId = Objects.equals(BuildConfig.BUILD_TYPE, "debug") ? R.string.server_client_id_debug : R.string.server_client_id;
        MDEBUG.debug("Build key" + resources.getString(clientId));
        return new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(resources.getString(clientId))
                .requestServerAuthCode(resources.getString(clientId), false)
                .requestEmail()
                .build();
    }

    public Member decodeMember(String encodeMember) {
        try {
            String memberDecodeStr = URLDecoder.decode(encodeMember, "UTF-8");
            return new Gson().fromJson(memberDecodeStr, Member.class);
        } catch (UnsupportedEncodingException ignored) {}
        return null;
    }

    public void sendSocialRegistEmail(Callback<ResponseObject<Member>> responseObjectCallback, SocialSignup socialSignup) {
        userApi.sendSocialRegistMail(socialSignup.instaId, socialSignup.facebookId, socialSignup.social, socialSignup.email).enqueue(responseObjectCallback);
    }

    /* 아이디와 패스워드로 로그인 */
    public void login(String id, String pw, Callback<ResponseObject<Member>> callback) {
        Map<String, String> loginInfo = new HashMap<>();
        loginInfo.put("username", id);
        loginInfo.put("password", pw);
        MDEBUG.debug("id = " + id);
        MDEBUG.debug("pw = " + pw);
        userApi.login(loginInfo).enqueue(callback);
    }

    /* 페이스북, 카카오, 네이버, 인스타그램 로그인 */
    public void socialLogin(Callback<ResponseBody> callback, @Properties.LoginSocial int social) {
        switch (social) {
            case FACEBOOK:
                userApi.facebookLogin().enqueue(callback);
                break;
            case KAKAO:
                userApi.kakaoLogin().enqueue(callback);
                break;
            case NAVER:
                userApi.naverLogin().enqueue(callback);
                break;
            case INSTA:
                userApi.instaLogin().enqueue(callback);
                break;
        }
    }

    /* 구글 로그인 */
    public void googleLogin(Callback<ResponseObject<Member>> callback, String idToken, String accessToken) {
        userApi.googleTokenLogin(GOOGLE_PROVIDER, idToken, accessToken).enqueue(callback);
    }

    /* 소셜 호스트 확인 */
    public boolean isSocialHost(String host) {
        Resources resources = MergeApplication.getMergeAppComponent().getUtility().getResuorces();
        String[] hosts = resources.getStringArray(R.array.social_host);
        return Arrays.asList(hosts).contains(host);
    }

    /* 비밀번호 찾기 */
    public void forgotPassword(Callback<ResponseObject<String>> callback, String id) {
        userApi.forgotPassword(id).enqueue(callback);
    }

    /* 탈퇴한 회원 복구하기 */
    public void restoreUser(Callback<ResponseObject<Member>> callback) {
        userApi.restoreUser().enqueue(callback);
    }

    /* 로그아웃 */
    public void logout(Callback<ResponseBody> callback) {
        userApi.logout().enqueue(callback);
    }
}
