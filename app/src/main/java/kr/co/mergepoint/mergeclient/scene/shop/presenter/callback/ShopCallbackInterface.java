package kr.co.mergepoint.mergeclient.scene.shop.presenter.callback;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.main.CombineImagesDetail;
import kr.co.mergepoint.mergeclient.scene.data.menu.ShopGroupCategory;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.PreExperience;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.GoogleSearchInfo;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.MergeReview;
import okhttp3.ResponseBody;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public interface ShopCallbackInterface {

    MergeCallback<ResponseObject<Integer>> getLikeCountCallback();

    /*매장의 상세정보를 받아온 결과*/
    MergeCallback<ShopDetail> shopDetailCallback();

    /*장바구니에 추가했을 때 결과*/
    MergeCallback<ResponseBody> addBasketCallback() ;

    /*메뉴 디테일 정보 응답*/
    MergeCallback<ShopMenuDetail> addMenuDetail();

    MergeCallback<ResponseObject<Integer>> getCheckFavoriteCall();

    MergeCallback<ResponseObject<ArrayList<PreExperience>>> getPreExperienceCallback();

    MergeCallback<ResponseObject<ArrayList<CombineImagesDetail>>> getMergesPickCallback();

    MergeCallback<GoogleSearchInfo> getGoogleReviewCallback();

    MergeCallback<ShopGroupCategory> getMenuGroupCategory();

    MergeCallback<ResponseObject<MergeReview>> getMergeReview();
}
