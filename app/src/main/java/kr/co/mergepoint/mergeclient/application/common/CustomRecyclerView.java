package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.ShopApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.ShopListAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 1017sjg on 2017. 8. 7..
 */

public class CustomRecyclerView extends RecyclerView implements BookMark.OnCheckFavorite ,RecyclerItemListener.RecyclerTouchListener{

    private BookMark.OnCheckFavorite onCheckFavorite;
    private CustomRecyclerViewTouch recyclerViewTouch;

    public void setRecyclerViewTouch(CustomRecyclerViewTouch recyclerViewTouch) {
        this.recyclerViewTouch = recyclerViewTouch;
    }

    public BookMark.OnCheckFavorite getOnCheckFavorite() {
        return onCheckFavorite;
    }

    public interface CustomRecyclerViewTouch {
        void onTouchItem(RecyclerView recyclerView, View view, int position);
    }

    public CustomRecyclerView(Context context) {
        super(context);
        init();
    }

    public CustomRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        onCheckFavorite = this;
    }

    protected void setInit(BasicListAdapter adapter) {
        setAdapter(adapter);
        setLayoutManager(new CustomLinearLayoutManager(getContext()));
        setItemAnimator(new DefaultItemAnimator());
        addOnItemTouchListener(new RecyclerItemListener(this, this));
    }

    @Override
    public void checkFavorite(View view, boolean isLogin) {
        if (isLogin) {
            ShopListAdapter adapter = getShopListAdapter();
            if (adapter != null) {
                int position = adapter.getPosition(view);
                checkFavoriteCall((int) view.getTag(), position);
            }
        } else {
            Snackbar.make(((BaseActivity) getContext()).findViewById(android.R.id.content), R.string.please_login, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void checkFavoriteCall(int shopOid, int position) {
        ShopApi shopApi = MergeApplication.getMergeAppComponent().getRetrofit().create(ShopApi.class);
        Call<ResponseObject<Integer>> call = shopApi.checkFavorite(shopOid);
        call.enqueue(getCheckFavoriteCall(position));
    }

    private Callback<ResponseObject<Integer>> getCheckFavoriteCall(final int position) {
        return new Callback<ResponseObject<Integer>>() {
            @Override
            public void onResponse(@NonNull Call<ResponseObject<Integer>> call, @NonNull Response<ResponseObject<Integer>> response) {
                ResponseObject<Integer> responseObject = response.body();
                if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                    int count = responseObject.getObject();
                    Shop shop = getShop(position);
                    if (shop != null) {
                        shop.favorite = !shop.favorite;
                        shop.setFavoriteCount(count);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseObject<Integer>> call, @NonNull Throwable t) {}
        };
    }

    private Shop getShop(int position) {
        ShopListAdapter adapter = getShopListAdapter();
        if (adapter != null)
            return adapter.getObservableArrayList().get(position);
        return null;
    }

    public ShopListAdapter getShopListAdapter() {
        return (getAdapter() instanceof ShopListAdapter) ? (ShopListAdapter) getAdapter() : null;
    }

    @Override
    public void onClickItem(View v, int position) {
        if (recyclerViewTouch != null)
            recyclerViewTouch.onTouchItem(this, v, position);
    }
}
