package kr.co.mergepoint.mergeclient.application.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 1017sjg on 2017. 7. 20..
 */

public class SmsMessageReceiver extends BroadcastReceiver {

    private OnSmsReceiver receiver;

    public interface OnSmsReceiver {
        void onReceiveSms(String certificationNum);
    }

    public SmsMessageReceiver(OnSmsReceiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Objects.equals(intent.getAction(), "android.provider.Telephony.SMS_RECEIVED")) {
            StringBuilder sms = new StringBuilder();
            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                Object[] objects = (Object[]) bundle.get("pdus");
                if (objects != null) {
                    SmsMessage[] messages = new SmsMessage[objects.length];

                    for (int i = 0; i<objects.length; i++) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            messages[i] = SmsMessage.createFromPdu((byte[]) objects[i], bundle.getString("format"));
                        } else {
                            messages[i] = SmsMessage.createFromPdu((byte[]) objects[i]);
                        }
                    }

                    for (SmsMessage message: messages)
                        sms.append(message.getMessageBody());

                    Pattern pattern = Pattern.compile("\\d{4}");
                    Matcher matcher = pattern.matcher(sms.toString());

                    if (matcher.find())
                        receiver.onReceiveSms(matcher.group(0));
                }
            }
        }
    }
}
