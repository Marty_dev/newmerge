package kr.co.mergepoint.mergeclient.scene.testpayment.payment.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.MergeBaseAdapter;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.Payment_v2_Activity;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopCoupon;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopTicket;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.view.MergeBottomDialog;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * User: Marty
 * Date: 2018-07-18
 * Time: 오전 11:31
 * Description:
 */
public class ShopListAdapter extends MergeBaseAdapter<ShopPayment, ShopViewholder> implements View.OnClickListener{

    public boolean isEnterMember;
    public int isEnterPlace;
    public String Shopname;
    public boolean isMulti;


    public ArrayList<ShopCoupon> coupons;
    public ArrayList<ShopTicket> tickets;


    public ShopListAdapter() {
        super();

    }

    public ShopListAdapter(int layout, Payment_v2_Activity mCon) {
        super(layout, mCon);
    }

    public ShopListAdapter(int layout, Payment_v2_Activity mCon, ArrayList<ShopPayment> list) {
        super(layout, mCon);
        arrayList = list;
        isMulti = arrayList.size() != 1;

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // TODO: 2018-07-19 화면 분기점
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_shop_item_v2, parent, false);

        ShopViewholder vh = new ShopViewholder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        isEnterPlace = arrayList.get(position).shopType;
        ShopPayment obj = arrayList.get(position);
        // TODO: 2018-07-19 기능정의
        ShopViewholder view = (ShopViewholder) holder;
        if (isMulti) {
            view.shopNameRl.setVisibility(View.VISIBLE);
            view.shopNameTv.setText(obj.shopName);
            view.shopPriseTv.setText(String.format("%,d",obj.orderPrice) + "원");
        }else {
            view.shopNameRl.setVisibility(View.GONE);
        }
        if (obj.allocateComPoints == 0){
            view.enterPointPrise.setVisibility(View.GONE);
        }else
            view.enterPointPrise.setVisibility(View.VISIBLE);

        if (obj.allocatePoints == 0){
            view.normalPointPrise.setVisibility(View.GONE);
        }else
            view.normalPointPrise.setVisibility(View.VISIBLE);

        view.enterPointPrise.setText(String.format("%,d",obj.allocateComPoints) + "P");
        view.normalPointPrise.setText(String.format("%,d",obj.allocatePoints) + "P");

        view.enterMypointTv.setText("보유 | " + String.format("%,d",obj.myHavaCompanyPoints) + " P");
        view.normalMypointTv.setText("보유 | " + String.format("%,d",obj.myHavePoints) + " P");
        view.enterUsepointEdt.setText((obj.myCompanyPoints == 0 ? null : String.format("%,d",obj.myCompanyPoints)));
        view.normalUsepointEdt.setText((obj.pointPrice == 0 ? null : String.format("%,d",obj.pointPrice)));

        view.enterTicketNametv.setText(obj.myTicketName == null ? "식권 선택" : obj.myTicketName);
        view.enterTicketCounttv.setText(obj.myTicketCount + "");
        view.normalTicketNametv.setText(obj.couponName == null ? "쿠폰 선택" : obj.couponName);
        MDEBUG.debug("Proccessing 0");


        view.enterPointAllbtn.setOnClickListener(view1->{
            long price = (obj.myHavaCompanyPoints >= (obj.payPrice + obj.myCompanyPoints) ?
                    (obj.payPrice + obj.myCompanyPoints) : obj.myHavaCompanyPoints);
            view.enterUsepointEdt.setText(String.format("%,d",price));

            ((Payment_v2_Activity) mCon).setEnterAdapt(obj.myTicketCount,
                    obj.myTicketRef,price,obj.crowdPayRef);
        });

        view.normalPointAllbtn.setOnClickListener(view1->{
            long price = (obj.myHavePoints >= (obj.payPrice + obj.pointPrice) ?
                    (obj.payPrice + obj.pointPrice) : obj.myHavePoints);
            view.normalUsepointEdt.setText(String.format("%,d",price));

            ((Payment_v2_Activity) mCon).setPointAdapt(obj.oid,
                    price,obj.couponRef);
        });



        view.enterTicketPlus.setOnClickListener(view1->{
            if (obj.myTicketRef == 0) return;
            int ticketlength = 1;

            for (ShopTicket ticket: obj.myHaveTicketList) {
                if (ticket.oid == obj.myTicketRef)
                    ticketlength = ticket.remainTicketCount;
            }
            if (Integer.parseInt(view.enterTicketCounttv.getText().toString())
                    == ticketlength)
                return;
            // TODO: 2018-07-20 ticket count ++
            ((Payment_v2_Activity) mCon).setEnterAdapt(obj.myTicketCount + 1,obj.myTicketRef,obj.myCompanyPoints,obj.crowdPayRef);

        });
        view.enterTicketMinus.setOnClickListener(view1->{
            if (obj.myTicketRef == 0) return;
            if (Integer.parseInt(view.enterTicketCounttv.getText().toString())
                    <= 1)
                return;
            // TODO: 2018-07-20 ticket count -- 
            ((Payment_v2_Activity) mCon).setEnterAdapt(obj.myTicketCount - 1,obj.myTicketRef,obj.myCompanyPoints,obj.crowdPayRef);

        });


        if (obj.shopCoupon == null)
            obj.shopCoupon = new ArrayList<>();
        if (obj.myHaveTicketList == null)
            obj.myHaveTicketList = new ArrayList<>();
        // TODO: 2018-07-30  ticket default Setting
      /*  if (!isMulti) {
            if (obj.myHaveTicketList.size() == 1) {
                ((Payment_v2_Activity) mCon).setEnterAdapt(1,obj.myHaveTicketList.get(0).oid,obj.myCompanyPoints,obj.crowdPayRef);
            }
        }
        */

        view.enterTicketRl.setOnClickListener((view1 -> {
            MergeBottomDialog bottomDialog = MergeBottomDialog.getInstance((Payment_v2_Activity)mCon,obj.myTicketRef,true);
            bottomDialog.setTicket(obj.myHaveTicketList,(ticket -> {
                if (ticket != null) {

                    ((Payment_v2_Activity) mCon).setEnterAdapt(1,ticket.oid,obj.myCompanyPoints,obj.crowdPayRef);
                }else{
                    ((Payment_v2_Activity) mCon).setEnterAdapt(0,0,obj.myCompanyPoints,obj.crowdPayRef);

                }
            }));
            bottomDialog.show(mCon.getSupportFragmentManager(),"TicketBottomDialog");
        }));
        view.normalTicketRl.setOnClickListener(view1 -> {

            MergeBottomDialog bottomDialog = MergeBottomDialog.getInstance((Payment_v2_Activity)mCon,obj.couponRef,false);
            bottomDialog.setCoupon(obj.shopCoupon,(coupon -> {
                if (coupon != null){
                    ((Payment_v2_Activity) mCon).setPointAdapt(obj.oid,obj.pointPrice,coupon.oid);
                }else{
                    ((Payment_v2_Activity) mCon).setPointAdapt(obj.oid,obj.pointPrice,0);

                }
            }));
            bottomDialog.show(mCon.getSupportFragmentManager(),"TicketBottomDialog");
        });

        view.normalPointConfirm.setOnClickListener((view1 ->{
            int price = 0;
            try{
                price = Integer.parseInt(view.normalUsepointEdt.getText().toString().replace(",",""));
            }catch (Exception e){
                price = 0;
            }
            if (view.normalUsepointEdt.getText() == null)
            {
                price = 0;
            }
            ((Payment_v2_Activity) mCon).setPointAdapt(obj.oid,
                    price, obj.couponRef);

        }));

        view.normalUsepointEdt.setImeOptions(EditorInfo.IME_ACTION_DONE);
        view.normalUsepointEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {

                    int price = 0;
                    try{
                        price = Integer.parseInt(view.normalUsepointEdt.getText().toString().replace(",",""));
                    }catch (Exception e){
                        price = 0;
                    }
                    ((Payment_v2_Activity) mCon).setPointAdapt(obj.oid,
                            price, obj.couponRef);

                    return true;
                }
                return false;
            }
        });
        view.enterUsepointEdt.setImeOptions(EditorInfo.IME_ACTION_DONE);
        view.enterUsepointEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    int price = 0;
                    try{
                        price = Integer.parseInt(view.enterUsepointEdt.getText().toString().replace(",",""));
                    }catch (Exception e){
                        price = 0;
                    }
                    ((Payment_v2_Activity) mCon).setEnterAdapt(obj.myTicketCount,
                            obj.myTicketRef,price,obj.crowdPayRef);

                    return true;
            }
                return false;
            }
        });

        view.enterPointConfirm.setOnClickListener((view1 ->{
            int price = 0;
            try {
                price = Integer.parseInt(view.enterUsepointEdt.getText().toString().replace(",", ""));
            }catch (NumberFormatException e ){
                price = 0;
            }
            if (view.enterUsepointEdt.getText() == null)
            {
                price = 0;
            }
            ((Payment_v2_Activity) mCon).setEnterAdapt(obj.myTicketCount,
                    obj.myTicketRef,price,obj.crowdPayRef);
        }));

        MDEBUG.debug("Proccessing 2");

        // TODO: 2018-07-19 select ticket setting

        // TODO: 2018-07-19 select coupon setting
        MDEBUG.debug("Position + " + position);
        MDEBUG.debug("Shop Name  : " + obj.shopName);

        MDEBUG.debug("isenterMember  : " + isEnterMember);
        MDEBUG.debug("isenterPlace   : " + isEnterPlace);
        MDEBUG.debug("usablecompanyPay  : " + obj.usableCompanyPay);
        if (isEnterMember && obj.usableCompanyPay){
            // 기업 회원
            if (isEnterPlace > 1  && isEnterPlace < 4){
                // 기업 제휴 매장
                view.normalPointRl.setVisibility(View.GONE);
                view.normalExpandl.setVisibility(View.GONE);
                view.enterPointPlus.setVisibility(View.GONE);
                view.enterPointRl.setVisibility(View.VISIBLE);
                view.enterExpandl.setVisibility(View.VISIBLE);
                view.normalPointPlus.setVisibility(View.GONE);
            }else{
                // 일반 매장
                view.normalPointRl.setVisibility(View.VISIBLE);
                view.normalExpandl.setVisibility(View.VISIBLE);
                view.enterPointPlus.setVisibility(View.VISIBLE);
                view.normalPointPlus.setVisibility(View.VISIBLE);
                view.enterPointRl.setVisibility(View.VISIBLE);
                view.enterExpandl.setVisibility(View.VISIBLE);
                view.enterExpandl.setBackground(mCon.getResources().getDrawable(R.drawable.gray_box));
                view.normalPointRl.setBackground(mCon.getResources().getDrawable(R.drawable.white_box));
                if (view.enterExpandl.isExpanded()){
                    view.normalExpandl.collapse();
                }

                view.enterPointRl.setOnClickListener((view1 -> {
                    MDEBUG.debug("enter click");
                    if (!view.enterExpandl.isExpanded()){
                        view.enterExpandl.expand();
                        view.normalExpandl.collapse();
                        view.enterPointPlus.setImageResource(R.drawable.minus_black);
                        view.normalPointPlus.setImageResource(R.drawable.plus_black);
                    }
                }));
                view.normalPointRl.setOnClickListener((view1 -> {
                    MDEBUG.debug("normal click");
                    if (!view.normalExpandl.isExpanded()){
                        view.enterExpandl.collapse();
                        view.normalExpandl.expand();
                        view.normalPointPlus.setImageResource(R.drawable.minus_black);
                        view.enterPointPlus.setImageResource(R.drawable.plus_black);
                    }
                }));
            }
        }else{
            // 일반회원
            MDEBUG.debug("is here??" + obj.shopName);
            view.enterPointRl.setVisibility(View.GONE);
            view.enterExpandl.setVisibility(View.GONE);
            view.normalPointPlus.setVisibility(View.GONE);
            view.normalPointRl.setVisibility(View.VISIBLE);
            view.normalExpandl.setVisibility(View.VISIBLE);
            view.enterPointPlus.setVisibility(View.VISIBLE);
            view.normalPointShopname.setText(isEnterMember ? "개인포인트" : "포인트 사용");
            view.normalExpandl.expand();

        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

        }

    }
}
