package kr.co.mergepoint.mergeclient.scene.search.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.scene.search.SearchActivity;

/**
 * Created by jgson on 2017. 6. 5..
 */

@SearchScope
@Component(modules = SearchModule.class)
public interface SearchComponent {
    void inject(SearchActivity activity);
}
