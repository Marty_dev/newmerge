package kr.co.mergepoint.mergeclient.application.common;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.kakao.kakaolink.v2.KakaoLinkResponse;
import com.kakao.kakaolink.v2.KakaoLinkService;
import com.kakao.message.template.ButtonObject;
import com.kakao.message.template.ContentObject;
import com.kakao.message.template.FeedTemplate;
import com.kakao.message.template.LinkObject;
import com.kakao.message.template.SocialObject;
import com.kakao.network.ErrorResult;
import com.kakao.network.callback.ResponseCallback;
import com.kakao.util.helper.log.Logger;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.databinding.SharedListBinding;

import static kr.co.mergepoint.mergeclient.application.common.Properties.BASE_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INTENT_SHOP;

/**
 * Created by 1017sjg on 2017. 8. 7..
 */

public class ShareFragment extends BaseFragment {

    private CallbackManager callbackManager;
    private String type;
    private String title;
    private String content;
    private String img;
    private int oid;

    @Properties.IntentType
    public String getType() {
        return type;
    }

    public void setType(@Properties.IntentType String type) {
        this.type = type;
    }

    public CallbackManager getCallbackManager() {
        return callbackManager;
    }

    public static ShareFragment newInstance(String title, String content, String img, int oid, @Properties.IntentType String type) {
        Bundle args = new Bundle();
        ShareFragment fragment = new ShareFragment();
        fragment.title = title;
        fragment.content = content;
        fragment.img = img;
        fragment.oid = oid;
        fragment.type = type;
        fragment.callbackManager = CallbackManager.Factory.create();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedListBinding sharedListBinding = DataBindingUtil.inflate(inflater, R.layout.shared_list, container, false);
        sharedListBinding.setOnClick(this);

        return sharedListBinding.getRoot();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.kakao:
                if (isInstallApp("com.kakao.talk", "카카오톡"))
                    sharedKaKao(title, content, BASE_URL + img, oid);
                break;
            case R.id.facebook:
                if (isInstallApp("com.facebook.katana", "페이스북")) {
                    sharedFacebook(oid, title, callbackManager, new FacebookCallback<Sharer.Result>() {
                        @Override
                        public void onSuccess(Sharer.Result result) {
                            showAlert(R.string.shared);
                        }

                        @Override
                        public void onCancel() {
                            showAlert(R.string.cancled);
                        }

                        @Override
                        public void onError(FacebookException error) {
                            showAlert(R.string.retry);
                        }
                    });
                }
                break;
            case R.id.twitter:
                if (isInstallApp("com.twitter.android", "트위터"))
                    sharedTwitter(oid, title);
                break;
            case R.id.shared_url:
                copyClipBoardLink(oid);
                break;
        }
        removeFragment(this);
    }

    private void removeFragment(Fragment fragment) {
        rootActivity.getSupportFragmentManager().popBackStack();
        rootActivity.getSupportFragmentManager().beginTransaction()
                .remove(fragment)
                .commit();
    }

    public boolean isInstallApp(String pakageName, String appName) {
        boolean isInstall = rootActivity.getPackageManager().getLaunchIntentForPackage(pakageName) != null;

        if (!isInstall)
            new MergeDialog.Builder(rootActivity).setContent(String.format(Locale.getDefault(), "%s 설치 후 사용해주세요.", appName)).setCancelBtn(false).build().show();

        return isInstall;
    }

    public void sharedKaKao(String title, String content, String imageUrl, int oid) {
        String scheme = rootActivity.getResources().getString(R.string.kakao_scheme);
        String host = rootActivity.getResources().getString(R.string.kakao_host);
        String param = String.format(Locale.getDefault(), "type=%s&oid=%d", type, oid);
        LinkObject linkObject = LinkObject.newBuilder()
                .setWebUrl("https://www.mergepoint.co.kr/")
                .setMobileWebUrl(String.format("%s://%s", scheme, host))
                .setAndroidExecutionParams(param)
                .build();

        FeedTemplate params = FeedTemplate
                .newBuilder(ContentObject.newBuilder(title, imageUrl, linkObject)
                        .setDescrption(content)
                        .build())
                .setSocial(SocialObject.newBuilder().build())
                .addButton(new ButtonObject("앱에서 보기", linkObject))
                .build();

        KakaoLinkService.getInstance().sendDefault(rootActivity, params, new ResponseCallback<KakaoLinkResponse>() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                Logger.e(errorResult.toString());
            }

            @Override
            public void onSuccess(KakaoLinkResponse result) {}
        });
    }

    public void sharedFacebook(int oid, String name, CallbackManager callbackManager, FacebookCallback<Sharer.Result> facebookCallback) {
        String typeString = Objects.equals(type, INTENT_SHOP) ? "visitShop" : "viewEvent";
        ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(name + " | " + "by 머지포인트").setContentUrl(Uri.parse(BASE_URL + typeString +"?oid=" + oid)).build();
        ShareDialog dialog = new ShareDialog(rootActivity);
        dialog.registerCallback(callbackManager, facebookCallback);
        if (ShareDialog.canShow(ShareLinkContent.class))
            dialog.show(linkContent, ShareDialog.Mode.NATIVE);

//        List<String> permissionNeeds = Collections.singletonList("publish_actions");
//        LoginManager manager = LoginManager.getInstance();
//        manager.logInWithPublishPermissions(activity, permissionNeeds);
//        manager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//            }
//
//            @Override
//            public void onCancel() {
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//            }
//        });
    }

    public void sharedTwitter(int oid, String name) {
        try {
            String typeString = Objects.equals(type, INTENT_SHOP) ? "visitShop" : "viewEvent";
            TweetComposer.Builder builder  = new TweetComposer.Builder(rootActivity)
                    .text(name + " | " + "by 머지포인트\n")
                    .url(new URL(BASE_URL + typeString + "?oid=" + oid));
            builder.show();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void copyClipBoardLink(int oid) {
        String typeString = Objects.equals(type, INTENT_SHOP) ? "visitShop" : "viewEvent";
        ClipboardManager clipboardManager = (ClipboardManager) rootActivity.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("label", BASE_URL + typeString +"?oid=" + oid);
        if (clipboardManager != null) {
            clipboardManager.setPrimaryClip(clipData);
            showAlert(R.string.copy_success);
        } else {
            showAlert(R.string.retry);
        }
    }
}
