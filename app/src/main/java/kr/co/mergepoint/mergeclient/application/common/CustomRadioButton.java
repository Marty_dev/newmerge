package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.shop.view.listener.OnRadioChange;
import kr.co.mergepoint.mergeclient.scene.shop.view.listener.RadioObserver;

/**
 * Created by 1017sjg on 2017. 6. 30..
 */

public class CustomRadioButton extends BaseRadioButton implements RadioObserver {

    public int maxCount;
    public int optionMenuOid;
    public int selectOptionOid;
    public OnRadioChange onRadioChange;
    public boolean preChecked;

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public CustomRadioButton(Context context) {
        super(context);
    }

    public CustomRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setOnRadioChange(OnRadioChange onRadioChange) {
        this.onRadioChange = onRadioChange;
        onRadioChange.add(this);
    }

    @Override
    public void setChecked(boolean checked) {
        preChecked = isChecked();
        super.setChecked(checked);
        setTextColor(ContextCompat.getColor(getContext(), checked ? R.color.point_red : R.color.gray_12));
    }

    @Override
    public void onChangeRadio(int selectMenuOid) {
        setChecked(selectMenuOid == selectOptionOid);
    }
}
