package kr.co.mergepoint.mergeclient.scene.shoppingbasket.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.BasketActivity;

/**
 * Created by jgson on 2017. 6. 5..
 */

@BasketScope
@Component(modules = BasketModule.class)
public interface BasketComponent {
    void inject(BasketActivity activity);
}
