package kr.co.mergepoint.mergeclient.application.common.mvvm;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import kr.co.mergepoint.mergeclient.application.common.mvvm.dagger.ActivityComponent;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public abstract class ViewModel extends BaseObservable {

    @Inject protected AttachedActivity attachedActivity;

    protected ViewModel(@Nullable State savedInstanceState, @NonNull ActivityComponent component) {
        component.inject(this);
    }

    public State getInstanceState() {
        return new State(this);
    }

    @CallSuper
    public void onResume() {}

    @CallSuper
    public void onStart() {}

    @CallSuper
    public void onStop() {}

    public static class State implements Parcelable {
        protected State(ViewModel viewModel) {}

        public State(Parcel in) {}

        @Override
        public int describeContents() {
            return 0;
        }

        @CallSuper
        @Override
        public void writeToParcel(Parcel dest, int flags) {}

        public static Creator<State> CREATOR = new Creator<State>() {
            @Override
            public State createFromParcel(Parcel source) {
                return new State(source);
            }

            @Override
            public State[] newArray(int size) {
                return new State[size];
            }
        };
    }
}
