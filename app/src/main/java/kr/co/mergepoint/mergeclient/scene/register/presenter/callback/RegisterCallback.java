package kr.co.mergepoint.mergeclient.scene.register.presenter.callback;

import android.os.AsyncTask;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;

import java.io.IOException;
import java.net.CookieHandler;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseCallback;
import kr.co.mergepoint.mergeclient.application.common.CustomCookieManager;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.register.IdConfirm;
import kr.co.mergepoint.mergeclient.scene.data.register.PhoneConfirm;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.register.model.RegisterModel;
import kr.co.mergepoint.mergeclient.scene.register.view.RegisterView;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import okhttp3.ResponseBody;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class RegisterCallback extends BaseCallback<RegisterView, RegisterModel> implements RegisterCallbackInterface {

    public RegisterCallback(RegisterView baseView, RegisterModel baseModel) {
        super(baseView, baseModel);
    }

    @Override
    public MergeCallback<PhoneConfirm> phoneConfirmCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            PhoneConfirm phoneConfirm = response.body();
            baseView.successPhone(response.isSuccessful() && phoneConfirm != null && phoneConfirm.result);
        });
    }

    @Override
    public MergeCallback<IdConfirm> idConfirmCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            IdConfirm idConfirm = response.body();
            baseView.successId(response.isSuccessful() && idConfirm != null && !idConfirm.result);
        });
    }

    @Override
    public MergeCallback<ResponseObject<String>> sendPhoneAuthNumCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<String> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                if (!responseObject.isFailed()) {
                    baseView.showAlert(R.string.send_sms);
                } else {
                    baseView.showAlert(responseObject.getMessage());
                }
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Member>> registerCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {

            ResponseObject<Member> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                baseView.userStateCheck(responseObject.getObject(), () -> {
                    baseView.showLoading();
                    baseModel.restoreUser(restoreUser());
                }, () -> baseModel.logout(getLogoutCallback()));
            } else {
                baseView.showAlert(R.string.retry);
            }
        }, R.string.register_fail);
    }

    @Override
    public MergeCallback<ResponseObject<Member>> restoreUser() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Member> responseObject = response.body();
            if (responseObject != null) {
                Member member = responseObject.getObject();
                if (!responseObject.isFailed() && member != null) {
                    baseView.openMain(member);
                } else {
                    baseView.showAlert(responseObject.getMessage());
                }
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseBody> getLogoutCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            CustomCookieManager customCookieManager = (CustomCookieManager) CookieHandler.getDefault();
            customCookieManager.getCookieStore().removeAll();
            InitialInfo initialInfo = MergeApplication.getInitialInfo();
            if (initialInfo != null)
                initialInfo.setLoginState(null, false);
            baseView.openClearTaskActivity(MainActivity.class);
        }, R.string.logout_fail);
    }



}
