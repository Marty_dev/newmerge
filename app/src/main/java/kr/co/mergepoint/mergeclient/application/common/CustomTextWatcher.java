package kr.co.mergepoint.mergeclient.application.common;

import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.Locale;

/**
 * Created by 1017sjg on 2017. 9. 5..
 */

public class CustomTextWatcher implements TextWatcher {

    private EditText editText;
    private String price = "";

    public CustomTextWatcher(EditText editText) {
        this.editText = editText;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        try {
            if (!charSequence.toString().equals(price)) { // StackOverflow 방지
                String strPrice = charSequence.toString().replace(",", "").trim();
                price = String.format(Locale.getDefault(), "%,2d", Integer.parseInt(strPrice));

                editText.setText(price);
                Editable editable = editText.getText();
                Selection.setSelection(editable, price.length());
            }
        } catch (NumberFormatException ignored) {}
    }

    @Override
    public void afterTextChanged(Editable editable) {}
}
