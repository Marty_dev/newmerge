package kr.co.mergepoint.mergeclient.scene.usagehistory;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

import javax.inject.Inject;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.FragmentLifeCycle;
import kr.co.mergepoint.mergeclient.application.common.RecyclerItemListener;
import kr.co.mergepoint.mergeclient.scene.usagehistory.dagger.DaggerUsageComponent;
import kr.co.mergepoint.mergeclient.scene.usagehistory.dagger.UsageModule;
import kr.co.mergepoint.mergeclient.scene.usagehistory.presenter.UsagePresenter;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * Created by jgson on 2017. 5. 31..
 */

public class UsageActivity extends BaseActivity implements RecyclerItemListener.RecyclerTouchListener, FragmentLifeCycle, AdapterView.OnItemSelectedListener, TabLayout.OnTabSelectedListener {

    @Inject UsagePresenter usagePresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MDEBUG.debug("UsageActivity is running");
        DaggerUsageComponent.builder().usageModule(new UsageModule(this)).build().inject(this);
        usagePresenter.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        usagePresenter.onResume();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        usagePresenter.onPostResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        usagePresenter.onDestroy();
    }

    @Override
    protected void onActivityClick(View view) {
        usagePresenter.onClick(view);
    }

    @Override
    public void onClickItem(View v, int position) {
        usagePresenter.onClickItem(v, position);
    }

    /*Fragment 생명주기*/
    @Override
    public void onFragmentDestroy() {
        usagePresenter.onFragmentDestroy();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        usagePresenter.onTabSelected(tab);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}

    @Override
    public void onTabReselected(TabLayout.Tab tab) {}

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        usagePresenter.onItemSelected(adapterView, view, i, l);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        usagePresenter.onNewIntent(intent);
    }
}
