package kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.scene.data.main.Question;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class Questions extends ExpandableGroup<Question> {

    public Questions(String title, List<Question> items) {
        super(title, items);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Questions)) return false;

        Questions questions = (Questions) o;
        return Objects.equals(getTitle(), questions.getTitle());
    }
}
