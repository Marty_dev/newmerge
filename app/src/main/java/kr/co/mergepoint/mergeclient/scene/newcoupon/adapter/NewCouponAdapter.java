package kr.co.mergepoint.mergeclient.scene.newcoupon.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.NewCouponApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.DateModel;
import kr.co.mergepoint.mergeclient.application.common.MergeActivity;
import kr.co.mergepoint.mergeclient.application.common.MergeBaseAdapter;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponDetail;
import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponV1;
import kr.co.mergepoint.mergeclient.scene.newcoupon.view.CouponDetailActivity;
import kr.co.mergepoint.mergeclient.scene.newcoupon.view.MergeNewDialog;
import kr.co.mergepoint.mergeclient.scene.newcoupon.view.newMyCouponActivity;
import kr.co.mergepoint.mergeclient.scene.newcoupon.viewholder.CouponViewHolder;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Response;

/**
 * User: Marty
 * Date: 2018-09-17
 * Time: 오전 10:02
 * Description:
 */
public class NewCouponAdapter extends MergeBaseAdapter<CouponV1,CouponViewHolder> {

    NewCouponApi API;
    DateModel date;
    public NewCouponAdapter() {
        super();
    }

    public NewCouponAdapter(int layout, MergeActivity mCon, NewCouponApi api) {
        super(layout, mCon);
        date = new DateModel();
        API = api;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CouponViewHolder viewHolder;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(mLayout, parent, false);

        viewHolder= new CouponViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CouponViewHolder vh = (CouponViewHolder)holder;

        CouponV1 item = arrayList.get(position);
        vh.couponShopName.setText(item.getCouponName());
        vh.couponPriceTv.setText(item.getCouponDescription() +"");
        vh.couponPriceBase.setText(item.getSmallDescription() + "");
        vh.couponRefund.setVisibility(View.GONE);
        vh.couponTopRl.setVisibility(View.VISIBLE);
        vh.couponBottomRl.setVisibility(View.VISIBLE);
        vh.another.setVisibility(View.GONE);
        if (item.getCouponState() == -1){
          vh.couponTopRl.setVisibility(View.GONE);
          vh.couponBottomRl.setVisibility(View.GONE);
          vh.another.setVisibility(View.VISIBLE);
        } else if (item.getCouponCategory() == 4){
            // TODO: 2018-09-18  프랜차이즈 쿠폰

            String filteredURL = MergeApplication.getMergeApplication().getImageUrlFiltered(item.getCouponImage());
            Picasso.with(mCon).load(filteredURL).error(R.drawable.merge_logo).into(vh.couponLogo);

            //MDEBUG.debug(Properties.BASE_URL+"resources/getObject/"+item.getCouponImage());
            String preDate = "";
            int CouponState = item.getCouponState();
            if (CouponState == 1){
                preDate = "발급 유효기간 ";
            }else{
                preDate = "유효기간 ";
            }
            if (item.getCouponState() < 4) {
                vh.couponTopRl.setBackground(mCon.getResources().getDrawable(R.drawable.coupon_top_white));
                vh.couponBottomRl.setBackground(mCon.getResources().getDrawable(R.drawable.coupon_bottom_white));
                vh.couponDateTv.setText(preDate + date.getSimpleDate(date.getDate(item.getCouponState() != 1 ? item.getExpDate() : item.getIssueExpDate())));
                vh.couponDateTv.setVisibility(View.VISIBLE);
                vh.couponShopName.setTextColor(Color.parseColor("#393939"));
                vh.couponPriceTv.setTextColor(Color.parseColor("#393939"));
                vh.couponPriceBase.setTextColor(Color.parseColor("#393939"));
                vh.couponDateTv.setTextColor(Color.parseColor("#8c8c8c"));
                vh.couponBottomTwobtn.setVisibility(View.GONE);
                vh.coupondetailbtn.setVisibility(View.VISIBLE);
                vh.coupondetailbtn.setText("바코드보기");
                vh.coupondetailbtn.setTextColor(Color.parseColor("#8b8b8b"));
                vh.coupondetailbtn.setOnClickListener(barcodeCallback(item));
            }else if (item.getCouponState() >= 4){
                vh.couponTopRl.setBackground(mCon.getResources().getDrawable(R.drawable.coupon_top_end));
                vh.couponBottomRl.setBackground(mCon.getResources().getDrawable(R.drawable.coupon_bottom_end));
                vh.couponDateTv.setVisibility(View.GONE);
                vh.couponShopName.setTextColor(Color.parseColor("#707070"));
                vh.couponPriceTv.setTextColor(Color.parseColor("#707070"));
                vh.couponPriceBase.setTextColor(Color.parseColor("#707070"));
                vh.couponBarcodeBtn.setTextColor(Color.parseColor("#8b8b8b"));
                vh.couponDeleteBtn.setTextColor(Color.parseColor("#8b8b8b"));
                if (item.getCouponState() == 5)
                    vh.couponRefund.setVisibility(View.VISIBLE);
                if (item.getCouponState() == 4 || item.getCouponState() == 5 || item.getCouponState() == 7) {
                    vh.coupondetailbtn.setVisibility(View.GONE);
                    vh.couponBottomTwobtn.setVisibility(View.VISIBLE);
                    vh.couponBarcodeBtn.setOnClickListener(barcodeCallback(item));
                    vh.couponDeleteBtn.setOnClickListener(barcodedeletecall(item));
                }else if (item.getCouponState() == 6){
                    vh.coupondetailbtn.setVisibility(View.VISIBLE);
                    vh.couponBottomTwobtn.setVisibility(View.GONE);
                    vh.coupondetailbtn.setTextColor(Color.parseColor("#8b8b8b"));
                    vh.coupondetailbtn.setText("환불요청");
                    vh.coupondetailbtn.setOnClickListener(view -> {
                        new MergeDialog.Builder(mCon).setContent("해당 쿠폰을 구매하신 구매금액이\n포인트로 변환되어 환불이 진행됩니다.\n환불을 계속 진행하시겠습니까")
                                .setConfirmClick(()->{


                        MergeApplication.getMergeApplication().showLoading(mCon);
                        API.deleteExpired(item.getOid()).enqueue(new retrofit2.Callback<ResponseObject<Integer>>() {
                            @Override
                            public void onResponse(Call<ResponseObject<Integer>> call, Response<ResponseObject<Integer>> response) {
                                MergeApplication.getMergeApplication().hideLoading(mCon);

                                if (response.isSuccessful() && !response.body().isFailed()){
                                    arrayList.remove(position);
                                    notifyItemRemoved(position);
                                    int count = response.body().getObject();
                                    ((newMyCouponActivity)mCon).refreshCount(count);
                                }else{
                                    ((MergeActivity)mCon).showAlert(R.string.retry);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseObject<Integer>> call, Throwable t) {
                                MergeApplication.getMergeApplication().hideLoading(mCon);
                                ((MergeActivity)mCon).showAlert(R.string.retry);

                            }
                        });
                        }).build().show();
                    });
                }


            }
        }else{
            vh.couponDateTv.setVisibility(View.VISIBLE);
            vh.couponShopName.setTextColor(Color.WHITE);
            vh.couponPriceTv.setTextColor(Color.WHITE);
            vh.couponPriceBase.setTextColor(Color.WHITE);
            vh.couponDateTv.setTextColor(Color.WHITE);
            // TODO: 2018-09-18  Merge Coupon
            Picasso.with(mCon).load(R.drawable.merge_icon_white).into(vh.couponLogo);
            vh.couponLogo.setImageResource(R.drawable.merge_icon_white);
            vh.couponTopRl.setBackground(mCon.getResources().getDrawable(R.drawable.coupon_top_color));
            vh.couponBottomRl.setBackground(mCon.getResources().getDrawable(R.drawable.coupon_bottom_color));
            vh.couponBottomTwobtn.setVisibility(View.GONE);
            vh.coupondetailbtn.setVisibility(View.VISIBLE);
            vh.coupondetailbtn.setText("상세보기");
            vh.coupondetailbtn.setTextColor(Color.WHITE);
            vh.coupondetailbtn.setOnClickListener((view)->{
                new MergeNewDialog(mCon,item,1).show();
            });
            vh.couponDateTv.setText("유효기간 " + date.getSimpleDate(date.getDate(item.getExpDate())));
        }

    }

    View.OnClickListener barcodeCallback(final CouponV1 item){
        return (view)->{
            if (item.getCouponState() == 1) {
                new MergeNewDialog(mCon, item, 2).setOnConfirm((v, data) -> {
                    MergeApplication.getMergeApplication().showLoading(mCon);

                    API.applyBarcode(item.getOid()).enqueue(new retrofit2.Callback<ResponseObject<CouponDetail>>() {
                        @Override
                        public void onResponse(Call<ResponseObject<CouponDetail>> call, Response<ResponseObject<CouponDetail>> response) {
                            if (!response.body().isFailed()) {
                                Intent inte = new Intent(mCon, CouponDetailActivity.class);
                                inte.putExtra("couponRef", response.body().getObject().getOid());
                                mCon.startActivity(inte);
                            } else {
                                new MergeDialog.Builder(mCon).setCancelBtn(false).setContent(mCon.getResources().getString(R.string.retry)).build().show();
                            }
                            MDEBUG.debug("Error?");
                            MergeApplication.getMergeApplication().hideLoading(mCon);


                        }

                        @Override
                        public void onFailure(Call<ResponseObject<CouponDetail>> call, Throwable t) {
                            MDEBUG.debug("Error!" + t.toString());
                            new MergeDialog.Builder(mCon).setCancelBtn(false).setContent(mCon.getResources().getString(R.string.retry)).build().show();
                            MergeApplication.getMergeApplication().hideLoading(mCon);

                        }
                    });
                }).show();
            }else if (item.getCouponState() >= 2){
                Intent inte = new Intent(mCon, CouponDetailActivity.class);
                inte.putExtra("couponRef", item.getOid());
                mCon.startActivity(inte);
            }

        };

    }
    View.OnClickListener barcodedeletecall(CouponV1 item){
        return (view)->{

            MergeDialog.Builder builder = new MergeDialog.Builder(mCon);
            builder.setCancelBtn(true);
            builder.setContent(R.string.barcode_delete_warning);
            builder.setConfirmClick(()->{
                MergeApplication.getMergeApplication().showLoading(mCon);
                API.deleteCoupon(item.getCouponCategory(),item.getOid()).enqueue(getBarcodeDeleteCallback());
            });
            builder.build().show();

        };
    }

    retrofit2.Callback<ResponseObject<ArrayList<CouponV1>>> getBarcodeDeleteCallback (){
        return new retrofit2.Callback<ResponseObject<ArrayList<CouponV1>>>() {
            @Override
            public void onResponse(Call<ResponseObject<ArrayList<CouponV1>>> call, Response<ResponseObject<ArrayList<CouponV1>>> response) {
                if (!response.body().isFailed()) {
                    arrayList.clear();
                    arrayList.addAll(response.body().getObject());
                    notifyDataSetChanged();

                } else {
                    new MergeDialog.Builder(mCon).setCancelBtn(false).setContent(mCon.getResources().getString(R.string.retry)).build().show();
                }
                MDEBUG.debug("Error?");
                MergeApplication.getMergeApplication().hideLoading(mCon);

            }

            @Override
            public void onFailure(Call<ResponseObject<ArrayList<CouponV1>>> call, Throwable t) {
                MDEBUG.debug("Error!" + t.toString());
                new MergeDialog.Builder(mCon).setCancelBtn(false).setContent(mCon.getResources().getString(R.string.retry)).build().show();
                MergeApplication.getMergeApplication().hideLoading(mCon);

            }
        };
    }
}
