package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.Iterator;

import kr.co.mergepoint.mergeclient.scene.shoplist.view.listener.FilterObserver;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.listener.OnFilterChange;

import static kr.co.mergepoint.mergeclient.application.common.Properties.DOWN;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NORMAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SELECT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UP;

/**
 * Created by 1017sjg on 2017. 8. 25..
 */

public class StateButton extends AppCompatButton implements View.OnClickListener, FilterObserver {

    private OnClickListener onClickListener;
    private ArrayList<Drawable> drawables;
    private Iterator<Drawable> iteratorDrawable;

    private OnFilterChange onFilterChange;
    private ArrayList<Integer> colors;
    private Iterator<Integer> iteratorColor;

    private int currentState;

    public void setCurrentState(int currentIndex) {
        if (drawables.size() == 2) {
            /* 기본, 셀렉트 */
            switch (currentIndex) {
                case 0:
                    this.currentState = NORMAL;
                    break;
                case 1:
                    this.currentState = SELECT;
                    break;
                default:
                    this.currentState = NORMAL;
                    break;
            }
        } else if (drawables.size() == 3) {
            /* 기본, 업, 다운 */
            switch (currentIndex) {
                case 0:
                    this.currentState = NORMAL;
                    break;
                case 1:
                    this.currentState = UP;
                    break;
                case 2:
                    this.currentState = DOWN;
                    break;
                default:
                    break;
            }
        }
    }

    /* 눌러진 상태에 따른 STATE 값 */
    @Properties.StateButton
    public int getState() {
        return currentState;
    }

    /* BIND로 버튼의 리소스 추가 */
    public void setDrawableClick(OnClickListener onClickListener, ArrayList<Drawable> drawables) {
        this.onClickListener = onClickListener;
        if (drawables.size() > 0) {
            this.drawables = drawables;
            resetDrawable();
        }
    }

    public void setColorClick(OnClickListener onClickListener, ArrayList<Integer> colors) {
        this.onClickListener = onClickListener;
        if (colors.size() > 0) {
            this.colors = colors;
            resetColor();
        }
    }


    /* 버튼 초기화 */
    public void resetDrawable() {
        if (drawables != null && drawables.size() > 0) {
            this.iteratorDrawable = drawables.iterator();
            setBackgroundDrawable(iteratorDrawable.next());
        }
    }

    public void resetColor() {
        if (colors != null && colors.size() > 0) {
            this.iteratorColor = colors.iterator();
            setBackgroundColor(iteratorColor.next());
        }
    }

    /* 버튼 셀렉트 */
    public void select() {
        if (drawables != null && drawables.size() > 0) {
            setCurrentDrawable();
        } else if (colors != null && colors.size() > 0) {
            setCurrentColor();
        }
    }

    /* 생성자 */
    public StateButton(Context context) {
        super(context);
        init();
    }

    public StateButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StateButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /* 생성자 초기화 */
    private void init() {
        setOnClickListener(this);
    }

    /* 내부 클릭 리스너 */
    @Override
    public void onClick(View view) {
        if (onClickListener != null) {
            if (drawables != null && drawables.size() > 0) {
                setCurrentDrawable();
            } else if (colors != null && colors.size() > 0) {
                setCurrentColor();
            }
            onClickListener.onClick(view);
        }
    }

    private void setCurrentDrawable() {
        if (!iteratorDrawable.hasNext())
            iteratorDrawable = drawables.iterator();
        Drawable currentDrawable = iteratorDrawable.next();
        int currentIndex = drawables.indexOf(currentDrawable);
        setCurrentState(currentIndex);
        setBackgroundDrawable(currentDrawable);
    }

    private void setCurrentColor() {
        if (!iteratorColor.hasNext())
            iteratorColor = colors.iterator();
        int currentColor = iteratorColor.next();
        int currentIndex = colors.indexOf(currentColor);
        setCurrentState(currentIndex);
        setBackgroundColor(currentColor);
    }

    public void setOnFilterChange(OnFilterChange onFilterChange) {
        this.onFilterChange = onFilterChange;
        onFilterChange.add(this);
    }

    @Override
    public void onChangeFilter(int viewId) {
        if (getId() != NO_ID && getId() != viewId) {
            resetDrawable();
            resetColor();
        }
    }
}
