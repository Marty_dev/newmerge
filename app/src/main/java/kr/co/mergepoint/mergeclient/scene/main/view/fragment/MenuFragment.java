package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.MenuBinding;
import kr.co.mergepoint.mergeclient.databinding.MenuMemberBinding;
import kr.co.mergepoint.mergeclient.databinding.MenuNonMemberBinding;
import kr.co.mergepoint.mergeclient.databinding.MenuToolbarBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;

import static android.support.constraint.ConstraintSet.BOTTOM;
import static android.support.constraint.ConstraintSet.END;
import static android.support.constraint.ConstraintSet.PARENT_ID;
import static android.support.constraint.ConstraintSet.START;
import static android.support.constraint.ConstraintSet.TOP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_ROLE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_ROLE2;

/**
 * Created by jgson on 2017. 6. 6..
 */

public class MenuFragment extends BaseFragment {

    public static MenuFragment newInstance() {
        Bundle args = new Bundle();
        MenuFragment fragment = new MenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MenuBinding menuBinding = DataBindingUtil.inflate(inflater, R.layout.menu, container, false);
        menuBinding.setOnClick(this);
        InitialInfo initialInfo = MergeApplication.getInitialInfo();
        if (initialInfo != null) {
            View view;
            if (initialInfo.isSignin()) {
                MenuMemberBinding menuMemberBinding = DataBindingUtil.inflate(inflater, R.layout.menu_member, container, false);
                menuMemberBinding.setInitInfo(initialInfo);
                menuMemberBinding.setMenuClick(this);
                view = menuMemberBinding.getRoot();

            } else {
                MenuNonMemberBinding menuNonMemberBinding = DataBindingUtil.inflate(inflater, R.layout.menu_non_member, container, false);
                menuNonMemberBinding.setMenuClick(this);
                view = menuNonMemberBinding.getRoot();
            }
            view.setId(View.generateViewId());
            menuBinding.menuRoot.addView(view);

            DisplayMetrics dm = getRootActivity().getApplicationContext().getResources().getDisplayMetrics();

            ConstraintSet set = new ConstraintSet();
            set.clone(menuBinding.menuRoot);
            set.connect(view.getId(), TOP, PARENT_ID, TOP);
            set.connect(view.getId(), BOTTOM, menuBinding.menuCenter.getRoot().getId(), TOP);
            set.connect(view.getId(), START, PARENT_ID, START);
            set.connect(view.getId(), END, PARENT_ID, END);
            set.applyTo(menuBinding.menuRoot);
            MenuToolbarBinding menuToolbar = menuBinding.toolbarBinding;
            menuToolbar.setInitInfo(initialInfo);

            Member member = initialInfo.getMember();
            menuBinding.menuCenter.enterpriseMenu.setVisibility(member != null && (member.getRoles().indexOf(ENTERPRISE_ROLE) >= 0 || member.getRoles().indexOf(ENTERPRISE_ROLE2) >= 0) ? View.VISIBLE : View.GONE);
            menuBinding.menuBottom.noticeText.setText(Html.fromHtml(" <b>운영시간</b> 09:00 - 18:00<br/>매주 토, 일, 법정 공휴일 휴무"));
        }
        return menuBinding.getRoot();
    }
}
