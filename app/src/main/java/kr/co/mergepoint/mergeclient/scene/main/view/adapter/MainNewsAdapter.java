package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.application.common.DateModel;
import kr.co.mergepoint.mergeclient.databinding.MainNewsItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.Notice;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class MainNewsAdapter extends BasicListAdapter<BasicListHolder<MainNewsItemBinding, Notice>, Notice> {

    private DateModel dateModel;

    public MainNewsAdapter(ArrayList<Notice> notices) {
        super(notices);
        dateModel = new DateModel();
    }

    @Override
    public BasicListHolder<MainNewsItemBinding, Notice> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_news_item, parent, false);
        MainNewsItemBinding itemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<MainNewsItemBinding, Notice>(itemBinding) {
            @Override
            public void setDataBindingWithData(Notice data) {
                getDataBinding().setNotice(data);
                getDataBinding().setIsLast(getObservableArrayList().indexOf(data) == 2);
                getDataBinding().setIsNew(data.creDateTime != null && dateModel.getCompareDateOfDays(3, dateModel.getDate(data.creDateTime)));
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<MainNewsItemBinding, Notice> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
