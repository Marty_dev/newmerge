package kr.co.mergepoint.mergeclient.application.common;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.PathMeasure;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

/**
 * Created by 1017sjg on 2017. 8. 10..
 */

public class LikeVitalSign extends View {

    float length;

    private Path path;
    private Paint paint;
//    private ObjectAnimator animator;

    private int width;
    private int height;

    public LikeVitalSign(Context context) {
        super(context);
        init();
    }

    public LikeVitalSign(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LikeVitalSign(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LikeVitalSign(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        width = dpToPixel(getContext(), getMeasuredWidth());
        height = dpToPixel(getContext(), getMeasuredHeight());
    }

    public int dpToPixel(Context context, int DP) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DP, context.getResources().getDisplayMetrics());
        return (int) px;
    }

    public void startVital() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.RED);
        paint.setStrokeWidth(10);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);

        int y = dpToPixel(getContext(), 40);

        path = new Path();
        path.moveTo(dpToPixel(getContext(), 350), y);
        path.lineTo(dpToPixel(getContext(), 300), y);
        path.lineTo(dpToPixel(getContext(), 295), y - 30);
        path.lineTo(dpToPixel(getContext(), 290), y + 80);
        path.lineTo(dpToPixel(getContext(), 285), y - 100);
        path.lineTo(dpToPixel(getContext(), 280), y);
        path.lineTo(dpToPixel(getContext(), 275), y - 60);
        path.lineTo(dpToPixel(getContext(), 270), y);
        path.lineTo(dpToPixel(getContext(), 100), y);
        path.lineTo(dpToPixel(getContext(), 95), y - 30);
        path.lineTo(dpToPixel(getContext(), 90), y + 80);
        path.lineTo(dpToPixel(getContext(), 85), y - 100);
        path.lineTo(dpToPixel(getContext(), 80), y);
        path.lineTo(dpToPixel(getContext(), 75), y - 60);
        path.lineTo(dpToPixel(getContext(), 70), y);
        path.lineTo(0, y);

        PathMeasure measure = new PathMeasure(path, false);
        length = measure.getLength();

        float[] intervals = new float[]{length, length};

        ObjectAnimator animator = ObjectAnimator.ofFloat(LikeVitalSign.this, "phase", 1.0f, 0.0f);
        animator.setDuration(445);
        animator.setRepeatCount(1);
        animator.setRepeatMode(ValueAnimator.REVERSE);
        animator.start();

        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                init();
            }
        });
    }

    public void setPhase(float phase) {
        paint.setPathEffect(createPathEffect(length, phase, 0.0f));
        invalidate();
    }

    private static PathEffect createPathEffect(float pathLength, float phase, float offset) {
        return new DashPathEffect(new float[] { pathLength, pathLength }, Math.max(phase * pathLength, offset));
    }

    @Override
    public void onDraw(Canvas c) {
        super.onDraw(c);
        if (paint != null && path != null)
            c.drawPath(path, paint);
    }
}
