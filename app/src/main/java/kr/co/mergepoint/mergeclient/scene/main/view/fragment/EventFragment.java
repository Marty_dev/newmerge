package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.EventApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.ExpandableItemListener;
import kr.co.mergepoint.mergeclient.databinding.EventBinding;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.main.EventInfo;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.EventAdapter;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder.Event;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jgson on 2017. 7. 19..
 */

public class EventFragment extends BaseFragment {

    private EventBinding eventBinding;
    private ExpandableItemListener.ExpandableTouchListener expandableTouchListener;

    public static EventFragment newInstance(ExpandableItemListener.ExpandableTouchListener expandableTouchListener) {
        Bundle args = new Bundle();
        EventFragment fragment = new EventFragment();
        fragment.expandableTouchListener = expandableTouchListener;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        eventBinding = DataBindingUtil.inflate(inflater, R.layout.event, container, false);
        eventBinding.setTitle(getString(R.string.event));
        eventBinding.setClick(this);

        return eventBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        requestMergesPickList();
    }

    private void requestMergesPickList() {
        MergeApplication.getMergeApplication().showLoading(rootActivity);
        EventApi eventApi = MergeApplication.getMergeAppComponent().getRetrofit().create(EventApi.class);
        Call<ResponseObject<ArrayList<EventInfo>>> call = eventApi.requestEventList();
        call.enqueue(getMergesPickListCallback());
    }

    private Callback<ResponseObject<ArrayList<EventInfo>>> getMergesPickListCallback() {
        return new Callback<ResponseObject<ArrayList<EventInfo>>>() {
            @Override
            public void onResponse(@NonNull Call<ResponseObject<ArrayList<EventInfo>>> call, @NonNull Response<ResponseObject<ArrayList<EventInfo>>> response) {
                ResponseObject<ArrayList<EventInfo>> responseObject = response.body();
                if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                    eventBinding.setExpandableAdapter(new EventAdapter(getEvent(responseObject.getObject())));
                    eventBinding.setChildTouch(expandableTouchListener);
                    eventBinding.getExpandableAdapter().toggleGroup(0);
                }

                delayHideLoading();
            }

            @Override
            public void onFailure(@NonNull Call<ResponseObject<ArrayList<EventInfo>>> call, @NonNull Throwable t) {
                showAlert(R.string.retry);
                delayHideLoading();
            }
        };
    }

    private List<Event> getEvent(ArrayList<EventInfo> eventInfos) {
        ArrayList<Event> eventArrayList = new ArrayList<>();

        ArrayList<EventInfo> ongoingEventList = new ArrayList<>();
        ArrayList<EventInfo> closedEventList = new ArrayList<>();

        for (EventInfo info : eventInfos) {
            if (info.state == 1) {
                ongoingEventList.add(info);
            } else {
                closedEventList.add(info);
            }
        }
        eventArrayList.add(new Event(getString(R.string.ongoing_event),ongoingEventList, true));
        eventArrayList.add(new Event(getString(R.string.closed_event),closedEventList, false));

        return eventArrayList;
    }
}
