package kr.co.mergepoint.mergeclient.scene.privacy.dagger;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.privacy.PrivacyActivity;
import kr.co.mergepoint.mergeclient.scene.privacy.model.PrivacyModel;
import kr.co.mergepoint.mergeclient.scene.privacy.presenter.PrivacyPresenter;
import kr.co.mergepoint.mergeclient.scene.privacy.presenter.callback.PrivacyCallback;
import kr.co.mergepoint.mergeclient.scene.privacy.view.PrivacyView;

/**
 * Created by jgson on 2017. 6. 5..
 */

@Module
public class PrivacyModule {

    private PrivacyActivity activity;

    public PrivacyModule(PrivacyActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PrivacyScope
    PrivacyModel provideModel() { return new PrivacyModel(); }

    @Provides
    @PrivacyScope
    PrivacyView provideView() { return new PrivacyView(activity, R.layout.privacy); }

    @Provides
    @PrivacyScope
    PrivacyPresenter providePresenter(PrivacyModel model, PrivacyView view) { return new PrivacyPresenter(view, model, new PrivacyCallback(view, model)); }
}
