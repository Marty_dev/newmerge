package kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.EnterpriseMenuTypeItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;

/**
 * Created by 1017sjg on 2017. 6. 24..
 */

public class EnterpriseMenuChildAdapter extends BasicListAdapter<BasicListHolder<EnterpriseMenuTypeItemBinding, ShopMenuDetail>, ShopMenuDetail> {

    public EnterpriseMenuChildAdapter(ArrayList<ShopMenuDetail> menuGroups) {
        super(menuGroups);
    }

    @Override
    public BasicListHolder<EnterpriseMenuTypeItemBinding, ShopMenuDetail> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.enterprise_menu_type_item, parent, false);
        EnterpriseMenuTypeItemBinding itemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<EnterpriseMenuTypeItemBinding, ShopMenuDetail>(itemBinding) {
            @Override
            public void setDataBindingWithData(ShopMenuDetail data) {
                getDataBinding().setTypeName(data.name);
                getDataBinding().setTypePrice(data.price);
                getDataBinding().setMenuString(data.comment);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<EnterpriseMenuTypeItemBinding, ShopMenuDetail> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
