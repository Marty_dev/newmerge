package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.animation.ObjectAnimator;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.GradeBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.GradeRes;
import kr.co.mergepoint.mergeclient.scene.data.main.MemberGrade;
import kr.co.mergepoint.mergeclient.scene.main.model.MainModel;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.GradeAdapter;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 1017sjg on 2017. 7. 19..
 */

public class GradeFragment extends BaseFragment {

    private GradeBinding gradeBinding;
    private int grade;
    private MainModel model;
    private GradeRes Res;
    private int[] gradeTermsPrices;
    private String[] gradeTermsName;


    public static GradeFragment newInstance(MainModel model) {
        Bundle args = new Bundle();
        GradeFragment fragment = new GradeFragment();
        fragment.grade = MergeApplication.getInitialInfo().getMember().getGrade();
        fragment.setArguments(args);
        fragment.model = model;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        String[] grades = getResources().getStringArray(R.array.grade);
        gradeBinding = DataBindingUtil.inflate(inflater, R.layout.grade, container, false);
        gradeBinding.setTitle(getString(R.string.rating_info));
        gradeBinding.setClick(this);
        gradeBinding.setMember(MergeApplication.getInitialInfo().getMember());
        gradeBinding.setGradeText(grades[grade - 1]);
        gradeBinding.setGradeNextText(grades[(grade != 5 ? grade + 1 : grade) - 1]);
        return gradeBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

    }
    private void onInit(GradeRes res){
        gradeTermsName = new String[res.gradeList.size()];
        for (int i = 0 ; i < res.gradeList.size();i++){
            gradeTermsName[i] = res.gradeList.get(i).kindName;
            if (res.grade == res.gradeList.get(i).kindOrder){
                // TODO: 2018-08-17  progress Setting
                MemberGrade obj = res.gradeList.get(i);
                gradeBinding.userGrade.setText(getResources().getString(R.string.current_grade,gradeBinding.getMember().getName(),obj.kindName));

                gradeBinding.userGradeProgressCurrent.setText(obj.kindName);
                gradeBinding.userGradeProgressNext.setText(res.gradeList.get(i).kindName);

                String current = res.grade == 1 ? obj.conditionCount + "회" : String.format("%,d",obj.conditionPrice) + "원";
                String next = res.grade == 1 ?   "1회" : String.format("%,d",res.gradeList.get(res.grade == 5 ? res.grade - 1 : res.grade  + 1).conditionPrice) + "원";

                gradeBinding.userGradeProgressStart.setText(current);
                gradeBinding.userGradeProgressEnd.setText(next);

                if (res.grade != 1 && res.grade != 5) {
                    gradeBinding.userGradeProgressText.setText(String.format("%,d", res.nextPrice) + "원");
                }else if (res.grade == 1){
                    gradeBinding.userGradeProgressText.setText("결제 1회");
                }else if (res.grade == 5){
                    gradeBinding.userGradeProgressText.setText(String.format("%,d",res.gradeList.get(4).conditionPrice) + "원 이상");
                    gradeBinding.userGradeProgressCurrent.setVisibility(View.GONE);
                    gradeBinding.userGradeProgressNext.setVisibility(View.GONE);
                    gradeBinding.userGradeProgressStart.setVisibility(View.GONE);
                    gradeBinding.userGradeProgressEnd.setVisibility(View.GONE);
                    gradeBinding.userGradeProgress.setVisibility(View.GONE);
                }


                if (res.grade == 5){
                    gradeBinding.userGradeProgress.setVisibility(View.INVISIBLE);
                    gradeBinding.userGradeProgressStart.setVisibility(View.INVISIBLE);
                    gradeBinding.userGradeProgressEnd.setVisibility(View.INVISIBLE);
                    gradeBinding.userGradeProgressCurrent.setVisibility(View.INVISIBLE);
                    gradeBinding.userGradeProgressNext.setVisibility(View.INVISIBLE);
                    gradeBinding.userGradeText01.setText("누적 결제금액");

                }else{
                    gradeBinding.userGradeProgress.setVisibility(View.VISIBLE);
                    gradeBinding.userGradeProgressStart.setVisibility(View.VISIBLE);
                    gradeBinding.userGradeProgressEnd.setVisibility(View.VISIBLE);
                    gradeBinding.userGradeProgressCurrent.setVisibility(View.VISIBLE);
                    gradeBinding.userGradeProgressNext.setVisibility(View.VISIBLE);

                }
            }
        }

        gradeBinding.setPagerAdapter(new GradeAdapter(rootActivity, gradeTermsName,res));
        gradeBinding.selectGradeLayout.setupWithViewPager(gradeBinding.gradeDetailPager);
        gradeBinding.setDetailPagerListener(new TabLayout.TabLayoutOnPageChangeListener(gradeBinding.selectGradeLayout));
        startGradeProgressAnim(Res);
    }

    @Override
    public void onStart() {
        super.onStart();
        model.readMemberGrade(new Callback<GradeRes>() {
            @Override
            public void onResponse(Call<GradeRes> call, Response<GradeRes> response) {
                Res = response.body();

                gradeTermsPrices = new int[Res.gradeList.size()];
                for (int i = 0; i < Res.gradeList.size(); i++){
                    gradeTermsPrices[i] = Res.gradeList.get(i).conditionPrice;
                }
                onInit(Res);
            }

            @Override
            public void onFailure(Call<GradeRes> call, Throwable t) {

            }
        });
    }

    private void startGradeProgressAnim(GradeRes res) {
        MDEBUG.debug("Progress Setting");
        new Handler().postDelayed(() -> {
            MDEBUG.debug("Progress is Animate");
            gradeBinding.gradeDetailPager.setCurrentItem(grade - 1);
            int[] gradePrice = gradeTermsPrices;//getContext().getResources().getIntArray(R.array.grade_price);//
            int currentGrade = res.grade;//gradeBinding.getMember().getGrade();//

            int preGradePrice = currentGrade == 1 ? gradePrice[currentGrade - 1] : gradePrice[currentGrade - 2];
            int currentGradePrice = gradePrice[currentGrade - 1];
            int nextGradePrice = currentGrade == 5 ? gradePrice[currentGrade - 1] : gradePrice[currentGrade];
            int remainingPrice = res.nextPrice;//gradeBinding.getMember().getNextPrice();//

            int gradeGap = nextGradePrice - currentGradePrice;

            int progressValue = remainingPrice >= gradeGap ? 0 : (int) (((gradeGap - remainingPrice) / (double) gradeGap) * 100);
            ObjectAnimator animation = ObjectAnimator.ofInt(gradeBinding.userGradeProgress, "progress", gradeBinding.userGradeProgress.getProgress(), progressValue);
            animation.setDuration(700);
            animation.setInterpolator(new DecelerateInterpolator());
            animation.start();
        }, 200);
    }
}
