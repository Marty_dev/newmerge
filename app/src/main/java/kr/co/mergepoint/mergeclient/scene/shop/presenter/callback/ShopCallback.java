package kr.co.mergepoint.mergeclient.scene.shop.presenter.callback;

import android.os.Bundle;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseCallback;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.application.common.WideBannerAdapter;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.main.CombineImagesDetail;
import kr.co.mergepoint.mergeclient.scene.data.menu.ShopGroupCategory;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.PreExperience;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.GoogleSearchInfo;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.MergeReview;
import kr.co.mergepoint.mergeclient.scene.shop.model.ShopModel;
import kr.co.mergepoint.mergeclient.scene.shop.view.ShopView;
import okhttp3.ResponseBody;

import static kr.co.mergepoint.mergeclient.application.common.Properties.COMBINE_IMG;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class ShopCallback extends BaseCallback<ShopView, ShopModel> implements ShopCallbackInterface {

    public ShopCallback(ShopView baseView, ShopModel baseModel) {
        super(baseView, baseModel);
    }

    @Override
    public MergeCallback<ShopDetail> shopDetailCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ShopDetail detail = response.body();
            if (response.isSuccessful() && detail != null) {
                baseView.binding.setShopDetail(detail);
                baseView.binding.getPagerAdapter().getInfoFragment().setShopDetail(detail);
                baseView.binding.setImageAdapter(new WideBannerAdapter(R.layout.single_image, detail.shopImage, R.drawable.img_not_shop_pic, null));
                baseView.binding.shopDetailPictureLayout.shopPictureIndicator.createDotPanel(detail.shopImage != null ? detail.shopImage.size() : 0);
                baseModel.getMenuCategory(detail.oid, getMenuGroupCategory());
                baseModel.requestMergeReview(getMergeReview(), detail.oid, 0);
//                baseModel.requestGoogleReview(getGoogleReviewCallback(), 0, baseView.getString(R.string.google_search_query, detail.shopName, MergeApplication.getStreetName(detail.streetCodeRef)));
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Integer>> getLikeCountCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Integer> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                int likeCount = responseObject.getObject();
                baseView.binding.getShopDetail().setLikeCount(likeCount);
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseBody> addBasketCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseBody responseBody = response.body();
            if (response.isSuccessful() && responseBody != null) {
                baseView.animBasket();
                baseView.activity.onBackPressed();
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ShopMenuDetail> addMenuDetail() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ShopMenuDetail detail = response.body();
            if (response.isSuccessful() && detail != null)
                baseView.openMenuDetail(detail);
        });
    }

    @Override
    public MergeCallback<ResponseObject<Integer>> getCheckFavoriteCall() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Integer> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                int count = responseObject.getObject();
                ShopDetail shopDetail = baseView.binding.getShopDetail();
                if (shopDetail != null) {
                    shopDetail.favorite = !shopDetail.favorite;
                    shopDetail.setFavoriteCount(count);
                }
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<PreExperience>>> getPreExperienceCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<PreExperience>> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed())
                baseView.openPreExperience(responseObject.getObject(), baseView.binding.getShopDetail().shopName);
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CombineImagesDetail>>> getMergesPickCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CombineImagesDetail>> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(COMBINE_IMG, responseObject.getObject());
                baseView.openMergesPickDetail(bundle);
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<GoogleSearchInfo> getGoogleReviewCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
//            GoogleSearchInfo responseObject = response.body();
//            if (response.isSuccessful() && responseObject != null) {
//                baseView.activity.runOnUiThread(() -> baseView.setReviewAdapter(responseObject));
//            }
        });
    }

    @Override
    public MergeCallback<ShopGroupCategory> getMenuGroupCategory() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ShopGroupCategory category = response.body();
            if (response.isSuccessful() && category != null) {
                baseView.getShopMenuFragment().setMenuGroup(category.getGroupMenus());
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<MergeReview>> getMergeReview() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<MergeReview> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                baseView.activity.runOnUiThread(() -> baseView.setReviewAdapter(responseObject.getObject()));
            }
        });
    }
}
