package kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.FilterItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.SelectItem;

/**
 * Created by 1017sjg on 2017. 8. 29..
 */

public class FilterAdapter extends BasicListAdapter<BasicListHolder<FilterItemBinding, SelectItem>, SelectItem> implements FilterHolder.OnItemSelectedListener{

    private FilterHolder.OnItemSelectedListener onItemSelectedListener;
    private boolean isTheme;

    public FilterAdapter(ArrayList<SelectItem> arrayList, FilterHolder.OnItemSelectedListener onItemSelectedListener, boolean isTheme) {
        super(arrayList);
        this.onItemSelectedListener = onItemSelectedListener;
        this.isTheme = isTheme;
    }

    @Override
    public BasicListHolder<FilterItemBinding, SelectItem> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_item, parent, false);
        FilterItemBinding binding = DataBindingUtil.bind(view);
        return new FilterHolder(binding, this);
    }

    @Override
    public void setBindViewHolder(BasicListHolder<FilterItemBinding, SelectItem> holder, int position) {
        SelectItem selectItem = getObservableArrayList().get(position);
        holder.bind(selectItem);
        holder.getDataBinding().stateBtn.resetDrawable();
        if (selectItem.isSelected())
            holder.getDataBinding().stateBtn.select();
    }

    @Override
    public void onItemSelected(SelectItem item) {
        if (isTheme) {
            for (SelectItem selectItem : getObservableArrayList()) {
                if (!selectItem.equals(item))
                    selectItem.setSelected(false);

            }
            notifyItemRangeChanged(0, getObservableArrayList().size());
        } else {
            notifyItemChanged(getObservableArrayList().indexOf(item));
        }

        onItemSelectedListener.onItemSelected(item);
    }
}
