package kr.co.mergepoint.mergeclient.scene.myzonelist.dagger;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocation;
import kr.co.mergepoint.mergeclient.scene.myzonelist.MyZoneListActivity;
import kr.co.mergepoint.mergeclient.scene.myzonelist.presenter.callback.MyZoneListCallback;
import kr.co.mergepoint.mergeclient.scene.myzonelist.model.MyZoneListModel;
import kr.co.mergepoint.mergeclient.scene.myzonelist.presenter.MyZoneListPresenter;
import kr.co.mergepoint.mergeclient.scene.myzonelist.view.MyZoneListView;

import static kr.co.mergepoint.mergeclient.application.common.Properties.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UPDATE_INTERVAL_IN_MILLISECONDS;

/**
 * Created by jgson on 2017. 6. 5..
 */

@Module
public class MyZoneListModule {

    private MyZoneListActivity activity;

    public MyZoneListModule(MyZoneListActivity activity) {
        this.activity = activity;
    }

    @Provides
    @MyZoneListScope
    FusedLocationProviderClient provideGoogleApiClientBuilder() {
        return LocationServices.getFusedLocationProviderClient(activity);
    }

    @Provides
    @MyZoneListScope
    LocationRequest provideLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        return locationRequest;
    }

    @Provides
    @MyZoneListScope
    UserLocation provideUserLocation(FusedLocationProviderClient providerClient, LocationRequest locationRequest) {
        return new UserLocation(providerClient, activity, locationRequest, activity);
    }

    @Provides
    @MyZoneListScope
    MyZoneListModel provideModel() { return new MyZoneListModel(); }

    @Provides
    @MyZoneListScope
    MyZoneListView provideView() { return new MyZoneListView(activity, R.layout.my_zone_list); }

    @Provides
    @MyZoneListScope
    MyZoneListPresenter providePresenter(MyZoneListView view, MyZoneListModel model, UserLocation userLocation) {
        return new MyZoneListPresenter(view, model, new MyZoneListCallback(view, model), userLocation);
    }
}
