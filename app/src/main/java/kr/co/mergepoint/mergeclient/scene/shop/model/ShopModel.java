package kr.co.mergepoint.mergeclient.scene.shop.model;

import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import kr.co.mergepoint.mergeclient.api.BasketApi;
import kr.co.mergepoint.mergeclient.api.MergesPickApi;
import kr.co.mergepoint.mergeclient.api.ShopApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseModel;
import kr.co.mergepoint.mergeclient.scene.data.basket.AddBasket;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.main.CombineImagesDetail;
import kr.co.mergepoint.mergeclient.scene.data.menu.ShopGroupCategory;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.PreExperience;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.GoogleSearchInfo;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.MergeReview;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static kr.co.mergepoint.mergeclient.application.common.Properties.CONNECT_TIMEOUT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.GOOGLE_SEARCH_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.READ_TIMEOUT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WRITE_TIMEOUT;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class ShopModel extends BaseModel {

    private ShopApi shopApi;
    private BasketApi basketApi;
    private MergesPickApi mergesPickApi;

    public ShopModel() {
        shopApi = retrofit.create(ShopApi.class);
        basketApi = retrofit.create(BasketApi.class);
        mergesPickApi = retrofit.create(MergesPickApi.class);
    }

    public void getShopDetailInfo(int oid, Callback<ShopDetail> callback) {
        shopApi.getShopDetailInfo(oid).enqueue(callback);
    }

    public void getMenuCategory(int oid, Callback<ShopGroupCategory> callback) {
        shopApi.getShopMenuInfo(oid).enqueue(callback);
    }

    public void getMenuDetailInfo(int menuOid, Callback<ShopMenuDetail> callback) {
        shopApi.getMenuDetailInfo(menuOid).enqueue(callback);
    }

    public void addShoppingBasket(Callback<ResponseBody> callback, AddBasket selectMenu) {
        basketApi.addShoppingBasket(selectMenu).enqueue(callback);
    }

    public void sendLikeCount(Callback<ResponseObject<Integer>> callback, int shopOid, int count) {
        shopApi.checkLike(shopOid, count).enqueue(callback);
    }

    public void checkFavoriteCall(Callback<ResponseObject<Integer>> callback, int shopOid) {
        shopApi.checkFavorite(shopOid).enqueue(callback);
    }

    public void getPreExperience(Callback<ResponseObject<ArrayList<PreExperience>>> callback, int shopOid) {
        shopApi.getPreExperienceInfo(shopOid).enqueue(callback);
    }

    public void requestMergesPickDetail(Callback<ResponseObject<ArrayList<CombineImagesDetail>>> callback, int oid) {
        mergesPickApi.requestMergesPickDetail(oid).enqueue(callback);
    }

    public void requestGoogleReview(Callback<GoogleSearchInfo> callback, int start, String query) {
        String key = MergeApplication.getUtility().getApiKeyFromManifest("google.key");
        String cx = MergeApplication.getUtility().getApiKeyFromManifest("google.cx");

        Map<String, String> params = new HashMap<>();
        params.put("key", key);
        params.put("fields", "items(title,link,displayLink,snippet,pagemap(cse_thumbnail/src)),queries/nextPage(count,startIndex)");
        params.put("cx", cx);
        params.put("start", String.valueOf(start * 10 + 1));
        params.put("q", query);
        params.put("filter", "1");
        params.put("cr", "countryKR");
        params.put("safe", "high");
        params.put("as_qdr", "y");

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .baseUrl(GOOGLE_SEARCH_URL)
                .client(okHttpClient)
                .build();

        retrofit.create(ShopApi.class).getGoogleSearchReview(params).enqueue(callback);
    }

    public void requestMergeReview(Callback<ResponseObject<MergeReview>> callback, int shopOid, int pageNum) {
        shopApi.getShopReview(shopOid, pageNum).enqueue(callback);
    }
}
