package kr.co.mergepoint.mergeclient.application.common;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

/**
 * Created by 1017sjg on 2017. 6. 30..
 */

public abstract class BasicListHolder<T extends ViewDataBinding, S> extends RecyclerView.ViewHolder {

    private T dataBinding;
    private S data;

    public boolean flag;
    public BasicListHolder(T dataBinding) {
        super(dataBinding.getRoot());
        this.dataBinding = dataBinding;
    }
    public BasicListHolder(T dataBinding,boolean flag) {
        super(dataBinding.getRoot());
        this.dataBinding = dataBinding;
        this.flag = flag;
    }
    public void bind(S data) {
        setDataBindingWithData(data);
        dataBinding.executePendingBindings();
    }

    public T getDataBinding() {
        return dataBinding;
    }

    public S getData() {
        return data;
    }

    public void setData(S data) {
        this.data = data;
    }

    public abstract void setDataBindingWithData (S data);
}
