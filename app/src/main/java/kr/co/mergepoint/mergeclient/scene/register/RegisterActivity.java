package kr.co.mergepoint.mergeclient.scene.register;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import javax.inject.Inject;

import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.SmsMessageReceiver;
import kr.co.mergepoint.mergeclient.scene.register.dagger.DaggerRegisterComponent;
import kr.co.mergepoint.mergeclient.scene.register.dagger.RegisterModule;
import kr.co.mergepoint.mergeclient.scene.register.presenter.RegisterPresenter;

/**
 * Created by jgson on 2017. 5. 31..
 */

public class RegisterActivity extends BaseActivity implements SmsMessageReceiver.OnSmsReceiver{

    @Inject RegisterPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerRegisterComponent.builder().registerModule(new RegisterModule(this)).build().inject(this);
        presenter.onCreate();
    }

    @Override
    protected void onActivityClick(View view) {}

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onReceiveSms(String certificationNum) {
        presenter.onReceiveSms(certificationNum);
    }

    // 권한
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
