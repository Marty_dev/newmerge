package kr.co.mergepoint.mergeclient.scene.enterprise.shop.model;

import android.widget.Button;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.api.BasketApi;
import kr.co.mergepoint.mergeclient.api.ShopApi;
import kr.co.mergepoint.mergeclient.application.common.BaseModel;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.basket.AddBasket;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.menu.ShopGroupCategory;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import okhttp3.ResponseBody;
import retrofit2.Callback;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class EnterpriseShopModel extends BaseModel {

    private ShopApi shopApi;
    private BasketApi basketApi;
    private Calendar calendar;
    public boolean isDailymenu;
    public Button Dailybtn;


    public Calendar getCalendar() {
        return calendar;
    }

    public EnterpriseShopModel() {
        shopApi = retrofit.create(ShopApi.class);
        basketApi = retrofit.create(BasketApi.class);
    }

    public void getShopDetailInfo(int oid, Callback<ShopDetail> callback) {
        shopApi.getShopDetailInfo(oid).enqueue(callback);
    }

    public void getMenuCategory(int oid, MergeCallback<ShopGroupCategory> callback) {
        shopApi.getShopMenuInfo(oid).enqueue(callback);
    }

    public void getMenuDetailInfo(int menuOid, Callback<ShopMenuDetail> callback) {
        shopApi.getMenuDetailInfo(menuOid).enqueue(callback);
    }

    public void addShoppingBasket(Callback<ResponseBody> callback, AddBasket selectMenu) {
        basketApi.addShoppingBasket(selectMenu).enqueue(callback);
    }

    public void checkFavoriteCall(Callback<ResponseObject<Integer>> callback, int shopOid) {
        shopApi.checkFavorite(shopOid).enqueue(callback);
    }

    public void getDailyMenuList(Callback<ResponseObject<ArrayList<ShopMenuDetail>>> callback, int shopOid, int add) {
        if (calendar == null) {
            calendar = Calendar.getInstance();
            calendar.setTimeInMillis(new Date().getTime());
        } else {
            calendar.add(Calendar.DAY_OF_MONTH, add);
        }
        int year = calendar.get(Calendar.YEAR);
        int month =  calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        shopApi.getShopDailyMenuList(shopOid, String.format(Locale.getDefault(), "%d-%02d-%02d", year, month, day)).enqueue(callback);
    }
}
