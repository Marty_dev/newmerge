package kr.co.mergepoint.mergeclient.scene.shoppingbasket.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.ShoppingBasketSelectItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionMenu;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionSelectMenu;

/**
 * Created by 1017sjg on 2017. 7. 3..
 */

public class OptionInSelectOptionAdapter extends BasicListAdapter<BasicListHolder<ShoppingBasketSelectItemBinding, ShopOptionSelectMenu>, ShopOptionSelectMenu> {

    OptionInSelectOptionAdapter(ShopOptionMenu optionMenu) {
        super(optionMenu.menus);
    }

    @Override
    public BasicListHolder<ShoppingBasketSelectItemBinding, ShopOptionSelectMenu> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_basket_select_item, parent, false);
        ShoppingBasketSelectItemBinding binding = DataBindingUtil.bind(view);

        return new BasicListHolder<ShoppingBasketSelectItemBinding, ShopOptionSelectMenu>(binding) {
            @Override
            public void setDataBindingWithData(ShopOptionSelectMenu data) {
                getDataBinding().setSelectOption(data);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<ShoppingBasketSelectItemBinding, ShopOptionSelectMenu> holder, int position) {
        ShopOptionSelectMenu detail = getObservableArrayList().get(position);
        holder.bind(detail);
    }
}
