package kr.co.mergepoint.mergeclient.scene.login.view.fragment;

import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.webkit.WebChromeClient;
import android.webkit.WebViewClient;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.CustomChromeClient;
import kr.co.mergepoint.mergeclient.application.common.CustomWebClient;
import kr.co.mergepoint.mergeclient.application.common.script.LoginScriptInterface;
import kr.co.mergepoint.mergeclient.application.common.script.LoginScriptListener;
import kr.co.mergepoint.mergeclient.application.common.SingleWebFragment;

import static kr.co.mergepoint.mergeclient.application.common.Properties.WEB_URL;

/**
 * Created by jgson on 2017. 6. 1..
 */

public class SocialLoginView extends SingleWebFragment<LoginScriptInterface> {

    private LoginScriptListener listener;
    private CustomChromeClient chromeClient;
    private CustomWebClient webClient;

    public static SocialLoginView newInstance(String url, LoginScriptListener listener) {
        Bundle args = new Bundle();
        args.putString(WEB_URL, url);
        SocialLoginView fragment = new SocialLoginView();
        fragment.chromeClient = new CustomChromeClient();
        fragment.webClient = new CustomWebClient();
        fragment.listener = listener;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected WebChromeClient setWebChromeClient(ContentLoadingProgressBar loadProgressBar) {
        chromeClient.setContentLoadingProgressBar(loadProgressBar);
        return chromeClient;
    }

    @Override
    protected WebViewClient setWebViewClient() {
        return webClient;
    }

    @Override
    protected LoginScriptInterface setJavascript() {
        return new LoginScriptInterface(listener);
    }

    @Override
    protected String setPageTitle() {
        return getString(R.string.easy_login_title);
    }
}
