package kr.co.mergepoint.mergeclient.application.common;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

/**
 * Created by 1017sjg on 2017. 10. 25..
 */

public class MergeCallbackV2<T> implements Callback<T> {

    public interface MergeCall<T> {
        void onResponse(Call<T> call, Response<T> response);
    }

    public interface MergeCallWithFailure<T> {
        void onResponse(Call<T> call, Response<T> response);
        void onFailure(@NonNull Call<T> call, @NonNull Throwable t);
    }

    private MergeCall<T> mergeCall;
    private MergeCallWithFailure<T> mergeCallWithFailure;
    private BaseActivity baseView;
    private int failAlertText;

    public MergeCallbackV2(BaseActivity baseView, MergeCall<T> mergeCall) {
        this.mergeCall = mergeCall;
        this.baseView = baseView;
        this.failAlertText = NO_VALUE;
    }

    public MergeCallbackV2(BaseActivity baseView, MergeCall<T> mergeCall, int failAlertText) {
        this.mergeCall = mergeCall;
        this.baseView = baseView;
        this.failAlertText = failAlertText;
    }

    public MergeCallbackV2(BaseActivity baseView, MergeCallWithFailure<T> mergeCall) {
        this.mergeCallWithFailure = mergeCall;
        this.baseView = baseView;
        this.failAlertText = NO_VALUE;
    }

    public MergeCallbackV2(BaseActivity baseView, MergeCallWithFailure<T> mergeCall, int failAlertText) {
        this.mergeCallWithFailure = mergeCall;
        this.baseView = baseView;
        this.failAlertText = failAlertText;
    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {

        if (mergeCall != null) {
            mergeCall.onResponse(call, response);
        } else if (mergeCallWithFailure != null) {
            mergeCallWithFailure.onResponse(call, response);
        }
        MergeApplication.getMergeApplication().hideLoading(baseView);
        baseView.hideKeyboard();
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        if (mergeCallWithFailure != null)
            mergeCallWithFailure.onFailure(call, t);
        MDEBUG.debug("t :  " + t.toString());
        Snackbar.make(baseView.findViewById(android.R.id.content),failAlertText != NO_VALUE ? failAlertText : R.string.retry , Snackbar.LENGTH_SHORT).show();
        MergeApplication.getMergeApplication().hideLoading(baseView);
        baseView.hideKeyboard();

    }
}

