package kr.co.mergepoint.mergeclient.scene.register.presenter;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;

import java.util.Objects;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BasePresenter;
import kr.co.mergepoint.mergeclient.application.common.SimpleWebFragment;
import kr.co.mergepoint.mergeclient.application.common.SmsMessageReceiver;
import kr.co.mergepoint.mergeclient.application.common.script.PostScriptListener;
import kr.co.mergepoint.mergeclient.scene.register.model.RegisterModel;
import kr.co.mergepoint.mergeclient.scene.register.presenter.callback.RegisterCallback;
import kr.co.mergepoint.mergeclient.scene.register.view.RegisterView;

import static kr.co.mergepoint.mergeclient.application.common.Properties.ELECT_TERMS_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ELECT_TERMS_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PERMISSIONS_REQUEST_SMS;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PRIVACY_POLICY_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PRIVACY_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.TERMS_USE_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UP_DOWN;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USER_GUIDE_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WEB_URL;

/**
 * Created by jgson on 2017. 6. 5..
 */

public class RegisterPresenter
        extends BasePresenter<RegisterView, RegisterModel, RegisterCallback>
        implements View.OnClickListener, PostScriptListener, CompoundButton.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener, View.OnFocusChangeListener {

    private SmsMessageReceiver receiver;

    public RegisterPresenter(RegisterView baseView, RegisterModel baseModel, RegisterCallback callback, SmsMessageReceiver receiver) {
        super(baseView, baseModel, callback);
        this.receiver = receiver;
    }

    @Override
    protected void setBindingProperty() {
        baseView.binding.setTitle(baseView.getString(R.string.registe_title));
        baseView.binding.setOnClick(this);
        baseView.binding.setAgreeListener(this);
        baseView.binding.setOnFocusChange(this);
    }

    public void onCreate() {
        baseView.setupSmsReceiver(receiver);
        baseView.setMemberInfo();
    }

    public void onDestroy() {
        baseView.removeSmsReceiver(receiver);
    }

    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.send_server:
                baseView.removeAllFocus();
                if (baseView.checkField())
                    baseModel.registerUser(baseView.getMemberInfo(), baseCallback.registerCallback());
                break;
            case R.id.phone_subscribed:
                if (baseView.validPhoneNumber())
                    requestSmsReceiver();
                break;
            /*case R.id.register_postcode:
                baseView.showSearchPost(this);
                break;
            */case R.id.user_birth:
                baseView.showCalendar(this);
                break;
            case R.id.ad_agree:
                baseView.showMarketingAgreeAlert(this, this);
                break;
            case R.id.close:
                baseView.closeMarketingAlert();
                break;
            case R.id.terms_of_use:
                baseView.openFragment(UP_DOWN, R.id.register_root, SimpleWebFragment.newInstance(createWebBundle(USER_GUIDE_URL), baseView.getString(R.string.terms_of_use)), TERMS_USE_TAG);
                break;
            case R.id.privacy_policy:
                baseView.openFragment(UP_DOWN, R.id.register_root, SimpleWebFragment.newInstance(createWebBundle(PRIVACY_URL), baseView.getString(R.string.use_of_personal_information)), PRIVACY_POLICY_TAG);
                break;
            /* 전자금융거래 이용약관 */
            case R.id.electronic_finance:
                baseView.openFragment(UP_DOWN, R.id.register_root, SimpleWebFragment.newInstance(createWebBundle(ELECT_TERMS_URL), baseView.getString(R.string.electronic_financial_terms)), ELECT_TERMS_TAG);
                break;
        }
    }

    private Bundle createWebBundle(String url) {
        Bundle bundle = new Bundle();
        bundle.putString(WEB_URL, url);
        return bundle;
    }

    private void requestSmsReceiver() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            /*마시멜로우 이상 권한체크*/
            int fineResult = ContextCompat.checkSelfPermission(baseView.activity, Manifest.permission.RECEIVE_SMS);

            if (fineResult != PackageManager.PERMISSION_GRANTED) {
                if (baseView.activity.shouldShowRequestPermissionRationale(Manifest.permission.RECEIVE_SMS)) {
                    baseModel.phoneConfirm(baseView.getPhoneNum(), baseCallback.sendPhoneAuthNumCallback());
                } else {
                    new MergeDialog.Builder(baseView.activity)
                            .setContent(baseView.getString(R.string.sms_permission_text))
                            .setConfirmClick(() -> baseView.requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS}, PERMISSIONS_REQUEST_SMS))
                            .setCancelClick(() -> baseModel.phoneConfirm(baseView.getPhoneNum(), baseCallback.sendPhoneAuthNumCallback())).build().show();
                }
            } else {
                baseModel.phoneConfirm(baseView.getPhoneNum(), baseCallback.sendPhoneAuthNumCallback());
            }
        } else {
            /*마시멜로우 아랫버전으로 권한체크 불필요*/
            baseModel.phoneConfirm(baseView.getPhoneNum(), baseCallback.sendPhoneAuthNumCallback());
        }
    }

    @Override
    public void setAddress(String add01, String add02, String add03) {
        baseView.setAddress(add01, add02, add03);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        String viewTag = (String) v.getTag();
        Resources resources = baseView.activity.getResources();
        if (!hasFocus) {
            if (Objects.equals(viewTag, resources.getString(R.string.certification_number))) {
                baseModel.phoneSubscripedConfirm(baseView.getPhoneNum(), baseView.getPhoneSubscribedNumber(), baseCallback.phoneConfirmCallback());
            } else if (Objects.equals(viewTag, resources.getString(R.string.id))) {
                /*id 확인*/
                if (MergeApplication.getMergeAppComponent().getUtility().checkEmailFormat(baseView.getID())) {
                    baseModel.subscripedId(baseView.getID(), baseCallback.idConfirmCallback());
                } else {
                    baseView.successId(false);
                    new MergeDialog.Builder(baseView.activity).setContent(R.string.register_email_format).setCancelBtn(false).build().show();
                }
            } else if (Objects.equals(viewTag, resources.getString(R.string.pw_confirm))) {
                /*비밀번호 확인*/
                baseView.successPw();
            }
        }
    }

    public void onReceiveSms(String certificationNum) {
        baseView.setCertificationNum(certificationNum);
        baseModel.phoneSubscripedConfirm(baseView.getPhoneNum(), certificationNum, baseCallback.phoneConfirmCallback());
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_SMS)
            baseModel.phoneConfirm(baseView.getPhoneNum(), baseCallback.sendPhoneAuthNumCallback());
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        baseView.setBirth(year, month, dayOfMonth);
    }

    /*수신 동의 및 약관동의 리스너*/
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.marketing_check:
                baseView.setMarketing(isChecked);
                break;
            case R.id.sms_check:
                baseView.setSms(isChecked);
                break;
            case R.id.mail_check:
                baseView.setMail(isChecked);
                break;
            default:
                break;
        }
    }
}
