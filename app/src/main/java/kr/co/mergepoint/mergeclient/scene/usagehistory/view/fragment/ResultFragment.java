package kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.PayCancelResultBinding;

/**
 * Created by 1017sjg on 2017. 7. 28..
 */

public class ResultFragment extends BaseFragment {

    private String title;
    private String contentString;

    public static ResultFragment newInstance(String title, String contentString) {
        Bundle args = new Bundle();
        ResultFragment fragment = new ResultFragment();
        fragment.contentString = contentString;
        fragment.title = title;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        PayCancelResultBinding resultBinding = DataBindingUtil.inflate(inflater, R.layout.pay_cancel_result, container, false);
        resultBinding.setTitle(title);
        resultBinding.setOnClick(this);
        resultBinding.setCancelString(contentString);
        resultBinding.setIsvisible(contentString.contains("취소") ? false : true);
        return resultBinding.getRoot();
    }
}
