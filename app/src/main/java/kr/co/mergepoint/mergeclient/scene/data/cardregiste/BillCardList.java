package kr.co.mergepoint.mergeclient.scene.data.cardregiste;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public class BillCardList {
    @SerializedName("billCardList")
    private ArrayList<CardInfo> billCardList;

    public ArrayList<CardInfo> getBillCardList() {
        return billCardList;
    }

    public void setBillCardList(ArrayList<CardInfo> billCardList) {
        this.billCardList = billCardList;
    }
}
