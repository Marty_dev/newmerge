package kr.co.mergepoint.mergeclient.api;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.main.MergesPick;
import kr.co.mergepoint.mergeclient.scene.data.main.Notice;
import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by 1017sjg on 2017. 9. 14..
 */

public interface NoticeApi {

    @POST("notice/list")
    Call<ResponseObject<ArrayList<Notice>>> requestNoticeList();
}
