package kr.co.mergepoint.mergeclient.scene.data.shop.review;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by jgson on 2018. 1. 3..
 */

public class MergeReview {
    @SerializedName("currentPage")
    public int currentPage;

    @SerializedName("totalPage")
    public int totalPage;

    @SerializedName("totalShop")
    public int totalShop;

    @SerializedName("listSize")
    public int listSize;

    @SerializedName("reviewList")
    public ArrayList<Review> reviewList;
}
