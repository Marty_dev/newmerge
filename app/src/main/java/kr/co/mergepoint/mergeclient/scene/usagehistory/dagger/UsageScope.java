package kr.co.mergepoint.mergeclient.scene.usagehistory.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by jgson on 2017. 5. 31..
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
@interface UsageScope {}
