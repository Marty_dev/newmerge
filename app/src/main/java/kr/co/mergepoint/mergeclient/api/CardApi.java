package kr.co.mergepoint.mergeclient.api;

import kr.co.mergepoint.mergeclient.scene.data.cardregiste.BillCardList;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public interface CardApi {
    @POST("payment/billList")
    Call<BillCardList> requestCardList();

    @GET("payment/billDelete/{oid}")
    Call<BillCardList> deleteCard(@Path("oid") int oid);

    @POST("payment/billRepresentation/{oid}")
    Call<BillCardList> representationCard(@Path("oid") int oid);
}
