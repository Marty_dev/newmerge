package kr.co.mergepoint.mergeclient.scene.data.newcoupon;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * User: Marty
 * Date: 2018-09-27
 * Time: 오전 10:06
 * Description:
 */
public class CouponUse {

    @SerializedName("oid")
    private int oid;
    @SerializedName("shopName")
    private String shopName;
    @SerializedName("usePrice")
    private int usePrice;
    @SerializedName("useDateTime")
    private ArrayList<Integer> useDateTime;

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public int getUsePrice() {
        return usePrice;
    }

    public void setUsePrice(int usePrice) {
        this.usePrice = usePrice;
    }

    public ArrayList<Integer> getUseDateTime() {
        return useDateTime;
    }

    public void setUseDateTime(ArrayList<Integer> useDateTime) {
        this.useDateTime = useDateTime;
    }
}
