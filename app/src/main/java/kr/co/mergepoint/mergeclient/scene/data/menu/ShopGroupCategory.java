package kr.co.mergepoint.mergeclient.scene.data.menu;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder.Menu;

/**
 * Created by 1017sjg on 2017. 6. 23..
 */

public class ShopGroupCategory {
    @SerializedName("categories")
    private ArrayList<ShopGroupInfo> categories;

    @SerializedName("tableMenuList")
    private ArrayList<ShopTableMenuInfo> tableMenuList;

    public ArrayList<Menu> getGroupMenus() {
        ArrayList<Menu> arrayList = new ArrayList<>();

        if (tableMenuList != null) {
            for (ShopTableMenuInfo menuInfo : tableMenuList) {
                arrayList.add(menuInfo.getGroupMenu());
            }
        }

        for (ShopGroupInfo groupInfo: categories) {
            arrayList.add(groupInfo.getGroupMenu());
        }

        return arrayList;
    }
}
