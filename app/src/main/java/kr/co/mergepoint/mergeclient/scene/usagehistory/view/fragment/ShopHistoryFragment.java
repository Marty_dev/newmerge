package kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.HistoryApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.UsageHistoryShopBinding;
import kr.co.mergepoint.mergeclient.scene.data.history.ShopHistoryPayment;
import kr.co.mergepoint.mergeclient.scene.data.history.ShopPaidMenu;
import kr.co.mergepoint.mergeclient.scene.data.history.UseMenu;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter.MenuHistoryAdapter;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter.ShopHistoryAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

/**
 * Created by 1017sjg on 2017. 7. 28..
 */

public class ShopHistoryFragment extends BaseFragment implements MenuHistoryAdapter.OnUseMenu {

    private int shopOid;
    private UsageHistoryShopBinding shopBinding;
    private SparseIntArray useMenu;
    private int consummerRef;

    public ArrayList<UseMenu> getUseMenu() {
        ArrayList<UseMenu> useMenuArrayList = new ArrayList<>();
        for(int i = 0; i < useMenu.size(); i++) {
            int menuOid = useMenu.keyAt(i);
            int count = useMenu.get(menuOid);
            useMenuArrayList.add(new UseMenu(menuOid, count));
        }
        return useMenuArrayList;
    }

    public static ShopHistoryFragment newInstance(int shopOid) {
        Bundle args = new Bundle();
        ShopHistoryFragment fragment = new ShopHistoryFragment();
        fragment.shopOid = shopOid;
        fragment.setArguments(args);
        return fragment;
    }
    public static ShopHistoryFragment newInstance(int shopOid,int consummerRef) {
        Bundle args = new Bundle();
        ShopHistoryFragment fragment = new ShopHistoryFragment();
        fragment.shopOid = shopOid;
        fragment.setArguments(args);
        fragment.consummerRef = consummerRef;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        shopBinding = DataBindingUtil.inflate(inflater, R.layout.usage_history_shop, container, false);
        shopBinding.setTitle(getString(R.string.pay_history_title));
        shopBinding.setClick(this);
        return shopBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        MergeApplication.getMergeApplication().showLoading(rootActivity);
        getShopDetailHistory();
    }

    @Override
    public void addUseMenu(boolean isCheck, int oid, int count) {
        int value = useMenu.get(oid, NO_VALUE);

        if (isCheck) {
            if (value == NO_VALUE) {
                useMenu.append(oid, count);
            } else {
                useMenu.put(oid, count);
            }
        } else {
            if (value != NO_VALUE)
                useMenu.removeAt(useMenu.indexOfKey(oid));
        }
    }

    public void getShopDetailHistory() {
        HistoryApi api = MergeApplication.getMergeAppComponent().getRetrofit().create(HistoryApi.class);
        Call<ShopHistoryPayment> call = api.requestShopDetail(consummerRef,shopOid);
        call.enqueue(getOrderHistoryCallback());
    }

    private Callback<ShopHistoryPayment> getOrderHistoryCallback() {
        return new Callback<ShopHistoryPayment>() {
            @Override
            public void onResponse(@Nullable Call<ShopHistoryPayment> call, @Nullable Response<ShopHistoryPayment> response) {
                if (response != null && response.isSuccessful()) {
                    ShopHistoryPayment historyPayment = response.body();
                    if (historyPayment != null) {
                        useMenu = new SparseIntArray();
                        shopBinding.setShopName(historyPayment.shopName);
                        if (historyPayment.paidMenu.size() > 0) {
                            shopBinding.setShopAdapter(new ShopHistoryAdapter(historyPayment.paidMenu, ShopHistoryFragment.this));
                            for (ShopPaidMenu paidMenu : historyPayment.paidMenu) {
                                for (ShopMenuDetail detail : paidMenu.payMenu)
                                    useMenu.put(detail.oid, detail.count - detail.usedCount);
                            }
                        }
                    }
                }
                delayHideLoading();
            }

            @Override
            public void onFailure(@Nullable Call<ShopHistoryPayment> call, @Nullable Throwable t) {
                showAlert(R.string.retry);
                delayHideLoading();
            }
        };
    }
}
