package kr.co.mergepoint.mergeclient.scene.register.view.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.CustomChromeClient;
import kr.co.mergepoint.mergeclient.application.common.SingleWebFragment;
import kr.co.mergepoint.mergeclient.application.common.script.PostScriptInterface;

import static kr.co.mergepoint.mergeclient.application.common.Properties.WEB_JAVASCRIPT;

/**
 * Created by 1017sjg on 2017. 6. 16..
 */

public class PostcodeView extends SingleWebFragment<PostScriptInterface> {

    private PostScriptInterface scriptInterface;

    public static PostcodeView newInstance(PostScriptInterface scriptInterface, String postcodeUrl) {
        Bundle args = new Bundle();
        args.putString("url", postcodeUrl);
        PostcodeView fragment = new PostcodeView();
        fragment.scriptInterface = scriptInterface;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected WebChromeClient setWebChromeClient(ContentLoadingProgressBar loadProgressBar) {
        CustomChromeClient chromeClient = new CustomChromeClient() {
            @SuppressLint("SetJavaScriptEnabled")
            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                WebView post = new WebView(view.getContext());
                post.getSettings().setJavaScriptEnabled(true);
                post.setWebViewClient(new WebViewClient());
                post.setWebChromeClient(new WebChromeClient());
                post.addJavascriptInterface(setJavascript(), WEB_JAVASCRIPT);
                post.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

                view.addView(post);

                WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
                transport.setWebView(post);
                resultMsg.sendToTarget();
                return true;
            }
        };
        chromeClient.setContentLoadingProgressBar(loadProgressBar);
        return chromeClient;
    }

    @Override
    protected WebViewClient setWebViewClient() {
        return new WebViewClient();
    }

    @Override
    protected PostScriptInterface setJavascript() {
        return scriptInterface;
    }

    @Override
    protected String setPageTitle() {
        return getString(R.string.postcode_title);
    }
}
