package kr.co.mergepoint.mergeclient.scene.data.payment;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class CrowdMember {
    @SerializedName("oid")
    public int oid;

    @SerializedName("name")
    public String name;

    @SerializedName("usePoint")
    public int usePoint;

    @SerializedName("level")
    public String level;

    @SerializedName("depart")
    public String depart;

    @SerializedName("favorite")
    public boolean favorite;
}
