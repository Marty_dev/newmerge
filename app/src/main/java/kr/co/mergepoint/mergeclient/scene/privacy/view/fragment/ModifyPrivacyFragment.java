package kr.co.mergepoint.mergeclient.scene.privacy.view.fragment;

import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.ModifyPrivacyBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

/**
 * Created by 1017sjg on 2017. 8. 3..
 */

public class ModifyPrivacyFragment extends BaseFragment implements View.OnFocusChangeListener {

    private Member member;
    private ModifyPrivacyBinding modifyPrivacyBinding;
    private boolean checkPw;

    public boolean isCheckPw() {
        return checkPw;
    }

    public static ModifyPrivacyFragment newInstance(Member member) {
        Bundle args = new Bundle();
        ModifyPrivacyFragment fragment = new ModifyPrivacyFragment();
        fragment.member = member;
        fragment.checkPw = true;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        modifyPrivacyBinding = DataBindingUtil.inflate(inflater, R.layout.modify_privacy, container, false);
        modifyPrivacyBinding.setTitle(getString(R.string.privacy_title));
        modifyPrivacyBinding.setOnClick(this);
        modifyPrivacyBinding.setMember(member);
        modifyPrivacyBinding.setOnFocusChange(this);
        return modifyPrivacyBinding.getRoot();
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        String viewTag = (String) view.getTag();
        Resources resources = getActivity().getResources();
        if (!hasFocus && Objects.equals(viewTag, resources.getString(R.string.pw_confirm))) {
            checkPw = modifyPrivacyBinding.registerPw.getText().toString().equals(modifyPrivacyBinding.registerPwConfirm.getText().toString());
            modifyPrivacyBinding.pwSubscriped.setImageResource(checkPw ? R.drawable.icon_registe_yes : R.drawable.icon_registe_no);
        }
    }

    public void setModifyMember() {
        String pw = modifyPrivacyBinding.registerPwConfirm.getText().toString();
        int gender = modifyPrivacyBinding.genderRadioGroup.getCheckedRadioButtonId();

        /* 나의 정보 */
        member.setName(modifyPrivacyBinding.registerName.getText().toString().trim());
        member.setGender(gender != NO_VALUE ? gender == R.id.male ? 1 : 2 : NO_VALUE);
        member.setPostcode(modifyPrivacyBinding.postcode.getText().toString().trim());
        member.setAddress01(modifyPrivacyBinding.registerAddress.getText().toString().trim());
        member.setAddress02(modifyPrivacyBinding.registerDetailedAddress.getText().toString().trim());
        member.setEmail(modifyPrivacyBinding.additionRegisterEmail.getText().toString().trim());

        /* pw 미입력시 변경안함 */
        if (!pw.trim().isEmpty() && pw.length() > 0)
            member.setPassword(pw);
    }
}
