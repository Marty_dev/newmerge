package kr.co.mergepoint.mergeclient.scene.newcoupon.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.NewCouponApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.DateModel;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponDetail;
import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponV1;
import kr.co.mergepoint.mergeclient.scene.newcoupon.BarcodeGen;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Url;

/**
 * User: Marty
 * Date: 2018-09-12
 * Time: 오전 9:47
 * Description:
 */
public class CouponDetailActivity extends BaseActivity {
    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.coupon_title)
    TextView couponTitle;
    @BindView(R.id.back)
    ConstraintLayout back;
    @BindView(R.id.basic_toolbar)
    Toolbar basicToolbar;
    @BindView(R.id.coupon_shopcard_img)
    ImageView couponShopcardImg;
    @BindView(R.id.shop_name_tv)
    TextView shopNameTv;
    @BindView(R.id.coupon_name_tv)
    TextView couponNameTv;
    @BindView(R.id.baseline1)
    TextView baseline1;
    @BindView(R.id.coupon_price_text)
    TextView couponPriceText;
    @BindView(R.id.coupon_leftprice_tv)
    TextView couponLeftpriceTv;
    @BindView(R.id.baseline2)
    TextView baseline2;
    @BindView(R.id.coupon_barcode_img)
    ImageView couponBarcodeImg;
    @BindView(R.id.barcode_rl)
    RelativeLayout barcodeRl;
    @BindView(R.id.baseline3)
    TextView baseline3;
    @BindView(R.id.coupon_date_text)
    TextView couponDateText;
    @BindView(R.id.coupon_date_tv)
    TextView couponDateTv;
    @BindView(R.id.baseline4)
    TextView baseline4;
    @BindView(R.id.coupon_state_text)
    TextView couponStateText;
    @BindView(R.id.coupon_state_tv)
    TextView couponStateTv;
    @BindView(R.id.baseline5)
    TextView baseline5;
    @BindView(R.id.coupon_detailinfo_rl)
    RelativeLayout couponDetailinfoRl;
    @BindView(R.id.coupon_top_rl)
    RelativeLayout couponTopRl;
    @BindView(R.id.baseline6)
    TextView baseline6;
    @BindView(R.id.coupon_bottom_rl)
    LinearLayout couponBottomRl;
    @BindView(R.id.coupon_delete_btn)
    TextView couponDeleteBtn;
    @BindView(R.id.coupon_noneplace)
    TextView couponNoneplace;
    @BindView(R.id.coupon_important)
    TextView couponImportant;
    @BindView(R.id.coupon_expdate)
    TextView couponExpdate;
    @BindView(R.id.coupon_howtouse)
    TextView couponHowtouse;
    @BindView(R.id.coupon_refund)
    TextView couponRefund;
    @BindView(R.id.barcode_text)
    TextView barcodeText;
    @BindView(R.id.coupon_bigbarcode)
    ImageView couponBigbarcode;
    @BindView(R.id.coupon_uselist)
    TextView couponUselist;

    MergeApplication mApp = MergeApplication.getMergeApplication();

    BaseActivity mCon = this;
    private CouponDetail coupon;
    private NewCouponApi API;
    private int couponRef;
    DateModel date;
    // 1  15일 이상
    // 2  15일 이하
    // 3  00일
    private int couponState;

    @Override
    protected void onActivityClick(View view) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newcoupon_detail);
        ButterKnife.bind(this);
        API = MergeApplication.getMergeAppComponent().getRetrofit().create(NewCouponApi.class);
        couponRef = getIntent().getIntExtra("couponRef", -1);
        date = new DateModel();
        requestData();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().getIntExtra("couponRef", -1) == -1)
            return;
        mApp.isBarcodeforMoney = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mApp.isBarcodeforMoney = false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String action = intent.getStringExtra("action");
        if (action != null){
            if (action.equals("19")){
                requestData();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_arrow:
                finish();
                break;
            case R.id.coupon_uselist:
                Intent intent = new Intent(this,CouponuseListActivity.class);
                intent.putExtra("oid",couponRef);
                startActivity(intent);
                break;
            case R.id.coupon_barcode_img:
            case R.id.coupon_bigbarcode:
                Intent inte = new Intent(this,Barcode_detailActivity.class);
                inte.putExtra("barcode",coupon.getBarCode());
                inte.putExtra("title",coupon.getCouponName());
                inte.putExtra("couponRef",coupon.getOid());
                inte.putExtra("price",coupon.getRemainPrice());
                startActivity(inte);
                break;
            case R.id.coupon_delete_btn:
                switch (couponState){
                    case 1:
                        barcodedeletecall(coupon);
                        break;
                    case 2:
                        // 유효기간 연장
                        MergeDialog.Builder builder = new MergeDialog.Builder(mCon);
                        builder.setContent(mCon.getResources().getString(R.string.barcode_expdate_warning,coupon.getCouponName()))
                                .setConfirmString("통화")
                                .setConfirmClick(()->{
                                    String uri = "tel:" + coupon.getProducerPhone().trim() ;
                                    Intent phone = new Intent(Intent.ACTION_DIAL);
                                    phone.setData(Uri.parse(uri));
                                    mCon.startActivity(phone);
                                })
                                .build().show();
                        break;
                    case 3:
                        // 환불 요청
                        MergeDialog.Builder builder2 = new MergeDialog.Builder(mCon);
                        builder2.setContent(mCon.getResources().getString(R.string.use_guide))
                                .setConfirmString("통화")
                                .setConfirmClick(()->{
                                    String uri = "tel:" + coupon.getProducerPhone().trim() ;
                                    Intent phone = new Intent(Intent.ACTION_DIAL);
                                    phone.setData(Uri.parse(uri));
                                    mCon.startActivity(phone);
                                })
                                .build().show();
                        break;
                }
                
                break;

        }
    }

    public void initData() {

        DateModel dday = new DateModel();
        int days = dday.getCalculateDay(coupon.getExpDate());
        try {
            couponDeleteBtn.setOnClickListener(this);
            couponDeleteBtn.setVisibility(View.GONE);

            if (coupon.getIssueState() == 4 ) {
                couponState = 1;
                couponDeleteBtn.setText("삭제");
                couponDeleteBtn.setVisibility(View.VISIBLE);
            }else if (days <=15 && days > 0){
                couponState = 2;
                couponDeleteBtn.setText("유효기간 연장");
                couponDeleteBtn.setVisibility(View.VISIBLE);
                couponDeleteBtn.setBackgroundColor(Color.parseColor("#d24b56"));
            }else if (days<=0 && coupon.getIssueState() == 7){
                couponState = 3;
                couponDeleteBtn.setText("환불요청");
                couponDeleteBtn.setVisibility(View.VISIBLE);
            }
            // TODO: 2018-09-27  delete btn side job
            backArrow.setOnClickListener(this);
            couponUselist.setOnClickListener(this);
            couponBigbarcode.setOnClickListener(this);
            couponBarcodeImg.setOnClickListener(this);
            couponTitle.setText(coupon.getCouponName());
            shopNameTv.setText(coupon.getCouponName());
            couponNameTv.setText(coupon.getCouponDescription());
            couponLeftpriceTv.setText(MoneyForm(coupon.getRemainPrice()) + "원");
            couponDateTv.setText(coupon.getExpDate() == null ? "Null" : date.getStringCalendarDate(date.getCalendarDate(coupon.getExpDate())));
            couponNoneplace.setText(coupon.getUnUsableShop());
            couponImportant.setText(coupon.getWarning());
            couponExpdate.setText(coupon.getValidityTerm());
            couponHowtouse.setText(coupon.getUseCondition());
            couponRefund.setText(coupon.getRefundRequest());

            String couponState = "";
            switch (coupon.getIssueState()){
                case 2 :
                    couponState = "사용안함";
                    break;
                case 3:
                    couponState = "사용중";
                    break;
                case 4:
                    couponState = "사용완료";
                    break;
                case 5:
                    couponState = "중도환급 완료";
                    break;
                case 7:
                    couponState = "유효기간 만료";
                    break;


            }

            couponStateTv.setText(couponState);

            Picasso.with(this).load(MergeApplication.getMergeApplication().getImageUrlFiltered(coupon.getDetailImage())).into(couponShopcardImg);
            BarcodeGen gen = new BarcodeGen();
            float wpx, hpx;
            wpx = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 220,
                    getResources().getDisplayMetrics()
            );
            hpx = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 70,
                    getResources().getDisplayMetrics()
            );
            try {
                Bitmap bitmap = gen.encodeAsBitmap(coupon.getBarCode(), BarcodeFormat.CODE_128, (int) wpx, (int) hpx);
                couponBarcodeImg.setImageBitmap(bitmap);
                barcodeText.setText(coupon.getBarCode());
            } catch (WriterException e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            MDEBUG.debug("Exception" + e.toString());
        }
    }

    public void requestData() {
        Context mCon = this;
        MergeApplication.getMergeApplication().showLoading((BaseActivity) mCon);
        API.getBarcodedetail(couponRef).enqueue(new Callback<ResponseObject<CouponDetail>>() {
            @Override
            public void onResponse(Call<ResponseObject<CouponDetail>> call, Response<ResponseObject<CouponDetail>> response) {
                if (!response.body().isFailed()) {
                    coupon = response.body().getObject();
                    initData();
                } else
                    new MergeDialog.Builder(mCon).setCancelBtn(false).setContent(mCon.getResources().getString(R.string.retry)).build().show();
                MergeApplication.getMergeApplication().hideLoading((BaseActivity) mCon);

            }

            @Override
            public void onFailure(Call<ResponseObject<CouponDetail>> call, Throwable t) {
                new MergeDialog.Builder(mCon).setCancelBtn(false).setContent(mCon.getResources().getString(R.string.retry)).build().show();
                MergeApplication.getMergeApplication().hideLoading((BaseActivity) mCon);

            }
        });
    }

    void barcodedeletecall(CouponDetail item){
            MergeDialog.Builder builder = new MergeDialog.Builder(mCon);
            builder.setCancelBtn(true);
            builder.setContent(R.string.barcode_delete_warning);
            builder.setConfirmClick(()->{
                MergeApplication.getMergeApplication().showLoading(mCon);
                API.deleteCoupon(item.getCouponCategory(),item.getOid()).enqueue(getBarcodeDeleteCallback());
            });
            builder.build().show();


    }

    retrofit2.Callback<ResponseObject<ArrayList<CouponV1>>> getBarcodeDeleteCallback (){
        return new retrofit2.Callback<ResponseObject<ArrayList<CouponV1>>>() {
            @Override
            public void onResponse(Call<ResponseObject<ArrayList<CouponV1>>> call, Response<ResponseObject<ArrayList<CouponV1>>> response) {
                if (!response.body().isFailed()) {
                    finish();

                } else {
                    new MergeDialog.Builder(mCon).setCancelBtn(false).setContent(mCon.getResources().getString(R.string.retry)).build().show();
                }
                MDEBUG.debug("Error?");
                MergeApplication.getMergeApplication().hideLoading(mCon);

            }

            @Override
            public void onFailure(Call<ResponseObject<ArrayList<CouponV1>>> call, Throwable t) {
                MDEBUG.debug("Error!" + t.toString());
                new MergeDialog.Builder(mCon).setCancelBtn(false).setContent(mCon.getResources().getString(R.string.retry)).build().show();
                MergeApplication.getMergeApplication().hideLoading(mCon);

            }
        };
    }
}
