package kr.co.mergepoint.mergeclient.scene.data.shoplist;

import android.location.Location;
import android.text.TextUtils;

import net.daum.mf.map.api.MapPoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.scene.data.splash.CategoryInfo;
import kr.co.mergepoint.mergeclient.scene.data.splash.StreetInfo;

import static kr.co.mergepoint.mergeclient.application.common.Properties.ASC;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_LIST_SIZE;

/**
 * Created by 1017sjg on 2017. 9. 7..
 */

public class Filter {

    private Map<String, Object> currentFilter;
    private Location location;
    private String direction;
    private String category;
    private String searchString;
    private ArrayList<CategoryInfo> categoryInfos;
    private StreetInfo streetInfo;

    /*SORT*/
    private String price;
    private boolean like;
    private boolean favorite;

    /*DISTANCE*/
    private int distance;

    /*THEME*/
    private String theme;

    /*PRICE RANGE*/
    private ArrayList<String> priceRange;
    private ArrayList<String> tempPriceRange;

    /*CONVENIENCE*/
    private Map<String, Boolean> convenience;

    public Filter(@Properties.ShopVisibleType int type) {
        if (type == LIST) {
            setDirection(ASC);
            distance = 0;
        }

        convenience = new HashMap<>();
        priceRange = new ArrayList<>();
        tempPriceRange = new ArrayList<>();

        for (String str: MergeApplication.getMergeAppComponent().getUtility().getResuorces().getStringArray(R.array.convenience))
            convenience.put(str, false);
    }

    public void resetFilterSearch() {
        price = null;
        like = false;
        favorite = false;
        distance = 0;
        theme = null;
        priceRange = new ArrayList<>();
        tempPriceRange = new ArrayList<>();
        convenience = new HashMap<>();
    }

    public ArrayList<String> getPriceRange() {
        priceRange.clear();
        priceRange.addAll(tempPriceRange);
        tempPriceRange.clear();
        return priceRange;
    }

    /* 내부 필터 */
    private Map<String, Object> getCommonFilter(int pageNum) {
        Map<String, Object> filterMap = new HashMap<>();

        /* 공통 필수 */
        filterMap.put("pageNum", pageNum);
        filterMap.put("pageSize", SHOP_LIST_SIZE);

        /* 공통 선택 */
        if (location != null) {
            filterMap.put("latitude", location.getLatitude());
            filterMap.put("longitude", location.getLongitude());
        }

        filterMap.put("direction", getDirection());

        return filterMap;
    }

    private void getStreetCategory(Map<String, Object> filterMap) {
        if (streetInfo != null)
            filterMap.put("street", streetInfo.getCode());
        if (categoryInfos != null && category != null) {
            String categoryCode = getCategoryCodeRef(category);
            if (!categoryCode.isEmpty())
                filterMap.put("category", categoryCode);
        }
    }

    /* 외부 제공 필터 */
    public Map<String, Object> getFilterSearch() {
        Map<String, Object> filterMap = getCommonFilter(0);

        /* 필터 검색 */
        if (price != null || like || favorite) {
            if (price != null) {
                filterMap.put("price", price);
            } else if (like) {
                filterMap.put("like", true);
            } else {
                filterMap.put("favorite", true);
            }
        }
        if (theme != null)
            filterMap.put("theme", theme);
        if (priceRange.size() > 0)
            filterMap.put("priceRange", TextUtils.join(", ", priceRange));

        for (String key : convenience.keySet()) {
            boolean value = convenience.get(key);
            if (value)
                filterMap.put(key, true);
        }

        filterMap.put("distance", distance);

        /* 리스트 탭 및 카테고리 */
        getStreetCategory(filterMap);

        currentFilter = filterMap;
        return filterMap;
    }

    public Map<String, Object> getSentenceSearch() {
        Map<String, Object> filterMap = getCommonFilter(0);
        /* 리스트 탭 및 카테고리 */
        getStreetCategory(filterMap);

        if (filterMap.containsKey("category"))
            filterMap.remove("category");
        /* 검색어 검색 */
        if (searchString != null && !searchString.isEmpty())
            filterMap.put("searchString", searchString);

        currentFilter = filterMap;
        return filterMap;
    }

    public Map<String, Object> getThemeSearch() {
        Map<String, Object> filterMap = getCommonFilter(0);
        /* 리스트 탭 및 카테고리 */
        /* 테마 검색 */
        if (theme != null)
            filterMap.put("theme", theme);

        currentFilter = filterMap;
        return filterMap;
    }

    public Map<String, Object> getMainSearch() {
        Map<String, Object> filterMap = getCommonFilter(0);
        /* 리스트 탭 및 카테고리 */
        /* 점포 검색 */
        filterMap.put("searchString", searchString);

        currentFilter = filterMap;
        return filterMap;
    }

    public Map<String, Object> getListFilter() {
        Map<String, Object> filterMap = getCommonFilter(0);
        /* 리스트 탭 및 카테고리 */
        getStreetCategory(filterMap);

        currentFilter = filterMap;
        return filterMap;
    }

    public Map<String, Object> getMyZoneListFilter() {
        Map<String, Object> filterMap = getCommonFilter(0);
        /* 리스트 탭 및 카테고리 */
        filterMap.put("distance", distance);
        filterMap.put("street", streetInfo.getCode());
        currentFilter = filterMap;
        return filterMap;
    }

    public Map<String, Object> getFavoriteFilter() {
        Map<String, Object> filterMap = new HashMap<>();

        /* 공통 선택 */
        if (location != null) {
            filterMap.put("latitude", location.getLatitude());
            filterMap.put("longitude", location.getLongitude());
        }

        currentFilter = filterMap;
        return filterMap;
    }

    public Map<String, Object> getMapFilter(MapPoint.GeoCoordinate tr, MapPoint.GeoCoordinate bl) {
        Map<String, Object> filterMap = new HashMap<>();

        if (tr != null && bl != null) {
            filterMap.put("ldlat", bl.latitude);
            filterMap.put("ldlong", bl.longitude);
            filterMap.put("rulat", tr.latitude);
            filterMap.put("rulong", tr.longitude);
        }

        getStreetCategory(filterMap);
        if (searchString != null && !searchString.isEmpty())
            filterMap.put("searchString", searchString);

        if (theme != null)
            filterMap.put("theme", theme);

        if (priceRange.size() > 0)
            filterMap.put("priceRange", TextUtils.join(", ", priceRange));

        for (String key : convenience.keySet()) {
            boolean value = convenience.get(key);
            if (value)
                filterMap.put(key, true);
        }

        currentFilter = filterMap;
        return filterMap;
    }

    public Map<String, Object> getLoadFilter(int pageNum) {
        Map<String, Object> filterMap = currentFilter;
        filterMap.put("pageNum", pageNum);

        return filterMap;
    }

    private String getCategoryCodeRef(String category) {
        for (CategoryInfo categoryInfo: categoryInfos) {
            if (categoryInfo.name.equals(category))
                return categoryInfo.code;
        }
        return "";
    }

    /* SETTER, GETTER */
    public Map<String, Object> getCurrentFilter() {
        return currentFilter;
    }

    public void setCurrentFilter(Map<String, Object> currentFilter) {
        this.currentFilter = currentFilter;
    }

    public Location getLocation() {
        return location;
    }

    public String getCategory() {
        return category;
    }

    public String getSearchString() {
        return searchString;
    }

    public ArrayList<CategoryInfo> getCategoryInfos() {
        return categoryInfos;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setPriceRange(ArrayList<String> priceRange) {
        this.priceRange = priceRange;
    }

    public ArrayList<String> getTempPriceRange() {
        return tempPriceRange;
    }

    public void setTempPriceRange(ArrayList<String> tempPriceRange) {
        this.tempPriceRange = tempPriceRange;
    }

    public Map<String, Boolean> getConvenience() {
        return convenience;
    }

    public void setConvenience(Map<String, Boolean> convenience) {
        this.convenience = convenience;
    }

    @Properties.Direction private String getDirection() {
        return direction;
    }

    public void setDirection(@Properties.Direction String direction) {
        this.direction = direction;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setStreetInfo(StreetInfo streetInfo) {
        this.streetInfo = streetInfo;
    }

    public StreetInfo getStreetInfo() {
        return streetInfo;
    }

    public void setCategoryInfos(ArrayList<CategoryInfo> categoryInfos) {
        this.categoryInfos = categoryInfos;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
