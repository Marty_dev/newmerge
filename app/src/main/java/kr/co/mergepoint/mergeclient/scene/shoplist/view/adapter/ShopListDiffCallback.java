package kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;

/**
 * Created by 1017sjg on 2017. 7. 6..
 */

public class ShopListDiffCallback extends DiffUtil.Callback {

    private ArrayList<Shop> oldShop;
    private ArrayList<Shop> newShop;

    ShopListDiffCallback(ArrayList<Shop> oldShop, ArrayList<Shop> newShop) {
        this.oldShop = oldShop;
        this.newShop = newShop;
    }

    @Override
    public int getOldListSize() {
        return oldShop.size();
    }

    @Override
    public int getNewListSize() {
        return newShop.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldShop.get(oldItemPosition).oid == newShop.get(newItemPosition).oid;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldShop.get(oldItemPosition).equals(newShop.get(newItemPosition));
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
