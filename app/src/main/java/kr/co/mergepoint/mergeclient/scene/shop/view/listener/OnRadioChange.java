package kr.co.mergepoint.mergeclient.scene.shop.view.listener;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class OnRadioChange implements RadioPublisher{
    private ArrayList<RadioObserver> radioObservers;
    private int selectMenuOid;

    public OnRadioChange() {
        radioObservers = new ArrayList<>();
    }

    @Override
    public void add(RadioObserver observer) {
        radioObservers.add(observer);
    }

    @Override
    public void delete(RadioObserver observer) {
        radioObservers.remove(observer);
    }

    @Override
    public void notifyObserver() {
        for (RadioObserver observer : radioObservers)   // TODO: java.util.ConcurrentModificationException
            observer.onChangeRadio(selectMenuOid);
    }

    public void setSelectMenuOid(int selectMenuOid) {
        this.selectMenuOid = selectMenuOid;
        notifyObserver();
    }
}
