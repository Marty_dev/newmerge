package kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.application.common.CustomCountView;
import kr.co.mergepoint.mergeclient.databinding.UsageHistoryMenuListItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * Created by 1017sjg on 2017. 7. 29..
 */

public class MenuHistoryAdapter
        extends BasicListAdapter<BasicListHolder<UsageHistoryMenuListItemBinding, ShopMenuDetail>, ShopMenuDetail>
        implements CompoundButton.OnCheckedChangeListener, CustomCountView.OnChangeCountListener {

    private boolean isUse;
    private OnUseMenu onUseMenu;
    private UsageHistoryMenuListItemBinding menuListItemBinding;

    public interface OnUseMenu {
        void addUseMenu(boolean isCheck, int oid, int count);
    }

    public MenuHistoryAdapter(ArrayList<ShopMenuDetail> arrayList, OnUseMenu onUseMenu) {
        super(arrayList);
        this.isUse = onUseMenu != null;
        this.onUseMenu = onUseMenu;
    }

    @Override
    public BasicListHolder<UsageHistoryMenuListItemBinding, ShopMenuDetail> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.usage_history_menu_list_item, parent, false);
        menuListItemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<UsageHistoryMenuListItemBinding, ShopMenuDetail>(menuListItemBinding) {
            @Override
            public void setDataBindingWithData(ShopMenuDetail data) {
                MDEBUG.debug("data count" + data.count);
                getDataBinding().setMaxCount(data.count - data.usedCount);
                getDataBinding().setIsUse(isUse);
                getDataBinding().setMenuDetail(data);
                getDataBinding().setOptionAdapter(new OptionMenuHistoryAdapter(data.optionMenu));
                getDataBinding().setUsingCheck(MenuHistoryAdapter.this);
                getDataBinding().setCountChangeListener(MenuHistoryAdapter.this);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<UsageHistoryMenuListItemBinding, ShopMenuDetail> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
        holder.getDataBinding().seperator.setVisibility(getItemCount() == 1 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int oid = (int) buttonView.getTag();
        int count = menuListItemBinding.countLayout.countView.currentCount();
        updateDate(buttonView, oid, count);
    }

    @Override
    public void changeCount(CustomCountView view, boolean isPlus) {
        updateDate(view, view.optionSelectOid, view.afterCount);
    }

    private void updateDate(View view, int oid, int count) {
        int position = getPosition(view);
        ShopMenuDetail menuDetail = getObservableArrayList().get(position);
        UsageHistoryMenuListItemBinding itemBinding = (UsageHistoryMenuListItemBinding) getViewDataBinding(view);
        menuDetail.isCheck = itemBinding.usingCheckBtn.isChecked();
        menuDetail.setDisplayCount(count);

        if (onUseMenu != null)
            onUseMenu.addUseMenu(menuDetail.isCheck, oid, count);
    }
}
