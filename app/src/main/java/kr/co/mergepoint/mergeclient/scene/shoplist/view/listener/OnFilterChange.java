package kr.co.mergepoint.mergeclient.scene.shoplist.view.listener;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class OnFilterChange implements FilterPublisher{
    private ArrayList<FilterObserver> filterObservers;
    private int viewId;

    public OnFilterChange() {
        filterObservers = new ArrayList<>();
    }

    @Override
    public void add(FilterObserver observer) {
        filterObservers.add(observer);
    }

    @Override
    public void delete(FilterObserver observer) {
        filterObservers.remove(observer);
    }

    @Override
    public void notifyObserver() {
        for (FilterObserver observer : filterObservers)
            observer.onChangeFilter(viewId);
    }

    public void setViewId(int viewId) {
        this.viewId = viewId;
        notifyObserver();
    }
}
