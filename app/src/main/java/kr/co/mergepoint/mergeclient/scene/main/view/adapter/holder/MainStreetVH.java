package kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kr.co.mergepoint.mergeclient.R;

/**
 * User: Marty
 * Date: 2018-11-14
 * Time: 오후 2:06
 * Description:
 */
public class MainStreetVH extends RecyclerView.ViewHolder {
    public TextView tv;
    public LinearLayout linearLayout;
    public MainStreetVH(View itemView) {
        super(itemView);
        tv = itemView.findViewById(R.id.main_street_item_tv);
        linearLayout = itemView.findViewById(R.id.main_street_item_rl);
    }
}
