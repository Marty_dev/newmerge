package kr.co.mergepoint.mergeclient.application.common;

import android.support.annotation.IntDef;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import kr.co.mergepoint.mergeclient.BuildConfig;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.FavoriteFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.MyCouponFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.QuestionDirectFragment;
import kr.co.mergepoint.mergeclient.scene.newcoupon.view.newMyCouponActivity;
import kr.co.mergepoint.mergeclient.scene.payment.PaymentActivity;
import kr.co.mergepoint.mergeclient.scene.privacy.PrivacyActivity;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.BasketActivity;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.Payment_v2_Activity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;

/**
 * Created by 1017sjg on 2017. 6. 15..
 */

public class Properties {
    /*URL*/
    //public static final String BASE_URL = "http://192.168.0.29/";
    public static final String BASE_URL = BuildConfig.BASE_URL;
    public static final String POSTCODE_URL = BASE_URL + "getAddress/";
    public static final String USER_GUIDE_URL = BASE_URL + "userGuide";
    public static final String WITHDRAWAL_POLICY_URL = BASE_URL + "userGuide#withdraw";
    public static final String PRIVACY_URL = BASE_URL + "privacy";
    public static final String ELECT_TERMS_URL = BASE_URL + "eFTransaction";
    public static final String INFO_PROVIDE_URL = BASE_URL + "infoProvide/";
    public static final String OPEN_SOURCE_LICENSE_URL = BASE_URL + "info/oss";
    public static final String COMPANY_INFO_URL = "http://www.ftc.go.kr/info/bizinfo/communicationViewPopup.jsp?wrkr_no=4158800670";
    public static final String GOOGLE_SEARCH_URL = "https://www.googleapis.com/";

    public static final Class<?>[] requiredLoginActivities = new Class[] {
            BasketActivity.class, PaymentActivity.class, UsageActivity.class, PrivacyActivity.class, Payment_v2_Activity.class,newMyCouponActivity.class
    };

    public static final Class<?>[] requiredLoginFragments = new Class[] {
            FavoriteFragment.class, MyCouponFragment.class, QuestionDirectFragment.class
    };

    /*기업 Role*/
    public static final String ENTERPRISE_ROLE = "02";
    public static final String ENTERPRISE_ROLE2 = "03";

    /*승인 방식 타입*/
    public static final int DEFAULT_APPROVAL = 1;
    public static final int AUTO_APPROVAL = 3;
    public static final int DELEGATION_APPROVAL = 2;

    /*기업 신청 상태*/
    public static final int NO_REGISTER = 0;
    public static final int REGISTER_WAIT = 1;
    public static final int REGISTER_APPROVAL = 2;
    public static final int REGISTER_REJECT = 3;

    /*메인 배너*/
    public static final int BANNER_DELAY = 5000;
    public static final int BANNER_PERIOD = 4000;

    /*유저 상태*/
    public static final int ACTIVE_USER = 1;
    public static final int DORMANCY_USER = 2;
    public static final int WITHDRAWAL_USER = 3;

    /*배너의 액션 타입*/
    public static final int EVENT_DETAIL = 1;
    public static final int EVENT_LIST = 2;
    public static final int THEME_LIST = 3;

    /*WebView의 UserAgent*/
    static final String APP_USER_AGENT = "marge";

    /*MVVM*/
    public static final String EXTRA_VIEW_MODEL_STATE = "viewModelState";

    /*고객센터 전화번호*/
    public static final String SERVICE_CENTER = "16006046";

    /*Webview javascript*/
    public static final String WEB_JAVASCRIPT = "AndroidJavaScript";

    /*FCM Receiver*/
    public static final String FCM_RECEIVER = "fcm_receiver";

    public static final String CROWD_RECEIVER = "crowd_receiver";

    /*Okhttp 타임아웃 시간*/
    public static final int CONNECT_TIMEOUT = 60;
    public static final int WRITE_TIMEOUT = 60;
    public static final int READ_TIMEOUT = 60;

    /*PersistentCookieStore TAG*/
    static final String LOG_TAG = "PersistentCookieStore";
    public static final String COOKIE_PREFS = "CookiePrefsFileOkHttp";
    static final String COOKIE_NAME_PREFIX = "cookie_okhttp_";
    public static final String SETTING_PREFS = "setting_pre";
    public static final String TUTORIAL_PREFS = "tutorial_pre";

    public static final String VIBRATION = "vibration";
    public static final String PUSH = "push";
    public static final String TUTORIAL = "tutorial";

    public static final String APPROVAL_TYPE = "approval_type";
    public static final String APPROVAL_DELEGATION_OID = "approval_delegation_oid";
    public static final String APPROVAL_DELEGATION_NAME = "approval_delegation_name";

    /*TAG*/
    public static final String SOCIAL_LOGIN_TAG = "social";
    public static final String MENU_FRAGMENT_TAG = "menu";
    public static final String POINT_FRAGMENT_TAG = "point";
    public static final String EVENT_FRAGMENT_TAG = "event";
    public static final String EVENT_DETAIL_FRAGMENT_TAG = "event";
    public static final String GRADE_FRAGMENT_TAG = "grade";
    public static final String ALARM_FRAGMENT_TAG = "alarm";
    public static final String CARD_PAYMENT_TAG = "card";
    public static final String ENTERPRISE_USE_POINTS_FRAGMENT_TAG = "enterprise_use_points";
    public static final String APPROVAL_REQUEST_FRAGMENT_TAG = "approval_request";
    public static final String ENTERPRISE_SELECT_POINTS_FRAGMENT_TAG = "enterprise_select_points";
    public static final String SEARCH_CROWD_PAY_FRAGMENT_TAG = "search_crowd_pay";
    public static final String SEARCH_CROWD_PAY_FRAGMENTV2_TAG = "search_crowd_payv2";

    public static final String CROWED_APPROVAL_FRAGMENT_TAG = "crowd_approval";
    public static final String CROWED_APPROVAL_FRAGMENTV2_TAG = "crowd_approvalv2";

    public static final String CROWD_APPROVAL_REQUEST_FRAGMENT_TAG = "crowd_request_approval";
    public static final String CROWD_APPROVAL_REQUEST_RESULT_FRAGMENT_TAG = "crowd_request_approval_result";

    public static final String CROWD_PAY_HISTORY_DETAIL_FRAGMENT_TAG = "crowd_pay_history_detail";
    public static final String FAVORITE_FRAGMENT_TAG = "favorite";
    public static final String TUTORIAL_FRAGMENT_TAG = "tutorial";
    public static final String COUPON_FRAGMENT_TAG = "coupon";
    public static final String NOTICE_FRAGMENT_TAG = "notice";
    public static final String NOTICE_DETAIL_TAG = "notice_detail";
    public static final String QUESTIONS_FRAGMENT_TAG = "questions";
    public static final String QUESTION_DIRECT_FRAGMENT_TAG = "question_direct";
    public static final String SETTINGS_FRAGMENT_TAG = "settings";
    public static final String SETTINGS_APPROVAL_FRAGMENT_TAG = "settings_approval";
    public static final String DELEGATION_USER_FRAGMENT_TAG = "delegation_user";
    public static final String FAVORITE_CROWED_USER_FRAGMENT_TAG = "favorite_crowd";
    public static final String ENTERPRISE_NOTICE_FRAGMENT_TAG = "enterprise_notice";
    public static final String TICKET_FRAGMENT_TAG = "favorite_crowd";
    public static final String ENTERPRISE_POINT_HISTORY_FRAGMENT_TAG = "favorite_crowd";
    public static final String CROWED_PAY_HISTORY_FRAGMENT_TAG = "crowd_pay_history";
    public static final String SEARCH_ENTERPRISE_FRAGMENT_TAG = "search_enterprise";
    public static final String RESULT_PAGE_FRAGMENT_TAG = "result_page";
    public static final String WITHDRAWAL_FRAGMENT_TAG = "withdraw";
    public static final String WITHDRAWAL_POLICY_TAG = "withdraw_policy";
    public static final String CONTACT_FRAGMENT_TAG = "contact_us";
    public static final String CARD_REGISTE_TAG = "card_registe";
    public static final String UPLUS_REGISTE_TAG = "card_registe_uplus";
    public static final String PAY_RESULT_TAG = "pay_result";
    public static final String USAGE_ORDER_TAG = "usage_order";
    public static final String USAGE_SHOP_TAG = "usage_shop";
    public static final String USING_MENU_TAG = "using_menu";
    public static final String BASKET_DETAIL_TAG = "basket_detail";
    public static final String FILTER_THEME_TAG = "filter_theme";
    public static final String FILTER_PRICE_TAG = "filter_price";
    public static final String FORGOT_PW_TAG = "forgot_pw";
    public static final String FORGOT_PW_RESULT_TAG = "forgot_pw_result";
    public static final String RESET_PW_TAG = "reset_pw";
    public static final String INPUT_INSTA_EMAIL_TAG = "forgot_pw_result";
    public static final String PAY_CANCEL_TAG = "pay_cancel";
    public static final String PAY_POINT_REFUND_TAG = "pay_point_refund";
    public static final String APPROVE_COMPLETE_TAG = "approve_complete";
    public static final String SIMPLE_PRIVACY_TAG = "simple_privacy";
    public static final String MODIFY_PRIVACY_TAG = "modify_privacy";
    public static final String TERMS_USE_TAG = "terms_use";
    public static final String PRIVACY_POLICY_TAG = "privacy";
    public static final String INFO_PROVIDE_TAG = "info_provide";
    public static final String ELECT_TERMS_TAG = "elect_terms";
    public static final String COMPANY_INFO_TAG = "company_info";
    public static final String OPEN_SOURCE_TAG = "open_source";
    public static final String MERGES_PICK_TAG = "merges_pick";
    public static final String MERGES_PICK_DETAIL_TAG = "merges_pick_detail";
    public static final String EXTERNAL_AD = "ex_ad";
    public static final String THEME_LIST_TAG = "theme_list";
    public static final String REGISTER_POST_TAG = "post";
    public static final String MENU_DETAIL_TAG = "menu_detail";
    public static final String ENTERPRISE_MENU_TAG = "enterprise_menu";
    public static final String SHOP_REVIEW_TAG = "shop_review";

    public static final String SHARE = "share";

    /*REQUEST CODE*/
    public static final int GOOGLE_SIGN_IN = 9001;
//    public static final int FCM_NOTIFICATION = 9002;
    public static final int REQUEST_RESOLVE_ERROR = 9003;
    public static final int QR_CODE_REQUEST = 9004;
    public static final int PAY_RESULT_REQUEST = 9005;
    public static final int PAY_PRICE_REQUEST = 9012;
    public static final int ATTACH_PIC_RESULT_REQUEST = 9006;
    public static final int GALLERY_REQUEST = 9007;
    public static final int SHOP_INFO_REQUEST = 9008;
    public static final int THEME_REQUEST = 9009;
    public static final int FAVORITE_REQUEST = 9010;
    public static final int UNCAUGHT_EXCEPTION_REQUEST = 9011;

    /*INTENT NAME*/
    public static final String STREET_NAME = "street_name";
    public static final String MEMBER_INFO = "member_info";
    public static final String PAYMENT_INFO = "payment_info";
    public static final String SHOP_INFO = "select_shop_info";
    public static final String PAYMENT_RESULT = "payment";
    public static final String PAYMENT_RESULT_ARRAY = "payment_array";
    public static final String WEB_URL = "url";
    public static final String COMBINE_IMG = "combine_images";
    public static final String COMBINE_EVENT_INFO = "combine_event_info";
    public static final String MAIN_SEARCH = "main_search";
    public static final String SHOP_FAVORITE = "shop_favorite";
    public static final String SHOP_OID = "shop_oid";
    public static final String OID = "oid";
    public static final String USER_PATH = "userPath";

    public static final String SOURCE_ACTIVITY = "source";

    /*GOOGLE Provider*/
    public static final String GOOGLE_PROVIDER = "google";

    /*Main 스플래쉬 기본 지속 시간*/
    public static final int SPLASH_DISPLAY_DEFAULT = 500;

    /*No Value*/
    public static final int NO_VALUE = -1;

    /*Shop List page size*/
    public static final int SHOP_LIST_SIZE = 15;

    /*Location*/
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    /*권한*/
    public static final int PERMISSIONS_REQUEST_LOCATION = 1001;
    public static final int PERMISSIONS_REQUEST_SMS = 1002;
    public static final int PERMISSIONS_REQUEST_LOCATION_THEME = 1003;
    public static final int PERMISSIONS_REQUEST_LOCATION_FAVORITE = 1004;
    public static final int REQUEST_CHECK_SETTINGS = 1005;

    /*Shop List 전체보기*/
    public static final String SHOP_TOTAL = "ALL";

    /*옵션의 타입*/
    public static final int OPTION_TYPE_MAIN = 1;

    /*점포 제휴 타입*/
    public static final int GENERAL_PARTNERSHIP = 1;
    public static final int CORPORATE_ALLIANCE = 2;
    public static final int CORPORATE_CAFETERIA = 3;
    public static final int FRANCHISE = 4;

    /*메뉴 자세히 보기 리스트 타입*/
    public static final int LIST_TYPE_ITEM = 1;
    public static final int LIST_TYPE_HEADER = 2;
    public static final int LIST_TYPE_FOOTER = 3;
    public static final int LIST_TYPE_HEADER_PRICE = 4;

    public static final int LIST_TYPE_CHECK = 6;
    public static final int LIST_TYPE_RADIO = 7;

    /*Notification ID*/
    public static final int MERGE_NOTIFICATION_ID = 0;
    public static final String MERGE_NOTIFICATION_CHANNEL_ID = "merge";

    /*Main Page*/
    @IntDef({EVENT_FRAGMENT, POINT_FRAGMENT, NOTICE_FRAGMENT, NOTICE_DETAIL_FRAGMENT, GRADE_FRAGMENT,
            MENU_FRAGMENT, SETTINGS_FRAGMENT, WITHDRAWAL_FRAGMENT, LICENSE_FRAGMENT, WITHDRAWAL_POLICY_FRAGMENT, MERGES_PICK_DETAIL_FRAGMENT,
            EVENT_DETAIL_FRAGMENT, CONTACT_US_FRAGMENT, TERMS_USE_FRAGMENT, PRIVACY_POLICY_FRAGMENT, ELECT_FINANCIAL_TERMS_FRAGMENT,
            COMPANY_INFO_FRAGMENT, QUESTIONS_FRAGMENT, QUESTION_DIRECT_FRAGMENT, MERGES_PICK_FRAGMENT, EXTERNAL_AD_FRAGMENT, RESET_PW_FRAGMENT,
            SETTING_APPROVAL, DELEGATION_USER, FAVORITE_CROWED_USER, ENTERPRISE_NOTICE, ALARM_LIST})
    @Retention(RetentionPolicy.SOURCE)
    public @interface MainFragment {}
    public static final int EVENT_FRAGMENT = 0;
    public static final int POINT_FRAGMENT = 1;
    public static final int NOTICE_FRAGMENT = 3;
    public static final int NOTICE_DETAIL_FRAGMENT = 4;
    public static final int GRADE_FRAGMENT = 5;
    public static final int MENU_FRAGMENT = 6;
    public static final int SETTINGS_FRAGMENT = 7;
    public static final int WITHDRAWAL_FRAGMENT = 8;
    public static final int LICENSE_FRAGMENT = 9;
    public static final int WITHDRAWAL_POLICY_FRAGMENT = 10;
    public static final int MERGES_PICK_DETAIL_FRAGMENT = 11;
    public static final int EVENT_DETAIL_FRAGMENT = 12;
    public static final int CONTACT_US_FRAGMENT = 13;
    public static final int TERMS_USE_FRAGMENT = 14;
    public static final int PRIVACY_POLICY_FRAGMENT = 15;
    public static final int ELECT_FINANCIAL_TERMS_FRAGMENT = 16;
    public static final int COMPANY_INFO_FRAGMENT = 18;
    public static final int QUESTIONS_FRAGMENT = 19;
    public static final int QUESTION_DIRECT_FRAGMENT = 20;
    public static final int MERGES_PICK_FRAGMENT = 21;
    public static final int EXTERNAL_AD_FRAGMENT = 22;
    public static final int RESET_PW_FRAGMENT = 23;
    public static final int SETTING_APPROVAL = 24;
    public static final int DELEGATION_USER = 25;
    public static final int FAVORITE_CROWED_USER = 26;
    public static final int ENTERPRISE_NOTICE = 27;
    public static final int ALARM_LIST= 28;


    /*Login Social*/
    @IntDef({FACEBOOK, KAKAO, NAVER, INSTA})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LoginSocial {}
    public static final int FACEBOOK = 0;
    public static final int KAKAO = 1;
    public static final int NAVER = 2;
    public static final int INSTA = 3;

    /*State Button*/
    @IntDef({NORMAL, UP, DOWN, SELECT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface StateButton {}
    public static final int NORMAL = 0;
    public static final int UP = 1;
    public static final int DOWN = 2;
    public static final int SELECT = 3;

    /*Fragment Anim*/
    @IntDef({UP_DOWN, RIGHT_LEFT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FragmentAnim {}
    public static final int UP_DOWN = 0;
    public static final int RIGHT_LEFT = 1;

    /*Filter*/
    @StringDef({DESC, ASC})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Direction {}
    public static final String DESC = "DESC";
    public static final String ASC = "ASC";

    /*Usage List Type*/
    @IntDef({ORDER, STORE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ListType {}
    public static final int ORDER = 0;
    public static final int STORE = 1;

    /*Usage Tab Type*/
    @StringDef({PAYMENT, USE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TabType {}
    public static final String PAYMENT = "payment";
    public static final String USE = "use";

    /*Terms*/
    @IntDef({ONE_MONTH, THREE_MONTH, SIX_MONTH})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Terms {}
    public static final int ONE_MONTH = 1;
    public static final int THREE_MONTH = 2;
    public static final int SIX_MONTH = 3;

    /*Gravity*/
    @IntDef({LEFT, CENTER, STREET,CUSTOM})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Gravity {}
    public static final int LEFT = 0;
    public static final int CENTER = 1;
    public static final int STREET = 2;
    public static final int CUSTOM = 17;

    /*Shop Tab*/
    @IntDef({INFO, MENU, REVIEW})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ShopTab {}
    public static final int INFO = 0;
    public static final int MENU = 1;
    public static final int REVIEW = 2;

    /*Like State*/
    @IntDef({PLAY, RESET, NONMEMBER, NOTBUY})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LikeState {}
    public static final int PLAY = 0;
    public static final int RESET = 1;
    public static final int NONMEMBER = 2;
    public static final int NOTBUY = 3;

    /*AppBar State*/
    @IntDef({EXPAND, COLLAPSE, IDLE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface AppBarState {}
    public static final int EXPAND = 0;
    public static final int COLLAPSE = 1;
    public static final int IDLE = 2;

    /*LoadList State*/
    @IntDef({INITIAL, ADD})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LoadState {}
    public static final int INITIAL = 0;
    public static final int ADD = 1;

    /*Touch Type*/
    @IntDef({TOUCH_SHOP, TOUCH_SHARE, TOUCH_COUPON, TOUCH_IN_AD, TOUCH_EX_AD, TOUCH_EVENT_LIST})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TouchType {}
    public static final int TOUCH_SHOP = 1;
    public static final int TOUCH_SHARE = 2;
    public static final int TOUCH_COUPON = 3;
    public static final int TOUCH_IN_AD = 4;
    public static final int TOUCH_EX_AD = 5;
    public static final int TOUCH_EVENT_LIST = 6;

    /*Intent Type*/
    @StringDef({INTENT_SHOP, INTENT_EVENT, INTENT_FORGOT_PW, INTENT_REGISTER})
    @Retention(RetentionPolicy.SOURCE)
    public @interface IntentType {}
    public static final String INTENT_SHOP = "SHOP";
    public static final String INTENT_EVENT = "EVENT";
    public static final String INTENT_FORGOT_PW = "FORGOT_PW";
    public static final String INTENT_REGISTER = "REGISTER";

    /*Shop Visible Type*/
    @IntDef({LIST, MAP})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ShopVisibleType {}
    public static final int LIST = 0;
    public static final int MAP = 1;

    /*Visible Type*/
    @IntDef({HIDE, SHOW})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Visible {}
    public static final int HIDE = 0;
    public static final int SHOW = 1;

    @IntDef({AVAILABLE, EXCEEDED, NOT_APPLICABLE, NUMBER_FORMAT_EXCEPTION})
    @Retention(RetentionPolicy.SOURCE)
    public @interface PointApplyState {}
    public static final int AVAILABLE = 0;                   /* 포인트를 3천 포인트 이상 보유포인트 이내 사용 */
    public static final int EXCEEDED = 1;                   /* 현재 입력한 포인트와 이전에 입력한 포인트의 합이 사용가능한 포인트를 넘었을 때 */
    public static final int NOT_APPLICABLE = 2;             /* 포인트가 3천 포인트 미만이면서 0이 아닐때 */
    public static final int NUMBER_FORMAT_EXCEPTION = 3;    /* 포인트에 적은 텍스트가 숫자가 아닐때 */
}
