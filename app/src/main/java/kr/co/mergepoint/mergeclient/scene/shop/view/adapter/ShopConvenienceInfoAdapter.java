package kr.co.mergepoint.mergeclient.scene.shop.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Map;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.ShopConvenienceImageBinding;

/**
 * Created by 1017sjg on 2017. 7. 13..
 */

public class ShopConvenienceInfoAdapter extends BasicListAdapter<BasicListHolder<ShopConvenienceImageBinding, Integer>, Integer> {

    public ShopConvenienceInfoAdapter(Map<String, Boolean> info) {
        super();
        for (String key : info.keySet()) {
            switch (key) {
                case "wifi":
                    getObservableArrayList().add(R.drawable.icon_serv_001_wifi_normal);
                    break;
                case "parking":
                    getObservableArrayList().add(R.drawable.icon_serv_002_parking_normal);
                    break;
                case "valetService":
                    getObservableArrayList().add(R.drawable.icon_serv_009_valet_normal);
                    break;
                case "toiletClassification":
                    getObservableArrayList().add(R.drawable.icon_serv_003_toilet_normal);
                    break;
                case "groupSeat":
                    getObservableArrayList().add(R.drawable.icon_serv_004_group_normal);
                    break;
                case "reservation":
                    getObservableArrayList().add(R.drawable.icon_serv_005_rese_normal);
                    break;
                case "takeout":
                    getObservableArrayList().add(R.drawable.icon_serv_006_pack_normal);
                    break;
                case "convenience":
                    getObservableArrayList().add(R.drawable.icon_serv_007_disa_normal);
                    break;
                case "creche":
                    getObservableArrayList().add(R.drawable.icon_serv_010_child_normal);
                    break;
                case "petAllowed":
                    getObservableArrayList().add(R.drawable.icon_serv_008_pet_normal);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public BasicListHolder<ShopConvenienceImageBinding, Integer> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_convenience_image, parent, false);
        ShopConvenienceImageBinding itemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<ShopConvenienceImageBinding, Integer>(itemBinding) {
            @Override
            public void setDataBindingWithData(Integer data) {
                getDataBinding().setDrawable(data);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<ShopConvenienceImageBinding, Integer> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
