package kr.co.mergepoint.mergeclient.application.common.observer;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public interface Observer<T> {
    void onTouch(T data);
}
