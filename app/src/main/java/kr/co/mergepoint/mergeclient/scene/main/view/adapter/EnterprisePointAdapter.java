package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.EnterprisePointHistoryItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.history.CompanyPointHistory;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class EnterprisePointAdapter extends BasicListAdapter<BasicListHolder<EnterprisePointHistoryItemBinding, CompanyPointHistory>, CompanyPointHistory> {

    public EnterprisePointAdapter(ArrayList<CompanyPointHistory> arrayList) {
        super(arrayList);
    }

    @Override
    public BasicListHolder<EnterprisePointHistoryItemBinding, CompanyPointHistory> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.enterprise_point_history_item, parent, false);
        EnterprisePointHistoryItemBinding pointListItemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<EnterprisePointHistoryItemBinding, CompanyPointHistory>(pointListItemBinding) {
            @Override
            public void setDataBindingWithData(CompanyPointHistory data) {
                MDEBUG.debug("point adapter is working 1");
                getDataBinding().setPointHistory(data);
            }
        };
    }
    public void setParams(ArrayList<CompanyPointHistory> list){
        if (list == null)
            return;
        this.getObservableArrayList().clear();
        this.getObservableArrayList().addAll(list);
    }

    @Override
    public void setBindViewHolder(BasicListHolder<EnterprisePointHistoryItemBinding, CompanyPointHistory> holder, int position) {
        MDEBUG.debug("point adapter is working 2");

        CompanyPointHistory obj = getObservableArrayList().get(position);
        holder.bind(getObservableArrayList().get(position));
        MDEBUG.debug("obj info : " + obj.reason);
        MDEBUG.debug("obj info : " + obj.givenDateTime);
        MDEBUG.debug("obj info : " + obj.point);
        MDEBUG.debug("obj info : " + obj.remainTicketPrice);
        MDEBUG.debug("obj info : " + obj.tickePrice);
        MDEBUG.debug("obj info : " + obj.type);
    }
}
