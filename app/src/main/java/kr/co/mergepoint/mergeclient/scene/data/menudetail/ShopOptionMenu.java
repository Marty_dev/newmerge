package kr.co.mergepoint.mergeclient.scene.data.menudetail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 6. 24..
 */

public class ShopOptionMenu implements Parcelable {

    @SerializedName("oid")
    public int oid;

    @SerializedName("type")
    public int type;

    @SerializedName("multiSelect")
    public boolean multiSelect;

    @SerializedName("name")
    public String name;

    @SerializedName("menus")
    public ArrayList<ShopOptionSelectMenu> menus;

    private ShopOptionMenu(Parcel in) {
        oid = in.readInt();
        type = in.readInt();
        multiSelect = in.readInt() == 1;
        name = in.readString();
        menus = new ArrayList<>();
        in.readTypedList(menus, ShopOptionSelectMenu.CREATOR);
    }

    public static final Parcelable.Creator<ShopOptionMenu> CREATOR = new Parcelable.Creator<ShopOptionMenu>() {
        @Override
        public ShopOptionMenu createFromParcel(Parcel in) {
            return new ShopOptionMenu(in);
        }

        @Override
        public ShopOptionMenu[] newArray(int size) {
            return new ShopOptionMenu[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(oid);
        dest.writeInt(type);
        dest.writeInt(multiSelect ? 1 : 0);
        dest.writeString(name);
        dest.writeTypedList(menus);
    }
}
