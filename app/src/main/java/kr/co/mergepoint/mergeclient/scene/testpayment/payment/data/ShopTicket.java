package kr.co.mergepoint.mergeclient.scene.testpayment.payment.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * User: Marty
 * Date: 2018-07-19
 * Time: 오전 10:54
 * Description:
 */
public class ShopTicket {

    @Expose
    @SerializedName("ticketName")
    public String ticketName;
    @Expose
    @SerializedName("remainTicketCount")
    public int remainTicketCount;
    @Expose
    @SerializedName("ticketDescription")
    public String ticketDescription;
    @Expose
    @SerializedName("oid")
    public int oid;
}
