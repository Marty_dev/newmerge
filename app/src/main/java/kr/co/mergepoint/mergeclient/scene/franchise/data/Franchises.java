package kr.co.mergepoint.mergeclient.scene.franchise.data;

import com.google.gson.annotations.SerializedName;

/**
 * User: Marty
 * Date: 2018-10-02
 * Time: 오전 10:19
 * Description:
 */
public class Franchises {
    @SerializedName("backImage")
    public String imgurl;
    @SerializedName("name")
    public String name;
    @SerializedName("oid")
    public int oid;
    @SerializedName("displayOrder")
    public int displayOrder;

    public Franchises() {
    }

    public Franchises(String name) {
        this.name = name;
    }

    public Franchises(String imgurl, String name) {
        this.imgurl = imgurl;
        this.name = name;
    }
}
