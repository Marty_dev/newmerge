package kr.co.mergepoint.mergeclient.scene.cardmanagement.dagger;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.CardManagementActivity;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.model.CardManagementModel;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.presenter.CardManagementPresenter;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.presenter.callback.CardCallback;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.view.CardManagementView;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.view.fragment.CardRegisteFragment;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.view.fragment.UplusRegisteFragment;

/**
 * Created by jgson on 2017. 6. 5..
 */

@Module
public class CardModule {

    private CardManagementActivity activity;

    public CardModule(CardManagementActivity activity) {
        this.activity = activity;
    }

    @Provides
    @CardScope
    CardManagementModel provideModel() {
        return new CardManagementModel();
    }

    @Provides
    @CardScope
    CardManagementView provideView() {
        return new CardManagementView(activity, R.layout.card_main);
    }

    @Provides
    @CardScope
    CardManagementPresenter providePresenter(CardManagementView view, CardManagementModel model) {
        return new CardManagementPresenter(view, model, new CardCallback(view, model));
    }
}
