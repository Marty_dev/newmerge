package kr.co.mergepoint.mergeclient.scene.data.menu;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder.Menu;

/**
 * Created by 1017sjg on 2017. 6. 23..
 */
public class ShopTableMenuInfo {
    @SerializedName("serveType")
    public int serveType;

    @SerializedName("serveName")
    public String serveName;

    @SerializedName("menus")
    public ArrayList<ShopChildInfo> menus;

    Menu getGroupMenu() {
        return new Menu(serveName, menus, serveType);
    }
}