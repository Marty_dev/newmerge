package kr.co.mergepoint.mergeclient.scene.shoplist.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.facebook.CallbackManager;

import net.daum.mf.map.api.MapCurrentLocationMarker;
import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.application.common.ShareFragment;
import kr.co.mergepoint.mergeclient.application.common.StateButton;
import kr.co.mergepoint.mergeclient.databinding.ShopListBinding;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.data.splash.CategoryInfo;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.EnterpriseShopActivity;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.PaymentResultFragment;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;
import kr.co.mergepoint.mergeclient.scene.shoplist.ShopListActivity;
import kr.co.mergepoint.mergeclient.scene.shoplist.presenter.ShopListPresenter;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.ShopListAdapter;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.fragment.FilterFragment;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.listener.OnFilterChange;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.BasketActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CORPORATE_ALLIANCE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CORPORATE_CAFETERIA;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FILTER_PRICE_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FILTER_THEME_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FRANCHISE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.GENERAL_PARTNERSHIP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.HIDE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INITIAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INTENT_SHOP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MAP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHARE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOW;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SOURCE_ACTIVITY;
import static kr.co.mergepoint.mergeclient.application.common.Properties.STREET_NAME;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class ShopListView extends BaseView<ShopListActivity, ShopListPresenter, ShopListBinding> implements ShopListInterface {

    private int mapId;
    private boolean isDescription;
    private MapPoint savePoint;
    private int saveZoomLevel;

    public OnFilterChange sortFilter;

    public ShopListView(ShopListActivity activity, int layout) {
        super(activity, layout);
        sortFilter = new OnFilterChange();
    }

    @Override
    protected Toolbar setToolbar() {
        return binding.shopListToolbarLayout.shopListToolbar;
    }

    @Override
    public void addMapView() {
        MapView oldMapView = binding.shopListMapLayout.findViewById(mapId);
        if (oldMapView != null)
            binding.shopListMapLayout.removeView(oldMapView);
        if (binding != null && binding.shopListMapLayout.getChildCount() > 0) {
            MapView mapView = new MapView(activity);
            mapId = View.generateViewId();
            mapView.setId(mapId);
            MapCurrentLocationMarker marker = new MapCurrentLocationMarker();
            marker.setTrackingOffImageId(R.drawable.obj_map_my_position);
            marker.setTrackingAnimationImageIds(new int[]{R.drawable.obj_map_my_position});
            marker.setTrackingAnimationImageAnchors(new float[]{0.5f}, new float[]{0.5f});
            mapView.setCurrentLocationMarker(marker);
            int statusResId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
            int statusHeight = statusResId > 0 ? activity.getResources().getDimensionPixelSize(statusResId) : 0;
            int toolbarHeight = (int) activity.getResources().getDimension(R.dimen.toolbar_height);
            int tabHeight = (int) activity.getResources().getDimension(R.dimen.tab_height);

            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
            int mapHeight = metrics.heightPixels - (statusHeight + toolbarHeight + tabHeight);

            mapView.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mapHeight));
            mapView.setMapViewEventListener(activity);
            mapView.setPOIItemEventListener(activity);
            binding.shopListMapLayout.addView(mapView, 0);
            Object tag = binding.shopListToolbarLayout.shopMap.getTag();

            int mapButton = tag == null ? MAP : (int) tag;
            mapView.setTranslationY(mapButton == MAP ? mapHeight : 0);
            binding.mapControllerLayout.setTranslationY(mapButton == MAP ? mapHeight : 0);
        }
    }

    @Override
    public void removeMapView() {
        MapView mapView = binding.shopListMapLayout.findViewById(mapId);
        if (binding != null && mapView != null) {
            binding.shopListMapLayout.removeView(mapView);
            savePoint = mapView.getMapCenterPoint();
            saveZoomLevel = mapView.getZoomLevel();
        }
    }

    @Override
    public void initStreetSpinner() {
        int position = getStreetArray().indexOf(getStreetName());
        binding.shopListToolbarLayout.streetSpinner.post(()->binding.shopListToolbarLayout.streetSpinner.setSelection(position, true));
    }

    @Override
    public void initDescription() {
        int height = (int) activity.getResources().getDimension(R.dimen.map_description_view_height);
        binding.mapSelectDescription.setTranslationY(height);
    }

    @Override
    public void selectStreet(String streetName, Map<String, Object> filter) {
        MDEBUG.debug("Call 1");
        measureStreetSpinnerWidth(streetName);
        binding.shopList.loadShopList(INITIAL, filter);
        binding.shopList.disableSearch();
    }

    @Override
    public void measureStreetSpinnerWidth(String streetName) {
        Rect bounds = new Rect();
        Paint textPaint = new Paint();
        textPaint.setAntiAlias(true);
        textPaint.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 17, Resources.getSystem().getDisplayMetrics()));
        textPaint.getTextBounds(streetName, 0, streetName.length(), bounds);
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) binding.shopListToolbarLayout.streetSpinner.getLayoutParams();
        layoutParams.width = bounds.width() + (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, Resources.getSystem().getDisplayMetrics());
    }

    @Override
    public void openSelectTheme() {
        ArrayList<String> theme = new ArrayList<>(Arrays.asList(activity.getResources().getStringArray(R.array.theme)));
        openFragment(RIGHT_LEFT, R.id.filter_fragment, FilterFragment.newInstance(theme, getString(R.string.theme), activity), FILTER_THEME_TAG);
    }

    @Override
    public void openSelectPriceRange() {
        ArrayList<String> price = new ArrayList<>(Arrays.asList(activity.getResources().getStringArray(R.array.price_range)));
        openFragment(RIGHT_LEFT, R.id.filter_fragment, FilterFragment.newInstance(price, getString(R.string.price_range), activity), FILTER_PRICE_TAG);
    }

    @Override
    public void openShoppingBasket() {
        openActivityForResult(BasketActivity.class, ShopListActivity.class, SOURCE_ACTIVITY, PAY_RESULT_REQUEST);
    }

    @Override
    public void openPaymentResult() {
        openFragment(RIGHT_LEFT, R.id.shop_list_filter_layout, PaymentResultFragment.newInstance(), PAY_RESULT_TAG);
    }



    public void openPaymentResult(int paymentRef ,boolean ismulti) {
        openFragment(RIGHT_LEFT, R.id.shop_list_filter_layout, PaymentResultFragment.newInstance(activity,paymentRef,R.id.shop_list_filter_layout,ismulti), PAY_RESULT_TAG);
    }
    @Override
    public void openShopDetail(Shop shop) {
        if (shop.type == GENERAL_PARTNERSHIP || shop.type == FRANCHISE) {
            openActivityForResultWithParcelable(ShopActivity.class, SHOP_INFO, shop, SHOP_INFO_REQUEST);
        } else if (shop.type == CORPORATE_ALLIANCE || shop.type == CORPORATE_CAFETERIA) {
            openActivityForResultWithParcelable(EnterpriseShopActivity.class, SHOP_INFO, shop, SHOP_INFO_REQUEST);
        }
    }

    @Override
    public void openMapIntent() {
        if (binding != null && binding.getSelectShop() != null) {
            Shop shop = binding.getSelectShop();
            Uri uri = Uri.parse(String.format(Locale.ENGLISH, "geo:%f, %f", shop.latitude, shop.longitude));
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
            if (mapIntent.resolveActivity(activity.getPackageManager()) != null)
                activity.startActivity(mapIntent);
        }
    }

    @Override
    public void openShopShared() {
        if (binding != null && binding.getSelectShop() != null) {
            Shop shop = binding.getSelectShop();
            // TODO: Shop 에 description이 없음 !
            openShareFragment(R.id.shop_list_root, ShareFragment.newInstance(shop.shopName, "", shop.shopImage.size() > 0 ? shop.shopImage.get(0) : "", shop.oid, INTENT_SHOP), SHARE);
        }
    }

    @Override
    public void animCart() {
        animCart(MAP, false);
    }

    @Override
    public void animCart(int mapButton, boolean isDescription) {
        binding.shopListCart.getCartCount(this, mapButton, isDescription);
    }

    @Override
    public void animSearchBar() {
        int search = (int) binding.shopListSearchLayout.searchBar.getTag();
        binding.shopListSearchLayout.searchBar.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                if (search == HIDE) {
                    binding.shopListSearchLayout.searchBar.setVisibility(View.VISIBLE);
                    showKeyboard(binding.shopListSearchLayout.searchText);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (search == SHOW) {
                    binding.shopListSearchLayout.searchBar.setVisibility(View.GONE);
                    hideKeyboard(binding.shopListSearchLayout.searchText);
                }
            }
        }).translationY(search == SHOW ? -activity.getResources().getDimension(R.dimen.shop_list_search_height) : 0).withLayer();
        binding.shopListSearchLayout.searchBar.setTag(search == SHOW ? HIDE : SHOW);
    }

    @Override
    public void animFilter() {
        binding.shopListFilterLayout.openDrawer(binding.filterFragment);
    }

    @Override
    public void animMap() {
        int mapButton = (int) binding.shopListToolbarLayout.shopMap.getTag();
        binding.shopListToolbarLayout.shopMap.setImageResource(mapButton == LIST ? R.drawable.icon_nav_map : R.drawable.icon_nav_list);
        MapView mapView = getMapView();
        if (mapView != null) {
            int duration = activity.getResources().getInteger(R.integer.map_anim_duration);
            int translation = mapButton == LIST ? mapView.getHeight() : 0;
            mapView.animate().setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    if (mapButton == MAP)
                        mapView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    if (mapButton == LIST)
                        mapView.setVisibility(View.GONE);
                }
            }).setDuration(duration).translationY(translation).start();
            binding.mapControllerLayout.animate().setDuration(duration).translationY(translation).start();
            binding.shopListToolbarLayout.shopMap.setTag(mapButton == LIST ? MAP : LIST);
            animCart(mapButton, isDescription);
        }
    }

    @Override
    public void animDescription(Shop shop, boolean visible) {
        binding.setSelectShop(shop);
        this.isDescription = visible;
        int duration = activity.getResources().getInteger(R.integer.map_anim_duration);
        int translation = !visible ? binding.mapSelectDescription.getHeight() : 0;
        int descriptionHeight = (int) activity.getResources().getDimension(R.dimen.map_description_view_height);
        int mapButton = (int) binding.shopListToolbarLayout.shopMap.getTag();
        binding.mapSelectDescription.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                if (visible)
                    binding.mapSelectDescription.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (!visible)
                    binding.mapSelectDescription.setVisibility(View.GONE);
            }
        }).setDuration(duration).translationY(translation).withLayer().start();
        animCart(mapButton, isDescription);
        animZoom(visible, -descriptionHeight);
    }

    @Override
    public void animZoom(boolean position, int value) {
        binding.mapZoomLayout.animate().translationY(position ? value : 0).withLayer().start();
    }

    @Override
    public void distanceSeekBar(SeekBar seekBar, int position) {
        final TextView trackText = binding.shopListFilter.distanceLayout.thumbTrackText;
        final String distanceText = activity.getResources().getStringArray(R.array.distance)[position];
        trackText.post(() -> trackText.setText(distanceText));
        float width = trackText.getPaint().measureText(distanceText, 0, distanceText.length());
        trackText.animate().setDuration(0).translationX(seekBar.getThumb().getBounds().right - width).withLayer().start();
    }

    @Override
    public void selectTheme(String name) {
        activity.getSupportFragmentManager().popBackStack();
        if (currentShopType() == MAP) {
            binding.shopMapFilter.themeText.setText(name);
        } else {
            binding.shopListFilter.themeText.setText(name);
        }
    }

    @Override
    public void removePriceRangeFilter(ArrayList<String> priceRange) {
        activity.getSupportFragmentManager().popBackStack();
        List<String> priceRangeResources = Arrays.asList(activity.getResources().getStringArray(R.array.price_range));
        List<String> priceStrArray;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            priceStrArray = priceRange.stream().map((String rangeStr) -> priceRangeResources.get(Integer.parseInt(rangeStr) - 1)).collect(Collectors.toList());
        } else {
            priceStrArray = new ArrayList<>();
            for (String priceStr : priceRange)
                priceStrArray.add(priceRangeResources.get(Integer.parseInt(priceStr) - 1));
        }
        (currentShopType() == MAP ? binding.shopMapFilter.priceRangeText : binding.shopListFilter.priceRangeText).setText(TextUtils.join(", ", priceStrArray));
    }

    @Override
    public void resetFilter() {
        TextView themeText = currentShopType() == MAP ? binding.shopMapFilter.themeText : binding.shopListFilter.themeText;
        TextView priceRangeText = currentShopType() == MAP ? binding.shopMapFilter.priceRangeText : binding.shopListFilter.priceRangeText;
        ConstraintLayout shopConvLayout = currentShopType() == MAP ? binding.shopMapFilter.shopConvLayout : binding.shopListFilter.shopConvLayout;
        if (currentShopType() == LIST) {
            for (int i = 0; i < binding.shopListFilter.sortLayout.getChildCount(); i++) {
                StateButton sortBtn = (StateButton) binding.shopListFilter.sortLayout.getChildAt(i);
                sortBtn.resetDrawable();
            }
            binding.shopListFilter.distanceLayout.seek.setProgress(0);
        }

        themeText.setText("");
        priceRangeText.setText("");
        for (int i = 0; i < shopConvLayout.getChildCount(); i++) {
            StateButton sortBtn = (StateButton) shopConvLayout.getChildAt(i);
            sortBtn.resetDrawable();
        }
    }

    @Override
    public void changeFilterView() {
        int visibleType = (int) binding.shopListToolbarLayout.shopMap.getTag();
        binding.shopListFilter.filterRoot.setVisibility(visibleType == LIST ? View.GONE : View.VISIBLE);
        binding.shopMapFilter.filterRoot.setVisibility(visibleType == LIST ? View.VISIBLE : View.GONE);
    }

    @Override
    public void searchShop(String searchString, Map<String, Object> filter) {
        animSearchBar();
        MDEBUG.debug("Call 2");

        hideKeyboard(binding.shopListSearchLayout.searchText);
        binding.shopList.loadShopList(INITIAL, filter);
        binding.shopList.setSearchText(searchString);
    }

    @Override
    public void searchCategoryShop(Map<String, Object> filter) {
        MDEBUG.debug("Call 3");

        binding.shopList.loadShopList(INITIAL,filter);
        binding.shopList.disableSearch();
    }

    @Override
    public void startListFilter(Map<String, Object> filter) {
        MDEBUG.debug("Call 4");

        binding.shopListFilterLayout.closeDrawers();
        binding.shopList.loadShopList(INITIAL, filter);
    }

    @Override
    public void startMapFilter() {
        if (getMapView() != null)
            getMapView().removeAllPOIItems();
        binding.shopListFilterLayout.closeDrawers();
    }

    /* MAP */
    @Override
    public void addPOIItems(ArrayList<Shop> shops) {
        MapView mapView = getMapView();
        boolean flag = true;
        if (mapView != null) {
            for (Shop shop : shops) {
                if (mapView.findPOIItemByTag(shop.oid) == null) {
                    ShopMapPOIItem poiItem = new ShopMapPOIItem();
                    poiItem.setShop(shop);
                    poiItem.setItemName(shop.shopName);
                    poiItem.setMapPoint(MapPoint.mapPointWithGeoCoord(shop.latitude, shop.longitude));
                    poiItem.setMarkerType(MapPOIItem.MarkerType.CustomImage);
                    poiItem.setSelectedMarkerType(MapPOIItem.MarkerType.CustomImage);
                    poiItem.setShowAnimationType(MapPOIItem.ShowAnimationType.SpringFromGround);
                    poiItem.setCustomImageResourceId(R.drawable.obj_map_position);
                    poiItem.setCustomSelectedImageResourceId(R.drawable.obj_map_position_select);
                    poiItem.setCustomImageAutoscale(false);
                    poiItem.setShowCalloutBalloonOnTouch(false);
                    poiItem.setCustomImageAnchor(0.5f, 1.0f);
                    poiItem.setTag(shop.oid);
                    mapView.addPOIItem(poiItem);
                  /*
                    if (flag){
                        flag = !flag;
                        mapView.selectPOIItem(poiItem, true);
                    }
                    */
                    Shop selectShop = binding.getSelectShop();
                    if (selectShop != null && selectShop.oid == shop.oid) {
                        mapView.selectPOIItem(poiItem, false);
                    }
                }
            }
        }
    }

    @Override
    public void addPOIItemsPostSearch(ArrayList<Shop> shops) {
        MapView mapView = getMapView();
        boolean flag = true;
        if (mapView != null) {
            for (Shop shop : shops) {
                if (mapView.findPOIItemByTag(shop.oid) == null) {
                    ShopMapPOIItem poiItem = new ShopMapPOIItem();
                    poiItem.setShop(shop);
                    poiItem.setItemName(shop.shopName);
                    poiItem.setMapPoint(MapPoint.mapPointWithGeoCoord(shop.latitude, shop.longitude));
                    poiItem.setMarkerType(MapPOIItem.MarkerType.CustomImage);
                    poiItem.setSelectedMarkerType(MapPOIItem.MarkerType.CustomImage);
                    poiItem.setShowAnimationType(MapPOIItem.ShowAnimationType.SpringFromGround);
                    poiItem.setCustomImageResourceId(R.drawable.obj_map_position);
                    poiItem.setCustomSelectedImageResourceId(R.drawable.obj_map_position_select);
                    poiItem.setCustomImageAutoscale(false);
                    poiItem.setShowCalloutBalloonOnTouch(false);
                    poiItem.setCustomImageAnchor(0.5f, 1.0f);
                    poiItem.setTag(shop.oid);
                    mapView.addPOIItem(poiItem);

                    if (flag){
                        flag = !flag;
                        animDescription(shop,true);
                        mapView.selectPOIItem(poiItem, true);
                        mapView.setMapCenterPointAndZoomLevel(poiItem.getMapPoint(), 1, true);
                    }

                    Shop selectShop = binding.getSelectShop();
                    if (selectShop != null && selectShop.oid == shop.oid) {
                        mapView.selectPOIItem(poiItem, false);
                    }
                }
            }
        }
    }
    @Override
    public void enableTrackingUserLocation() {
        MapView mapView = getMapView();
        if (mapView != null) {
            showLoading();
            mapView.setCurrentLocationTrackingMode(MapView.CurrentLocationTrackingMode.TrackingModeOnWithoutHeading);
        }
    }

    @Override
    public void disableTrackingUserLocation() {
        MapView mapView = getMapView();
        boolean fineResult = ContextCompat.checkSelfPermission(activity, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED;
        boolean coarseResult = ContextCompat.checkSelfPermission(activity, ACCESS_COARSE_LOCATION) == PERMISSION_GRANTED;
        if (mapView != null && fineResult && coarseResult) {
            hideLoading();
            mapView.setCurrentLocationTrackingMode(MapView.CurrentLocationTrackingMode.TrackingModeOff);
        }
    }

    @Override
    public void animEnlargementMap() {
        MapView mapView = getMapView();
        if (mapView != null)
            mapView.zoomIn(true);
    }

    @Override
    public void animReductionMap() {
        MapView mapView = getMapView();
        if (mapView != null)
            mapView.zoomOut(true);
    }

    @Override
    public void callShopPhone() {
        if (binding != null && binding.getSelectShop() != null)
            activity.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + binding.getSelectShop().shopPhone)));
    }

    /* GETTER or SETTER */
    @Override
    public @Properties.ShopVisibleType int currentShopType() {
        int mapButton = (int) binding.shopListToolbarLayout.shopMap.getTag();
        return mapButton == MAP ? LIST : MAP;
    }

    @Override
    public int getCurrentCategoryPosition() {
        return binding.shopCategory.getSelectedTabPosition() - 1;
    }

    @Override
    public String getStreetName() {
        MDEBUG.debug("Street Name" + activity.getIntent().getStringExtra(STREET_NAME));
        return activity.getIntent().getStringExtra(STREET_NAME);
    }

    @Override
    public ShopListAdapter getShopListAdapter() {
        return (ShopListAdapter) binding.shopList.getAdapter();
    }

    @Override
    public boolean isCurrentListShow() {
        ShopListAdapter listAdapter = (ShopListAdapter) binding.shopList.getAdapter();
        return listAdapter.getObservableArrayList().size() > 0;
    }

    @Override
    public String[] getStreetStringArray() {
        return getStreetArray().toArray(new String[getStreetArray().size()]);
    }

    @Override
    public String getCurrentCategory(ArrayList<CategoryInfo> categoryInfos) {
        return getCurrentCategoryPosition() >= 0 ? categoryInfos.get(getCurrentCategoryPosition()).name : null;
    }

    @Override
    public MapPoint getSavePoint() {
        return savePoint;
    }

    @Override
    public int getSaveZoomLevel() {
        return saveZoomLevel;
    }

    @Override
    public CallbackManager getShareFacebookCallback() {
        ShareFragment fragment = (ShareFragment) findFragmentByTag(SHARE);
        return fragment != null ? fragment.getCallbackManager() : null;
    }

    @Override
    public MapView getMapView() {
        return binding.shopListMapLayout.findViewById(mapId);
    }

    @Override
    public MapPoint.GeoCoordinate getTopRightPoint() {
        return getMapView() != null ? getMapView().getMapPointBounds().topRight.getMapPointGeoCoord() : null;
    }

    @Override
    public MapPoint.GeoCoordinate getBottomLeftPoint() {
        return getMapView() != null ? getMapView().getMapPointBounds().bottomLeft.getMapPointGeoCoord() : null;
    }
}
