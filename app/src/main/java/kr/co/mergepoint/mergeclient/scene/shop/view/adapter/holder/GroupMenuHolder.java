package kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.databinding.ShopMenuGroupBinding;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class GroupMenuHolder extends GroupViewHolder {

    public ShopMenuGroupBinding shopMenuGroupBinding;
    private Context context;

    public GroupMenuHolder(ShopMenuGroupBinding shopMenuGroupBinding, Context context) {
        super(shopMenuGroupBinding.getRoot());
        this.shopMenuGroupBinding = shopMenuGroupBinding;
        this.context = context;
    }

    public void setGroupMenu(Menu group) {
        shopMenuGroupBinding.setGroup(group);
    }

    @Override
    public void expand() {
        shopMenuGroupBinding.menuGroupName.setTextColor(ContextCompat.getColor(context, R.color.point_red));
        shopMenuGroupBinding.groupState.setImageResource(R.drawable.icon_menu_close);
        shopMenuGroupBinding.groupState.invalidate();
    }

    @Override
    public void collapse() {
        shopMenuGroupBinding.menuGroupName.setTextColor(ContextCompat.getColor(context, R.color.gray_16));
        shopMenuGroupBinding.groupState.setImageResource(R.drawable.icon_menu_open);
        shopMenuGroupBinding.groupState.invalidate();
    }
}
