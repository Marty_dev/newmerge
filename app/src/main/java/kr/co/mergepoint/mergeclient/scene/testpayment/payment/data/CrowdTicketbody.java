package kr.co.mergepoint.mergeclient.scene.testpayment.payment.data;

/**
 * Created by Charny on 2018-07-23.
 */

public class CrowdTicketbody {
    long payShopRef;
    long crowdPayRef;
    long memberRef;
    long companyPoint;
    long ticketRef;
    long ticketCount;

    public CrowdTicketbody(long payShopRef, long crowdPayRef, long memberRef, long companyPoint, long ticketRef, long ticketCount) {
        this.payShopRef = payShopRef;
        this.crowdPayRef = crowdPayRef;
        this.memberRef = memberRef;
        this.companyPoint = companyPoint;
        this.ticketRef = ticketRef;
        this.ticketCount = ticketCount;
    }
}
