package kr.co.mergepoint.mergeclient.scene.shoplist.view.listener;

import android.view.View;

/**
 * Created by 1017sjg on 2017. 6. 28..
 */

public class CategoryListener implements View.OnClickListener {

    private CategoryTouch touch;

    public CategoryListener(CategoryTouch touch) {
        this.touch = touch;
    }

    public interface CategoryTouch {
        void onCategoryClick(View view);
    }

    @Override
    public void onClick(View v) {
        touch.onCategoryClick(v);
    }

}
