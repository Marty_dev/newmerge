package kr.co.mergepoint.mergeclient.scene.data.main;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jgson on 2018. 3. 10..
 */

public class Company {

    @SerializedName("oid")
    public int oid;

    @SerializedName("companyName")
    public String companyName;

    @SerializedName("ownerName")
    public String ownerName;
}
