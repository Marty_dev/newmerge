package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

/**
 * Created by 1017sjg on 2017. 6. 30..
 */

public class CustomCountView extends android.support.v7.widget.AppCompatTextView implements View.OnClickListener {

    public OnChangeCountListener countListener;
    public int maxCount;
    public int minCount = 1;
    public int optionSelectOid;
    public int menuPrice;

    public int afterCount;

    public interface OnChangeCountListener {
        void changeCount(CustomCountView view, boolean isPlus);
    }

    public CustomCountView(Context context) {
        super(context);
        init();
    }

    public CustomCountView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomCountView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        maxCount = NO_VALUE;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        ConstraintLayout parentLayout = (ConstraintLayout) getParent();
        parentLayout.findViewById(R.id.count_addition).setOnClickListener(this);
        parentLayout.findViewById(R.id.count_subtract).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.count_addition:
                upCount();
                break;
            case R.id.count_subtract:
                downCount();
                break;
            default:
                break;
        }
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        try {
            if (countListener != null) {
                int pre = Integer.parseInt(getText().toString());
                afterCount = Integer.parseInt(text.toString());
                countListener.changeCount(this, pre < afterCount);
            }
        } catch (NumberFormatException ignored) {}

        super.setText(text, type);
    }

    public void upCount() {
        if ((maxCount > NO_VALUE) && (currentCount() < maxCount))
            setText(String.valueOf((currentCount() + 1)));
    }

    public void downCount() {
        if (currentCount() > minCount)
            setText(String.valueOf(currentCount() - 1));
    }

    public int currentCount() {
        // TODO: java.lang.NumberFormatException: Invalid int: ""
        MDEBUG.debug("pre count : " + Integer.parseInt(getText().toString()));
        return Integer.parseInt(getText().toString());
    }
}
