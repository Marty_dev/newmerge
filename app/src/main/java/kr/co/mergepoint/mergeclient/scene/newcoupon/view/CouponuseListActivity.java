package kr.co.mergepoint.mergeclient.scene.newcoupon.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.NewCouponApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponUseInfo;
import kr.co.mergepoint.mergeclient.scene.newcoupon.adapter.CouponUseAdapter;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouponuseListActivity extends BaseActivity {

    BaseActivity mCon = this;
    @BindView(R.id.back_arrow)
    ImageView backArrow;

    @BindView(R.id.coupon_uselist)
    RecyclerView couponUselist;
    @BindView(R.id.coupon_leftprice_tv)
    TextView couponLeftpriceTv;
    private int oid;
    NewCouponApi API;
    CouponUseAdapter adapter;

    @Override
    protected void onActivityClick(View view) {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_couponuse_list);
        ButterKnife.bind(this);
        API = MergeApplication.getMergeAppComponent().getRetrofit().create(NewCouponApi.class);
        adapter = new CouponUseAdapter(mCon);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getData();
    }

    void getData() {
        Intent inte = getIntent();
        oid = inte.getIntExtra("oid", -1);

        MergeApplication.getMergeApplication().showLoading(mCon);
        API.getCouponUseList(oid).enqueue(getCallback());

    }


    Callback<ResponseObject<CouponUseInfo>> getCallback() {
        return new Callback<ResponseObject<CouponUseInfo>>() {
            @Override
            public void onResponse(Call<ResponseObject<CouponUseInfo>> call, Response<ResponseObject<CouponUseInfo>> response) {
                if (!response.body().isFailed()) {
                    adapter.setList(response.body().getObject().getCouponUseList());
                    couponLeftpriceTv.setText(MoneyForm(response.body().getObject().getRemainPrice()) + "원");

                    if (couponUselist.getAdapter() == null)
                        couponUselist.setAdapter(adapter);
                } else {
                    new MergeDialog.Builder(mCon).setCancelBtn(false).setContent(mCon.getResources().getString(R.string.retry)).build().show();
                }
                MergeApplication.getMergeApplication().hideLoading(mCon);

            }

            @Override
            public void onFailure(Call<ResponseObject<CouponUseInfo>> call, Throwable t) {
                MDEBUG.debug("Error!" + t.toString());
                new MergeDialog.Builder(mCon).setCancelBtn(false).setContent(mCon.getResources().getString(R.string.retry)).build().show();
                MergeApplication.getMergeApplication().hideLoading(mCon);

            }
        };
    }

    @OnClick(R.id.back_arrow)
    public void onViewClicked() {

        finish();
    }
}
