package kr.co.mergepoint.mergeclient.scene.enterprise.shop.view;

import com.facebook.CallbackManager;

import java.util.ArrayList;
import java.util.Calendar;

import kr.co.mergepoint.mergeclient.scene.data.basket.AddBasket;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.DailyMenuCategory;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.fragment.EnterpriseShopMenuFragment;

/**
 * Created by jgson on 2017. 12. 8..
 */

public interface EnterpriseShopViewInterface {

    /*페이지 열기*/
    void openPayment();

    void openShopShared(ShopDetail shopDetail);

    void openEnterpriseMenu(ArrayList<DailyMenuCategory> dailyMenuCategories, Calendar calendar);

    void openMenuDetail(ShopMenuDetail detail);

    void openPaymentResult();

    void openShoppingBasket();

    void openMapIntent(double latitude, double longitude);

    /*GETTER or SETTER*/
    AddBasket getSelectOptions() ;

    EnterpriseShopMenuFragment getShopMenuFragment();

    CallbackManager getShareFacebookCallback();

    boolean startShoppingBasket();

    /*ANIMATION*/
    void animBasket();

    /*ETC*/
    void callShopPhone();

    void onBackPressed();

    void changeDailyMenuDate(Calendar calendar);

    void changeDailyMenu(ArrayList<DailyMenuCategory> menuCategories, Calendar calendar);
}
