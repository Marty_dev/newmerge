package kr.co.mergepoint.mergeclient.scene.shop.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.RecyclerItemListener;
import kr.co.mergepoint.mergeclient.databinding.ShopDetailPreExperienceBinding;
import kr.co.mergepoint.mergeclient.scene.data.shop.PreExperience;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.PreExperienceGridAdapter;

/**
 * Created by jgson on 2017. 12. 7..
 */

public class PreExperienceFragment extends BaseFragment {

    private ArrayList<PreExperience> preExperiences;
    private RecyclerItemListener.RecyclerTouchListener touchListener;
    private int select;

    public static PreExperienceFragment newInstance(RecyclerItemListener.RecyclerTouchListener touchListener, ArrayList<PreExperience> preExperiences, int select) {
        Bundle args = new Bundle();
        PreExperienceFragment fragment = new PreExperienceFragment();
        fragment.preExperiences = preExperiences;
        fragment.touchListener = touchListener;
        fragment.select = select;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        touchListener.onClickItem(null, select);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ShopDetailPreExperienceBinding preExperienceBinding = DataBindingUtil.inflate(inflater, R.layout.shop_detail_pre_experience, container, false);
        preExperienceBinding.setOnClick(getRootActivity());
        preExperienceBinding.setTitle("Pre-experience");
        preExperienceBinding.setGridAdapter(new PreExperienceGridAdapter(preExperiences));
        preExperienceBinding.setGridTouch(touchListener);
        return preExperienceBinding.getRoot();
    }
}
