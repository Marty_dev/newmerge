package kr.co.mergepoint.mergeclient.api;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.main.EventDetail;
import kr.co.mergepoint.mergeclient.scene.data.main.EventInfo;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public interface EventApi {

    @POST("event/detail/{oid}")
    Call<ResponseObject<EventDetail>> requestEventDetail(@Path("oid") int oid);

    @POST("event/list")
    Call<ResponseObject<ArrayList<EventInfo>>> requestEventList();
}
