package kr.co.mergepoint.mergeclient.scene.testpayment.payment.data;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * User: Marty
 * Date: 2018-07-18
 * Time: 오전 11:39
 * Description:
 */
public class ShopPayment {
    @Expose
    @SerializedName("couponName")
    public String couponName;
    @Expose
    @SerializedName("couponPrice")
    public long couponPrice ;
    @Expose
    @SerializedName("payPrice")
    public long payPrice ;
    @Expose
    @SerializedName("shopCoupon")
    public List<ShopCoupon> shopCoupon;
    @Expose
    @SerializedName("crowdPay")
    public List<CrowdSimpleListdata> crowdPay;
    @Expose
    @SerializedName("myTicketName")
    public String myTicketName;
    @Expose
    @SerializedName("myHavePoints")
    public long myHavePoints;
    @Expose
    @SerializedName("myHaveCompanyPoints")
    public long myHavaCompanyPoints;
    @Expose
    @SerializedName("myHaveTicketList")
    public List<ShopTicket> myHaveTicketList;
    @Expose
    @SerializedName("myTicketCount")
    public int myTicketCount;
    @Expose
    @SerializedName("myTicketRef")
    public int myTicketRef;
    @Expose
    @SerializedName("myCompanyPoints")
    public int myCompanyPoints;
    @Expose
    @SerializedName("attendeeCount")
    public int attendeeCount;
    @Expose
    @SerializedName("crowdPayType")
    public boolean crowdPayType;
    @Expose
    @SerializedName("crowdPayRef")
    public int crowdPayRef;
    @Expose
    @SerializedName("couponRef")
    public int couponRef;
    @Expose
    @SerializedName("companyPoints")
    public int companyPoints;
    @Expose
    @SerializedName("pointPrice")
    public int pointPrice;
    @Expose
    @SerializedName("orderPrice")
    public int orderPrice;
    @Expose
    @SerializedName("shopType")
    public int shopType;
    @Expose
    @SerializedName("shopName")
    public String shopName;
    @Expose
    @SerializedName("shopRef")
    public int shopRef;
    @Expose
    @SerializedName("paymentRef")
    public int paymentRef;
    @Expose
    @SerializedName("memberRef")
    public int memberRef;
    @Expose
    @SerializedName("oid")
    public int oid;

    @Expose
    @SerializedName("allocatePoints")
    public int allocatePoints ;

    @Expose
    @SerializedName("allocateComPoints")
    public int allocateComPoints ;
    @SerializedName("usableCompanyPay")
    public boolean usableCompanyPay;
}
