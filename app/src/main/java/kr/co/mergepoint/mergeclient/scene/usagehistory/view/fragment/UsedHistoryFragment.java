package kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.UsedHistoryOrderBinding;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.newcoupon.view.Barcode_detailActivity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter.UsedMenuHistoryAdapter;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_ROLE;

/**
 * Created by 1017sjg on 2017. 7. 28..
 */

public class UsedHistoryFragment extends BaseFragment implements View.OnClickListener{

    public UsagePayment usagePayment;

    public static UsedHistoryFragment newInstance(UsagePayment usagePayment) {
        Bundle args = new Bundle();
        UsedHistoryFragment fragment = new UsedHistoryFragment();
        fragment.usagePayment = usagePayment;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        UsedHistoryOrderBinding orderBinding = DataBindingUtil.inflate(inflater, R.layout.used_history_order, container, false);
        orderBinding.setTitle(getString(R.string.used_history_title));
        orderBinding.setOnClick(this);
        orderBinding.setPayment(usagePayment);
        orderBinding.setBarcodeguide(getString(R.string.refund_guide_barcode));
        orderBinding.setAdapter(new UsedMenuHistoryAdapter(usagePayment.usedMenuList));
        Member member = MergeApplication.getMember();
        boolean isEnterpriseUser = member != null && member.getRoles().indexOf(ENTERPRISE_ROLE) >= 0;
//        orderBinding.setIsEnterpriseUser(isEnterpriseUser);
        MDEBUG.debug("usagePayment Approve Type : "  +  usagePayment.approveType);
        MDEBUG.debug("usagePayment Barcode : "  +  usagePayment.barCode);
        MDEBUG.debug("CompanyPoints : " +  usagePayment.companyPoints );
        orderBinding.setIsEnterpriseUser(isEnterpriseUser);






        return orderBinding.getRoot();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.barcode_detail){
            Intent inte = new Intent(rootActivity,Barcode_detailActivity.class);
            inte.putExtra("viewtype",3);
            inte.putExtra("title",usagePayment.shopName);
            inte.putExtra("barcode",usagePayment.barCode);
            startActivity(inte);


        }
        super.onClick(v);

    }
}
