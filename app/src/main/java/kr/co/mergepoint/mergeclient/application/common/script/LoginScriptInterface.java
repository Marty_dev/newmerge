package kr.co.mergepoint.mergeclient.application.common.script;

import android.webkit.JavascriptInterface;

/**
 * Created by jgson on 2017. 6. 2..
 */

public class LoginScriptInterface {

    private LoginScriptListener listener;

    public LoginScriptInterface(LoginScriptListener listener) {
        this.listener = listener;
    }

    @JavascriptInterface
    public void signup(String memberString) {
        // 회원가입 페이지로 이동 및 해당 페이지에 정보 넣어주기
        listener.signup(memberString);
    }

    @JavascriptInterface
    public void signin(boolean result, String memberString) {
        // 이미 가입되어 있는 회원으로 로그인 성공 후 메인으로 이동
        listener.signin(result, memberString);
    }

    @JavascriptInterface
    public void inputEmail(String instaId, String facebookId, String social) {
        listener.inputEmail(instaId, facebookId, social);
    }
}

