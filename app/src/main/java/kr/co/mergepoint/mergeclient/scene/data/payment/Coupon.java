package kr.co.mergepoint.mergeclient.scene.data.payment;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.application.common.DateModel;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class Coupon {
    @SerializedName("oid")
    public int oid;

    @SerializedName("shopName")
    public String shopName;

    @SerializedName("couponNum")
    public String couponNum;

    @SerializedName("couponName")
    public String couponName;

    @SerializedName("couponDescription")
    public String couponDescription;

    @SerializedName("issuedDateTime")
    public ArrayList<Integer> issuedDateTime;

    @SerializedName("expDate")
    public ArrayList<Integer> expDate;

    private int dday;
    public boolean used;
    public int selectShopRef;

    public int getDday() {
        return new DateModel().getCalculateDay(expDate);
    }

    public Coupon(int oid) {
        this.oid = oid;
    }
}
