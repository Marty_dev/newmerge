package kr.co.mergepoint.mergeclient.scene.data.menudetail;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.android.databinding.library.baseAdapters.BR;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 1017sjg on 2017. 6. 24..
 */

public class ShopOptionSelectMenu extends BaseObservable implements Parcelable {

    @SerializedName("oid")
    public int oid;

    @SerializedName("name")
    public String name;

    @SerializedName("price")
    public int price;

    @SerializedName("salePrice")
    public int salePrice;

    @SerializedName("soldout")
    public String soldout;

    @SerializedName("soldoutPeriod")
    public int soldoutPeriod;

    @SerializedName("comment")
    public String comment;

    @SerializedName("optionCount")
    public int count;

    @SerializedName("maxCount")
    public int maxCount;

    @Bindable
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
        notifyPropertyChanged(BR.price);
    }

    @Bindable
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
        notifyPropertyChanged(BR.count);
    }

    public ShopOptionSelectMenu(int price) {
        this.price = price;
    }

    private ShopOptionSelectMenu(Parcel in) {
        oid = in.readInt();
        name = in.readString();
        price = in.readInt();
        salePrice = in.readInt();
        soldout = in.readString();
        soldoutPeriod = in.readInt();
        comment = in.readString();
        count = in.readInt();
        maxCount = in.readInt();
    }

    public static final Parcelable.Creator<ShopOptionSelectMenu> CREATOR = new Parcelable.Creator<ShopOptionSelectMenu>() {
        @Override
        public ShopOptionSelectMenu createFromParcel(Parcel in) {
            return new ShopOptionSelectMenu(in);
        }

        @Override
        public ShopOptionSelectMenu[] newArray(int size) {
            return new ShopOptionSelectMenu[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(oid);
        dest.writeString(name);
        dest.writeInt(price);
        dest.writeInt(salePrice);
        dest.writeString(soldout);
        dest.writeInt(soldoutPeriod);
        dest.writeString(comment);
        dest.writeInt(count);
        dest.writeInt(maxCount);
    }
}
