package kr.co.mergepoint.mergeclient.scene.shoppingbasket.view.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.application.common.CustomCountView;
import kr.co.mergepoint.mergeclient.databinding.ShoppingBasketListItemBinding;
import kr.co.mergepoint.mergeclient.databinding.ShoppingBasketTotalBinding;
import kr.co.mergepoint.mergeclient.scene.data.basket.Basket;
import kr.co.mergepoint.mergeclient.scene.data.basket.BasketInShop;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.BasketActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_FOOTER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_ITEM;

/**
 * Created by 1017sjg on 2017. 6. 13..
 */

public class BasketListAdapter extends BasicListAdapter<BasicListHolder, BasketInShop> {

    private BasketActivity activity;
    private Basket basket;

    public Basket getBasket() {
        return basket;
    }

    public BasketListAdapter(Basket basket, BasketActivity activity) {
        super(basket.shopCart);
        this.basket = basket;
        this.activity = activity;
    }

    @Override
    public BasicListHolder setCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == LIST_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_basket_list_item, parent, false);
            ShoppingBasketListItemBinding itemBinding = DataBindingUtil.bind(view);

            return new BasicListHolder<ShoppingBasketListItemBinding, BasketInShop>(itemBinding) {
                @Override
                public void setDataBindingWithData(BasketInShop data) {
                    getDataBinding().setShopName(data.shopName);
                    getDataBinding().setShopInMenuAdapter(new ShopInMenuAdapter(BasketListAdapter.this, data, activity));
                }
            };

        } else if (viewType == LIST_TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_basket_total, parent, false);
            ShoppingBasketTotalBinding totalBinding = DataBindingUtil.bind(view);

            return new BasicListHolder<ShoppingBasketTotalBinding, Basket>(totalBinding) {
                @Override
                public void setDataBindingWithData(Basket data) {
                    getDataBinding().setBasket(data);
                    getDataBinding().setOnClick(activity);
                }
            };
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setBindViewHolder(BasicListHolder holder, int position) {
        if (holder.getDataBinding() instanceof ShoppingBasketListItemBinding) {
            holder.bind(getObservableArrayList().get(position));
            ((ShoppingBasketListItemBinding) holder.getDataBinding()).basketItemRoot.setBackgroundResource((position == getItemCount() - 2) ? android.R.color.transparent : R.drawable.bottom_line_dash_02);
        } else if (holder.getDataBinding() instanceof ShoppingBasketTotalBinding) {
            holder.bind(basket);
        }
    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position == getObservableArrayList().size() ? LIST_TYPE_FOOTER : LIST_TYPE_ITEM;
    }

    void addPrice(int price) {
        basket.setPayPrice(basket.getPayPrice() + price);
    }

    void substractPrice(int price) {
        basket.setPayPrice(basket.getPayPrice() - price);
        MDEBUG.debug("reset pay price");
    }

    // 페이먼트에 보낼 Check된 메뉴의 oid 걸러내기
    public ArrayList<Integer> getSelectMenuOid() {
        ArrayList<Integer> select = new ArrayList<>();
        for (BasketInShop shop: getObservableArrayList()) {
            for (ShopMenuDetail menuDetail: shop.menu) {
                if (menuDetail.isCheck)
                    select.add(menuDetail.oid);
            }
        }
        return select;
    }

    public void deleteMenu(View view, int price) {
        ShoppingBasketListItemBinding item = (ShoppingBasketListItemBinding) getViewDataBinding(view);
        MDEBUG.debug("menu 2");

        MDEBUG.debug(item == null ? "item is null" : item.basketMenu == null ? "basket is null" : "WTF?");
        if (item != null && item.basketMenu != null) {
            MDEBUG.debug("menu 1");
            ShopInMenuAdapter menuAdapter = (ShopInMenuAdapter) item.basketMenu.getAdapter();
            boolean isCheckble = menuAdapter.getCheckable(view);
            int position;
            if (menuAdapter.deleteItem(view)) {
                MDEBUG.debug("menu deleted");
                position = getPosition(view);
                getObservableArrayList().remove(position);
                notifyItemRemoved(position);

                if (getObservableArrayList().size() == 0){
                    activity.finish();
                }
            }
            MDEBUG.debug("menuAdapter.getCheckable(view)" + isCheckble);
            if (isCheckble)
                substractPrice(price);
        }
    }

    public void checkMenu(View view, boolean isChecked) {
        ShopMenuDetail menuDetail = getShopMenuDetail(view);
        if (menuDetail != null) {
            if (isChecked) {
                addPrice((menuDetail.payPrice * menuDetail.count));
            } else {
                substractPrice((menuDetail.payPrice * menuDetail.count));
            }
            menuDetail.setCheck(isChecked);
        }
    }

    // View로 ShopMenuDetail를 서치
    public ShopMenuDetail getShopMenuDetail(View view) {
        ShoppingBasketListItemBinding itemBinding = getItemBinding(view);
        return itemBinding != null ? itemBinding.getShopInMenuAdapter().getShopMenuDetail(view) : null;
    }

    public CustomCountView getCountView(View view) {
        ShoppingBasketListItemBinding itemBinding = getItemBinding(view);
        return itemBinding != null ? itemBinding.getShopInMenuAdapter().getCountView(view) : null;
    }

    public void setMenuCount(View view, int count) {
        ShoppingBasketListItemBinding itemBinding = getItemBinding(view);
        if (itemBinding != null)
            itemBinding.getShopInMenuAdapter().setMenuCount(view, count);
    }

    private ShoppingBasketListItemBinding getItemBinding(View view) {
        ViewDataBinding viewDataBinding = getViewDataBinding(view);
        return viewDataBinding instanceof ShoppingBasketListItemBinding ? (ShoppingBasketListItemBinding) viewDataBinding : null;
    }
}
