package kr.co.mergepoint.mergeclient.scene.franchise.data;

import java.util.ArrayList;

/**
 * User: Marty
 * Date: 2018-10-02
 * Time: 오전 10:19
 * Description:
 */
public class FranchisesShop {
    public String imgurl;
    public String name;
    public int orderCnt;
    public boolean isMarking;
    public String shopTime;
    public int heartnum;
    public int marknum;
    public ArrayList<String> Categorys;
    public ArrayList<String> Tags;
    public int distance;
    public int oid;



    public FranchisesShop() {
    }

    public FranchisesShop(String name) {
        this.name = name;
    }

    public FranchisesShop(String imgurl, String name) {
        this.imgurl = imgurl;
        this.name = name;
    }

    public FranchisesShop(String imgurl, String name, int orderCnt, boolean isMarking, String shopTime, int heartnum, int marknum, ArrayList<String> categorys, ArrayList<String> tags, int distance, int oid) {
        this.imgurl = imgurl;
        this.name = name;
        this.orderCnt = orderCnt;
        this.isMarking = isMarking;
        this.shopTime = shopTime;
        this.heartnum = heartnum;
        this.marknum = marknum;
        Categorys = categorys;
        Tags = tags;
        this.distance = distance;
        this.oid = oid;
    }
}
