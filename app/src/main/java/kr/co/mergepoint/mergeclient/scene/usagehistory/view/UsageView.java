package kr.co.mergepoint.mergeclient.scene.usagehistory.view;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.databinding.UsageHistoryBinding;
import kr.co.mergepoint.mergeclient.scene.data.history.ShopHistoryPayment;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.scene.data.main.UsageFilter;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.presenter.UsagePresenter;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter.UsageHistoryListAdapter;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.OrderHistoryFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.ResultFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.ShopHistoryFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.UsedHistoryFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.UsingFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.UsingFragmentv2;

import static kr.co.mergepoint.mergeclient.application.common.Properties.APPROVE_COMPLETE_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ORDER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAYMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAYMENT_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_CANCEL_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_POINT_REFUND_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USAGE_ORDER_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USAGE_SHOP_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USING_MENU_TAG;

/**
 * Created by 1017sjg on 2017. 9. 16..
 */

public class UsageView extends BaseView<UsageActivity, UsagePresenter, UsageHistoryBinding> implements UsageViewInterface {

    public UsageFilter usageFilter;
    private UsageHistoryListAdapter listAdapter = new UsageHistoryListAdapter();
    public UsageView(UsageActivity activity, int layout) {
        super(activity, layout);
        this.usageFilter = new UsageFilter();
    }

    @Override
    protected Toolbar setToolbar() {
        return binding.toolbarBinding.basicToolbar;
    }

    @Override
    public void setPaymentPagerData(ArrayList<UsagePayment> usagePayments, int position) {

        listAdapter.setParams(usagePayments, usageFilter.isPaymentTab());
        binding.getPagerAdapter().setListAdapter(position, listAdapter, activity);
    }

    @Override
    public void openHistory(UsagePayment usagePayment) {
        boolean isPayment = Objects.equals(usageFilter.getTabType(), PAYMENT);
        boolean isOrder = usageFilter.getListType() == ORDER;
        String tag = isOrder ? USAGE_ORDER_TAG : USAGE_SHOP_TAG;
        Fragment fragment;
        if (isPayment) {
            fragment = isOrder ? OrderHistoryFragment.newInstance(usagePayment.oid) : ShopHistoryFragment.newInstance(usagePayment.shopRef,usagePayment.consummerRef);
        } else {
            fragment = UsedHistoryFragment.newInstance(usagePayment);
        }
        openFragment(RIGHT_LEFT, R.id.usage_layout, fragment, tag);
    }

    @Override
    public void openShopHistory(int oid) {
        openFragment(RIGHT_LEFT, R.id.usage_layout, ShopHistoryFragment.newInstance(oid), USAGE_SHOP_TAG);
    }
    public void openShopHistory(int oid,int ref) {
        openFragment(RIGHT_LEFT, R.id.usage_layout, ShopHistoryFragment.newInstance(oid,ref), USAGE_SHOP_TAG);
    }
    @Override
    public int getPayCancelOid() {
        OrderHistoryFragment orderHistoryFragment = getOrderHistoryFragment();
        return orderHistoryFragment != null && orderHistoryFragment.orderBinding != null ? orderHistoryFragment.orderBinding.getPayment().oid : NO_VALUE;
    }

    @Override
    public void openPayCancelFragment(boolean isPayCancelStr) {
        String contentStr = isPayCancelStr ? activity.getString(R.string.pay_cancel_text) : activity.getString(R.string.point_refund_text);
        String tag = isPayCancelStr ? PAY_CANCEL_TAG : PAY_POINT_REFUND_TAG;
        removeFragment(findFragmentByTag(USAGE_ORDER_TAG));
        openFragment(RIGHT_LEFT, R.id.usage_layout, ResultFragment.newInstance(getString(R.string.usage_history_title), contentStr), tag);
    }

    @Override
    public void openApproveResultFragment() {
        ResultFragment fragment = ResultFragment.newInstance(getString(R.string.use_approve_complete_title), getString(R.string.use_approve_complete_content));
        openFragment(RIGHT_LEFT, R.id.usage_layout, fragment, USING_MENU_TAG, APPROVE_COMPLETE_TAG);
    }

    @Override
    public void showUsageMenuAlert(MergeDialog.OnDialogClick sendMenu) {
        ShopHistoryFragment shopHistoryFragment = (ShopHistoryFragment) findFragmentByTag(USAGE_SHOP_TAG);
        int menuSize = shopHistoryFragment.getUseMenu().size();
        if (menuSize > 0) {
            new MergeDialog.Builder(activity)
                    .setContent(activity.getString(R.string.order_menu_text, menuSize))
                    .setConfirmString("확인").setConfirmClick(sendMenu).build().show();
        } else {
            new MergeDialog.Builder(activity)
                    .setContent(R.string.select_menu_text).setCancelBtn(false).build().show();
        }
    }

    @Override
    public void updateShopHistory() {
        ShopHistoryFragment shopHistoryFragment = getShopHistoryFragment();
        if (shopHistoryFragment != null)
            shopHistoryFragment.getShopDetailHistory();
    }

    @Override
    public void updateOrderHistory() {
        OrderHistoryFragment orderHistoryFragment = getOrderHistoryFragment();
        if (orderHistoryFragment != null)
            orderHistoryFragment.getOrderDetailHistory();
    }

    @Override
    public void setApproveFlag(boolean isApprove) {
        UsingFragmentv2 usingFragment = getUsingFragment();
        if (usingFragment != null)
            usingFragment.setApprove(isApprove);
    }

    @Override
    public void resetPinCodeView() {
        UsingFragmentv2 usingFragment = getUsingFragment();
        if (usingFragment != null)
            usingFragment.resetPinCodeView();
    }

    @Override
    public void getIntent() {
        Intent intent = activity.getIntent();
        if (intent != null) {
            ShopHistoryPayment shopHistoryPayment = intent.getParcelableExtra(PAYMENT_INFO);
            if (shopHistoryPayment != null)
                openFragment(RIGHT_LEFT, R.id.usage_layout, ShopHistoryFragment.newInstance(shopHistoryPayment.shopRef,shopHistoryPayment.consummerRef), USAGE_SHOP_TAG);
        }
    }

    @Override
    public UsingFragmentv2 getUsingFragment() {
        return (UsingFragmentv2) findFragmentByTag(USING_MENU_TAG);
    }

    @Override
    public OrderHistoryFragment getOrderHistoryFragment() {
        return (OrderHistoryFragment) findFragmentByTag(USAGE_ORDER_TAG);
    }

    @Override
    public ShopHistoryFragment getShopHistoryFragment() {
        return (ShopHistoryFragment) findFragmentByTag(USAGE_SHOP_TAG);
    }

    @Override
    public int getRejectOid() {
        return getUsingFragment().getOid();
    }

    @Override
    public Map<String, Object> getApproveInfo() {
        UsingFragmentv2 usingFragment = (UsingFragmentv2) findFragmentByTag(USING_MENU_TAG);
        Map<String, Object> info = usingFragment.getApproveInfo();
        boolean flag = false;
        /*if (info.get("pin") != null){
            String pin = (String) info.get("pin");
            if (pin.isEmpty()){
                //showAlert("PIN 번호를 제대로 입력해주세요.");
            }
            //flag = !pin.isEmpty();
        }else{
            flag = true;
        }*/

        flag = true;

        setApproveFlag(flag);
        return info;
    }
}
