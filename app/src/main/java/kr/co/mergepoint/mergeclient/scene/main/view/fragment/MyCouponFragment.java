package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.UserApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.MyCouponBinding;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.CouponAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 1017sjg on 2017. 8. 7..
 */

public class MyCouponFragment extends BaseFragment {


    private ArrayList<Coupon> coupons;

    public static MyCouponFragment newInstance(ArrayList<Coupon> coupons) {
        Bundle args = new Bundle();
        MyCouponFragment fragment = new MyCouponFragment();
        fragment.coupons = coupons;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MyCouponBinding myCouponBinding = DataBindingUtil.inflate(inflater, R.layout.my_coupon, container, false);
        myCouponBinding.setTitle(getString(R.string.my_coupons));
        myCouponBinding.setOnClick(this);
        myCouponBinding.couponList.setVisibility(coupons.size() > 0 ? View.VISIBLE : View.GONE);
        myCouponBinding.noCoupon.setVisibility(coupons.size() > 0 ? View.GONE : View.VISIBLE);
        myCouponBinding.setListAdapter(new CouponAdapter(coupons));
        return myCouponBinding.getRoot();
    }
}
