package kr.co.mergepoint.mergeclient.scene.testpayment.payment.data;

import com.google.gson.annotations.SerializedName;

/**
 * User: Marty
 * Date: 2018-07-25
 * Time: 오후 2:11
 * Description:
 */
public class PreUseRequest {
    @SerializedName("payMenu")
    int paymenuoid;
    @SerializedName("useCount")
    int useCount;
}
