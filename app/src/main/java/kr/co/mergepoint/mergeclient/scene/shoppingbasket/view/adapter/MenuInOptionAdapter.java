package kr.co.mergepoint.mergeclient.scene.shoppingbasket.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.ShoppingBasketOptionItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionMenu;

/**
 * Created by 1017sjg on 2017. 7. 3..
 */

public class MenuInOptionAdapter extends BasicListAdapter<BasicListHolder<ShoppingBasketOptionItemBinding, ShopOptionMenu>, ShopOptionMenu> {

    public MenuInOptionAdapter(ArrayList<ShopOptionMenu> optionMenu) {
        super(optionMenu);
    }

    @Override
    public BasicListHolder<ShoppingBasketOptionItemBinding, ShopOptionMenu> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_basket_option_item, parent, false);
        ShoppingBasketOptionItemBinding binding = DataBindingUtil.bind(view);

        return new BasicListHolder<ShoppingBasketOptionItemBinding, ShopOptionMenu>(binding) {
            @Override
            public void setDataBindingWithData(ShopOptionMenu data) {
                getDataBinding().setOptionMenu(data);
                getDataBinding().setOptionInSelectOptionAdapter(new OptionInSelectOptionAdapter(data));
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<ShoppingBasketOptionItemBinding, ShopOptionMenu> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
