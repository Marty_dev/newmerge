package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import kr.co.mergepoint.mergeclient.R;

/**
 * Created by 1017sjg on 2017. 7. 6..
 */

public class CustomCheckButton extends BaseCheckButton {

    public int selectOptionOid;

    public CustomCheckButton(Context context) {
        super(context);
    }

    public CustomCheckButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomCheckButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        setTextColor(ContextCompat.getColor(getContext(), checked ? R.color.point_red : R.color.gray_12));
    }
}
