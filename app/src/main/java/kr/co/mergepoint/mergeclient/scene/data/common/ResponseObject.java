package kr.co.mergepoint.mergeclient.scene.data.common;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 1017sjg on 2017. 8. 3..
 */

public class ResponseObject<T> {
    @SerializedName("object")
    private T object;

    @SerializedName("code")
    private String code;

    @SerializedName("message")
    private String message;

    @SerializedName("source")
    private String source;

    @SerializedName("failed")
    private boolean failed;

    public T getObject() {
        return object;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public boolean isFailed() {
        return failed;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
