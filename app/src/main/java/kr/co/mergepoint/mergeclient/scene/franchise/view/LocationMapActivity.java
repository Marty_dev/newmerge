package kr.co.mergepoint.mergeclient.scene.franchise.view;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapReverseGeoCoder;
import net.daum.mf.map.api.MapView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.co.mergepoint.mergeclient.BuildConfig;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.MergeActivity;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

public class LocationMapActivity extends MergeActivity implements  MapView.MapViewEventListener,MapReverseGeoCoder.ReverseGeoCodingResultListener{

    @BindView(R.id.back_close)
    TextView backClose;
    @BindView(R.id.fran_current_location_text)
    TextView franCurrentLocationText;
    @BindView(R.id.fran_location_select)
    TextView franLocationSelect;
    @BindView(R.id.location_mapview)
    RelativeLayout locationMapview;
    MapView mapView;
    MapPoint savePoint;
    public static final int MAP_SUCCESS = 1010;
    public static final int MAP_FAILED = 1111;
    public static final String IS_ADDR= "isAddrSet";
    public static final String ADDRESS= "address";
    public static final String LAT = "latitude";
    public static final String LON = "longitude";

    private final int IntegerNULL = -123456;
    private Geocoder geocoder;
    int saveZoom  = IntegerNULL; // null 대용
    private String currentAddr = null;
    private boolean isAddrSet = false;

    int mapid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_map);
        ButterKnife.bind(this);
        geocoder = new Geocoder(this);
        MapView mapView = new MapView(this);
        mapid = View.generateViewId();
        mapView.setId(mapid);

        mapView.setMapViewEventListener(this);
        locationMapview.addView(mapView,0);
        getCurrentData();
    }


    private void getCurrentData(){
        Intent data = getIntent();

        if (data.getExtras() != null){
            double lat,lon;
            lat = data.getDoubleExtra(LAT,-1.0f);
            lon = data.getDoubleExtra(LON,-1.0f);
            if (lat != -1.0f && lon != -1.0f){
                savePoint = MapPoint.mapPointWithGeoCoord(lat,lon);
                mapView.setMapCenterPoint(savePoint,false);

            }
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
//
//        MapView oldmap = locationMapview.findViewById(mapid);
//        if (oldmap != null){
//            locationMapview.removeView(oldmap);
//        }

        if (savePoint != null && saveZoom != IntegerNULL){
            MapView map = locationMapview.findViewById(mapid);
            if (map != null){
                map.setMapCenterPoint(savePoint,false);
                map.setZoomLevel(saveZoom,false);
            }
        }
    }
    @OnClick({R.id.back_close, R.id.fran_location_select})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_close:
                setResultwithFinish(null,MAP_FAILED);
                break;
            case R.id.fran_location_select:
                setResultwithFinish(savePoint,MAP_SUCCESS);
                break;
        }
    }
    private void setResultwithFinish(MapPoint endPoint , int result){
        Intent inte = getIntent();
        if (endPoint != null){
            inte.putExtra(ADDRESS ,currentAddr);
            inte.putExtra(IS_ADDR, isAddrSet);
            inte.putExtra(LAT ,endPoint.getMapPointGeoCoord().latitude);
            inte.putExtra(LON , endPoint.getMapPointGeoCoord().longitude);
        }
        setResult(result,inte);
        finish();
    }

    void getAddress(MapPoint savePoint){
        String apiKey = BuildConfig.BUILD_TYPE.equals("debug") ? getString(R.string.kakao_key_debug) : getString(R.string.kakao_key);
        MapReverseGeoCoder reverseGeoCoder = new MapReverseGeoCoder(apiKey, savePoint, this, this);
        reverseGeoCoder.startFindingAddress();
    }
    @Override
    public void onReverseGeoCoderFoundAddress(MapReverseGeoCoder mapReverseGeoCoder, String addressString) {
        // 주소를 찾은 경우.
        franCurrentLocationText.setText(addressString);
        currentAddr = addressString;
        isAddrSet = true;

    }

    @Override
    public void onReverseGeoCoderFailedToFindAddress(MapReverseGeoCoder mapReverseGeoCoder) {
        // 호출에 실패한 경우.
        franCurrentLocationText.setText( "주소 정보를 가져올수없습니다.");
        currentAddr = null;
        isAddrSet = false;

    }
    @Override
    public void onMapViewInitialized(MapView mapView) {
        savePoint = mapView.getMapCenterPoint();
        saveZoom = mapView.getZoomLevel();
        getAddress(mapView.getMapCenterPoint());
    }
    @Override
    public void onMapViewMoveFinished(MapView mapView, MapPoint mapPoint) {
        savePoint = mapView.getMapCenterPoint();
        saveZoom = mapView.getZoomLevel();
        getAddress(mapView.getMapCenterPoint());
    }

    @Override
    public void onMapViewCenterPointMoved(MapView mapView, MapPoint mapPoint) {
    }
    @Override
    public void onMapViewZoomLevelChanged(MapView mapView, int i) {
    }
    @Override
    public void onMapViewSingleTapped(MapView mapView, MapPoint mapPoint) {
    }
    @Override
    public void onMapViewDoubleTapped(MapView mapView, MapPoint mapPoint) {
    }
    @Override
    public void onMapViewLongPressed(MapView mapView, MapPoint mapPoint) {
    }
    @Override
    public void onMapViewDragStarted(MapView mapView, MapPoint mapPoint) {
    }
    @Override
    public void onMapViewDragEnded(MapView mapView, MapPoint mapPoint) {
    }


}
