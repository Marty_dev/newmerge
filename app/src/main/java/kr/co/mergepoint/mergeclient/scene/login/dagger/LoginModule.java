package kr.co.mergepoint.mergeclient.scene.login.dagger;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.login.LoginActivity;
import kr.co.mergepoint.mergeclient.scene.login.model.LoginModel;
import kr.co.mergepoint.mergeclient.scene.login.presenter.LoginPresenter;
import kr.co.mergepoint.mergeclient.scene.login.presenter.callback.LoginCallback;
import kr.co.mergepoint.mergeclient.scene.login.view.LoginView;

/**
 * Created by jgson on 2017. 5. 31..
 */

@Module
public class LoginModule {

    private LoginActivity loginActivity;

    public LoginModule(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
    }

    @Provides
    @LoginScope
    LoginView provideView() { return new LoginView(loginActivity, R.layout.login); }

    @Provides
    @LoginScope
    LoginModel provideModel() { return new LoginModel(); }

    @Provides
    @LoginScope
    LoginPresenter providePresent(LoginView view, LoginModel model) {
        return new LoginPresenter(view, model, new LoginCallback(view, model));
    }
}
