package kr.co.mergepoint.mergeclient.scene.data.splash;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.BR;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.main.Banner;
import kr.co.mergepoint.mergeclient.scene.data.main.MergesPick;
import kr.co.mergepoint.mergeclient.scene.data.main.Notice;
import kr.co.mergepoint.mergeclient.scene.data.menu.EnterpriseNotice;

/**
 * Created by 1017sjg on 2017. 6. 14..
 */

public class InitialInfo extends BaseObservable implements Parcelable {

    @SerializedName("version")
    private String version;

    @SerializedName("signin")
    private boolean signin;

    @SerializedName("member")
    private Member member;

    @SerializedName("street")
    private ArrayList<StreetInfo> street;

    @SerializedName("category")
    private ArrayList<CategoryInfo> category;

    @SerializedName("banner")
    private ArrayList<Banner> banner;

    @SerializedName("notice")
    private ArrayList<Notice> notice;

    @SerializedName("mergespick")
    private ArrayList<MergesPick> mergespick;

    @SerializedName("comNotice")
    private EnterpriseNotice comNotice;

    @SerializedName("deptNotice")
    private EnterpriseNotice deptNotice;

    @Bindable
    public boolean isSignin() {
        return signin;
    }

    public void setSignin(boolean signin) {
        this.signin = signin;
        notifyPropertyChanged(BR.signin);
    }

    @Bindable
    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
        notifyPropertyChanged(BR.member);
    }

    @Bindable
    public ArrayList<StreetInfo> getStreet() {
        return street;
    }

    public void setStreet(ArrayList<StreetInfo> street) {
        this.street = street;
        notifyPropertyChanged(BR.street);
    }

    @Bindable
    public ArrayList<CategoryInfo> getCategory() {
        return category;
    }

    public void setCategory(ArrayList<CategoryInfo> category) {
        this.category = category;
        notifyPropertyChanged(BR.category);
    }

    public ArrayList<String> getStreetName() {
        ArrayList<String> arrayList = new ArrayList<>();
        for (StreetInfo info : street) {
            arrayList.add(info.getName());
        }
        return arrayList;
    }

    public StreetInfo getStreet(String streetName) {
        for (StreetInfo str: street) {
            if (Objects.equals(str.getName(), streetName)) {
                return str;
            }
        }
        return null;
    }

    public boolean isLoginState() {
        return getMember() != null && isSignin();
    }

    public void setLoginState(Member member, boolean signin) {
        setMember(member);
        setSignin(signin);
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public ArrayList<Banner> getBanner() {
        return banner;
    }

    public ArrayList<Notice> getNotice() {
        return notice;
    }

    public ArrayList<MergesPick> getMergespick() {
        return mergespick;
    }

    public EnterpriseNotice getComNotice() {
        return comNotice;
    }

    public EnterpriseNotice getDeptNotice() {
        return deptNotice;
    }

    public static final Creator<InitialInfo> CREATOR = new Creator<InitialInfo>() {
        @Override
        public InitialInfo createFromParcel(Parcel in) {
            return new InitialInfo(in);
        }

        @Override
        public InitialInfo[] newArray(int size) {
            return new InitialInfo[size];
        }
    };

    private InitialInfo(Parcel in) {
        signin = in.readInt() == 1;
        member = in.readParcelable(Member.class.getClassLoader());
        in.readTypedList(street, StreetInfo.CREATOR);
        in.readTypedList(category, CategoryInfo.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(signin ? 1 : 0);
        dest.writeParcelable(member, flags);
        dest.writeTypedList(street);
        dest.writeTypedList(category);
    }
}
