package kr.co.mergepoint.mergeclient.scene.data.basket;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 6. 28..
 */

public class AddBasket implements Parcelable {
    public int shopRef;
    public int menuRef;
    public int menuCount;
    public ArrayList<OptionMenu> optionMenus;
    public int consummerRef;

    public AddBasket(int shopRef, int menuRef, int menuCount, ArrayList<OptionMenu> optionMenus) {
        this.shopRef = shopRef;
        this.menuRef = menuRef;
        this.menuCount = menuCount;
        this.optionMenus = optionMenus;
    }

    public AddBasket(int shopRef, int menuRef, int menuCount, ArrayList<OptionMenu> optionMenus, int consummerRef) {
        this.shopRef = shopRef;
        this.menuRef = menuRef;
        this.menuCount = menuCount;
        this.optionMenus = optionMenus;
        this.consummerRef = consummerRef;
    }

    private AddBasket(Parcel in) {
        shopRef = in.readInt();
        menuRef = in.readInt();
        menuCount = in.readInt();
        optionMenus = new ArrayList<>();
        in.readTypedList(optionMenus, OptionMenu.CREATOR);
        consummerRef = in.readInt();
    }

    public static final Creator<AddBasket> CREATOR = new Creator<AddBasket>() {
        @Override
        public AddBasket createFromParcel(Parcel in) {
            return new AddBasket(in);
        }

        @Override
        public AddBasket[] newArray(int size) {
            return new AddBasket[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(shopRef);
        dest.writeInt(menuRef);
        dest.writeInt(menuCount);
        dest.writeTypedList(optionMenus);
        dest.writeInt(consummerRef);
    }
}
