package kr.co.mergepoint.mergeclient.scene.enterprise.shop.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.EnterpriseShopActivity;

/**
 * Created by jgson on 2017. 6. 5..
 */

@EnterpriseShopScope
@Component(modules = EnterpriseShopModule.class)
public interface EnterpriseShopComponent {
    void inject(EnterpriseShopActivity activity);
}
