package kr.co.mergepoint.mergeclient.scene.data.payment;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;

/**
 * Created by 1017sjg on 2017. 7. 10..
 */

public class PaymentMenuGroup extends ExpandableGroup<PaymentMenuChild> {

    private int oid;
    private int menuPrice;
    private int count;

    public PaymentMenuGroup(ShopMenuDetail menuDetail, List<PaymentMenuChild> items) {
        super(menuDetail.name, items);
        this.oid = menuDetail.oid;
        this.menuPrice = menuDetail.payPrice;
        this.count = menuDetail.count;
    }

    public int getOid() {
        return oid;
    }

    public int getMenuPrice() {
        return menuPrice;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaymentMenuGroup)) return false;

        PaymentMenuGroup menu = (PaymentMenuGroup) o;
        return Objects.equals(getTitle(), menu.getTitle());
    }
}
