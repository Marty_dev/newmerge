package kr.co.mergepoint.mergeclient.scene.franchise.view;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.widget.ImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.StreetApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.MergeActivity;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.application.common.MergeCallbackV2;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.franchise.adapter.FranchiseAdapter;
import kr.co.mergepoint.mergeclient.scene.franchise.data.Franchises;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

public class FranchiseListActivity extends MergeActivity {

    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.franchise_list)
    RecyclerView franchiseList;

    FranchiseAdapter adapter;

    MergeCallbackV2<ResponseObject<ArrayList<Franchises>>> callback;
    StreetApi API;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_franchise_list);
        ButterKnife.bind(this);
        franchiseList.setLayoutManager(new GridLayoutManager(this,3));
        adapter = new FranchiseAdapter(this);
        API = MergeApplication.getMergeAppComponent().getRetrofit().create(StreetApi.class);
        initData();
    }


    private void initData(){
        franchiseList.setAdapter(adapter);
        settingCallback();
        API.getFranchisesList().enqueue(callback);

    }

    private void settingCallback(){
        callback = new MergeCallbackV2<ResponseObject<ArrayList<Franchises>>> (BASE, new MergeCallbackV2.MergeCall<ResponseObject<ArrayList<Franchises>>> () {
            @Override
            public void onResponse(Call<ResponseObject<ArrayList<Franchises>>>  call, Response<ResponseObject<ArrayList<Franchises>>>  response) {
                MDEBUG.debug("is Success");
                if (response.isSuccessful()){
                    ArrayList<Franchises> items = response.body().getObject();
                    adapter.arrayList.addAll(items);
                    adapter.notifyDataSetChanged();
                }else{
                    Snackbar.make(findViewById(android.R.id.content),R.string.retry , Snackbar.LENGTH_SHORT).show();

                }
            }
        });
    }



    @OnClick(R.id.back_arrow)
    public void onViewClicked() {
        finish();
    }
}
