package kr.co.mergepoint.mergeclient.application;


import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatDialog;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.twitter.sdk.android.core.Twitter;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import io.fabric.sdk.android.Fabric;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.application.common.Utility;
import kr.co.mergepoint.mergeclient.application.module.MergeModule;
import kr.co.mergepoint.mergeclient.application.module.NetworkModule;
import kr.co.mergepoint.mergeclient.application.module.UtilityModule;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;
import kr.co.mergepoint.mergeclient.scene.data.splash.StreetInfo;
import kr.co.mergepoint.mergeclient.scene.newcoupon.BarcodeGen;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * Created by jgson on 2017. 5. 30..
 */

public class MergeApplication extends Application implements Application.ActivityLifecycleCallbacks {

    private static MergeApplication mergeApplication;
    private static MergeAppComponent mergeAppComponent;
    private static InitialInfo initialInfo;
    private AppCompatDialog loadingDialog;
    public static boolean _isAppActive = false;

    // 결제권
    public static boolean isBarcodeforPay = false;
    // 금액권
    public static boolean isBarcodeforMoney = false;
    // 바코드 디테일
    public static boolean isBarcodeforUnion = false;

    public String ADID_KEY = "";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mergeApplication = this;
        /* Fresco 초기화 및 캐시 클리어 */
        Fresco.initialize(this);
        Fresco.getImagePipeline().clearCaches();
        /* Twitter 초기화 */
        Twitter.initialize(this);
        /* Fabric 초기화 */
        Fabric.with(this, new Crashlytics());
        /* Firebase의 토픽을 'all'로 설정 */
        FirebaseMessaging.getInstance().subscribeToTopic("all");
        /* Dagger 초기화 */
        initMergeComponent();
        // 디버깅용 activity life cycle Log  Activate
        registerActivityLifecycleCallbacks(this);
    }

    private void initMergeComponent() {
        mergeAppComponent = DaggerMergeAppComponent.builder()
                .mergeModule(new MergeModule(this))
                .networkModule(new NetworkModule(this))
                .utilityModule(new UtilityModule(this))
                .build();
    }

    public static MergeApplication getMergeApplication() {
        return mergeApplication;
    }

    public static MergeAppComponent getMergeAppComponent() {
        return mergeAppComponent;
    }

    public static Utility getUtility() {
        return getMergeAppComponent().getUtility();
    }

    public static Member getMember() {
        return initialInfo != null && initialInfo.getMember() != null ? initialInfo.getMember() : null;
    }

    public static InitialInfo getInitialInfo() {
        return initialInfo;
    }

    public static void setInitialInfo(InitialInfo initialInfo) {
        MergeApplication.initialInfo = initialInfo;
    }

    public static boolean isLoginState() {
        return getInitialInfo() != null && getInitialInfo().isLoginState();
    }

    public static String getStreetName(String streetCode) {
        StreetInfo streetInfo = null;
        if (getInitialInfo() != null) {
            ArrayList<StreetInfo> streets = getInitialInfo().getStreet();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                List<StreetInfo> filterStreet = streets.stream().filter((StreetInfo info) -> Objects.equals(info.getCode(), streetCode)).collect(Collectors.toList());
                streetInfo = filterStreet.size() > 0 ? filterStreet.get(0) : null;
            } else {
                for (StreetInfo info : streets) {
                    if (Objects.equals(info.getCode(), streetCode))
                        streetInfo = info;
                }
            }
        }
        return streetInfo != null ? streetInfo.getName() : "";
    }

    /* 로딩 */
    public void showLoading(BaseActivity activity) {
        if (activity == null || activity.isFinishing())
            return;

        if (loadingDialog != null && loadingDialog.isShowing())
            return;

        loadingDialog = new AppCompatDialog(activity, R.style.AlertDialog);
        loadingDialog.setContentView(R.layout.loading);
        loadingDialog.setOnKeyListener((dialogInterface, keyCode, keyEvent) -> keyCode == KeyEvent.KEYCODE_BACK || loadingDialog.onKeyDown(keyCode, keyEvent));
        Window window = loadingDialog.getWindow();
        if (window != null)
            window.getAttributes().windowAnimations = R.style.MergeDialogAnim;
        loadingDialog.show();

        final ImageView loadingFrame = loadingDialog.findViewById(R.id.loading_img);
        final AnimationDrawable frameAnimation;
        if (loadingFrame != null) {
            frameAnimation = (AnimationDrawable) loadingFrame.getBackground();
            loadingFrame.post(frameAnimation::start);
        }
    }

    public void hideLoading(BaseActivity activity) {
        if (!activity.isDestroyed() && loadingDialog != null && loadingDialog.isShowing())
            loadingDialog.dismiss();
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        String classname = activity.getLocalClassName();
        classname = classname.substring(classname.lastIndexOf(".")+1);
        MDEBUG.debug( classname + " is on Created");
        _isAppActive = true;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        String classname = activity.getLocalClassName();
        classname = classname.substring(classname.lastIndexOf(".")+1);
        MDEBUG.debug( classname + " is on Resumed");
        _isAppActive = true;

    }

    @Override
    public void onActivityStopped(Activity activity) {
        String classname = activity.getLocalClassName();
        classname = classname.substring(classname.lastIndexOf(".")+1);
        MDEBUG.debug( classname + " is on Stopped");
        _isAppActive = true;
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {



    }
    public Bitmap BarcodeGenerateRotate(String code){
        BarcodeGen gen = new BarcodeGen();
        float wpx, hpx;
        wpx = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 350,
                getResources().getDisplayMetrics()
        );
        hpx = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 111,
                getResources().getDisplayMetrics()
        );
        try {
            Bitmap bitmap = gen.encodeAsBitmap(code, BarcodeFormat.CODE_128, (int) wpx, (int) hpx);
            Matrix ma = new Matrix();
            ma.postRotate(90f);
            Bitmap rotatebitBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),ma,false);
            return rotatebitBitmap;
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;
    }
    public Bitmap BarcodeGenerate(String code){
        BarcodeGen gen = new BarcodeGen();
        float wpx, hpx;
        wpx = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 220,
                getResources().getDisplayMetrics()
        );
        hpx = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 70,
                getResources().getDisplayMetrics()
        );
        try {
            Bitmap bitmap = gen.encodeAsBitmap(code, BarcodeFormat.CODE_128, (int) wpx, (int) hpx);
            return bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;

    }
    public String getImageUrlFiltered(String url) {
        if (url != null) {
            Matcher matcher = Pattern.compile(".*/(.*)").matcher(url);

            if (matcher.find()) {
                String imgName = matcher.group(1);
                if (imgName.matches(".*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*")) {
                    // 이미지 이름이 한글일 때
                    String encodeImageName = null;

                    try {
                        encodeImageName = URLEncoder.encode(imgName, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    if (encodeImageName != null) {

                        url = url.replace(imgName, encodeImageName.replace("+","%20"));

                    }
                }
            }
        }
        MDEBUG.debug("Properties.BASE_URL+url " + Properties.BASE_URL+url);
        return Properties.BASE_URL+url;
    }
}
