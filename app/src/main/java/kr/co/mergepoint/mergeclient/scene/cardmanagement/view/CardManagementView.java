package kr.co.mergepoint.mergeclient.scene.cardmanagement.view;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.databinding.CardMainBinding;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.CardManagementActivity;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.presenter.CardManagementPresenter;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.view.fragment.CardRegisteFragment;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.view.fragment.UplusRegisteFragment;

import static kr.co.mergepoint.mergeclient.application.common.Properties.BASE_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CARD_REGISTE_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UPLUS_REGISTE_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WEB_URL;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public class CardManagementView extends BaseView<CardManagementActivity, CardManagementPresenter, CardMainBinding> {

    private CardRegisteFragment cardRegisteFragment;
    private UplusRegisteFragment uplusRegisteFragment;

    public CardManagementView(CardManagementActivity activity, int layout) {
        super(activity, layout);
    }

    @Override
    protected Toolbar setToolbar() {
        return binding.toolbarBinding.basicToolbar;
    }

    public void showCardRegiste() {
        openFragment(RIGHT_LEFT, R.id.card_main_layout, cardRegisteFragment, CARD_REGISTE_TAG);
    }

    public void showUplusRegiste() {
        registeBirthClearFocus();
        try {
            String birth = cardRegisteFragment.getUserBirthday();
            String nickName = cardRegisteFragment.getCardNickName();
            Bundle bundle = new Bundle();
            bundle.putString(WEB_URL, BASE_URL + "payment/billRegist?birthday=" + birth + "&nickName=" + nickName);
            uplusRegisteFragment.setArguments(bundle);
            openFragment(RIGHT_LEFT, R.id.card_main_layout, uplusRegisteFragment, UPLUS_REGISTE_TAG);
        } catch (NumberFormatException e) {
            Snackbar.make(getContentView(), R.string.request_birthday, Snackbar.LENGTH_SHORT).show();
        }
    }

    public void registeBirthClearFocus() {
        cardRegisteFragment.clearFocus();
    }

    public void cardRegist(boolean isSuccess, String error) {
        removeFragment(uplusRegisteFragment);
        final String resultString = isSuccess ? activity.getResources().getString(R.string.card_registe_success) : error;
        activity.runOnUiThread(() -> Snackbar.make(getContentView(), resultString, Snackbar.LENGTH_LONG).show());
    }
}
