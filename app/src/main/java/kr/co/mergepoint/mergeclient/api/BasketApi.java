package kr.co.mergepoint.mergeclient.api;

import kr.co.mergepoint.mergeclient.scene.data.basket.AddBasket;
import kr.co.mergepoint.mergeclient.scene.data.basket.Basket;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by 1017sjg on 2017. 6. 28..
 */

public interface BasketApi {
    @GET("cart/read")
    Call<Basket> getUserShoppingBasket();

    @GET("cart/count")
    Call<ResponseObject<Integer>> getShoppingBasketCount();

    @POST("cart/add")
    Call<ResponseBody> addShoppingBasket(@Body AddBasket basket);

    @POST("cart/modify/menu")
    @FormUrlEncoded
    Call<ResponseObject<Integer>> modifyShoppingBasketMenu(@Field("oid") int menuOid, @Field("count") int count);

    @POST("cart/delete")
    @FormUrlEncoded
    Call<ResponseObject<Integer>> deleteShoppingBasket(@Field("oid") int menuOid);
}
