package kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment;

import android.animation.ObjectAnimator;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.FragmentLifeCycle;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.databinding.UsingMenuBinding;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter.UsingMenuAdapter;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

/**
 * Created by 1017sjg on 2017. 10. 23..
 */

public class UsingFragment extends BaseFragment implements View.OnTouchListener {

    private UsagePayment usagePayment;
    private UsingMenuBinding menuBinding;
    private boolean isApprove;
    BaseActivity activity;

    public boolean isApprove() {
        return isApprove;
    }

    public void setApprove(boolean approve) {
        isApprove = approve;
    }

    public int getOid() {
        return usagePayment != null ? usagePayment.oid : NO_VALUE;
    }

    public Map<String, Object> getApproveInfo() {
        String pin = menuBinding.pin1.getText().toString() + menuBinding.pin2.getText().toString() + menuBinding.pin3.getText().toString() + menuBinding.pin4.getText().toString();
        Map<String, Object> info = new HashMap<>();
        info.put("oid", usagePayment.oid);
        if (usagePayment.shopType <= 1) {
            info.put("pin", pin.length() == 4 ? pin : "");
        }
        return info;
    }

    public void resetPinCodeView() {
        menuBinding.pin1.getText().clear();
        menuBinding.pin2.getText().clear();
        menuBinding.pin3.getText().clear();
        menuBinding.pin4.getText().clear();
        menuBinding.pin1.requestFocus();
    }

    public static UsingFragment newInstance(FragmentLifeCycle lifeCycle, UsagePayment usagePayment) {
        Bundle args = new Bundle();
        UsingFragment fragment = new UsingFragment();
        fragment.lifeCycle = lifeCycle;
        fragment.usagePayment = usagePayment;
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        menuBinding = DataBindingUtil.inflate(inflater, R.layout.using_menu, container, false);
        menuBinding.setTitle(getString(R.string.use_approve_title));
        menuBinding.setOnClick(this);
        menuBinding.setOnTouch(this);
        menuBinding.setUseApprove(usagePayment);
        menuBinding.setUseAdapter(new UsingMenuAdapter(usagePayment.usedMenuList));
        menuBinding.pinLayout.setVisibility(usagePayment.shopType > 1 ? View.INVISIBLE : View.VISIBLE);
        menuBinding.approveBtn.setVisibility(usagePayment.shopType > 1 ? View.INVISIBLE : View.VISIBLE);
        menuBinding.cancelBtn.setVisibility(usagePayment.shopType > 1 ? View.INVISIBLE : View.VISIBLE);
        menuBinding.enterpriseConfirmLl.setVisibility(usagePayment.shopType > 1 ? View.VISIBLE : View.GONE);
        menuBinding.enterprisePayNotice.setText(Html.fromHtml(getResources().getString(R.string.order_approval_warning_enterprize)));
        return menuBinding.getRoot();
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view.getId() == menuBinding.pin1.getId() || view.getId() == menuBinding.pin2.getId() || view.getId() == menuBinding.pin3.getId() || view.getId() == menuBinding.pin4.getId())
            menuBinding.paymentScroll.post(() -> ObjectAnimator.ofInt(menuBinding.paymentScroll, "scrollY", menuBinding.paymentScroll.getBottom()).setDuration(300).start());
        view.performClick();
        return false;
    }


}
