package kr.co.mergepoint.mergeclient.scene.payment.view.adapter.holder;

import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.databinding.PaymentMenuGroupBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.PaymentMenuGroup;

/**
 * Created by 1017sjg on 2017. 9. 4..
 */

public class PaymentGroupMenuHolder extends GroupViewHolder {

    private PaymentMenuGroupBinding menuGroupBinding;

    public PaymentGroupMenuHolder(PaymentMenuGroupBinding menuGroupBinding) {
        super(menuGroupBinding.getRoot());
        this.menuGroupBinding = menuGroupBinding;
    }

    public void setGroupMenu(PaymentMenuGroup group) {
        menuGroupBinding.setGroup(group);
    }

    @Override
    public void expand() {
        if (menuGroupBinding.getGroup().getItems().size() > 0) {
            menuGroupBinding.expandMenu.setImageResource(R.drawable.icon_pay_menu_close);
            menuGroupBinding.expandMenu.invalidate();
        }
    }

    @Override
    public void collapse() {
        if (menuGroupBinding.getGroup().getItems().size() > 0) {
            menuGroupBinding.expandMenu.setImageResource(R.drawable.icon_pay_menu_open);
            menuGroupBinding.expandMenu.invalidate();
        }
    }
}

