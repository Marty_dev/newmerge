package kr.co.mergepoint.mergeclient.scene.shoplist.view;

import android.widget.SeekBar;

import com.facebook.CallbackManager;

import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;

import java.util.ArrayList;
import java.util.Map;

import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.data.splash.CategoryInfo;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.ShopListAdapter;

/**
 * Created by jgson on 2018. 1. 8..
 */

public interface ShopListInterface {

    /* INIT */
    void addMapView();

    void removeMapView();

    void initStreetSpinner();

    void initDescription();

    void selectStreet(String streetName, Map<String, Object> filter);

    void measureStreetSpinnerWidth(String streetName);

    /* OPEN */
    void openSelectTheme();

    void openSelectPriceRange();

    /* 장바구니 열기 */
    void openShoppingBasket();

    void openPaymentResult();

    void openShopDetail(Shop shop);

    void openMapIntent();

    void openShopShared();

    void animCart();

    void animCart(int mapButton, boolean isDescription);

    /* 매장 리스트 검색 바 애니메이션 */
    void animSearchBar();

    /* 매장 리스트 필터 애니메이션 */
    void animFilter();

    void animMap();

    void animDescription(Shop shop, boolean visible);

    void animZoom(boolean position, int value);

    void distanceSeekBar(SeekBar seekBar, final int position);

    void selectTheme(String name);

    void removePriceRangeFilter(ArrayList<String> priceRange);

    void resetFilter();

    void changeFilterView();

    void searchShop(String searchString, Map<String, Object> filter);

    void searchCategoryShop(Map<String, Object> filter);

    void startListFilter(Map<String, Object> filter);

    void startMapFilter();

    /* MAP */
    void addPOIItems(ArrayList<Shop> shops);
    void addPOIItemsPostSearch(ArrayList<Shop> shops);
    void enableTrackingUserLocation();

    void disableTrackingUserLocation();

    void animEnlargementMap();

    void animReductionMap();

    void callShopPhone();

    /* GETTER or SETTER */
    @Properties.ShopVisibleType int currentShopType();

    int getCurrentCategoryPosition();

    String getStreetName();

    ShopListAdapter getShopListAdapter();

    boolean isCurrentListShow();

    String[] getStreetStringArray();

    String getCurrentCategory(ArrayList<CategoryInfo> categoryInfos);

    MapPoint getSavePoint();

    int getSaveZoomLevel();

    CallbackManager getShareFacebookCallback();

    MapView getMapView();

    MapPoint.GeoCoordinate getTopRightPoint();

    MapPoint.GeoCoordinate getBottomLeftPoint();
}
