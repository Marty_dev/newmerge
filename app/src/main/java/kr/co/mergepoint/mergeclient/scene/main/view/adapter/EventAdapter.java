package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.databinding.EventChildBinding;
import kr.co.mergepoint.mergeclient.databinding.EventGroupBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.EventInfo;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder.ChildEventHolder;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder.Event;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder.GroupEventHolder;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class EventAdapter extends ExpandableRecyclerViewAdapter<GroupEventHolder, ChildEventHolder> {

    public EventAdapter(List<Event> groups) {
        super(groups);
    }

    @Override
    public GroupEventHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_group, parent, false);
        EventGroupBinding groupBinding = DataBindingUtil.bind(view);
        return new GroupEventHolder(groupBinding);
    }

    @Override
    public ChildEventHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_child, parent, false);
        EventChildBinding childBinding = DataBindingUtil.bind(view);
        return new ChildEventHolder(childBinding);
    }

    @Override
    public void onBindChildViewHolder(ChildEventHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        EventInfo childInfo = ((Event) group).getItems().get(childIndex);
        holder.onBind(childInfo);
    }

    @Override
    public void onBindGroupViewHolder(GroupEventHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setGroupMenu((Event) group);
    }
}
