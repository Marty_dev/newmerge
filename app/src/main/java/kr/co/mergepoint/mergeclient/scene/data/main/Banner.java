package kr.co.mergepoint.mergeclient.scene.data.main;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 9. 12..
 */

public class Banner {

    @SerializedName("oid")
    public int oid;

    @SerializedName("viewOrder")
    public int viewOrder;

    @SerializedName("actionType")
    public int actionType;

    @SerializedName("actionData")
    public String actionData;

    @SerializedName("image")
    public String image;

    @SerializedName("state")
    public int state;

    @SerializedName("creDateTime")
    public ArrayList<Integer> creDateTime;
    @SerializedName("modDateTime")
    public ArrayList<Integer> modDateTime;
}
