package kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.scene.data.menu.ShopChildInfo;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class Menu extends ExpandableGroup<ShopChildInfo> {

    private int oid;

    public Menu(String title, List<ShopChildInfo> items, int oid) {
        super(title, items);
        this.oid = oid;
    }

    public int getOid() {
        return oid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Menu)) return false;

        Menu menu = (Menu) o;
        return Objects.equals(getTitle(), menu.getTitle());
    }
}
