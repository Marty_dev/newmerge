package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

/**
 * Created by 1017sjg on 2017. 8. 4..
 */

public class CustomViewPager extends ViewPager implements View.OnTouchListener {

    private LikeListener listener;
    private GestureDetector gestureDetector;
    private Handler handler;
    private boolean longClick;
    private int longPressCount = 0;

    public void setListener(LikeListener listener) {
        this.listener = listener;
    }

    public interface LikeListener {
        boolean onLikeTap(int count, boolean isLongTouch);
        void onLikeTapLongEnd(int count);
    }

    public CustomViewPager(Context context) {
        super(context);
        init();
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        handler = new Handler();
        setOnTouchListener(this);
        gestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public void onLongPress(MotionEvent e) {
                longClick = true;
                handler.post(action);
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                if (listener == null){
                    return false;
                }
                longClick = false;
                listener.onLikeTap(NO_VALUE, false);
                return true;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                longClick = false;
                return true;
            }
        });
        gestureDetector.setIsLongpressEnabled(true);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (longClick && event.getAction() == MotionEvent.ACTION_UP) {

            handler.removeCallbacks(action);
            listener.onLikeTapLongEnd(longPressCount);
            longPressCount = 0;
        }
        return gestureDetector.onTouchEvent(event);
    }

    private Runnable action = new Runnable() {
        @Override
        public void run() {
            if (listener.onLikeTap(longPressCount, true)) {
                handler.postDelayed(this, 800);
                longPressCount+=1;
            } else {
                longPressCount = 0;
            }
        }
    };
}
