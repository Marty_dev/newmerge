package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.CombineImages;
import kr.co.mergepoint.mergeclient.application.common.OnTypeListener;
import kr.co.mergepoint.mergeclient.databinding.CombineImagesDetailBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.CombineImagesDetail;
import kr.co.mergepoint.mergeclient.scene.data.main.EventInfo;

import static kr.co.mergepoint.mergeclient.application.common.Properties.COMBINE_EVENT_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.COMBINE_IMG;

/**
 * Created by 1017sjg on 2017. 9. 11..
 */

public class CombineImagesDetailFragment extends BaseFragment {

    private String title;
    private EventInfo eventInfo;
    private OnTypeListener onTypeListener;
    private ArrayList<CombineImagesDetail> imagesDetail;
    private CombineImages combineImages;

    public EventInfo getEventInfo() {
        return eventInfo;
    }

    public static CombineImagesDetailFragment newInstance(String title, OnTypeListener onTypeListener, Bundle bundle) {
        CombineImagesDetailFragment fragment = new CombineImagesDetailFragment();
        fragment.onTypeListener = onTypeListener;
        fragment.title = title;
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        combineImages = new CombineImages(rootActivity);
        if (getArguments() != null) {
            imagesDetail = getArguments().getParcelableArrayList(COMBINE_IMG);
            eventInfo = getArguments().getParcelable(COMBINE_EVENT_INFO);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Fresco.getImagePipeline().clearCaches();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        CombineImagesDetailBinding detailBinding = DataBindingUtil.inflate(inflater, R.layout.combine_images_detail, container, false);
        detailBinding.setTitle(title);
        detailBinding.setOnClick(this);
        combineImages.addCombineImages(imagesDetail, detailBinding.combineImg, onTypeListener, MergeApplication.getUtility().getDeviceWidth());
        return detailBinding.getRoot();
    }

    @Override
    public void onStop() {
        super.onStop();
        Fresco.getImagePipeline().clearCaches();
    }
}
