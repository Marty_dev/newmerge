package kr.co.mergepoint.mergeclient.application.common;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import kr.co.mergepoint.mergeclient.R;

/**
 * Created by 1017sjg on 2017. 10. 20..
 */

public class CustomDatePickerDialog extends DatePickerDialog {
    @TargetApi(Build.VERSION_CODES.N)
    public CustomDatePickerDialog(@NonNull Context context) {
        super(context);
        changeDividerColor(this);
    }

    @TargetApi(Build.VERSION_CODES.N)
    public CustomDatePickerDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    public CustomDatePickerDialog(@NonNull Context context, @Nullable OnDateSetListener listener, int year, int month, int dayOfMonth) {
        super(context, listener, year, month, dayOfMonth);
        changeDividerColor(this);
    }

    public CustomDatePickerDialog(@NonNull Context context, int themeResId, @Nullable OnDateSetListener listener, int year, int monthOfYear, int dayOfMonth) {
        super(context, themeResId, listener, year, monthOfYear, dayOfMonth);
        // Force spinners on Android 7.0 only (SDK 24).
        // Note: I'm using a naked SDK value of 24 here, because I'm
        // targeting SDK 23, and Build.VERSION_CODES.N is not available yet.
        // But if you target SDK >= 24, you should have it.
        if (Build.VERSION.SDK_INT == 24) {
            try {
                final Field field = this.findField(
                        DatePickerDialog.class,
                        DatePicker.class,
                        "mDatePicker"
                );

                final DatePicker datePicker = (DatePicker) field.get(this);
                final Class<?> delegateClass = Class.forName("android.widget.DatePicker$DatePickerDelegate");
                final Field delegateField = this.findField(
                        DatePicker.class,
                        delegateClass,
                        "mDelegate"
                );

                final Object delegate = delegateField.get(datePicker);
                final Class<?> spinnerDelegateClass = Class.forName("android.widget.DatePickerSpinnerDelegate");

                if (delegate.getClass() != spinnerDelegateClass) {
                    delegateField.set(datePicker, null);
                    datePicker.removeAllViews();

                    final Constructor spinnerDelegateConstructor =
                            spinnerDelegateClass.getDeclaredConstructor(
                                    DatePicker.class,
                                    Context.class,
                                    AttributeSet.class,
                                    int.class,
                                    int.class
                            );
                    spinnerDelegateConstructor.setAccessible(true);

                    final Object spinnerDelegate = spinnerDelegateConstructor.newInstance(
                            datePicker,
                            context,
                            null,
                            android.R.attr.datePickerStyle,
                            0
                    );
                    delegateField.set(datePicker, spinnerDelegate);

                    datePicker.init(year, monthOfYear, dayOfMonth, this);
                    datePicker.setCalendarViewShown(false);
                    datePicker.setSpinnersShown(true);
                }
            } catch (Exception e) { /* Do nothing */ }
        }
        changeDividerColor(this);
    }

    /**
     * Find Field with expectedName in objectClass. If not found, find first occurrence of
     * target fieldClass in objectClass.
     */
    private Field findField(Class objectClass, Class fieldClass, String expectedName) {
        try {
            final Field field = objectClass.getDeclaredField(expectedName);
            field.setAccessible(true);
            return field;
        } catch (NoSuchFieldException e) { /* Ignore */ }

        // Search for it if it wasn't found under the expectedName.
        for (final Field field : objectClass.getDeclaredFields()) {
            if (field.getType() == fieldClass) {
                field.setAccessible(true);
                return field;
            }
        }

        return null;
    }

    private void changeDividerColor(DatePickerDialog datePickerDialog) {
        DatePicker datePicker = datePickerDialog.getDatePicker();
        datePicker.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.gray_18));
        LinearLayout rootLayout = (LinearLayout) datePicker.getChildAt(0);
        LinearLayout childLayout = (LinearLayout) rootLayout.getChildAt(0);
        Typeface font = ResourcesCompat.getFont(getContext(), R.font.spoqa_han_sans_regular);
        ColorDrawable color = new ColorDrawable(getContext().getResources().getColor(R.color.colorAccent));

        for (int i = 0; i < childLayout.getChildCount(); i++) {
            View view = childLayout.getChildAt(i);
            if (view instanceof NumberPicker) {
                NumberPicker picker = (NumberPicker) view;
                try {
                    /*클래스의 휠 Paint 객체를 꺼내 폰트를 적용*/
                    Field dividerField = NumberPicker.class.getDeclaredField("mSelectionDivider");
                    dividerField.setAccessible(true);
                    dividerField.set(picker, color);
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }

                try {
                    /*클래스의 휠 Paint 객체를 꺼내 폰트를 적용*/
                    Field field = NumberPicker.class.getDeclaredField("mSelectorWheelPaint");
                    field.setAccessible(true);
                    ((Paint)field.get(picker)).setTypeface(font);
                    ((Paint)field.get(picker)).setColor(ContextCompat.getColor(getContext(), android.R.color.white));
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }

                /*선택된 숫자를 보여주는 TextView*/
                int count = picker.getChildCount();
                for (int j = 0; j < count; j++) {
                /*자식뷰로 꺼내 폰트를 적용*/
                    View childText = picker.getChildAt(j);
                    if (childText instanceof TextView) {
                        ((TextView) childText).setTypeface(font);
                        ((TextView) childText).setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
                    }
                }
            }
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setTitle("");
    }

    @Override
    public void show() {
        super.show();
        getButton(DatePickerDialog.BUTTON_POSITIVE).setBackgroundResource(R.color.gray_18);
        getButton(DatePickerDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        getButton(DatePickerDialog.BUTTON_POSITIVE).setText(R.string.confirm);
        getButton(DatePickerDialog.BUTTON_NEGATIVE).setBackgroundResource(R.color.gray_18);
        getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        getButton(DatePickerDialog.BUTTON_NEGATIVE).setText(R.string.cancel);
    }
}
