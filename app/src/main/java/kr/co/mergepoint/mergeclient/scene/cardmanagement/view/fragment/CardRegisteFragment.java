package kr.co.mergepoint.mergeclient.scene.cardmanagement.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Locale;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.CardRegisteBinding;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public class CardRegisteFragment extends BaseFragment implements View.OnClickListener {

    private View.OnClickListener onClickListener;
    private CardRegisteBinding registeBinding;

    public static CardRegisteFragment newInstance(View.OnClickListener onClickListener) {
        CardRegisteFragment cardRegisteFragment = new CardRegisteFragment();
        cardRegisteFragment.onClickListener = onClickListener;

        return cardRegisteFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View registeView = inflater.inflate(R.layout.card_registe, container, false);

        registeBinding = CardRegisteBinding.bind(registeView);
        registeBinding.setTitle("등록하기");
        registeBinding.setClick(this);

        return registeView;
    }

    public String getUserBirthday() throws NumberFormatException{
        int year = Integer.parseInt(registeBinding.year.getText().toString());
        int month = Integer.parseInt(registeBinding.month.getText().toString());
        int day = Integer.parseInt(registeBinding.day.getText().toString());

        return String.format(Locale.getDefault(), "%d-%02d-%02d", year, month, day);
    }

    public String getCardNickName() {
        return registeBinding.cardNickName.getText().toString().trim();
    }

    public void clearFocus() {
        registeBinding.registeBirthLayout.clearFocus();
    }

    @Override
    public void onClick(View v) {
        onClickListener.onClick(v);
    }
}
