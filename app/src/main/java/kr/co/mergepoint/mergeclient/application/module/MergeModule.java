package kr.co.mergepoint.mergeclient.application.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jgson on 2017. 6. 2..
 */

@Module
public class MergeModule {

    private final Context context;

    public MergeModule(Context context) { this.context = context; }

    @Provides
    @Singleton
    Context provideMergeContext() { return context; }
}