package kr.co.mergepoint.mergeclient.scene.shoplist;

import android.annotation.TargetApi;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;

import javax.inject.Inject;

import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocationListener;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.SelectItem;
import kr.co.mergepoint.mergeclient.scene.shoplist.dagger.DaggerShopListComponent;
import kr.co.mergepoint.mergeclient.scene.shoplist.dagger.ShopListModule;
import kr.co.mergepoint.mergeclient.scene.shoplist.presenter.ShopListPresenter;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.FilterHolder;

/**
 * Created by 1017sjg on 2017. 6. 13..
 */

public class ShopListActivity extends BaseActivity implements UserLocationListener, FilterHolder.OnItemSelectedListener, MapView.MapViewEventListener, MapView.POIItemEventListener {

    @Inject ShopListPresenter presenter;

    @Override
    protected void onPostResume() {
        super.onPostResume();
        presenter.onPostResume();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerShopListComponent.builder().shopListModule(new ShopListModule(this)).build().inject(this);
        presenter.onCreate();
    }

    @Override
    public void onBackPressed() {
        if (!presenter.onFilterBackPressed())
            super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    protected void onActivityClick(View view) {
        presenter.onClick(view);
    }

    @Override
    public void requestPermissions() {
        presenter.requestPermissions();
    }

    @Override
    public void startLoadList(Location location) {
        presenter.startLoadList(location);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onItemSelected(SelectItem item) {
        presenter.onItemSelected(item);
    }

    @Override
    public void onMapViewInitialized(MapView mapView) {
        presenter.onMapViewInitialized(mapView);
    }

    @Override
    public void onMapViewCenterPointMoved(MapView mapView, MapPoint mapPoint) {}

    @Override
    public void onMapViewZoomLevelChanged(MapView mapView, int i) {}

    @Override
    public void onMapViewSingleTapped(MapView mapView, MapPoint mapPoint) {
        presenter.onMapViewSingleTapped(mapView, mapPoint);
    }

    @Override
    public void onMapViewDoubleTapped(MapView mapView, MapPoint mapPoint) {}

    @Override
    public void onMapViewLongPressed(MapView mapView, MapPoint mapPoint) {}

    @Override
    public void onMapViewDragStarted(MapView mapView, MapPoint mapPoint) {}

    @Override
    public void onMapViewDragEnded(MapView mapView, MapPoint mapPoint) {}

    @Override
    public void onMapViewMoveFinished(MapView mapView, MapPoint mapPoint) {
        presenter.onMapViewMoveFinished(mapView, mapPoint);
    }

    @Override
    public void onPOIItemSelected(MapView mapView, MapPOIItem mapPOIItem) {
        presenter.onPOIItemSelected(mapView, mapPOIItem);
    }

    @Override
    public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem) {}

    @Override
    public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem, MapPOIItem.CalloutBalloonButtonType calloutBalloonButtonType) {}

    @Override
    public void onDraggablePOIItemMoved(MapView mapView, MapPOIItem mapPOIItem, MapPoint mapPoint) {}
}