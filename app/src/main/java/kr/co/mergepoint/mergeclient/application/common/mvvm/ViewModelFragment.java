package kr.co.mergepoint.mergeclient.application.common.mvvm;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.mvvm.dagger.ActivityComponent;

import static kr.co.mergepoint.mergeclient.application.common.Properties.EXTRA_VIEW_MODEL_STATE;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public abstract class ViewModelFragment extends BaseFragment {

    private ViewModel viewModel;

    @Nullable
    protected abstract ViewModel createAndBindViewModel(View root, @NonNull ActivityComponent component, @Nullable ViewModel.State savedViewModelState);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ViewModelActivity activity = (ViewModelActivity) getActivity();
        ViewModel.State savedViewModelState = (savedInstanceState != null) ? (ViewModel.State) savedInstanceState.getParcelable(EXTRA_VIEW_MODEL_STATE) : null;

        viewModel = createAndBindViewModel(getView(), activity.getComponent(), savedViewModelState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (viewModel != null)
            outState.putParcelable(EXTRA_VIEW_MODEL_STATE, viewModel.getInstanceState());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (viewModel != null)
            viewModel.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (viewModel != null)
            viewModel.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (viewModel != null)
            viewModel.onStop();
    }
}
