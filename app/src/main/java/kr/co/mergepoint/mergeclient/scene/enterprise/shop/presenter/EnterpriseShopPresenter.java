package kr.co.mergepoint.mergeclient.scene.enterprise.shop.presenter;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasePresenter;
import kr.co.mergepoint.mergeclient.application.common.BookMark;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.model.EnterpriseShopModel;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.presenter.callback.EnterpriseShopCallback;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.EnterpriseShopView;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.adapter.EnterpriseShopPagerAdapter;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder.ChildMenuHolder;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.view.PricePaymentActivity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;

import static android.app.Activity.RESULT_OK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_PRICE_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class EnterpriseShopPresenter extends BasePresenter<EnterpriseShopView, EnterpriseShopModel, EnterpriseShopCallback> implements ViewPager.OnPageChangeListener, BookMark.OnCheckFavorite {

    private Runnable payEndResultRunnable;

    public EnterpriseShopPresenter(EnterpriseShopView baseView, EnterpriseShopModel baseModel, EnterpriseShopCallback callback) {
        super(baseView, baseModel, callback);
    }

    @Override
    protected void setBindingProperty() {
        baseView.binding.setOnClick(this);
        baseView.binding.setDetailShopImagePagerListener(this);
        baseView.binding.shopDetailTab.setupWithViewPager(baseView.binding.enterpriseShopDetailPager);
        baseView.binding.setPagerAdapter(new EnterpriseShopPagerAdapter(baseView.activity,baseModel));
        baseView.binding.setDetailPagerListener(new TabLayout.TabLayoutOnPageChangeListener(baseView.binding.shopDetailTab));
    }

    public void onCreate(Bundle savedInstanceState) {
        baseView.showLoading();
        if (baseView.activity.getIntent().getExtras() != null) {
            Object shopObj= baseView.activity.getIntent().getExtras().get(SHOP_INFO);
            if (shopObj instanceof Shop) {
                baseModel.getShopDetailInfo(
                        ((Shop) shopObj).oid,baseCallback.shopDetailCallback());
            } else if (shopObj instanceof Integer) {
                baseModel.getShopDetailInfo((int) shopObj, baseCallback.shopDetailCallback());
            }
        }



    }

    public void onPostResume() {
        if (payEndResultRunnable != null)
            new Handler().postDelayed(payEndResultRunnable, 500);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        ShopDetail shopDetail = baseView.binding.getShopDetail();
        switch (v.getId()) {
            case R.id.shoppingbasket:
                if (baseView.startShoppingBasket()) {
                    baseView.showLoading();
                    baseModel.addShoppingBasket(baseCallback.addBasketCallback(), baseView.getSelectOptions());
                }
                break;
            case R.id.payment:
                baseView.openPayment();
                break;
            case R.id.usage_history:
            case R.id.payment_result_history:
                baseView.openActivity(UsageActivity.class);
                break;
            case R.id.payment_result_main:
                baseView.openClearTaskActivity(MainActivity.class);
                break;
            case R.id.shop_call:
                baseView.callShopPhone();
                break;
            case R.id.shop_share:
                baseView.openShopShared(shopDetail);
                break;
            case R.id.shop_map:
            case R.id.shop_map_front:
                baseView.openMapIntent(shopDetail.latitude, shopDetail.longitude,shopDetail.address01);
                break;
            case R.id.shop_list_cart:
                baseView.openShoppingBasket();
                break;
            case R.id.enterprise_menu:
                baseView.showLoading();
                baseModel.getDailyMenuList(baseCallback.getShopDailyMenuList(), shopDetail.oid, 0);
                break;
            case R.id.pre_date:
                baseModel.getDailyMenuList(baseCallback.getShopDailyMenuList(), shopDetail.oid, -1);
                baseView.changeDailyMenuDate(baseModel.getCalendar());
                break;
            case R.id.post_date:
                baseModel.getDailyMenuList(baseCallback.getShopDailyMenuList(), shopDetail.oid, 1);
                baseView.changeDailyMenuDate(baseModel.getCalendar());
                break;
            case R.id.enterprise_pricecustom:
                baseView.openActivityForResult(PricePaymentActivity.class,PAY_PRICE_REQUEST);
                break;
        }
    }

    /*메뉴의 child 메뉴 클릭했을 때*/
    public void onChildTouch(ChildViewHolder childViewHolder) {
        if (childViewHolder != null && childViewHolder instanceof ChildMenuHolder) {
            ChildMenuHolder childMenuHolder = (ChildMenuHolder) childViewHolder;
            int childOid = childMenuHolder.getShopMenuChildBinding().getShopChildInfo().oid;
            baseModel.getMenuDetailInfo(childOid, baseCallback.addMenuDetail());
        } else {
            baseView.showAlert(R.string.retry);
        }
    }

    /*매장 상단 이미지 페이지 변경 리스너*/
    @Override
    public void onPageSelected(int position) {
        baseView.binding.enterpriseShopDetailPictureLayout.shopPictureIndicator.selectDot(position);
    }
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
    @Override
    public void onPageScrollStateChanged(int state) {}

    @Override
    public void checkFavorite(View view, boolean isLogin) {
        if (isLogin) {
            baseModel.checkFavoriteCall(baseCallback.getCheckFavoriteCall(), baseView.binding.getShopDetail().getOid());
        } else {
            baseView.showAlert(R.string.please_login);
        }
    }

    public void onStop() {
        payEndResultRunnable = null;
    }

    int paymentRef;
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PAY_RESULT_REQUEST) {
                paymentRef = data.getIntExtra("paymentRef",-1);
                payEndResultRunnable = () -> baseView.openPaymentResult(paymentRef,data.getBooleanExtra("isMulti",false));

            }else if (requestCode == PAY_PRICE_REQUEST){
                int price = data.getIntExtra("price", -1);
                int shopRef = baseView.binding.getShopDetail().oid;

                baseView.openPaymentByPrice(price,shopRef);
            }
        }
        /*페이스북 콜백 매니져*/
        if (baseView.getShareFacebookCallback() != null)
            baseView.getShareFacebookCallback().onActivityResult(requestCode, resultCode, data);
    }

    public void onBackPressed() {
        baseView.onBackPressed();
    }
}
