package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.MyCouponListFooterBinding;
import kr.co.mergepoint.mergeclient.databinding.MyCouponListHeaderBinding;
import kr.co.mergepoint.mergeclient.databinding.MyCouponListItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;

import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_FOOTER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_HEADER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_ITEM;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class CouponAdapter extends BasicListAdapter<BasicListHolder, Coupon> {

    private Resources resources;

    public CouponAdapter(ArrayList<Coupon> arrayList) {
        super(arrayList);
        resources = MergeApplication.getMergeAppComponent().getUtility().getResuorces();
    }

    @Override
    public BasicListHolder setCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == LIST_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_coupon_list_item, parent, false);
            MyCouponListItemBinding mycouponListItemBinding = DataBindingUtil.bind(view);

            return new BasicListHolder<MyCouponListItemBinding, Coupon>(mycouponListItemBinding) {
                @Override
                public void setDataBindingWithData(Coupon data) {
                    getDataBinding().setCoupon(data);
                }
            };
        } else if (viewType == LIST_TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_coupon_list_header, parent, false);
            MyCouponListHeaderBinding mycouponListHeaderBinding = DataBindingUtil.bind(view);

            return new BasicListHolder<MyCouponListHeaderBinding, Integer>(mycouponListHeaderBinding) {
                @Override
                public void setDataBindingWithData(Integer data) {
                    getDataBinding().setCouponString(fromHtml(resources.getString(R.string.available_coupons, data)));
                }
            };
        } else if (viewType == LIST_TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_coupon_list_footer, parent, false);
            MyCouponListFooterBinding mycouponListFooterBinding = DataBindingUtil.bind(view);

            return new BasicListHolder<MyCouponListFooterBinding, String>(mycouponListFooterBinding) {
                @Override
                public void setDataBindingWithData(String data) {}
            };
        }

        return null;
    }

    private Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
            return Html.fromHtml(source);
        return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setBindViewHolder(BasicListHolder holder, int position) {
        if (position == 0) {
            holder.bind(getObservableArrayList().size());
        } else if (position > 0 && position < getItemCount() - 1) {
            holder.bind(getObservableArrayList().get(position - 1));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return LIST_TYPE_HEADER;
        } else if (getItemCount() - 1 == position) {
            return LIST_TYPE_FOOTER;
        } else {
            return LIST_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + 2;
    }
}
