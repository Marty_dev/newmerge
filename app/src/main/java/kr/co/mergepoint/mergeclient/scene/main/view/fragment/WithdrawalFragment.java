package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.WithdrawalBinding;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * Created by 1017sjg on 2017. 8. 9..
 */

public class WithdrawalFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {

    private boolean isRefund;

    public boolean isRefund() {
        return isRefund;
    }

    public static WithdrawalFragment newInstance() {
        Bundle args = new Bundle();
        WithdrawalFragment fragment = new WithdrawalFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        WithdrawalBinding withdrawalBinding = DataBindingUtil.inflate(inflater, R.layout.withdrawal, container, false);
        withdrawalBinding.setTitle(getString(R.string.withdrawal_title));
        withdrawalBinding.setOnClick(this);
        withdrawalBinding.setOnRefund(this);
//        withdrawalBinding.setPoint(MergeApplication.getInitialInfo().getMember().getBuyPoints());
        return withdrawalBinding.getRoot();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        MDEBUG.debug("isRefun check" + isChecked);
        isRefund = isChecked;
    }
}
