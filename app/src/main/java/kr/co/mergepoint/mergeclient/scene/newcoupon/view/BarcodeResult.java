package kr.co.mergepoint.mergeclient.scene.newcoupon.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.requiredLoginActivities;

public class BarcodeResult extends AppCompatActivity {

    @BindView(R.id.open_main)
    Button openMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_result);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.open_main)
    public void onViewClicked() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onViewClicked();
    }
}
