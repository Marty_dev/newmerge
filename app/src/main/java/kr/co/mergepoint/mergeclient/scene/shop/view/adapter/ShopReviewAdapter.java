package kr.co.mergepoint.mergeclient.scene.shop.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.ShopDetailReviewItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.Review;

/**
 * Created by jgson on 2018. 1. 3..
 */

public class ShopReviewAdapter extends BasicListAdapter<BasicListHolder<ShopDetailReviewItemBinding, Review>, Review> {

    @Override
    public BasicListHolder<ShopDetailReviewItemBinding, Review> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_detail_review_item, parent, false);
        ShopDetailReviewItemBinding itemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<ShopDetailReviewItemBinding, Review>(itemBinding) {
            @Override
            public void setDataBindingWithData(Review data) {
                getDataBinding().setReview(data);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<ShopDetailReviewItemBinding, Review> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
