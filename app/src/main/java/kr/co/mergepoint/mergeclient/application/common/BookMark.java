package kr.co.mergepoint.mergeclient.application.common;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;

/**
 * Created by 1017sjg on 2017. 6. 29..
 */

public class BookMark extends View implements View.OnClickListener, View.OnTouchListener {

    private int backgroundId;
    private OnCheckFavorite checkFavorite;
    private boolean isFavorite;

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setCheckFavorite(OnCheckFavorite checkFavorite) {
        this.checkFavorite = checkFavorite;
    }

    public interface OnCheckFavorite {
        void checkFavorite(View view, boolean isLogin);
    }

    public BookMark(Context context) {
        super(context);
        init();
    }

    public BookMark(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BookMark(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BookMark(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setIcon(R.drawable.icon_bookmark_empty);
        setOnClickListener(this);
        setOnTouchListener(this);
    }

    private void setIcon(int icon) {
        backgroundId = icon;
        setBackgroundResource(backgroundId);
    }

    @Override
    public void onClick(View v) {
        InitialInfo initialInfo = MergeApplication.getInitialInfo();
        boolean isLogin = initialInfo != null && initialInfo.isLoginState();
        if (isLogin) {
            if (backgroundId == R.drawable.icon_bookmark_empty) {
                setFavoriteFill();
            } else if (backgroundId == R.drawable.icon_bookmark_fill) {
                setFavoriteEmpty();
            }
        }

        if (checkFavorite != null)
            checkFavorite.checkFavorite(this, isLogin);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)  {
        return false;
    }

    public void setFavoriteStatus(boolean isFilled) {
        if (isFilled) {
            if (backgroundId != R.drawable.icon_bookmark_fill) {
                setFavoriteFill();
            }
        } else {
            if (backgroundId != R.drawable.icon_bookmark_empty) {
                setFavoriteEmpty();
            }
        }
    }

    private void setFavoriteFill() {
        setIcon(R.drawable.icon_bookmark_fill);
        isFavorite = true;
    }

    private void setFavoriteEmpty() {
        setIcon(R.drawable.icon_bookmark_empty);
        isFavorite = false;
    }
}
