package kr.co.mergepoint.mergeclient.scene.shop.view.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.application.common.CustomCountView;
import kr.co.mergepoint.mergeclient.application.common.CustomRadioButton;
import kr.co.mergepoint.mergeclient.databinding.ShopMenuOptionBinding;
import kr.co.mergepoint.mergeclient.databinding.ShopMenuOptionRadioBinding;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionMenu;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionSelectMenu;
import kr.co.mergepoint.mergeclient.scene.shop.view.listener.OnOptionCheckListener;
import kr.co.mergepoint.mergeclient.scene.shop.view.listener.OnRadioChange;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_CHECK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_RADIO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.OPTION_TYPE_MAIN;

/**
 * Created by 1017sjg on 2017. 6. 24..
 */

public class ShopMenuOptionAdapter extends BasicListAdapter<BasicListHolder, ShopOptionSelectMenu> implements CustomCountView.OnChangeCountListener, View.OnClickListener {

    private OnRadioChange radioChange = new OnRadioChange();
    private ShopOptionMenu optionMenu;
    private OnOptionCheckListener optionListener;
    private ShopOptionSelectMenu preSelectMenu;

    ShopMenuOptionAdapter(ShopOptionMenu optionMenu, int selectPrice, ShopMenuDetailAdapter menuDetailAdapter) {
        super(optionMenu.menus);
        this.optionMenu = optionMenu;
        this.optionListener = menuDetailAdapter;
        if (optionMenu.type == OPTION_TYPE_MAIN) {
            for (ShopOptionSelectMenu selectMenu: optionMenu.menus) {
                if (selectMenu.price == selectPrice) {
                    preSelectMenu = selectMenu;
                    break;
                }
            }
        }
    }

    @Override
    public BasicListHolder setCreateViewHolder(final ViewGroup parent, int viewType) {
        if (viewType == LIST_TYPE_CHECK) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_menu_option, parent, false);
            ShopMenuOptionBinding binding = DataBindingUtil.bind(view);
            return new BasicListHolder<ShopMenuOptionBinding, ShopOptionSelectMenu>(binding) {
                @Override
                public void setDataBindingWithData(ShopOptionSelectMenu data) {
                    getDataBinding().setOption(data);
                    getDataBinding().setTouchListener(ShopMenuOptionAdapter.this);
                    getDataBinding().countLayout.countAddition.setBackgroundResource(R.drawable.btn_quantity_black_add);
                    getDataBinding().countLayout.countSubtract.setBackgroundResource(R.drawable.btn_quantity_black_sub);
                }
            };
        } else if (viewType == LIST_TYPE_RADIO) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_menu_option_radio, parent, false);
            ShopMenuOptionRadioBinding radioBinding = DataBindingUtil.bind(view);
            return new BasicListHolder<ShopMenuOptionRadioBinding, ShopOptionSelectMenu>(radioBinding) {
                @Override
                public void setDataBindingWithData(ShopOptionSelectMenu data) {
                    getDataBinding().setOption(data);
                    getDataBinding().setMainOption(optionMenu.type == OPTION_TYPE_MAIN);
                    getDataBinding().setRadioListener(radioChange);
                    getDataBinding().setTouchListener(ShopMenuOptionAdapter.this);

                    getDataBinding().optionSingleSelect.setChecked(preSelectMenu != null && preSelectMenu.oid == data.oid);
                    getDataBinding().countLayout.countAddition.setBackgroundResource(preSelectMenu != null ? (optionMenu.type == OPTION_TYPE_MAIN && data.price == preSelectMenu.price ? R.drawable.btn_quantity_plus : R.drawable.btn_quantity_black_add) : R.drawable.btn_quantity_black_add);
                    getDataBinding().countLayout.countSubtract.setBackgroundResource(preSelectMenu != null ? (optionMenu.type == OPTION_TYPE_MAIN && data.price == preSelectMenu.price ? R.drawable.btn_quantity_minus : R.drawable.btn_quantity_black_sub) : R.drawable.btn_quantity_black_sub);
                }
            };
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setBindViewHolder(BasicListHolder holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return optionMenu.type == OPTION_TYPE_MAIN ? LIST_TYPE_RADIO : (optionMenu.multiSelect ? LIST_TYPE_CHECK : LIST_TYPE_RADIO);
    }

    private ShopOptionSelectMenu getShopOptionSelectMenu(View view) {
        return getPosition(view) != NO_VALUE ? getObservableArrayList().get(getPosition(view)) : null;
    }

    @Override
    public void onClick(View view) {
        // 멀티 선택 ( optionMenu.multiSelect 값에 따른 사이드 싱글 선택 멀티선택)
        ViewDataBinding viewDataBinding = getViewDataBinding(view);
        ShopOptionSelectMenu selectMenu = getShopOptionSelectMenu(view);
        CompoundButton compoundButton = (CompoundButton) view;
        if (selectMenu != null) {
            if (optionMenu.type == 1) {
                /* 메인메뉴의 무조건 1개 선택, 선택 해제가 불가능!
                *  터치리스너연결로 인해 같은 버튼 눌렀을 때 가격변화가 있음.
                *  preSelectMenu는 메뉴상세 진입시 해당하는 가격을 기본으로 선택하게 되어있음. */
                MDEBUG.debug("here!!!?");
                int preOid = preSelectMenu != null ? preSelectMenu.oid : NO_VALUE;
                if (preOid != optionMenu.oid) {
                    ShopMenuOptionRadioBinding radioBinding = (ShopMenuOptionRadioBinding) viewDataBinding;
                    optionListener.onSingleRadio(optionMenu, true, preSelectMenu, selectMenu, optionMenu.oid,0);
                    radioChange.setSelectMenuOid(selectMenu.oid);

                    setTouchEnable(radioBinding.countLayout.countWrapper, compoundButton.isChecked());
                }
            } else {
                /* 멀티 선택 이든 싱글 선택이든 선택 해제가 가능! */
                if (optionMenu.multiSelect) {
                    // 멀티 선택
                    ShopMenuOptionBinding checkBinding = (ShopMenuOptionBinding) viewDataBinding;
                    checkBinding.countLayout.countAddition.setBackgroundResource(compoundButton.isChecked() ? R.drawable.btn_quantity_plus : R.drawable.btn_quantity_black_add);
                    checkBinding.countLayout.countSubtract.setBackgroundResource(compoundButton.isChecked() ? R.drawable.btn_quantity_minus : R.drawable.btn_quantity_black_sub);
                    optionListener.onMultiCheck(compoundButton.isChecked(), selectMenu, checkBinding.countLayout.countView.currentCount(), optionMenu.oid);

                    setTouchEnable(checkBinding.countLayout.countWrapper, compoundButton.isChecked());
                } else {
                    // 싱글 선택
                    ShopMenuOptionRadioBinding radioBinding = (ShopMenuOptionRadioBinding) viewDataBinding;
                    MDEBUG.debug("here?????");
                    CustomRadioButton radioButton = (CustomRadioButton) view;
                    optionListener.onSingleRadio(optionMenu, radioButton.isChecked() != radioButton.preChecked,preSelectMenu, selectMenu, optionMenu.oid
                    ,radioBinding.countLayout.countView.currentCount());
                    radioChange.setSelectMenuOid(radioButton.isChecked() != radioButton.preChecked ? selectMenu.oid : NO_VALUE);

                    setTouchEnable(radioBinding.countLayout.countWrapper, radioButton.isChecked());
                }
            }
            preSelectMenu = selectMenu;
        }
    }

    private void setTouchEnable(ConstraintLayout layout, boolean isTouch) {
        layout.setClickable(isTouch);
        layout.setEnabled(isTouch);
        layout.setFocusable(isTouch);
    }

    @Override
    public void changeCount(CustomCountView view, boolean isPlus) {
        ShopOptionSelectMenu selectMenu = getShopOptionSelectMenu(view);
        ViewDataBinding viewDataBinding = getViewDataBinding(view);
        CompoundButton compoundButton = null;

        if (viewDataBinding instanceof ShopMenuOptionBinding) {
            compoundButton = ((ShopMenuOptionBinding) viewDataBinding).optionMultiSelect;
        } else if (viewDataBinding instanceof ShopMenuOptionRadioBinding) {
            compoundButton = ((ShopMenuOptionRadioBinding) viewDataBinding).optionSingleSelect;
        }

        if (selectMenu != null && compoundButton != null && compoundButton.isChecked()) {
            selectMenu.setCount(isPlus ? selectMenu.count + 1 : selectMenu.count - 1);
            optionListener.onOptionCount(isPlus, selectMenu, optionMenu.oid);
        }
    }
}
