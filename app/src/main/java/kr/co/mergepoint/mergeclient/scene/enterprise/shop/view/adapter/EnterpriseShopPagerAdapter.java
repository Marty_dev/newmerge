package kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.EnterpriseShopActivity;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.model.EnterpriseShopModel;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.fragment.EnterpriseShopInfoFragment;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.fragment.EnterpriseShopMenuFragment;
import kr.co.mergepoint.mergeclient.scene.shop.view.fragment.ShopMenuFragment;

/**
 * Created by 1017sjg on 2017. 6. 23..
 */

public class EnterpriseShopPagerAdapter extends FragmentPagerAdapter {

    private String[] title;
    private Fragment[] fragments = new Fragment[2];

    public EnterpriseShopPagerAdapter(EnterpriseShopActivity activity, EnterpriseShopModel basemodel) {
        super(activity.getSupportFragmentManager());

        title = activity.getResources().getStringArray(R.array.enterprise_shop_detail_tab);
        fragments[0] = EnterpriseShopInfoFragment.newInstance();
        fragments[1] = EnterpriseShopMenuFragment.newInstance(activity,basemodel);
    }

    public EnterpriseShopInfoFragment getInfoFragment() {
        return (EnterpriseShopInfoFragment) getItem(0);
    }

    public EnterpriseShopMenuFragment getMenuFragment() {
        return (EnterpriseShopMenuFragment) getItem(1);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }
}
