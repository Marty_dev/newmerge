package kr.co.mergepoint.mergeclient.scene.data.login;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.BR;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

/**
 * Created by 1017sjg on 2017. 6. 16..
 */

public class Member extends BaseObservable implements Parcelable {

    @SerializedName("oid")
    private int oid;

    @SerializedName("companyRef")
    private int companyRef;

    @SerializedName("companyRegistState")
    private int companyRegistState;

    @SerializedName("companyName")
    private String companyName;

    @SerializedName("roles")
    private ArrayList<String> roles;

    @SerializedName("id")
    private String id;

    private String password;

    @SerializedName("name")
    private String name;

    @SerializedName("state")
    private int state;

    @SerializedName("phone")
    private String phone;

    @SerializedName("email")
    private String email;

    @SerializedName("postcode")
    private String postcode;

    @SerializedName("address01")
    private String address01;

    @SerializedName("address02")
    private String address02;

    @SerializedName("grade")
    private int grade;

    @SerializedName("rate")
    private int rate;

    @SerializedName("cart")
    private int cart;

    @SerializedName("birthday")
    private ArrayList<Integer> birthday;

    @SerializedName("gender")
    private int gender;

    @SerializedName("buyPoints")
    private int buyPoints;

    @SerializedName("points")
    private int points;

    @SerializedName("plannedPoints")
    private int plannedPoints;

    @SerializedName("mainCardName")
    private String mainCardName;

    @SerializedName("mainCardNickname")
    private String mainCardNickname;

    @SerializedName("needRegist")
    private boolean needRegist;

    @SerializedName("create")
    private boolean create;

    @SerializedName("nextPrice")
    private int nextPrice;

    @SerializedName("fcmKey")
    private String fcmKey;

    @SerializedName("instaId")
    private String instaId;

    @SerializedName("facebookId")
    private String facebookId;

    @SerializedName("socialProviderId")
    private ArrayList<String> socialProviderId;

    @SerializedName("emailConfirm")
    private boolean emailConfirm;

    @SerializedName("phoneConfirm")
    private boolean phoneConfirm;

    @SerializedName("marketingAgree")
    private boolean marketingAgree;

    @SerializedName("smsAgree")
    private boolean smsAgree;

    @SerializedName("emailAgree")
    private boolean emailAgree;

    @SerializedName("perInfoAgree")
    private boolean perInfoAgree;

    @SerializedName("crowdPayApproveType")
    private int crowdPayApproveType;

    @SerializedName("approveRef")
    private int approveRef;

    @SerializedName("approveName")
    private String approveName;

    @SerializedName("subscribeAlarmed")
    private boolean subscribeAlarmed;

    public int EnterPoint;
    public int personalPoint;
    public boolean isCompanyMember = false;
    public int alarmCount;

    /* isSocial은 앱 내에서 회원가입 폼으로 진입이 소셜로 진입했는지 회원가입을 눌러서 진입했는지를 구분하기 위한 Flag
    *  직렬화, 역직렬화에서 제외하기 위해 transient 추가 */
    private transient boolean isSocial;

    public Member() {
        this.oid = NO_VALUE;
        this.gender = 0;
        this.create = true;
        this.isSocial = false;
    }

    public Member(int oid, String name, int points, int grade, int cart, boolean needRegist, int state) {
        this.oid = oid;
        this.name = name;
        this.points = points;
        this.grade = grade;
        this.cart = cart;
        this.needRegist = needRegist;
        this.state = state;
    }

    public Member(Member member) {
        this.oid = member.getOid();
        this.id = member.getId();
        this.phone = member.getPhone();
        this.name = member.getName();
        this.postcode = member.getPostcode();
        this.address01 = member.getAddress01();
        this.address02 = member.getAddress02();
        this.email = member.getEmail();
        this.create = member.isCreate();
        this.isSocial = true;
    }

    public Member(int oid, String id, String name, String phone, String email, String postCode, String address, String detailedAddress, boolean create, boolean needRegist) {
        this.oid = oid;
        this.id = id;
        this.phone = phone;
        this.name = name;
        this.postcode = postCode;
        this.address01 = address;
        this.address02 = detailedAddress;
        this.email = email;
        this.create = create;
        this.isSocial = true;
        this.needRegist = needRegist;
    }

    private Member(Parcel in) {
        oid = in.readInt();
        companyRef = in.readInt();
        companyRegistState = in.readInt();
        companyName = in.readString();

        roles = new ArrayList<>();
        in.readList(roles, String.class.getClassLoader());

        id = in.readString();
        password = in.readString();
        name = in.readString();
        state = in.readInt();
        phone = in.readString();
        email = in.readString();
        postcode = in.readString();
        address01 = in.readString();
        address02 = in.readString();
        grade = in.readInt();
        rate = in.readInt();
        cart = in.readInt();

        birthday = new ArrayList<>();
        in.readList(birthday, Integer.class.getClassLoader());

        gender = in.readInt();
        buyPoints = in.readInt();
        points = in.readInt();
        plannedPoints = in.readInt();
        mainCardName = in.readString();
        mainCardNickname = in.readString();
        needRegist = in.readInt() == 1;
        create = in.readInt() == 1;
        nextPrice = in.readInt();

        fcmKey = in.readString();
        instaId = in.readString();
        facebookId = in.readString();
        socialProviderId = new ArrayList<>();
        in.readList(socialProviderId, String.class.getClassLoader());

        emailConfirm = in.readInt() == 1;
        phoneConfirm = in.readInt() == 1;
        marketingAgree = in.readInt() == 1;
        smsAgree = in.readInt() == 1;
        emailAgree = in.readInt() == 1;
        perInfoAgree = in.readInt() == 1;
        crowdPayApproveType = in.readInt();
        approveRef = in.readInt();
        approveName = in.readString();
        subscribeAlarmed = in.readInt() == 1;
        isSocial = in.readInt() == 1;
    }

    public static final Creator<Member> CREATOR = new Creator<Member>() {
        @Override
        public Member createFromParcel(Parcel in) {
            return new Member(in);
        }

        @Override
        public Member[] newArray(int size) {
            return new Member[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(oid);
        dest.writeInt(companyRef);
        dest.writeInt(companyRegistState);
        dest.writeString(companyName);
        dest.writeList(roles);
        dest.writeString(id);
        dest.writeString(password);
        dest.writeString(name);
        dest.writeInt(state);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(postcode);
        dest.writeString(address01);
        dest.writeString(address02);
        dest.writeInt(grade);
        dest.writeInt(rate);
        dest.writeInt(cart);
        dest.writeList(birthday);
        dest.writeInt(gender);
        dest.writeInt(buyPoints);
        dest.writeInt(points);
        dest.writeInt(plannedPoints);
        dest.writeString(mainCardName);
        dest.writeString(mainCardNickname);
        dest.writeInt(needRegist ? 1 : 0);
        dest.writeInt(create ? 1 : 0);
        dest.writeInt(nextPrice);
        dest.writeString(fcmKey);
        dest.writeString(instaId);
        dest.writeString(facebookId);
        dest.writeList(socialProviderId);

        dest.writeInt(emailConfirm ? 1 : 0);
        dest.writeInt(phoneConfirm ? 1 : 0);
        dest.writeInt(marketingAgree ? 1 : 0);
        dest.writeInt(smsAgree ? 1 : 0);
        dest.writeInt(emailAgree ? 1 : 0);
        dest.writeInt(perInfoAgree ? 1 : 0);

        dest.writeInt(crowdPayApproveType);
        dest.writeInt(approveRef);
        dest.writeString(approveName);
        dest.writeInt(subscribeAlarmed ? 1 : 0);
        dest.writeInt(isSocial ? 1 : 0);
    }

    @Bindable
    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
        notifyPropertyChanged(BR.oid);
    }

    public ArrayList<String> getRoles() {
        return roles;
    }

    public int getCompanyRef() {
        return companyRef;
    }

    public int getCompanyRegistState() {
        return companyRegistState;
    }

    public String getCompanyName() {
        return companyName;
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
        notifyPropertyChanged(BR.points);
    }

    @Bindable
    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
        notifyPropertyChanged(BR.grade);
    }

    @Bindable
    public int getCart() {
        return cart;
    }

    public void setCart(int cart) {
        this.cart = cart;
        notifyPropertyChanged(BR.cart);
    }

    public boolean isNeedRegist() {
        return needRegist;
    }

    public void setNeedRegist(boolean needRegist) {
        this.needRegist = needRegist;
    }

    public boolean isCreate() {
        return create;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Bindable
    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
        notifyPropertyChanged(BR.postcode);
    }

    @Bindable
    public String getAddress01() {
        return address01;
    }

    public void setAddress01(String address01) {
        this.address01 = address01;
        notifyPropertyChanged(BR.address01);
    }

    @Bindable
    public String getAddress02() {
        return address02;
    }

    public void setAddress02(String address02) {
        this.address02 = address02;
        notifyPropertyChanged(BR.address02);
    }

    @Bindable
    public ArrayList<Integer> getBirthday() {
        return birthday;
    }

    public void setBirthday(ArrayList<Integer> birthday) {
        this.birthday = birthday;
        notifyPropertyChanged(BR.birthday);
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    @Bindable
    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
        notifyPropertyChanged(BR.gender);
    }

    public String getMainCardName() {
        return mainCardName;
    }

    public void setMainCardName(String mainCardName) {
        this.mainCardName = mainCardName;
    }

    public String getMainCardNickname() {
        return mainCardNickname;
    }

    public void setMainCardNickname(String mainCardNickname) {
        this.mainCardNickname = mainCardNickname;
    }

    public int getBuyPoints() {
        return buyPoints;
    }

    public void setBuyPoints(int buyPoints) {
        this.buyPoints = buyPoints;
    }

    public int getNextPrice() {
        return nextPrice;
    }

    public void setNextPrice(int nextPrice) {
        this.nextPrice = nextPrice;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFcmKey() {
        return fcmKey;
    }

    public void setFcmKey(String fcmKey) {
        this.fcmKey = fcmKey;
    }

    public int getPlannedPoints() {
        return plannedPoints;
    }

    public void setPlannedPoints(int plannedPoints) {
        this.plannedPoints = plannedPoints;
    }

    public String getInstaId() {
        return instaId;
    }

    public void setInstaId(String instaId) {
        this.instaId = instaId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public ArrayList<String> getSocialProviderId() {
        return socialProviderId;
    }

    public void setSocialProviderId(ArrayList<String> socialProviderId) {
        this.socialProviderId = socialProviderId;
    }

    public boolean isSocial() {
        return isSocial;
    }

    public void setSocial(boolean social) {
        isSocial = social;
    }

    public boolean isEmailConfirm() {
        return emailConfirm;
    }

    public void setEmailConfirm(boolean emailConfirm) {
        this.emailConfirm = emailConfirm;
    }

    public boolean isPhoneConfirm() {
        return phoneConfirm;
    }

    public void setPhoneConfirm(boolean phoneConfirm) {
        this.phoneConfirm = phoneConfirm;
    }

    public boolean isMarketingAgree() {
        return marketingAgree;
    }

    public void setMarketingAgree(boolean marketingAgree) {
        this.marketingAgree = marketingAgree;
    }

    public boolean isSmsAgree() {
        return smsAgree;
    }

    public void setSmsAgree(boolean smsAgree) {
        this.smsAgree = smsAgree;
    }

    public boolean isEmailAgree() {
        return emailAgree;
    }

    public void setEmailAgree(boolean emailAgree) {
        this.emailAgree = emailAgree;
    }

    public boolean isPerInfoAgree() {
        return perInfoAgree;
    }

    public void setPerInfoAgree(boolean perInfoAgree) {
        this.perInfoAgree = perInfoAgree;
    }

    public int getCrowedPayApproveType() {
        return crowdPayApproveType;
    }

    public int getApproveRef() {
        return approveRef;
    }

    public String getApproveName() {
        return approveName;
    }

    public boolean isSubscribeAlarmed() {
        return subscribeAlarmed;
    }

    public Map<String, Object> getMemberMap() {
        Map<String, Object> user = new HashMap<>();
        // 필수 정보
        user.put("oid", oid);
        user.put("id", id);
        if (password != null && !password.isEmpty() && password.length() > 0)
            user.put("password", password);
        user.put("phone", phone);
        user.put("name", name);
        user.put("create", create);

        if (gender != 0)
            user.put("gender", gender);
        if (birthday != null && birthday.size() == 3)
            user.put("strBirthday", String.format(Locale.getDefault(), "%d-%02d-%02d", birthday.get(0), birthday.get(1), birthday.get(2)));

        // 부가 정보 (선택)
        if (postcode != null && !postcode.isEmpty() && postcode.length() > 0)
            user.put("postcode", postcode);
        if (address01 != null && !address01.isEmpty() && address01.length() > 0)
            user.put("address01", address01);
        if (address02 != null && !address02.isEmpty() && address02.length() > 0)
            user.put("address02", address02);
        if (email != null && !email.isEmpty() && email.length() > 0)
            user.put("email", email);

        // 약관 및 정보 제공 동의 사항
        user.put("emailConfirm", isEmailConfirm());         /* ID 중복 확인 */
        user.put("phoneConfirm", isPhoneConfirm());         /* 핸드폰 인증 확인 */

        user.put("marketingAgree", isMarketingAgree());     /* 마케팅 수신 동의 */
        user.put("smsAgree", isSmsAgree());                 /* SMS 수신 동의*/
        user.put("emailAgree", isEmailConfirm());           /* 메일 수신 동의 */
        user.put("perInfoAgree", isPerInfoAgree());         /* 개인정보 동의 */

        return user;
    }

    @Override
    public boolean equals(Object obj) {
        Member member = (Member) obj;
        return oid == member.oid
                && Objects.equals(password, member.password)
                && Objects.equals(name, member.name)
                && Objects.equals(phone, member.phone)
                && Objects.equals(email, member.email)
                && Objects.equals(postcode, member.postcode)
                && Objects.equals(address01, member.address01)
                && Objects.equals(address02, member.address02)
                && birthday == member.birthday
                && gender == member.gender;
    }
}
