package kr.co.mergepoint.mergeclient.scene.franchise.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.MergeBaseAdapter;
import kr.co.mergepoint.mergeclient.scene.franchise.adapter.viewholder.Franchise_ViewHolder;
import kr.co.mergepoint.mergeclient.scene.franchise.data.Franchises;
import kr.co.mergepoint.mergeclient.scene.franchise.view.FranchiseShopListActivity;

import static kr.co.mergepoint.mergeclient.scene.franchise.view.FranchiseShopListActivity.OID_KEY;
import static kr.co.mergepoint.mergeclient.scene.franchise.view.FranchiseShopListActivity.TITLE_KEY;

/**
 * User: Marty
 * Date: 2018-10-02
 * Time: 오전 10:17
 * Description:
 */
public class FranchiseAdapter extends MergeBaseAdapter<Franchises,Franchise_ViewHolder>{
    public FranchiseAdapter(BaseActivity mCon) {
        super(R.layout.franchise_item, mCon);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Franchise_ViewHolder viewHolder;
        View v = inflater.inflate(mLayout,parent,false);
        viewHolder = new Franchise_ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Franchise_ViewHolder v  = (Franchise_ViewHolder)holder;
        Franchises item = arrayList.get(position);
        v.nameView.setText(arrayList.get(position).name);
        Picasso.with(mCon).load(MergeApplication.getMergeApplication().getImageUrlFiltered(item.imgurl))
                .into(v.logoView);
        v.contentview.setTag(item.oid);

        v.contentview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent inte = new Intent(mCon,
                        FranchiseShopListActivity.class);
                inte.putExtra(OID_KEY,(int)view.getTag());
                inte.putExtra(TITLE_KEY,item.name);
                mCon.startActivity(inte);
            }
        });
    }

}
