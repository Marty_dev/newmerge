package kr.co.mergepoint.mergeclient.scene.myzonelist.presenter;

import android.Manifest;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BasePresenter;
import kr.co.mergepoint.mergeclient.application.common.LoadRecyclerView;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocation;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocationListener;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.ShopInfo;
import kr.co.mergepoint.mergeclient.scene.data.splash.StreetInfo;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.myzonelist.model.MyZoneListModel;
import kr.co.mergepoint.mergeclient.scene.myzonelist.presenter.callback.MyZoneListCallback;
import kr.co.mergepoint.mergeclient.scene.myzonelist.view.MyZoneListView;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.ShopListAdapter;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;

import static android.app.Activity.RESULT_OK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ADD;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INITIAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PERMISSIONS_REQUEST_LOCATION;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_FAVORITE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_OID;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class MyZoneListPresenter extends BasePresenter<MyZoneListView, MyZoneListModel, MyZoneListCallback> implements LoadRecyclerView.LoadRecyclerViewListener, UserLocationListener {

    private boolean isLogin;
    private UserLocation userLocation;
    private Runnable payEndResultRunnable;

    public MyZoneListPresenter(MyZoneListView baseView, MyZoneListModel baseModel, MyZoneListCallback callback, UserLocation userLocation) {
        super(baseView, baseModel, callback);
        this.userLocation = userLocation;
    }

    @Override
    protected void setBindingProperty() {
        baseView.binding.setTitle("My Zone");
        baseView.binding.setOnClick(this);
        baseView.binding.setListListener(this);
        baseView.binding.setListAdapter(new ShopListAdapter());
    }

    public void onCreate() {
        userLocation.startLocationUpdates();
        isLogin = MergeApplication.isLoginState();
    }

    public void onResume() {
        if (isLogin != MergeApplication.isLoginState())
            userLocation.startLocationUpdates();
        baseView.animCart();
    }

    public void onPostResume() {
        if (payEndResultRunnable != null)
            new Handler().postDelayed(payEndResultRunnable, 500);
    }

    public void onStop() {
        payEndResultRunnable = null;
    }
    int paymentRef;
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
                if (resultCode == RESULT_OK) {
                    if (requestCode == PAY_RESULT_REQUEST) {
                        paymentRef = data.getIntExtra("paymentRef",-1);
                        MDEBUG.debug("payment REf : " + paymentRef);
                        MDEBUG.debug("is Multy : " + data.getBooleanExtra("isMulti",false));
                        payEndResultRunnable = () -> baseView.openPaymentResult(paymentRef,data.getBooleanExtra("isMulti",false));
                    } else if (requestCode == SHOP_INFO_REQUEST) {
                        boolean isFavorite = data.getBooleanExtra(SHOP_FAVORITE, false);
                int oid = data.getIntExtra(SHOP_OID, NO_VALUE);
                ShopListAdapter shopListAdapter = baseView.getShopListAdapter();
                shopListAdapter.singleShopFavoriteUpdate(oid, isFavorite);
            }
        }
    }

    /* 위치 권한 */
    @Override
    protected void grantedLocation() {
        userLocation.startLocationUpdates();
    }

    @Override
    protected void deniedLocation() {
        baseView.binding.myZoneList.setVisibility(View.GONE);
        baseView.binding.notSearch.setVisibility(View.VISIBLE);
    }

    /* 위치 권한 요청 및 위치에 따른 리스트 업데이트 */
    @Override
    public void requestPermissions() {
        baseView.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_LOCATION);
    }

    @Override
    public void startLoadList(Location location) {
        baseView.showLoading();
        baseModel.getListFilter().setLocation(location);
        baseView.binding.myZoneList.loadShopList(INITIAL, baseModel.getListFilter().getMyZoneListFilter());
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.shop_list_cart:
                baseView.openShoppingBasket();
                break;
            /* 결제 완료 페이지의 버튼 */
            case R.id.payment_result_history:
                baseView.openActivity(UsageActivity.class);
                break;
            case R.id.payment_result_main:
                baseView.openClearTaskActivity(MainActivity.class);
                break;
        }
    }

    /* 점포 리스트 터치 */
    @Override
    public void onTouchItem(RecyclerView recyclerView, View view, int position) {
        ShopListAdapter adapter = baseView.getShopListAdapter();
        baseView.openShopDetail(adapter.getObservableArrayList().get(position));
    }

    /* 점포 리스트 로드 */
    @Override
    public void onLoadItem(LoadRecyclerView loadRecyclerView, int currentPage) {
        baseView.showLoading();
        baseView.binding.myZoneList.loadShopList(ADD, baseModel.getListFilter().getLoadFilter(currentPage));
    }

    @Override
    public void onInitSuccess(ShopInfo shopInfo) {
        ShopListAdapter adapter = baseView.getShopListAdapter();
        adapter.updateShopList(shopInfo.getShopList());

        if (baseView.binding.myZoneList.getListLoadScrollListener() != null)
            baseView.binding.myZoneList.getListLoadScrollListener().resetScroll();

        baseView.binding.myZoneList.setTotalPage(shopInfo.getTotalPage());
        baseView.binding.notSearch.setVisibility(shopInfo.getShopList().size() > 0 ? View.GONE : View.VISIBLE);
        baseView.hideLoading();
    }

    @Override
    public void onAddSuccess(final ShopInfo shopInfo) {
        final ShopListAdapter adapter = baseView.getShopListAdapter();
        baseView.activity.runOnUiThread(() -> {
            adapter.addShopList(shopInfo.getShopList());
            baseView.binding.myZoneList.setTotalPage(shopInfo.getTotalPage());
        });
        baseView.hideLoading();
    }

    @Override
    public void onResponseFailure(@NonNull Call<ShopInfo> call) {
        baseView.showAlert(R.string.retry);
        baseView.binding.notSearch.setVisibility(View.VISIBLE);
    }
}
