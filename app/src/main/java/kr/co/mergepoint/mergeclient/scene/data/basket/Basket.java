package kr.co.mergepoint.mergeclient.scene.data.basket;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.BR;

/**
 * Created by 1017sjg on 2017. 6. 28..
 */

public class Basket extends BaseObservable{

    @SerializedName("memberRef")
    public int memberRef;

    @SerializedName("payPrice")
    public int payPrice;

    @SerializedName("shopCart")
    public ArrayList<BasketInShop> shopCart;

    public Basket() {
        shopCart = new ArrayList<>();
    }

    @Bindable
    public int getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(int payPrice) {
        this.payPrice = payPrice;
        notifyPropertyChanged(BR.payPrice);
    }
}
