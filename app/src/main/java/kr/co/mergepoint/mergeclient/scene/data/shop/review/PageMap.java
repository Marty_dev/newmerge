package kr.co.mergepoint.mergeclient.scene.data.shop.review;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by jgson on 2018. 1. 3..
 */

public class PageMap {
    @SerializedName("cse_thumbnail")
    public ArrayList<Thumbnail> cse_thumbnail;
}
