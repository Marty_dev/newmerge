package kr.co.mergepoint.mergeclient.application.common;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import kr.co.mergepoint.mergeclient.R;

/**
 * Created by 1017sjg on 2017. 8. 28..
 */

public class PagerIndicator extends LinearLayout {

    private Context context;

    //원 사이의 간격
    private int itemMargin = 10;

    //애니메이션 시간
    private int animDuration = 250;

    private int defaultCircle;
    private int selectCircle;

    private ImageView[] imageDot;

    public void setItemMargin(int itemMargin) {
        this.itemMargin = itemMargin;
    }

    public void setAnimDuration(int animDuration) {
        this.animDuration = animDuration;
    }

    public PagerIndicator(Context context) {
        super(context);
        init(context);
    }

    public PagerIndicator(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PagerIndicator(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PagerIndicator(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        this.defaultCircle = R.drawable.page_indicator_non_select;
        this.selectCircle = R.drawable.page_indicator_select;
    }

    public void createDotPaner(int defaultCircle, int selectCircle, int count) {
        this.defaultCircle = defaultCircle;
        this.selectCircle = selectCircle;
        createDotPanel(count);
    }

    /**
     * 기본 점 생성
     * @param count 점의 갯수
     */
    public void createDotPanel(int count) {
        imageDot = new ImageView[count];

        for (int i = 0; i < count; i++) {

            imageDot[i] = new ImageView(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.topMargin = itemMargin;
            params.bottomMargin = itemMargin;
            params.leftMargin = itemMargin;
            params.rightMargin = itemMargin;
            params.gravity = Gravity.CENTER;

            imageDot[i].setLayoutParams(params);
            imageDot[i].setImageResource(defaultCircle);
            imageDot[i].setTag(imageDot[i].getId(), false);
            this.addView(imageDot[i]);
        }

        //첫인덱스 선택
        selectDot(0);
    }


    /**
     * 선택된 점 표시
     * @param position
     */
    public void selectDot(int position) {

        for (int i = 0; i < imageDot.length; i++) {
            if (i == position) {
                imageDot[i].setImageResource(selectCircle);
                selectScaleAnim(imageDot[i],1f,1.2f);
            } else {

                if((boolean) imageDot[i].getTag(imageDot[i].getId())){
                    imageDot[i].setImageResource(defaultCircle);
                    defaultScaleAnim(imageDot[i], 1.2f, 1f);
                }
            }
        }
    }


    /**
     * 선택된 점의 애니메이션
     * @param view
     * @param startScale
     * @param endScale
     */
    public void selectScaleAnim(View view, float startScale, float endScale) {
        Animation anim = new ScaleAnimation(
                startScale, endScale,
                startScale, endScale,
                Animation.RELATIVE_TO_SELF, 0.9f,
                Animation.RELATIVE_TO_SELF, 0.9f);
        anim.setFillAfter(true);
        anim.setDuration(animDuration);
        view.startAnimation(anim);
        view.setTag(view.getId(),true);
    }


    /**
     * 선택되지 않은 점의 애니메이션
     * @param view
     * @param startScale
     * @param endScale
     */
    public void defaultScaleAnim(View view, float startScale, float endScale) {
        Animation anim = new ScaleAnimation(
                startScale, endScale,
                startScale, endScale,
                Animation.RELATIVE_TO_SELF, 0.9f,
                Animation.RELATIVE_TO_SELF, 0.9f);
        anim.setFillAfter(true);
        anim.setDuration(animDuration);
        view.startAnimation(anim);
        view.setTag(view.getId(),false);
    }
}
