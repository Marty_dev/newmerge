package kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder;

import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import kr.co.mergepoint.mergeclient.databinding.QuestionsGroupBinding;
import kr.co.mergepoint.mergeclient.databinding.ShopMenuGroupBinding;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder.Menu;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class GroupQuestionHolder extends GroupViewHolder {

    private QuestionsGroupBinding questionsGroupBinding;

    public GroupQuestionHolder(QuestionsGroupBinding questionsGroupBinding) {
        super(questionsGroupBinding.getRoot());
        this.questionsGroupBinding = questionsGroupBinding;
    }

    public void setQuestionsBinding(Questions group) {
        questionsGroupBinding.setTitle(group.getTitle());
    }

    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
            animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate = new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(10);
        rotate.setFillAfter(true);
        questionsGroupBinding.arrow.setAnimation(rotate);
    }

    private void animateCollapse() {
        RotateAnimation rotate = new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(10);
        rotate.setFillAfter(true);
        questionsGroupBinding.arrow.setAnimation(rotate);
    }
}
