package kr.co.mergepoint.mergeclient.scene.register.model;

import java.util.Map;

import kr.co.mergepoint.mergeclient.api.UserApi;
import kr.co.mergepoint.mergeclient.application.common.BaseModel;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.register.IdConfirm;
import kr.co.mergepoint.mergeclient.scene.data.register.PhoneConfirm;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import okhttp3.ResponseBody;
import retrofit2.Callback;

/**
 * Created by jgson on 2017. 6. 5..
 */

public class RegisterModel extends BaseModel{

    private UserApi userApi;

    public RegisterModel() {
        userApi = retrofit.create(UserApi.class);
    }

    public void registerUser(Map<String, Object> registerFormData, Callback<ResponseObject<Member>> callback) {
        userApi.register(registerFormData).enqueue(callback);
    }

    public void subscripedId(String email, Callback<IdConfirm> callback) {
        userApi.subscribed(email).enqueue(callback);
    }

    public void phoneConfirm(String phoneNum, Callback<ResponseObject<String>> callback) {
        userApi.requestPhone(phoneNum).enqueue(callback);
    }

    public void phoneSubscripedConfirm(String phoneNum, String subscripedNum, Callback<PhoneConfirm> callback) {
        userApi.phoneSubscribed(phoneNum, subscripedNum).enqueue(callback);
    }

    /* 탈퇴한 회원 복구하기 */
    public void restoreUser(Callback<ResponseObject<Member>> callback) {
        userApi.restoreUser().enqueue(callback);
    }

    /* 로그아웃 */
    public void logout(Callback<ResponseBody> callback) {
        userApi.logout().enqueue(callback);
    }
}
