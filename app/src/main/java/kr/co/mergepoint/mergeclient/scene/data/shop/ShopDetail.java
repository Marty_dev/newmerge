package kr.co.mergepoint.mergeclient.scene.data.shop;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Map;

import kr.co.mergepoint.mergeclient.BR;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class ShopDetail extends BaseObservable {

    @SerializedName("oid")
    public int oid;

    @SerializedName("streetCodeRef")
    public String streetCodeRef;

    @SerializedName("shopName")
    public String shopName;

    @SerializedName("shopPhone")
    public String shopPhone;

    @SerializedName("averageAppr")
    public int averageAppr;

    @SerializedName("themeCodeRef")
    public ArrayList<String> themeCodeRef;

    @SerializedName("categoryCodeRef")
    public ArrayList<String> categoryCodeRef;

    @SerializedName("favorite")
    public boolean favorite;

    @SerializedName("favoriteCount")
    public int favoriteCount;

    @SerializedName("origin")
    public String origin;

    @SerializedName("likeCount")
    public int likeCount;

    @SerializedName("shopImage")
    public ArrayList<String> shopImage;

    @SerializedName("preExImage")
    public ArrayList<String> preExImage;

//    @SerializedName("mainMenus")
//    public ArrayList<ShopChildInfo> mainMenus;

    @SerializedName("latitude")
    public double latitude;

    @SerializedName("consummerRef")
    public int consummerRef;

    @SerializedName("longitude")
    public double longitude;

    @SerializedName("introduce")
    public String introduce;

    @SerializedName("mergesPickRef")
    public int mergesPickRef;

    @SerializedName("address01")
    public String address01;

    @SerializedName("address02")
    public String address02;

    @SerializedName("businessHours")
    public String businessHours;

    @SerializedName("shopClosingDay")
    public String shopClosingDay;

    @SerializedName("breakTime")
    public String breakTime;

    @SerializedName("tHoliday")
    public String tHoliday;

    @SerializedName("strPriceRange")
    public String strPriceRange;

    @SerializedName("additional")
    public Map<String, Boolean> additional;

    @SerializedName("maxLikeCount")
    public int maxLikeCount;

    @SerializedName("givenLikeCount")
    public int givenLikeCount;

    @SerializedName("paid")
    public int paid;

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public ArrayList<String> getThemeCodeRef() {
        return themeCodeRef;
    }

    public String getShopClosingDay() {
        return shopClosingDay;
    }

    public String getBreakTime() {
        return breakTime;
    }

    @Bindable
    public int getGivenLikeCount() {
        return givenLikeCount;
    }

    public void setGivenLikeCount(int givenLikeCount) {
        this.givenLikeCount = givenLikeCount;
        notifyPropertyChanged(BR.givenLikeCount);
    }

    @Bindable
    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
        notifyPropertyChanged(BR.shopName);
    }

    @Bindable
    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
        notifyPropertyChanged(BR.shopPhone);
    }

    @Bindable
    public int getAverageAppr() {
        return averageAppr;
    }

    public void setAverageAppr(int averageAppr) {
        this.averageAppr = averageAppr;
        notifyPropertyChanged(BR.averageAppr);
    }

    @Bindable
    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
        notifyPropertyChanged(BR.introduce);
    }

    @Bindable
    public String getBusinessHours() {
        return businessHours;
    }

    public void setBusinessHours(String businessHours) {
        this.businessHours = businessHours;
        notifyPropertyChanged(BR.businessHours);
    }

    @Bindable
    public Map<String, Boolean> getAdditional() {
        return additional;
    }

    public void setAdditional(Map<String, Boolean> additional) {
        this.additional = additional;
        notifyPropertyChanged(BR.additional);
    }

    @Bindable
    public int getFavoriteCount() {
        return favoriteCount;
    }

    public void setFavoriteCount(int favoriteCount) {
        this.favoriteCount = favoriteCount;
        notifyPropertyChanged(BR.favoriteCount);
    }

    @Bindable
    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
        notifyPropertyChanged(BR.likeCount);
    }
}
