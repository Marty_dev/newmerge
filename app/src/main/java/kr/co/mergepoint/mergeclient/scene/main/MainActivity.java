package kr.co.mergepoint.mergeclient.scene.main;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.inject.Inject;

import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.CustomRecyclerView;
import kr.co.mergepoint.mergeclient.application.common.ExpandableItemListener;
import kr.co.mergepoint.mergeclient.application.common.OnTypeListener;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocationListener;
import kr.co.mergepoint.mergeclient.scene.data.main.Notice;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPayItem;
import kr.co.mergepoint.mergeclient.scene.main.dagger.DaggerMainComponent;
import kr.co.mergepoint.mergeclient.scene.main.dagger.MainModule;
import kr.co.mergepoint.mergeclient.scene.main.presenter.MainPresenter;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.NoticeFragment;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.CrowdPayHistoryListener;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.DelegationUserListener;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.EnterprisePointListener;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.SettingsCrowedApprovalListener;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.SearchCrowdPayUserListener;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static com.kakao.util.maps.helper.Utility.getPackageInfo;

public class MainActivity extends BaseActivity
        implements CustomRecyclerView.CustomRecyclerViewTouch, ExpandableItemListener.ExpandableTouchListener,
        OnTypeListener, NoticeFragment.OnNotice, UserLocationListener, SettingsCrowedApprovalListener,
        SearchCrowdPayUserListener, EnterprisePointListener, CrowdPayHistoryListener, DelegationUserListener {

    @Inject public MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerMainComponent.builder().mainModule(new MainModule(this)).build().inject(this);
        presenter.onCreate();

        MDEBUG.debug(getKeyHash(this));


    }

    public static String getKeyHash(final Context context) {
        PackageInfo packageInfo = getPackageInfo(context, PackageManager.GET_SIGNATURES);
        if (packageInfo == null)
            return null;

        for (Signature signature : packageInfo.signatures) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                return Base64.encodeToString(md.digest(), Base64.NO_WRAP);
            } catch (NoSuchAlgorithmException e) {
                MDEBUG.debug("Unable to get MessageDigest. signature=" + signature);
            }
        }
        return null;
    }
    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        presenter.onPostResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    public void requestPermissions() {
        presenter.requestPermissions();
    }

    @Override
    public void startLoadList(Location location) {
        presenter.startLoadList(location);
    }

    @Override
    protected void onActivityClick(View view) {
        presenter.onClick(view);
    }

    @Override
    public void onTouchItem(RecyclerView recyclerView, View view, int position) {
        presenter.onListTouchItem(recyclerView, view, position);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onChildTouch(ChildViewHolder childViewHolder) {
        presenter.onTouchEvent(childViewHolder);
    }

    @Override
    public void onTypeClick(@Properties.TouchType int type, String data) {
        presenter.onTypeClick(type, data);
    }

    @Override
    public void onNoticeClick(Notice notice) {
        presenter.onNoticeClick(notice);
    }

    @Override
    public void onClickApprovalType(int type) {
        presenter.onClickApprovalType(type);
    }

    @Override
    public void onClickApprovalType(int type, int approvalDelegationUser, String approvalDelegationUserName) {
        presenter.onClickApprovalType(type, approvalDelegationUser, approvalDelegationUserName);
    }

    @Override
    public void updateCrowedMemberTable(int type, String searchString) {
        presenter.updateCrowedMemberTable(type, searchString);
    }

    @Override
    public void changeEnterprisePointHistory(int type, int period) {
        presenter.changeEnterprisePointHistory(type, period);
    }

    @Override
    public void changeCrowdPayHistory(int type, int period) {
        presenter.changeCrowdPayHistory(type, period);
    }

    @Override
    public void crowdPayHistoryDetail(CrowdPayItem crowdPayItem) {
        presenter.crowdPayHistoryDetail(crowdPayItem);
    }

    @Override
    public void updateDelegationTable(int type, String searchString) {
        presenter.updateDelegationTable(type, searchString);
    }
}
