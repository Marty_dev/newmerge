package kr.co.mergepoint.mergeclient.api;

import java.util.ArrayList;
import java.util.Map;

import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.ShopInfo;
import kr.co.mergepoint.mergeclient.scene.franchise.data.Franchises;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by 1017sjg on 2017. 6. 15..
 */

public interface StreetApi {
    @POST("listShop")
    @FormUrlEncoded
    Call<ShopInfo> getStreetInfo(@FieldMap Map<String, Object> search);

    @GET("listFranchise")
    Call<ResponseObject<ArrayList<Franchises>>>  getFranchisesList();
}
