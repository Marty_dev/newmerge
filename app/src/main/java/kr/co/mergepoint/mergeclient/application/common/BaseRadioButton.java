package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;

import kr.co.mergepoint.mergeclient.R;

/**
 * Created by 1017sjg on 2017. 6. 30..
 */

public class BaseRadioButton extends android.support.v7.widget.AppCompatRadioButton {

    public BaseRadioButton(Context context) {
        super(context);
        init(context);
    }

    public BaseRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BaseRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        Typeface spoqa = ResourcesCompat.getFont(context, R.font.spoqa);
        setTypeface(spoqa);
    }
}
