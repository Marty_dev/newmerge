package kr.co.mergepoint.mergeclient.scene.login.presenter.callback;

import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import okhttp3.ResponseBody;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public interface LoginCallbackInterface {

    /* 소셜 로그인 */
    MergeCallback<ResponseBody> socialLoginCallback();

    /* ID, PW 로그인*/
    MergeCallback<ResponseObject<Member>> idLoginCallback();

    /* 구글 로그인 응답 */
    MergeCallback<ResponseObject<Member>> googleResponseCallback();

    /* 비밀번호 찾기 */
    MergeCallback<ResponseObject<String>> forgotCallback();

    /* 인스타 그램 회원가입 메일 보내기 */
    MergeCallback<ResponseObject<Member>> sendRegistMail();

    /* 회원 탈퇴 처리한 회원 복구하기 */
    MergeCallback<ResponseObject<Member>> restoreUser();

    /* 로그아웃 */
    MergeCallback<ResponseBody> getLogoutCallback();
}
