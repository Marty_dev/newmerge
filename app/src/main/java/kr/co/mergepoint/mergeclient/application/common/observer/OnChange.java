package kr.co.mergepoint.mergeclient.application.common.observer;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class OnChange<T> implements Publisher<Observer<T>>{

    private ArrayList<Observer<T>> radioObservers;
    private T oid;

    public OnChange() {
        radioObservers = new ArrayList<>();
    }

    @Override
    public void add(Observer<T> observer) {
        radioObservers.add(observer);
    }

    @Override
    public void delete(Observer<T> observer) {
        radioObservers.remove(observer);
    }

    @Override
    public void notifyObserver() {
        for (Observer<T> observer : radioObservers) {
            observer.onTouch(oid);
        }
    }

    public T getOid() {
        return oid;
    }

    public void setOid(T oid) {
        this.oid = oid;
        notifyObserver();
    }
}
