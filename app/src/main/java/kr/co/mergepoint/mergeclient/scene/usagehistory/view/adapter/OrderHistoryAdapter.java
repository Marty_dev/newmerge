package kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.UsageHistoryOrderItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;

/**
 * Created by jgson on 2017. 7. 29..
 */

public class OrderHistoryAdapter extends BasicListAdapter<BasicListHolder<UsageHistoryOrderItemBinding, ShopPayment>, ShopPayment> {

    private View.OnClickListener onClickListener;

    public OrderHistoryAdapter(UsagePayment usagePayment, View.OnClickListener onClickListener) {
        super(usagePayment.shopPayment);
        this.onClickListener = onClickListener;
    }

    @Override
    public BasicListHolder<UsageHistoryOrderItemBinding, ShopPayment> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.usage_history_order_item, parent, false);
        UsageHistoryOrderItemBinding orderItemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<UsageHistoryOrderItemBinding, ShopPayment>(orderItemBinding) {
            @Override
            public void setDataBindingWithData(ShopPayment data) {
                getDataBinding().setShopPayment(data);
                getDataBinding().setClick(onClickListener);
                getDataBinding().nextShopList.setTag(data.shopRef + "|"+data.consummerRef);
                getDataBinding().setMenuAdapter(new MenuHistoryAdapter(data.menu, null));
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<UsageHistoryOrderItemBinding, ShopPayment> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
