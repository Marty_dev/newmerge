package kr.co.mergepoint.mergeclient.scene.cardmanagement.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.CardManagementActivity;

/**
 * Created by jgson on 2017. 6. 5..
 */

@CardScope
@Component(modules = CardModule.class)
public interface CardComponent {
    void inject(CardManagementActivity activity);
}
