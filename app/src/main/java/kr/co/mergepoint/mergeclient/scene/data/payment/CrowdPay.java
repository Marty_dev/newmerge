package kr.co.mergepoint.mergeclient.scene.data.payment;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.Observable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.BR;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class CrowdPay extends BaseObservable {

    @SerializedName("oid")
    public int oid;

    @SerializedName("payShopRef")
    public int payShopRef;

    @SerializedName("requestRef")
    public int requestRef;

    @SerializedName("memberRef")
    public int memberRef;

    @SerializedName("memberName")
    public String memberName;

    @SerializedName("requesterName")
    public String requesterName;

    @SerializedName("levelName")
    public String levelName;

    @SerializedName("departName")
    public String departName;

    @SerializedName("approveState")
    public int approveState;

    @SerializedName("paymentPrice")
    public int paymentPrice;

    @SerializedName("usePoints")
    public int usePoints;

    @SerializedName("returnPoints")
    public int returnPoints;

    @SerializedName("allocatePoint")
    public int allocatePoint;

    @SerializedName("reqDateTime")
    public ArrayList<Integer> reqDateTime;

    @SerializedName("comPayPoints")
    public ArrayList<Ticket> comPayPoints;

    @Bindable
    public int getApproveState() {
        return approveState;
    }

    public void setApproveState(int approveState) {
        this.approveState = approveState;
        notifyPropertyChanged(BR.approveState);
    }
}
