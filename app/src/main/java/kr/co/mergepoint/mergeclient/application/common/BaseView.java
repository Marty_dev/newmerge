package kr.co.mergepoint.mergeclient.application.common;

import android.annotation.TargetApi;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Build;
import android.os.Handler;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.scene.data.splash.CategoryInfo;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;
import kr.co.mergepoint.mergeclient.scene.data.splash.StreetInfo;
import kr.co.mergepoint.mergeclient.scene.login.LoginActivity;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UP_DOWN;
import static kr.co.mergepoint.mergeclient.application.common.Properties.requiredLoginActivities;
import static kr.co.mergepoint.mergeclient.application.common.Properties.requiredLoginFragments;

/**
 * Created by jgson on 2017. 6. 5..
 */

public abstract class BaseView<T extends BaseActivity,  K extends BasePresenter, S extends ViewDataBinding> {
    public T activity;
    public S binding;
    public Toolbar toolbar;

    public BaseView(T activity, int layout) {
        init(activity, layout);
    }

    protected void init(T activity, int layout) {
        this.binding = DataBindingUtil.setContentView(activity, layout);
        this.activity = activity;
        this.toolbar = setToolbar();

        activity.setSupportActionBar(toolbar);
        if (activity.getSupportActionBar() !=  null)
            activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    protected abstract Toolbar setToolbar();

    public void removeAllFocus() {
        getContentView().clearFocus();
    }

    public void hideKeyboard() {
        activity.hideKeyboard();
    }

    public void hideKeyboard(View view) {
        view.clearFocus();
        activity.hideKeyboard();
    }

    protected void showKeyboard() {
        activity.showKeyboard();
    }

    protected void showKeyboard(View view) {
        view.requestFocus();
        activity.showKeyboard();
    }

    public ArrayList<String> getStreetArray() {
        return MergeApplication.getInitialInfo() != null ? MergeApplication.getInitialInfo().getStreetName() : new ArrayList<>();
    }

    public ArrayList<CategoryInfo> getCategoryArray() {
        return MergeApplication.getInitialInfo() != null ? MergeApplication.getInitialInfo().getCategory() : new ArrayList<>();
    }

    public StreetInfo getStreetInfo(String streetName) {
        return MergeApplication.getInitialInfo() != null ? MergeApplication.getInitialInfo().getStreet(streetName) : null;
    }

    public BaseFragment findFragmentByTag(String tag) {
        return (BaseFragment) activity.getSupportFragmentManager().findFragmentByTag(tag);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissions(String[] permissions, int request) {
        ActivityCompat.requestPermissions(activity, permissions, request);
    }

    protected int getEnterAnim(@Properties.FragmentAnim int anim) {
        return anim == UP_DOWN ? R.anim.slide_from_down : R.anim.slide_from_right;
    }

    protected int getExitAnim(@Properties.FragmentAnim int anim) {
        return anim == UP_DOWN ? 0 : R.anim.slide_to_left;
    }

    protected int getPopEnterAnim(@Properties.FragmentAnim int anim) {
        return anim == UP_DOWN ? 0 : R.anim.slide_pop_from_right;
    }

    protected int getPopExitAnim(@Properties.FragmentAnim int anim) {
        return anim == UP_DOWN ? R.anim.slide_to_down : R.anim.slide_pop_to_right;
    }

    /* Fragment */
    public void openFragment(@Properties.FragmentAnim int fragmentAnim, int layout, Fragment fragment, String tag) {
        hideKeyboard();
        if (Arrays.asList(requiredLoginFragments).contains(fragment.getClass()) && !MergeApplication.isLoginState()) {
            activity.startActivity(new Intent(activity, LoginActivity.class));
        } else {
            // TODO: java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState
            activity.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(getEnterAnim(fragmentAnim), getExitAnim(fragmentAnim), getPopEnterAnim(fragmentAnim), getPopExitAnim(fragmentAnim))
                    .replace(layout, fragment, tag)
                    .addToBackStack(tag).commit();
        }
    }

    protected void openFragment(@Properties.FragmentAnim int fragmentAnim, int layout, Fragment fragment, String removeTag, String replaceTag) {
        if (Arrays.asList(requiredLoginFragments).contains(fragment.getClass()) && !MergeApplication.isLoginState()) {
            activity.startActivity(new Intent(activity, LoginActivity.class));
        } else {
            hideKeyboard();
            removeFragment(removeTag);
            activity.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(getEnterAnim(fragmentAnim), getExitAnim(fragmentAnim), getPopEnterAnim(fragmentAnim), getPopExitAnim(fragmentAnim))
                    .replace(layout, fragment, replaceTag)
                    .addToBackStack(replaceTag).commit();
        }
    }

    protected void openAddFragment(@Properties.FragmentAnim int fragmentAnim, int layout, Fragment fragment, String tag) {
        if (Arrays.asList(requiredLoginFragments).contains(fragment.getClass()) && !MergeApplication.isLoginState()) {
            activity.startActivity(new Intent(activity, LoginActivity.class));
        } else {
            hideKeyboard();
            activity.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(getEnterAnim(fragmentAnim), getExitAnim(fragmentAnim), getPopEnterAnim(fragmentAnim), getPopExitAnim(fragmentAnim))
                    .add(layout, fragment, tag)
                    .addToBackStack(tag).commit();
        }
    }

    protected void openShareFragment(int layout, Fragment fragment, String tag) {
        activity.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.share_fade_in, R.anim.share_fade_out, R.anim.share_fade_in, R.anim.share_fade_out)
                .add(layout, fragment, tag)
                .addToBackStack(tag).commit();
    }

    protected void openMenu(int layout, Fragment fragment, String tag) {
        hideKeyboard();
        activity.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_pop_from_left, R.anim.slide_pop_to_left)
                .replace(layout, fragment, tag)
                .addToBackStack(tag).commit();
    }

    public void removeFragment(String tag) {
        Fragment fragment = findFragmentByTag(tag);
        removeFragment(fragment);
    }

    protected void removeFragment(Fragment fragment) {
        if (fragment != null) {
            activity.getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            activity.getSupportFragmentManager().popBackStack();
        }
    }

    public void refreshFragment(String tag) {
        Fragment fragment = findFragmentByTag(tag);
        refreshFragment(fragment);
    }

    private void refreshFragment(Fragment fragment) {
        activity.getSupportFragmentManager().beginTransaction()
                .detach(fragment)
                .attach(fragment)
                .commit();
    }

    /* Activity */
    private void startActivity(Boolean requiredLogin, Intent intent) {
        if (requiredLogin && !MergeApplication.isLoginState()) {
            activity.startActivity(new Intent(activity, LoginActivity.class));
        } else {
            activity.startActivity(intent);
        }
    }

    private void startActivityForResult(Boolean requiredLogin, Intent intent, int requestCode) {
        if (requiredLogin && !MergeApplication.isLoginState()) {
            activity.startActivity(new Intent(activity, LoginActivity.class));
        } else {
            activity.startActivityForResult(intent, requestCode);
        }
    }

    public void openClearTaskActivity(Class<?> dest) {
        Intent intent = new Intent(activity, dest);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(Arrays.asList(requiredLoginActivities).contains(dest), intent);
    }

    protected void openClearTopActivity(Class<?> dest) {
        Intent intent = new Intent(activity, dest);
        intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP | FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(Arrays.asList(requiredLoginActivities).contains(dest), intent);
    }

    public void openActivity(Class<?> dest) {
        Intent intent = new Intent(activity, dest);
        startActivity(Arrays.asList(requiredLoginActivities).contains(dest), intent);
    }

    public void openActivityForResult(Class<?> dest, int requestCode) {
        Intent intent = new Intent(activity, dest);
        startActivityForResult(Arrays.asList(requiredLoginActivities).contains(dest), intent, requestCode);
    }

    protected void openActivityForResult(Class<?> dest,  int price,int shopRef) {
        Intent intent = new Intent(activity, dest);
        intent.putExtra("price", price);
        intent.putExtra("shopRef",shopRef);
        startActivityForResult(Arrays.asList(requiredLoginActivities).contains(dest), intent, PAY_RESULT_REQUEST);
    }
    protected void openActivityForResult(Class<?> dest, Class<?> source, String name, int requestCode) {
        Intent intent = new Intent(activity, dest);
        intent.putExtra(name, source);
        startActivityForResult(Arrays.asList(requiredLoginActivities).contains(dest), intent, requestCode);
    }

    public void openActivityWithParcelable(Class<?> dest, String name, Parcelable parcelable) {
        Intent intent = new Intent(activity, dest);
        intent.putExtra(name, parcelable);
        startActivity(Arrays.asList(requiredLoginActivities).contains(dest), intent);
    }

    public void openActivityForResultWithParcelable(Class<?> dest, String name, Parcelable parcelable, int requestCode) {
        Intent intent = new Intent(activity, dest);
        intent.putExtra(name, parcelable);
        startActivityForResult(Arrays.asList(requiredLoginActivities).contains(dest), intent, requestCode);
    }

    public void openActivityWithString(Class<?> dest, String name, String string) {
        Intent intent = new Intent(activity, dest);
        intent.putExtra(name, string);
        startActivity(Arrays.asList(requiredLoginActivities).contains(dest), intent);
    }

    public void openActivityWithInt(Class<?> dest, String name, int obj) {
        Intent intent = new Intent(activity, dest);
        intent.putExtra(name, obj);
        startActivity(Arrays.asList(requiredLoginActivities).contains(dest), intent);
    }

    protected void openActivityWithClass(Class<?> dest, Class<?> source, String name) {
        Intent intent = new Intent(activity, dest);
        intent.putExtra(name, source);
        startActivity(Arrays.asList(requiredLoginActivities).contains(dest), intent);
    }

    /* Snackbar를 붙일 content */
    protected View getContentView() {
        return activity.findViewById(android.R.id.content);
    }

    /* Loading */
    public void showLoading() {
        MergeApplication.getMergeApplication().showLoading(activity);
    }

    public void hideLoading() {
        MergeApplication.getMergeApplication().hideLoading(activity);
    }

    /* 앱내의 strings에 저장된 문자열 보여주기 */
    public void showAlert(int message) {
        hideKeyboard();
        Snackbar.make(getContentView(), message, Snackbar.LENGTH_SHORT).show();
    }

    /* 서버에서 보내온 오류 메시지 보여주기 */
    public void showAlert(String message) {
        hideKeyboard();
        Snackbar.make(getContentView(), message, Snackbar.LENGTH_SHORT).show();
    }

    public String getString(int string) {
        return activity.getString(string);
    }

    public String getString(int string, Object... formatArgs) {
        return activity.getString(string, formatArgs);
    }
}
