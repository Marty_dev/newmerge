package kr.co.mergepoint.mergeclient.scene.data.payment;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class CrowdPayItem {

    @SerializedName("crowdPayRef")
    public int crowdPayRef;

    @SerializedName("attendeeCount")
    public int attendeeCount;

    @SerializedName("shopRef")
    public int shopRef;

    @SerializedName("shopName")
    public String shopName;

    @SerializedName("useState")
    public int useState;

    @SerializedName("menuList")
    public ArrayList<String> menuList;

    @SerializedName("payDateTime")
    public ArrayList<Integer> payDateTime;

    @SerializedName("useDateTime")
    public ArrayList<Integer> useDateTime;

    public ArrayList<Integer> getExpDate() {
        ArrayList<Integer> expDate = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        if (payDateTime != null && payDateTime.size() == 6) {
            calendar.set(payDateTime.get(0), payDateTime.get(1), payDateTime.get(2) , payDateTime.get(3), payDateTime.get(4), payDateTime.get(5));
            calendar.add(Calendar.DAY_OF_MONTH, 90);
        }
        expDate.add(calendar.get(Calendar.YEAR));
        expDate.add(calendar.get(Calendar.MONTH));
        expDate.add(calendar.get(Calendar.DAY_OF_MONTH));
        expDate.add(calendar.get(Calendar.HOUR));
        expDate.add(calendar.get(Calendar.MINUTE));
        expDate.add(calendar.get(Calendar.SECOND));
        return expDate;
    }
}
