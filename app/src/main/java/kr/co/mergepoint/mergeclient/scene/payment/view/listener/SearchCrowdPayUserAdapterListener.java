package kr.co.mergepoint.mergeclient.scene.payment.view.listener;

/**
 * Created by jgson on 2018. 4. 5..
 */

public interface SearchCrowdPayUserAdapterListener {
    void checkUser(int count);
}
