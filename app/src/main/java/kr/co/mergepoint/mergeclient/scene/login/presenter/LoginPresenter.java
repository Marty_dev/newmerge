package kr.co.mergepoint.mergeclient.scene.login.presenter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasePresenter;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.login.SocialSignup;
import kr.co.mergepoint.mergeclient.scene.login.model.LoginModel;
import kr.co.mergepoint.mergeclient.scene.login.presenter.callback.LoginCallback;
import kr.co.mergepoint.mergeclient.scene.login.view.LoginView;
import kr.co.mergepoint.mergeclient.scene.login.view.fragment.SocialSignupEmailFragment;
import kr.co.mergepoint.mergeclient.scene.register.RegisterActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import okhttp3.ResponseBody;
import retrofit2.Callback;

import static kr.co.mergepoint.mergeclient.application.common.Properties.FACEBOOK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.GOOGLE_SIGN_IN;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INPUT_INSTA_EMAIL_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INSTA;
import static kr.co.mergepoint.mergeclient.application.common.Properties.KAKAO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NAVER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;

/**
 * Created by jgson on 2017. 5. 31..
 */

public class LoginPresenter extends BasePresenter<LoginView, LoginModel, LoginCallback> implements TextView.OnEditorActionListener, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    public LoginPresenter(LoginView view, LoginModel model, LoginCallback callback) {
        super(view, model, callback);
    }

    public void onCreate(){

    }

    @Override
    protected void setBindingProperty() {
        baseView.binding.setTitle(baseView.getString(R.string.signin_title));
        baseView.binding.setOnClick(this);
        baseView.binding.setOnLoginListener(this);
    }

    public void onDestroy() {
        baseModel.stopGoogleApiClient(baseView.activity);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                String idToken = acct.getIdToken();
                String accessToken = acct.getServerAuthCode();
                // send token to server and validate server-side
                baseModel.googleLogin(baseCallback.googleResponseCallback(), idToken, accessToken);
                baseModel.stopGoogleApiClient(baseView.activity);
            } else {
                baseView.hideLoading();
                MDEBUG.debug("here3");
                MDEBUG.debug("google result" + result.getStatus());
                baseView.showAlert(R.string.retry);
                baseModel.stopGoogleApiClient(baseView.activity);
            }
        }
    }

    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            /* ID, PW 로그인 */
            case R.id.login:
                if (baseView.login())
                    baseModel.login(baseView.getUserID(), baseView.getUserPW(), baseCallback.idLoginCallback());
                break;
            /* 회원가입 */
            case R.id.register:
                baseView.openActivity(RegisterActivity.class);
                break;
            /* 패스워드 찾기 페이지 열기 */
            case R.id.forgot_pw:
                baseView.openForgotPasswordPage(this);
                break;
            /* 패스워드 찾기 이메일 보내기 */
            case R.id.pw_send_mail:
                if (baseView.requirePwAuthMail())
                    baseModel.forgotPassword(baseCallback.forgotCallback(), baseView.getForgotPasswordEmail());
                break;
            /* 인스타그램, 페이스북 회원가입 이메일 보내기 */
            case R.id.send_social_email:
                SocialSignup socialSignup = baseView.getSocialRegisterInfo();
                if (socialSignup != null)
                    baseModel.sendSocialRegistEmail(baseCallback.sendRegistMail(), socialSignup);
                break;
            /* 소셜 로그인 */
            case R.id.sns_google:
                baseView.showLoading();
                baseModel.startGoogleLogin(baseView.activity, baseModel.getGoogleSignInOptions(), this);
                break;
            case R.id.sns_facebook:
                socialLogin(baseCallback.socialLoginCallback(), FACEBOOK);
                break;
            case R.id.sns_kakao:
                socialLogin(baseCallback.socialLoginCallback(), KAKAO);
                break;
            case R.id.sns_naver:
                socialLogin(baseCallback.socialLoginCallback(), NAVER);
                break;
            case R.id.sns_insta:
                socialLogin(baseCallback.socialLoginCallback(), INSTA);
                break;
            /* 비밀번호 찾기에서 메일을 성공적으로 보낸 후 로그인 페이지로 가기 */
            case R.id.login_page:
                baseView.forgotPwEnd();
                break;
        }
    }

    /* 구글 로그인 연결 실패 응답 */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        /* 구글 연결 실패 */
        MDEBUG.debug("google Connect is Failed\n" + connectionResult.getErrorMessage());
        MDEBUG.debug("google Connect is Failed code\n" + connectionResult.getErrorCode());
        baseView.hideLoading();
        baseView.showAlert(R.string.retry);
        baseModel.stopGoogleApiClient(baseView.activity);
    }

    /* 가상 키보드에서 완료 이벤트 리스너 */
    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            baseView.hideKeyboard();
            if (baseView.forgotPw()) {
                if (baseView.requirePwAuthMail())
                    baseModel.forgotPassword(baseCallback.forgotCallback(), baseView.getForgotPasswordEmail());
            } else if (baseView.login()) {
                baseModel.login(baseView.getUserID(), baseView.getUserPW(), baseCallback.idLoginCallback());
            }
            return true;
        }
        return false;
    }

    private void socialLogin(Callback<ResponseBody> callback, @Properties.LoginSocial int social) {
        baseView.showLoading();
        baseModel.socialLogin(callback, social);
    }

    /* Login JavaScript 에서 실행 */
    public void signup(String memberString) {
        Member member = baseModel.decodeMember(memberString);
        /* 회원가입 페이지로 이동 */
        if (member != null) {
            member.setSocial(true);
            baseView.openSocialRegister(member);
        } else {
            MDEBUG.debug("here2");
            baseView.showAlert(R.string.retry);
        }
    }

    public void signin(boolean result, String memberString) {
        Member member = baseModel.decodeMember(memberString);
        /* 로그인 페이지 종료 후 메인 페이지로 이동 */
        if (result && member != null) {
            baseView.userStateCheck(member, () -> {
                baseView.showLoading();
                baseModel.restoreUser(baseCallback.restoreUser());
            }, () -> baseModel.logout(baseCallback.getLogoutCallback()));
        } else {
            MDEBUG.debug("here1");
            baseView.showAlert(R.string.retry);
        }
    }

    public void inputEmail(String instaId, String facebookId, String social) {
        baseView.openFragment(RIGHT_LEFT, R.id.main_wrapper, SocialSignupEmailFragment.newInstance(new SocialSignup(instaId, facebookId, social)), INPUT_INSTA_EMAIL_TAG);
    }
}
