package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.MergesPickListItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.MergesPick;

/**
 * Created by 1017sjg on 2017. 9. 13..
 */

public class MergesPickListAdapter extends BasicListAdapter<BasicListHolder<MergesPickListItemBinding, MergesPick>, MergesPick> {

    public MergesPickListAdapter(ArrayList<MergesPick> arrayList) {
        super(arrayList);
    }

    @Override
    public BasicListHolder<MergesPickListItemBinding, MergesPick> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.merges_pick_list_item, parent, false);
        MergesPickListItemBinding listItemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<MergesPickListItemBinding, MergesPick>(listItemBinding) {
            @Override
            public void setDataBindingWithData(MergesPick data) {
                getDataBinding().setMergespick(data);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<MergesPickListItemBinding, MergesPick> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
