package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.TutorialBinding;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.TutorialAdapter;

/**
 * Created by jgson on 2018. 2. 7..
 */

public class TutorialFragment extends BaseFragment implements ViewPager.OnPageChangeListener {

    private TutorialBinding tutorialBinding;

    public static TutorialFragment newInstance() {
        Bundle args = new Bundle();
        TutorialFragment fragment = new TutorialFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        tutorialBinding = DataBindingUtil.inflate(inflater, R.layout.tutorial, container,false);
        tutorialBinding.setTutorialAdapter(new TutorialAdapter());
        tutorialBinding.setTutorialPageListener(this);
        tutorialBinding.setOnClick(this);
        tutorialBinding.tutorialIndicator.createDotPaner(R.drawable.tutorial_dot_default, R.drawable.tutorial_dot_select, 4);
        return tutorialBinding.getRoot();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageScrollStateChanged(int state) {}

    @Override
    public void onPageSelected(int position) {
        tutorialBinding.tutorialIndicator.selectDot(position);
        tutorialBinding.tutorialEnd.setClickable(position == 3);
        tutorialBinding.tutorialEnd.animate().alpha(position == 3 ? 1 : 0).setDuration(300).start();
        tutorialBinding.tutorialIndicator.animate().alpha(position == 3 ? 0 : 1).setDuration(300).start();
    }
}
