package kr.co.mergepoint.mergeclient.application.common;

import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import kr.co.mergepoint.mergeclient.R;

/**
 * Created by 1017sjg on 2017. 6. 13..
 */

public class RecyclerItemListener extends GestureDetector.SimpleOnGestureListener implements RecyclerView.OnItemTouchListener {

    private RecyclerTouchListener listener;
    private GestureDetector gd;
    private RecyclerView rv;

    public interface RecyclerTouchListener {
        void onClickItem(View v, int position) ;
//        void onLongClickItem(View v, int position);
    }

    public RecyclerItemListener(final RecyclerView rv, final RecyclerTouchListener listener) {
        this.listener = listener;
        this.rv = rv;
        gd = new GestureDetector(rv.getContext(), this);
    }

//    @Override
//    public void onLongPress(MotionEvent e) {
//        View v = rv.findChildViewUnder(e.getX(), e.getY());
//        listener.onLongClickItem(v, rv.getChildAdapterPosition(v));
//    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        View v = rv.findChildViewUnder(e.getX(), e.getY());
        listener.onClickItem(v, rv.getChildAdapterPosition(v));
        return true;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View child = rv.findChildViewUnder(e.getX(), e.getY());

        return !isTouchLikeHeart(e.getRawX(), e.getRawY(), child) && (child != null && gd.onTouchEvent(e));
    }

    private boolean isTouchLikeHeart(float x, float y, View child) {
        if (child != null) {
            BookMark heart = child.findViewById(R.id.bookmark);
            if (heart != null) {
                int[] location = new int[2];
                heart.getLocationOnScreen(location);
                int heartX = location[0];
                int heartY = location[1];

                return ( x > heartX && x < (heartX + heart.getWidth())) && ( y > heartY && y < (heartY + heart.getHeight()));
            } else {
                return false;
            }
        }

        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {}

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}
}