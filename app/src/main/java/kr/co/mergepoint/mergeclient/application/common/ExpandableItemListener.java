package kr.co.mergepoint.mergeclient.application.common;

import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

/**
 * Created by 1017sjg on 2017. 6. 13..
 */

public class ExpandableItemListener extends GestureDetector.SimpleOnGestureListener implements RecyclerView.OnItemTouchListener {

    private final ExpandableTouchListener listener;
    private RecyclerView recyclerView;
    private GestureDetector gestureDetector;

    public interface ExpandableTouchListener {
        void onChildTouch(ChildViewHolder childViewHolder) ;
    }

    ExpandableItemListener(RecyclerView recyclerView, final ExpandableTouchListener listener) {
        this.listener = listener;
        this.recyclerView = recyclerView;
        gestureDetector = new GestureDetector(recyclerView.getContext(), this);
    }

    @Override
    public void onLongPress(MotionEvent e) {}

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        View v = recyclerView.findChildViewUnder(e.getX(), e.getY());
        RecyclerView.ViewHolder viewHolder = recyclerView.getChildViewHolder(v);
        if (viewHolder instanceof ChildViewHolder)
            listener.onChildTouch((ChildViewHolder) viewHolder);

        return true;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View child = rv.findChildViewUnder(e.getX(), e.getY());
        if (child != null) {
            RecyclerView.ViewHolder viewHolder = recyclerView.getChildViewHolder(child);
            return !(viewHolder instanceof GroupViewHolder) && gestureDetector.onTouchEvent(e);
        }

        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {}

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}
}