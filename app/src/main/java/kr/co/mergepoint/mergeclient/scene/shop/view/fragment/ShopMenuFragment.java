package kr.co.mergepoint.mergeclient.scene.shop.view.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.util.ArrayList;
import java.util.List;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.ExpandableItemListener;
import kr.co.mergepoint.mergeclient.databinding.ShopDetailMenuBinding;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.ShopExpandMenuAdapter;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder.Menu;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.view.PricePaymentActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_PRICE_REQUEST;

/**
 * Created by 1017sjg on 2017. 6. 23..
 */

public class ShopMenuFragment extends BaseFragment {

    private ShopDetailMenuBinding shopDetailMenuBinding;
    private ExpandableItemListener.ExpandableTouchListener expandableTouchListener;
    private static ShopActivity baseactivity;

    public static ShopMenuFragment newInstance(ShopActivity activity) {
        Bundle args = new Bundle();
        ShopMenuFragment fragment = new ShopMenuFragment();

        fragment.expandableTouchListener = activity;
        fragment.setArguments(args);
        baseactivity = activity;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        shopDetailMenuBinding = DataBindingUtil.inflate(inflater, R.layout.shop_detail_menu, container, false);

        return shopDetailMenuBinding.getRoot();
    }

    public void setMenuGroup(ArrayList<Menu> menuGroup) {
        shopDetailMenuBinding.setExpandableAdapter(new ShopExpandMenuAdapter(menuGroup, getContext()));
        shopDetailMenuBinding.setChildTouch(expandableTouchListener);
        shopDetailMenuBinding.shopPricecustom.setOnClickListener(baseactivity);
        for (int i = 0 ; i < shopDetailMenuBinding.getExpandableAdapter().getGroups().size(); i++){
            if (shopDetailMenuBinding.getExpandableAdapter().getGroups().get(i).getTitle().equals("(시그니쳐)")){
                shopDetailMenuBinding.getExpandableAdapter().toggleGroup(shopDetailMenuBinding.getExpandableAdapter().getGroups().get(i));
            }

        }


    }
    public void onMenuLoad(){
        MDEBUG.debug("expand start");
        if (shopDetailMenuBinding.getExpandableAdapter().signatureGroup != null) {
            shopDetailMenuBinding.getExpandableAdapter().toggleGroup(shopDetailMenuBinding.getExpandableAdapter().signatureGroup);
            MDEBUG.debug("signature init");
        }
        MDEBUG.debug("expand end");
    }
}
