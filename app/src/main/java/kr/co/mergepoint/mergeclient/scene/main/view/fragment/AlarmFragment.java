package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.UserApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.ContactusBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.Alarmdata;
import kr.co.mergepoint.mergeclient.scene.data.main.Contactus;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.AlarmAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 1017sjg on 2017. 8. 10..
 */

public class AlarmFragment extends BaseFragment {


    ConstraintLayout back;
    RecyclerView list;
    ImageView emptyimg;
    TextView emptytv;
    MainActivity activity;
    AlarmAdapter adapter;
    public static AlarmFragment newInstance(MainActivity onClickListener) {
        Bundle args = new Bundle();
        AlarmFragment fragment = new AlarmFragment();
        fragment.setArguments(args);
        fragment.activity = onClickListener;
        return fragment;
    }

    UserApi api;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      View view  = inflater.inflate(R.layout.activity_alarm,container,false);

      back = view.findViewById(R.id.back);
      emptyimg = view.findViewById(R.id.emptyimage);
      emptytv = view.findViewById(R.id.emptytv);
      list = (RecyclerView)view.findViewById(R.id.alarmlist);
      back.setOnClickListener(activity);
        adapter = new AlarmAdapter(R.layout.alarm_item,activity);
        list.setAdapter(adapter);
      return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = MergeApplication.getMergeAppComponent().getRetrofit().create(UserApi.class);


    }

    @Override
    public void onStart() {
        super.onStart();
        initdata();
    }

    public void initdata(){
        MergeApplication.getMergeApplication().showLoading(activity);
        api.readAlarmList().enqueue(new Callback<ArrayList<Alarmdata>>() {
            @Override
            public void onResponse(Call<ArrayList<Alarmdata>> call, Response<ArrayList<Alarmdata>> response) {

                MergeApplication.getMergeApplication().hideLoading(activity);

                if (response.isSuccessful() && response.body().size() != 0 ){
                    initListitem(response.body());
                    setVisibleEmpty(false);
                }else {
                    setVisibleEmpty(true);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Alarmdata>> call, Throwable t) {
                MergeApplication.getMergeApplication().hideLoading(activity);

            }
        });
    }

    public void setVisibleEmpty(boolean flag){
        emptytv.setVisibility(flag ? View.VISIBLE : View.GONE);
        emptyimg.setVisibility(flag ? View.VISIBLE : View.GONE);
    }
    public void initListitem(ArrayList<Alarmdata> datas){
        adapter.arrayList = datas;
        adapter.notifyDataSetChanged();
    }
}
