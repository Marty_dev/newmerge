package kr.co.mergepoint.mergeclient.scene.data.newcoupon;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * User: Marty
 * Date: 2018-09-19
 * Time: 오전 10:55
 * Description:
 */
public class CouponDetail {
    @SerializedName("oid")
    private int oid;
    @SerializedName("couponRef")
    private String couponRef;
    @SerializedName("couponNum")
    private String couponNum;
    @SerializedName("couponCategory")
    private int couponCategory;
    @SerializedName("couponName")
    private String couponName;
    @SerializedName("couponDescription")
    private String couponDescription;
    @SerializedName("detailImage")
    private String detailImage;
    @SerializedName("producerPhone")
    private String producerPhone;
    @SerializedName("couponMinPrice")
    private int couponMinPrice;
    @SerializedName("issuedDateTime")
    private ArrayList<Integer> issuedDateTime;
    @SerializedName("issueExpDate")
    private ArrayList<Integer> issueExpDate;
    @SerializedName("expDate")
    private ArrayList<Integer> expDate;
    @SerializedName("couponState")
    private int couponState;
    @SerializedName("barCode")
    private String barCode;
    @SerializedName("codeCouponRef")
    private int codeCouponRef;
    @SerializedName("issueState")
    private int issueState;
    @SerializedName("couponPrice")
    private int couponPrice;
    @SerializedName("remainPrice")
    private int remainPrice;
    @SerializedName("usableShop")
    private String usableShop;
    @SerializedName("unUsableShop")
    private String unUsableShop;
    @SerializedName("warning")
    private String warning;
    @SerializedName("validityTerm")
    private String validityTerm;
    @SerializedName("useCondition")
    private String useCondition;
    @SerializedName("refundRequest")
    private String refundRequest;

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public String getCouponRef() {
        return couponRef;
    }

    public void setCouponRef(String couponRef) {
        this.couponRef = couponRef;
    }

    public String getCouponNum() {
        return couponNum;
    }

    public void setCouponNum(String couponNum) {
        this.couponNum = couponNum;
    }

    public int getCouponCategory() {
        return couponCategory;
    }

    public void setCouponCategory(int couponCategory) {
        this.couponCategory = couponCategory;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponDescription() {
        return couponDescription;
    }

    public void setCouponDescription(String couponDescription) {
        this.couponDescription = couponDescription;
    }

    public String getDetailImage() {
        return detailImage;
    }

    public void setDetailImage(String detailImage) {
        this.detailImage = detailImage;
    }

    public String getProducerPhone() {
        return producerPhone;
    }

    public void setProducerPhone(String producerPhone) {
        this.producerPhone = producerPhone;
    }

    public int getCouponMinPrice() {
        return couponMinPrice;
    }

    public void setCouponMinPrice(int couponMinPrice) {
        this.couponMinPrice = couponMinPrice;
    }

    public ArrayList<Integer> getIssuedDateTime() {
        return issuedDateTime;
    }

    public void setIssuedDateTime(ArrayList<Integer> issuedDateTime) {
        this.issuedDateTime = issuedDateTime;
    }

    public ArrayList<Integer> getIssueExpDate() {
        return issueExpDate;
    }

    public void setIssueExpDate(ArrayList<Integer> issueExpDate) {
        this.issueExpDate = issueExpDate;
    }

    public ArrayList<Integer> getExpDate() {
        return expDate;
    }

    public void setExpDate(ArrayList<Integer> expDate) {
        this.expDate = expDate;
    }

    public int getCouponState() {
        return couponState;
    }

    public void setCouponState(int couponState) {
        this.couponState = couponState;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public int getCodeCouponRef() {
        return codeCouponRef;
    }

    public void setCodeCouponRef(int codeCouponRef) {
        this.codeCouponRef = codeCouponRef;
    }

    public int getIssueState() {
        return issueState;
    }

    public void setIssueState(int issueState) {
        this.issueState = issueState;
    }

    public int getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(int couponPrice) {
        this.couponPrice = couponPrice;
    }

    public int getRemainPrice() {
        return remainPrice;
    }

    public void setRemainPrice(int remainPrice) {
        this.remainPrice = remainPrice;
    }

    public String getUsableShop() {
        return usableShop;
    }

    public void setUsableShop(String usableShop) {
        this.usableShop = usableShop;
    }

    public String getUnUsableShop() {
        return unUsableShop;
    }

    public void setUnUsableShop(String unUsableShop) {
        this.unUsableShop = unUsableShop;
    }

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }

    public String getValidityTerm() {
        return validityTerm;
    }

    public void setValidityTerm(String validityTerm) {
        this.validityTerm = validityTerm;
    }

    public String getUseCondition() {
        return useCondition;
    }

    public void setUseCondition(String useCondition) {
        this.useCondition = useCondition;
    }

    public String getRefundRequest() {
        return refundRequest;
    }

    public void setRefundRequest(String refundRequest) {
        this.refundRequest = refundRequest;
    }
}
