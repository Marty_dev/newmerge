package kr.co.mergepoint.mergeclient.scene.enterprise.shop.view;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.Toolbar;

import com.facebook.CallbackManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.application.common.ShareFragment;
import kr.co.mergepoint.mergeclient.databinding.EnterpriseShopDetailBinding;
import kr.co.mergepoint.mergeclient.scene.data.basket.AddBasket;
import kr.co.mergepoint.mergeclient.scene.data.basket.OptionMenu;
import kr.co.mergepoint.mergeclient.scene.data.basket.SelectOption;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionMenu;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionSelectMenu;
import kr.co.mergepoint.mergeclient.scene.data.shop.DailyMenuCategory;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.EnterpriseShopActivity;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.presenter.EnterpriseShopPresenter;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.fragment.EnterpriseMenuFragment;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.fragment.EnterpriseShopMenuFragment;
import kr.co.mergepoint.mergeclient.scene.login.LoginActivity;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.PaymentResultFragment;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;
import kr.co.mergepoint.mergeclient.scene.shop.view.fragment.ShopMenuDetailFragment;
import kr.co.mergepoint.mergeclient.scene.shoplist.ShopListActivity;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.BasketActivity;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.Payment_v2_Activity;

import static android.app.Activity.RESULT_OK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_MENU_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INTENT_SHOP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MENU_DETAIL_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAYMENT_RESULT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHARE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_FAVORITE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_OID;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SOURCE_ACTIVITY;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class EnterpriseShopView extends BaseView<EnterpriseShopActivity, EnterpriseShopPresenter, EnterpriseShopDetailBinding> implements EnterpriseShopViewInterface {

    public EnterpriseShopView(EnterpriseShopActivity activity, int layout) {
        super(activity, layout);
    }

    @Override
    protected Toolbar setToolbar() {
        return binding.enterpriseShopDetailToolbarLayout.basicToolbar;
    }
    public void openPaymentByPrice(int price , int shopRef) {
        openActivityForResult(Payment_v2_Activity.class, price, shopRef);
    }
    @Override
    public void openPayment() {
        AddBasket addBasket = getSelectOptions();
        if (addBasket ==  null) {
            new MergeDialog.Builder(activity).setContent(getString(R.string.soldout_menu_text)).setCancelBtn(false).build().show();
        } else {
            Intent inte =new Intent(activity,Payment_v2_Activity.class);
            inte.putExtra(PAYMENT_RESULT,addBasket);
            //activity.startActivity(inte);
            openActivityForResultWithParcelable(Payment_v2_Activity.class, PAYMENT_RESULT, addBasket, PAY_RESULT_REQUEST);
        }
    }

    @Override
    public void openShopShared(ShopDetail shopDetail) {
        openShareFragment(R.id.shop_detail_root, ShareFragment.newInstance(
                shopDetail.shopName,
                shopDetail.introduce,
                shopDetail.shopImage.size() > 0 ? shopDetail.shopImage.get(0) : "",
                shopDetail.oid, INTENT_SHOP), SHARE);
    }

    @Override
    public void openEnterpriseMenu(ArrayList<DailyMenuCategory> dailyMenuCategories, Calendar calendar) {
        openFragment(RIGHT_LEFT, R.id.shop_detail_root, EnterpriseMenuFragment.newInstance(dailyMenuCategories, calendar), ENTERPRISE_MENU_TAG);
    }

    @Override
    public void openMenuDetail(ShopMenuDetail detail) {
        openFragment(RIGHT_LEFT, R.id.shop_detail_root, ShopMenuDetailFragment.newInstance(detail), MENU_DETAIL_TAG);
    }
    @Override
    public void openPaymentResult() {
        openFragment(RIGHT_LEFT, R.id.shop_detail_root, PaymentResultFragment.newInstance(), PAY_RESULT_TAG);
    }

    public void openPaymentResult(int paymentRef,boolean ismulti) {
        openFragment(RIGHT_LEFT, R.id.shop_detail_root, PaymentResultFragment.newInstance(activity,paymentRef,R.id.shop_detail_root,ismulti), PAY_RESULT_TAG);
    }

    @Override
    public void openShoppingBasket() {
        openActivityForResult(BasketActivity.class, ShopListActivity.class, SOURCE_ACTIVITY, PAY_RESULT_REQUEST);
    }

    @Override
    public void openMapIntent(double latitude, double longitude) {
        Uri uri = Uri.parse(String.format(Locale.ENGLISH, "geo:%f, %f", latitude, longitude));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
        if (mapIntent.resolveActivity(activity.getPackageManager()) != null)
            activity.startActivity(mapIntent);
    }

    public void openMapIntent(double latitude, double longitude,String placename) {
        Uri uri = Uri.parse(String.format(Locale.ENGLISH, "geo:%f, %f?q=%s", latitude, longitude,placename));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
        if (mapIntent.resolveActivity(activity.getPackageManager()) != null)
            activity.startActivity(mapIntent);
    }
    @Override
    public AddBasket getSelectOptions() {
        ShopMenuDetailFragment fragment = (ShopMenuDetailFragment) activity.getSupportFragmentManager().findFragmentByTag(MENU_DETAIL_TAG);
        ShopMenuDetail detail = fragment.getShopMenuDetail();

        if (detail.soldout)
            return null;

        ArrayList<OptionMenu> kindOptions = new ArrayList<>();
        for (ShopOptionMenu optionMenu: fragment.getOptionMenuArray()) {
            ArrayList<SelectOption> selectOptions = new ArrayList<>();

            for (ShopOptionSelectMenu selectMenu : optionMenu.menus) {
                if (selectMenu.getCount() > 0)
                    selectOptions.add(new SelectOption(selectMenu.oid, selectMenu.count));
            }

            if (selectOptions.size() > 0)
                kindOptions.add(new OptionMenu(optionMenu.oid, selectOptions));
        }

        return new AddBasket(detail.shopRef, detail.oid, detail.count, kindOptions.size() < 1 ? null : kindOptions,binding.getShopDetail().consummerRef);
    }

    @Override
    public EnterpriseShopMenuFragment getShopMenuFragment() {
        return binding.getPagerAdapter().getMenuFragment();
    }

    @Override
    public CallbackManager getShareFacebookCallback() {
        ShareFragment fragment = (ShareFragment) findFragmentByTag(SHARE);
        return fragment != null ? fragment.getCallbackManager() : null;
    }

    @Override
    public boolean startShoppingBasket() {
        if (MergeApplication.isLoginState()) {
            AddBasket addBasket = getSelectOptions();
            if (addBasket ==  null) {
                new MergeDialog.Builder(activity).setContent(getString(R.string.soldout_menu_text)).setCancelBtn(false).build().show();
            } else {
                return true;
            }
        } else {
            openActivityWithClass(LoginActivity.class, ShopActivity.class, SOURCE_ACTIVITY);
        }

        return false;
    }

    @Override
    public void animBasket() {
        binding.shopListCart.getCartCount(this);
    }

    @Override
    public void callShopPhone() {
        ShopDetail detail = binding.getShopDetail();
        if (detail != null)
            activity.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + detail.getShopPhone())));
    }

    @Override
    public void onBackPressed() {
        if (MergeApplication.isLoginState() && binding.getShopDetail() != null) {
            Intent shopFavorite = new Intent();
            shopFavorite.putExtra(SHOP_FAVORITE, binding.enterpriseShopDetailToolbarLayout.favoritShop.isFavorite());
            shopFavorite.putExtra(SHOP_OID, binding.getShopDetail().oid);
            activity.setResult(RESULT_OK, shopFavorite);
        }
    }

    @Override
    public void changeDailyMenuDate(Calendar calendar) {
        EnterpriseMenuFragment menuFragment = (EnterpriseMenuFragment) findFragmentByTag(ENTERPRISE_MENU_TAG);
        menuFragment.changeDailyMenuDate(calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.DAY_OF_WEEK));
    }

    @Override
    public void changeDailyMenu(ArrayList<DailyMenuCategory> menuCategories, Calendar calendar) {
        EnterpriseMenuFragment menuFragment = (EnterpriseMenuFragment) findFragmentByTag(ENTERPRISE_MENU_TAG);
        menuFragment.changeDailyMenuAdapter(menuCategories, calendar);
    }
}
