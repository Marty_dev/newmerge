package kr.co.mergepoint.mergeclient.scene.data.main;

import com.google.gson.annotations.SerializedName;

/**
 * User: Marty
 * Date: 2018-08-16
 * Time: 오후 3:29
 * Description:
 */
public class MemberGrade {

    @SerializedName("kindOrder")
    public int kindOrder;
    @SerializedName("kindName")
    public String kindName;
    @SerializedName("terms")
    public String terms;
    @SerializedName("benefitComment")
    public String benefitComment;
    @SerializedName("conditionType")
    public int conditionType;
    @SerializedName("conditionCount")
    public int conditionCount;
    @SerializedName("conditionPrice")
    public int conditionPrice;




}
