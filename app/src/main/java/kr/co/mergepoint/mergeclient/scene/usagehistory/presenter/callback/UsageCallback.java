package kr.co.mergepoint.mergeclient.scene.usagehistory.presenter.callback;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseCallback;
import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.model.UsageModel;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.UsageView;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.OrderHistoryFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.UsingFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.UsingFragmentv2;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USAGE_ORDER_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USING_MENU_TAG;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class UsageCallback extends BaseCallback<UsageView, UsageModel> implements UsageCallbackInterface{

    public UsageCallback(UsageView baseView, UsageModel baseModel) {
        super(baseView, baseModel);
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<UsagePayment>>> getHistoryCallback(final int position) {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<UsagePayment>> history = response.body();
            if (history != null && response.isSuccessful() && !history.isFailed() && history.getObject() != null) {
                baseModel.usagePayment = history.getObject();
                baseView.setPaymentPagerData(history.getObject(), position);
                int payref =  baseView.activity.getIntent().getIntExtra("Payref",-1);
                if (payref != -1){
                    baseView.activity.getIntent().putExtra("Payref",-1);
                    baseView.openFragment(RIGHT_LEFT, R.id.usage_layout, OrderHistoryFragment.newInstance(payref),USAGE_ORDER_TAG );

                }
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Integer>> getPayCancel(final boolean isPayCancel) {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Integer> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                baseView.showAlert(responseObject.getMessage());
                if (!responseObject.isFailed())
                    baseView.openPayCancelFragment(isPayCancel);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<UsagePayment>> getSendMenuCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<UsagePayment> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                UsagePayment usagePayment = responseObject.getObject();
                if (!responseObject.isFailed()) {
                    baseView.openFragment(RIGHT_LEFT, R.id.usage_layout, UsingFragmentv2.newInstance(baseView.activity, usagePayment,R.id.usage_layout), USING_MENU_TAG);
                } else {
                    if (Objects.equals(responseObject.getCode(), "USE004")) {
                        new MergeDialog.Builder(baseView.activity)
                                .setContent(responseObject.getMessage())
                                .setConfirmString(R.string.reorder)
                                .setCancelString(R.string.order_cancel)
                                .setConfirmClick(() -> baseView.openFragment(RIGHT_LEFT, R.id.usage_layout, UsingFragmentv2.newInstance(baseView.activity, usagePayment,R.id.usage_layout), USING_MENU_TAG))
                                .setCancelClick(() -> baseModel.rejectMenu(rejectMenu(), usagePayment.oid)).build().show();
                    } else {
                        baseView.showAlert(responseObject.getMessage());
                    }
                }
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Integer>> approveMenu() {
        return new MergeCallback<>(baseView, new MergeCallback.MergeCallWithFailure<ResponseObject<Integer>>() {
            @Override
            public void onResponse(Call<ResponseObject<Integer>> call, Response<ResponseObject<Integer>> response) {
                ResponseObject<Integer> responseObject = response.body();
                baseView.hideLoading();
                if (response.isSuccessful() && responseObject != null) {
                    if (!responseObject.isFailed()) {
                        baseView.openApproveResultFragment();
                    } else {
                        MDEBUG.debug("getmessage : " + responseObject.getMessage());
                        baseView.setApproveFlag(false);
                        baseView.showAlert(responseObject.getMessage());
                        if (Objects.equals(responseObject.getCode(), "USE010"))
                            baseView.resetPinCodeView();
                    }
                } else {
                    baseView.setApproveFlag(false);
                    baseView.showAlert(R.string.retry);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseObject<Integer>> call, @NonNull Throwable t) {
                baseView.hideLoading();

                baseView.setApproveFlag(false);
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Integer>> rejectMenu() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Integer> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && responseObject.isFailed())
                baseView.showAlert(R.string.retry);
        });
    }
}
