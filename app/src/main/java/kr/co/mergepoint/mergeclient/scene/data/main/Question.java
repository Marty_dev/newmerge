package kr.co.mergepoint.mergeclient.scene.data.main;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 1017sjg on 2017. 8. 14..
 */

public class Question implements Parcelable {

    private String content;

    public String getContent() {
        return content;
    }

    public Question(String content) {
        this.content = content;
    }

    private Question(Parcel in) {
        content = in.readString();
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(content);
    }
}
