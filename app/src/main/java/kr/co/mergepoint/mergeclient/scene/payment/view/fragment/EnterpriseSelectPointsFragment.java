package kr.co.mergepoint.mergeclient.scene.payment.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.application.common.CustomCountView;
import kr.co.mergepoint.mergeclient.databinding.EnterpriseSelectPointsBinding;
import kr.co.mergepoint.mergeclient.databinding.EnterpriseSelectPointsItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.MemberPoint;
import kr.co.mergepoint.mergeclient.scene.data.payment.AdaptPoint;
import kr.co.mergepoint.mergeclient.scene.data.payment.AdaptTicket;
import kr.co.mergepoint.mergeclient.scene.payment.view.adapter.TicketListAdapter;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * Created by 1017sjg on 2017. 8. 8..
 */

public class EnterpriseSelectPointsFragment extends BaseFragment implements TextView.OnEditorActionListener, CustomCountView.OnChangeCountListener {

    private EnterpriseSelectPointsBinding selectPointsBinding;
    private MemberPoint memberPoint;
    private int remainPayPrice;
    private int pointPrice;
    private int ticketPrice;
    private int totalPrice;

    public static EnterpriseSelectPointsFragment newInstance(MemberPoint memberPoint, int remainPayPrice) {
        Bundle args = new Bundle();
        EnterpriseSelectPointsFragment fragment = new EnterpriseSelectPointsFragment();
        fragment.memberPoint = memberPoint;
        fragment.remainPayPrice = remainPayPrice;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        selectPointsBinding = DataBindingUtil.inflate(inflater, R.layout.enterprise_select_points, container, false);
        selectPointsBinding.setTitle("기업포인트 사용");
        selectPointsBinding.setOnClick(this);
        selectPointsBinding.setMemberPoint(memberPoint);
        selectPointsBinding.setRemainPayPrice(remainPayPrice);
        selectPointsBinding.setTotalPoint(totalPrice);
        selectPointsBinding.setTicketAdapter(new TicketListAdapter(memberPoint.useTickets, this));
        selectPointsBinding.setOnAdaptPointListener(this);
        return selectPointsBinding.getRoot();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        EditText editText = selectPointsBinding.usedPointText;
        editText.clearFocus();
        getRootActivity().hideKeyboard();
        switch (v.getId()) {
            case R.id.enterprise_user_all_point_apply:
                int allPoint = remainPayPrice>=memberPoint.companyPoints ? memberPoint.companyPoints : remainPayPrice;
                editText.setText(getRootActivity().getString(R.string.separator_price, allPoint));
                setPointPrice(allPoint);
                break;
            case R.id.used_point_text:
            case R.id.enterprise_user_point_apply:
                String pointStr = editText.getText().toString().trim();
                pointStr = pointStr.replace("P","");
                int usePoint = pointStr.isEmpty() ? 0 : Integer.parseInt(pointStr.replaceAll(",",""));
                editText.setText(getRootActivity().getString(R.string.separator_price, usePoint));
                setPointPrice(usePoint);
                break;
        }
    }

    @Override
    public void changeCount(CustomCountView view, boolean isPlus) {
        int price = isPlus ? view.menuPrice : -view.menuPrice;
        setTicketPrice(price);
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            onClick(textView);
            return true;
        }
        return false;
    }

    private void setPointPrice(int point) {
        this.pointPrice = point;
        setTotalPrice();
    }

    private void setTicketPrice(int ticket) {
        this.ticketPrice += ticket;
        setTotalPrice();
    }

    private void setTotalPrice() {
        totalPrice = pointPrice + ticketPrice;
        selectPointsBinding.setTotalPoint(totalPrice);
    }

    public AdaptPoint getSelectPoint() {
        String pointStr = selectPointsBinding.usedPointText.getText().toString().trim();
        int usePoint;
        try {
           usePoint = pointStr.isEmpty() ? 0 : Integer.parseInt(pointStr.replaceAll(",", ""));
        }catch (Exception e){
            MDEBUG.debug(e.toString());
            pointStr = pointStr.replace("P","");
            usePoint = pointStr.isEmpty() ? 0 : Integer.parseInt(pointStr.replaceAll(",", ""));

        }
        ArrayList<AdaptTicket> tickets = new ArrayList<>();
        int ticketViews = selectPointsBinding.ticketTable.getChildCount();
        for (int i = 0; i < ticketViews; i++) {

            View view = selectPointsBinding.ticketTable.getChildAt(i);
            RecyclerView.ViewHolder viewHolder = selectPointsBinding.ticketTable.getChildViewHolder(view);
            if (viewHolder instanceof BasicListHolder) {
                BasicListHolder basicListHolder = (BasicListHolder) viewHolder;
                EnterpriseSelectPointsItemBinding itemBinding = (EnterpriseSelectPointsItemBinding) basicListHolder.getDataBinding();
                int ticketRef = (int) itemBinding.countView.getTag();
                int ticketCount = itemBinding.countView.currentCount();
                tickets.add(new AdaptTicket(ticketRef, ticketCount));
            }
        }

        return new AdaptPoint(memberPoint.crowdPayRef, memberPoint.memberRef, usePoint, tickets);
    }
}
