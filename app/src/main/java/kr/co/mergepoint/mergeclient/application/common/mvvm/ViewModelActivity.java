package kr.co.mergepoint.mergeclient.application.common.mvvm;

import android.os.Bundle;
import android.support.annotation.Nullable;

import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.mvvm.dagger.ActivityComponent;
import kr.co.mergepoint.mergeclient.application.common.mvvm.dagger.ActivityModule;
import kr.co.mergepoint.mergeclient.application.common.mvvm.dagger.DaggerActivityComponent;

import static kr.co.mergepoint.mergeclient.application.common.Properties.EXTRA_VIEW_MODEL_STATE;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public abstract class ViewModelActivity extends BaseActivity {
    private ViewModel viewModel;
    private ActivityComponent component;

    @Nullable
    protected abstract ViewModel createViewModel(@Nullable ViewModel.State savedViewModelState);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        component = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .build();

        ViewModel.State savedViewModelState = null;
        if (savedInstanceState != null)
            savedViewModelState = savedInstanceState.getParcelable(EXTRA_VIEW_MODEL_STATE);
        viewModel = createViewModel(savedViewModelState);
    }

    public final ActivityComponent getComponent() {
        return component;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (viewModel != null)
            outState.putParcelable(EXTRA_VIEW_MODEL_STATE, viewModel.getInstanceState());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (viewModel != null)
            viewModel.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (viewModel != null)
            viewModel.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (viewModel != null)
            viewModel.onStop();
    }
}
