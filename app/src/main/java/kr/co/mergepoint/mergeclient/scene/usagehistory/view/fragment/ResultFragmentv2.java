package kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.PayCancelResultBinding;
import kr.co.mergepoint.mergeclient.scene.login.LoginActivity;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.requiredLoginActivities;

/**
 * Created by 1017sjg on 2017. 7. 28..
 */

public class ResultFragmentv2 extends BaseFragment {

    private String title;
    private String contentString;

    public static ResultFragmentv2 newInstance(String title, String contentString) {
        Bundle args = new Bundle();
        ResultFragmentv2 fragment = new ResultFragmentv2();
        fragment.contentString = contentString;
        fragment.title = title;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        PayCancelResultBinding resultBinding = DataBindingUtil.inflate(inflater, R.layout.pay_cancel_result, container, false);
        resultBinding.setTitle(title);
        resultBinding.setOnClick(this);
        resultBinding.setCancelString(contentString);
        resultBinding.setIsvisible(true);
        return resultBinding.getRoot();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(rootActivity, MainActivity.class);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(Arrays.asList(requiredLoginActivities).contains(MainActivity.class), intent);
    }
    private void startActivity(Boolean requiredLogin, Intent intent) {
        if (requiredLogin && !MergeApplication.isLoginState()) {
            rootActivity.startActivity(new Intent(rootActivity, LoginActivity.class));
        } else {
            rootActivity.startActivity(intent);
        }
    }


}
