package kr.co.mergepoint.mergeclient.scene.payment.view.listener;

/**
 * Created by jgson on 2018. 3. 16..
 */

public interface CrowdApprovalListener {
    void endApproval();
}
