package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.view.View;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.WideBannerAdapter;
import kr.co.mergepoint.mergeclient.scene.data.main.Banner;

/**
 * Created by 1017sjg on 2017. 9. 12..
 */

public class MainBannerAdapter extends WideBannerAdapter {

    private ArrayList<Banner> banners;

    public ArrayList<Banner> getBanners() {
        return banners;
    }

    public MainBannerAdapter(ArrayList<Banner> banners, int emptyImg, View.OnClickListener onClickListener) {
        super(R.layout.banner_item, new ArrayList<>(), emptyImg, onClickListener);
        for (Banner banner: banners)
            imgUrl.add(banner.image);
        this.banners = banners;
    }
}
