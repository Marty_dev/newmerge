package kr.co.mergepoint.mergeclient.scene.data.shop.review;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jgson on 2018. 1. 3..
 */

public class Review {
    @SerializedName("title")
    public String title;

    @SerializedName("link")
    public String link;

    @SerializedName("displayLink")
    public String displayLink;

    @SerializedName("snippet")
    public String snippet;

    @SerializedName("thumbnail")
    public String thumbnail;
}
