package kr.co.mergepoint.mergeclient.scene.data.payment;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class AdaptPoint {

    @SerializedName("crowdPayRef")
    public int crowdPayRef;

    @SerializedName("memberRef")
    public int memberRef;

    @SerializedName("companyPoint")
    public int companyPoint;

    @SerializedName("tickets")
    public ArrayList<AdaptTicket> tickets;

    public AdaptPoint(int crowdPayRef, int memberRef, int companyPoint, ArrayList<AdaptTicket> tickets) {
        this.crowdPayRef = crowdPayRef;
        this.memberRef = memberRef;
        this.companyPoint = companyPoint;
        this.tickets = tickets;
    }
}
