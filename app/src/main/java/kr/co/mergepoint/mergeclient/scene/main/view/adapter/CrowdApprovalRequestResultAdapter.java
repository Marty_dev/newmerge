package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.CrowdApprovalRequestResultItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class CrowdApprovalRequestResultAdapter extends BasicListAdapter<BasicListHolder<CrowdApprovalRequestResultItemBinding, CrowdPay>, CrowdPay> {

    private Context context;

    public CrowdApprovalRequestResultAdapter(ArrayList<CrowdPay> arrayList, Context context) {
        super(arrayList);
        this.context = context;
    }

    @Override
    public BasicListHolder<CrowdApprovalRequestResultItemBinding, CrowdPay> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.crowd_approval_request_result_item, parent, false);
        CrowdApprovalRequestResultItemBinding itemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<CrowdApprovalRequestResultItemBinding, CrowdPay>(itemBinding) {
            @Override
            public void setDataBindingWithData(CrowdPay data) {
                String levelName = data.levelName != null ? data.levelName : "";
                String departName = data.departName != null ? data.departName : "";
                String name = String.format(Locale.getDefault(), "%s %s %s", data.memberName, levelName, departName);
                getDataBinding().setName(name);
                getDataBinding().setPointPrice(String.format(Locale.getDefault(), "%,2dP", data.allocatePoint));

                if (getObservableArrayList().size() == 1) {
                    getDataBinding().resultLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.approval_request_rect));
                } else {
                    if (getObservableArrayList().indexOf(data) == 0) {
                        getDataBinding().resultLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.approval_request_top));
                    } else if (getObservableArrayList().indexOf(data) == getObservableArrayList().size() - 1) {
                        getDataBinding().resultLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.approval_request_bottom));
                    } else {
                        getDataBinding().resultLayout.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
                    }
                }
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<CrowdApprovalRequestResultItemBinding, CrowdPay> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
