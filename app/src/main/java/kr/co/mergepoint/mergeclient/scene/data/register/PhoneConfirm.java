package kr.co.mergepoint.mergeclient.scene.data.register;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jgson on 2017. 7. 9..
 */

public class PhoneConfirm {
    @SerializedName("result")
    public boolean result;
    @SerializedName("message")
    public String message;
}
