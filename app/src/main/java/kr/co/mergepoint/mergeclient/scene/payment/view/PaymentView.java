package kr.co.mergepoint.mergeclient.scene.payment.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.application.common.SimpleWebFragment;
import kr.co.mergepoint.mergeclient.databinding.PaymentBinding;
import kr.co.mergepoint.mergeclient.databinding.PaymentShopListBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.MemberPoint;
import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.PayConfirm;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.payment.PaymentActivity;
import kr.co.mergepoint.mergeclient.scene.payment.presenter.PaymentPresenter;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.CardPaymentWebFragment;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.CrowdApprovalFragment;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.EnterpriseSelectPointsFragment;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.EnterpriseUsePointsFragment;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.SearchCrowdPayUserFragment;

import static android.app.Activity.RESULT_OK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.BASE_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CARD_PAYMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CROWED_APPROVAL_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_SELECT_POINTS_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_USE_POINTS_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INFO_PROVIDE_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INFO_PROVIDE_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SEARCH_CROWD_PAY_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UP_DOWN;
import static kr.co.mergepoint.mergeclient.application.common.Properties.WEB_URL;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class PaymentView extends BaseView<PaymentActivity, PaymentPresenter, PaymentBinding> {

    public PaymentView(PaymentActivity activity, int layout) {
        super(activity, layout);
    }

    @Override
    protected Toolbar setToolbar() {
        return null;
    }

    public boolean isConfirmUserBilling() {
        return binding.paymentNotice.confirmPaymentCheck.isChecked();
    }

    public void openCardPaymentPage() {
        if (isPayConfirm())
            openFragment(UP_DOWN, R.id.payment_layout, CardPaymentWebFragment.newInstance(BASE_URL + "payment/cardPay/" + binding.getPayConfirm().oid, activity), CARD_PAYMENT_TAG);
    }

    public void openCrowedPay(int shopPayOid, ArrayList<CrowdPay> crowdPays, boolean single) {
        for (ShopPayment shopPayment :binding.getPayConfirm().shopPayment) {
            if (shopPayment.oid == shopPayOid) {
                openFragment(RIGHT_LEFT, R.id.payment_layout, EnterpriseUsePointsFragment.newInstance(shopPayment, crowdPays, single), ENTERPRISE_USE_POINTS_FRAGMENT_TAG);
                return;
            }
        }
    }

    public void openSettingCrowedPayPoint(MemberPoint memberPoint) {
        EnterpriseUsePointsFragment usePointsFragment = (EnterpriseUsePointsFragment) findFragmentByTag(ENTERPRISE_USE_POINTS_FRAGMENT_TAG);
        openFragment(RIGHT_LEFT, R.id.payment_layout, EnterpriseSelectPointsFragment.newInstance(memberPoint, usePointsFragment.getRemainPayPrice()), ENTERPRISE_SELECT_POINTS_FRAGMENT_TAG);
    }

    public void openSearchCrowdMember() {
        openFragment(RIGHT_LEFT, R.id.payment_layout, SearchCrowdPayUserFragment.newInstance(activity), SEARCH_CROWD_PAY_FRAGMENT_TAG);
    }

    public void openCrowdApproval(ArrayList<CrowdPay> crowdPays) {
        openFragment(RIGHT_LEFT, R.id.payment_layout, CrowdApprovalFragment.newInstance(activity, crowdPays), CROWED_APPROVAL_FRAGMENT_TAG);
    }

    private boolean isPayConfirm() {
        boolean isPay = binding.getPayConfirm() != null;
        if (!isPay)
            new MergeDialog.Builder(activity).setContent(R.string.retry_payment).setCancelBtn(false).build().show();
        return isPay;
    }

    public void paymentResult(boolean isSuccess, String error) {
        if (isSuccess) {
            Intent intent = new Intent();
            activity.setResult(RESULT_OK, intent);
            activity.finish();
        } else {
            new MergeDialog.Builder(activity)
                    .setContent(error)
                    .setConfirmClick(() -> removeFragment(findFragmentByTag(CARD_PAYMENT_TAG))).setCancelBtn(false).build().show();
        }
    }

    public void setShopSelectCoupon(View view, ArrayList<Coupon> mergeCoupons) {
        PaymentShopListBinding listBinding = (PaymentShopListBinding) binding.getPaymentShopListAdapter().getViewDataBinding(view);
        ArrayList<Coupon> shopCoupons = listBinding.getShopPayment().shopCoupon;
        listBinding.getShopCouponAdapter().changeCoupons(shopCoupons, mergeCoupons);
    }

    public void setBasketSelectCoupon(ArrayList<Coupon> basketCoupons) {
        binding.getCouponAdapter().changeCoupons(basketCoupons);
    }

    public void openInfoProvideWeb() {
        if (isPayConfirm()) {
            Bundle bundle = new Bundle();
            bundle.putString(WEB_URL, INFO_PROVIDE_URL + binding.getPayConfirm().oid);
            openFragment(UP_DOWN, R.id.payment_layout, SimpleWebFragment.newInstance(bundle, getString(R.string.personal_information_provided)), INFO_PROVIDE_TAG);
        }
    }

    public EditText getPayShopPointView(View view) {
        PaymentShopListBinding listBinding = (PaymentShopListBinding) binding.getPaymentShopListAdapter().getViewDataBinding(view);
        return listBinding != null ? listBinding.usedPointText : null;
    }

    public ShopPayment getShopPayment(View view) {
        PaymentShopListBinding listBinding = (PaymentShopListBinding) binding.getPaymentShopListAdapter().getViewDataBinding(view);
        return listBinding != null ? listBinding.getShopPayment() : null;
    }

    public PayConfirm getPayConfirm() {
        return binding.getPayConfirm();
    }

    public void backToPayment() {
        removeFragment(findFragmentByTag(CROWED_APPROVAL_FRAGMENT_TAG));
        removeFragment(findFragmentByTag(ENTERPRISE_USE_POINTS_FRAGMENT_TAG));
    }
}
