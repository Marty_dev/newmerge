package kr.co.mergepoint.mergeclient.scene.usagehistory.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.AdapterView;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BasePresenter;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.application.common.RecyclerItemListener;
import kr.co.mergepoint.mergeclient.application.common.RecyclerPagerAdapter;
import kr.co.mergepoint.mergeclient.application.common.SpinnerAdapter;
import kr.co.mergepoint.mergeclient.scene.data.main.UsageFilter;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.model.UsageModel;
import kr.co.mergepoint.mergeclient.scene.usagehistory.presenter.callback.UsageCallback;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.UsageView;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.ShopHistoryFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.UsingFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.UsingFragmentv2;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static kr.co.mergepoint.mergeclient.application.common.Properties.CENTER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FCM_RECEIVER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAYMENT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USAGE_SHOP_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USING_MENU_TAG;

/**
 * Created by 1017sjg on 2017. 9. 16..
 */

public class UsagePresenter extends BasePresenter<UsageView, UsageModel, UsageCallback> implements RecyclerItemListener.RecyclerTouchListener{

    public UsagePresenter(UsageView baseView, UsageModel baseModel, UsageCallback baseCallback) {
        super(baseView, baseModel, baseCallback);
    }

    @Override
    protected void setBindingProperty() {
        baseView.binding.usageTab.setupWithViewPager(baseView.binding.usageDetailPager);
        baseView.binding.setDetailPagerListener(new TabLayout.TabLayoutOnPageChangeListener(baseView.binding.usageTab));
        baseView.binding.setTitle(baseView.getString(R.string.usage_history_title));
        baseView.binding.setOnClick(this);

        baseView.binding.setVisibleType(true);
        baseView.binding.setTabListener(baseView.activity);
        baseView.binding.setPagerAdapter(new RecyclerPagerAdapter(baseView.activity, baseView.activity.getResources().getStringArray(R.array.usage_tab)));
        baseView.binding.setTermSpinnerAdpater(new SpinnerAdapter(baseView.activity.getResources().getStringArray(R.array.usage_history_term_spinner), CENTER));
        baseView.binding.setTypeSpinnerAdpater(new SpinnerAdapter(baseView.activity.getResources().getStringArray(R.array.usage_type_spinner), CENTER));

        baseView.binding.setSpinnerListener(baseView.activity);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getStringExtra("action") == null){
                return;
            }
            /* UsageActivity 새로고침 */
            updateList();
            /* OrderHistoryFragment 새로고침 */
            baseView.updateOrderHistory();
            /* ShopHistoryFragment 새로고침 */
            baseView.updateShopHistory();
        }
    };

    public void onCreate() {
        baseView.activity.registerReceiver(receiver, new IntentFilter(FCM_RECEIVER));
    }

    public void onResume() {
        updateList();
    }

    public void onPostResume() {
        baseView.getIntent();
    }

    public void onDestroy() {
        baseView.activity.unregisterReceiver(receiver);
    }

    public void onTabSelected(TabLayout.Tab tab) {
        baseView.usageFilter.setTabType(tab.getPosition() == 0 ? PAYMENT : USE);
        baseView.binding.setVisibleType(baseView.usageFilter.isPaymentTab());
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        baseView.usageFilter.setTermOrType((String) adapterView.getTag(), position);
        updateList();
    }

    @Override
    public void onClickItem(View v, int position) {
        baseView.openHistory(baseModel.usagePayment.get(position));
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.next_shop_list:
                String tagdata = (String)v.getTag();
                int length = tagdata.indexOf("|");
                int oid = Integer.parseInt(tagdata.substring(0,length++));
                int consummer = Integer.parseInt(tagdata.substring(length));
                baseView.openShopHistory(oid,consummer);
                //baseView.openShopHistory((int) v.getTag());
                break;
            case R.id.usage_history_layout: /* 사용내역 보기 */

                baseView.openHistory(baseModel.usagePayment.get((int) v.getTag()));
                break;
            case R.id.pay_cancel:
                final boolean isPayCancel = (boolean) v.getTag();
                new MergeDialog.Builder(baseView.activity)
                        .setContent(isPayCancel ? baseView.getString(R.string.pay_cancel_confirm_text) : baseView.getString(R.string.point_refund_confirm_text))
                        .setConfirmClick(() -> {
                            int payOid = baseView.getPayCancelOid();
                            if (payOid != NO_VALUE) {
                                if (isPayCancel) {
                                    baseModel.payCancel(baseCallback.getPayCancel(true), baseView.getPayCancelOid());
                                } else {
                                    baseModel.pointRefund(baseCallback.getPayCancel(false), baseView.getPayCancelOid());
                                }
                            } else {
                                baseView.showAlert(R.string.retry);
                            }
                }).build().show();
                break;
            case R.id.using_button:
                baseView.showUsageMenuAlert(() -> {
                    baseView.showLoading();
                    ShopHistoryFragment shopHistoryFragment = (ShopHistoryFragment) baseView.findFragmentByTag(USAGE_SHOP_TAG);
                    baseModel.sendUsageMenu(baseCallback.getSendMenuCallback(), shopHistoryFragment.getUseMenu());
                });
                break;
            case R.id.open_main:
                baseView.openClearTaskActivity(MainActivity.class);
                break;
            /*승인하기*/
            case R.id.enterprise_confirm_btn:
            case R.id.approve_btn:
                baseView.showLoading();
                baseModel.approveMenu(baseCallback.approveMenu(), baseView.getApproveInfo());
                break;
            /*뒤로가기 및 취소를 눌렀을 때 승인대기중인 메뉴 취소하기*/
            case R.id.cancel_btn:
                baseView.activity.onBackPressed();
                break;
        }
    }

    private void updateList() {
        MDEBUG.debug("Update List");
        baseView.showLoading();
        int position = baseView.binding.usageDetailPager.getCurrentItem();

        if (position == 0) {
            baseModel.getHistory(baseCallback.getHistoryCallback(position), baseView.usageFilter);
        } else {
            baseModel.getUsedHistory(baseCallback.getHistoryCallback(position), baseView.usageFilter);
        }
    }

    /*Fragment 생명주기 컨트롤*/
    public void onFragmentDestroy() {
        UsingFragmentv2 usingFragment = (UsingFragmentv2) baseView.findFragmentByTag(USING_MENU_TAG);
        if (usingFragment != null && !usingFragment.isApprove());
            //baseModel.rejectMenu(baseCallback.rejectMenu(), baseView.getRejectOid());
    }
    public void onNewIntent(Intent data){
        UsingFragmentv2 usingFragment = (UsingFragmentv2) baseView.findFragmentByTag(USING_MENU_TAG);
        if (usingFragment != null){
            usingFragment.onNewIntent(data);
        }
    }
}
