package kr.co.mergepoint.mergeclient.scene.data.basket;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by 1017sjg on 2017. 6. 28..
 */

public class SelectOption implements Parcelable {

    @Expose public int optionDetailRef;
    @Expose public int optionCount;

    public SelectOption(int optionDetailRef, int optionCount) {
        this.optionDetailRef = optionDetailRef;
        this.optionCount = optionCount;
    }

    private SelectOption(Parcel in) {
        optionDetailRef = in.readInt();
        optionCount = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SelectOption> CREATOR = new Creator<SelectOption>() {
        @Override
        public SelectOption createFromParcel(Parcel in) {
            return new SelectOption(in);
        }

        @Override
        public SelectOption[] newArray(int size) {
            return new SelectOption[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(optionDetailRef);
        dest.writeInt(optionCount);
    }
}
