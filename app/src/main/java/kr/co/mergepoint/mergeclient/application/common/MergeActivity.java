package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * User: Marty
 * Date: 2018-10-02
 * Time: 오전 10:22
 * Description:
 */
public class MergeActivity extends BaseActivity {

    public Context mCon = this;
    public BaseActivity BASE = this;
    @Override
    protected void onActivityClick(View view) {

    }

    public void showAlert(Object msg){
        if (msg instanceof Integer){
            Snackbar.make(findViewById(android.R.id.content),(int)msg,Snackbar.LENGTH_SHORT).show();

        }else{
            Snackbar.make(findViewById(android.R.id.content),(String)msg,Snackbar.LENGTH_SHORT).show();

        }

    }
}
