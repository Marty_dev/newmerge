package kr.co.mergepoint.mergeclient.scene.payment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;

import javax.inject.Inject;

import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.script.WebReturnScriptListener;
import kr.co.mergepoint.mergeclient.scene.payment.dagger.DaggerPaymentComponent;
import kr.co.mergepoint.mergeclient.scene.payment.dagger.PaymentModule;
import kr.co.mergepoint.mergeclient.scene.payment.presenter.PaymentPresenter;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.CrowdApprovalListener;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.SearchCrowdPayUserListener;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class PaymentActivity extends BaseActivity implements  SearchCrowdPayUserListener, CrowdApprovalListener {

    @Inject PaymentPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerPaymentComponent.builder().paymentModule(new PaymentModule(this)).build().inject(this);
        presenter.onCreate();

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void success() {
        presenter.paymentSuccess();
    }

    @Override
    public void failure(String error) {
        presenter.paymentFailure(error);
    }

    @Override
    protected void onActivityClick(View view) {
        presenter.onClick(view);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        try {
            return presenter.onKeyDown(keyCode, event);
        } catch (NullPointerException ignore) {}

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        presenter.onNewIntent(intent);
    }

    @Override
    public void updateCrowedMemberTable(int type, String searchString) {
        presenter.updateCrowedMemberTable(type, searchString);
    }

    @Override
    public void endApproval() {
        presenter.endApproval();
    }
}
