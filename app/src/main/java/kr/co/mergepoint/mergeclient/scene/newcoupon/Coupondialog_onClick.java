package kr.co.mergepoint.mergeclient.scene.newcoupon;

import android.view.View;

import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponV1;

/**
 * User: Marty
 * Date: 2018-09-19
 * Time: 오전 11:31
 * Description:
 */
public interface Coupondialog_onClick {
    public void onConfirm(View v, CouponV1 data);
}
