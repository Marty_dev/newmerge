package kr.co.mergepoint.mergeclient.application.common;

/**
 * Created by 1017sjg on 2017. 6. 20..
 */

public abstract class BaseCallback<T extends BaseView, S extends BaseModel> {
    public T baseView;
    public S baseModel;

    public BaseCallback(T baseView, S baseModel) {
        this.baseView = baseView;
        this.baseModel = baseModel;
    }
}
