package kr.co.mergepoint.mergeclient.scene.data.register;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 1017sjg on 2017. 7. 20..
 */

public class IdConfirm {
    @SerializedName("id")
    public String id;
    @SerializedName("result")
    public boolean result;
}
