package kr.co.mergepoint.mergeclient.scene.shoplist.model;

import net.daum.mf.map.api.MapPoint;

import java.util.ArrayList;
import java.util.Map;

import kr.co.mergepoint.mergeclient.api.MapApi;
import kr.co.mergepoint.mergeclient.application.common.BaseModel;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Filter;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.data.splash.CategoryInfo;
import kr.co.mergepoint.mergeclient.scene.data.splash.StreetInfo;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Callback;

import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MAP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_TOTAL;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class ShopListModel extends BaseModel {

    private MapApi mapApi;

    private Filter listFilter;
    private Filter mapFilter;
    private StreetInfo streetInfo;
    private String category;

    public ShopListModel() {
        mapApi = retrofit.create(MapApi.class);
        this.listFilter = new Filter(LIST);
        this.mapFilter = new Filter(MAP);
    }

    public Filter getListFilter() {
        return listFilter;
    }

    public Filter getMapFilter() {
        return mapFilter;
    }

    public String[] getCategoryString(ArrayList<CategoryInfo> categoryInfos) {
        String[] title = new String[categoryInfos != null ? categoryInfos.size() + 1 : 0];
        if (categoryInfos != null && categoryInfos.size() > 0) {
            title[0] = SHOP_TOTAL;
            for (int i = 1; i < title.length; i++)
                title[i] = categoryInfos.get(i - 1).name;
        }
        return title;
    }

    public void initFilter(StreetInfo streetInfo, ArrayList<CategoryInfo> infos) {
        listFilter.setStreetInfo(streetInfo);
        listFilter.setCategoryInfos(infos);
        mapFilter.setStreetInfo(streetInfo);
        mapFilter.setCategoryInfos(infos);
        this.streetInfo = streetInfo;
    }

    public Map<String, Object> getListSearchString(String search) {
        listFilter.setSearchString(search);
        return listFilter.getSentenceSearch();
    }

    public void setMapSearchString(String search) {
        mapFilter.setSearchString(search);
    }

    public void setStreetInfo(StreetInfo streetInfo) {
        listFilter.setStreetInfo(streetInfo);
        mapFilter.setStreetInfo(streetInfo);
        this.streetInfo = streetInfo;
    }

    public void setCategoryInfo(String category) {
        listFilter.setCategory(category);
        mapFilter.setCategory(category);
        this.category = category;
    }

    public StreetInfo getStreetInfo() {
        return streetInfo;
    }

    public String getCategoryInfo() {
        return category;
    }

    public ArrayList<String> getPriceRange(@Properties.ShopVisibleType int type) {
        return type == MAP ? mapFilter.getPriceRange() : listFilter.getPriceRange();
    }

    public ArrayList<String> getTempPriceRange(@Properties.ShopVisibleType int type) {
        return type == MAP ? mapFilter.getTempPriceRange() : listFilter.getTempPriceRange();
    }

    public void setConvenience(@Properties.ShopVisibleType int type, String name, boolean value) {
        if (type == MAP) {
            mapFilter.getConvenience().put(name, value);
        } else {
            listFilter.getConvenience().put(name, value);
        }
    }

    public void setTheme(@Properties.ShopVisibleType int type, String code) {
        if (type == MAP) {
            mapFilter.setTheme(code);
        } else {
            listFilter.setTheme(code);
        }
    }

    public void resetFilter(@Properties.ShopVisibleType int type) {
        if (type == MAP) {
            mapFilter.resetFilterSearch();
        } else {
            listFilter.resetFilterSearch();
        }
    }

    public void requestMapIntoShopList(Callback<ResponseObject<ArrayList<Shop>>> callback, MapPoint.GeoCoordinate tr, MapPoint.GeoCoordinate bl) {
        MDEBUG.debug("MapCalled");
        mapApi.getMapIntoShopList(mapFilter.getMapFilter(tr, bl)).enqueue(callback);
    }
}
