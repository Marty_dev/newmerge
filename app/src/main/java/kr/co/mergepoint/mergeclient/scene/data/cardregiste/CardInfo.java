package kr.co.mergepoint.mergeclient.scene.data.cardregiste;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public class CardInfo implements Parcelable {

    @SerializedName("oid")
    private int oid;
    @SerializedName("financeName")
    private String financeName;
    @SerializedName("nickname")
    private String nickname;
    @SerializedName("mainCard")
    private boolean mainCard;

    public CardInfo(int oid, String financeName, String financeNickName, boolean isMain) {
        this.oid = oid;
        this.financeName = financeName;
        this.nickname = financeNickName;
        this.mainCard = isMain;
    }

    public CardInfo(Parcel in) {
        oid = in.readInt();
        financeName = in.readString();
        nickname = in.readString();
        mainCard = in.readInt() == 1;
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public String getFinanceName() {
        return financeName;
    }

    public void setFinanceName(String financeName) {
        this.financeName = financeName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public boolean isMainCard() {
        return mainCard;
    }

    public void setMainCard(boolean mainCard) {
        this.mainCard = mainCard;
    }

    public static final Creator<CardInfo> CREATOR = new Creator<CardInfo>() {
        @Override
        public CardInfo createFromParcel(Parcel in) {
            return new CardInfo(in);
        }

        @Override
        public CardInfo[] newArray(int size) {
            return new CardInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(oid);
        dest.writeString(financeName);
        dest.writeString(nickname);
        dest.writeInt(mainCard ? 1 : 0);
    }
}
