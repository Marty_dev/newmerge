package kr.co.mergepoint.mergeclient.scene.shoplist.presenter;

import android.Manifest;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.SeekBar;
import android.widget.TextView;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BasePresenter;
import kr.co.mergepoint.mergeclient.application.common.LoadRecyclerView;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.application.common.SpinnerAdapter;
import kr.co.mergepoint.mergeclient.application.common.StateButton;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocation;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocationListener;
import kr.co.mergepoint.mergeclient.databinding.SpinnerDropdownStreetBinding;
import kr.co.mergepoint.mergeclient.databinding.SpinnerNormalStreetBinding;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.SelectItem;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.ShopInfo;
import kr.co.mergepoint.mergeclient.scene.data.splash.StreetInfo;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.shoplist.model.ShopListModel;
import kr.co.mergepoint.mergeclient.scene.shoplist.presenter.callback.ShopListCallback;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.ShopListView;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.ShopMapPOIItem;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.ShopListAdapter;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;

import static android.app.Activity.RESULT_OK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ADD;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ASC;
import static kr.co.mergepoint.mergeclient.application.common.Properties.DESC;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FILTER_PRICE_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FILTER_THEME_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INITIAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MAP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PERMISSIONS_REQUEST_LOCATION;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_FAVORITE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_OID;
import static kr.co.mergepoint.mergeclient.application.common.Properties.STREET;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class ShopListPresenter extends BasePresenter<ShopListView, ShopListModel, ShopListCallback>
        implements TextView.OnEditorActionListener, AdapterView.OnItemSelectedListener, LoadRecyclerView.LoadRecyclerViewListener, DrawerLayout.DrawerListener, SeekBar.OnSeekBarChangeListener, UserLocationListener {

    private boolean isLogin;
    private UserLocation userLocation;
    private Runnable payEndResultRunnable;
    private boolean SpinnerFlag =true;

    public ShopListPresenter(ShopListView baseView, ShopListModel baseModel, ShopListCallback callback, UserLocation userLocation) {
        super(baseView, baseModel, callback);
        this.userLocation = userLocation;
    }

    @Override
    protected void setBindingProperty() {
        baseView.binding.setOnClick(this);
        baseView.binding.setSearchListener(this);
        baseView.binding.setListListener(this);
        baseView.binding.setSpinnerItemListener(this);
        baseView.binding.setFilterListener(this);
        baseView.binding.setDistanceListener(this);
        baseView.binding.setTabSelectListener(getListTabListener());
        baseView.binding.setCategory(baseModel.getCategoryString(baseView.getCategoryArray()));
        baseView.binding.setListAdapter(new ShopListAdapter());
        baseView.binding.setSpinnerAdapter(new SpinnerAdapter<SpinnerNormalStreetBinding, SpinnerDropdownStreetBinding>(baseView.getStreetStringArray(), STREET));
        baseView.binding.setSortListener(baseView.sortFilter);
    }

    public void onCreate() {
        baseView.initStreetSpinner();
        baseView.initDescription();
        baseModel.initFilter(baseView.getStreetInfo(baseView.getStreetName()), baseView.getCategoryArray());
        userLocation.startLocationUpdates();
        isLogin = MergeApplication.isLoginState();
        baseView.showLoading();
    }

    public void onResume() {
        baseView.addMapView();
        if (isLogin != MergeApplication.isLoginState())
            userLocation.startLocationUpdates();
        baseView.animCart();
    }

    public void onPostResume() {
        if (payEndResultRunnable != null)
            new Handler().postDelayed(payEndResultRunnable, 500);
    }

    public void onPause() {
        baseView.removeMapView();
    }

    public void onStop() {
        payEndResultRunnable = null;
    }
    int paymentRef;
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PAY_RESULT_REQUEST) {
                paymentRef = data.getIntExtra("paymentRef",-1);
                    payEndResultRunnable = () -> baseView.openPaymentResult(paymentRef,data.getBooleanExtra("isMulti",false));
            } else if (requestCode == SHOP_INFO_REQUEST) {
                boolean isFavorite = data.getBooleanExtra(SHOP_FAVORITE, false);
                int oid = data.getIntExtra(SHOP_OID, NO_VALUE);
                ShopListAdapter shopListAdapter = baseView.getShopListAdapter();
                shopListAdapter.singleShopFavoriteUpdate(oid, isFavorite);
            }
        }

        /*페이스북 콜백 매니져*/
        if (baseView.getShareFacebookCallback() != null)
            baseView.getShareFacebookCallback().onActivityResult(requestCode, resultCode, data);
    }

    public boolean onFilterBackPressed() {
        boolean isTheme = baseView.activity.getSupportFragmentManager().findFragmentByTag(FILTER_THEME_TAG) == null;
        boolean isPrice = baseView.activity.getSupportFragmentManager().findFragmentByTag(FILTER_PRICE_TAG) == null;
        boolean openFilter = baseView.binding.shopListFilterLayout.isDrawerOpen(GravityCompat.END);
        if (openFilter && (isTheme && isPrice))
            baseView.binding.shopListFilterLayout.closeDrawers();

        baseModel.getListFilter().getTempPriceRange().clear();
        baseModel.getMapFilter().getTempPriceRange().clear();
        return openFilter && (isTheme && isPrice);
    }

    /* 위치 권한 */
    @Override
    protected void grantedLocation() {
        userLocation.startLocationUpdates();
    }

    @Override
    protected void deniedLocation() {
        MDEBUG.debug("DeniedLocation");
        baseView.binding.shopList.loadShopList(INITIAL, baseModel.getListFilter().getListFilter());
    }

    /* 위치 권한 요청 및 위치에 따른 리스트 업데이트 */
    @Override
    public void requestPermissions() {
        baseView.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_LOCATION);
    }

    @Override
    public void startLoadList(Location location) {
        baseView.showLoading();
        baseModel.getListFilter().setLocation(location);
        MDEBUG.debug("onStartLoadList");
        baseView.binding.shopList.loadShopList(INITIAL, baseModel.getListFilter().getListFilter());
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.map_call:
                baseView.callShopPhone();
                break;
            case R.id.map_position:
                baseView.openMapIntent();
                break;
            case R.id.map_share:
                baseView.openShopShared();
                break;
            case R.id.shop_map:
                baseView.animMap();
                break;
            case R.id.shop_filter:
                baseView.animFilter();
                break;
            case R.id.shop_search:
                baseView.animSearchBar();
                break;
            case R.id.shop_list_cart:
                baseView.openShoppingBasket();
                break;
            case R.id.close_filter:
                baseView.binding.shopListFilterLayout.closeDrawers();
                break;
            case R.id.theme_text:
            case R.id.theme_select:
                baseView.openSelectTheme();
                break;
            case R.id.price_range_text:
            case R.id.price_range_select:
                baseView.openSelectPriceRange();
                break;
                /*검색 필터*/
            case R.id.sort_price:
                StateButton price = (StateButton) v;
                baseModel.getListFilter().setPrice(price.getState() == Properties.NORMAL ? null : (price.getState() == Properties.UP ? ASC : DESC));
                baseView.sortFilter.setViewId(v.getId());
                break;
            case R.id.sort_like:
                StateButton like = (StateButton) v;
                baseModel.getListFilter().setLike(like.getState() == Properties.SELECT);
                baseView.sortFilter.setViewId(v.getId());
                break;
            case R.id.sort_favorite:
                StateButton favorite = (StateButton) v;
                baseModel.getListFilter().setFavorite(favorite.getState() == Properties.SELECT);
                baseView.sortFilter.setViewId(v.getId());
                break;
            case R.id.select:
                baseView.removePriceRangeFilter(baseModel.getPriceRange(baseView.currentShopType()));
                break;
            case R.id.wifi:
            case R.id.parking:
            case R.id.valet:
            case R.id.toilet:
            case R.id.group:
            case R.id.reservation:
            case R.id.pack:
            case R.id.disa:
            case R.id.child:
            case R.id.pet:
                StateButton stateButton = (StateButton) v;
                String tag = (String) v.getTag();
                baseModel.setConvenience(baseView.currentShopType(), tag, stateButton.getState() == Properties.SELECT);
                break;
            case R.id.filter_start:
                if (baseView.currentShopType() == MAP) {
                    baseView.startMapFilter();
                    baseModel.requestMapIntoShopList(baseCallback.searchingMapCallback(), baseView.getTopRightPoint(), baseView.getBottomLeftPoint());
                } else {
                    baseView.startListFilter(baseModel.getListFilter().getFilterSearch());
                }
                break;
            case R.id.filter_reset:
                baseModel.resetFilter(baseView.currentShopType());
                baseView.resetFilter();
                break;
            /* 결제 완료 페이지의 버튼 */
            case R.id.payment_result_history:
                baseView.openActivity(UsageActivity.class);
                break;
            case R.id.payment_result_main:
                baseView.openClearTaskActivity(MainActivity.class);
                break;
            /* 맵 리스너 */
            case R.id.user_current_location:
                baseView.enableTrackingUserLocation();
                break;
            case R.id.map_enlargement:
                baseView.animEnlargementMap();
                break;
            case R.id.map_reduction:
                baseView.animReductionMap();
                break;
            case R.id.map_select_description:
                Shop shop = baseView.binding.getSelectShop();
                if (shop != null)
                    baseView.openShopDetail(shop);
                break;
        }
    }

    /* 필터의 거리 리스너 */
    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        int[] distanceArray = baseView.activity.getResources().getIntArray(R.array.distance_value);
        baseModel.getListFilter().setDistance(distanceArray[i]);
        baseView.distanceSeekBar(seekBar, i);
    }
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {}

    /* 필터의 테마 혹은 가격대 셀렉트 리스너 */
    public void onItemSelected(SelectItem item) {
        if (item.isTheme()) {
            baseModel.setTheme(baseView.currentShopType(), item.getCode());
            baseView.selectTheme(item.getName());
        } else {
            ArrayList<String> priceRange = baseModel.getTempPriceRange(baseView.currentShopType());
            if (item.isSelected()) {
                if (!priceRange.contains(item.getCode()))
                    priceRange.add(item.getCode());
            } else {
                if (priceRange.contains(item.getCode()))
                    priceRange.remove(item.getCode());
            }
        }
    }

    /* 검색 키보드 액션 리스너 */
    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            String search = textView.getText().toString().trim().replaceAll("\\s", ",");
            baseView.searchShop(search, baseModel.getListSearchString(search));
            baseModel.setMapSearchString(search);
            baseView.getMapView().removeAllPOIItems();
            baseModel.requestMapIntoShopList(baseCallback.searchingMapCallbackinSearch(), baseView.getTopRightPoint(), baseView.getBottomLeftPoint());
            return true;
        }
        return false;
    }

    /* 스트릿 스피너 셀렉트 */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (SpinnerFlag){
            SpinnerFlag = false;
            return;

        }
        MDEBUG.debug("Spinner Flag");
        MDEBUG.debug("Spinner Position " + position);
        StreetInfo streetInfo = baseView.getStreetInfo(baseView.getStreetArray().get(position));
        baseModel.setStreetInfo(streetInfo);
        if (baseView.getMapView() != null) {
            baseView.animDescription(null, false);
            baseView.getMapView().removeAllPOIItems();
            baseView.getMapView().setMapCenterPoint(MapPoint.mapPointWithGeoCoord(streetInfo.getLatitude(), streetInfo.getLongitude()), false);
            baseModel.requestMapIntoShopList(baseCallback.searchingMapCallback(), baseView.getTopRightPoint(), baseView.getBottomLeftPoint());
        }
        baseView.selectStreet(streetInfo.getName(), baseModel.getListFilter().getListFilter());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}

    /* 점포 리스트 터치 */
    @Override
    public void onTouchItem(RecyclerView recyclerView, View view, int position) {
        ShopListAdapter adapter = baseView.getShopListAdapter();
        baseView.openShopDetail(adapter.getObservableArrayList().get(position));
    }

    /* 점포 리스트 로드 */
    @Override
    public void onLoadItem(LoadRecyclerView loadRecyclerView, int currentPage) {
        baseView.showLoading();
        MDEBUG.debug("onLoadItem Calll");
        baseView.binding.shopList.loadShopList(ADD, baseModel.getListFilter().getLoadFilter(currentPage));
    }

    @Override
    public void onInitSuccess(ShopInfo shopInfo) {
        ShopListAdapter adapter = baseView.getShopListAdapter();
        adapter.updateShopList(shopInfo.getShopList());

        if (baseView.binding.shopList.getListLoadScrollListener() != null)
            baseView.binding.shopList.getListLoadScrollListener().resetScroll();

        baseView.binding.shopList.setTotalPage(shopInfo.getTotalPage());
        baseView.binding.notSearch.setVisibility(shopInfo.getShopList().size() > 0 ? View.GONE : View.VISIBLE);
        baseView.hideLoading();
    }

    @Override
    public void onAddSuccess(final ShopInfo shopInfo) {
        final ShopListAdapter adapter = baseView.getShopListAdapter();
        baseView.activity.runOnUiThread(() -> {
            adapter.addShopList(shopInfo.getShopList());
            baseView.binding.shopList.setTotalPage(shopInfo.getTotalPage());
        });
        baseView.hideLoading();
    }

    @Override
    public void onResponseFailure(@NonNull Call<ShopInfo> call) {
        baseView.showAlert(R.string.retry);
        baseView.binding.notSearch.setVisibility(View.VISIBLE);
    }

    /* 필터 Drawer 리스너 */
    @Override
    public void onDrawerOpened(View drawerView) {}

    @Override
    public void onDrawerClosed(View drawerView) {}

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {}

    @Override
    public void onDrawerStateChanged(int newState) {
        if (DrawerLayout.STATE_SETTLING == newState)
            baseView.changeFilterView();
    }

    /* 카테고리 탭 */
    private TabLayout.OnTabSelectedListener getListTabListener() {
        return new TabLayout.OnTabSelectedListener() {
            /* 카테고리 셀렉트 리스너 */
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                baseModel.setCategoryInfo(tab.getText() != null ? tab.getText().toString() : "");
                baseView.getMapView().removeAllPOIItems();
                baseView.searchCategoryShop(baseModel.getListFilter().getListFilter());
                baseModel.requestMapIntoShopList(baseCallback.searchingMapCallback(), baseView.getTopRightPoint(), baseView.getBottomLeftPoint());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        };
    }

    /* 맵뷰 인터페이스 */
    public void onMapViewInitialized(MapView mapView) {
        MapPoint mapPoint = baseView.getSavePoint();
        int zoomLevel = baseView.getSaveZoomLevel();
        Double lat = mapPoint != null ? mapPoint.getMapPointGeoCoord().latitude : baseModel.getStreetInfo().getLatitude();
        Double lng = mapPoint != null ? mapPoint.getMapPointGeoCoord().longitude : baseModel.getStreetInfo().getLongitude();
        if (zoomLevel != 0)
            mapView.setZoomLevel(zoomLevel, false);
        mapView.setMapCenterPoint(MapPoint.mapPointWithGeoCoord(lat, lng), false);
    }

    public void onMapViewSingleTapped(MapView mapView, MapPoint mapPoint) {
        baseView.animDescription(null, false);
    }

    public void onMapViewMoveFinished(MapView mapView, MapPoint mapPoint) {
        baseModel.requestMapIntoShopList(baseCallback.searchingMapCallback(), baseView.getTopRightPoint(), baseView.getBottomLeftPoint());
        baseView.disableTrackingUserLocation();
    }

    public void onPOIItemSelected(MapView mapView, MapPOIItem mapPOIItem) {
        if (mapPOIItem instanceof ShopMapPOIItem) {
            ShopMapPOIItem shopMapPOIItem = (ShopMapPOIItem) mapPOIItem;
            Shop shop = shopMapPOIItem.getShop();
            baseView.animDescription(shop, true);
        }
    }
}
