package kr.co.mergepoint.mergeclient.application.common;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.HttpCookie;

/**
 * Created by jgson on 2017. 5. 31..
 */

class ParcelableHttpCookie implements Parcelable {

    private transient final HttpCookie cookie;
    private transient HttpCookie clientCookie;

    public ParcelableHttpCookie(HttpCookie cookie) {
        this.cookie = cookie;
    }

    public ParcelableHttpCookie(Parcel source) {
        String name = source.readString();
        String value = source.readString();
        cookie = new HttpCookie(name, value);
        cookie.setComment(source.readString());
        cookie.setCommentURL(source.readString());
        cookie.setDiscard(source.readByte() != 0);
        cookie.setDomain(source.readString());
        cookie.setMaxAge(source.readLong());
        cookie.setPath(source.readString());
        cookie.setPortlist(source.readString());
        cookie.setSecure(source.readByte() != 0);
        cookie.setVersion(source.readInt());
    }

    HttpCookie getCookie() {
        HttpCookie bestCookie = cookie;
        if (clientCookie != null) {
            bestCookie = clientCookie;
        }

        return bestCookie;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cookie.getName());
        dest.writeString(cookie.getValue());
        dest.writeString(cookie.getComment());
        dest.writeString(cookie.getCommentURL());
        dest.writeByte((byte) (cookie.getDiscard() ? 1 : 0));
        dest.writeString(cookie.getDomain());
        dest.writeLong(cookie.getMaxAge());
        dest.writeString(cookie.getPath());
        dest.writeString(cookie.getPortlist());
        dest.writeByte((byte) (cookie.getSecure() ? 1 : 0));
        dest.writeInt(cookie.getVersion());
    }

    public static final Parcelable.Creator<ParcelableHttpCookie> CREATOR = new Creator<ParcelableHttpCookie>() {
        @Override
        public ParcelableHttpCookie[] newArray(int size) {
            return new ParcelableHttpCookie[size];
        }

        @Override
        public ParcelableHttpCookie createFromParcel(Parcel source) {
            return new ParcelableHttpCookie(source);
        }
    };
}
