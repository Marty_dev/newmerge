package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.MyTicketBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.Ticket;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.TicketAdapter;

/**
 * Created by 1017sjg on 2017. 8. 7..
 */

public class MyTicketFragment extends BaseFragment {

    private ArrayList<Ticket> tickets;

    public static MyTicketFragment newInstance(ArrayList<Ticket> tickets) {
        Bundle args = new Bundle();
        MyTicketFragment fragment = new MyTicketFragment();
        fragment.tickets = tickets;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MyTicketBinding myTicketBinding = DataBindingUtil.inflate(inflater, R.layout.my_ticket, container, false);
        myTicketBinding.setTitle("나의 식권");
        myTicketBinding.setOnClick(this);
        myTicketBinding.setIsTicket(tickets.size() > 0);
        myTicketBinding.setTicketAdapter(new TicketAdapter(tickets));
        return myTicketBinding.getRoot();
    }
}
