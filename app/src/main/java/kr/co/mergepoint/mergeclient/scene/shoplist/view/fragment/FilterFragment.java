package kr.co.mergepoint.mergeclient.scene.shoplist.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.FilterSelectLayoutBinding;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.SelectItem;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.FilterAdapter;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.FilterHolder;

/**
 * Created by 1017sjg on 2017. 6. 27..
 */

public class FilterFragment extends BaseFragment  {

    private String title;
    private boolean isTheme;
    private ArrayList<String> itemStrings;
    private ArrayList<SelectItem> selectItems;
    private FilterHolder.OnItemSelectedListener onItemSelectedListener;

    public static FilterFragment newInstance(ArrayList<String> arrays, String title, FilterHolder.OnItemSelectedListener onItemSelectedListener) {
        Bundle args = new Bundle();
        FilterFragment fragment = new FilterFragment();
        fragment.title = title;
        fragment.itemStrings = arrays;
        fragment.selectItems = new ArrayList<>();
        fragment.onItemSelectedListener = onItemSelectedListener;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isTheme = Objects.equals(title, getString(R.string.theme));
        for (int i = 0; i < itemStrings.size(); i++)
            selectItems.add(new SelectItem(isTheme, itemStrings.get(i), String.format(Locale.getDefault(), isTheme ? "00%d" : "%d", i+1)));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FilterSelectLayoutBinding selectLayoutBinding = DataBindingUtil.inflate(inflater, R.layout.filter_select_layout, container, false);
        selectLayoutBinding.setTitle(title);
        selectLayoutBinding.setOnClick(this);
        selectLayoutBinding.setIsTheme(isTheme);
        selectLayoutBinding.setPriceAdapter(new FilterAdapter(selectItems, onItemSelectedListener, isTheme));
        return selectLayoutBinding.getRoot();
    }
}
