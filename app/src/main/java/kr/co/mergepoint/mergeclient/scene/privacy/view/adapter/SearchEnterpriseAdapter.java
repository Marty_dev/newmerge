package kr.co.mergepoint.mergeclient.scene.privacy.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.SearchEnterpriseItemsBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.Company;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class SearchEnterpriseAdapter extends BasicListAdapter<BasicListHolder<SearchEnterpriseItemsBinding, Company>, Company> {

    public SearchEnterpriseAdapter(ArrayList<Company> arrayList) {
        super(arrayList);
    }

    @Override
    public BasicListHolder<SearchEnterpriseItemsBinding, Company> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_enterprise_items, parent, false);
        SearchEnterpriseItemsBinding searchEnterpriseItemsBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<SearchEnterpriseItemsBinding, Company>(searchEnterpriseItemsBinding) {
            @Override
            public void setDataBindingWithData(Company data) {
                getDataBinding().setCompany(data);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<SearchEnterpriseItemsBinding, Company> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
