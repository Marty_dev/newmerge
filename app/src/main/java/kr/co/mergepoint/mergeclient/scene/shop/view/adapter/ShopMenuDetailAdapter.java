package kr.co.mergepoint.mergeclient.scene.shop.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.application.common.CustomCountView;
import kr.co.mergepoint.mergeclient.databinding.ShopMenuDetailListFooterBinding;
import kr.co.mergepoint.mergeclient.databinding.ShopMenuDetailListHeaderBinding;
import kr.co.mergepoint.mergeclient.databinding.ShopMenuDetailListHeaderPriceBinding;
import kr.co.mergepoint.mergeclient.databinding.ShopMenuDetailListItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionMenu;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionSelectMenu;
import kr.co.mergepoint.mergeclient.scene.shop.view.listener.OnOptionCheckListener;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_FOOTER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_HEADER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_HEADER_PRICE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST_TYPE_ITEM;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MAIN_SEARCH;
import static kr.co.mergepoint.mergeclient.application.common.Properties.OPTION_TYPE_MAIN;

/**
 * Created by 1017sjg on 2017. 6. 24..
 */

public class ShopMenuDetailAdapter extends BasicListAdapter<BasicListHolder, ShopOptionMenu> implements CustomCountView.OnChangeCountListener, OnOptionCheckListener  {

    private ShopMenuDetail menuDetail;
    private Context context;

    private int menuPrice;
    public Context getContext() {
        return context;
    }

    public ShopMenuDetailAdapter(ShopMenuDetail menuDetail, Context context) {

        super(menuDetail.optionMenu);
        for (ShopOptionMenu option : getObservableArrayList()) {
            if (option.type == OPTION_TYPE_MAIN)
                Collections.swap(getObservableArrayList(), 0, getObservableArrayList().indexOf(option));
        }
        this.menuDetail = menuDetail;
        this.context = context;
    }

    @Override
    public BasicListHolder setCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == LIST_TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_menu_detail_list_header, parent, false);
            ShopMenuDetailListHeaderBinding headerBinding = DataBindingUtil.bind(view);

            return new BasicListHolder<ShopMenuDetailListHeaderBinding, ShopMenuDetail>(headerBinding) {
                @Override
                public void setDataBindingWithData(ShopMenuDetail data) {
                    getDataBinding().setMenuDetail(data);
                }
            };
        } else if (viewType == LIST_TYPE_HEADER_PRICE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_menu_detail_list_header_price, parent, false);
            ShopMenuDetailListHeaderPriceBinding headerBinding = DataBindingUtil.bind(view);

            return new BasicListHolder<ShopMenuDetailListHeaderPriceBinding, ShopMenuDetail>(headerBinding) {
                @Override
                public void setDataBindingWithData(ShopMenuDetail data) {
                    getDataBinding().setMenuDetail(data);
                }
            };
        } else if (viewType == LIST_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_menu_detail_list_item, parent, false);
            ShopMenuDetailListItemBinding itemBinding = DataBindingUtil.bind(view);

            return new BasicListHolder<ShopMenuDetailListItemBinding, ShopOptionMenu>(itemBinding) {
                @Override
                public void setDataBindingWithData(ShopOptionMenu data) {
                    getDataBinding().setShopOption(data);
                    getDataBinding().setOptionAdapter(new ShopMenuOptionAdapter(data, menuDetail.price, ShopMenuDetailAdapter.this));
                }
            };
        } else if (viewType == LIST_TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_menu_detail_list_footer, parent, false);
            ShopMenuDetailListFooterBinding footerBinding = DataBindingUtil.bind(view);


            return new BasicListHolder<ShopMenuDetailListFooterBinding, ShopMenuDetail>(footerBinding,true) {
                @Override
                public void setDataBindingWithData(ShopMenuDetail data) {
                    getDataBinding().setMenuDetail(data);
                    getDataBinding().setCountListener(getDataBinding().countBind.countView);
                    getDataBinding().setCountChangeListener(ShopMenuDetailAdapter.this);
                    getDataBinding().countBind.setCount(data.count);
                    menuPrice = menuDetail.payPrice;
                }
            };
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setBindViewHolder(BasicListHolder holder, int position) {
        if (holder.getDataBinding() instanceof ShopMenuDetailListItemBinding) {
            holder.bind(getObservableArrayList().get(position - 1));
        } else {
            if (holder.flag){
            }
            holder.bind(menuDetail);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0) ? (menuDetail.serveType > 1 ? LIST_TYPE_HEADER_PRICE : LIST_TYPE_HEADER) : ((position == getItemCount() - 1) ? LIST_TYPE_FOOTER : LIST_TYPE_ITEM);
    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + 2;
    }

    /* 메뉴 수량 리스너 */
    @Override
    public void changeCount(CustomCountView view, boolean isPlus) {
        menuDetail.setCount(isPlus ? menuDetail.count + 1 : menuDetail.count - 1);
    }

    /* 옵션 어댑터
    * 옵션 카운터 변경 */
    @Override
    public void onOptionCount(boolean isPlus, ShopOptionSelectMenu selectMenu, int optionKind) {
        menuDetail.setPayPrice(isPlus ? menuDetail.payPrice + selectMenu.price : menuDetail.payPrice - selectMenu.price);
    }

    /* 싱글 체크 및 멀티 체크 리스너 */
    @Override
    public void onMultiCheck(boolean checked, ShopOptionSelectMenu selectMenu, int dispayCount, int optionKind) {
        int optionPrice = selectMenu.price * dispayCount;
        MDEBUG.debug("pro count  " + dispayCount);
        menuDetail.setPayPrice(checked ? menuDetail.payPrice + optionPrice : menuDetail.payPrice - optionPrice);
        selectMenu.setCount(checked ? dispayCount : 0);
    }

    @Override
    public void onSingleRadio(ShopOptionMenu optionMenu, boolean checked, ShopOptionSelectMenu preSelectMenu, ShopOptionSelectMenu touchSelectMenu, int optionKind,int count) {
        if (preSelectMenu != null) {
            if (optionMenu.type == OPTION_TYPE_MAIN) {
                /* 제일 처음 체크한 이후
                *  메인 택일 옵션 메뉴일 경우 */
                if (checked && preSelectMenu.oid != touchSelectMenu.oid) {
                    /* 싱글 체크 */
                    preSelectMenu.setCount(0);
                    touchSelectMenu.setCount(1);
                    menuDetail.setPayPrice(menuDetail.payPrice - preSelectMenu.price + touchSelectMenu.price);
                    MDEBUG.debug("this!");
                }
            } else {
                MDEBUG.debug("thius???");
                MDEBUG.debug("menuPrice" + menuDetail.payPrice);
                /* 제일 처음 체크한 이후
                *  일반 선택 옵션 일 경우 */
                if (checked) {
                /* 싱글 체크 */
                    if (preSelectMenu.oid == touchSelectMenu.oid) {
                        menuDetail.setPayPrice(menuDetail.payPrice + touchSelectMenu.price);
                    } else {
                        menuDetail.setPayPrice(menuDetail.payPrice - (preSelectMenu.price * preSelectMenu.getCount()) + touchSelectMenu.price);
                        preSelectMenu.setCount(0);

                    }
                    touchSelectMenu.setCount(1);
                } else {
                /* 싱글 체크 해제 */
                    menuDetail.setPayPrice(menuDetail.payPrice - (touchSelectMenu.getCount() == 0 ? touchSelectMenu.price : touchSelectMenu.price *  touchSelectMenu.getCount()));
                    touchSelectMenu.setCount(0);
                }
            }
        } else {
            /* 싱글 체크가 안되있는 제일 처음 체크했을 때 */
            MDEBUG.debug("this!!2");
            touchSelectMenu.setCount(count);
            MDEBUG.debug("TouchMenu Price _ " + touchSelectMenu.price);
            MDEBUG.debug("TouchMenu Count _ " + touchSelectMenu.getCount());
            MDEBUG.debug("Sale Price" + menuDetail.salePrice);
            int MainPrice = 0;
            if (touchSelectMenu.getCount() == 0 ){
                touchSelectMenu.setCount(1);
               menuDetail.setPayPrice(menuDetail.payPrice - menuPrice);
            }
            //menuDetail.setPayPrice((menuDetail.payPrice - MainPrice)+ (touchSelectMenu.price * (touchSelectMenu.getCount() == 0 ? 1 : touchSelectMenu.getCount())));
            menuDetail.setPayPrice(menuDetail.payPrice + touchSelectMenu.price * touchSelectMenu.getCount());
        }
    }
}
