package kr.co.mergepoint.mergeclient.scene.login.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.InputEmailInstaBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.SocialSignup;

/**
 * Created by jgson on 2017. 7. 19..
 */

public class SocialSignupEmailFragment extends BaseFragment {

    private InputEmailInstaBinding inputEmailInstaBinding;
    private SocialSignup socialSignup;

    public InputEmailInstaBinding getInputEmailInstaBinding() {
        return inputEmailInstaBinding;
    }

    public SocialSignup getSocialSignup() {
        return socialSignup;
    }

    public static SocialSignupEmailFragment newInstance(SocialSignup socialSignup) {
        Bundle args = new Bundle();
        SocialSignupEmailFragment fragment = new SocialSignupEmailFragment();
        fragment.socialSignup = socialSignup;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        inputEmailInstaBinding = DataBindingUtil.inflate(inflater, R.layout.input_email_insta, container, false);
        if (socialSignup != null) {
            inputEmailInstaBinding.setTitle(getString(R.string.social_title, socialSignup.social));
            inputEmailInstaBinding.setSocial(socialSignup.social);
            inputEmailInstaBinding.setOnClick(this);
        }
        return inputEmailInstaBinding.getRoot();
    }
}
