package kr.co.mergepoint.mergeclient.scene.search.dagger;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocation;
import kr.co.mergepoint.mergeclient.scene.search.SearchActivity;
import kr.co.mergepoint.mergeclient.scene.search.model.SearchModel;
import kr.co.mergepoint.mergeclient.scene.search.presenter.SearchPresenter;
import kr.co.mergepoint.mergeclient.scene.search.presenter.callback.SearchCallback;
import kr.co.mergepoint.mergeclient.scene.search.view.SearchView;

import static kr.co.mergepoint.mergeclient.application.common.Properties.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS;
import static kr.co.mergepoint.mergeclient.application.common.Properties.UPDATE_INTERVAL_IN_MILLISECONDS;

/**
 * Created by jgson on 2017. 6. 5..
 */

@Module
public class SearchModule {

    private SearchActivity activity;

    public SearchModule(SearchActivity activity) {
        this.activity = activity;
    }

    @Provides
    @SearchScope
    FusedLocationProviderClient provideGoogleApiClientBuilder() {
        return LocationServices.getFusedLocationProviderClient(activity);
    }

    @Provides
    @SearchScope
    LocationRequest provideLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        return locationRequest;
    }

    @Provides
    @SearchScope
    UserLocation provideUserLocation(FusedLocationProviderClient providerClient, LocationRequest locationRequest) {
        return new UserLocation(providerClient, activity, locationRequest, activity);
    }

    @Provides
    @SearchScope
    SearchModel provideModel() { return new SearchModel(); }

    @Provides
    @SearchScope
    SearchView provideView() { return new SearchView(activity, R.layout.search_shop); }

    @Provides
    @SearchScope
    SearchPresenter providePresenter(SearchView view, SearchModel model, UserLocation userLocation) {
        return new SearchPresenter(view, model, new SearchCallback(view, model), userLocation);
    }
}
