package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.ContactusBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.Contactus;

/**
 * Created by 1017sjg on 2017. 8. 10..
 */

public class ContactusFragment extends BaseFragment {

    private ContactusBinding contactusBinding;

    public static ContactusFragment newInstance() {
        Bundle args = new Bundle();
        ContactusFragment fragment = new ContactusFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contactusBinding = DataBindingUtil.inflate(inflater, R.layout.contactus, container, false);
        contactusBinding.setTitle(getString(R.string.alliance_title));
        contactusBinding.setOnClick(this);

        return contactusBinding.getRoot();
    }

    public Contactus getAllianceData() {
        String email = contactusBinding.email.getText().toString();
        String phone = contactusBinding.phone.getText().toString();
        String name = contactusBinding.name.getText().toString();
        String title = contactusBinding.titleText.getText().toString();
        String content = contactusBinding.content.getText().toString();

        if (!contactusBinding.agreeCheck.isChecked()) {
            showAlert(R.string.check_personal_info);
            return null;
        } else if (email.trim().isEmpty()) {
           showAlert(R.string.enter_email);
           return null;
        } else if (phone.trim().isEmpty()) {
            showAlert(R.string.enter_phone);
            return null;
        } else if (name.trim().isEmpty()) {
            showAlert(R.string.enter_name);
            return null;
        } else if (title.trim().isEmpty()) {
            showAlert(R.string.enter_title);
            return null;
        } else if (content.trim().isEmpty()) {
            showAlert(R.string.enter_text);
            return null;
        }

        return new Contactus(email, phone, name, title, content);
    }
}
