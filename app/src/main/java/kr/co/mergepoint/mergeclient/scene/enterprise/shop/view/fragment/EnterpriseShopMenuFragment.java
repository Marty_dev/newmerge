package kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.ExpandableItemListener;
import kr.co.mergepoint.mergeclient.databinding.EnterpriseShopDetailMenuBinding;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.EnterpriseShopActivity;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.model.EnterpriseShopModel;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.ShopExpandMenuAdapter;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder.Menu;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.view.PricePaymentActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * Created by 1017sjg on 2017. 6. 23..
 */

public class EnterpriseShopMenuFragment extends BaseFragment {

    EnterpriseShopActivity activity;
    private EnterpriseShopDetailMenuBinding shopDetailMenuBinding;
    private ExpandableItemListener.ExpandableTouchListener expandableTouchListener;
    private EnterpriseShopModel Basemodel;


    public static EnterpriseShopMenuFragment newInstance(EnterpriseShopActivity activity, EnterpriseShopModel basemodel) {
        Bundle args = new Bundle();
        EnterpriseShopMenuFragment fragment = new EnterpriseShopMenuFragment();
        fragment.expandableTouchListener = activity;
        fragment.activity = activity;
        fragment.Basemodel = basemodel;

        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        shopDetailMenuBinding = DataBindingUtil.inflate(inflater, R.layout.enterprise_shop_detail_menu, container, false);
        MDEBUG.debug("setVisibe " + Basemodel.isDailymenu);
        Basemodel.Dailybtn = shopDetailMenuBinding.enterpriseMenu;
        shopDetailMenuBinding.enterpriseMenu.setVisibility(Basemodel.isDailymenu? View.VISIBLE :  View.GONE);
        shopDetailMenuBinding.enterprisePricecustom.setOnClickListener(activity);
        return shopDetailMenuBinding.getRoot();
    }

    public void setMenuGroup(ArrayList<Menu> menuGroup) {
        shopDetailMenuBinding.setOnClick(getRootActivity());

        shopDetailMenuBinding.setExpandableAdapter(new ShopExpandMenuAdapter(menuGroup, getContext()));
        shopDetailMenuBinding.setChildTouch(expandableTouchListener);
    }
}
