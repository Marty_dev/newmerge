package kr.co.mergepoint.mergeclient.application.common;

import android.util.Log;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Created by 1017sjg on 2017. 6. 12..
 */

public class CustomCookieManager extends CookieManager {

    public CustomCookieManager(CookieStore store, CookiePolicy cookiePolicy) {
        super(store, cookiePolicy);
    }

    @Override
    public void put(URI uri, Map<String, List<String>> responseHeaders) throws IOException {
        super.put(uri, responseHeaders);
        if (responseHeaders != null && responseHeaders.get("Set-Cookie") != null)
            for (String cookieValue: responseHeaders.get("Set-Cookie")) {
                Log.d("JSESSIONID_RETROFIT", cookieValue);
            }
    }

    @Override
    public Map<String, List<String>> get(URI uri, Map<String, List<String>> requestHeaders) throws IOException {
        return super.get(uri, requestHeaders);
    }
}
