package kr.co.mergepoint.mergeclient.scene.register.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.scene.register.RegisterActivity;

/**
 * Created by jgson on 2017. 6. 5..
 */

@RegisterScope
@Component(modules = RegisterModule.class)
public interface RegisterComponent {
    void inject(RegisterActivity activity);
}
