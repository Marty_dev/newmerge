package kr.co.mergepoint.mergeclient.scene.login.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.scene.login.LoginActivity;

/**
 * Created by jgson on 2017. 5. 31..
 */

@LoginScope
@Component(modules = LoginModule.class)
public interface LoginComponent {
    void inject(LoginActivity context);
}
