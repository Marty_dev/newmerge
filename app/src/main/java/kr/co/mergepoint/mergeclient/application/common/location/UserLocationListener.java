package kr.co.mergepoint.mergeclient.application.common.location;


import android.location.Location;

/**
 * Created by jgson on 2018. 1. 2..
 */

public interface UserLocationListener {
    void requestPermissions();
    void startLoadList(Location location);
}
