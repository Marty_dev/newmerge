package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.CrowdApprovalRequestResultBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.CrowdApprovalRequestResultAdapter;

/**
 * Created by 1017sjg on 2017. 8. 7..
 */

public class CrowdApprovalRequestResultFragment extends BaseFragment {

    private CrowdApprovalRequestResultBinding resultBinding;
    private ArrayList<CrowdPay> crowdPays;
    private ArrayList<CrowdPay> approvalCrowdList;
    private ArrayList<CrowdPay> rejectCrowdList;
    private String message;

    public static CrowdApprovalRequestResultFragment newInstance(ArrayList<CrowdPay> crowdPays, String message) {
        Bundle args = new Bundle();
        CrowdApprovalRequestResultFragment fragment = new CrowdApprovalRequestResultFragment();
        fragment.crowdPays = crowdPays;
        fragment.message = message;
        fragment.approvalCrowdList = new ArrayList<>();
        fragment.rejectCrowdList = new ArrayList<>();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        for (CrowdPay crowdPay :crowdPays) {
            if (crowdPay.approveState == 2) {
                approvalCrowdList.add(crowdPay);
            } else if (crowdPay.approveState >= 3) {
                rejectCrowdList.add(crowdPay);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        resultBinding = DataBindingUtil.inflate(inflater, R.layout.crowd_approval_request_result, container, false);
        resultBinding.setTitle("함께결제 승인하기");
        resultBinding.setOnClick(this);
        String message;
        if (approvalCrowdList.size() == crowdPays.size()) {
            message = "함께 결제하기를\n승인 완료하였습니다.";
        } else if (rejectCrowdList.size() == crowdPays.size()){
            message = rejectCrowdList.get(0).approveState == 4 ? "시간이 초과되어\n함께결제하기를\n실패하였습니다." : "함께결제하기를\n거절하였습니다.";
        } else {
            message = "함께 결제하기를\n거절한 사람을 제외하고\n승인 완료하였습니다.";
        }
        resultBinding.setMessage(message);
        resultBinding.resultImg.setImageDrawable(ContextCompat.getDrawable(getContext(), approvalCrowdList.size() == 0 ? R.drawable.obj_failed :  R.drawable.obj_cancel_ok));
        resultBinding.setCrowedApprovalAdapter(new CrowdApprovalRequestResultAdapter(approvalCrowdList, getContext()));
        resultBinding.setCrowedRejectAdapter(new CrowdApprovalRequestResultAdapter(rejectCrowdList, getContext()));

        resultBinding.approvalListTitle.setVisibility(approvalCrowdList.size() > 0 ? View.VISIBLE : View.GONE);
        resultBinding.approvalList.setVisibility(approvalCrowdList.size() > 0 ? View.VISIBLE : View.GONE);

        resultBinding.rejectListTitle.setVisibility(rejectCrowdList.size() > 0 ? View.VISIBLE : View.GONE);
        resultBinding.rejectList.setVisibility(rejectCrowdList.size() > 0 ? View.VISIBLE : View.GONE);

        return resultBinding.getRoot();
    }
}
