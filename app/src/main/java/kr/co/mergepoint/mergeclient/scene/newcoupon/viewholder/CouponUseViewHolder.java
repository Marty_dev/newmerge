package kr.co.mergepoint.mergeclient.scene.newcoupon.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import kr.co.mergepoint.mergeclient.R;

/**
 * User: Marty
 * Date: 2018-09-27
 * Time: 오전 10:41
 * Description:
 */
public class CouponUseViewHolder extends RecyclerView.ViewHolder {
    public TextView useShopname;
    public TextView useDate;
    public TextView newCouponprice;

    public CouponUseViewHolder(View itemView) {
        super(itemView);
        useShopname = (TextView)itemView.findViewById(R.id.use_shopname);
        useDate = (TextView)itemView.findViewById(R.id.use_date);
        newCouponprice = (TextView)itemView.findViewById(R.id.new_couponprice);

    }
}
