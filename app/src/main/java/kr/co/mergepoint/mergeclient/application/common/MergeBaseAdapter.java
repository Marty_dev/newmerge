package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import java.util.ArrayList;


/**
 * User: Marty
 * Date: 2018-07-18
 * Time: 오전 11:32
 * Description:
 */
public abstract class MergeBaseAdapter<T,S extends RecyclerView.ViewHolder>extends RecyclerView.Adapter{
    public ArrayList<T> arrayList;
    public LayoutInflater inflater;
    public BaseActivity mCon;
    public int mLayout;


    public MergeBaseAdapter() {
        this.arrayList = new ArrayList<>();
    }

    public MergeBaseAdapter(int layout,BaseActivity mCon){
        this.arrayList = new ArrayList<>();
        this.mCon = mCon;
        mLayout = layout;
        this.inflater = ((LayoutInflater)mCon.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

}
