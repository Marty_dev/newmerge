package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.QuestionsBinding;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.QuestionsPageAdapter;

/**
 * Created by 1017sjg on 2017. 8. 14..
 */

public class QuestionsFragment extends BaseFragment implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener {

    private QuestionsBinding questionsBinding;

    public static QuestionsFragment newInstance() {
        Bundle args = new Bundle();
        QuestionsFragment fragment = new QuestionsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        questionsBinding = DataBindingUtil.inflate(inflater, R.layout.questions, container, false);
        questionsBinding.setTitle(getString(R.string.questions_title));
        questionsBinding.setOnClick(this);
        questionsBinding.setTabSelectListener(this);
        questionsBinding.setPageChangeListener(this);
        questionsBinding.setPagerAdapter(new QuestionsPageAdapter(getContext()));

        return questionsBinding.getRoot();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        questionsBinding.questionPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}

    @Override
    public void onTabReselected(TabLayout.Tab tab) {}

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {
        TabLayout.Tab tab = questionsBinding.questionTab.getTabAt(position);
        if (tab != null)
            tab.select();
    }

    @Override
    public void onPageScrollStateChanged(int state) {}
}
