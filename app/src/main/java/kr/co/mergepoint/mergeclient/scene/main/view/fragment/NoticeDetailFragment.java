package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.DateModel;
import kr.co.mergepoint.mergeclient.databinding.NoticeDetailBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.Notice;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NOTICE_DETAIL_TAG;

/**
 * Created by 1017sjg on 2017. 8. 7..
 */

public class NoticeDetailFragment extends BaseFragment {

    private Notice notice;
    private DateModel dateModel;

    public static NoticeDetailFragment newInstance(Bundle bundle) {
        NoticeDetailFragment fragment = new NoticeDetailFragment();
        fragment.dateModel = new DateModel();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notice = getArguments().getParcelable(NOTICE_DETAIL_TAG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        NoticeDetailBinding noticeDetailBinding = DataBindingUtil.inflate(inflater, R.layout.notice_detail, container, false);
        noticeDetailBinding.setTitle(getString(R.string.notice_title));
        noticeDetailBinding.setOnClick(this);
        noticeDetailBinding.setNotice(notice);
        noticeDetailBinding.setIsNew(notice.creDateTime != null && dateModel.getCompareDateOfDays(3, dateModel.getDate(notice.creDateTime)));
        return noticeDetailBinding.getRoot();
    }
}
