package kr.co.mergepoint.mergeclient.scene.shoplist.view;

import net.daum.mf.map.api.MapPOIItem;

import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;

/**
 * Created by jgson on 2018. 1. 10..
 */

public class ShopMapPOIItem extends MapPOIItem {
    private Shop shop;

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }
}
