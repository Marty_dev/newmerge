package kr.co.mergepoint.mergeclient.application.common;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.view.View;

import kr.co.mergepoint.mergeclient.R;

import static kr.co.mergepoint.mergeclient.application.common.Properties.PERMISSIONS_REQUEST_LOCATION;

/**
 * Created by 1017sjg on 2017. 6. 20..
 */

public abstract class BasePresenter<T extends BaseView, S extends BaseModel, C extends BaseCallback> implements View.OnClickListener{
    public T baseView;
    public S baseModel;
    public C baseCallback;

    public BasePresenter(T baseView, S baseModel, C baseCallback) {
        this.baseView = baseView;
        this.baseModel = baseModel;
        this.baseCallback = baseCallback;

        setBindingProperty();
    }

    protected abstract void setBindingProperty();

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                baseView.activity.onBackPressed();
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @SuppressLint("MissingPermission")
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                /*권한 허락*/
                grantedLocation();
            } else {
                /*권한 거절*/
                if (baseView.activity.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) ||
                        baseView.activity.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    baseView.showAlert(R.string.request_permission);
                }
                deniedLocation();
            }
        }
    }

    protected void grantedLocation() {}
    protected void deniedLocation() {}
}
