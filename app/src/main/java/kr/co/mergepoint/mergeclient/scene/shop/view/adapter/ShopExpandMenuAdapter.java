package kr.co.mergepoint.mergeclient.scene.shop.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.databinding.ShopMenuChildBinding;
import kr.co.mergepoint.mergeclient.databinding.ShopMenuGroupBinding;
import kr.co.mergepoint.mergeclient.scene.data.menu.ShopChildInfo;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder.ChildMenuHolder;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder.GroupMenuHolder;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.holder.Menu;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class ShopExpandMenuAdapter extends ExpandableRecyclerViewAdapter<GroupMenuHolder, ChildMenuHolder> {

    private Context context;

    public GroupMenuHolder signatureHolder;
    public ExpandableGroup signatureGroup;

    public signatureListener listener;

    interface signatureListener{
        void onSignatureComplete();
    }

    public ShopExpandMenuAdapter(List<Menu> groups, Context context) {
        super(groups);
        this.context = context;
    }

    @Override
    public boolean toggleGroup(ExpandableGroup group) {
        return super.toggleGroup(group);
    }

    @Override
    public GroupMenuHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_menu_group, parent, false);
        ShopMenuGroupBinding groupBinding = DataBindingUtil.bind(view);

        GroupMenuHolder holder = new GroupMenuHolder(groupBinding, context);

        return holder;
    }

    @Override
    public ChildMenuHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_menu_child, parent, false);
        ShopMenuChildBinding childBinding = DataBindingUtil.bind(view);
        return new ChildMenuHolder(childBinding);
    }



    @Override
    public void onBindChildViewHolder(ChildMenuHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        ShopChildInfo childInfo = ((Menu) group).getItems().get(childIndex);
        holder.onBind(childInfo);
    }

    @Override
    public void onBindGroupViewHolder(GroupMenuHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setGroupMenu((Menu) group);
        holder.shopMenuGroupBinding.groupState.setVisibility(((Menu)group).getOid() == -1 ? View.GONE : View.VISIBLE);
        if (((Menu) group).getTitle().contains("(시그니쳐)")) {
            signatureHolder = holder;
            signatureGroup = group;
            MDEBUG.debug("시그니처 init");

        }
    }

}
