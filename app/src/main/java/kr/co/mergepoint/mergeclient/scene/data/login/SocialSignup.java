package kr.co.mergepoint.mergeclient.scene.data.login;

/**
 * Created by 1017sjg on 2017. 10. 2..
 */

public class SocialSignup {
    public String instaId;
    public String facebookId;
    public String social;
    public String email;

    public SocialSignup(String instaId, String facebookId, String social) {
        this.instaId = instaId;
        this.facebookId = facebookId;
        this.social = social;
    }
}
