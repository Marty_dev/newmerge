package kr.co.mergepoint.mergeclient.scene.payment.view.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Map;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.PaymentResultBinding;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.PreUseList;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.PreUseRequest;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.http.Api;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.OrderHistoryFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.UsingFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.UsingFragmentv2;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.USAGE_ORDER_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.USING_MENU_TAG;

/**
 * Created by 1017sjg on 2017. 8. 8..
 */

public class PaymentResultFragment extends BaseFragment {


    Api apis ;
    int paymentRef = -1;
    BaseActivity activity;
    int mLayout;
    boolean ismulti;
    Button btn;
    boolean isExcute;

    public static PaymentResultFragment newInstance(BaseActivity activity,int paymentRef,int mLayout,boolean ismulti) {
        Bundle args = new Bundle();
        PaymentResultFragment fragment = new PaymentResultFragment();
        fragment.setArguments(args);
        fragment.activity = activity;
        fragment.mLayout = mLayout;
        fragment.ismulti = ismulti;
        fragment.isExcute = true;
        fragment.paymentRef = paymentRef;
        return fragment;
    }
    public static PaymentResultFragment newInstance() {
        Bundle args = new Bundle();
        PaymentResultFragment fragment = new PaymentResultFragment();
        fragment.setArguments(args);
        fragment.isExcute = true;
        return fragment;
    }
    TimeThread timer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        PaymentResultBinding resultBinding = DataBindingUtil.inflate(inflater, R.layout.payment_result, container, false);
        resultBinding.setTitle(getString(R.string.finish_payment_title));
        resultBinding.setOnClick(this);
        apis = MergeApplication.getMergeAppComponent().getRetrofit().create(Api.class);
        btn = resultBinding.paymentResultHistory;
        timer = new TimeThread(btn);


        return resultBinding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Handler().postDelayed(()->{
            if (isExcute){
                btn.callOnClick();
            }
        },3000);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.payment_result_history){
            MergeApplication.getMergeApplication().showLoading(activity);
            timer.isRun = false;
            isExcute = false;
            if (!ismulti){
                apis.requestpaymentRefv2(paymentRef).enqueue(new Callback<ResponseObject<ArrayList<PreUseRequest>>>() {
                    @Override
                    public void onResponse(Call<ResponseObject<ArrayList<PreUseRequest>>> call, Response<ResponseObject<ArrayList<PreUseRequest>>> response) {

                        if (response.isSuccessful()){
                            if (!response.body().isFailed()){
                                PreUseList list = new PreUseList();
                                list.requestList = response.body().getObject();
                                apis.requestUseMenus(2,list).enqueue(new Callback<ResponseObject<UsagePayment>>() {
                                    @Override
                                    public void onResponse(Call<ResponseObject<UsagePayment>> call, Response<ResponseObject<UsagePayment>> response) {
                                        if (!response.body().isFailed()) {
                                            if (activity == null){
                                            }
                                            activity.getSupportFragmentManager().beginTransaction()
                                                    .replace(mLayout, UsingFragmentv2.newInstance(response.body().getObject(),activity,mLayout), USING_MENU_TAG)
                                                    .addToBackStack(USING_MENU_TAG).commit();
                                        }else{
                                            new MergeDialog.Builder(activity).setContent(response.body().getMessage()).setCancelBtn(false).build().show();

                                        }
                                        MergeApplication.getMergeApplication().hideLoading(activity);
                                    }
                                    @Override
                                    public void onFailure(Call<ResponseObject<UsagePayment>> call, Throwable t) {
                                        MergeApplication.getMergeApplication().hideLoading(activity);
                                        new MergeDialog.Builder(activity).setContent(R.string.retry).setCancelBtn(false).build().show();
                                    }
                                });
                            }else{
                                new MergeDialog.Builder(activity).setContent(response.body().getMessage()).setCancelBtn(false).build().show();
                                MergeApplication.getMergeApplication().hideLoading(activity);
                            }
                        }else{
                            MergeApplication.getMergeApplication().hideLoading(activity);
                            new MergeDialog.Builder(activity).setContent(R.string.retry).setCancelBtn(false).build().show();
                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseObject<ArrayList<PreUseRequest>>> call, Throwable t) {
                        MergeApplication.getMergeApplication().hideLoading(activity);
                        new MergeDialog.Builder(activity).setContent(R.string.retry).setCancelBtn(false).build().show();
                    }
                });
            }else{
                Intent inte = new Intent(activity, UsageActivity.class);
                inte.putExtra("Payref",paymentRef);
                activity.startActivity(inte);

            }
        }
        else {
            timer.isRun = false;
            isExcute = false;
            super.onClick(v);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        timer.isRun = false;
        isExcute = false;
    }
}

class TimeThread extends Thread{
    public TimeThread(Button btn) {
        this.btn = btn;
    }

    Button btn;
    public boolean isRun = true;
    @Override
    public void run() {
        super.run();
        try{
            Thread.sleep(3000);
        }catch (Exception e) {
            MDEBUG.debug("error" + e.toString());
        }

        if (isRun){
            btn.callOnClick();
        }
    }
}