package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.RecyclerItemListener;
import kr.co.mergepoint.mergeclient.application.common.SpinnerAdapter;
import kr.co.mergepoint.mergeclient.databinding.CrowdPayHistoryBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPayItem;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.CrowdHistoryAdapter;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.CrowdPayHistoryListener;

import static kr.co.mergepoint.mergeclient.application.common.Properties.CENTER;

/**
 * Created by 1017sjg on 2017. 7. 28..
 */

public class CrowdPayHistoryFragment extends BaseFragment implements AdapterView.OnItemSelectedListener, TabLayout.OnTabSelectedListener, RecyclerItemListener.RecyclerTouchListener {

    public int type = 1;
    public int period = 1;

    private CrowdPayHistoryBinding historyBinding;
    private ArrayList<CrowdPayItem> crowdPays;
    private CrowdPayHistoryListener listener;

    private CrowdHistoryAdapter adapter;
    public static CrowdPayHistoryFragment newInstance(CrowdPayHistoryListener listener, ArrayList<CrowdPayItem> crowdPays) {
        Bundle args = new Bundle();
        CrowdPayHistoryFragment fragment = new CrowdPayHistoryFragment();
        fragment.crowdPays = crowdPays;
        fragment.listener = listener;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        adapter = new CrowdHistoryAdapter(type, crowdPays);

        historyBinding = DataBindingUtil.inflate(inflater, R.layout.crowd_pay_history, container, false);
        historyBinding.setTitle("함께결제 내역");
        historyBinding.setOnClick(this);
        historyBinding.setTabListener(this);
        historyBinding.setTabArray(new String[]{"결제내역", "사용내역"});
        historyBinding.setTermSpinnerAdapter(new SpinnerAdapter(getRootActivity().getResources().getStringArray(R.array.usage_history_term_spinner), CENTER));
        historyBinding.setCrowdHistoryAdapter(adapter);
        historyBinding.setSpinnerListener(this);
        historyBinding.setHistoryTouch(this);

        return historyBinding.getRoot();
    }

    // 포인트 기간 설정에 대한 리스
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        period = 0 == position ? 1 : (3 == position ? 3 : 6);
        listener.changeCrowdPayHistory(type, period);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}

    // 포인트 상단의 탭 리스너
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        type = tab.getPosition() + 1;
        listener.changeCrowdPayHistory(type, period);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}
    @Override
    public void onTabReselected(TabLayout.Tab tab) {}

    public void changeCrowdPayHistory(ArrayList<CrowdPayItem> crowdPays) {
        this.crowdPays = crowdPays;
        if (historyBinding != null) {
            adapter.setParams(type, crowdPays);
            historyBinding.setCrowdHistoryAdapter(adapter);
        }
    }

    @Override
    public void onClickItem(View v, int position) {
        listener.crowdPayHistoryDetail(historyBinding.getCrowdHistoryAdapter().getObservableArrayList().get(position));
    }
}
