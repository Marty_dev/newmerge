package kr.co.mergepoint.mergeclient.scene.data.payment;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class CrowdPayResult {

    @SerializedName("crowdPayResult")
    public ArrayList<CrowdPayResultState> crowdPayResult;

}
