package kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.EnterpriseMenuListBinding;
import kr.co.mergepoint.mergeclient.scene.data.shop.DailyMenuCategory;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.adapter.EnterpriseMenuGroupAdapter;

/**
 * Created by 1017sjg on 2017. 6. 23..
 */

public class EnterpriseMenuFragment extends BaseFragment {

    private String[] weekStr = new String[] { "일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"};
    private Calendar calendar;
    private EnterpriseMenuListBinding listBinding;
    private ArrayList<DailyMenuCategory> menuCategories;

    public static EnterpriseMenuFragment newInstance(ArrayList<DailyMenuCategory> menuCategories, Calendar calendar) {
        Bundle args = new Bundle();
        EnterpriseMenuFragment fragment = new EnterpriseMenuFragment();
        fragment.calendar = calendar;
        fragment.menuCategories = menuCategories;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        listBinding = DataBindingUtil.inflate(inflater, R.layout.enterprise_menu_list, container, false);
        listBinding.setTitle("식단표 보기");
        listBinding.setOnClick(this);
        listBinding.setMenuAdapter(new EnterpriseMenuGroupAdapter(menuCategories));
        changeDailyMenuDate(calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.DAY_OF_WEEK));
        return listBinding.getRoot();
    }

    public void changeDailyMenuDate(int month, int day, int week) {
        listBinding.menuDate.setText(String.format(Locale.getDefault(), "%02d월 %02d일 %s", month + 1, day, weekStr[week - 1]));
    }

    public void changeDailyMenuAdapter(ArrayList<DailyMenuCategory> menuCategories, Calendar calendar) {
        this.calendar = calendar;
        this.menuCategories = menuCategories;
        listBinding.getMenuAdapter().setListData(this.menuCategories);
    }
}
