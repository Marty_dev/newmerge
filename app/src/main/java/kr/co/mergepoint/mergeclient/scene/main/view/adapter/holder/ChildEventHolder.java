package kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import kr.co.mergepoint.mergeclient.databinding.EventChildBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.EventInfo;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class ChildEventHolder extends ChildViewHolder {

    private EventInfo eventInfo;
    private EventChildBinding eventChildBinding;

    public EventInfo getEventInfo() {
        return eventInfo;
    }

    public ChildEventHolder(EventChildBinding eventChildBinding) {
        super(eventChildBinding.getRoot());
        this.eventChildBinding = eventChildBinding;
    }

    public void onBind(EventInfo eventInfo) {
        this.eventInfo = eventInfo;
        eventChildBinding.setEventInfo(eventInfo);
    }
}
