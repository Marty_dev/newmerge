package kr.co.mergepoint.mergeclient.scene.data.main;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * User: Marty
 * Date: 2018-08-01
 * Time: 오후 5:35
 * Description:
 */
public class Alarmdata {

    @SerializedName("sendDateTime")
    public  List<Integer> sendDateTime;
    @SerializedName("clickAction")
    public String clickAction;
    @SerializedName("dataMessage")
    public String dataMessage;
    @SerializedName("title")
    public String title;
    @SerializedName("memberRef")
    public int memberRef;
    @SerializedName("oid")
    public int oid;
}
