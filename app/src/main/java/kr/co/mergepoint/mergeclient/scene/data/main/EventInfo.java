package kr.co.mergepoint.mergeclient.scene.data.main;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 8. 21..
 */

public class EventInfo implements Parcelable {

    @SerializedName("oid")
    public int oid;

    @SerializedName("title")
    public String title;

    @SerializedName("listImage")
    public String listImage;

    @SerializedName("state")
    public int state;

    @SerializedName("creDateTime")
    public ArrayList<Integer> creDateTime;
    @SerializedName("modDateTime")
    public ArrayList<Integer> modDateTime;

    public EventInfo(int oid, String title, String listImage, int state, ArrayList<Integer> creDateTime, ArrayList<Integer> modDateTime) {
        this.oid = oid;
        this.title = title;
        this.listImage = listImage;
        this.state = state;
        this.creDateTime = creDateTime;
        this.modDateTime = modDateTime;
    }

    public EventInfo(Parcel in) {
        oid = in.readInt();
        title = in.readString();
        listImage = in.readString();
        state = in.readInt();
        in.readList(creDateTime, Integer.class.getClassLoader());
        in.readList(modDateTime, Integer.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(oid);
        dest.writeString(title);
        dest.writeString(listImage);
        dest.writeInt(state);
        dest.writeList(creDateTime);
        dest.writeList(modDateTime);
    }

    public static final Creator<EventInfo> CREATOR = new Creator<EventInfo>() {
        @Override
        public EventInfo createFromParcel(Parcel in) {
            return new EventInfo(in);
        }

        @Override
        public EventInfo[] newArray(int size) {
            return new EventInfo[size];
        }
    };
}
