package kr.co.mergepoint.mergeclient.scene.data.history;

/**
 * Created by 1017sjg on 2017. 8. 28..
 */

public class UseMenu {

    private int payMenu;
    private int useCount;

    public UseMenu(int payMenu, int useCount) {
        this.payMenu = payMenu;
        this.useCount = useCount;
    }

    public int getPayMenu() {
        return payMenu;
    }

    public void setPayMenu(int payMenu) {
        this.payMenu = payMenu;
    }

    public int getUseCount() {
        return useCount;
    }

    public void setUseCount(int useCount) {
        this.useCount = useCount;
    }
}
