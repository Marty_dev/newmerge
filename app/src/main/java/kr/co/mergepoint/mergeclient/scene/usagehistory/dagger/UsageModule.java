package kr.co.mergepoint.mergeclient.scene.usagehistory.dagger;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.usagehistory.UsageActivity;
import kr.co.mergepoint.mergeclient.scene.usagehistory.model.UsageModel;
import kr.co.mergepoint.mergeclient.scene.usagehistory.presenter.UsagePresenter;
import kr.co.mergepoint.mergeclient.scene.usagehistory.presenter.callback.UsageCallback;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.UsageView;

/**
 * Created by jgson on 2017. 5. 31..
 */

@Module
public class UsageModule {

    private UsageActivity usageActivity;

    public UsageModule(UsageActivity usageActivity) {
        this.usageActivity = usageActivity;
    }

    @Provides
    @UsageScope
    UsageView provideView() { return new UsageView(usageActivity, R.layout.usage_history); }

    @Provides
    @UsageScope
    UsageModel provideModel() { return new UsageModel(); }

    @Provides
    @UsageScope
    UsagePresenter providePresent(UsageView view, UsageModel model) {
        return new UsagePresenter(view, model, new UsageCallback(view, model));
    }
}
