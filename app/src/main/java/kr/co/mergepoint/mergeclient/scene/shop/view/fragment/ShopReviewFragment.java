package kr.co.mergepoint.mergeclient.scene.shop.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.CustomLinearLayoutManager;
import kr.co.mergepoint.mergeclient.application.common.ListLoadScrollListener;
import kr.co.mergepoint.mergeclient.application.common.RecyclerItemListener;
import kr.co.mergepoint.mergeclient.databinding.ShopDetailReviewBinding;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.MergeReview;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.ShopReviewAdapter;
import kr.co.mergepoint.mergeclient.scene.shop.view.listener.OnAddReviewsListener;

/**
 * Created by 1017sjg on 2017. 6. 23..
 */

public class ShopReviewFragment extends BaseFragment implements RecyclerItemListener.RecyclerTouchListener {

    private ShopDetailReviewBinding reviewBinding;
    private MergeReview mergeReview;
    private OnAddReviewsListener addReviewsListener;
    private ShopReviewAdapter reviewAdapter;

    public static ShopReviewFragment newInstance(OnAddReviewsListener addReviewsListener) {
        Bundle args = new Bundle();
        ShopReviewFragment fragment = new ShopReviewFragment();
        fragment.addReviewsListener = addReviewsListener;
        fragment.reviewAdapter = new ShopReviewAdapter();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        reviewBinding = DataBindingUtil.inflate(inflater, R.layout.shop_detail_review, container, false);
        reviewBinding.setReviewAdapter(reviewAdapter);
        reviewBinding.setReviewTouch(ShopReviewFragment.this);
        reviewBinding.reviewTable.setLayoutManager(new CustomLinearLayoutManager(getContext()));
        reviewBinding.reviewTable.addOnScrollListener(new ListLoadScrollListener() {
            @Override
            public void onLoadMore(int currentPage) {
                if (currentPage < mergeReview.totalPage)
                    addReviewsListener.addReviews(currentPage);
            }

            @Override
            public LinearLayoutManager setLinearLayoutManager() {
                return (CustomLinearLayoutManager) reviewBinding.reviewTable.getLayoutManager();
            }
        });
        reviewBinding.emptyReview.setVisibility(mergeReview != null && mergeReview.reviewList != null && mergeReview.reviewList.size() > 0 ? View.GONE : View.VISIBLE);
        return reviewBinding.getRoot();
    }

    public void setReviewAdapter(MergeReview mergeReview) {
        this.mergeReview = mergeReview;
        reviewAdapter.addData(mergeReview.reviewList);
        if (reviewBinding != null)
            reviewBinding.emptyReview.setVisibility(mergeReview.reviewList.size() > 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onClickItem(View v, int position) {
        super.onClick(v);
    }
}
