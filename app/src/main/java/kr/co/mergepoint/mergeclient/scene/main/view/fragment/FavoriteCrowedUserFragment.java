package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.FavoriteCrowdUserBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.FavoriteCrowedUserAdapter;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.SearchCrowdPayUserListener;

/**
 * Created by 1017sjg on 2017. 8. 8..
 */

public class FavoriteCrowedUserFragment extends BaseFragment implements TabLayout.OnTabSelectedListener, TextView.OnEditorActionListener {

    private FavoriteCrowdUserBinding favoriteCrowedUserBinding;
    private SearchCrowdPayUserListener listener;

    public static FavoriteCrowedUserFragment newInstance(SearchCrowdPayUserListener listener) {
        Bundle args = new Bundle();
        FavoriteCrowedUserFragment fragment = new FavoriteCrowedUserFragment();
        fragment.listener = listener;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listener.updateCrowedMemberTable(1, "");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        favoriteCrowedUserBinding = DataBindingUtil.inflate(inflater, R.layout.favorite_crowd_user, container, false);
        favoriteCrowedUserBinding.setTitle("즐겨찾기 관리");
        favoriteCrowedUserBinding.setOnClick(this);
        favoriteCrowedUserBinding.setTabArray(new String[]{"전체", "나의 부서", "즐겨찾기"});
        favoriteCrowedUserBinding.setTabListener(this);
        favoriteCrowedUserBinding.setSearchListener(this);
        return favoriteCrowedUserBinding.getRoot();
    }

    public void setFavoriteCrowedPayUserAdapter(ArrayList<CrowdMember> arrayList) {
        favoriteCrowedUserBinding.setUserAdapter(new FavoriteCrowedUserAdapter(arrayList,this));
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition() + 1;
        favoriteCrowedUserBinding.enterpriseSearchLayout.setVisibility(position == 3 ? View.GONE : View.VISIBLE);
        listener.updateCrowedMemberTable(tab.getPosition() + 1, favoriteCrowedUserBinding.searchText.getText().toString());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}

    @Override
    public void onTabReselected(TabLayout.Tab tab) {}

    @Override
    public boolean onEditorAction(TextView textView, int keyCode, KeyEvent keyEvent) {
        if (keyCode == EditorInfo.IME_ACTION_SEARCH) {
            listener.updateCrowedMemberTable(favoriteCrowedUserBinding.searchEnterpriseUserTab.getSelectedTabPosition() + 1, favoriteCrowedUserBinding.searchText.getText().toString());
            return true;
        }
        return false;
    }
}
