package kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder;

import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.databinding.EventGroupBinding;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class GroupEventHolder extends GroupViewHolder {

    private EventGroupBinding eventGroupBinding;

    public GroupEventHolder(EventGroupBinding eventGroupBinding) {
        super(eventGroupBinding.getRoot());
        this.eventGroupBinding = eventGroupBinding;
    }

    public void setGroupMenu(Event group) {
        eventGroupBinding.setGroup(group);
    }

    @Override
    public void expand() {
        eventGroupBinding.groupState.setImageResource(R.drawable.icon_event_close);
        eventGroupBinding.groupState.invalidate();
    }

    @Override
    public void collapse() {
        eventGroupBinding.groupState.setImageResource(R.drawable.icon_event_open);
        eventGroupBinding.groupState.invalidate();
    }
}
