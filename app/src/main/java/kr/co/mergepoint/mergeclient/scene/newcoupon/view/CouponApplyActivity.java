package kr.co.mergepoint.mergeclient.scene.newcoupon.view;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.NewCouponApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.newcoupon.adapter.NewCouponAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouponApplyActivity extends BaseActivity {

    BaseActivity activity = this;
    @BindView(R.id.coupon_apply_btn)
    TextView couponApplyBtn;

    @Override
    protected void onActivityClick(View view) {

    }

    NewCouponApi API;
    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.coupon_title)
    TextView couponTitle;
    @BindView(R.id.back)
    ConstraintLayout back;
    @BindView(R.id.basic_toolbar)
    Toolbar basicToolbar;
    @BindView(R.id.coupon_apply_text)
    TextView couponApplyText;
    @BindView(R.id.coupon_edt)
    EditText couponEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_apply2);
        ButterKnife.bind(this);
        API = MergeApplication.getMergeAppComponent().getRetrofit().create(NewCouponApi.class);

    }


    @OnClick({R.id.back_arrow, R.id.coupon_apply_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_arrow:
                finish();
                break;
            case R.id.coupon_apply_btn:
                hideKeyboard();

                if (couponEdt.getText().toString().isEmpty()){
                    Snackbar.make(findViewById(android.R.id.content), "쿠폰코드를 입력해주세요.", Snackbar.LENGTH_SHORT).show();
                }else{
                    API.applyCoupon(couponEdt.getText().toString().trim(),2).enqueue(new Callback<ResponseObject<String>>() {
                        @Override
                        public void onResponse(Call<ResponseObject<String>> call, Response<ResponseObject<String>> response) {
                            if (response.body().isFailed()){
                                new MergeDialog.Builder(activity).setContent(response.body().getMessage()).setCancelBtn(false).build().show();
                            }else{
                                Snackbar.make(findViewById(android.R.id.content), "쿠폰이 추가되었습니다.", Snackbar.LENGTH_SHORT).show();
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseObject<String>> call, Throwable t) {
                            Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.retry), Snackbar.LENGTH_SHORT).show();
                        }
                    });
                }
                break;
        }
    }
}
