package kr.co.mergepoint.mergeclient.application.common.mvvm;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import kr.co.mergepoint.mergeclient.application.common.mvvm.dagger.ActivityComponent;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public abstract class ItemViewModel<ITEM_T> extends ViewModel {
    public ItemViewModel(@Nullable State savedInstanceState, @NonNull ActivityComponent component) {
        super(savedInstanceState, component);
    }

    public abstract void setItem(ITEM_T item);
}
