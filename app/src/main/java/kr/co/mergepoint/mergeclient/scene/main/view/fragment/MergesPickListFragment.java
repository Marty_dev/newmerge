package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.MergesPickApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.RecyclerItemListener;
import kr.co.mergepoint.mergeclient.databinding.MergesPickListBinding;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.main.MergesPick;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.MergesPickListAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jgson on 2017. 7. 19..
 */

public class MergesPickListFragment extends BaseFragment implements RecyclerItemListener.RecyclerTouchListener {

    private MergesPickListBinding listBinding;

    public static MergesPickListFragment newInstance() {
        Bundle args = new Bundle();
        MergesPickListFragment fragment = new MergesPickListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        listBinding = DataBindingUtil.inflate(inflater, R.layout.merges_pick_list, container, false);
        listBinding.setTitle("MERGE'S PICK");
        listBinding.setOnClick(this);
        return listBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        requestMergesPickList();
    }

    private void requestMergesPickList() {
        MergeApplication.getMergeApplication().showLoading(rootActivity);
        MergesPickApi mergesPickApi = MergeApplication.getMergeAppComponent().getRetrofit().create(MergesPickApi.class);
        Call<ResponseObject<ArrayList<MergesPick>>> call = mergesPickApi.requestMergesPickList();
        call.enqueue(getMergesPickListCallback());
    }

    private Callback<ResponseObject<ArrayList<MergesPick>>> getMergesPickListCallback() {
        return new Callback<ResponseObject<ArrayList<MergesPick>>>() {
            @Override
            public void onResponse(@NonNull Call<ResponseObject<ArrayList<MergesPick>>> call, @NonNull Response<ResponseObject<ArrayList<MergesPick>>> response) {
                ResponseObject<ArrayList<MergesPick>> responseObject = response.body();
                if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                    listBinding.setListAdapter(new MergesPickListAdapter(responseObject.getObject()));
                    listBinding.setListTouch(MergesPickListFragment.this);
                }

                delayHideLoading();
            }

            @Override
            public void onFailure(@NonNull Call<ResponseObject<ArrayList<MergesPick>>> call, @NonNull Throwable t) {
                showAlert(R.string.retry);
                delayHideLoading();
            }
        };
    }

    @Override
    public void onClickItem(View v, int position) {
        super.onClick(v);
    }
}
