package kr.co.mergepoint.mergeclient.scene.search.view;

import android.support.v7.widget.Toolbar;
import android.view.View;

import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.databinding.SearchShopBinding;
import kr.co.mergepoint.mergeclient.scene.search.SearchActivity;
import kr.co.mergepoint.mergeclient.scene.search.presenter.SearchPresenter;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.ShopListAdapter;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class SearchView extends BaseView<SearchActivity, SearchPresenter, SearchShopBinding> {

    public SearchView(SearchActivity activity, int layout) {
        super(activity, layout);
    }

    @Override
    protected Toolbar setToolbar() {
        return binding.toolbarBinding.basicToolbar;
    }

    public void visibleTextClear(boolean isVisible) {
        binding.searchTextDelete.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    public boolean isSearchFocused() {
        return binding.searchEditText.isFocused();
    }

    public void searchTextClear() {
        binding.searchEditText.setText("");
    }

    public void searchClearFocused() {
        binding.searchEditText.clearFocus();
    }

    public void setSearchText(String trimString) {
        binding.searchList.setSearchText(trimString);
        binding.searchEditText.setText(trimString);
    }

    public String getSearchText() {
        return binding.searchEditText.getText().toString().trim();
    }

    public void emptyResults(boolean isShow) {
        binding.notSearch.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    public ShopListAdapter getShopListAdapter() {
        return (ShopListAdapter) binding.searchList.getAdapter();
    }
}
