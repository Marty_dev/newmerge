package kr.co.mergepoint.mergeclient.scene.search.presenter.callback;

import kr.co.mergepoint.mergeclient.application.common.BaseCallback;
import kr.co.mergepoint.mergeclient.scene.search.model.SearchModel;
import kr.co.mergepoint.mergeclient.scene.search.view.SearchView;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class SearchCallback extends BaseCallback<SearchView, SearchModel> implements SearchCallbackInterface {

    public SearchCallback(SearchView baseView, SearchModel baseModel) {
        super(baseView, baseModel);
    }
}
