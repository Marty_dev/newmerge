package kr.co.mergepoint.mergeclient.scene.payment.presenter.callback;

import android.support.v7.widget.AppCompatSpinner;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseCallback;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.login.MemberPoint;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.PayConfirm;
import kr.co.mergepoint.mergeclient.scene.data.payment.PaymentMenuChild;
import kr.co.mergepoint.mergeclient.scene.data.payment.PaymentMenuGroup;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.payment.model.PaymentModel;
import kr.co.mergepoint.mergeclient.scene.payment.presenter.PaymentPresenter;
import kr.co.mergepoint.mergeclient.scene.payment.view.PaymentView;
import kr.co.mergepoint.mergeclient.scene.payment.view.adapter.CouponSpinnerAdapter;
import kr.co.mergepoint.mergeclient.scene.payment.view.adapter.PaymentShopListAdapter;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.CrowdApprovalFragment;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.EnterpriseUsePointsFragment;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.SearchCrowdPayUserFragment;
import kr.co.mergepoint.mergeclient.util.Merge_Constant;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static kr.co.mergepoint.mergeclient.application.common.Properties.CROWED_APPROVAL_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_USE_POINTS_FRAGMENT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SEARCH_CROWD_PAY_FRAGMENT_TAG;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class PaymentCallback extends BaseCallback<PaymentView, PaymentModel> implements PaymentCallbackInterface {

    public PaymentCallback(PaymentView baseView, PaymentModel baseModel) {
        super(baseView, baseModel);
    }

    @Override
    public MergeCallback<ResponseObject<PayConfirm>>
    getPayConfirmCallback(PaymentPresenter presenter) {
        return getPayConfirmCallback(presenter, "", null, NO_VALUE, NO_VALUE);
    }

    @Override
    public MergeCallback<ResponseObject<PayConfirm>> getPayConfirmCallback(PaymentPresenter presenter, String message) {
        return getPayConfirmCallback(presenter, message, null, NO_VALUE, NO_VALUE);
    }

    @Override
    public MergeCallback<ResponseObject<PayConfirm>> getPayConfirmCallback(PaymentPresenter presenter, View view, int couponOid, int shopPaymentOid) {
        return getPayConfirmCallback(presenter, "", view, couponOid, shopPaymentOid);
    }

    @Override
    public MergeCallback<ResponseObject<PayConfirm>> getPayConfirmCallback(final PaymentPresenter presenter, String message, View view, int couponOid, int shopPaymentOid) {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<PayConfirm> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                PayConfirm payConfirm = responseObject.getObject();
                if (payConfirm != null) {
                    if (!responseObject.isFailed()) {
                        presenter._shoptype = payConfirm.shopPayment.get(0).shopType;
                        for(ShopPayment pays : payConfirm.shopPayment){
                            if (pays.shopType != 1) {
                                presenter._shoptype = pays.shopType;
                            }
                        }
                        /* 쿠폰 혹은 포인트 적용 성공 */
                        if (baseView.binding.getPayConfirm() == null) {
                            baseView.binding.setPayConfirm(payConfirm);

                            for (ShopPayment shopPayment : payConfirm.shopPayment) {
                                List<PaymentMenuGroup> menuGroups = new ArrayList<>();
                                for (ShopMenuDetail menuDetail : shopPayment.menu)
                                    menuGroups.add(new PaymentMenuGroup(menuDetail, new ArrayList<>(Collections.singletonList(new PaymentMenuChild(menuDetail.payPrice, menuDetail.count)))));
                                shopPayment.menuGroups = menuGroups;
                            }

                            //test logic
                            View card = baseView.activity.findViewById(R.id.payment_select_card);

                            if (payConfirm.shopPayment.get(0).shopType != 1){
                                card.setVisibility(View.GONE);
                            }else{
                                card.setVisibility(View.VISIBLE);
                            }


                            baseView.binding.setPaymentShopListAdapter(new PaymentShopListAdapter(payConfirm.shopPayment, payConfirm.availablePoints, presenter));
                            baseView.binding.setCouponAdapter(new CouponSpinnerAdapter(R.layout.coupon_normal, R.layout.coupon_dropdown));
                            baseView.binding.setCouponListener(presenter);
                            baseView.binding.paymentPrice.basketSelectCoupon.setOnTouchListener(presenter);

                            /* Model에 쿠폰 정보 및 포인트 정보 넣기 */
                            baseModel.setMergeCoupons(payConfirm.mergeCoupon);
                            baseModel.setBasketCoupons(payConfirm.cartCoupon);
                        }
                    } else {
                        /* 서버에서 온 오류 혹은 성공 메시지 보여주기 */
                        new MergeDialog.Builder(baseView.activity).setContent(message.isEmpty() ? responseObject.getMessage() : message).setCancelBtn(false).build().show();
                    }

                    if (view != null && view instanceof AppCompatSpinner && couponOid != NO_VALUE) {
                        if (responseObject.isFailed()) {
                            AppCompatSpinner spinner = (AppCompatSpinner) view;
                            spinner.setSelection(0);
                        }
                        int changePaymentOid = shopPaymentOid != NO_VALUE ? shopPaymentOid : payConfirm.oid;
                        int changeCouponOid = responseObject.isFailed() ? 0 : couponOid;
                        baseModel.changeCoupon(changePaymentOid, changeCouponOid);
                    }

                    if (baseView.binding.getPayConfirm() != null) {
                        baseView.binding.getPayConfirm().setCouponPrice(payConfirm.getCouponPrice());
                        baseView.binding.getPayConfirm().setPointPrice(payConfirm.getPointPrice());
                        baseView.binding.getPayConfirm().setPayPrice(payConfirm.getPayPrice());
                        baseView.binding.getPayConfirm().setPlannedPoints(payConfirm.getPlannedPoints());
                    }
                }
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CrowdPay>>> requestCrowedPayCallback(int shopPayOid) {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CrowdPay>> responseObject = response.body();
            if (responseObject != null) {
                ArrayList<CrowdPay> crowdPays = responseObject.getObject();
                if (crowdPays != null) {
                    baseView.openCrowedPay(shopPayOid, crowdPays, responseObject.getSource().equals("Single"));
                }
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<MemberPoint>> requestSettingCrowedPayPoint() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<MemberPoint> responseObject = response.body();
            if (responseObject != null) {
                MemberPoint memberPoint = responseObject.getObject();
                if (memberPoint != null) {
                    baseView.openSettingCrowedPayPoint(memberPoint);
                }
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CrowdMember>>> getCrowedMemberList() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CrowdMember>> responseObject = response.body();
            SearchCrowdPayUserFragment fragment = (SearchCrowdPayUserFragment) baseView.findFragmentByTag(SEARCH_CROWD_PAY_FRAGMENT_TAG);
            if (fragment != null && responseObject != null) {
                fragment.setSearchCrowedPayUserAdapter(responseObject.getObject());
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CrowdPay>>> requestSelectCrowedMember() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CrowdPay>> responseObject = response.body();

            EnterpriseUsePointsFragment pointsFragment = (EnterpriseUsePointsFragment) baseView.findFragmentByTag(ENTERPRISE_USE_POINTS_FRAGMENT_TAG);
            if (pointsFragment != null && responseObject != null) {
                pointsFragment.setEnterpriseUserAdapter(responseObject.getObject());
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CrowdPay>>> requestSelectPoint() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CrowdPay>> responseObject = response.body();

            EnterpriseUsePointsFragment pointsFragment = (EnterpriseUsePointsFragment) baseView.findFragmentByTag(ENTERPRISE_USE_POINTS_FRAGMENT_TAG);
            if (pointsFragment != null && responseObject != null) {
                MDEBUG.debug("ersponse reqpoint  : " + responseObject.getMessage());
                MDEBUG.debug("ersponse reqpoint  : " + responseObject.getCode());
                if (!responseObject.getCode().equals(Merge_Constant.SUCCESS_CODE)) {
                    new MergeDialog.Builder(baseView.activity).setContent(responseObject.getMessage()).setCancelBtn(false).build().show();
                    return;
                }

                pointsFragment.setCrowdPayList(responseObject.getObject());
                baseView.activity.onBackPressed();
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CrowdPay>>> requestCrowdApproval(final PaymentPresenter presenter) {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CrowdPay>> responseObject = response.body();

            if (responseObject != null) {
                ArrayList<CrowdPay> crowdPays = responseObject.getObject();
                Member member = MergeApplication.getMember();
                MDEBUG.debug("crows info : " + responseObject.getCode());
                MDEBUG.debug("crows info : " + responseObject.getMessage());
                for (CrowdPay pay : responseObject.getObject()){
                    MDEBUG.debug("pay info : " + pay.getApproveState());
                    MDEBUG.debug("pay info : " + pay.memberName);

                }
                if (!responseObject.getCode().equals(Merge_Constant.SUCCESS_CODE)) {
                    new MergeDialog.Builder(baseView.activity).setContent(responseObject.getMessage()).setCancelBtn(false).build().show();
                    return;
                }

                if (crowdPays.size() == 1 && member != null && crowdPays.get(0).memberRef == member.getOid()) {
                    /* 기업포인트로 혼자 승인요청 했을 때 */
                    int payOid = baseView.getPayConfirm().oid;
                    baseView.activity.onBackPressed();
                    baseView.showLoading();
                    baseModel.readPayment(readPayment(presenter), payOid);
                } else {
                    /* 함께 결제 승인요청 했을 때 */
//                    for (CrowdPay pay : crowdPays){
//                    }

                    baseView.openCrowdApproval(crowdPays);
                }
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<PayConfirm>> readPayment(final PaymentPresenter presenter) {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<PayConfirm> responseObject = response.body();
            if (responseObject != null) {
                MDEBUG.debug("response message: " + responseObject.getMessage().toString());
                MDEBUG.debug("response getCompanyPoints : " + responseObject.getObject().getCompanyPoints());
                MDEBUG.debug("response getCouponPrice : " + responseObject.getObject().getCouponPrice());
                MDEBUG.debug("response getPayPrice : " + responseObject.getObject().getPayPrice());
                MDEBUG.debug("responsMe getPlannedPoints : " + responseObject.getObject().getPlannedPoints());
                MDEBUG.debug("response getPointPrice : " + responseObject.getObject().getPointPrice());
                MDEBUG.debug("response payConfirm.shopPayment.get(0).shopType : " + responseObject.getObject().shopPayment.get(0).shopType);


                PayConfirm payConfirm = responseObject.getObject();

                for (ShopPayment s : payConfirm.shopPayment){
                    MDEBUG.debug("for : "+ s.shopType);
                }
                if(payConfirm.getCompanyPoints() != 0  || payConfirm.getCouponPrice() != 0){
                    if(payConfirm.getPayPrice() != 0){
                        presenter._isBillingOkByEnterPrizePoint = false;
                    }
                    else{
                        presenter._isBillingOkByEnterPrizePoint = true;
                    }
                }
                PayConfirm currentPayConfirm = baseView.binding.getPayConfirm();
                currentPayConfirm.setCouponPrice(payConfirm.getCouponPrice());
                currentPayConfirm.setCompanyPoints(payConfirm.getCompanyPoints());
                currentPayConfirm.setPointPrice(payConfirm.getPointPrice());
                currentPayConfirm.setPlannedPoints(payConfirm.getPlannedPoints());
                currentPayConfirm.setPayPrice(payConfirm.getPayPrice());
//                for (int i = 0; i < currentPayConfirm.shopPayment.size(); i++) {
//                    ShopPayment currentShopPayment = currentPayConfirm.shopPayment.get(i);
//                    currentShopPayment.setCompanyPoints(payConfirm.shopPayment.get(i).companyPoints);
//                }


                baseView.binding.setPayConfirm(currentPayConfirm);
                for (int i = 0; i < payConfirm.shopPayment.size(); i++) {
                    ShopPayment shopPayment = payConfirm.shopPayment.get(i);
                    baseView.binding.getPaymentShopListAdapter().changeShopPayment(shopPayment.oid, shopPayment.companyPoints);
                }
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Long>> changeFavoriteCrowdMember() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Long> responseObject = response.body();
            if (responseObject != null) {
//                fragment.setFavoriteCrowedPayUserAdapter(responseObject.getObject());
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<CrowdPay>>> updateCrowdApproval() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<CrowdPay>> responseObject = response.body();

            if (responseObject != null) {
                ArrayList<CrowdPay> crowdPays = responseObject.getObject();
                CrowdApprovalFragment approvalFragment = (CrowdApprovalFragment) baseView.findFragmentByTag(CROWED_APPROVAL_FRAGMENT_TAG);
                if (approvalFragment != null) {
                    approvalFragment.updateApprovalTable(crowdPays);

                }
            }
        });
    }
}
