package kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.UsageHistoryShopItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.history.ShopPaidMenu;

/**
 * Created by jgson on 2017. 7. 29..
 */

public class ShopHistoryAdapter extends BasicListAdapter<BasicListHolder<UsageHistoryShopItemBinding, ShopPaidMenu>, ShopPaidMenu> {

    private MenuHistoryAdapter.OnUseMenu onUseMenu;

    public ShopHistoryAdapter(ArrayList<ShopPaidMenu> arrayList, MenuHistoryAdapter.OnUseMenu onUseMenu) {
        super(arrayList);
        this.onUseMenu = onUseMenu;
    }

    @Override
    public BasicListHolder<UsageHistoryShopItemBinding, ShopPaidMenu> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.usage_history_shop_item, parent, false);
        UsageHistoryShopItemBinding shopItemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<UsageHistoryShopItemBinding, ShopPaidMenu>(shopItemBinding) {
            @Override
            public void setDataBindingWithData(ShopPaidMenu data) {
                getDataBinding().setShopPayment(data);
                getDataBinding().setMenuAdapter(new MenuHistoryAdapter(data.payMenu, onUseMenu));
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<UsageHistoryShopItemBinding, ShopPaidMenu> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
