package kr.co.mergepoint.mergeclient.scene.usagehistory.presenter.callback;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public interface UsageCallbackInterface {
    /*이용내역 리스트 받아오기*/
    MergeCallback<ResponseObject<ArrayList<UsagePayment>>> getHistoryCallback(int position);

    /*결제취소 및 포인트환불*/
    MergeCallback<ResponseObject<Integer>> getPayCancel(final boolean isPayCancel);

    /*메뉴사용하기*/
    MergeCallback<ResponseObject<UsagePayment>> getSendMenuCallback();

    /*사용한 메뉴 승인하기*/
    MergeCallback<ResponseObject<Integer>> approveMenu();

    /*사용한 메뉴 거절하기*/
    MergeCallback<ResponseObject<Integer>> rejectMenu();
}
