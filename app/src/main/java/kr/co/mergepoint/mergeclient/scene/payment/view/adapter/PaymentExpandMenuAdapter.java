package kr.co.mergepoint.mergeclient.scene.payment.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.databinding.PaymentMenuChildBinding;
import kr.co.mergepoint.mergeclient.databinding.PaymentMenuGroupBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.PaymentMenuChild;
import kr.co.mergepoint.mergeclient.scene.data.payment.PaymentMenuGroup;
import kr.co.mergepoint.mergeclient.scene.payment.view.adapter.holder.PaymentChildMenuHolder;
import kr.co.mergepoint.mergeclient.scene.payment.view.adapter.holder.PaymentGroupMenuHolder;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class PaymentExpandMenuAdapter extends ExpandableRecyclerViewAdapter<PaymentGroupMenuHolder, PaymentChildMenuHolder> {


    PaymentExpandMenuAdapter(List<PaymentMenuGroup> groups) {
        super(groups);
    }

    @Override
    public PaymentGroupMenuHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_menu_group, parent, false);
        PaymentMenuGroupBinding groupBinding = DataBindingUtil.bind(view);
        return new PaymentGroupMenuHolder(groupBinding);
    }

    @Override
    public PaymentChildMenuHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_menu_child, parent, false);
        PaymentMenuChildBinding childBinding = DataBindingUtil.bind(view);
        return new PaymentChildMenuHolder(childBinding);
    }

    @Override
    public void onBindGroupViewHolder(PaymentGroupMenuHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setGroupMenu((PaymentMenuGroup) group);
    }

    @Override
    public void onBindChildViewHolder(PaymentChildMenuHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        PaymentMenuChild childInfo = ((PaymentMenuGroup) group).getItems().get(childIndex);
        holder.onBind(childInfo);
    }
}
