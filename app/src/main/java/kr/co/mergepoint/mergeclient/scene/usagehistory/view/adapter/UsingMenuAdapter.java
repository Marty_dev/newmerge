package kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.UsedHistoryMenuListItemBinding;
import kr.co.mergepoint.mergeclient.databinding.UsingMenuListItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.history.UsedMenu;

/**
 * Created by 1017sjg on 2017. 7. 29..
 */

public class UsingMenuAdapter extends BasicListAdapter<BasicListHolder<UsingMenuListItemBinding, UsedMenu>, UsedMenu> {

    public UsingMenuAdapter(ArrayList<UsedMenu> arrayList) {
        super(arrayList);
    }

    @Override
    public BasicListHolder<UsingMenuListItemBinding, UsedMenu> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.using_menu_list_item, parent, false);
        UsingMenuListItemBinding menuListItemBinding = DataBindingUtil.bind(view);


        return new BasicListHolder<UsingMenuListItemBinding, UsedMenu>(menuListItemBinding) {
            @Override
            public void setDataBindingWithData(UsedMenu data) {
                getDataBinding().setUseCount(data.useCount);
                getDataBinding().setMenuDetail(data.payMenu);
                getDataBinding().setOptionAdapter(new OptionMenuHistoryAdapter(data.payMenu.optionMenu));
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<UsingMenuListItemBinding, UsedMenu> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
        holder.getDataBinding().seperator.setVisibility(position == 1 ? View.GONE : View.VISIBLE);
        holder.getDataBinding().seperator.setVisibility(position == getObservableArrayList().size() - 1 ? View.GONE : View.VISIBLE);

    }
}
