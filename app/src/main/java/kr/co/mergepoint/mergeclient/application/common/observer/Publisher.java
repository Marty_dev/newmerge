package kr.co.mergepoint.mergeclient.application.common.observer;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public interface Publisher<T> {
    void add(T observer);
    void delete(T observer);
    void notifyObserver();
}
