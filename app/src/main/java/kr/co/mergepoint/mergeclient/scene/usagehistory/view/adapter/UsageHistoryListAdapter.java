package kr.co.mergepoint.mergeclient.scene.usagehistory.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.application.common.DateModel;
import kr.co.mergepoint.mergeclient.databinding.UsageHistoryListItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.util.MDEBUG;

/**
 * Created by 1017sjg on 2017. 7. 25..
 */

public class UsageHistoryListAdapter extends BasicListAdapter<BasicListHolder<UsageHistoryListItemBinding, UsagePayment>, UsagePayment> {

    private boolean isPaymentTab;

    public UsageHistoryListAdapter(ArrayList<UsagePayment> arrayList, boolean isPaymentTab) {
        super(arrayList);
        this.isPaymentTab = isPaymentTab;
    }

    public UsageHistoryListAdapter() {

    }
    public void setParams(ArrayList<UsagePayment> arrayList, boolean isPaymentTab) {
        if (arrayList != null) {
            getObservableArrayList().clear();
            getObservableArrayList().addAll(arrayList);
        }
        this.isPaymentTab = isPaymentTab;
    }


    @Override
    public BasicListHolder<UsageHistoryListItemBinding, UsagePayment> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.usage_history_list_item, parent, false);
        UsageHistoryListItemBinding itemBinding = DataBindingUtil.bind(view);

        TextView rounded = (TextView)view.findViewById(R.id.rounded_text);
        TextView storetext = (TextView)view.findViewById(R.id.store_text);
        TextView itemdate = (TextView)view.findViewById(R.id.date_text);
        TextView menutext = (TextView)view.findViewById(R.id.menu_text);

        rounded.setVisibility(isPaymentTab ? View.VISIBLE : View.GONE);
//        storetext.setText("tester");
        return new BasicListHolder<UsageHistoryListItemBinding, UsagePayment>(itemBinding) {
            @Override
            public void setDataBindingWithData(UsagePayment data) {
                MDEBUG.debug("usage history databinding is Acting");
                MDEBUG.debug("is Payment Tap : " + isPaymentTab);
                MDEBUG.debug("is Data date : " + (data.getDate() == null ? "null" : "Notnull"));
                getDataBinding().setUsageHistory(data);
                getDataBinding().setIsPaymentTab(isPaymentTab);
                MDEBUG.debug("1");
                if (isPaymentTab){
                    MDEBUG.debug("2");

                    if (data.getDate() != null || data.getStateNum() >= 3) {
                        MDEBUG.debug("3");
                        final DateModel dateModel = new DateModel();
                        MDEBUG.debug("data.getState Num : " + data.getStateNum());
                        switch (data.getStateNum()) {
                            case 1: /* 미사용 */
                                rounded.setBackgroundResource(R.drawable.rounded_rect_fill_green);
                                rounded.setText(String.format(Locale.getDefault(), "D-%02d", dateModel.getCalculateDay(data.getDate())));
                                break;
                                case 2: /* 일부 사용 */
                                    rounded.setBackgroundResource(R.drawable.rounded_rect_fill_red);
                                    rounded.setText(String.format(Locale.getDefault(), "D-%02d", dateModel.getCalculateDay(data.getDate())));
                                    break;
                                case 3: /* 모두 사용 */
                                    rounded.setBackgroundResource(R.drawable.rounded_rect_fill_gray_02);
                                    rounded.setText("사용완료");
                                    break;
                                case 4: /* 포인트 전환 */
                                    rounded.setBackgroundResource(R.drawable.rounded_rect_fill_black_02);
                                    rounded.setText("기간만료");
                                    break;
                                case 5: /* 사용기간 만료 */
                                    rounded.setBackgroundResource(R.drawable.rounded_rect_fill_yellow);
                                    rounded.setText("포인트전환");
                                    break;
                                default:
                                    break;
                            }
                    }
                }
                if (data.getPayTime() != null) {
                    DateModel dateModel = new DateModel();
                    itemdate.setText(dateModel.getStringDateWithSeconds(data.getPayTime()));
                }else{
                    MDEBUG.debug("getPayTime is null");
                    if (data.getDate() == null)
                        MDEBUG.debug("getdate is null");
                }
                menutext.setText(data.getMenu());
                storetext.setText(data.getStore());

            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<UsageHistoryListItemBinding, UsagePayment> holder, int position) {
        MDEBUG.debug("data binding is bind");
        holder.bind(getObservableArrayList().get(position));
    }
}
