package kr.co.mergepoint.mergeclient.scene.login.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.ForgotPasswordResultBinding;

/**
 * Created by jgson on 2017. 7. 19..
 */

public class ForgotPwResultFragment extends BaseFragment {

    public static ForgotPwResultFragment newInstance() {
        Bundle args = new Bundle();
        ForgotPwResultFragment fragment = new ForgotPwResultFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ForgotPasswordResultBinding resultBinding = DataBindingUtil.inflate(inflater, R.layout.forgot_password_result, container, false);
        resultBinding.setTitle(getString(R.string.forgot_pw_title));
        resultBinding.setOnClick(this);
        return resultBinding.getRoot();
    }
}
