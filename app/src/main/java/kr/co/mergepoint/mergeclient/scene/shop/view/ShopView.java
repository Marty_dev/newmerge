package kr.co.mergepoint.mergeclient.scene.shop.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.facebook.CallbackManager;

import java.util.ArrayList;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.application.common.RecyclerItemListener;
import kr.co.mergepoint.mergeclient.application.common.ShareFragment;
import kr.co.mergepoint.mergeclient.application.common.SimpleWebFragment;
import kr.co.mergepoint.mergeclient.databinding.ShopDetailBinding;
import kr.co.mergepoint.mergeclient.scene.data.basket.AddBasket;
import kr.co.mergepoint.mergeclient.scene.data.basket.OptionMenu;
import kr.co.mergepoint.mergeclient.scene.data.basket.SelectOption;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionMenu;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionSelectMenu;
import kr.co.mergepoint.mergeclient.scene.data.shop.PreExperience;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.GoogleSearchInfo;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.MergeReview;
import kr.co.mergepoint.mergeclient.scene.login.LoginActivity;
import kr.co.mergepoint.mergeclient.scene.main.view.fragment.CombineImagesDetailFragment;
import kr.co.mergepoint.mergeclient.scene.payment.PaymentActivity;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.PaymentResultFragment;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;
import kr.co.mergepoint.mergeclient.scene.shop.presenter.ShopPresenter;
import kr.co.mergepoint.mergeclient.scene.shop.view.fragment.PreExperienceDetailFragment;
import kr.co.mergepoint.mergeclient.scene.shop.view.fragment.PreExperienceFragment;
import kr.co.mergepoint.mergeclient.scene.shop.view.fragment.ShopMenuDetailFragment;
import kr.co.mergepoint.mergeclient.scene.shop.view.fragment.ShopMenuFragment;
import kr.co.mergepoint.mergeclient.scene.shoplist.ShopListActivity;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.BasketActivity;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.Payment_v2_Activity;

import static android.app.Activity.RESULT_OK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INTENT_SHOP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MENU_DETAIL_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MERGES_PICK_DETAIL_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NONMEMBER;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NOTBUY;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAYMENT_RESULT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PLAY;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RESET;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHARE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_FAVORITE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_OID;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_REVIEW_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SOURCE_ACTIVITY;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class ShopView extends BaseView<ShopActivity, ShopPresenter, ShopDetailBinding> implements ShopViewInterface{

    private AnimatorSet animatorSet;

    public ShopView(ShopActivity activity, int layout) {
        super(activity, layout);
    }

    @Override
    protected Toolbar setToolbar() {
        return binding.shopDetailToolbarLayout.basicToolbar;
    }

    @Override
    public void openPayment() {
        AddBasket addBasket = getSelectOptions();
        if (addBasket ==  null) {
            new MergeDialog.Builder(activity).setContent(getString(R.string.soldout_menu_text)).setCancelBtn(false).build().show();
        } else {
            openActivityForResultWithParcelable(Payment_v2_Activity.class, PAYMENT_RESULT, addBasket, PAY_RESULT_REQUEST);
        }
    }
    public void openPaymentByPrice(int price , int shopRef) {
            openActivityForResult(Payment_v2_Activity.class, price, shopRef);
    }
    @Override
    public void openShopShared(ShopDetail shopDetail) {
        openShareFragment(R.id.shop_detail_root, ShareFragment.newInstance(
                shopDetail.shopName,
                shopDetail.introduce,
                shopDetail.shopImage.size() > 0 ? shopDetail.shopImage.get(0) : "",
                shopDetail.oid, INTENT_SHOP), SHARE);
    }

    @Override
    public void openMergesPickDetail(Bundle bundle) {
        openFragment(RIGHT_LEFT, R.id.shop_detail_root, CombineImagesDetailFragment.newInstance(getString(R.string.merges_pick), null, bundle), MERGES_PICK_DETAIL_TAG);
    }

    @Override
    public void openPreExperience(ArrayList<PreExperience> preExperiences, String shopName) {
        if (preExperiences != null) {
            int index = binding.getPagerAdapter().getInfoFragment().getPreExperienceIndex();
            RecyclerItemListener.RecyclerTouchListener touch = (v, position) -> openFragment(RIGHT_LEFT, R.id.shop_detail_root, PreExperienceDetailFragment.newInstance(preExperiences, position, shopName), MENU_DETAIL_TAG);
            openFragment(RIGHT_LEFT, R.id.shop_detail_root, PreExperienceFragment.newInstance(touch, preExperiences, index), MENU_DETAIL_TAG);
        }
    }

    @Override
    public void openMenuDetail(ShopMenuDetail detail) {
        openFragment(RIGHT_LEFT, R.id.shop_detail_root, ShopMenuDetailFragment.newInstance(detail), MENU_DETAIL_TAG);
    }


    public void openPaymentResult(int paymentRef,boolean ismulti) {
        openFragment(RIGHT_LEFT, R.id.shop_detail_root, PaymentResultFragment.newInstance(activity,paymentRef,R.id.shop_detail_root,ismulti), PAY_RESULT_TAG);
    }
    @Override
    public void openPaymentResult() {
        openFragment(RIGHT_LEFT, R.id.shop_detail_root, PaymentResultFragment.newInstance(), PAY_RESULT_TAG);
    }

    @Override
    public void openShoppingBasket() {
        openActivityForResult(BasketActivity.class, ShopListActivity.class, SOURCE_ACTIVITY, PAY_RESULT_REQUEST);
    }

    @Override
    public void openMapIntent(double latitude, double longitude) {
        Uri uri = Uri.parse(String.format(Locale.ENGLISH, "geo:%f, %f?q=%f, %f", latitude, longitude,latitude, longitude));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
        if (mapIntent.resolveActivity(activity.getPackageManager()) != null)
            activity.startActivity(mapIntent);
    }

    public void openMapIntent(double latitude, double longitude,String placename) {
        Uri uri = Uri.parse(String.format(Locale.ENGLISH, "geo:%f, %f?q=%s", latitude, longitude,placename));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
        if (mapIntent.resolveActivity(activity.getPackageManager()) != null)
            activity.startActivity(mapIntent);
    }

    @Override
    public void openReviewWeb(Bundle bundle) {
        openFragment(RIGHT_LEFT, R.id.shop_detail_root, SimpleWebFragment.newInstance(bundle, binding.getShopDetail().shopName), SHOP_REVIEW_TAG);
    }

    @Override
    public AddBasket getSelectOptions() {
        ShopMenuDetailFragment fragment = (ShopMenuDetailFragment) activity.getSupportFragmentManager().findFragmentByTag(MENU_DETAIL_TAG);
        ShopMenuDetail detail = fragment.getShopMenuDetail();

        if (detail.soldout)
            return null;

        ArrayList<OptionMenu> kindOptions = new ArrayList<>();
        for (ShopOptionMenu optionMenu: fragment.getOptionMenuArray()) {
            ArrayList<SelectOption> selectOptions = new ArrayList<>();

            for (ShopOptionSelectMenu selectMenu : optionMenu.menus) {
                if (selectMenu.getCount() > 0)
                    selectOptions.add(new SelectOption(selectMenu.oid, selectMenu.count));
            }

            if (selectOptions.size() > 0)
                kindOptions.add(new OptionMenu(optionMenu.oid, selectOptions));
        }

        return new AddBasket(detail.shopRef, detail.oid, detail.count, kindOptions.size() < 1 ? null : kindOptions,binding.getShopDetail().consummerRef);
    }

    @Override
    public ShopMenuFragment getShopMenuFragment() {
        return binding.getPagerAdapter().getMenuFragment();
    }

    @Override
    public CallbackManager getShareFacebookCallback() {
        ShareFragment fragment = (ShareFragment) findFragmentByTag(SHARE);
        return fragment != null ? fragment.getCallbackManager() : null;
    }

    @Override
    public void setReviewAdapter(MergeReview mergeReview) {
        binding.getPagerAdapter().getReviewFragment().setReviewAdapter(mergeReview);
    }

    @Override
    public boolean startShoppingBasket() {
        if (MergeApplication.isLoginState()) {
            AddBasket addBasket = getSelectOptions();
            if (addBasket ==  null) {
                new MergeDialog.Builder(activity).setContent(getString(R.string.soldout_menu_text)).setCancelBtn(false).build().show();
            } else {
                return true;
            }
        } else {
            openActivityWithClass(LoginActivity.class, ShopActivity.class, SOURCE_ACTIVITY);
        }

        return false;
    }

    @Override
    public void animBasket() {
        binding.shopListCart.getCartCount(this);
    }

    @Override
    @Properties.LikeState
    public int animateLike() {
        if (MergeApplication.isLoginState() && binding.getShopDetail() != null) {
            final ImageView heart = binding.shopDetailPictureLayout.likeAnim;
            final TextSwitcher heartNum = binding.shopDetailPictureLayout.likeNum;
            final int nextCount = binding.getShopDetail().givenLikeCount + 1;

            if (binding.getShopDetail().maxLikeCount == 0) {
                return NOTBUY;
            } else if (nextCount <= binding.getShopDetail().maxLikeCount) {
                heartNum.setText(String.format(Locale.getDefault(), "%d", nextCount));

                if (animatorSet == null)
                    createAnimSet(heart, heartNum);

                if (animatorSet.isRunning())
                    animatorSet.end();
                animatorSet.start();
                binding.getShopDetail().setGivenLikeCount(nextCount);

                return PLAY;
            } else {
                return RESET;
            }
        } else {
            return NONMEMBER;
        }
    }

    @Override
    public void createAnimSet(final ImageView heart, final TextSwitcher heartNum) {
        animatorSet = new AnimatorSet();
        ObjectAnimator scaleUpXAnim = createAnim(heart, "scaleX", 0f, 1f, 400, new OvershootInterpolator(6));
        ObjectAnimator scaleUpYAnim = createAnim(heart, "scaleY", 0f, 1f, 400, new OvershootInterpolator(6));
        ObjectAnimator scaleDownXAnim = createAnim(heart, "scaleX", 1f, 0f, 200, new AccelerateInterpolator());
        ObjectAnimator scaleDownYAnim = createAnim(heart, "scaleY", 1f, 0f, 200, new AccelerateInterpolator());

        ObjectAnimator alphaShow = createAnim(heartNum, "alpha", 0f, 1f, 200, new AccelerateInterpolator());
        ObjectAnimator alphaHidden = createAnim(heartNum, "alpha", 1f, 0f, 400, new AccelerateInterpolator());

        animatorSet.playTogether(scaleUpYAnim, scaleUpXAnim, alphaShow);
        animatorSet.play(scaleDownYAnim).with(scaleDownXAnim).with(alphaHidden).after(250).after(scaleUpYAnim);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                heart.setVisibility(View.VISIBLE);
                heartNum.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                heart.setVisibility(View.GONE);
                heartNum.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public ObjectAnimator createAnim(View view, String property, float start, float end, int duration, TimeInterpolator interpolator) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, property, start, end);
        objectAnimator.setDuration(duration);
        objectAnimator.setInterpolator(interpolator);

        return objectAnimator;
    }

    @Override
    public void setupLikeTextSwitcher() {
        binding.shopDetailPictureLayout.likeNum.setFactory(() -> {
            TextView textView = new TextView(activity);
            Typeface typeface;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                typeface = activity.getResources().getFont(R.font.spoqa_han_sans_bold);
            } else {
                typeface = ResourcesCompat.getFont(activity, R.font.spoqa_han_sans_bold);
            }
            textView.setTypeface(typeface);

            textView.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                textView.setTextAppearance(activity, R.style.LikeNumText);
            } else {
                textView.setTextAppearance(R.style.LikeNumText);
            }

            return textView;
        });

        binding.shopDetailPictureLayout.likeNum.setInAnimation(AnimationUtils.loadAnimation(activity, R.anim.fade_in_like));
        binding.shopDetailPictureLayout.likeNum.setOutAnimation(AnimationUtils.loadAnimation(activity, R.anim.fade_out_like));
    }

    @Override
    public void callShopPhone() {
        ShopDetail detail = binding.getShopDetail();
        if (detail != null)
            activity.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + detail.getShopPhone())));
    }

    @Override
    public void onBackPressed() {
        if (MergeApplication.isLoginState() && binding.getShopDetail() != null) {
            Intent shopFavorite = new Intent();
            shopFavorite.putExtra(SHOP_FAVORITE, binding.shopDetailToolbarLayout.favoritShop.isFavorite());
            shopFavorite.putExtra(SHOP_OID, binding.getShopDetail().oid);
            activity.setResult(RESULT_OK, shopFavorite);
        }
    }
}
