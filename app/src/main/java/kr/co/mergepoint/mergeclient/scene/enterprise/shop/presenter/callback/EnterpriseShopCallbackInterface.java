package kr.co.mergepoint.mergeclient.scene.enterprise.shop.presenter.callback;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.menu.ShopGroupCategory;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import okhttp3.ResponseBody;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public interface EnterpriseShopCallbackInterface {

    /*매장의 상세정보를 받아온 결과*/
    MergeCallback<ShopDetail> shopDetailCallback();

    /*장바구니에 추가했을 때 결과*/
    MergeCallback<ResponseBody> addBasketCallback() ;

    /*메뉴 디테일 정보 응답*/
    MergeCallback<ShopMenuDetail> addMenuDetail();

    MergeCallback<ResponseObject<Integer>> getCheckFavoriteCall();

    MergeCallback<ShopGroupCategory> getMenuGroupCategory();

    MergeCallback<ResponseObject<ArrayList<ShopMenuDetail>>> getShopDailyMenuList();
}
