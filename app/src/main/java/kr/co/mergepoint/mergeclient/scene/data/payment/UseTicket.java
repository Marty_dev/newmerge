package kr.co.mergepoint.mergeclient.scene.data.payment;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class UseTicket implements Parcelable {
    @SerializedName("ticketCount")
    public int ticketCount;

    @SerializedName("returnPoints")
    public int returnPoints;

    @SerializedName("companyTicket")
    public Ticket companyTicket;

    private UseTicket(Parcel in) {
        ticketCount = in.readInt();
        returnPoints = in.readInt();
        companyTicket = in.readParcelable(Ticket.class.getClassLoader());
    }

    public static final Parcelable.Creator<UseTicket> CREATOR = new Parcelable.Creator<UseTicket>() {
        @Override
        public UseTicket createFromParcel(Parcel in) {
            return new UseTicket(in);
        }

        @Override
        public UseTicket[] newArray(int size) {
            return new UseTicket[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ticketCount);
        dest.writeInt(returnPoints);
        dest.writeParcelable(companyTicket, flags);
    }
}
