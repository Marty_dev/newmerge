package kr.co.mergepoint.mergeclient.scene.payment.model;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;

import android.text.TextUtils;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.EditText;

import java.util.HashMap;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.EnterpriseApi;
import kr.co.mergepoint.mergeclient.api.PaymentApi;
import kr.co.mergepoint.mergeclient.application.common.BaseModel;
import kr.co.mergepoint.mergeclient.application.common.Properties;
import kr.co.mergepoint.mergeclient.scene.data.basket.AddBasket;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.login.MemberPoint;
import kr.co.mergepoint.mergeclient.scene.data.payment.AdaptPoint;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;
import kr.co.mergepoint.mergeclient.scene.data.payment.PayConfirm;
import okhttp3.ResponseBody;
import retrofit2.Callback;

import static kr.co.mergepoint.mergeclient.application.common.Properties.AVAILABLE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.EXCEEDED;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NOT_APPLICABLE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.NUMBER_FORMAT_EXCEPTION;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class PaymentModel extends BaseModel {

    private @Properties.PointApplyState int pointState;

    private PaymentApi paymentApi;
    private EnterpriseApi enterpriseApi;
    private SparseIntArray useCoupon;
    private ArrayList<Coupon> mergeCoupons;
    private ArrayList<Coupon> basketCoupons;
    public HashMap<View, Integer> usedPoints;

    @Properties.PointApplyState
    private int getPointState() {
        return pointState;
    }

    private void setPointState(@Properties.PointApplyState int pointState) {
        this.pointState = pointState;
    }

    public PaymentModel() {
        useCoupon = new SparseIntArray();
        usedPoints = new HashMap<>();
        paymentApi = retrofit.create(PaymentApi.class);
        enterpriseApi = retrofit.create(EnterpriseApi.class);
    }

    /* API */
    public void requestPayment(Callback<ResponseObject<PayConfirm>> confirmCallback, AddBasket basket) {
        paymentApi.requestPayment(basket).enqueue(confirmCallback);
    }

    public void requestPayment(Callback<ResponseObject<PayConfirm>> confirmCallback, ArrayList<Integer> oids) {
        paymentApi.requestPayment(TextUtils.join(",", oids)).enqueue(confirmCallback);
    }

    public void requestEasyPayment(Callback<ResponseBody> billPayResultCallback, int cardOid) {
        paymentApi.requestEasyPayment(cardOid).enqueue(billPayResultCallback);
    }

    public void requestPaymentCoupon(Callback<ResponseObject<PayConfirm>> payConfirmCallback, int paymentOid, int couponOid) {
        paymentApi.requestPaymentCoupon(paymentOid, couponOid).enqueue(payConfirmCallback);
    }

    public void requestPaymentShopCoupon(Callback<ResponseObject<PayConfirm>> payConfirmCallback, int shopPaymentOid, int couponOid) {
        paymentApi.requestPaymentShopCoupon(shopPaymentOid, couponOid).enqueue(payConfirmCallback);
    }

    public void requestPaymentShopPoint(Callback<ResponseObject<PayConfirm>> payConfirmCallback, int shopPaymentOid, int point) {
        paymentApi.requestPaymentShopPoints(shopPaymentOid, point).enqueue(payConfirmCallback);
    }

    public void requestCrowedPay(Callback<ResponseObject<ArrayList<CrowdPay>>> callback, int payShopOid) {
        enterpriseApi.requestCrowedPay(payShopOid).enqueue(callback);
    }

    public void requestSettingCrowedPayPoint(Callback<ResponseObject<MemberPoint>> callback, int crowdPayRef) {
        enterpriseApi.requestSettingCrowedPayPoint(crowdPayRef).enqueue(callback);
    }

    public void getCrowedMemberList(Callback<ResponseObject<ArrayList<CrowdMember>>> callback, int type, String keyword) {
        enterpriseApi.getCrowedMemberList(type, keyword).enqueue(callback);
    }

    public void requestSelectCrowedMember(Callback<ResponseObject<ArrayList<CrowdPay>>> callback, int payShopRef, String oids) {
        enterpriseApi.requestSelectCrowedMember(payShopRef, oids).enqueue(callback);
    }

    public void requestSelectPoint(Callback<ResponseObject<ArrayList<CrowdPay>>> callback, AdaptPoint adaptPoint) {
        enterpriseApi.requestSelectPoint(adaptPoint).enqueue(callback);
    }

    public void requestCrowdApproval(Callback<ResponseObject<ArrayList<CrowdPay>>> callback, int payShopRef) {
        enterpriseApi.requestCrowdApproval(payShopRef).enqueue(callback);
    }

    public void readPayment(Callback<ResponseObject<PayConfirm>> callback, int payShopRef) {
        paymentApi.readPayment(payShopRef).enqueue(callback);
    }

    public void changeFavoriteCrowedMember(Callback<ResponseObject<Long>> callback, int memberOid) {
        enterpriseApi.changeCrowdPayFavorite(memberOid).enqueue(callback);
    }

    public void retryCrowdApproval(Callback<ResponseObject<ArrayList<CrowdPay>>> callback, int crowdPayRef) {
        enterpriseApi.retryCrowdApproval(crowdPayRef).enqueue(callback);
    }

    public void deleteCrowdApproval(Callback<ResponseObject<ArrayList<CrowdPay>>> callback, int crowdPayRef) {
        enterpriseApi.deleteCrowdApproval(crowdPayRef).enqueue(callback);
    }

    /* 로직 */
    private int getTotalPoints() {
        int currentPoint = 0;
        for (View view : usedPoints.keySet())
            currentPoint += usedPoints.get(view);
        return currentPoint;
    }

    public void setMergeCoupons(ArrayList<Coupon> mergeCoupons) {
        this.mergeCoupons = mergeCoupons;
    }

    public void setBasketCoupons(ArrayList<Coupon> basketCoupons) {
        this.basketCoupons = basketCoupons;
    }

    public ArrayList<Coupon> getMergeCoupons() {
        ArrayList<Coupon> arrayList = new ArrayList<>();
        for (Coupon coupon :mergeCoupons) {
            if (useCoupon.indexOfValue(coupon.oid) == -1)
                arrayList.add(coupon);
        }
        return arrayList;
    }

    public ArrayList<Coupon> getBasketCoupons() {
        ArrayList<Coupon> arrayList = new ArrayList<>();
        for (Coupon coupon :basketCoupons) {
            if (useCoupon.indexOfValue(coupon.oid) == -1)
                arrayList.add(coupon);
        }
        return arrayList;
    }

    public void changeCoupon(int paymentOid, int couponOid) {
        int index = useCoupon.indexOfKey(paymentOid);
        if (couponOid != 0) {
            /* 페이먼트 Oid로 된 쿠폰 입력 */
            if (index == -1) {
                /* 입력된 쿠폰이 없을 때 */
                useCoupon.append(paymentOid, couponOid);
            } else {
                /* 입력된 쿠폰이 있을 때 */
                useCoupon.put(paymentOid, couponOid);
            }
        } else {
            /* 페이먼트 Oid로 된 쿠폰 사용내역 삭제 */
            if (index != -1) {
                useCoupon.removeAt(index);
            }
        }
    }

    public void checkPointState(int point, int availablePoints) {
        try {
            if (point >= 3000 || point == 0) {
                /* 포인트를 3천 포인트 이상 사용 */
                if (getTotalPoints() + point > availablePoints) {
                    /* 현재 입력한 포인트와 이전에 입력한 포인트의 합이 사용가능한 포인트를 넘었을 때 */
                    setPointState(EXCEEDED);
                } else {
                    setPointState(AVAILABLE);
                }
                /* 사용가능한 포인트이내 일 때 */
            } else {
                /* 포인트가 3천 포인트 미만이면서 0이 아닐때 */
                setPointState(NOT_APPLICABLE);
            }
        } catch (NumberFormatException exception) {
            setPointState(NUMBER_FORMAT_EXCEPTION);
        }
    }

    public int checkPointMessage(EditText editText) {
        switch (getPointState()) {
            case AVAILABLE:
                return 0;
            case EXCEEDED:
            case NOT_APPLICABLE:
            case NUMBER_FORMAT_EXCEPTION:
                editText.getText().clear();
                if (getPointState() == NOT_APPLICABLE) {
                    return R.string.available_minimum_points;
                } else if (getPointState() == NUMBER_FORMAT_EXCEPTION) {
                    return R.string.enter_only_num;
                }
                return 0;
            default:
                return 0;
        }
    }
}
