package kr.co.mergepoint.mergeclient.scene.data.shoplist;

/**
 * Created by 1017sjg on 2017. 9. 7..
 */

public class SelectItem extends SingleItem {

    private boolean isSelected;

    public SelectItem(boolean isTheme, String name, String code) {
        super(isTheme, name, code);
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
