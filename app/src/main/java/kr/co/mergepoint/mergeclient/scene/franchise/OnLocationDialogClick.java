package kr.co.mergepoint.mergeclient.scene.franchise;

/**
 * User: Marty
 * Date: 2018-10-04
 * Time: 오후 5:30
 * Description:
 */
public interface OnLocationDialogClick {
    public void onClick(int position);
}
