package kr.co.mergepoint.mergeclient.scene.usagehistory.model;

import java.util.ArrayList;
import java.util.Map;

import kr.co.mergepoint.mergeclient.api.HistoryApi;
import kr.co.mergepoint.mergeclient.api.PaymentApi;
import kr.co.mergepoint.mergeclient.application.common.BaseModel;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.scene.data.history.UseMenu;
import kr.co.mergepoint.mergeclient.scene.data.history.UseMenuList;
import kr.co.mergepoint.mergeclient.scene.data.main.UsageFilter;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Callback;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.ORDER;

/**
 * Created by 1017sjg on 2017. 9. 16..
 */

public class UsageModel extends BaseModel implements UsageModelInterface {

    public ArrayList<UsagePayment> usagePayment;
    private HistoryApi historyApi;
    private PaymentApi paymentApi;

    public UsageModel() {
        historyApi = retrofit.create(HistoryApi.class);
        paymentApi = retrofit.create(PaymentApi.class);
    }

    @Override
    public void getHistory(Callback<ResponseObject<ArrayList<UsagePayment>>> callback, UsageFilter filter) {
        if (filter.getListType() == ORDER) {
            historyApi.requestOrderHistory(filter.getTerm()).enqueue(callback);
        } else {
            historyApi.requestShopHistory(filter.getTerm()).enqueue(callback);
        }
    }

    @Override
    public void getUsedHistory(Callback<ResponseObject<ArrayList<UsagePayment>>> callback, UsageFilter filter) {
        historyApi.requestUsedHistory(filter.getTerm()).enqueue(callback);
    }

    @Override
    public void payCancel(Callback<ResponseObject<Integer>> callback, int oid) {
        paymentApi.requestPayCancel(oid).enqueue(callback);
    }

    @Override
    public void pointRefund(Callback<ResponseObject<Integer>> callback, int oid) {
        paymentApi.requestPointRefund(oid).enqueue(callback);
    }

    @Override
    public void sendUsageMenu(Callback<ResponseObject<UsagePayment>> callback, ArrayList<UseMenu> useMenus) {
        historyApi.requestMenuUse(new UseMenuList(useMenus)).enqueue(callback);
    }

    @Override
    public void approveMenu(Callback<ResponseObject<Integer>> callback, Map<String, Object> info) {
        int oid = (int) info.get("oid");
        String pin = (String) info.get("pin");
        boolean checkPin = (boolean) info.get("checkPin");
        MDEBUG.debug("oid is  : " + oid);
        /*if (pin != null && !pin.isEmpty()) {
            historyApi.approveMenu(oid, pin).enqueue(callback);
        } else {
            historyApi.approveMenu(oid).enqueue(callback);
        }*/
        historyApi.approveMenu(oid,checkPin).enqueue(callback);

    }

    @Override
    public void rejectMenu(Callback<ResponseObject<Integer>> callback, int rejectOid) {
        if (rejectOid != NO_VALUE)
            historyApi.rejectMenu(rejectOid).enqueue(callback);
    }
}
