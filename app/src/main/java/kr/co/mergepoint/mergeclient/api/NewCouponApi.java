package kr.co.mergepoint.mergeclient.api;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponDetail;
import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponUseInfo;
import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponV1;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * User: Marty
 * Date: 2018-09-17
 * Time: 오후 2:07
 * Description:
 */;
public interface NewCouponApi {

    @GET("coupon/list/v1")
    Call<ResponseObject<ArrayList<CouponV1>>> getCouponList();

    @POST("coupon/gift/download")
    @FormUrlEncoded
    Call<ResponseObject<String>> applyCoupon(@Field("code") String code ,@Field("type") int type);

    @POST("coupon/issue/{oid}")
    Call<ResponseObject<CouponDetail>> applyBarcode(@Path("oid") int ref);

    @GET("coupon/detail/{oid}")
    Call<ResponseObject<CouponDetail>> getBarcodedetail(@Path("oid") int ref);

    @POST("coupon/delete/{couponCategory}/{oid}")
    Call<ResponseObject<ArrayList<CouponV1>>> deleteCoupon(@Path("couponCategory") int cate,@Path("oid") int ref);

    @GET("coupon/gift/useList/{couponRef}")
    Call<ResponseObject<CouponUseInfo>> getCouponUseList(@Path("couponRef") int ref);

    @POST("/coupon/deleteExpired")
    @FormUrlEncoded
    Call<ResponseObject<Integer>> deleteExpired(@Field("oid") int oid);

}
