package kr.co.mergepoint.mergeclient.scene.cardmanagement.model;

import kr.co.mergepoint.mergeclient.api.CardApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseModel;
import kr.co.mergepoint.mergeclient.scene.data.cardregiste.BillCardList;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public class CardManagementModel extends BaseModel {

    public void userCardList(Callback<BillCardList> callback) {
        CardApi cardApi = retrofit.create(CardApi.class);
        Call<BillCardList> call = cardApi.requestCardList();
        call.enqueue(callback);
    }

    public void deleteCard(Callback<BillCardList> callback, Object tag) {
        int oid = Integer.parseInt(tag.toString());
        CardApi cardApi = retrofit.create(CardApi.class);
        Call<BillCardList> call = cardApi.deleteCard(oid);
        call.enqueue(callback);
    }

    public void representationCard(Callback<BillCardList> callback, Object tag) {
        int oid = Integer.parseInt(tag.toString());
        CardApi cardApi = retrofit.create(CardApi.class);
        Call<BillCardList> call = cardApi.representationCard(oid);
        call.enqueue(callback);
    }
}
