package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.CrowdPayHistoryDetailMemberItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class CrowdHistoryDetailMemberAdapter extends BasicListAdapter<BasicListHolder<CrowdPayHistoryDetailMemberItemBinding, CrowdPay>, CrowdPay> {

    public CrowdHistoryDetailMemberAdapter(ArrayList<CrowdPay> arrayList) {
        super(arrayList);
    }

    @Override
    public BasicListHolder<CrowdPayHistoryDetailMemberItemBinding, CrowdPay> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.crowd_pay_history_detail_member_item, parent, false);
        CrowdPayHistoryDetailMemberItemBinding itemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<CrowdPayHistoryDetailMemberItemBinding, CrowdPay>(itemBinding) {
            @Override
            public void setDataBindingWithData(CrowdPay data) {
                getDataBinding().setCrowdPay(data);
                getDataBinding().line.setVisibility(getObservableArrayList().indexOf(data) == getObservableArrayList().size() - 1 ? View.GONE : View.VISIBLE);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<CrowdPayHistoryDetailMemberItemBinding, CrowdPay> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
