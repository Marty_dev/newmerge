package kr.co.mergepoint.mergeclient.scene.newcoupon.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.services.common.SafeToast;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.NewCouponApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.MergeActivity;
import kr.co.mergepoint.mergeclient.scene.coupon.CouponApply;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.newcoupon.CouponV1;
import kr.co.mergepoint.mergeclient.scene.newcoupon.CountListener;
import kr.co.mergepoint.mergeclient.scene.newcoupon.adapter.NewCouponAdapter;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class newMyCouponActivity extends MergeActivity implements View.OnClickListener ,CountListener{
    @BindView(R.id.coupon_addbtn)
    TextView couponAddbtn;

    BaseActivity mCon = this;

    @Override
    protected void onActivityClick(View view) {
        // Nothing
    }

    NewCouponApi API;
    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.back)
    ConstraintLayout back;
    @BindView(R.id.basic_toolbar)
    Toolbar basicToolbar;
    @BindView(R.id.coupon_ticketcnt)
    TextView couponTicketcnt;
    @BindView(R.id.coupon_status_rl)
    RelativeLayout couponStatusRl;
    @BindView(R.id.coupon_nonell)
    LinearLayout couponNonell;
    @BindView(R.id.coupon_list)
    RecyclerView couponList;
    @BindView(R.id.coupon_notice_rl)
    LinearLayout couponNoticeRl;

    ArrayList<CouponV1> ListItems;
    NewCouponAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_my_coupon);
        ButterKnife.bind(this);
        API = MergeApplication.getMergeAppComponent().getRetrofit().create(NewCouponApi.class);

        adapter = new NewCouponAdapter(R.layout.newcoupon_item, this,API);
        couponList.setLayoutManager(new LinearLayoutManager(this));
        couponList.setNestedScrollingEnabled(false);
        backArrow.setOnClickListener(this);
        couponAddbtn.setOnClickListener(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    void initData() {
        ListItems = new ArrayList<>();


        MergeApplication.getMergeApplication().showLoading(this);
        API.getCouponList().enqueue(new Callback<ResponseObject<ArrayList<CouponV1>>>() {
            @Override
            public void onResponse(Call<ResponseObject<ArrayList<CouponV1>>> call, Response<ResponseObject<ArrayList<CouponV1>>> response) {
                MergeApplication.getMergeApplication().hideLoading(mCon);

                if (!response.isSuccessful()){
                    Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.retry), Snackbar.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().isFailed()) {
                    Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.retry), Snackbar.LENGTH_SHORT).show();
                } else {
                    if (response.body().getObject().size() == 0) {
                        // TODO: 2018-09-17  쿠폰이 없어요 이미지
                        couponNonell.setVisibility(View.VISIBLE);
                        couponList.setVisibility(View.GONE);
                    } else {
                        int couponCount = 0;
                        couponNonell.setVisibility(View.GONE);
                        couponList.setVisibility(View.VISIBLE);
                        for (int i=0 ; i < response.body().getObject().size(); i++){
                            CouponV1 item = response.body().getObject().get(i);
                            MDEBUG.debug("item Get Coupon State" + item.getCouponState());
                            if (item.getCouponState() >= 4){
                                CouponV1 ref = new CouponV1();
                                ref.setCouponState(-1);
                                couponCount = -1;
                                response.body().getObject().add(i,ref);
                                break;
                            }
                        }
                        couponNonell.setVisibility(View.GONE);
                        couponList.setVisibility(View.VISIBLE);
                        adapter.arrayList.clear();
                        adapter.arrayList.addAll(response.body().getObject());
                        if (couponList.getAdapter() == null) {
                            couponList.setAdapter(adapter);
                        }
                        adapter.notifyDataSetChanged();

                        couponCount += adapter.arrayList.size();
                        couponTicketcnt.setText(getResources().getString(R.string.leftticket_count, couponCount));
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseObject<ArrayList<CouponV1>>> call, Throwable t) {
                new MergeDialog.Builder(mCon).setContent(R.string.retry).build().show();
                MergeApplication.getMergeApplication().hideLoading(mCon);


            }
        });
    }

    @Override
    public void refreshCount(int count) {
        couponTicketcnt.setText(getResources().getString(R.string.leftticket_count,count));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_arrow:
                finish();
                break;
            case R.id.coupon_addbtn:
                Intent inte = new Intent(this,CouponApply.class);
                startActivity(inte);
                break;

        }
    }

}
