package kr.co.mergepoint.mergeclient.scene.cardmanagement.presenter;

import android.support.annotation.NonNull;
import android.view.View;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasePresenter;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.model.CardManagementModel;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.presenter.callback.CardCallback;
import kr.co.mergepoint.mergeclient.scene.cardmanagement.view.CardManagementView;
import kr.co.mergepoint.mergeclient.scene.data.cardregiste.BillCardList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 1017sjg on 2017. 7. 26..
 */

public class CardManagementPresenter extends BasePresenter<CardManagementView, CardManagementModel, CardCallback> {

    public CardManagementPresenter(CardManagementView baseView, CardManagementModel baseModel, CardCallback callback) {
        super(baseView, baseModel, callback);
    }

    @Override
    protected void setBindingProperty() {
        baseView.binding.setTitle("카드관리");
        baseView.binding.setClick(this);
    }

    public void onCreate() {
        baseModel.userCardList(getCardListInfo());
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.card_representation:
                baseModel.representationCard(getCardListInfo(), v.getTag());
                break;
            case R.id.card_delete:
                baseModel.deleteCard(getCardListInfo(), v.getTag());
                break;
//            case R.id.card_img:
//                baseView.showCardRegiste();
//                break;
            case R.id.uplus_next:
                baseView.showUplusRegiste();
                break;
            case R.id.registe_birth_layout:
                baseView.registeBirthClearFocus();
                break;
            default:
                break;
        }
    }

    private Callback<BillCardList> getCardListInfo() {
        return new Callback<BillCardList>() {
            @Override
            public void onResponse(@NonNull Call<BillCardList> call, @NonNull Response<BillCardList> response) {
                BillCardList billCardList = response.body();
                if (response.isSuccessful() && billCardList != null) {
                    baseView.binding.setBillCard(billCardList);
                } else {
                    baseView.showAlert(R.string.retry);
                }
            }

            @Override
            public void onFailure(@NonNull Call<BillCardList> call, @NonNull Throwable t) {
                baseView.showAlert(R.string.retry);
            }
        };
    }

    // 카드 등록 결과
    public void easyPayCardRegistResult(boolean isSuccess, String error) {
        baseView.cardRegist(isSuccess, error);
    }
}
