package kr.co.mergepoint.mergeclient.api;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.history.ShopHistoryPayment;
import kr.co.mergepoint.mergeclient.scene.data.history.UseMenuList;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by 1017sjg on 2017. 7. 25..
 */

public interface HistoryApi {
    @POST("payment/payMenuList")
    @FormUrlEncoded
    Call<ResponseObject<ArrayList<UsagePayment>>> requestOrderHistory(@Field("period") int period);

    @POST("payment/payShopList")
    @FormUrlEncoded
    Call<ResponseObject<ArrayList<UsagePayment>>> requestShopHistory(@Field("period") int period);

    @POST("payment/usedList")
    @FormUrlEncoded
    Call<ResponseObject<ArrayList<UsagePayment>>> requestUsedHistory(@Field("period") int period);

    @GET("payment/paymentMenu/{oid}")
    Call<ResponseObject<UsagePayment>> requestOrderDetail(@Path("oid") int oid);

    @GET("payment/shopPaymentList/v1/{consummerRef}/{oid}")
    Call<ShopHistoryPayment> requestShopDetail(@Path("consummerRef") int consummerRef,@Path("oid") int oid);

    @POST("payment/useRequest/2")
    Call<ResponseObject<UsagePayment>> requestMenuUse(@Body UseMenuList menus);

    @POST("payment/approveRequest")
    @FormUrlEncoded
    Call<ResponseObject<Integer>> approveMenu(@Field("oid") int oid, @Field("shopPin") String pin);

    @POST("payment/approveRequest")
    @FormUrlEncoded
    Call<ResponseObject<Integer>> approveMenu(@Field("oid") int oid,@Field("checkPin") boolean checkPin);

    @POST("payment/rejectRequest")
    @FormUrlEncoded
    Call<ResponseObject<Integer>> rejectMenu(@Field("oid") int oid);

    @GET("/payment/order/refreshBarcode/{orderRef}")
    Call<ResponseObject<UsagePayment>> refreshBarcode(@Path("orderRef") int oid);
}
