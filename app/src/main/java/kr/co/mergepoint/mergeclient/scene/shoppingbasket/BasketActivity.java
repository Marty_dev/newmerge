package kr.co.mergepoint.mergeclient.scene.shoppingbasket;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CompoundButton;

import javax.inject.Inject;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.CustomCountView;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.dagger.BasketModule;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.dagger.DaggerBasketComponent;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.presenter.BasketPresenter;

/**
 * Created by 1017sjg on 2017. 6. 26..
 */

public class BasketActivity extends BaseActivity implements CustomCountView.OnChangeCountListener, CompoundButton.OnCheckedChangeListener {

    @Inject BasketPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerBasketComponent.builder().basketModule(new BasketModule(this)).build().inject(this);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onActivityClick(View view) {
        if (view.getId() == R.id.confirm) {
            onBackPressed();
        } else {
            presenter.onClick(view);
        }
    }

    @Override
    public void changeCount(CustomCountView view, boolean isPlus) {
        presenter.changeCount(view, isPlus);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
        presenter.onCheckedChanged(compoundButton, checked);
    }
}
