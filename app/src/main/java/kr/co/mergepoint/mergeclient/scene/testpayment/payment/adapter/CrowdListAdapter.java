package kr.co.mergepoint.mergeclient.scene.testpayment.payment.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.MergeBaseAdapter;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.Payment_v2_Activity;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.CrowdInfo;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.CrowdSimpleListdata;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.PaymentRequest;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopPayment;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * User: Marty
 * Date: 2018-07-18
 * Time: 오후 2:32
 * Description:
 */
public class CrowdListAdapter extends MergeBaseAdapter<CrowdSimpleListdata,CrowdViewholder>{
    public CrowdListAdapter() {
        super();

    }
    public CrowdListAdapter(int layout, Payment_v2_Activity mCon) {
        super(layout,mCon);
    }

    public CrowdListAdapter(int layout, Payment_v2_Activity mCon, ArrayList<CrowdSimpleListdata> list) {
        super(layout,mCon);
        arrayList = list;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.crowd_listitem, parent, false);


        CrowdViewholder vh = new CrowdViewholder(v);
        vh.crowdName = (TextView)v.findViewById(R.id.crowd_name);
        vh.crowdListDelete = (ImageView)v.findViewById(R.id.crowd_list_delete);
        vh.crowdListPrise = (TextView)v.findViewById(R.id.crowd_list_prise);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CrowdViewholder vh = (CrowdViewholder)holder;
        CrowdSimpleListdata data = arrayList.get(position);

        if(data == null)
        {
            MDEBUG.debug("NULL?>");
            return;

        }else if(vh.crowdName == null){
            MDEBUG.debug("TV NULL");
            return;
        }

        vh.crowdName.setText(data.memberName);
        vh.crowdListPrise.setText(String.format("%,d",data.allocatePoint) + "P");
        vh.crowdListDelete.setOnClickListener(view1->{
            MergeApplication.getMergeApplication().showLoading(mCon);

            ((Payment_v2_Activity)mCon).callbacks.apis.requestdeleteCrowdMemberv2(data.oid).enqueue(new Callback<ResponseObject<PaymentRequest>>() {
                @Override
                public void onResponse(Call<ResponseObject<PaymentRequest>> call, Response<ResponseObject<PaymentRequest>> response) {
                    if (response.body().isFailed()){
                        new MergeDialog.Builder(mCon).setContent(response.body().getMessage()).setCancelBtn(false).build().show();
                    }else{
                     //   ((Payment_v2_Activity)mCon).initcrowdList((ArrayList<ShopPayment>) response.body().getObject().shopPayment);
                        ((Payment_v2_Activity)mCon).initList(response.body().getObject(),((Payment_v2_Activity)mCon).isEnterMember);
                    }

                    MergeApplication.getMergeApplication().hideLoading(mCon);

                }

                @Override
                public void onFailure(Call<ResponseObject<PaymentRequest>> call, Throwable t) {
                    MergeApplication.getMergeApplication().hideLoading(mCon);

                }
            });
        });



    }
}
