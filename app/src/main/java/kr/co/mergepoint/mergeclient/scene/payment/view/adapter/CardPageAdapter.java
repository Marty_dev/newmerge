package kr.co.mergepoint.mergeclient.scene.payment.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.databinding.PaymentCardItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.cardregiste.CardInfo;

/**
 * Created by 1017sjg on 2017. 6. 19..
 */

public class CardPageAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<CardInfo> cardInfos;
    private ArrayList<Integer> cardArray;

    public CardPageAdapter(Context context, ArrayList<CardInfo> cardInfos) {
        this.context = context;
        this.cardInfos = cardInfos == null ? new ArrayList<CardInfo>() : cardInfos;
//        this.cardArray = new ArrayList<>(Arrays.asList(R.drawable.obj_card_no1, R.drawable.obj_card_no2, R.drawable.obj_card_no3));
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        PaymentCardItemBinding cardItemBinding = DataBindingUtil.inflate(LayoutInflater.from(container.getContext()), R.layout.payment_card_item, container, false);
        cardItemBinding.setCardInfo(cardInfos.size() == 0 ? null : cardInfos.get(position));
        if (cardInfos.size() > 0)
            cardItemBinding.setCardImg(ContextCompat.getDrawable(context, cardArray.get(position)));

        container.addView(cardItemBinding.getRoot());
        return cardItemBinding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return cardInfos.size() == 0 ? 1 : cardInfos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
