package kr.co.mergepoint.mergeclient.api;

import java.util.ArrayList;
import java.util.Map;

import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by jgson on 2018. 1. 10..
 */

public interface MapApi {
    @GET("shop/mapList")
    Call<ResponseObject<ArrayList<Shop>>> getMapIntoShopList(@QueryMap Map<String, Object> search);
}
