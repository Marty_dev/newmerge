package kr.co.mergepoint.mergeclient.application.common;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopOptionSelectMenu;
import kr.co.mergepoint.mergeclient.scene.payment.view.adapter.CouponSpinnerAdapter;
import kr.co.mergepoint.mergeclient.scene.shop.view.adapter.PreExperienceDecoration;
import kr.co.mergepoint.mergeclient.scene.shop.view.listener.OnRadioChange;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.ShopListAdapter;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.listener.OnFilterChange;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.http.Body;

import static kr.co.mergepoint.mergeclient.application.common.Properties.NO_VALUE;

/**
 * Created by 1017sjg on 2017. 6. 28..
 */

public class Binder {

//    // MVVM
//    @BindingAdapter("pager_vm")
//    public static void bindPagerViewModel(ViewPager viewPager, ViewPagerViewModel viewModel) {
//        viewModel.setupPagerView(viewPager);
//    }
//
//    @BindingAdapter("recycler_vm")
//    public static void bindRecyclerViewViewModel(RecyclerView recyclerView, RecyclerViewViewModel viewModel) {
//        viewModel.setupRecyclerView(recyclerView);
//    }

    @BindingAdapter({"onTouch"})
    public static void bindOnTouch(View view, View.OnTouchListener onTouchListener) {
        view.setOnTouchListener(onTouchListener);
    }

    @BindingAdapter({"tab_items", "tab_listener"})
    public static void bindTabItemsWithListener(TabLayout view, String[] tabs, TabLayout.OnTabSelectedListener onTabSelectedListener) {
        if (tabs.length != view.getTabCount()) {
            for (String tabName : tabs) {
                TabLayout.Tab tab = view.newTab();
                tab.setText(tabName);
                view.addTab(tab);
            }
        }
        view.addOnTabSelectedListener(onTabSelectedListener);
    }

    @BindingAdapter("question_tab_listener")
    public static void bindTabItemsImgWithListener(TabLayout view, TabLayout.OnTabSelectedListener onTabSelectedListener) {
        view.addOnTabSelectedListener(onTabSelectedListener);
        TypedArray typedArray = view.getResources().obtainTypedArray(R.array.questions_icon);
        for (int i=0; i<typedArray.length(); i++) {
            ImageButton imageButton = new ImageButton(view.getContext());
            imageButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            imageButton.setClickable(false);
            imageButton.setBackgroundColor(ContextCompat.getColor(view.getContext(), android.R.color.transparent));
            imageButton.setImageResource(typedArray.getResourceId(i, -1));

            view.addTab(view.newTab().setCustomView(imageButton));
        }
        typedArray.recycle();
    }

    @BindingAdapter("tab_listener")
    public static void bindTabListener(TabLayout view, TabLayout.OnTabSelectedListener onTabSelectedListener) {
        view.addOnTabSelectedListener(onTabSelectedListener);
    }

    /*리스트 어댑터*/
    @BindingAdapter("list_adapter")
    public static void bindListAdapter(RecyclerView view, BasicListAdapter listAdapter) {
        view.setAdapter(listAdapter);
    }

    @BindingAdapter("no_scroll_list_adapter")
    public static void bindDisabledScrollListAdapter(RecyclerView view, BasicListAdapter listAdapter) {
        view.setAdapter(listAdapter);
        ViewCompat.setNestedScrollingEnabled(view, false);
    }

    @BindingAdapter({"expand_adapter", "expand_child_touch"})
    public static void bindExpandListAdapter(RecyclerView view, ExpandableRecyclerViewAdapter expandableRecyclerViewAdapter, ExpandableItemListener.ExpandableTouchListener touchListener) {
        view.setAdapter(expandableRecyclerViewAdapter);
        ViewCompat.setNestedScrollingEnabled(view, false);  // NestedScroll 내부의 있는 RecyclerView의 스크롤을 부드럽게 하기 위한 설정
        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setRemoveDuration(80);
        defaultItemAnimator.setAddDuration(80);
        defaultItemAnimator.setChangeDuration(80);
        defaultItemAnimator.setMoveDuration(80);

        view.setItemAnimator(defaultItemAnimator);
        if (touchListener != null)
            view.addOnItemTouchListener(new ExpandableItemListener(view, touchListener));
    }

    @BindingAdapter("expand_adapter")
    public static void bindExpandListWithoutTouchAdapter(RecyclerView view, ExpandableRecyclerViewAdapter expandableRecyclerViewAdapter) {
        view.setAdapter(expandableRecyclerViewAdapter);
        ViewCompat.setNestedScrollingEnabled(view, false);  // NestedScroll 내부의 있는 RecyclerView의 스크롤을 부드럽게 하기 위한 설정
        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setRemoveDuration(80);
        defaultItemAnimator.setAddDuration(80);
        defaultItemAnimator.setChangeDuration(80);
        defaultItemAnimator.setMoveDuration(80);

        view.setItemAnimator(defaultItemAnimator);
    }

    @BindingAdapter("anim_expand_adapter")
    public static void bindExpandListWithoutTouchLongAdapter(RecyclerView view, ExpandableRecyclerViewAdapter expandableRecyclerViewAdapter) {
        view.setAdapter(expandableRecyclerViewAdapter);
        ViewCompat.setNestedScrollingEnabled(view, false);  // NestedScroll 내부의 있는 RecyclerView의 스크롤을 부드럽게 하기 위한 설정
        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setRemoveDuration(0);
        defaultItemAnimator.setAddDuration(0);
        defaultItemAnimator.setChangeDuration(0);
        defaultItemAnimator.setMoveDuration(0);

        view.setItemAnimator(defaultItemAnimator);
    }

    /*CustomRecyclerView - 리스트 내에 즐겨찾기 버튼이 추가된 커스텀 리스트*/
    @BindingAdapter({"list_adapter", "list_touch"})
    public static void bindListAdapter(CustomRecyclerView view, BasicListAdapter listAdapter, CustomRecyclerView.CustomRecyclerViewTouch customRecyclerViewTouch) {
        view.setInit(listAdapter);
        view.setRecyclerViewTouch(customRecyclerViewTouch);
    }

    /*기본 리사이클러 뷰*/
    @BindingAdapter({"list_adapter", "touch"})
    public static void bindListAdapter(RecyclerView view, BasicListAdapter listAdapter,  RecyclerItemListener.RecyclerTouchListener recyclerTouchListener) {
        if (listAdapter != null)
            view.setAdapter(listAdapter);
        if (recyclerTouchListener != null)
            view.addOnItemTouchListener(new RecyclerItemListener(view, recyclerTouchListener));
    }

    @BindingAdapter({"pre_experience_adapter", "pre_experience_touch"})
    public static void bindPreExperience(RecyclerView recyclerView, BasicListAdapter adapter, RecyclerItemListener.RecyclerTouchListener recyclerTouchListener) {
        int spacing = recyclerView.getResources().getDimensionPixelSize(R.dimen.pre_experience_spacing);
        recyclerView.setAdapter(adapter);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);  // NestedScroll 내부의 있는 RecyclerView의 스크롤을 부드럽게 하기 위한 설정
        recyclerView.addItemDecoration(new PreExperienceDecoration(spacing, spacing));
        recyclerView.addOnItemTouchListener(new RecyclerItemListener(recyclerView, recyclerTouchListener));
    }

    /*로드 리스트*/
    @BindingAdapter({"load_adapter", "load_listener"})
    public static void bindList(LoadRecyclerView view, ShopListAdapter listAdapter, LoadRecyclerView.LoadRecyclerViewListener listener) {
        view.setInit(listAdapter, listener);
    }

    @BindingAdapter({"radio_select_oid", "radio_listener"})
    public static void bindSelectList(CustomRadioButton radioButton, int selectOptionOid, OnRadioChange radioChange) {
        radioButton.selectOptionOid = selectOptionOid;
        radioButton.setOnRadioChange(radioChange);
    }

    @BindingAdapter("check_select_oid")
    public static void bindSelectList(CustomCheckButton checkButton, int selectOptionOid) {
        checkButton.selectOptionOid = selectOptionOid;
    }

    @BindingAdapter(value = {"radio_select_oid", "radio_listener"}, requireAll = false)
    public static void bindCountAddButton(CustomCountAddButton countButton, int selectOptionOid, OnRadioChange radioChange) {
        countButton.selectOptionOid = selectOptionOid;
        countButton.setOnRadioChange(radioChange);
    }

    @BindingAdapter(value = {"radio_select_oid", "radio_listener"}, requireAll = false)
    public static void bindCountSubButton(CustomCountSubButton countButton, int selectOptionOid, OnRadioChange radioChange) {
        countButton.selectOptionOid = selectOptionOid;
        countButton.setOnRadioChange(radioChange);
    }

    @BindingAdapter(value = {"count_listener", "select_oid", "max_count", "type"}, requireAll = false)
    public static void bindCount(CustomCountView optionCount, CustomCountView.OnChangeCountListener listener, int selectOid, int maxCount, String type) {
        if (type != null)
            optionCount.setTag(type);
        if (listener != null)
            optionCount.countListener = listener;
        optionCount.optionSelectOid = selectOid;
        optionCount.maxCount = maxCount;
    }

    @BindingAdapter("check_listener")
    public static void bindSelectList(CheckBox checkBox, CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        checkBox.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    @BindingAdapter({"favorite", "favorite_listener"})
    public static void bindFavorite(BookMark bookMark, boolean favorite, BookMark.OnCheckFavorite onCheckFavorite) {
        bookMark.setFavoriteStatus(favorite);
        bookMark.setCheckFavorite(onCheckFavorite);
    }

    /*SimpleDraweeView 이미지 넣기*/
    @BindingAdapter(value = {"img_src", "empty_img"}, requireAll = false)
    public static void bindImageUrl(SimpleDraweeView draweeView, String url, int emptyImg) {
        if (url != null) {
            Matcher matcher = Pattern.compile(".*/(.*)").matcher(url);

            if (matcher.find()) {
                String imgName = matcher.group(1);
                if (imgName.matches(".*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*")) {
                    // 이미지 이름이 한글일 때
                    String encodeImageName = null;

                    try {
                        encodeImageName = URLEncoder.encode(imgName, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    if (encodeImageName != null)
                        url = url.replace(imgName, encodeImageName.replace("+","%20"));
                }
            }

            MDEBUG.debug("Thumbnail : " + url);
            ImageRequestBuilder imageRequestBuilder = ImageRequestBuilder.newBuilderWithSource(Uri.parse(Properties.BASE_URL + url));
            DraweeController draweeController = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(imageRequestBuilder.build())
                    .setOldController(draweeView.getController())
                    .setAutoPlayAnimations(true)
                    .build();
            draweeView.setController(draweeController);
        } else {
            if (emptyImg != 0)
                draweeView.setImageURI(ImageRequestBuilder.newBuilderWithResourceId(emptyImg).build().getSourceUri());
        }
    }

    @BindingAdapter({"img_url"})
    public static void bindImageUrl(SimpleDraweeView draweeView, String url) {
        boolean isNotEmpty = url != null && !url.isEmpty();
        draweeView.setAlpha(isNotEmpty ? 1.0f : 0.0f);
        if (isNotEmpty) {
            DraweeController controller = Fresco.newDraweeControllerBuilder().setUri(Uri.parse(url)).build();
            draweeView.setController(controller);
        }
    }

    /*ConstraintLayout 에 태그 뷰 넣기*/
    @BindingAdapter(value = {"gravity", "color_black", "category", "theme"}, requireAll = false)
    public static void bindTag(TagLayout tagLayout, int gravity, boolean isBlack, ArrayList<String> category, ArrayList<String> theme) {
        tagLayout.setTagView(gravity, isBlack, category, theme);
    }

    @BindingAdapter("option_price_string")
    public static void bindOptionPriceString(final TextView textView, final ArrayList<ShopOptionSelectMenu> selectMenus) {
        final Handler handler = new Handler();
        new Thread(() -> handler.post(() -> {
            StringBuilder stringBuilder = new StringBuilder();
            if (selectMenus.size() > 0) {
                for (ShopOptionSelectMenu select : selectMenus) {
                    stringBuilder.append(String.format(Locale.getDefault(), "+%d원\n", select.salePrice * select.count))
                            .append(String.format(Locale.getDefault(), "(%s %d개)", select.name, select.count));

                    if (selectMenus.indexOf(select) < selectMenus.size() - 1)
                        stringBuilder.append("\n");
                }
                textView.setText(stringBuilder.toString());
            }
        })).start();
    }

    @BindingAdapter("pager_adapter")
    public static void bindPagerAdapter(ViewPager viewPager, PagerAdapter pagerAdapter) {
        viewPager.setAdapter(pagerAdapter);
    }

    @BindingAdapter("page_margin")
    public static void bindPagerAdapter(ViewPager viewPager, float margin) {
        viewPager.setPageMargin((int) margin);
    }

    @BindingAdapter({"pager_adapter", "pager_listener"})
    public static void bindPagerAdapter(ViewPager viewPager, PagerAdapter pagerAdapter, ViewPager.OnPageChangeListener onPageChangeListener) {
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(onPageChangeListener);
    }

    @BindingAdapter({"pager_adapter", "like_listener"})
    public static void bindPagerAdapter(CustomViewPager viewPager, PagerAdapter pagerAdapter, CustomViewPager.LikeListener listener) {
        viewPager.setAdapter(pagerAdapter);
        viewPager.setListener(listener);
    }

    @BindingAdapter({"pager_adapter", "like_listener", "pager_listener"})
    public static void bindPagerAdapter(CustomViewPager viewPager, PagerAdapter pagerAdapter, CustomViewPager.LikeListener listener, ViewPager.OnPageChangeListener onPageChangeListener) {
        bindPagerAdapter(viewPager, pagerAdapter, listener);
        viewPager.addOnPageChangeListener(onPageChangeListener);
    }

    @BindingAdapter(value = {"spinner_adapter", "spinner_listener", "selected_item", "spinner_enable"}, requireAll = false)
    public static void bindSpinner(Spinner spinner, SpinnerAdapter adapter, AdapterView.OnItemSelectedListener listener, int selectedItem, Boolean visible) {
        spinner.setAdapter(adapter);
        spinner.setSelection(selectedItem, false);
        spinner.setOnItemSelectedListener(listener);
        if (visible != null) {
            spinner.setEnabled(visible);
            spinner.setFocusable(visible);
        }
    }

    @BindingAdapter(value = {"coupon_adapter", "coupon_listener", "selected_item", "spinner_enable"}, requireAll = false)
    public static void bindCouponSpinner(Spinner spinner, CouponSpinnerAdapter adapter, AdapterView.OnItemSelectedListener listener, int selectedItem, Boolean visible) {
        spinner.setAdapter(adapter);
        spinner.setSelection(selectedItem, false);
        spinner.setOnItemSelectedListener(listener);
        if (visible != null) {
            spinner.setEnabled(visible);
            spinner.setFocusable(visible);
        }
    }

    @BindingAdapter("init_search_position")
    public static void bindSearchView(ConstraintLayout constraintLayout, float height) {
        constraintLayout.setTranslationY(-height);
    }

    @BindingAdapter("focus_change")
    public static void bindTextFocus(EditText editText, View.OnFocusChangeListener onFocusChangeListener) {
        if (onFocusChangeListener != null)
            editText.setOnFocusChangeListener(onFocusChangeListener);
    }

    @BindingAdapter("drawable_img")
    public static void bindFrescoDrawable(SimpleDraweeView draweeView, int drawable) {
        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(drawable).build();
        draweeView.setImageURI(imageRequest.getSourceUri());
    }

    @BindingAdapter("convert_date_news")
    public static void bindDateNews(final TextView view, final ArrayList<Integer> date) {
        if (date != null) {
            view.post(() -> {
                DateModel dateModel = new DateModel();
                view.setText(dateModel.getStringDate(dateModel.getDate(date)));
            });
        }
    }

    @BindingAdapter("convert_date")
    public static void bindDatePeriod(final TextView view, final ArrayList<Integer> date) {
        if (date != null) {
            view.post(() -> {
                DateModel dateModel = new DateModel();
                view.setText(dateModel.getStringDateWithSeconds(date));
            });
        }
    }

    @BindingAdapter({"convert_period_start", "convert_period_end"})
    public static void bindDatePeriod(final TextView view, final ArrayList<Integer> startDate, final ArrayList<Integer> endDate) {
        if (startDate != null && endDate != null) {
            view.post(() -> {
                DateModel dateModel = new DateModel();
                String startDateStr = dateModel.getStringCalendarDate(dateModel.getCalendarDate(startDate));
                String endDateStr = dateModel.getStringCalendarDate(dateModel.getCalendarDate(endDate));
                view.setText(String.format(Locale.getDefault(), "%s - %s", startDateStr, endDateStr));
            });
        }
    }

    @BindingAdapter({"target_date", "state"})
    public static void bindTextColorWithBold(final TextView view, final ArrayList<Integer> date, final int state) {
        if (date != null) {
            final DateModel dateModel = new DateModel();
            view.post(() -> {
                switch (state) {
                    case 1: /* 미사용 */
                        view.setBackgroundResource(R.drawable.rounded_rect_fill_green);
                        view.setText(String.format(Locale.getDefault(), "D-%02d", dateModel.getCalculateDay(date)));
                        break;
                    case 2: /* 일부 사용 */
                        view.setBackgroundResource(R.drawable.rounded_rect_fill_red);
                        view.setText(String.format(Locale.getDefault(), "D-%02d", dateModel.getCalculateDay(date)));
                        break;
                    case 3: /* 모두 사용 */
                        view.setBackgroundResource(R.drawable.rounded_rect_fill_gray_02);
                        view.setText("사용완료");
                        break;
                    case 4: /* 포인트 전환 */
                        view.setBackgroundResource(R.drawable.rounded_rect_fill_black_02);
                        view.setText("기간만료");
                        break;
                    case 5: /* 사용기간 만료 */
                        view.setBackgroundResource(R.drawable.rounded_rect_fill_yellow);
                        view.setText("포인트전환");
                        break;
                    default:
                        break;
                }
            });
        }
    }

    @BindingAdapter("grade_img")
    public static void bindGradeImage(ImageView imageView, int grade) { // grade 1-5
        if (grade > 0 ) {
            TypedArray grades = imageView.getResources().obtainTypedArray(R.array.grade_big_img);
            imageView.setImageResource(grades.getResourceId(grade - 1, NO_VALUE));
            grades.recycle();
        }
    }
    @BindingAdapter("grade_menu_img")
    public static void bindGradeMenuImage(ImageView imageView, int grade) { // grade 1-5
        if (grade > 0 ) {
            TypedArray grades = imageView.getResources().obtainTypedArray(R.array.grade_menu_img);
            imageView.setImageResource(grades.getResourceId(grade - 1, NO_VALUE));
            grades.recycle();
        }
    }

    @BindingAdapter("grade_price")
    public static void bindGradePriceText(TextView textView, int grade) { // grade 1-5
        if (((textView.getTag() == null ? true : (String)textView.getTag()).equals("maximum") && grade == 2)||grade == 1 )
            return;
        int[] grades = textView.getResources().getIntArray(R.array.grade_price);
        textView.setText(String.format(Locale.getDefault(), "%,2d", grades[grade - 1]));
    }

    @BindingAdapter("drawer_listenr")
    public static void bindGradePriceText(DrawerLayout drawerLayout, DrawerLayout.DrawerListener listener) {
        drawerLayout.addDrawerListener(listener);
    }

    @BindingAdapter("html_text")
    public static void bindHtmlText(TextView textView, String text) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(text));
        } else {
            textView.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY));
        }
    }

    @BindingAdapter({"text", "start_index", "end_index", "span_click"})
    public static void bindHtmlTextWithClick(TextView textView, String text, int start, int end, View.OnClickListener clickListener) {
        SpannableString spannableString = new SpannableString(text);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.onClick(view);
            }
        }, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(spannableString);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    // STATE 버튼
    @BindingAdapter(value = {"onClick", "normal", "select", "select_listener"}, requireAll = false)
    public static void stateButtonSelect(StateButton stateButton, View.OnClickListener onClickListener, Drawable normal, Drawable select, OnFilterChange onFilterChange) {
        stateButton.setDrawableClick(onClickListener, new ArrayList<>(Arrays.asList(normal, select)));
        if (onFilterChange != null)
            stateButton.setOnFilterChange(onFilterChange);
    }

    @BindingAdapter({"onClick", "normal", "up", "down", "select_listener"})
    public static void stateButtonSelect(StateButton stateButton, View.OnClickListener onClickListener, Drawable normal, Drawable up, Drawable down, OnFilterChange onFilterChange) {
        stateButton.setDrawableClick(onClickListener, new ArrayList<>(Arrays.asList(normal, up, down)));
        stateButton.setOnFilterChange(onFilterChange);
    }

    @BindingAdapter("switch_listener")
    public static void stateButtonSelect(SwitchCompat sw, CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        sw.setOnCheckedChangeListener(onCheckedChangeListener);
    }


    @BindingAdapter("onDistanceListener")
    public static void setOnSeekerListener(AppCompatSeekBar seekBar, SeekBar.OnSeekBarChangeListener seekBarChangeListener) {
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);
    }

    @BindingAdapter(value = {"editor_listener", "editor_watcher"}, requireAll = false)
    public static void setOnEditorListener(EditText editText, TextView.OnEditorActionListener editorActionListener, TextWatcher textWatcher) {
        editText.setOnEditorActionListener(editorActionListener);
        if (textWatcher != null)
            editText.addTextChangedListener(textWatcher);
    }

    @BindingAdapter({"auto_resizing_text", "parent_width"})
    public static void bindSearchView(TextView textView, String text, int parentWidth) {
        textView.setText(text);
        int textSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 16, Resources.getSystem().getDisplayMetrics());
        Rect bounds = new Rect();
        Paint textPaint = textView.getPaint();
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(textSize);
        textPaint.getTextBounds(text, 0, text.length(), bounds);

        if (bounds.width() > (parentWidth * 0.9)) {
            for (int i = textSize; i > 0; i--) {
                textPaint.setTextSize(i);
                textPaint.getTextBounds(text, 0, text.length(), bounds);
                if ((parentWidth * 0.9) > bounds.width()) {
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, i / Resources.getSystem().getDisplayMetrics().density);
                    return;
                }
            }
        }
    }

    @BindingAdapter("layout_width")
    public static void setLayoutWidth(SimpleDraweeView view, float width) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = (int) width;
        view.setLayoutParams(layoutParams);
    }

    // 마진 넣기
    @BindingAdapter("android:layout_marginBottom")
    public static void setBottomMargin(View view, float bottomMargin) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.rightMargin, Math.round(bottomMargin));
        view.setLayoutParams(layoutParams);
    }

    @BindingAdapter("android:layout_marginStart")
    public static void setStartMargin(View view, float startMargin) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.setMargins(Math.round(startMargin), layoutParams.topMargin, layoutParams.rightMargin, layoutParams.bottomMargin);
        view.setLayoutParams(layoutParams);
    }

    @BindingAdapter("android:layout_marginTop")
    public static void setTopMargin(View view, float topMargin) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.setMargins(layoutParams.leftMargin, Math.round(topMargin), layoutParams.rightMargin, layoutParams.bottomMargin);
        view.setLayoutParams(layoutParams);
    }

    @BindingAdapter("android:onTextChanged")
    public static void setTextChanged(EditText editText, TextWatcher textWatcher) {
        editText.addTextChangedListener(textWatcher);
    }

    @BindingAdapter("android:visibility")
    public static void setVisible(final View view, final boolean isVisible) {
        view.post(() -> view.setVisibility(isVisible ? View.VISIBLE : View.GONE));
    }

    @BindingAdapter("android:text")
    public static void setText(final TextView view, final String text) {
        view.post(() -> view.setText(text));
    }

    @BindingAdapter("android:text")
    public static void setText(final TextView view, final int text) {
        view.post(() -> view.setText(text));
    }
    @BindingAdapter("barcode_image")
    public static void setbarcode(final ImageView view, final String code){
        if (code == null)
            return;
        Bitmap barcode = MergeApplication.getMergeApplication().BarcodeGenerate(code);
        view.setImageBitmap(barcode);
    }
    @BindingAdapter("barcode_imagerotate")
    public static void setbarcoderotate(final ImageView view, final String code){
        if (code == null)
            return;
        Bitmap barcode = MergeApplication.getMergeApplication().BarcodeGenerateRotate(code);
        view.setImageBitmap(barcode);
    }
}
