package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.mvvm.RecyclerViewAdapter;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder.MainStreetVH;


/**
 * User: Marty
 * Date: 2018-11-14
 * Time: 오후 2:06
 * Description:
 */
public class MainStreetAdapter extends RecyclerView.Adapter<MainStreetVH>  {

    ArrayList<String> streetName;
    View.OnClickListener listener;
    Context mCon;


    public MainStreetAdapter(ArrayList<String> streetName, View.OnClickListener listener, Context mCon) {
        this.streetName = new ArrayList<>();
        this.streetName.addAll(streetName);
        this.listener = listener;
        this.mCon = mCon;
        int count = streetName.size();
        int leftcount = 3 - (count % 3);

        for (int i = 0 ; i < leftcount ; i++){
            this.streetName.add("COMMING SOON");
        }
    }

    @Override
    public int getItemCount() {
        return streetName.size();
    }

    @Override
    public MainStreetVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = ((LayoutInflater)mCon.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.main_street_listitem,parent,false);
        MainStreetVH vh = new MainStreetVH(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MainStreetVH holder, int position) {
        holder.tv.setText(streetName.get(position));
        holder.linearLayout.setTag(streetName.get(position));
        if (streetName.get(position).equals("COMMING SOON")){
            holder.linearLayout.setOnClickListener(null);
            holder.tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 9);
        }else {
            holder.linearLayout.setOnClickListener(listener);
            holder.tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        }
    }
}
