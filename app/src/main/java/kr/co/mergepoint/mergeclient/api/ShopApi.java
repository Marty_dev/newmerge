package kr.co.mergepoint.mergeclient.api;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Map;

import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.menu.ShopGroupCategory;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.PreExperience;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.GoogleSearchInfo;
import kr.co.mergepoint.mergeclient.scene.data.shop.review.MergeReview;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by 1017sjg on 2017. 6. 24..
 */

public interface ShopApi {
    @POST("shopDetail")
    @FormUrlEncoded
    Call<ShopDetail> getShopDetailInfo(@Field("oid") int shopOid);

    @POST("listMenu")
    @FormUrlEncoded
    Call<ShopGroupCategory> getShopMenuInfo(@Field("oid") int shopOid);

    @POST("menuDetail")
    @FormUrlEncoded
    Call<ShopMenuDetail> getMenuDetailInfo(@Field("oid") int menuOid);

    @POST("shop/favoriteList")
    @FormUrlEncoded
    Call<ResponseObject<ArrayList<Shop>>> getFavoriteList(@FieldMap Map<String, Object> position);

    @GET("checkFavorite/{shop_oid}")
    Call<ResponseObject<Integer>> checkFavorite(@Path("shop_oid") int oid);

    @GET("checkLike/{shop_oid}/{count}")
    Call<ResponseObject<Integer>> checkLike(@Path("shop_oid") int oid, @Path("count") int count);

    @GET("shop/visit/{shop_oid}")
    Call<ResponseObject<JsonObject>> getShopVisitInfo(@Path("shop_oid") int oid);

    @GET("shop/preEx/{shop_oid}")
    Call<ResponseObject<ArrayList<PreExperience>>> getPreExperienceInfo(@Path("shop_oid") int oid);

    @GET("/customsearch/v1")
    Call<GoogleSearchInfo> getGoogleSearchReview(@QueryMap Map<String, String> params);

    @GET("tableMenu/list/{oid}")
    Call<ResponseObject<ArrayList<ShopMenuDetail>>> getShopDailyMenuList(@Path("oid") int shopOid, @Query("date") String date);

    @GET("getReview/{shop_oid}")
    Call<ResponseObject<MergeReview>> getShopReview(@Path("shop_oid") int oid, @Query("pageNum") int pageNum);
}
