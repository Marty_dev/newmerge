package kr.co.mergepoint.mergeclient.scene.main.view.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.math.BigDecimal;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.application.common.LoadRecyclerView;
import kr.co.mergepoint.mergeclient.application.common.location.UserLocation;
import kr.co.mergepoint.mergeclient.databinding.ThemeShopBinding;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Filter;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.ShopInfo;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.ShopListAdapter;
import retrofit2.Call;

import static kr.co.mergepoint.mergeclient.application.common.Properties.ADD;
import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.THEME_REQUEST;

/**
 * Created by 1017sjg on 2017. 9. 18..
 */

public class ThemeFragment extends BaseFragment implements LoadRecyclerView.LoadRecyclerViewListener {

    private Filter filter;
    private UserLocation userLocation;
    private ThemeShopBinding themeShopBinding;

    public UserLocation getUserLocation() {
        return userLocation;
    }

    public static ThemeFragment newInstance(UserLocation userLocation, String themeCode) {
        Bundle args = new Bundle();
        ThemeFragment fragment = new ThemeFragment();
        fragment.userLocation = userLocation;
        fragment.filter = new Filter(LIST);
        fragment.filter.setTheme(themeCode);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        themeShopBinding = DataBindingUtil.inflate(inflater, R.layout.theme_shop, container, false);
        themeShopBinding.setOnClick(this);
        themeShopBinding.setListListener(this);
        themeShopBinding.setListAdapter(new ShopListAdapter());
        int themeIndex = Integer.parseInt(new BigDecimal(filter.getTheme()).toString());
        themeShopBinding.setTitle(rootActivity.getResources().getStringArray(R.array.theme)[themeIndex - 1]);
        return themeShopBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        userLocation.startLocationUpdates();
    }

    public LoadRecyclerView getShopList() {
        return themeShopBinding.searchList;
    }

    public Filter getFilter() {
        return filter;
    }

    @Override
    public void onTouchItem(RecyclerView recyclerView, View view, int position) {
        ShopListAdapter adapter = themeShopBinding.searchList.getShopListAdapter();
        openActivityForResultWithParcelable(ShopActivity.class, SHOP_INFO, adapter.getObservableArrayList().get(position), THEME_REQUEST);
    }

    public void openActivityForResultWithParcelable(Class<?> dest, String name, Parcelable parcelable, int requestCode) {
        Intent intent = new Intent(rootActivity, dest);
        intent.putExtra(name, parcelable);
        rootActivity.startActivityForResult(intent, requestCode);
    }

    @Override
    public void onLoadItem(LoadRecyclerView loadRecyclerView, int currentPage) {
        MergeApplication.getMergeApplication().showLoading(rootActivity);
        themeShopBinding.searchList.loadShopList(ADD, filter.getLoadFilter(currentPage));
    }

    @Override
    public void onInitSuccess(ShopInfo shopInfo) {
        ShopListAdapter adapter = themeShopBinding.searchList.getShopListAdapter();
        adapter.updateShopList(shopInfo.getShopList());

        if (themeShopBinding.searchList.getListLoadScrollListener() != null)
            themeShopBinding.searchList.getListLoadScrollListener().resetScroll();

        themeShopBinding.searchList.setTotalPage(shopInfo.getTotalPage());
        themeShopBinding.notSearch.setVisibility(shopInfo.getListSize() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onAddSuccess(final ShopInfo shopInfo) {
        final ShopListAdapter adapter = themeShopBinding.searchList.getShopListAdapter();
        rootActivity.runOnUiThread(() -> {
            adapter.addShopList(shopInfo.getShopList());
            themeShopBinding.searchList.setTotalPage(shopInfo.getTotalPage());
        });
    }

    @Override
    public void onResponseFailure(@NonNull Call<ShopInfo> call) {
        showAlert(R.string.retry);
        themeShopBinding.notSearch.setVisibility(View.VISIBLE);
    }

    public void setThemeFavorite(int oid, boolean favorite) {
        if (themeShopBinding != null)
            themeShopBinding.getListAdapter().singleShopFavoriteUpdate(oid, favorite);
    }
}
