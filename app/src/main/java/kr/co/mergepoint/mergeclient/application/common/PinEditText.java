package kr.co.mergepoint.mergeclient.application.common;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import kr.co.mergepoint.mergeclient.R;

/**
 * Created by 1017sjg on 2017. 10. 24..
 */

public class PinEditText extends AppCompatEditText {

    private TextWatcher watcher;

    public PinEditText(Context context) {
        super(context);
        init();
    }

    public PinEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PinEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        setBackground(ContextCompat.getDrawable(getContext(), R.drawable.obj_pin_box_gray));
        watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                setBackground(ContextCompat.getDrawable(getContext(), s.length() == 0 ? R.drawable.obj_pin_box_gray : R.drawable.obj_pin_box_red));

                PinEditText pinEditText = getRootView().findViewById(s.length() > 0 ? getNextFocusDownId() : getNextFocusUpId());
                if (pinEditText != null && s.length() != 2) {
                    clearFocus();
                }

                removeTextChangedListener(watcher);
                if (s.length() == 1) {
                    setText(getIconText(s));
                } else if (s.length() == 0) {
                    getText().clear();
                } else  if (s.length() == 2) {
                    String temp = getText().toString();
                    getText().clear();
                    setText(getIconText(String.valueOf(temp.charAt(0)), 0, 1));

                    if (pinEditText != null) {
                        pinEditText.setText(getIconText(String.valueOf(temp.charAt(1)), 0, 1));
                    }
                }
                addTextChangedListener(watcher);
                if (pinEditText != null) {
                    pinEditText.requestFocus();
                    pinEditText.setCursorVisible(true);
                    pinEditText.setSelection(pinEditText.getText().length());
                }
                setSelection(pinEditText != null ? s.length() : (getText().length() == 0 ? 0 : 1));
            }

            @Override
            public void afterTextChanged(Editable s) {}
        };

        addTextChangedListener(watcher);
        setOnKeyListener((view, keyCode, keyEvent) -> {
            if (getText().length() == 0 && keyCode == KeyEvent.KEYCODE_DEL) {
                PinEditText pinEditText = getRootView().findViewById(getNextFocusUpId());
                if (pinEditText != null) {
                    clearFocus();
                    pinEditText.requestFocus();
                    pinEditText.setCursorVisible(true);
                }
            }
            return false;
        });
        setOnEditorActionListener((textView, keyCode, keyEvent) -> {
            if (keyCode == EditorInfo.IME_ACTION_DONE) {
                clearFocus();
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.hideSoftInputFromWindow(getWindowToken(), 0);
                return true;
            }
            return false;
        });
    }

    private Spannable getIconText(CharSequence text) {
        return getIconText(text,text.length() - 1, text.length());
    }

    private Spannable getIconText(CharSequence text, int start, int end) {
        SpannableString spannable = new SpannableString(text);
        ImageSpan imageSpan = new ImageSpan(getContext(), R.drawable.obj_pin_star);
        spannable.setSpan(imageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }
}
