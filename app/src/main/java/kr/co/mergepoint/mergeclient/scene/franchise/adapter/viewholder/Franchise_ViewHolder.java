package kr.co.mergepoint.mergeclient.scene.franchise.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import kr.co.mergepoint.mergeclient.R;

/**
 * User: Marty
 * Date: 2018-10-02
 * Time: 오전 10:17
 * Description:
 */
public class Franchise_ViewHolder extends RecyclerView.ViewHolder {
    public ImageView logoView;
    public TextView nameView;
    public View contentview;

    public Franchise_ViewHolder(View itemView) {
        super(itemView);
        logoView = (ImageView)itemView.findViewById(R.id.franchise_logo);
        nameView = (TextView)itemView.findViewById(R.id.franchise_name);
        contentview = itemView.findViewById(R.id.fran_listitem);
    }

}
