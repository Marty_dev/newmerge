package kr.co.mergepoint.mergeclient.scene.testpayment.payment.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * User: Marty
 * Date: 2018-07-18
 * Time: 오후 6:29
 * Description:
 */
public class PaymentRequest {
    @Expose
    @SerializedName("menuNum")
    public int menuNum;
    @Expose
    @SerializedName("plannedPoints")
    public int plannedPoints;
    @Expose
    @SerializedName("payPrice")
    public int payPrice;
    @Expose
    @SerializedName("couponPrice")
    public int couponPrice;
    @Expose
    @SerializedName("companyPoints")
    public int companyPoints;
    @Expose
    @SerializedName("pointPrice")
    public int pointPrice;
    @Expose
    @SerializedName("orderPrice")
    public int orderPrice;
    @Expose
    @SerializedName("memberRef")
    public int memberRef;
    @Expose
    @SerializedName("oid")
    public int oid;

    @SerializedName("shopPayment")
    public List<ShopPayment> shopPayment;


}
