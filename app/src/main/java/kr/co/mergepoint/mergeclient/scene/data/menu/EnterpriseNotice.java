package kr.co.mergepoint.mergeclient.scene.data.menu;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 6. 23..
 */

public class EnterpriseNotice {

    @SerializedName("oid")
    public int oid;

    @SerializedName("type")
    public int type;

    @SerializedName("companyRef")
    public int companyRef;

    @SerializedName("departRef")
    public int departRef;

    @SerializedName("title")
    public String title;

    @SerializedName("bodyText")
    public String bodyText;

    @SerializedName("staDateTime")
    public ArrayList<Integer> staDateTime;
}
