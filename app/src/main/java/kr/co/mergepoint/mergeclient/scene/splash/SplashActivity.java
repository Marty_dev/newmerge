package kr.co.mergepoint.mergeclient.scene.splash;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.Objects;
import java.util.Set;

import kr.co.mergepoint.mergeclient.BuildConfig;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.UserApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.scene.WelcomeMainActivity;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import kr.co.mergepoint.mergeclient.util.Merge_Constant;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INTENT_FORGOT_PW;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SPLASH_DISPLAY_DEFAULT;

/**
 * Created by jgson on 2017. 5. 30..
 */

public class SplashActivity extends BaseActivity implements Callback<InitialInfo> {

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        DataBindingUtil.setContentView(this, R.layout.splash);

        new GoogleADIDTask().execute();

    }

    public void test(){
        Intent inte = getIntent();
        if (inte.ACTION_VIEW.equals(inte.getAction())){
            MDEBUG.debug("is Action");
            Uri uri = inte.getData();
            String oid = uri.getQueryParameter("oid");
            MDEBUG.debug("oid is"  + oid);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(this::init, SPLASH_DISPLAY_DEFAULT);
    }

    /*로그인 정보 받아오기*/
    private void init() {

        UserApi user = MergeApplication.getMergeAppComponent().getRetrofit().create(UserApi.class);
        Call<InitialInfo> call = user.getInitialInfo(FirebaseInstanceId.getInstance().getToken());
        call.enqueue(this);
    }

    /*초기 로그인 화면*/
    private void openPage(Class tClass, int flag) {
        Intent intent = new Intent(SplashActivity.this, tClass);
        Intent splashIntent = getIntent();
        String ActionMode = "";
        if (splashIntent != null && splashIntent.getData() != null) {
            Uri uri = splashIntent.getData();
            if (uri.getQueryParameter("type").equals(INTENT_FORGOT_PW)){
                intent = new Intent(SplashActivity.this,MainActivity.class);
            }
            for (String queryName: uri.getQueryParameterNames()) {
                intent.putExtra(queryName, uri.getQueryParameter(queryName));
                MDEBUG.debug("query :" + uri.getQueryParameter(queryName));
                MDEBUG.debug("query name" +queryName);
            }
            ActionMode = uri.getQueryParameter("type");
        }
        if (!ActionMode.equals(INTENT_FORGOT_PW) && splashIntent != null && splashIntent.getExtras() != null) {
            Set<String> keySet = splashIntent.getExtras().keySet();
            for (String key: keySet) {
                MDEBUG.debug("extras : " + key);
                intent.putExtra(key, splashIntent.getStringExtra(key));
            }
        }
        intent.setFlags(flag);
        startActivity(intent);
    }

    private boolean isUpdateApp(String initVersionStr) {
        String[] initVersion = initVersionStr.split("\\.");
        String[] appVersion = BuildConfig.VERSION_NAME.split("\\.");

        for (int i = 0; i < initVersion.length; i++) {
            if (!Objects.equals(initVersion[i], appVersion[i]))
                return Integer.parseInt(initVersion[i]) > Integer.parseInt(appVersion[i]);
        }
        return false;
    }

    @Override
    public void onResponse(@NonNull Call<InitialInfo> call, @NonNull Response<InitialInfo> response) {
        InitialInfo initialInfo = response.body();
        if (response.isSuccessful() && initialInfo != null) {
            if (isUpdateApp(initialInfo.getVersion())) {
                showUpdateAlert(() -> {
                    Intent mergeMarket = new Intent(Intent.ACTION_VIEW);
                    mergeMarket.setData(Uri.parse("market://details?id=kr.co.mergepoint.mergeclient"));
                    startActivity(mergeMarket);
                });
            } else {
                MergeApplication.setInitialInfo(initialInfo);
                if (response.isSuccessful() && initialInfo.isSignin()) {
                    openPage(MainActivity.class, FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TASK);
                } else {
                    openPage(WelcomeMainActivity.class, FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TASK);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            }
        } else {
            Snackbar.make(findViewById(android.R.id.content), R.string.restart_app, Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    @Override
    public void onFailure(@NonNull Call<InitialInfo> call, @NonNull Throwable t) {
        Snackbar.make(findViewById(android.R.id.content), R.string.restart_app, Snackbar.LENGTH_INDEFINITE).show();
    }

    @Override
    protected void onActivityClick(View view) {}

    public void showUpdateAlert(MergeDialog.OnDialogClick onDialogClick) {
        new MergeDialog.Builder(this)
                .setContent(R.string.update_text)
                .setConfirmString(R.string.update)
                .setCancelBtn(false).setConfirmClick(onDialogClick).build().show();
    }

    class GoogleADIDTask extends AsyncTask<Void,Void,String> {
        String ADID = "";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... strings) {
            try {
                ADID = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext()).getId();
                Log.d("<de>","ADID : " + ADID);

                MergeApplication.getMergeApplication().ADID_KEY = ADID;
            }catch (Exception e){
                Log.d("<de>","error"  + e.toString());
            }
            /*
            requestWebServer("2422", ADID, new okhttp3.Callback() {
                @Override
                public void onFailure(okhttp3.Call call, IOException e) {

                }

                @Override
                public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                    Log.d("<de>","res"+response.message() + "/" +response.body().string());
                }
            });
            */
            return ADID;
        }

        private OkHttpClient client = new OkHttpClient();

        public void requestWebServer(String parameter, String parameter2, okhttp3.Callback callback) {
            HttpUrl.Builder urlBuilder = HttpUrl.parse("http://api.i-screen.kr/api/ads_complete_run").newBuilder();
            urlBuilder.addQueryParameter("ai", parameter);
            urlBuilder.addQueryParameter("av", parameter2);
            String url = urlBuilder.build().toString();

            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json; charset=utf-8")
                    .url(url)
                    .build();
            client.newCall(request).enqueue(callback);
        }

    }

}
