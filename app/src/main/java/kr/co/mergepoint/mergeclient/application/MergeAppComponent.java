package kr.co.mergepoint.mergeclient.application;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import kr.co.mergepoint.mergeclient.application.common.Utility;
import kr.co.mergepoint.mergeclient.application.module.MergeModule;
import kr.co.mergepoint.mergeclient.application.module.NetworkModule;
import kr.co.mergepoint.mergeclient.application.module.UtilityModule;
import retrofit2.Retrofit;

/**
 * Created by jgson on 2017. 6. 2..
 */

@Singleton
@Component(modules = {MergeModule.class, NetworkModule.class, UtilityModule.class})
public interface MergeAppComponent {
    Context getContext();
    Retrofit getRetrofit();
    Utility getUtility();
}
