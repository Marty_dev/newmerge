package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.view.View;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.WideBannerAdapter;
import kr.co.mergepoint.mergeclient.scene.data.main.MergesPick;

/**
 * Created by 1017sjg on 2017. 6. 19..
 */

public class MergesPickAdapter extends WideBannerAdapter {

    private ArrayList<MergesPick> mergesPicks;

    public ArrayList<MergesPick> getMergesPicks() {
        return mergesPicks;
    }

    public MergesPickAdapter(ArrayList<MergesPick> mergesPicks, int emptyImg, View.OnClickListener onClickListener) {
        super(R.layout.banner_merges_item, new ArrayList<>(), emptyImg, onClickListener);
        for (MergesPick mergesPick: mergesPicks)
            imgUrl.add(mergesPick.mainImage);
        this.mergesPicks = mergesPicks;
    }
}
