package kr.co.mergepoint.mergeclient.scene.payment.dagger;

import dagger.Component;
import kr.co.mergepoint.mergeclient.scene.payment.PaymentActivity;

/**
 * Created by jgson on 2017. 6. 5..
 */

@PaymentScope
@Component(modules = PaymentModule.class)
public interface PaymentComponent {
    void inject(PaymentActivity activity);
}
