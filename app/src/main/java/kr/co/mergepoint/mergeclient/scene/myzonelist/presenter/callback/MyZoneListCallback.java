package kr.co.mergepoint.mergeclient.scene.myzonelist.presenter.callback;

import kr.co.mergepoint.mergeclient.application.common.BaseCallback;
import kr.co.mergepoint.mergeclient.scene.myzonelist.model.MyZoneListModel;
import kr.co.mergepoint.mergeclient.scene.myzonelist.view.MyZoneListView;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class MyZoneListCallback extends BaseCallback<MyZoneListView, MyZoneListModel> implements MyZoneListCallbackInterface {

    public MyZoneListCallback(MyZoneListView baseView, MyZoneListModel baseModel) {
        super(baseView, baseModel);
    }
}
