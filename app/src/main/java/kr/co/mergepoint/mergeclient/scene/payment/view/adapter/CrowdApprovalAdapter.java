package kr.co.mergepoint.mergeclient.scene.payment.view.adapter;

import android.databinding.DataBindingUtil;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.CrowdApprovalItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdPay;

/**
 * Created by 1017sjg on 2017. 7. 6..
 */

public class CrowdApprovalAdapter extends BasicListAdapter<BasicListHolder<CrowdApprovalItemBinding, CrowdPay>, CrowdPay> {

    private View.OnClickListener onClickListener;

    public CrowdApprovalAdapter(ArrayList<CrowdPay> arrayList, View.OnClickListener onClickListener) {
        super(arrayList);
        this.onClickListener = onClickListener;
    }

    @Override
    public BasicListHolder<CrowdApprovalItemBinding, CrowdPay> setCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.crowd_approval_item, parent, false);
        CrowdApprovalItemBinding listBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<CrowdApprovalItemBinding, CrowdPay>(listBinding) {
            @Override
            public void setDataBindingWithData(CrowdPay data) {
                getDataBinding().setCrowdPay(data);
                getDataBinding().setOnClick(onClickListener);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<CrowdApprovalItemBinding, CrowdPay> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }

    public void updateState(SparseIntArray intArray) {
        for (CrowdPay crowdPay: getObservableArrayList()) {
            crowdPay.approveState = intArray.get(crowdPay.oid);
        }
        notifyDataSetChanged();
    }
}
