package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.databinding.QuestionsChildBinding;
import kr.co.mergepoint.mergeclient.databinding.QuestionsGroupBinding;
import kr.co.mergepoint.mergeclient.scene.data.main.Question;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder.ChildQuestionHolder;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder.GroupQuestionHolder;
import kr.co.mergepoint.mergeclient.scene.main.view.adapter.holder.Questions;

/**
 * Created by 1017sjg on 2017. 8. 1..
 */

public class QuestionsAdapter extends ExpandableRecyclerViewAdapter<GroupQuestionHolder, ChildQuestionHolder> {

    public QuestionsAdapter(List<Questions> groups) {
        super(groups);
    }

    @Override
    public GroupQuestionHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.questions_group, parent, false);
        QuestionsGroupBinding groupBinding = DataBindingUtil.bind(view);
        return new GroupQuestionHolder(groupBinding);
    }

    @Override
    public ChildQuestionHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.questions_child, parent, false);
        QuestionsChildBinding childBinding = DataBindingUtil.bind(view);
        return new ChildQuestionHolder(childBinding);
    }

    @Override
    public void onBindChildViewHolder(ChildQuestionHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        Question question = ((Questions) group).getItems().get(childIndex);
        holder.onBind(question);
    }

    @Override
    public void onBindGroupViewHolder(GroupQuestionHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setQuestionsBinding((Questions) group);
    }
}
