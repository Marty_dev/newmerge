package kr.co.mergepoint.mergeclient.scene.payment.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.SearchCrowdUserBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.payment.view.adapter.SearchCrowdPayUserAdapter;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.SearchCrowdPayUserAdapterListener;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.SearchCrowdPayUserListener;

/**
 * Created by 1017sjg on 2017. 8. 8..
 */

public class SearchCrowdPayUserFragment extends BaseFragment implements TabLayout.OnTabSelectedListener, SearchCrowdPayUserAdapterListener {

    private SearchCrowdUserBinding crowdUserBinding;
    private SearchCrowdPayUserListener listener;

    public static SearchCrowdPayUserFragment newInstance(SearchCrowdPayUserListener listener) {
        Bundle args = new Bundle();
        SearchCrowdPayUserFragment fragment = new SearchCrowdPayUserFragment();
        fragment.listener = listener;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listener.updateCrowedMemberTable(1, "");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        crowdUserBinding = DataBindingUtil.inflate(inflater, R.layout.search_crowd_user, container, false);
        crowdUserBinding.setTitle("함께결제 직원 추가");
        crowdUserBinding.setOnClick(this);
        crowdUserBinding.setTabListener(this);
        crowdUserBinding.setTabArray(new String[]{"전체", "나의 부서", "즐겨찾기"});
        crowdUserBinding.searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(crowdUserBinding.searchText.getText() == null || crowdUserBinding.searchText.getText().toString().isEmpty()) {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "검색어를 입력해주세요", Snackbar.LENGTH_INDEFINITE);
                    return false;
                }
                if(i == EditorInfo.IME_ACTION_SEARCH){
                    int position = crowdUserBinding.searchEnterpriseUserTab.getSelectedTabPosition() + 1;
                    listener.updateCrowedMemberTable(position, crowdUserBinding.searchText.getText().toString());
                }
                return true;
            }
        });
        return crowdUserBinding.getRoot();
    }

    public void setSearchCrowedPayUserAdapter(ArrayList<CrowdMember> arrayList) {
        crowdUserBinding.setUserAdapter(new SearchCrowdPayUserAdapter(this, this, arrayList));
    }

    // 체크된 인원 가져오기
    public String getSelectUserOid() {
        return TextUtils.join(",", crowdUserBinding.getUserAdapter().getOidList());
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition() + 1;
        crowdUserBinding.enterpriseSearchLayout.setVisibility(position == 3 ? View.GONE : View.VISIBLE);
        listener.updateCrowedMemberTable(tab.getPosition() + 1, null);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}

    @Override
    public void onTabReselected(TabLayout.Tab tab) {}

    @Override
    public void checkUser(int count) {
        crowdUserBinding.currentDelegationUserCount.setText(String.format(Locale.getDefault(), "현재 %d명 선택", count));
    }
}
