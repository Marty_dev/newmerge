package kr.co.mergepoint.mergeclient.scene.data.payment;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 1017sjg on 2017. 7. 5..
 */

public class AdaptTicket implements Parcelable {

    @SerializedName("ticketRef")
    public int ticketRef;

    @SerializedName("ticketCount")
    public int ticketCount;

    public AdaptTicket(int ticketRef, int ticketCount) {
        this.ticketRef = ticketRef;
        this.ticketCount = ticketCount;
    }

    private AdaptTicket(Parcel in) {
        ticketRef = in.readInt();
        ticketCount = in.readInt();
    }

    public static final Creator<AdaptTicket> CREATOR = new Creator<AdaptTicket>() {
        @Override
        public AdaptTicket createFromParcel(Parcel in) {
            return new AdaptTicket(in);
        }

        @Override
        public AdaptTicket[] newArray(int size) {
            return new AdaptTicket[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ticketRef);
        dest.writeInt(ticketCount);
    }
}
