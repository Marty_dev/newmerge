package kr.co.mergepoint.mergeclient.scene.data.shop;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class DailyMenuCategory {

    public String title;

    public ArrayList<ShopMenuDetail> menuDetails;

    public DailyMenuCategory(String title) {
        this.title = title;
        this.menuDetails = new ArrayList<>();
    }
}
