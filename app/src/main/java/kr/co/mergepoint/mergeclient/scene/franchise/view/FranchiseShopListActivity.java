package kr.co.mergepoint.mergeclient.scene.franchise.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapReverseGeoCoder;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.co.mergepoint.mergeclient.BuildConfig;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.api.StreetApi;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BasketView;
import kr.co.mergepoint.mergeclient.application.common.ListLoadScrollListener;
import kr.co.mergepoint.mergeclient.application.common.MergeActivity;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.ShopInfo;
import kr.co.mergepoint.mergeclient.scene.franchise.OnLocationDialogClick;
import kr.co.mergepoint.mergeclient.scene.franchise.adapter.FranchiseShopAdapter;
import kr.co.mergepoint.mergeclient.scene.franchise.data.FranchisesShop;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.BasketActivity;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.support.design.widget.Snackbar.LENGTH_SHORT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PERMISSIONS_REQUEST_LOCATION;
import static kr.co.mergepoint.mergeclient.application.common.Properties.REQUEST_CHECK_SETTINGS;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SOURCE_ACTIVITY;
import static kr.co.mergepoint.mergeclient.scene.franchise.view.LocationMapActivity.ADDRESS;
import static kr.co.mergepoint.mergeclient.scene.franchise.view.LocationMapActivity.IS_ADDR;
import static kr.co.mergepoint.mergeclient.scene.franchise.view.LocationMapActivity.LAT;
import static kr.co.mergepoint.mergeclient.scene.franchise.view.LocationMapActivity.LON;
import static kr.co.mergepoint.mergeclient.scene.franchise.view.LocationMapActivity.MAP_SUCCESS;

public class FranchiseShopListActivity extends MergeActivity implements LocationListener, OnLocationDialogClick,MapReverseGeoCoder.ReverseGeoCodingResultListener{

    FranchiseShopListActivity activity = this;
    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.fran_title)
    TextView franTitle;
    @BindView(R.id.fran_location_btn)
    TextView franLocationBtn;
    @BindView(R.id.fran_location_rl)
    RelativeLayout franLocationRl;
    @BindView(R.id.fran_none_text1)
    TextView franNoneText1;
    @BindView(R.id.fran_none_text2)
    TextView franNoneText2;
    @BindView(R.id.fran_none_text)
    TextView franNoneText;
    @BindView(R.id.fran_location_none)
    RelativeLayout franLocationNone;
    @BindView(R.id.fran_shoplist)
    RecyclerView franShoplist;
    public static final String OID_KEY = "OID";
    public static final String TITLE_KEY = "TITLE";

    FusedLocationProviderClient fusedLocationProviderClient;
    public static final int LOCATION_SEARCH = 300;
    LocationManager locationManager;
    @BindView(R.id.fran_basketbtn)
    BasketView franBasketbtn;
    String mNowAddress;

    mLocation mNowLocation;
    mLocation mCurrentLopcation;
    FranchiseShopAdapter adapter;
    StreetApi Api;
    int currentPage=0;
    int totalPage=0;
    public int mOid = -1;
    public String mTitle;
    public final int loadItemCount  = 10;
    private ListLoadScrollListener loadScrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_franchise_shop_list);
        ButterKnife.bind(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        getData();
        adapter = new FranchiseShopAdapter(BASE);
        franShoplist.setAdapter(adapter);

        LocationPermission();


    }

    private void getData(){
        Api = MergeApplication.getMergeAppComponent().getRetrofit().create(StreetApi.class);
        Intent data  = getIntent();

        loadScrollListener = new ListLoadScrollListener() {
            @Override
            public void onLoadMore(int currentPage) {
                getListByPage(currentPage);
            }

            @Override
            public LinearLayoutManager setLinearLayoutManager() {
                return (LinearLayoutManager) franShoplist.getLayoutManager();
            }
        };

        franShoplist.addOnScrollListener(loadScrollListener);

        mOid = data.getIntExtra(OID_KEY,-1);
        mTitle = data.getStringExtra(TITLE_KEY);
        franTitle.setText(mTitle);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getListByLocation(boolean isLocation) {

        if (mOid == -1){
            return;
        }
        MDEBUG.debug("Get List");
        if (!isLocation) {
            franShoplist.setVisibility(View.GONE);
            franLocationNone.setVisibility(View.VISIBLE);
            franBasketbtn.getCartCount(BASE,3,false);
        } else {
            franShoplist.setVisibility(View.VISIBLE);
            franLocationNone.setVisibility(View.GONE);
            franBasketbtn.getCartCount(BASE);

            getListByPage(0);

        }
    }



    public void getListByPage(int page){
        currentPage = page;
        if (mCurrentLopcation == null)
            return;
        boolean isInit = currentPage == 0 ? true : false;

        HashMap<String,Object> params = new HashMap<>();

        params.put("pageNum",currentPage);
        params.put("pageSize",loadItemCount);
        params.put("franchise",mOid);
        params.put("latitude", mCurrentLopcation == null ? null : mCurrentLopcation.lat);
        params.put("longitude",mCurrentLopcation == null ? null :  mCurrentLopcation.lon);
        params.put("version","3.1.2");

        MergeApplication.getMergeApplication().showLoading(BASE);
        Api.getStreetInfo(params).enqueue(new Callback<ShopInfo>() {
            @Override
            public void onResponse(Call<ShopInfo> call, Response<ShopInfo> response) {
                MDEBUG.debug("Success");
                MergeApplication.getMergeApplication().hideLoading(BASE);

                if (response.isSuccessful()){
                    ShopInfo res = response.body();
                    currentPage = res.getCurrentPage() + 1 ;
                    totalPage = res.getTotalPage();
                    adapter = (FranchiseShopAdapter) franShoplist.getAdapter();

                    if (isInit){
                        adapter.arrayList.clear();
                        adapter.arrayList.addAll(res.getShopList());
                        adapter.notifyDataSetChanged();
                    }else{
                        adapter.arrayList.addAll(res.getShopList());
                        adapter.notifyDataSetChanged();
                    }
                }else{
                    showAlert(R.string.retry);
                    loadScrollListener.onFailedLoadList();
                }
            }

            @Override
            public void onFailure(Call<ShopInfo> call, Throwable t) {
                MergeApplication.getMergeApplication().hideLoading(BASE);
                MDEBUG.debug("Failed" + t.toString());
                loadScrollListener.onFailedLoadList();
                showAlert(R.string.retry);

            }
        });
    }
    public void getinitList(){
        currentPage = 0;
        if (mNowLocation == null)
            return;
        boolean isInit = currentPage == 0 ? true : false;

        HashMap<String,Object> params = new HashMap<>();

        params.put("pageNum",currentPage);
        params.put("pageSize",loadItemCount);
        params.put("franchise",mOid);
        params.put("latitude", mNowLocation == null ? null : mNowLocation.lat);
        params.put("longitude",mNowLocation == null ? null :  mNowLocation.lon);
        params.put("version","3.1.2");
        MergeApplication.getMergeApplication().showLoading(BASE);
        Api.getStreetInfo(params).enqueue(new Callback<ShopInfo>() {
            @Override
            public void onResponse(Call<ShopInfo> call, Response<ShopInfo> response) {
                MDEBUG.debug("Success");
                MergeApplication.getMergeApplication().hideLoading(BASE);

                if (response.isSuccessful()){
                    ShopInfo res = response.body();
                    currentPage = res.getCurrentPage() + 1 ;
                    totalPage = res.getTotalPage();
                    adapter = (FranchiseShopAdapter) franShoplist.getAdapter();

                    if (isInit) {
                        adapter.arrayList.clear();
                        adapter.arrayList.addAll(res.getShopList());
                        adapter.notifyDataSetChanged();
                    }
                    franLocationBtn.setText(mNowAddress);
                }else{
                    showAlert(R.string.retry);
                    loadScrollListener.onFailedLoadList();
                }
            }

            @Override
            public void onFailure(Call<ShopInfo> call, Throwable t) {
                MergeApplication.getMergeApplication().hideLoading(BASE);
                MDEBUG.debug("Failed" + t.toString());
                loadScrollListener.onFailedLoadList();
                showAlert(R.string.retry);

            }
        });
    }
    @OnClick({R.id.back_arrow, R.id.fran_location_btn, R.id.fran_none_text, R.id.fran_basketbtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_arrow:
                finish();
                break;
            case R.id.fran_location_btn:
                new LocationDialog(this,this).show();
                break;
            case R.id.fran_none_text:
                LocationPermission();
                break;
            case R.id.fran_basketbtn:
                Intent intent = new Intent(this, BasketActivity.class);
                intent.putExtra(SOURCE_ACTIVITY, FranchiseShopListActivity.class);

                startActivityForResult(intent, PAY_RESULT_REQUEST);
                break;
        }
    }

    @Override
    public void onClick(int position) {
        switch (position){
            case 1:// dialog_map_location:
                Intent inte = new Intent(this,LocationMapActivity.class);
                startActivityForResult(inte,LOCATION_SEARCH);
                break;
            case 2:// dialog_research_location:
                getinitList();
                return;
        }

    }

    @Override
    public void onLocationChanged(Location location) {
       /* locationManager.removeUpdates(this);
        MergeApplication.getMergeApplication().hideLoading(this);
        MDEBUG.debug("Location Init" + location.toString());
        mCurrentLopcation = new mLocation(location.getLatitude(),location.getLongitude());
        mNowLocation = new mLocation(location.getLatitude(), location.getLongitude());
        MapPoint current = MapPoint.mapPointWithGeoCoord(location.getLatitude(),location.getLongitude());
        getAddress(current);
        getListByLocation(true);
        */
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        franBasketbtn.getCartCount(BASE);

        if (requestCode == LOCATION_SEARCH) {
            if (resultCode == MAP_SUCCESS) {
                boolean isSet = data.getBooleanExtra(IS_ADDR, false);
                String currentaddr = data.getStringExtra(ADDRESS);

                double lat = data.getDoubleExtra(LAT, 0.0f);
                double lon = data.getDoubleExtra(LON, 0.0f);
                franLocationBtn.setText(isSet ? currentaddr : "위치정보 없음");
                mCurrentLopcation = new mLocation(lat,lon);


                if (isSet) {
                    getListByLocation( isSet);
                }
            }
        }
    }

    void getAddress(MapPoint savePoint){
        String apiKey = BuildConfig.BUILD_TYPE.equals("debug") ? getString(R.string.kakao_key_debug) : getString(R.string.kakao_key);
        MapReverseGeoCoder reverseGeoCoder = new MapReverseGeoCoder(apiKey, savePoint, this, this);
        reverseGeoCoder.startFindingAddress();
    }
    @Override
    public void onReverseGeoCoderFoundAddress(MapReverseGeoCoder mapReverseGeoCoder, String addressString) {
        // 주소를 찾은 경우.
        franLocationBtn.setText(addressString);
        mNowAddress = addressString;

    }

    @Override
    public void onReverseGeoCoderFailedToFindAddress(MapReverseGeoCoder mapReverseGeoCoder) {
        // 호출에 실패한 경우.
        franLocationBtn.setText( "주소 정보를 가져올수없습니다.");

    }
    void LocationPermission() {
        MergeApplication.getMergeApplication().showLoading(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            /*마시멜로우 이상 권한체크*/
            boolean fineResult = ContextCompat.checkSelfPermission(mCon, ACCESS_FINE_LOCATION) != PERMISSION_GRANTED;
            boolean coarseResult = ContextCompat.checkSelfPermission(mCon, ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED;
            if (fineResult && coarseResult) {
                if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION) || shouldShowRequestPermissionRationale(ACCESS_COARSE_LOCATION)) {
                    /*권한을 취소한적이 있을 때*/
                    Snackbar.make(findViewById(android.R.id.content), R.string.request_permission, LENGTH_SHORT).show();
                } else {
                    /*권한 취소를 한적이 없는 최초의 권한 동의*/
                    new Handler().postDelayed(() -> {
                        new MergeDialog.Builder(mCon)
                                .setContent(R.string.location_permission_text)
                                .setConfirmClick(() -> {
                                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_LOCATION);
                                })
                                .setCancelClick(() -> {
                                    getListByLocation(false);
                                }).build().show();
                    }, 400);
                }
            } else {
                /*권한 동의했을 때*/
                requestLocationUpdates();
            }
        } else {
            /*마시멜로우 아랫버전으로 권한체크 불필요*/
            requestLocationUpdates();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @SuppressLint("MissingPermission")
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                /*권한 허락*/
                LocationPermission();
            } else {
                /*권한 거절*/
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) ||
                        shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    hideKeyboard();
                    Snackbar.make(findViewById(android.R.id.content), getString(R.string.request_permission), Snackbar.LENGTH_SHORT).show();
                    getListByLocation(false);

                }

            }
        }
    }

    @SuppressLint("MissingPermission")
    private void requestLocationUpdates() {
        fusedLocationProviderClient = new FusedLocationProviderClient(BASE);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(new LocationRequest());
        SettingsClient client = LocationServices.getSettingsClient(BASE);
        client.checkLocationSettings(builder.build()).addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                LocationRequest request = new LocationRequest();
                LocationCallback callback = new LocationCallback(){
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        super.onLocationResult(locationResult);
                        MDEBUG.debug("onLocationRESULT");
                        double lat = locationResult.getLocations().get(0).getLatitude();
                        double lon = locationResult.getLocations().get(0).getLongitude();
                        MDEBUG.debug("LOCATION  = LAT  : " + lat +", LON : " + lon);
                        fusedLocationProviderClient.removeLocationUpdates(this);
                        MergeApplication.getMergeApplication().hideLoading(BASE);
                        mCurrentLopcation = new mLocation(lat,lon);
                        mNowLocation = new mLocation(lat,lon);
                        MapPoint current = MapPoint.mapPointWithGeoCoord(lat,lon);
                        getAddress(current);
                        getListByLocation(true);
                    }

                    @Override
                    public void onLocationAvailability(LocationAvailability locationAvailability) {
                        super.onLocationAvailability(locationAvailability);
                        fusedLocationProviderClient.removeLocationUpdates(this);
                        MDEBUG.debug("onLocationAVAIABLE");
                    }
                };
                fusedLocationProviderClient.requestLocationUpdates(request,callback, Looper.myLooper());
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 1, activity);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 1, activity);
            }
        }).addOnFailureListener(BASE, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                MergeApplication.getMergeApplication().hideLoading(BASE);

                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        /*위치 설정에 만족하지 못했을때, 위치설정 업그레이드가 필요함!*/
                        Log.i("UserLocation", "Location settings are not satisfied. Attempting to upgrade location settings ");
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the
                            // result in onActivityResult().
                            ResolvableApiException rae = (ResolvableApiException) e;
                            rae.startResolutionForResult(BASE, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sie) {
                            Log.i("UserLocation", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        /*위치 설정이 적합하지않음, 설정을 고쳐야함!*/
                        String errorMessage = "Location settings are inadequate, and cannot be fixed here. Fix in Settings.";
                        Log.e("UserLocation", errorMessage);
                        Snackbar.make(BASE.findViewById(android.R.id.content), errorMessage, LENGTH_SHORT).show();
                }
            }
        });


    }


    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
    }

    @Override
    public void onProviderDisabled(String s) {
        mCurrentLopcation = null;
        MergeApplication.getMergeApplication().hideLoading(this);
        MDEBUG.debug("Disabled");
    }

    class mLocation{
        public mLocation(double lat, double lon) {
            this.lat = lat;
            this.lon = lon;
        }

        public double lat;
        public double lon;

    }




}
