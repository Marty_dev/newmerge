package kr.co.mergepoint.mergeclient.api;

import java.util.ArrayList;
import java.util.Map;

import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.main.Alarmdata;
import kr.co.mergepoint.mergeclient.scene.data.main.GradeRes;
import kr.co.mergepoint.mergeclient.scene.data.main.MemberGrade;
import kr.co.mergepoint.mergeclient.scene.data.main.UserPoints;
import kr.co.mergepoint.mergeclient.scene.data.main.userPointinfo;
import kr.co.mergepoint.mergeclient.scene.data.menu.CouponApplyInfo;
import kr.co.mergepoint.mergeclient.scene.data.payment.Coupon;
import kr.co.mergepoint.mergeclient.scene.data.register.IdConfirm;
import kr.co.mergepoint.mergeclient.scene.data.register.PhoneConfirm;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by jgson on 2017. 5. 31..
 */

public interface
UserApi {

    /*POST*/
    @POST("signin/authenticate")
    @FormUrlEncoded
    Call<ResponseObject<Member>> login(@FieldMap Map<String, String> loginInfo);

    @POST("signup")
    @FormUrlEncoded
    Call<ResponseObject<Member>> register(@FieldMap Map<String, Object> userInfo);

    @POST("forgotPassword")
    @FormUrlEncoded
    Call<ResponseObject<String>> forgotPassword(@Field("id") String id);

    @POST("getInitialInfo")
    @FormUrlEncoded
    Call<InitialInfo> getInitialInfo(@Field("registrationId") String id);

    @POST("registMemberKey")
    @FormUrlEncoded
    Call<ResponseBody> sendMemberKey(@Field("registrationId") String id);

    @POST("phoneConfirm")
    @FormUrlEncoded
    Call<PhoneConfirm> phoneSubscribed(@Field("phone") String phoneNum, @Field("confirmNumber") String subscripedNum);

    @POST("confirmPerson")
    @FormUrlEncoded
    Call<ResponseObject<Member>> getUserPrivacy(@Field("password") String password);

    @POST("member/retire")
    @FormUrlEncoded
    Call<ResponseObject<Object>> userWithdrawal(@Field("refund") boolean refund);

    @POST("points")
    @FormUrlEncoded
    Call<ResponseObject<UserPoints>> pointList(@Field("type") int type, @Field("period") int period);

    @POST("member/readPoints")
    Call<ResponseObject<Long>> getUserPoints();

    @POST("modifyInfo")
    Call<ResponseObject<Member>> userModifyInfo(@Body Member member);

    @POST("coupon/download")
    @FormUrlEncoded
    Call<ResponseObject<Integer>> downCoupon(@Field("oid") int oid);

    @POST("socialAuth")
    @FormUrlEncoded
    Call<ResponseObject<Member>> googleTokenLogin(@Field("provider")String provider, @Field("idToken") String idToken, @Field("accessToken") String accessToken);

    @POST("socialSignup")
    @FormUrlEncoded
    Call<ResponseObject<Member>> sendSocialRegistMail(@Field("instaId") String instaId, @Field("facebookId") String facebookId, @Field("social") String social, @Field("email") String email);

    @POST("getMemberInfo")
    @FormUrlEncoded
    Call<ResponseObject<Member>> getMemberInfo(@Field("userPath") String userPath);

    @POST("member/restoreReq")
    Call<ResponseObject<Member>> restoreUser();

    @POST("member/resetPassword")
    @FormUrlEncoded
    Call<ResponseObject<Member>> resetPassword(@Field("oid") int oid, @Field("userPath") String userPath, @Field("password") String password);

    @POST("set/approveType")
    @FormUrlEncoded
    Call<ResponseObject<Boolean>> changeCrowedApprovalType(@Field("type") int type, @Field("approver") int approver);

    @POST("set/approveType")
    @FormUrlEncoded
    Call<ResponseObject<Boolean>> changeCrowedApprovalType(@Field("type") int type);

    /*GET*/
    @GET("checkSubscribed")
    Call<IdConfirm> subscribed(@Query("id") String email);

    @GET("phoneConfirm/{phone}")
    Call<ResponseObject<String>> requestPhone(@Path("phone") String phoneNum);

    @GET("logout")
    Call<ResponseBody> logout();

    @GET("auth/facebook")
    Call<ResponseBody> facebookLogin();


    @GET("auth/naver")
    Call<ResponseBody> naverLogin();

    @GET("auth/kakao")
    Call<ResponseBody> kakaoLogin();

    @GET("auth/instagram")
    Call<ResponseBody> instaLogin();

    @GET("coupon/list")
    Call<ResponseObject<ArrayList<Coupon>>> couponList();


    @POST("coupon/download/v1")
    @FormUrlEncoded
    Call<ResponseObject<CouponApplyInfo>> applyCoupon(@Field("code") String code );
    //@Field("type") String type

    @GET("/member/readInfo")
    Call<userPointinfo> readMemberInfo();

    @GET("/fcm/list")
    Call<ArrayList<Alarmdata>> readAlarmList();

    @GET("/memberGrade/read")
    Call<GradeRes> readmembergrade();
}
