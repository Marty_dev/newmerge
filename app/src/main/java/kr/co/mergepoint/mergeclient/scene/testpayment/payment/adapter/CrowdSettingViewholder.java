package kr.co.mergepoint.mergeclient.scene.testpayment.payment.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.co.mergepoint.mergeclient.R;

/**
 * User: Marty
 * Date: 2018-07-22
 * Time: 오후 8:11
 * Description:
 */
public class CrowdSettingViewholder extends RecyclerView.ViewHolder {


        @BindView(R.id.crowd_delete)
        ImageView crowdDelete;
        @BindView(R.id.crowd_membername)
        TextView crowdMembername;
        @BindView(R.id.crowd_member_pointconfirm)
        TextView crowdMemberPointconfirm;
        @BindView(R.id.crowd_member_pointedt)
        EditText crowdMemberPointedt;
        @BindView(R.id.crowd_member_pointrl)
        RelativeLayout crowdMemberPointrl;
        @BindView(R.id.crowd_member_tickettv)
        TextView crowdMemberTickettv;
        @BindView(R.id.crowd_member_ticketsp)
        RelativeLayout crowdMemberTicketsp;
        @BindView(R.id.crowd_ticket_minus)
        ImageView crowdTicketMinus;
        @BindView(R.id.crowd_ticket_counttv)
        TextView crowdTicketCounttv;
        @BindView(R.id.crowd_ticket_plus)
        ImageView crowdTicketPlus;
        @BindView(R.id.crowd_ticket_countrl)
        RelativeLayout crowdTicketCountrl;
        @BindView(R.id.crowd_member_ticketrl)
        RelativeLayout crowdMemberTicketrl;
        @BindView(R.id.crowd_member_havepoint)
        TextView crowdMemberHavepoint;

        @BindView(R.id.crowd_member_ticketdiscription)
        TextView crowdMemberTicketdiscription;
        public CrowdSettingViewholder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }

}
