package kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.EnterpriseMenuListItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.shop.DailyMenuCategory;

/**
 * Created by 1017sjg on 2017. 6. 24..
 */

public class EnterpriseMenuGroupAdapter extends BasicListAdapter<BasicListHolder<EnterpriseMenuListItemBinding, DailyMenuCategory>, DailyMenuCategory> {

    public EnterpriseMenuGroupAdapter(ArrayList<DailyMenuCategory> menuGroups) {
        super(menuGroups);
    }

    @Override
    public BasicListHolder<EnterpriseMenuListItemBinding, DailyMenuCategory> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.enterprise_menu_list_item, parent, false);
        EnterpriseMenuListItemBinding itemBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<EnterpriseMenuListItemBinding, DailyMenuCategory>(itemBinding) {
            @Override
            public void setDataBindingWithData(DailyMenuCategory data) {
                getDataBinding().setMenuGroupName(data.title);
                getDataBinding().setMenuChildAdapter(new EnterpriseMenuChildAdapter(data.menuDetails));
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<EnterpriseMenuListItemBinding, DailyMenuCategory> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }
}
