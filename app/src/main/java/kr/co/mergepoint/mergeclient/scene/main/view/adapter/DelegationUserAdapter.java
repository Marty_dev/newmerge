package kr.co.mergepoint.mergeclient.scene.main.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.databinding.SearchDelegationUserItemBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.main.view.listener.DelegationUserListener;

/**
 * Created by 1017sjg on 2017. 8. 16..
 */

public class DelegationUserAdapter extends BasicListAdapter<BasicListHolder<SearchDelegationUserItemBinding, CrowdMember>, CrowdMember> implements View.OnClickListener {

    private int selectUserOid;
    private View.OnClickListener onClickListener;

    public CrowdMember getSelectCrowdMember() {
        for (CrowdMember crowdMember :getObservableArrayList()) {
            if (selectUserOid == -1)
                return null;
            if (crowdMember.oid == selectUserOid)
                return crowdMember;
        }
        return null;
    }

    public DelegationUserAdapter(ArrayList<CrowdMember> arrayList, View.OnClickListener onClickListener) {
        super(arrayList);
        this.onClickListener = onClickListener;
    }
    public DelegationUserAdapter(ArrayList<CrowdMember> arrayList, View.OnClickListener onClickListener,int selectUserOid) {
        super(arrayList);
        this.onClickListener = onClickListener;
        this.selectUserOid = selectUserOid;
    }


    @Override
    public BasicListHolder<SearchDelegationUserItemBinding, CrowdMember> setCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_delegation_user_item, parent, false);
        SearchDelegationUserItemBinding crowdUserBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<SearchDelegationUserItemBinding, CrowdMember>(crowdUserBinding) {
            @Override
            public void setDataBindingWithData(CrowdMember data) {
                getDataBinding().setUser(data);
                getDataBinding().setCheckOid(selectUserOid);
                getDataBinding().setOnClick(DelegationUserAdapter.this);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<SearchDelegationUserItemBinding, CrowdMember> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.radio_button:
                selectUserOid = (int) view.getTag();
                notifyDataSetChanged();
                break;
            default:
                onClickListener.onClick(view);
                break;
        }
    }
}
