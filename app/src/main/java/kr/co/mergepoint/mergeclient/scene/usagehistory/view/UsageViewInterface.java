package kr.co.mergepoint.mergeclient.scene.usagehistory.view;

import java.util.ArrayList;
import java.util.Map;

import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.scene.data.history.UsagePayment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.OrderHistoryFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.ShopHistoryFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.UsingFragment;
import kr.co.mergepoint.mergeclient.scene.usagehistory.view.fragment.UsingFragmentv2;

/**
 * Created by jgson on 2017. 12. 13..
 */

public interface UsageViewInterface {

    void setPaymentPagerData(ArrayList<UsagePayment> usagePayments, int position);

    /* 이용내역 리스트에서 탭에 따라 열기 */
    void openHistory(UsagePayment usagePayment);

    /* oid로 한 점포의 구매한 내역 보여주기 */
    void openShopHistory(int oid);

    /* 주문별 이용내역에서 oid 가져오기 */
    int getPayCancelOid();

    /* 결제취소 혹은 포인트 환불이 완료된 페이지 열기 */
    void openPayCancelFragment(boolean isPayCancelStr);

    /* 메뉴 승인 완료된 페이지 열기 */
    void openApproveResultFragment();

    /* 몇건의 메뉴를 사용할지 사용자에게 알려주는 다이얼로그 */
    void showUsageMenuAlert(MergeDialog.OnDialogClick sendMenu);

    /* 사용하기 후 점포의 사용할 수 있는 메뉴 내역 새로고침 */
    void updateShopHistory();

    /* 사용하기 후 결제에 대한 메뉴 새로고침 */
    void updateOrderHistory();

    void setApproveFlag(boolean isApprove) ;

    void resetPinCodeView();

    void getIntent();

    /*GETTER*/
    UsingFragmentv2 getUsingFragment();

    OrderHistoryFragment getOrderHistoryFragment();

    ShopHistoryFragment getShopHistoryFragment();

    int getRejectOid();

    Map<String, Object> getApproveInfo();
}
