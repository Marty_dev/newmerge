package kr.co.mergepoint.mergeclient.scene.login.presenter.callback;

import java.net.CookieHandler;
import java.util.Objects;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.MergeDialog;
import kr.co.mergepoint.mergeclient.application.common.BaseCallback;
import kr.co.mergepoint.mergeclient.application.common.CustomCookieManager;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.splash.InitialInfo;
import kr.co.mergepoint.mergeclient.scene.login.model.LoginModel;
import kr.co.mergepoint.mergeclient.scene.login.view.LoginView;
import kr.co.mergepoint.mergeclient.scene.main.MainActivity;
import kr.co.mergepoint.mergeclient.scene.register.RegisterActivity;
import okhttp3.HttpUrl;
import okhttp3.ResponseBody;

import static kr.co.mergepoint.mergeclient.application.common.Properties.BASE_URL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.MEMBER_INFO;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class LoginCallback extends BaseCallback<LoginView, LoginModel> implements LoginCallbackInterface{

    public LoginCallback(LoginView baseView, LoginModel baseModel) {
        super(baseView, baseModel);
    }

    @Override
    public MergeCallback<ResponseBody> socialLoginCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            HttpUrl url = response.raw().request().url();
            if (response.isSuccessful() && url != null && !Objects.equals(url.toString(), BASE_URL) && baseModel.isSocialHost(url.host())) {
                baseView.openSocialLogin(url.toString(), baseView.activity);
            } else {
                baseView.showAlert(R.string.retry);
            }
        }, R.string.login_fail);
    }

    @Override
    public MergeCallback<ResponseObject<Member>> idLoginCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Member> responseObject = response.body();
            if (responseObject != null) {
                Member member = responseObject.getObject();
                if (!responseObject.isFailed() && member != null) {
                    baseView.userStateCheck(member, () -> {
                        baseView.showLoading();
                        baseModel.restoreUser(restoreUser());
                    }, () -> baseModel.logout(getLogoutCallback()));
                } else {
                    baseView.showAlert(responseObject.getMessage());
                }
            } else {
                baseView.showAlert(R.string.retry);
            }
        }, R.string.login_fail);
    }

    @Override
    public MergeCallback<ResponseObject<Member>> googleResponseCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Member> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                Member member = responseObject.getObject();
                if (Objects.equals(responseObject.getCode(), "MRG011") && responseObject.isFailed()) {
                    /* 회원정보 등록이 필요한 회원 - UserConnection 혹은 MemberInfo 둘중 하나만 있을 경우*/
                    member.setSocial(true);
                    baseView.openActivityWithParcelable(RegisterActivity.class, MEMBER_INFO, member);
                } else if (member != null){
                    baseView.userStateCheck(member, () -> {
                        baseView.showLoading();
                        baseModel.restoreUser(restoreUser());
                    }, () -> baseModel.logout(getLogoutCallback()));
                } else {
                    baseView.showAlert(R.string.retry);
                }
            } else {
                baseView.showAlert(R.string.retry);
            }
        }, R.string.google_login_fail);
    }

    @Override
    public MergeCallback<ResponseObject<String>> forgotCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<String> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                if (!responseObject.isFailed()) {
                    baseView.openForgotPwResultPage();
                } else {
                    baseView.showAlert(responseObject.getMessage());
                }
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Member>> sendRegistMail() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Member> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null) {
                if (!responseObject.isFailed()) {
                    new MergeDialog.Builder(baseView.activity)
                            .setContent(baseView.getString(R.string.send_signup_mail))
                            .setConfirmString(baseView.getString(R.string.move_main)).setCancelBtn(false)
                            .setConfirmClick(() -> baseView.openClearTaskActivity(MainActivity.class)).build().show();
                } else {
                    baseView.showAlert(responseObject.getMessage());
                }
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<Member>> restoreUser() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Member> responseObject = response.body();
            if (responseObject != null) {
                Member member = responseObject.getObject();
                if (!responseObject.isFailed() && member != null) {
                    baseView.openMain(member);
                } else {
                    baseView.showAlert(responseObject.getMessage());
                }
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseBody> getLogoutCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            CustomCookieManager customCookieManager = (CustomCookieManager) CookieHandler.getDefault();
            customCookieManager.getCookieStore().removeAll();
            InitialInfo initialInfo = MergeApplication.getInitialInfo();
            if (initialInfo != null)
                initialInfo.setLoginState(null, false);
            baseView.openClearTaskActivity(MainActivity.class);
        }, R.string.logout_fail);
    }
}
