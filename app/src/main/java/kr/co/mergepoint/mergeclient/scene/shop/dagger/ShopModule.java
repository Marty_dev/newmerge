package kr.co.mergepoint.mergeclient.scene.shop.dagger;

import dagger.Module;
import dagger.Provides;
import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;
import kr.co.mergepoint.mergeclient.scene.shop.model.ShopModel;
import kr.co.mergepoint.mergeclient.scene.shop.presenter.ShopPresenter;
import kr.co.mergepoint.mergeclient.scene.shop.presenter.callback.ShopCallback;
import kr.co.mergepoint.mergeclient.scene.shop.view.ShopView;

/**
 * Created by jgson on 2017. 6. 5..
 */

@Module
public class ShopModule {

    private ShopActivity activity;

    public ShopModule(ShopActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ShopScope
    ShopModel provideModel() { return new ShopModel(); }

    @Provides
    @ShopScope
    ShopView provideView() { return new ShopView(activity, R.layout.shop_detail); }

    @Provides
    @ShopScope
    ShopPresenter providePresenter(ShopView view, ShopModel model) {
        return new ShopPresenter(view, model, new ShopCallback(view, model));
    }
}
