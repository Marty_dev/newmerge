package kr.co.mergepoint.mergeclient.scene.testpayment.payment.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseActivity;
import kr.co.mergepoint.mergeclient.application.common.BaseFragment;
import kr.co.mergepoint.mergeclient.databinding.SearchCrowdUserBinding;
import kr.co.mergepoint.mergeclient.scene.data.payment.CrowdMember;
import kr.co.mergepoint.mergeclient.scene.payment.view.adapter.SearchCrowdPayUserAdapter;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.SearchCrowdPayUserAdapterListener;
import kr.co.mergepoint.mergeclient.scene.payment.view.listener.SearchCrowdPayUserListener;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.CrowdSettingActivity;
import kr.co.mergepoint.mergeclient.scene.testpayment.payment.Payment_v2_Activity;

/**
 * Created by 1017sjg on 2017. 8. 8..
 */

public class SearchCrowdPayUserFragmentV2 extends BaseFragment implements TabLayout.OnTabSelectedListener, SearchCrowdPayUserAdapterListener {

    private SearchCrowdUserBinding crowdUserBinding;
    private SearchCrowdPayUserListener listener;
    private BaseActivity activity;
    int oid;

    public static SearchCrowdPayUserFragmentV2 newInstance(SearchCrowdPayUserListener listener, BaseActivity activity) {
        Bundle args = new Bundle();
        SearchCrowdPayUserFragmentV2 fragment = new SearchCrowdPayUserFragmentV2();
        fragment.listener = listener;
        fragment.activity= activity;
        fragment.setArguments(args);
        if (activity instanceof Payment_v2_Activity)
            fragment.oid = ((Payment_v2_Activity)activity).shops.get(0).oid;
        else if (activity instanceof CrowdSettingActivity)
            fragment.oid = ((CrowdSettingActivity)activity).oid;
        return fragment;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() ==  R.id.back){
            activity.getSupportFragmentManager().beginTransaction()
                    .remove(this)
                    .commit();
            activity.getSupportFragmentManager().popBackStack();
        }else if (v.getId() == R.id.crowd_favorite_button){
            activity.callbacks.Requestfavorit((int)v.getTag());
        }else if (v.getId() == R.id.add_crowd_pay_user){

//            if (getSelectUserOid() == null || getSelectUserOid().isEmpty()){
//                new MergeDialog.Builder(activity).setContent("함께 결제할 인원을 선택해 주세요").setCancelBtn(false).build().show();
//                return;
//            }
            activity.callbacks.RequestAddCrowds(oid,getSelectUserOid());
            if (activity instanceof CrowdSettingActivity){
                activity.getSupportFragmentManager().beginTransaction()
                        .remove(this)
                        .commit();
                activity.getSupportFragmentManager().popBackStack();

            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listener.updateCrowedMemberTable(1, "");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        crowdUserBinding = DataBindingUtil.inflate(inflater, R.layout.search_crowd_user, container, false);
        crowdUserBinding.setTitle("함께결제 직원 추가");
        crowdUserBinding.setOnClick(this);
        crowdUserBinding.setTabListener(this);
        crowdUserBinding.setTabArray(new String[]{"전체", "나의 부서", "즐겨찾기"});
        crowdUserBinding.searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                String searchtext = null;
                if(crowdUserBinding.searchText.getText() == null || crowdUserBinding.searchText.getText().toString().isEmpty()) {
                    searchtext = "";
                }
                if(i == EditorInfo.IME_ACTION_SEARCH){
                    int position = crowdUserBinding.searchEnterpriseUserTab.getSelectedTabPosition() + 1;
                    listener.updateCrowedMemberTable(position, (searchtext == null ? crowdUserBinding.searchText.getText().toString() : searchtext));
                }
                return true;
            }
        });
        return crowdUserBinding.getRoot();
    }

    public void setSearchCrowedPayUserAdapter(ArrayList<CrowdMember> arrayList) {
        crowdUserBinding.setUserAdapter(new SearchCrowdPayUserAdapter(this, this, arrayList));
    }

    // 체크된 인원 가져오기
    public String getSelectUserOid() {
        return TextUtils.join(",", crowdUserBinding.getUserAdapter().getOidList());
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition() + 1;
        crowdUserBinding.enterpriseSearchLayout.setVisibility(position == 3 ? View.GONE : View.VISIBLE);
        listener.updateCrowedMemberTable(tab.getPosition() + 1, null);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}

    @Override
    public void onTabReselected(TabLayout.Tab tab) {}

    @Override
    public void checkUser(int count) {
        crowdUserBinding.currentDelegationUserCount.setText(String.format(Locale.getDefault(), "현재 %d명 선택", count));
    }
}
