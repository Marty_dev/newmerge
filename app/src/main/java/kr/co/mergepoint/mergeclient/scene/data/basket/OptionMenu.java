package kr.co.mergepoint.mergeclient.scene.data.basket;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by 1017sjg on 2017. 6. 28..
 */

public class OptionMenu implements Parcelable {
    public int optionKindRef;
    public ArrayList<SelectOption> menus;

    public OptionMenu(int optionKindRef, ArrayList<SelectOption> menus) {
        this.optionKindRef = optionKindRef;
        this.menus = menus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    private OptionMenu(Parcel in) {
        optionKindRef = in.readInt();
        menus = new ArrayList<>();
        in.readTypedList(menus, SelectOption.CREATOR);
    }

    public static final Creator<OptionMenu> CREATOR = new Creator<OptionMenu>() {
        @Override
        public OptionMenu createFromParcel(Parcel in) {
            return new OptionMenu(in);
        }

        @Override
        public OptionMenu[] newArray(int size) {
            return new OptionMenu[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(optionKindRef);
        dest.writeTypedList(menus);
    }
}
