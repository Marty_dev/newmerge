package kr.co.mergepoint.mergeclient.scene.myzonelist.model;

import kr.co.mergepoint.mergeclient.application.common.BaseModel;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Filter;
import kr.co.mergepoint.mergeclient.scene.data.splash.StreetInfo;

import static kr.co.mergepoint.mergeclient.application.common.Properties.LIST;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class MyZoneListModel extends BaseModel {

    private Filter listFilter;

    public MyZoneListModel() {
        this.listFilter = new Filter(LIST);
        this.listFilter.setDistance(3);
        this.listFilter.setStreetInfo(new StreetInfo("00"));
    }

    public Filter getListFilter() {
        return listFilter;
    }
}
