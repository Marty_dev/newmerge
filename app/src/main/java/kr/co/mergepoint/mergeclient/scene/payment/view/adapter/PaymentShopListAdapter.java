package kr.co.mergepoint.mergeclient.scene.payment.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.MergeApplication;
import kr.co.mergepoint.mergeclient.application.common.BasicListAdapter;
import kr.co.mergepoint.mergeclient.application.common.BasicListHolder;
import kr.co.mergepoint.mergeclient.application.common.CustomTextWatcher;
import kr.co.mergepoint.mergeclient.databinding.PaymentShopListBinding;
import kr.co.mergepoint.mergeclient.scene.data.login.Member;
import kr.co.mergepoint.mergeclient.scene.data.payment.ShopPayment;
import kr.co.mergepoint.mergeclient.scene.payment.presenter.PaymentPresenter;
import kr.co.mergepoint.mergeclient.scene.payment.view.adapter.CouponSpinnerAdapter;

import kr.co.mergepoint.mergeclient.util.MDEBUG;

import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_ROLE;

/**
 * Created by 1017sjg on 2017. 7. 6..
 */

public class PaymentShopListAdapter extends BasicListAdapter<BasicListHolder<PaymentShopListBinding, ShopPayment>, ShopPayment> {

    private int points;
    private boolean isEnterpriseUser;
    private View.OnClickListener onClickListener;
    private AdapterView.OnItemSelectedListener onItemSelectedListener;
    private View.OnTouchListener onTouchListener;
    private TextView.OnEditorActionListener onEditorActionListener;

    public PaymentShopListAdapter(ArrayList<ShopPayment> arrayList, int points, PaymentPresenter presenter) {
        super(arrayList);
        this.points = points;
        this.onItemSelectedListener = presenter;
        this.onTouchListener = presenter;
        this.onEditorActionListener = presenter;
        this.onClickListener = presenter;

        Member member = MergeApplication.getMember();
        isEnterpriseUser = member != null && member.getRoles().indexOf(ENTERPRISE_ROLE) >= 0;
    }

    @Override
    public BasicListHolder<PaymentShopListBinding, ShopPayment> setCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_shop_list, parent, false);
        PaymentShopListBinding listBinding = DataBindingUtil.bind(view);

        return new BasicListHolder<PaymentShopListBinding, ShopPayment>(listBinding) {
            @Override
            public void setDataBindingWithData(ShopPayment data) {

                getDataBinding().setOnClick(onClickListener);
                getDataBinding().setShopPayment(data);
                getDataBinding().setUserPoints(points);
                getDataBinding().setShopCouponAdapter(new CouponSpinnerAdapter(R.layout.shop_coupon_normal, R.layout.shop_coupon_dropdown));
                getDataBinding().setShopCouponListener(onItemSelectedListener);
                getDataBinding().setPointChange(new CustomTextWatcher(getDataBinding().usedPointText));

                MDEBUG.debug("shop Type =  :"+data.shopType);
                if (data.shopType == 3 || data.shopType == 2) {
                    getDataBinding().enterpriseLayout.setBackground(MergeApplication.getUtility().getDrawable(R.drawable.payment_bottom));
                } else {
                    getDataBinding().enterpriseLayout.setBackgroundColor(MergeApplication.getUtility().getColor(android.R.color.white));
                }
                getDataBinding().setIsEnterpriseUser(isEnterpriseUser);
                getDataBinding().shopSelectCoupon.setOnTouchListener(onTouchListener);
                getDataBinding().usedPointText.setOnEditorActionListener(onEditorActionListener);
            }
        };
    }

    @Override
    public void setBindViewHolder(BasicListHolder<PaymentShopListBinding, ShopPayment> holder, int position) {
        holder.bind(getObservableArrayList().get(position));
    }

    public void changeShopPayment(int shopPayOid, int companyPoints) {
        for (ShopPayment shopPayment :getObservableArrayList()) {
            if (shopPayment.oid == shopPayOid) {
                shopPayment.setCompanyPoints(companyPoints);
            }
        }
        notifyDataSetChanged();
    }
}
