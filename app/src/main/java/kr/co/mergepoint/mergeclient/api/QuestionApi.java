package kr.co.mergepoint.mergeclient.api;

import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by 1017sjg on 2017. 9. 14..
 */

public interface QuestionApi {

    @POST("question")
    @Multipart
    Call<ResponseObject<Integer>> requestQuestion(@Part("email") RequestBody email, @Part("phone") RequestBody phone, @Part("type") RequestBody type,
                                                  @Part("title") RequestBody title, @Part("body") RequestBody body, @Part MultipartBody.Part image);

    @POST("question")
    @Multipart
    Call<ResponseObject<Integer>> requestQuestion(@Part("email") RequestBody email, @Part("phone") RequestBody phone, @Part("type") RequestBody type,
                                                  @Part("title") RequestBody title, @Part("body") RequestBody body);

    @POST("alliance")
    @FormUrlEncoded
    Call<ResponseObject<Object>> requestAlliance(@Field("email") String email, @Field("phone") String phone,
                                                 @Field("name") String name, @Field("title") String title, @Field("body") String body);
}
