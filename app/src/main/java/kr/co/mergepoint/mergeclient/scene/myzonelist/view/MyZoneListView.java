package kr.co.mergepoint.mergeclient.scene.myzonelist.view;

import android.support.v7.widget.Toolbar;

import java.util.Map;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseView;
import kr.co.mergepoint.mergeclient.databinding.MyZoneListBinding;
import kr.co.mergepoint.mergeclient.scene.data.shoplist.Shop;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.EnterpriseShopActivity;
import kr.co.mergepoint.mergeclient.scene.myzonelist.MyZoneListActivity;
import kr.co.mergepoint.mergeclient.scene.myzonelist.presenter.MyZoneListPresenter;
import kr.co.mergepoint.mergeclient.scene.payment.view.fragment.PaymentResultFragment;
import kr.co.mergepoint.mergeclient.scene.shop.ShopActivity;
import kr.co.mergepoint.mergeclient.scene.shoplist.ShopListActivity;
import kr.co.mergepoint.mergeclient.scene.shoplist.view.adapter.ShopListAdapter;
import kr.co.mergepoint.mergeclient.scene.shoppingbasket.BasketActivity;

import static kr.co.mergepoint.mergeclient.application.common.Properties.CORPORATE_ALLIANCE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.CORPORATE_CAFETERIA;
import static kr.co.mergepoint.mergeclient.application.common.Properties.FRANCHISE;
import static kr.co.mergepoint.mergeclient.application.common.Properties.GENERAL_PARTNERSHIP;
import static kr.co.mergepoint.mergeclient.application.common.Properties.INITIAL;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.PAY_RESULT_TAG;
import static kr.co.mergepoint.mergeclient.application.common.Properties.RIGHT_LEFT;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SHOP_INFO_REQUEST;
import static kr.co.mergepoint.mergeclient.application.common.Properties.SOURCE_ACTIVITY;
import static kr.co.mergepoint.mergeclient.application.common.Properties.STREET_NAME;

/**
 * Created by 1017sjg on 2017. 6. 22..
 */

public class MyZoneListView extends BaseView<MyZoneListActivity, MyZoneListPresenter, MyZoneListBinding> implements MyZoneListInterface {

    public MyZoneListView(MyZoneListActivity activity, int layout) {
        super(activity, layout);
    }

    @Override
    protected Toolbar setToolbar() {
        return binding.toolbarBinding.basicToolbar;
    }

    @Override
    public void openShoppingBasket() {
        openActivityForResult(BasketActivity.class, ShopListActivity.class, SOURCE_ACTIVITY, PAY_RESULT_REQUEST);
    }

    @Override
    public void openPaymentResult() {
        openFragment(RIGHT_LEFT, R.id.shop_list_filter_layout, PaymentResultFragment.newInstance(), PAY_RESULT_TAG);
    }


    public void openPaymentResult(int paymentRef , boolean ismulti) {
        openFragment(RIGHT_LEFT, R.id.shop_list_root, PaymentResultFragment.newInstance(activity,paymentRef,R.id.shop_list_root,ismulti), PAY_RESULT_TAG);
    }

    @Override
    public void openShopDetail(Shop shop) {
        if (shop.type == GENERAL_PARTNERSHIP || shop.type == FRANCHISE) {
            openActivityForResultWithParcelable(ShopActivity.class, SHOP_INFO, shop, SHOP_INFO_REQUEST);
        } else if (shop.type == CORPORATE_ALLIANCE || shop.type == CORPORATE_CAFETERIA) {
            openActivityForResultWithParcelable(EnterpriseShopActivity.class, SHOP_INFO, shop, SHOP_INFO_REQUEST);
        }
    }

    @Override
    public void animCart() {
        binding.shopListCart.getCartCount(this);
    }

    @Override
    public void searchCategoryShop(Map<String, Object> filter) {
        binding.myZoneList.loadShopList(INITIAL,filter);
        binding.myZoneList.disableSearch();
    }

    /* GETTER or SETTER */
    @Override
    public String getStreetName() {
        return activity.getIntent().getStringExtra(STREET_NAME);
    }

    @Override
    public ShopListAdapter getShopListAdapter() {
        return (ShopListAdapter) binding.myZoneList.getAdapter();
    }

    @Override
    public String[] getStreetStringArray() {
        return getStreetArray().toArray(new String[getStreetArray().size()]);
    }
}
