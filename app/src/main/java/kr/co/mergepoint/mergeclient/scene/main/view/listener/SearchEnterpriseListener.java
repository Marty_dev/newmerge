package kr.co.mergepoint.mergeclient.scene.main.view.listener;

public interface SearchEnterpriseListener {
    void searchEnterprise(String searchText);
    void registerEnterprise(int companyRef);
}
