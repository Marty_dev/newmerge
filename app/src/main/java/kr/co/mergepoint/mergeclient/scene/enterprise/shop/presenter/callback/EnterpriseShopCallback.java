package kr.co.mergepoint.mergeclient.scene.enterprise.shop.presenter.callback;

import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;

import kr.co.mergepoint.mergeclient.R;
import kr.co.mergepoint.mergeclient.application.common.BaseCallback;
import kr.co.mergepoint.mergeclient.application.common.MergeCallback;
import kr.co.mergepoint.mergeclient.application.common.WideBannerAdapter;
import kr.co.mergepoint.mergeclient.scene.data.common.ResponseObject;
import kr.co.mergepoint.mergeclient.scene.data.menu.ShopGroupCategory;
import kr.co.mergepoint.mergeclient.scene.data.menudetail.ShopMenuDetail;
import kr.co.mergepoint.mergeclient.scene.data.shop.DailyMenuCategory;
import kr.co.mergepoint.mergeclient.scene.data.shop.ShopDetail;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.model.EnterpriseShopModel;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.EnterpriseShopView;
import kr.co.mergepoint.mergeclient.scene.enterprise.shop.view.fragment.EnterpriseShopMenuFragment;
import kr.co.mergepoint.mergeclient.util.MDEBUG;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kr.co.mergepoint.mergeclient.application.common.Properties.ENTERPRISE_MENU_TAG;

/**
 * Created by 1017sjg on 2017. 9. 15..
 */

public class EnterpriseShopCallback extends BaseCallback<EnterpriseShopView, EnterpriseShopModel> implements EnterpriseShopCallbackInterface {

    public EnterpriseShopCallback(EnterpriseShopView baseView, EnterpriseShopModel baseModel) {
        super(baseView, baseModel);
    }

    @Override
    public MergeCallback<ShopDetail> shopDetailCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ShopDetail detail = response.body();
            if (response.isSuccessful() && detail != null) {
                baseView.binding.setShopDetail(detail);
                baseView.binding.getPagerAdapter().getInfoFragment().setDetail(detail);
                baseView.binding.setImageAdapter(new WideBannerAdapter(R.layout.single_image, detail.shopImage, R.drawable.img_not_shop_pic, null));
                baseView.binding.enterpriseShopDetailPictureLayout.shopPictureIndicator.createDotPanel(detail.shopImage != null ? detail.shopImage.size() : 0);
                baseModel.getMenuCategory(detail.oid, getMenuGroupCategory());

                // 식단표 메뉴 init
                baseModel.getDailyMenuList(new Callback<ResponseObject<ArrayList<ShopMenuDetail>>>() {
                    @Override
                    public void onResponse(Call<ResponseObject<ArrayList<ShopMenuDetail>>> call, Response<ResponseObject<ArrayList<ShopMenuDetail>>> response) {
                        baseModel.isDailymenu = response.body().getObject().size() != 0;
                        baseModel.Dailybtn.setVisibility(baseModel.isDailymenu ? View.VISIBLE : View.GONE);
                        MDEBUG.debug("is Daily menu " + baseModel.isDailymenu);

                    }

                    @Override
                    public void onFailure(Call<ResponseObject<ArrayList<ShopMenuDetail>>> call, Throwable t) {
                        MDEBUG.debug("Error   " +  t.toString());

                    }
                }, baseView.binding.getShopDetail().oid, 0);
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ResponseBody> addBasketCallback() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseBody responseBody = response.body();
            if (response.isSuccessful() && responseBody != null) {
                baseView.animBasket();
                baseView.activity.onBackPressed();
            } else {
                baseView.showAlert(R.string.retry);
            }
        });
    }

    @Override
    public MergeCallback<ShopMenuDetail> addMenuDetail() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ShopMenuDetail detail = response.body();
            if (response.isSuccessful() && detail != null)
                baseView.openMenuDetail(detail);
        });
    }

    @Override
    public MergeCallback<ResponseObject<Integer>> getCheckFavoriteCall() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<Integer> responseObject = response.body();
            if (response.isSuccessful() && responseObject != null && !responseObject.isFailed()) {
                int count = responseObject.getObject();
                ShopDetail shopDetail = baseView.binding.getShopDetail();
                if (shopDetail != null) {
                    shopDetail.favorite = !shopDetail.favorite;
                    shopDetail.setFavoriteCount(count);
                }
            }
        });
    }

    @Override
    public MergeCallback<ShopGroupCategory> getMenuGroupCategory() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ShopGroupCategory category = response.body();
            if (response.isSuccessful() && category != null) {
                baseView.getShopMenuFragment().setMenuGroup(category.getGroupMenus());
            }
        });
    }

    @Override
    public MergeCallback<ResponseObject<ArrayList<ShopMenuDetail>>> getShopDailyMenuList() {
        return new MergeCallback<>(baseView, (call, response) -> {
            ResponseObject<ArrayList<ShopMenuDetail>> menuDetails = response.body();
            if (response.isSuccessful() && menuDetails != null) {
                DailyMenuCategory[] menuCategories = new DailyMenuCategory[] {
                        new DailyMenuCategory("조식"),
                        new DailyMenuCategory("중식"),
                        new DailyMenuCategory("석식"),
                        new DailyMenuCategory("야식"),
                        new DailyMenuCategory("간식")
                };
                for (ShopMenuDetail shopMenuDetail :menuDetails.getObject()) {
                    try {
                        menuCategories[shopMenuDetail.serveType - 1].menuDetails.add(shopMenuDetail);
                    }catch (Exception e ){
                        MDEBUG.debug(e.toString());
                    }
                }
                ArrayList<DailyMenuCategory> group = new ArrayList<>();
                for (DailyMenuCategory menuCategory :menuCategories) {
                    if (menuCategory.menuDetails.size() > 0)
                        group.add(menuCategory);
                }
                MDEBUG.debug("group size : " + group.size() );
                if (baseView.findFragmentByTag(ENTERPRISE_MENU_TAG) != null) {
                    baseView.changeDailyMenu(group, baseModel.getCalendar());
                } else {
                    baseView.openEnterpriseMenu(group, baseModel.getCalendar());
                }
            }
        });
    }
}
