# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/jgson/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

##---------------Begin: proguard configuration common for all Android apps ----------

-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-verbose
-dump class_files.txt
-printseeds seeds.txt
-printusage unused.txt
-printmapping build/outputs/mapping/release/mapping.txt
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

-allowaccessmodification
-keepattributes *Annotation*
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable
-repackageclasses ''

-keepnames class * implements java.io.Serializable
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    !private <fields>;
    !private <methods>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

##---------------End: proguard configuration common for all Android apps ----------

#---------------Begin: proguard configuration for support library  ----------
#v4
-dontwarn android.support.v4.**
-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep class android.support.v4.** { *; }
#v7
-dontwarn android.support.v7.**
-keep class android.support.v7.internal.** { *; }
-keep interface android.support.v7.internal.** { *; }
-keep class android.support.v7.** { *; }
#design
-dontwarn android.support.design.**
-keep class android.support.design.** { *; }
-keep interface android.support.design.** { *; }
-keep public class android.support.design.R$* { *; }

-keep public class * extends android.support.v4.view.ActionProvider {
    public <init>(android.content.Context);
}

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version. We know about them, and they are safe.
-dontwarn android.support.**
-dontwarn com.google.ads.**

-dontnote android.widget.DatePicker$DatePickerDelegate
-dontnote android.widget.DatePickerSpinnerDelegate

##---------------End: proguard configuration for support library  ----------

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-dontnote sun.misc.Unsafe
-dontnote com.google.gson.internal.UnsafeAllocator
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
#-keep class com.google.gson.examples.android.model.** { *; }

# Prevent proguard from stripping interface information from TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer
##---------------End: proguard configuration for Gson  ----------

##---------------Begin: proguard configuration for Retrofit2  ----------

-dontwarn retrofit2.**
-dontwarn org.codehaus.mojo.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions
-keepattributes RuntimeVisibleAnnotations
-keepattributes RuntimeInvisibleAnnotations
-keepattributes RuntimeVisibleParameterAnnotations
-keepattributes RuntimeInvisibleParameterAnnotations

-keepattributes EnclosingMethod

-keepclasseswithmembers class * {
    @retrofit2.* <methods>;
}

-keepclasseswithmembers interface * {
    @retrofit2.* <methods>;
}

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}
-dontnote retrofit2.Platform
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
-dontwarn retrofit2.Platform$Java8

-dontnote okhttp3.**
-dontwarn okhttp3.**
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }

-dontwarn okio.**
-dontwarn java.lang.invoke.**
-dontwarn javax.annotation.**
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement

-keep class kr.co.mergepoint.mergeclient.scene.data.** {*;}
-keep class kr.co.mergepoint.mergeclient.scene.franchise.data.** {*;}

##---------------End: proguard configuration for Retrofit2  ----------

##---------------Begin: proguard configuration for Fresco  ----------
# Fresco v1.5.0 ProGuard rules.
# https://github.com/facebook/fresco

-keep,allowobfuscation @interface com.facebook.common.internal.DoNotStrip

# Do not strip any method/class that is annotated with @DoNotStrip
-keep @com.facebook.common.internal.DoNotStrip class *
-keepclassmembers class * {
    @com.facebook.common.internal.DoNotStrip *;
}

# Keep native methods
-keepclassmembers class * {
    native <methods>;
}

-dontwarn okio.**
-dontwarn com.squareup.okhttp.**
-dontwarn okhttp3.**
-dontwarn javax.annotation.**
-dontwarn com.android.volley.toolbox.**
-dontwarn com.facebook.infer.**
##---------------End: proguard configuration for Fresco  ----------

##---------------Begin: proguard configuration for Fabric and Twitter  ----------

-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**
-keep public class * extends java.lang.Exception

-keep class io.fabric.sdk.android.** {*;}
-dontwarn io.fabric.sdk.android.**
-keep class io.fabric.sdk.android.ActivityLifecycleManager
-dontwarn io.fabric.sdk.android.ActivityLifecycleManager
-keep class io.fabric.sdk.android.Kit
-dontwarn io.fabric.sdk.android.Kit

-dontwarn com.google.appengine.api.urlfetch.**
-dontwarn rx.**
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}
-keep class okio.** { *; }

##---------------End: proguard configuration for Fabric and Twitter  ----------

##---------------Begin: proguard configuration for Google  ----------

-keepnames class * implements android.os.Parcelable
-keepclassmembers class * implements android.os.Parcelable {
  public static final *** CREATOR;
}

-keep @interface android.support.annotation.Keep
-keep @android.support.annotation.Keep class *
-keepclasseswithmembers class * {
  @android.support.annotation.Keep <fields>;
}
-keepclasseswithmembers class * {
  @android.support.annotation.Keep <methods>;
}

-keep @interface com.google.android.gms.common.annotation.KeepName
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
  @com.google.android.gms.common.annotation.KeepName *;
}

#-keep @interface com.google.android.gms.common.util.DynamiteApi
#-keep public @com.google.android.gms.common.util.DynamiteApi class * {
#  public <fields>;
#  public <methods>;
#}

-dontwarn android.security.NetworkSecurityPolicy

##---------------Begin: proguard configuration for Google  ----------

##---------------Begin: proguard configuration for Facebook ----------
-keep class com.facebook.GraphResponse
-dontwarn com.facebook.GraphResponse
-keep class com.facebook.drawee.interfaces.DraweeHierarchy
-dontwarn com.facebook.drawee.interfaces.DraweeHierarchy
-keep class com.facebook.drawee.interfaces.DraweeController
-dontwarn com.facebook.drawee.interfaces.DraweeController
-keep class com.facebook.drawee.generic.GenericDraweeHierarchy
-dontwarn com.facebook.drawee.generic.GenericDraweeHierarchy
-keep class com.facebook.login.LoginManager
-dontwarn com.facebook.login.LoginManager

-keep class com.facebook.login.widget.LoginButton$LoginButtonProperties
-dontwarn com.facebook.login.widget.LoginButton$LoginButtonProperties
-keep class com.facebook.login.widget.ProfilePictureView$OnErrorListener
-dontwarn com.facebook.login.widget.ProfilePictureView$OnErrorListener
-keep class com.facebook.share.widget.LikeView$OnErrorListener
-dontwarn com.facebook.share.widget.LikeView$OnErrorListener

-keepclassmembers class * implements java.io.Serializable {
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keepnames class com.facebook.FacebookActivity
-keepnames class com.facebook.CustomTabActivity

-keep class com.facebook.share.Share
##---------------Begin: proguard configuration for Facebook  ----------

##---------------Begin: proguard configuration for Google  ----------
-keep class com.google.android.gms.common.zza
-dontwarn com.google.android.gms.common.zza
-keep class com.google.android.gms.common.internal.zzq
-dontwarn com.google.android.gms.common.internal.zzq
-keep class com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks
-dontwarn com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks
-keep class com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener
-dontwarn com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener
-keep class com.google.android.gms.common.api.Api
-dontwarn com.google.android.gms.common.api.Api
-keep class com.google.android.gms.common.api.Api$ApiOptions
-dontwarn com.google.android.gms.common.api.Api$ApiOptions
-keep class com.google.android.gms.common.api.internal.zzcz
-dontwarn com.google.android.gms.common.api.internal.zzcz
-keep class com.google.android.gms.common.api.GoogleApi$zza
-dontwarn com.google.android.gms.common.api.GoogleApi$zza

-keep class com.google.android.gms.common.api.internal.zzcz
-dontwarn com.google.android.gms.common.api.internal.zzcz
-keep class com.google.android.gms.common.api.internal.zzm
-dontwarn com.google.android.gms.common.api.internal.zzm
-keep class com.google.android.gms.common.api.internal.zzdd
-dontwarn com.google.android.gms.common.api.internal.zzdd
-keep class com.google.android.gms.common.api.internal.zzbr
-dontwarn com.google.android.gms.common.api.internal.zzbr
-keep class com.google.android.gms.common.api.ResultCallback
-dontwarn com.google.android.gms.common.api.ResultCallback
-keep class com.google.android.gms.common.api.PendingResult$zza
-dontwarn com.google.android.gms.common.api.PendingResult$zza

-keep class com.google.android.gms.common.api.ResultTransform
-dontwarn com.google.android.gms.common.api.ResultTransform
-keep class com.google.android.gms.common.api.internal.zzcf
-dontwarn com.google.android.gms.common.api.internal.zzcf
-keep class com.google.android.gms.common.api.Api$zzb
-dontwarn com.google.android.gms.common.api.Api$zzb
-keep class com.google.android.gms.tasks.TaskCompletionSource
-dontwarn com.google.android.gms.tasks.TaskCompletionSource
-keep class com.google.android.gms.common.api.Api$zzc
-dontwarn com.google.android.gms.common.api.Api$zzc

-keep class com.google.android.gms.common.api.GoogleApiClient
-dontwarn com.google.android.gms.common.api.GoogleApiClient
-keep class com.google.android.gms.common.api.Result
-dontwarn com.google.android.gms.common.api.Result
-keep class com.google.android.gms.common.api.internal.zzdl
-dontwarn com.google.android.gms.common.api.internal.zzdl
-keep class com.google.android.gms.common.internal.zzap
-dontwarn com.google.android.gms.common.internal.zzap
-keep class com.google.android.gms.common.api.internal.zzs
-dontwarn com.google.android.gms.common.api.internal.zzs
-keep class com.google.android.gms.common.internal.zzaf
-dontwarn com.google.android.gms.common.internal.zzaf

-keep class com.google.android.gms.common.api.GoogleApiClient
-dontwarn com.google.android.gms.common.api.GoogleApiClient
-keep class com.google.android.gms.common.api.Result
-dontwarn com.google.android.gms.common.api.Result
-keep class com.google.android.gms.common.api.internal.zzdl
-dontwarn com.google.android.gms.common.api.internal.zzdl
-keep class com.google.android.gms.common.internal.zzap
-dontwarn com.google.android.gms.common.internal.zzap
-keep class com.google.android.gms.common.api.internal.zzs
-dontwarn com.google.android.gms.common.api.internal.zzs
-keep class com.google.android.gms.common.internal.zzaf
-dontwarn com.google.android.gms.common.internal.zzaf
-keep class com.google.android.gms.common.GoogleApiAvailability
-dontwarn com.google.android.gms.common.GoogleApiAvailability

-keep class com.google.android.gms.common.internal.zzg
-dontwarn com.google.android.gms.common.internal.zzg
-keep class com.google.android.gms.common.zze
-dontwarn com.google.android.gms.common.zze
-keep class com.google.android.gms.common.internal.zzf
-dontwarn com.google.android.gms.common.internal.zzf
-keep class com.google.android.gms.common.internal.zzj
-dontwarn com.google.android.gms.common.internal.zzj
-keep class com.google.android.gms.common.internal.zzam
-dontwarn com.google.android.gms.common.internal.zzam
-keep class com.google.android.gms.common.internal.zzd
-dontwarn com.google.android.gms.common.internal.zzd
-keep class com.google.android.gms.common.internal.zzax
-dontwarn com.google.android.gms.common.internal.zzax

-keep class com.google.android.gms.dynamite.zza
-dontwarn com.google.android.gms.dynamite.zza
-keep class com.google.android.gms.dynamic.IObjectWrapper
-dontwarn com.google.android.gms.dynamic.IObjectWrapper
-keep class com.google.android.gms.measurement.AppMeasurement$EventInterceptor
-dontwarn com.google.android.gms.measurement.AppMeasurement$EventInterceptor
-keep class com.google.android.gms.measurement.AppMeasurement$OnEventListener
-dontwarn com.google.android.gms.measurement.AppMeasurement$OnEventListener
-keep class com.google.android.gms.measurement.AppMeasurement$zza
-dontwarn com.google.android.gms.measurement.AppMeasurement$zza
-keep class com.google.android.gms.measurement.AppMeasurement$zzb
-dontwarn com.google.android.gms.measurement.AppMeasurement$zzb
-keep class com.google.android.gms.common.api.GoogleApi
-dontwarn com.google.android.gms.common.api.GoogleApi
-keep class com.google.zxing.datamatrix.detector.Detector$ResultPointsAndTransitions
-dontwarn com.google.zxing.datamatrix.detector.Detector$ResultPointsAndTransitions
-keep class com.google.zxing.datamatrix.detector.Detector$1
-dontwarn com.google.zxing.datamatrix.detector.Detector$1
-keep class com.google.zxing.multi.qrcode.QRCodeMultiReader$1
-dontwarn com.google.zxing.multi.qrcode.QRCodeMultiReader$1
-keep class com.google.zxing.multi.qrcode.detector.MultiFinderPatternFinder$1
-dontwarn com.google.zxing.multi.qrcode.detector.MultiFinderPatternFinder$1
-keep class com.google.zxing.common.BitMatrix
-dontwarn com.google.zxing.common.BitMatrix
-keep class com.google.zxing.qrcode.detector.FinderPatternFinder$1
-dontwarn com.google.zxing.qrcode.detector.FinderPatternFinder$1
-keep class com.google.zxing.qrcode.detector.FinderPattern
-dontwarn com.google.zxing.qrcode.detector.FinderPattern

##---------------Begin: proguard configuration for Google ----------

-keep class bolts.Task
-dontwarn bolts.Task

-keep class android.databinding.Observable$OnPropertyChangedCallback
-dontwarn android.databinding.Observable$OnPropertyChangedCallback
-keep class android.databinding.ObservableList$OnListChangedCallback
-dontwarn android.databinding.ObservableList$OnListChangedCallback

-keep class bolts.Task$TaskCompletionSource
-dontwarn bolts.Task$TaskCompletionSource

##---------------Begin: proguard configuration for Gson  ----------

-keep class com.google.gson.LongSerializationPolicy$1
-dontwarn com.google.gson.LongSerializationPolicy$1
-keep class com.google.gson.JsonSerializationContext
-dontwarn com.google.gson.JsonSerializationContext
-keep class com.google.gson.JsonDeserializationContext
-dontwarn com.google.gson.JsonDeserializationContext
-keep class com.google.gson.Gson
-dontwarn com.google.gson.Gson
-keep class com.google.gson.JsonObject
-dontwarn com.google.gson.JsonObject
-keep class com.google.gson.reflect.TypeToken
-dontwarn com.google.gson.reflect.TypeToken
-keep class com.google.gson.TypeAdapter
-dontwarn com.google.gson.TypeAdapter

-keep class com.google.gson.FieldNamingPolicy$1
-dontwarn com.google.gson.FieldNamingPolicy$1
-keep class com.google.gson.stream.JsonWriter
-dontwarn com.google.gson.stream.JsonWriter
-keep class com.google.gson.stream.JsonReader
-dontwarn com.google.gson.stream.JsonReader
-keep class com.google.gson.internal.LinkedHashTreeMap$Node
-dontwarn com.google.gson.internal.LinkedHashTreeMap$Node
-keep class com.google.gson.internal.LinkedHashTreeMap$Node
-dontwarn com.google.gson.internal.LinkedHashTreeMap$Node
-keep class com.google.gson.JsonElement
-dontwarn com.google.gson.JsonElement

##---------------Begin: proguard configuration for Gson  ----------

-keep class com.google.firebase.FirebaseApp
-dontwarn com.google.firebase.FirebaseApp

-keep class com.google.zxing.qrcode.decoder.Version
-dontwarn com.google.zxing.qrcode.decoder.Version

-keep class com.google.gson.internal.LinkedTreeMap$Node
-dontwarn com.google.gson.internal.LinkedTreeMap$Node

##---------------Begin: proguard configuration for Twitter  ----------

-keep class com.twitter.sdk.android.core.models.ApiError
-dontwarn com.twitter.sdk.android.core.models.ApiError
-keep class com.twitter.sdk.android.core.TwitterRateLimit
-dontwarn com.twitter.sdk.android.core.TwitterRateLimit
-keep class com.twitter.sdk.android.core.Callback
-dontwarn com.twitter.sdk.android.core.Callback
-keep class com.twitter.sdk.android.core.models.Tweet
-dontwarn com.twitter.sdk.android.core.models.Tweet
-keep class com.twitter.sdk.android.core.models.Card
-dontwarn com.twitter.sdk.android.core.models.Card
-keep class com.twitter.sdk.android.core.internal.scribe.ScribeItem$1
-dontwarn com.twitter.sdk.android.core.internal.scribe.ScribeItem$1
-keep class com.twitter.sdk.android.core.models.UserEntities
-dontwarn com.twitter.sdk.android.core.models.UserEntities

-keep class com.twitter.sdk.android.core.services.params.Geocode
-dontwarn com.twitter.sdk.android.core.services.params.Geocode
-keep class com.twitter.sdk.android.tweetcomposer.ComposerController$ComposerCallbacks
-dontwarn com.twitter.sdk.android.tweetcomposer.ComposerController$ComposerCallbacks
-keep class com.twitter.sdk.android.tweetcomposer.TweetUploadService$DependencyProvider
-dontwarn com.twitter.sdk.android.tweetcomposer.TweetUploadService$DependencyProvider
-keep class com.twitter.sdk.android.tweetcomposer.internal.util.ObservableScrollView$ScrollViewListener
-dontwarn com.twitter.sdk.android.tweetcomposer.internal.util.ObservableScrollView$ScrollViewListener
-keep class com.twitter.sdk.android.core.Callback
-dontwarn com.twitter.sdk.android.core.Callback

-dontwarn retrofit.**
-keep class retrofit.** { *; }

##---------------Begin: proguard configuration for Twitter  ----------

##---------------Begin: proguard configuration for Mergepoint ----------

-keep class kr.co.mergepoint.mergeclient.scene.data.history.UseMenuList
-dontwarn kr.co.mergepoint.mergeclient.scene.data.history.UseMenuList
-keep class kr.co.mergepoint.mergeclient.application.common.BookMark$OnCheckFavorite
-dontwarn kr.co.mergepoint.mergeclient.application.common.BookMark$OnCheckFavorite
-keep class kr.co.mergepoint.mergeclient.scene.shop.view.listener.OnRadioChange
-dontwarn kr.co.mergepoint.mergeclient.scene.shop.view.listener.OnRadioChange
-keep class kr.co.mergepoint.mergeclient.application.common.CustomRecyclerView$CustomRecyclerViewTouch
-dontwarn kr.co.mergepoint.mergeclient.application.common.CustomRecyclerView$CustomRecyclerViewTouch
-keep class kr.co.mergepoint.mergeclient.application.common.BasicListAdapter
-dontwarn kr.co.mergepoint.mergeclient.application.common.BasicListAdapter
-keep class kr.co.mergepoint.mergeclient.application.common.CustomViewPager$LikeListener
-dontwarn kr.co.mergepoint.mergeclient.application.common.CustomViewPager$LikeListener
-keep class kr.co.mergepoint.mergeclient.application.common.LoadRecyclerView$LoadRecyclerViewListener
-dontwarn kr.co.mergepoint.mergeclient.application.common.LoadRecyclerView$LoadRecyclerViewListener
-keep class kr.co.mergepoint.mergeclient.scene.shoplist.view.listener.OnFilterChange
-dontwarn kr.co.mergepoint.mergeclient.scene.shoplist.view.listener.OnFilterChange
-keep class kr.co.mergepoint.mergeclient.application.common.OnTypeListener
-dontwarn kr.co.mergepoint.mergeclient.application.common.OnTypeListener

-keep class kr.co.mergepoint.mergeclient.application.common.CustomDatePickerDialog
-dontwarn kr.co.mergepoint.mergeclient.application.common.CustomDatePickerDialog

##---------------Begin: proguard configuration for Mergepoint  ----------

##---------------Begin: proguard configuration for Mergepoint  ----------

-keep class dagger.internal.ReferenceReleasingProvider
-dontwarn dagger.internal.ReferenceReleasingProvider
-keep class dagger.internal.ReferenceReleasingProviderManager$1
-dontwarn dagger.internal.ReferenceReleasingProviderManager$1

##---------------Begin: proguard configuration for Mergepoint  ----------

##---------------Begin: proguard configuration for DaumMap  ----------

-keep class net.daum.mf.map.n.** { *; }
-keep class net.daum.mf.map.api.MapView { *; }
-keep class net.daum.android.map.location.MapViewLocationManager { *; }
-keep class net.daum.mf.map.api.MapPolyline { *; }
-keep class net.daum.mf.map.api.MapPoint** { *; }

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keep class android.opengl.alta.GLSurfaceView$GLWrapper
-dontwarn android.opengl.alta.GLSurfaceView$GLWrapper

-keep class android.opengl.alt.GLSurfaceView$GLWrapper
-dontwarn android.opengl.alt.GLSurfaceView$GLWrapper
-keep class android.opengl.alt.GLSurfaceView$Renderer
-dontwarn android.opengl.alt.GLSurfaceView$Renderer
-keep class android.opengl.alt.GLSurfaceView$EGLContextFactory
-dontwarn android.opengl.alt.GLSurfaceView$EGLContextFactory
-keep class android.opengl.alt.GLSurfaceView$EGLWindowSurfaceFactory
-dontwarn android.opengl.alt.GLSurfaceView$EGLWindowSurfaceFactory
-keep class android.opengl.alt.GLSurfaceView$EGLConfigChooser
-dontwarn android.opengl.alt.GLSurfaceView$EGLConfigChooser

-keep class android.opengl.alta.GLSurfaceView$Renderer
-dontwarn android.opengl.alta.GLSurfaceView$Renderer
-keep class android.opengl.alta.GLSurfaceView$EGLContextFactory
-dontwarn android.opengl.alta.GLSurfaceView$EGLContextFactory
-keep class android.opengl.alta.GLSurfaceView$EGLWindowSurfaceFactory
-dontwarn android.opengl.alta.GLSurfaceView$EGLWindowSurfaceFactory
-keep class android.opengl.alta.GLSurfaceView$EGLConfigChooser
-dontwarn android.opengl.alta.GLSurfaceView$EGLConfigChooser
-keep class com.google.android.gms.common.api.internal.zzce
-dontwarn com.google.android.gms.common.api.internal.zzce

-keep class net.daum.mf.map.api.MapPoint$GeoCoordinate
-dontwarn net.daum.mf.map.api.MapPoint$GeoCoordinate
-keep class net.daum.android.map.MapViewTouchEventListener
-dontwarn net.daum.android.map.MapViewTouchEventListener
-keep class net.daum.mf.map.api.MapView$OpenAPIKeyAuthenticationResultListener
-dontwarn net.daum.mf.map.api.MapView$OpenAPIKeyAuthenticationResultListener
-keep class net.daum.mf.map.api.MapView$CurrentLocationEventListener
-dontwarn net.daum.mf.map.api.MapView$CurrentLocationEventListener
-keep class net.daum.mf.map.api.MapView$POIItemEventListener
-dontwarn net.daum.mf.map.api.MapView$POIItemEventListener
-keep class net.daum.mf.map.api.CalloutBalloonAdapter
-dontwarn net.daum.mf.map.api.CalloutBalloonAdapter

-keep class net.daum.android.map.MapViewEventListener
-dontwarn net.daum.android.map.MapViewEventListener
-keep class net.daum.mf.map.api.MapView$MapViewEventListener
-dontwarn net.daum.mf.map.api.MapView$MapViewEventListener
-keep class net.daum.mf.map.api.MapCurrentLocationMarker
-dontwarn net.daum.mf.map.api.MapCurrentLocationMarker
-keep class net.daum.mf.map.n.api.NativeConvertibleMapCoord
-dontwarn net.daum.mf.map.n.api.NativeConvertibleMapCoord

-keep class net.daum.mf.map.n.api.NativeMapCoord
-dontwarn net.daum.mf.map.n.api.NativeMapCoord
-keep class net.daum.mf.map.api.MapPolyline
-dontwarn net.daum.mf.map.api.MapPolyline
-keep class net.daum.mf.map.n.api.NativeMapViewUiEvent
-dontwarn net.daum.mf.map.n.api.NativeMapViewUiEvent

-keep class android.opengl.alt.GLSurfaceView
-dontwarn android.opengl.alt.GLSurfaceView
-keep class net.daum.android.map.location.MapViewLocationManager$GpsSeekingTimerTask
-dontwarn net.daum.android.map.location.MapViewLocationManager$GpsSeekingTimerTask
-keep class net.daum.android.map.coord.MapCoord
-dontwarn net.daum.android.map.coord.MapCoord

-keep class net.daum.mf.map.api.MapPOIItem$ImageOffset
-dontwarn net.daum.mf.map.api.MapPOIItem$ImageOffset
-keep class net.daum.mf.map.api.MapPOIItem
-dontwarn net.daum.mf.map.api.MapPOIItem
-keep class net.daum.mf.map.api.MapCircle
-dontwarn net.daum.mf.map.api.MapCircle

-keep class net.daum.mf.map.api.CameraUpdate
-dontwarn net.daum.mf.map.api.CameraUpdate
-keep class net.daum.mf.map.api.CancelableCallback
-dontwarn net.daum.mf.map.api.CancelableCallback
-keep class net.daum.android.map.coord.MapCoordLatLng
-dontwarn net.daum.android.map.coord.MapCoordLatLng

##---------------Begin: proguard configuration for DaumMap  ----------

-keep class me.dm7.barcodescanner.core.CameraWrapper
-dontwarn me.dm7.barcodescanner.core.CameraWrapper
-keep class me.dm7.barcodescanner.zxing.ZXingScannerView$ResultHandler
-dontwarn me.dm7.barcodescanner.zxing.ZXingScannerView$ResultHandler

-keep class com.google.zxing.Result
-dontwarn com.google.zxing.Result

-keep class com.squareup.picasso.Picasso
-dontwarn com.squareup.picasso.Picasso

-dontnote android.net.http.*
-dontnote org.apache.http.**
-dontnote com.google.android.gms.internal.**
-dontnote com.facebook.**
-dontnote com.squareup.picasso.Utils
-dontnote com.crashlytics.android.core.CrashlyticsController
-dontnote io.fabric.sdk.android.FabricKGoogleApiAvailabilSafeParcelableityitsFinder
-dontnote io.fabric.sdk.android.FabricKitsFinder
-dontnote com.google.android.gms.common.internal.safeparcel.SafeParcelable
-dontnote com.android.vending.billing.IInAppBillingService

-dontnote com.google.android.gms.common.api.internal.BasePendingResult.ReleasableResultGuardian
-dontnote com.google.android.gms.common.api.internal.BasePendingResult

##-----------------------Begin: Proguard DataClass By EunChan---- Emergency
-keep class kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.CrowdSimpleListdata
-keep class kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.PaymentRequest
-keep class kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopCoupon
-keep class kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopPayment
-keep class kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.ShopTicket
-keep class kr.co.mergepoint.mergeclient.scene.testpayment.payment.data.** {*;}


# Begin: Proguard rules for okhttp3

-dontwarn okhttp3.**
-dontwarn okio.**

-dontnote okhttp3.**

# End: Proguard rules for okhttp3

# Begin: Proguard rules for retrofit2

# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on RoboVM on iOS. Will not be used at runtime.
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

# End: Proguard rules for retrofit2
